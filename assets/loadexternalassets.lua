--external assets

			--level1frg2 = loadAsset("externalassets/levels/level1/level1frg2.webp", "Image")
			--level1frg = loadAsset("externalassets/levels/level1/level1frg.webp", "Image")
			--level1bck = loadAsset("/externalassets/levels/level1/level1bck.webp", "Image")
			--level1bck2 = loadAsset("/externalassets/levels/level1/level1bck2.webp", "Image")

--[[
This code defines various assets like images, fonts, and sounds to be used in a game or application.
The loadavatars() function loads different avatar images based on the value of the skin variable.
The menu section loads various images for the game menu, including a map, different branches, and fonts.
The sounds section loads different sound effects used in the game.
These assets are loaded using the Love2D framework's love.graphics.newImage(), love.graphics.setFont(), and love.audio.newSource() functions, which load images, fonts, and audio files respectively. The love.filesystem.newFileData() function is used to read the asset files from the file system.
--]]

	function loadavatars()
		if skin=="remake" then
			avatar = loadAsset("lib/talkies/example/assets/fishtalk.webp", "Image")
			avatar2 = loadAsset("lib/talkies/example/assets/fishtalk2.webp", "Image")
	elseif skin=="classic" then
			avatar = loadAsset("lib/talkies/example/assets/fishtalkcl.webp", "Image")
			avatar2 = loadAsset("lib/talkies/example/assets/fishtalk2cl.webp", "Image")
	end
	end

	--menu
	
	menumap						=  loadAsset("externalassets/classic/menu/map.webp", "Image")
	menumapupscaled 			=  loadAsset("externalassets/classic/menu/upscaled/map.webp", "Image")
	menumapremake				=  loadAsset("externalassets/levels/menu/map.webp", "Image")
	menumapupscaled_complete	=  loadAsset("externalassets/classic/menu/upscaled/map_complete.webp", "Image")
	
	allbranch =  loadAsset("externalassets/classic/menu/upscaled/allbranch.webp", "Image")
	branch1house =  loadAsset("externalassets/classic/menu/upscaled/branch1house.webp", "Image")
	branch2ship =  loadAsset("externalassets/classic/menu/upscaled/branch2ship.webp", "Image")
	branch3city =  loadAsset("externalassets/classic/menu/upscaled/branch3city.webp", "Image")
	branch4 =  loadAsset("externalassets/classic/menu/upscaled/branch4.webp", "Image")
	branch5 =  loadAsset("externalassets/classic/menu/upscaled/branch5.webp", "Image")
	branch67 =  loadAsset("externalassets/classic/menu/upscaled/branch6-7.webp", "Image")
	branch89 =  loadAsset("externalassets/classic/menu/upscaled/branch8-9.webp", "Image")
	
--fonts

poorfish = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 84) -- Thanks @hicchicc for the font
poorfishmiddle = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 64) -- Thanks @hicchicc for the font
poorfishsmall = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32) -- Thanks @hicchicc for the font

 love.graphics.setFont(poorfish)



--sounds
scrape6 = love.audio.newSource( "externalassets/sounds/scrape/scrape6_lowfilter.ogg","static" )
underwatermovement2 = love.audio.newSource( "externalassets/sounds/scrape/underwater_object_movement_quick_deep_15.ogg","static" )
impactmetal = love.audio.newSource( "externalassets/sounds/impact/Impact_MetalDust_lowfilter.ogg","static" )
impactgolf = love.audio.newSource( "externalassets/sounds/impact/Underwater_Golfball_Bounce_Fienup_001.ogg","static" )
gui21 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static" )
gui24 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static" )
gui28 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_028.ogg","static" )
gui29 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_029.ogg","static" )
gui30 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static" )

gui071 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_071.ogg","static" )
gui072 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_072.ogg","static" )
gui073 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_073.ogg","static" )
gui074 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_074.ogg","static" )
gui075 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_075.ogg","static" )
gui076 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_076.ogg","static" )
gui077 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_077.ogg","static" )
gui078 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_078.ogg","static" )
gui079 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_079.ogg","static" )
gui080 = love.audio.newSource( "externalassets/sounds/GUI Sound Effects_8_BY_Jalastram/GUI Sound Effects_080.ogg","static" )
negativegui = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative.ogg","static" )


switch_off_sound = love.audio.newSource( "externalassets/sounds/level56/switch_off.ogg","static" )
switch_on_sound = love.audio.newSource( "externalassets/sounds/level56/switch_on.ogg","static" )
switch_off_sound:setVolume(0.4)
switch_on_sound:setVolume(0.4)

bubbles = love.audio.newSource( "externalassets/sounds/bubbles.ogg","static" )
bubbles:setVolume(0.5)
LoadSound = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects_by_Lokif/load.ogg","static" )
SaveSound = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects_by_Lokif/save.ogg","static" )

--gamepad sounds
misc_menu_4 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects_by_Lokif/misc_menu_4.ogg","static" )

-- Screenshot sound
photosound = love.audio.newSource( "externalassets/sounds/photo.ogg","static" )
