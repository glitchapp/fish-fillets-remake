--[[
This code section loads and sets various images to be used in the game.

The first part sets up the mouse pointer using love.mouse.getSystemCursor() to select the "ibeam" system cursor, and then overrides it with a custom image using love.mouse.newCursor().
--]]

--mouse pointer
--cursor = love.mouse.getSystemCursor( "ibeam" )
--cursor = love.mouse.newCursor( (WebP.loadImage(love.filesystem.newFileData("/assets/interface/fishpointer.webp", "Image"), 0, 0 )
--cursor = love.mouse.newCursor("/assets/interface/fishpointer100.png", 0, 0 )

cursor = love.mouse.newCursor("/assets/interface/ichtys_cursor100.png", 0, 0 )
love.mouse.setCursor(cursor)
ichtys_cursor100= love.graphics.newImage("/assets/interface/ichtys_cursor100.png") -- load in a custom mouse image

cursorRetro = love.mouse.newCursor("/assets/interface/ichtys_cursor100Retro.png", 0, 0 )
--love.mouse.setCursor(cursorRetro)
ichtys_cursor100Retro= love.graphics.newImage("/assets/interface/ichtys_cursor100Retro.png") -- load in a custom mouse image

--level1frg2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg2.webp")))

    xboxgamepad= love.graphics.newImage("/assets/interface/xboxgamepad2.png") 
    
    xboxgamepad1= loadAsset("/assets/interface/xboxgamepad.webp", "Image") 
	
--Gamepad
Abutton =  loadAsset("/assets/interface/Abutton.webp", "Image")
Bbutton =  loadAsset("/assets/interface/Bbutton.webp", "Image")
Xbutton =  loadAsset("/assets/interface/Xbutton.webp", "Image")
Ybutton =  loadAsset("/assets/interface/Ybutton.webp", "Image")
leftshoulder = love.graphics.newImage("assets/interface/leftshoulder.png")
rightshoulder = love.graphics.newImage("assets/interface/rightshoulder.png")

-- Key icons

KeyIcon =   loadAsset("/assets/interface/KeyIcon.webp", "Image")
Qkeyicon =  loadAsset("/assets/interface/Qkeyicon.webp", "Image")
EKeyicon =  loadAsset("/assets/interface/Ekeyicon.webp", "Image")

-- Level editor icons

ShellIcon =  love.graphics.newImage("/assets/interface/editor/shellpointer100.png")
Eraser =  loadAsset("/assets/interface/editor/eraser.webp", "Image")
borderIcon =  loadAsset("/assets/interface/editor/border.webp", "Image")
objectIcon =  loadAsset("/assets/interface/editor/object.webp", "Image")
ExitEditor =  loadAsset("/assets/interface/editor/exit.webp", "Image")
heavyIcon =  loadAsset("/assets/interface/editor/heavy.webp", "Image")
lightIcon =  loadAsset("/assets/interface/editor/feather.webp", "Image")
 AddLineIcon =  loadAsset("/assets/interface/editor/AddLine.webp", "Image")
 AddRectangleIcon =  loadAsset("/assets/interface/editor/AddRectangle.webp", "Image")
 AddCircleIcon =  loadAsset("/assets/interface/editor/AddCircle.webp", "Image")


AboutIcon =  loadAsset("/assets/interface/editor/helpicon.webp", "Image")
InfoIcon =  loadAsset("/assets/interface/editor/info.webp", "Image")
levelEditorIcon =  loadAsset("/assets/interface/editor/leveleditor.webp", "Image")
tilesIcon =  loadAsset("/assets/interface/editor/tiles.webp", "Image")
bckIcon =  loadAsset("/assets/interface/editor/background.webp", "Image")



PlayIcon =  loadAsset("/assets/interface/editor/play.webp", "Image")
LoadIcon =  loadAsset("/assets/interface/editor/load.webp", "Image")
ExportIcon =  loadAsset("/assets/interface/editor/export.webp", "Image")
FolderIcon =  loadAsset("/assets/interface/editor/folder.webp", "Image")
ScreenshotIcon =  loadAsset("/assets/interface/editor/Screenshot.webp", "Image")
clearIcon =  loadAsset("/assets/interface/editor/clearIcon.webp", "Image")
ToggleGridIcon=  loadAsset("/assets/interface/editor/Grid.webp", "Image")
gridWidthIcon=  loadAsset("/assets/interface/editor/gridWidth.webp", "Image")
gridHeightIcon=  loadAsset("/assets/interface/editor/gridHeight.webp", "Image")
zoomInIcon =  loadAsset("/assets/interface/editor/zoomIn.webp", "Image")
zoomOutIcon =  loadAsset("/assets/interface/editor/zoomOut.webp", "Image")
DropCustomLevelIcon =  loadAsset("/assets/interface/editor/DropCustomLevel.webp", "Image")


nesfish3x1mini = loadAsset("/assets/interface/editor/fish-3x1.webp", "Image")
nesfish4x2mini = loadAsset("/assets/interface/editor/fish-4x2.webp", "Image")

SwitchOFF =  loadAsset("/assets/interface/SwitchOFF100.webp", "Image")
SwitchON =  loadAsset("/assets/interface/SwitchON100.webp", "Image")


--The next section loads in custom images for the debug interface, including a custom mouse image and a reload icon.
--debug interface
--cursor = loadAsset("assets/interface/debug/cursor.webp", "Image") -- load in a custom mouse image

reload=loadAsset("assets/interface/debug/reload.webp", "Image")

--shader mask
--The next line loads an image to be used as a shader mask for the levels.
smask = loadAsset("/assets/levels/mask.webp", "Image")

-- tiles textures applied on the retro mode and level editor
tileTextures = {
    [1] = loadAsset("externalassets/textures/level1Tile.webp", "Image"),
    [2] = loadAsset("externalassets/textures/level3Tile.webp", "Image"),
    [3] = loadAsset("externalassets/textures/level11Tile.webp", "Image"),
    [4] = loadAsset("externalassets/textures/level52Tile.webp", "Image"),
    [5] = loadAsset("externalassets/textures/speccy/Tile1.webp", "Image"),
    [6] = loadAsset("externalassets/textures/speccy/Tile2.webp", "Image"),
    [7] = loadAsset("externalassets/textures/speccy/Tile3.webp", "Image"),
    [8] = loadAsset("externalassets/textures/speccy/Tile4.webp", "Image"),
    [9] = loadAsset("externalassets/textures/speccy/Tile5.webp", "Image"),
    [10] = loadAsset("externalassets/textures/speccy/Tile6.webp", "Image"),
    [11] = loadAsset("externalassets/textures/speccy/Tile7.webp", "Image"),
}

--Finally, the section loads in images for the various players in the game, including different variations of fish sprites for the Sinclair ZX Spectrum, NES, Commodore 64, and a "grid" version. Each fish variation comes in two sizes, 3x1 and 4x2.
--players
	z80fish3x1 = loadAsset("/assets/players/sinclair/fish-3x1.webp", "Image")
	z80fish4x2 = loadAsset("/assets/players/sinclair/fish-4x2.webp", "Image")
	
	nesfish3x1 = loadAsset("/assets/players/nes/fish-3x1.webp", "Image")
	nesfish4x2 = loadAsset("/assets/players/nes/fish-4x2.webp", "Image")
	
	c64fish3x1 = loadAsset("/assets/players/c64/fish-3x1.webp", "Image")
	c64fish4x2 = loadAsset("/assets/players/c64/fish-4x2.webp", "Image")
	
	gridfish3x1 = loadAsset("/assets/players/grid/fish-3x1.webp", "Image")
	gridfish4x2 = loadAsset("/assets/players/grid/fish-4x2.webp", "Image")
	
--[[
This code appears to be defining and loading images for fish characters in a game using the Lua programming language and the Love2D game engine.

The code contains a conditional statement using if and elseif to determine which type of fish skin to load. If the skin is "remake", then the code loads images of two fish characters, fish4x2 and fish3x1, each with a 100% resolution and also with 400% resolution. Additionally, the code loads images of two dead fish, fish1dead and fish2dead.
--]]

	--fishes
	--if skin=="remake" then
		--fish4x2 = loadAsset("/assets/players/fish-4x2.webp", "Image")
		--fish3x1 = loadAsset("/assets/players/fish-3x1.webp", "Image")
		fish4x2_100 = loadAsset("/assets/players/fish-4x2.webp", "Image")
		fish3x1_100 = loadAsset("/assets/players/fish-3x1.webp", "Image")
		fish4x2 = loadAsset("/assets/players/fish-4x2-x4.webp", "Image")	-- 400% resolution
		fish3x1 = loadAsset("/assets/players/fish-3x1-x4.webp", "Image")	-- 400% resolution
		
		--normals
		fish4x2Normal = loadAsset("/assets/players/fish-4x2-x4Normals.webp", "Image")	-- 400% resolution
		fish3x1Normal = loadAsset("/assets/players/fish-3x1-x4Normals.webp", "Image")	-- 400% resolution
		
		fish1dead = loadAsset("externalassets/classic/fishes/upscaled/big/body_skeleton_00.webp", "Image")
		fish2dead = loadAsset("externalassets/classic/fishes/upscaled/small/body_skeleton_00.webp", "Image")
		
		--height maps
		fish4x2Height = loadAsset("/assets/players/fish-4x2-x4heightmap.webp", "Image")	-- 400% resolution
		fish3x1Height = loadAsset("/assets/players/fish-3x1-x4heightmap.webp", "Image")	-- 400% resolution
	
	--If the skin is "classic", the code loads images for several fish frames with different poses, including fish4x2cl and fish3x1cl. It also loads images for the selection aura, rest frames, rest contour, and swam frames of these fish characters.
	--Overall, this code seems to be loading a variety of fish character images based on the skin type selected, likely for use in a game.
	--elseif skin=="classic" then
		--if skinupscaled==false then
			--stand
			fish4x2cl = loadAsset("/assets/players/classic/fish-4x2.webp", "Image")
			fish3x1cl = loadAsset("/assets/players/classic/fish-3x1.webp", "Image")
			--rest frames
			smallfishframe=0
			bigfishframe=0
			
			--selection aura
			fish4x2claura = loadAsset("/externalassets/classic/fishes/big/aura.webp", "Image")
			fish3x1claura = loadAsset("/externalassets/classic/fishes/small/aura.webp", "Image")
			
			--rest
			fish4x2clrest00 = loadAsset("/externalassets/classic/fishes/big/body_rest_00.webp", "Image")
			fish4x2clrest01 = loadAsset("/externalassets/classic/fishes/big/body_rest_01.webp", "Image")
			fish4x2clrest02 = loadAsset("/externalassets/classic/fishes/big/body_rest_02.webp", "Image")
			
			fish3x1clrest00 = loadAsset("/externalassets/classic/fishes/small/body_rest_00.webp", "Image")
			fish3x1clrest01 = loadAsset("/externalassets/classic/fishes/small/body_rest_01.webp", "Image")
			fish3x1clrest02 = loadAsset("/externalassets/classic/fishes/small/body_rest_02.webp", "Image")
			
			--rest contour
			fish4x2clrest00co = loadAsset("/externalassets/classic/fishes/big/grid/body_rest_00.webp", "Image")
			fish4x2clrest01co = loadAsset("/externalassets/classic/fishes/big/grid/body_rest_01.webp", "Image")
			fish4x2clrest02co = loadAsset("/externalassets/classic/fishes/big/grid/body_rest_02.webp", "Image")
			
			fish3x1clrest00co = loadAsset("/externalassets/classic/fishes/small/grid/body_rest_00.webp", "Image")
			fish3x1clrest01co = loadAsset("/externalassets/classic/fishes/small/grid/body_rest_01.webp", "Image")
			fish3x1clrest02co = loadAsset("/externalassets/classic/fishes/small/grid/body_rest_02.webp", "Image")
			
			--swam
			fish4x2clswam00 = loadAsset("/externalassets/classic/fishes/big/body_swam_00.webp", "Image")
			fish4x2clswam01 = loadAsset("/externalassets/classic/fishes/big/body_swam_01.webp", "Image")
			fish4x2clswam02 = loadAsset("/externalassets/classic/fishes/big/body_swam_02.webp", "Image")
			fish4x2clswam03 = loadAsset("/externalassets/classic/fishes/big/body_swam_03.webp", "Image")
			fish4x2clswam04 = loadAsset("/externalassets/classic/fishes/big/body_swam_04.webp", "Image")
			fish4x2clswam05 = loadAsset("/externalassets/classic/fishes/big/body_swam_05.webp", "Image")
			
			fish3x1clswam00 = loadAsset("/externalassets/classic/fishes/small/body_swam_00.webp", "Image")
			fish3x1clswam01 = loadAsset("/externalassets/classic/fishes/small/body_swam_01.webp", "Image")
			fish3x1clswam02 = loadAsset("/externalassets/classic/fishes/small/body_swam_02.webp", "Image")
			fish3x1clswam03 = loadAsset("/externalassets/classic/fishes/small/body_swam_03.webp", "Image")
			fish3x1clswam04 = loadAsset("/externalassets/classic/fishes/small/body_swam_04.webp", "Image")
			fish3x1clswam05 = loadAsset("/externalassets/classic/fishes/small/body_swam_05.webp", "Image")
			
			--swam contour
			fish4x2clswam00co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_00.webp", "Image")
			fish4x2clswam01co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_01.webp", "Image")
			fish4x2clswam02co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_02.webp", "Image")
			fish4x2clswam03co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_03.webp", "Image")
			fish4x2clswam04co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_04.webp", "Image")
			fish4x2clswam05co = loadAsset("/externalassets/classic/fishes/big/grid/body_swam_05.webp", "Image")
			
			fish3x1clswam00co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_00.webp", "Image")
			fish3x1clswam01co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_01.webp", "Image")
			fish3x1clswam02co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_02.webp", "Image")
			fish3x1clswam03co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_03.webp", "Image")
			fish3x1clswam04co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_04.webp", "Image")
			fish3x1clswam05co = loadAsset("/externalassets/classic/fishes/small/grid/body_swam_05.webp", "Image")
			
			--dead
			fish1deadcl = loadAsset("externalassets/classic/fishes/big/body_skeleton_00.webp", "Image")
			fish2deadcl = loadAsset("externalassets/classic/fishes/small/body_skeleton_00.webp", "Image")
			
			--pushing
			
			fish4x2clpush00 = loadAsset("/externalassets/classic/fishes/big/body_push_00.webp", "Image")
			fish3x1clpush00 = loadAsset("/externalassets/classic/fishes/small/body_push_00.webp", "Image")
			
			--pushing contour
			
			fish4x2clpush00co = loadAsset("/externalassets/classic/fishes/big/grid/body_push_00.webp", "Image")
			fish3x1clpush00co = loadAsset("/externalassets/classic/fishes/small/grid/body_push_00.webp", "Image")
			
			--talking
			
			fish4x2cltalk00 = loadAsset("/externalassets/classic/fishes/big/body_talk_00.webp", "Image")
			fish4x2cltalk01 = loadAsset("/externalassets/classic/fishes/big/body_talk_01.webp", "Image")
			fish4x2cltalk02 = loadAsset("/externalassets/classic/fishes/big/body_talk_02.webp", "Image")
			
			fish3x1cltalk00 = loadAsset("/externalassets/classic/fishes/small/body_talk_00.webp", "Image")
			fish3x1cltalk01 = loadAsset("/externalassets/classic/fishes/small/body_talk_01.webp", "Image")
			fish3x1cltalk02 = loadAsset("/externalassets/classic/fishes/small/body_talk_02.webp", "Image")
			
			--talking contour
			fish4x2cltalk00co = loadAsset("/externalassets/classic/fishes/big/grid/body_talk_00.webp", "Image")
			fish3x1cltalk00co = loadAsset("/externalassets/classic/fishes/small/grid/body_talk_00.webp", "Image")
			
			--lips
			body_talk_base4x2 = loadAsset("/externalassets/classic/fishes/big/lips/base.webp", "Image")
			body_talk_base4x2a = loadAsset("/externalassets/classic/fishes/big/lips/a.webp", "Image")
			body_talk_base4x2b = loadAsset("/externalassets/classic/fishes/big/lips/b.webp", "Image")
			body_talk_base4x2c = loadAsset("/externalassets/classic/fishes/big/lips/c.webp", "Image")
			body_talk_base4x2d = loadAsset("/externalassets/classic/fishes/big/lips/d.webp", "Image")
			body_talk_base4x2e = loadAsset("/externalassets/classic/fishes/big/lips/e.webp", "Image")
			body_talk_base4x2f = loadAsset("/externalassets/classic/fishes/big/lips/f.webp", "Image")
			body_talk_base4x2g = loadAsset("/externalassets/classic/fishes/big/lips/g.webp", "Image")
			body_talk_base4x2h = loadAsset("/externalassets/classic/fishes/big/lips/h.webp", "Image")
			body_talk_base4x2x = loadAsset("/externalassets/classic/fishes/big/lips/x.webp", "Image")
			
			fish3x1base = loadAsset("/externalassets/classic/fishes/small/lips/base.webp", "Image")
			fish3x1a = loadAsset("/externalassets/classic/fishes/small/lips/a.webp", "Image")
			fish3x1b = loadAsset("/externalassets/classic/fishes/small/lips/b.webp", "Image")
			fish3x1c = loadAsset("/externalassets/classic/fishes/small/lips/c.webp", "Image")
			fish3x1d = loadAsset("/externalassets/classic/fishes/small/lips/d.webp", "Image")
			fish3x1e = loadAsset("/externalassets/classic/fishes/small/lips/e.webp", "Image")
			fish3x1f = loadAsset("/externalassets/classic/fishes/small/lips/f.webp", "Image")
			fish3x1g = loadAsset("/externalassets/classic/fishes/small/lips/g.webp", "Image")
			fish3x1h = loadAsset("/externalassets/classic/fishes/small/lips/h.webp", "Image")
			fish3x1x = loadAsset("/externalassets/classic/fishes/small/lips/x.webp", "Image")
			
		--elseif skinupscaled==true then
			fish4x2clup = loadAsset("/assets/players/classic/upscaled/fish-4x2.webp", "Image")
			fish3x1clup = loadAsset("/assets/players/classic/upscaled/fish-3x1.webp", "Image")
			fish1deadclup = loadAsset("externalassets/classic/fishes/upscaled/big/body_skeleton_00.webp", "Image")
			fish2deadclup = loadAsset("externalassets/classic/fishes/upscaled/small/body_skeleton_00.webp", "Image")
			
			--rest upscaled
			fish4x2cluprest00 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_rest_00.webp", "Image")
			fish4x2cluprest01 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_rest_01.webp", "Image")
			fish4x2cluprest02 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_rest_02.webp", "Image")
			
			fish3x1cluprest00 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_rest_00.webp", "Image")
			fish3x1cluprest01 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_rest_01.webp", "Image")
			fish3x1cluprest02 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_rest_02.webp", "Image")
			
			--rest upscaled contour
			fish4x2cluprest00co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_rest_00.webp", "Image")
			fish4x2cluprest01co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_rest_01.webp", "Image")
			fish4x2cluprest02co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_rest_02.webp", "Image")
			
			fish3x1cluprest00co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_rest_00.webp", "Image")
			fish3x1cluprest01co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_rest_01.webp", "Image")
			fish3x1cluprest02co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_rest_02.webp", "Image")
			
			--selection aura
			fish4x2clupaura = loadAsset("/externalassets/classic/fishes/upscaled/big/aura.webp", "Image")
			fish3x1clupaura = loadAsset("/externalassets/classic/fishes/upscaled/small/aura.webp", "Image")
			
			--swam upscaled
			fish4x2clupswam00 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_00.webp", "Image")
			fish4x2clupswam01 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_01.webp", "Image")
			fish4x2clupswam02 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_02.webp", "Image")
			fish4x2clupswam03 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_03.webp", "Image")
			fish4x2clupswam04 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_04.webp", "Image")
			fish4x2clupswam05 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_swam_05.webp", "Image")
			
			fish3x1clupswam00 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_00.webp", "Image")
			fish3x1clupswam01 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_01.webp", "Image")
			fish3x1clupswam02 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_02.webp", "Image")
			fish3x1clupswam03 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_03.webp", "Image")
			fish3x1clupswam04 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_04.webp", "Image")
			fish3x1clupswam05 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_swam_05.webp", "Image")
			
			--swam upscaled contour
			fish4x2clupswam00co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_00.webp", "Image")
			fish4x2clupswam01co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_01.webp", "Image")
			fish4x2clupswam02co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_02.webp", "Image")
			fish4x2clupswam03co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_03.webp", "Image")
			fish4x2clupswam04co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_04.webp", "Image")
			fish4x2clupswam05co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_swam_05.webp", "Image")
			
			fish3x1clupswam00co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_00.webp", "Image")
			fish3x1clupswam01co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_01.webp", "Image")
			fish3x1clupswam02co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_02.webp", "Image")
			fish3x1clupswam03co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_03.webp", "Image")
			fish3x1clupswam04co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_04.webp", "Image")
			fish3x1clupswam05co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_swam_05.webp", "Image")
			
			--pushing upscaled
			
			fish4x2cluppush00 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_push_00.webp", "Image")
			fish3x1cluppush00 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_push_00.webp", "Image")
			
			--pushing upscaled contour
			
			fish4x2cluppush00co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_push_00.webp", "Image")
			fish3x1cluppush00co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_push_00.webp", "Image")
			
			--talking upscaled
			
			fish4x2cluptalk00 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_talk_00.webp", "Image")
			fish4x2cluptalk01 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_talk_01.webp", "Image")
			fish4x2cluptalk02 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_talk_02.webp", "Image")
			
			fish3x1cluptalk00 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_talk_00.webp", "Image")
			fish3x1cluptalk01 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_talk_01.webp", "Image")
			fish3x1cluptalk02 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_talk_02.webp", "Image")
			
			--talking contour
			fish4x2cluptalk00co = loadAsset("/externalassets/classic/fishes/upscaled/big/grid/body_talk_00.webp", "Image")
			fish3x1cluptalk00co = loadAsset("/externalassets/classic/fishes/upscaled/small/grid/body_talk_00.webp", "Image")
			
			--lips upscaled
			body_talk_base4x2up = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/base.webp", "Image")
			body_talk_base4x2aup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/a.webp", "Image")
			body_talk_base4x2bup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/b.webp", "Image")
			body_talk_base4x2cup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/c.webp", "Image")
			body_talk_base4x2dup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/d.webp", "Image")
			body_talk_base4x2eup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/e.webp", "Image")
			body_talk_base4x2fup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/f.webp", "Image")
			body_talk_base4x2gup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/g.webp", "Image")
			body_talk_base4x2hup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/h.webp", "Image")
			body_talk_base4x2xup = loadAsset("/externalassets/classic/fishes/upscaled/big/lips/x.webp", "Image")
			
			body_talk_base3x1up = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/base.webp", "Image")
			body_talk_base3x1aup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/a.webp", "Image")
			body_talk_base3x1bup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/b.webp", "Image")
			body_talk_base3x1cup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/c.webp", "Image")
			body_talk_base3x1dup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/d.webp", "Image")
			body_talk_base3x1eup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/e.webp", "Image")
			body_talk_base3x1fup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/f.webp", "Image")
			body_talk_base3x1gup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/g.webp", "Image")
			body_talk_base3x1hup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/h.webp", "Image")
			body_talk_base3x1xup = loadAsset("/externalassets/classic/fishes/upscaled/small/lips/x.webp", "Image")
			
			
			--turning upscaled
			fish4x2clupturn00 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_00.webp", "Image")
			fish4x2clupturn01 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_01.webp", "Image")
			fish4x2clupturn02 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_02.webp", "Image")
			fish4x2clupturn02l = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_02l.webp", "Image")
			fish4x2clupturn03 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_03.webp", "Image")
			fish4x2clupturn04 = loadAsset("/externalassets/classic/fishes/upscaled/big/body_turn_04.webp", "Image")
			
			fish3x1clupturn00 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_00.webp", "Image")
			fish3x1clupturn01 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_01.webp", "Image")
			fish3x1clupturn02 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_02.webp", "Image")
			fish3x1clupturn02l = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_02l.webp", "Image")
			fish3x1clupturn03 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_03.webp", "Image")
			fish3x1clupturn04 = loadAsset("/externalassets/classic/fishes/upscaled/small/body_turn_04.webp", "Image")
			
		
		--end
	--end
	
	--[[
	This code is using the Love2D game engine's graphics module to load images from the specified file paths and create new image objects. The images are mostly icons for various interface elements such as touch controls, screen options, sound and music icons, and controller icons.

	The images are loaded in a sequence of lines using the love.graphics.newImage function along with WebP.loadImage to load the images from their file paths. The images are then stored in variables such as touchcontrols1, contcontrast, fishtouch1, xboxgamepad, etc. for later use in the game's interface.
	--]]



--touch controls / radial menu

    touchcontrols1= loadAsset("/assets/interface/touchcontroltrans.webp", "Image")
    contcontrast= loadAsset("/assets/interface/contcontrast.webp", "Image")
    fishtouch1= loadAsset("/assets/interface/fishtouch1.webp", "Image")
    fishtouch2= loadAsset("/assets/interface/fishtouch2.webp", "Image")
    fishtouchcontrast= loadAsset("/assets/interface/fishtouchcontrast.webp", "Image")
    levelcontrol1= loadAsset("/assets/interface/level1.webp", "Image")
    confcontrol1= loadAsset("/assets/interface/conftranswhite.webp", "Image")
    --leditoricon= loadAsset("/assets/interface/leveleditor.webp", "Image")
    touchicon= loadAsset("/assets/interface/touchicon.webp", "Image")
    netcontrols= loadAsset("/assets/interface/netcontrols.webp", "Image")
    
    loadicon= loadAsset("/assets/interface/load.webp", "Image")
    saveicon= loadAsset("/assets/interface/save.webp", "Image")
    restarticon= loadAsset("/assets/interface/restart.webp", "Image")
    
    --screen options
    
	screenoptionsicon= loadAsset("/assets/interface/screenoptionsicon.webp", "Image")
    helpicon= loadAsset("/assets/interface/helpicon.webp", "Image")
    paletteicon= loadAsset("/assets/interface/paletteicon.webp", "Image")
    crteffecticon= loadAsset("/assets/interface/crteffect.webp", "Image")
    shadericon= loadAsset("/assets/interface/shadericon.webp", "Image")
    threedicon= loadAsset("/assets/interface/3dicon.webp", "Image")
    dioramaicon= loadAsset("/assets/interface/dioramaicon.webp", "Image")
    screenresolutionicon= loadAsset("/assets/interface/screenresolutionicon.webp", "Image")
    closeicon= loadAsset("/assets/interface/closeiconwhite.webp", "Image")
    
    -- show hide panels
    
    showpanelright= loadAsset("/assets/interface/showpanelright.webp", "Image")
    hidepanelright= loadAsset("/assets/interface/hidepanelright.webp", "Image")
    panelup= loadAsset("/assets/interface/panelup.webp", "Image")
    paneldown= loadAsset("/assets/interface/paneldown.webp", "Image")
    
    -- sound icons
    
    soundonicon= loadAsset("/assets/interface/soundon.webp", "Image")
    soundoff= loadAsset("/assets/interface/soundoff.webp", "Image")
    
    --music icons
    
    musiconicon= loadAsset("/assets/interface/musicon.webp", "Image")
    musicoff= loadAsset("/assets/interface/musicoff.webp", "Image")
    
    --dialogs icons
    
    opendialogs= loadAsset("/assets/interface/opendialogs.webp", "Image")
    closedialogs= loadAsset("/assets/interface/closedialogs.webp", "Image")
    readdialog= loadAsset("/assets/interface/readdialog.webp", "Image")
    
    --controller icons
    
    
    
    --flags
    bg= loadAsset("/assets/flags/bg.webp", "Image")
    br= loadAsset("/assets/flags/br.webp", "Image")
    ch= loadAsset("/assets/flags/ch.webp", "Image")
    cs= loadAsset("/assets/flags/cs.webp", "Image")
    de= loadAsset("/assets/flags/de.webp", "Image")
    eo= loadAsset("/assets/flags/eo.webp", "Image")
    es= loadAsset("/assets/flags/es.webp", "Image")
    fr= loadAsset("/assets/flags/fr.webp", "Image")
    gb= loadAsset("/assets/flags/gb.webp", "Image")
    it= loadAsset("/assets/flags/it.webp", "Image")
    nl= loadAsset("/assets/flags/nl.webp", "Image")
    pl= loadAsset("/assets/flags/pl.webp", "Image")
    pt= loadAsset("/assets/flags/pt.webp", "Image")
    ru= loadAsset("/assets/flags/ru.webp", "Image")
    se= loadAsset("/assets/flags/se.webp", "Image")
    sk= loadAsset("/assets/flags/sk.webp", "Image")



--[[
This Lua function loads ultra high-resolution assets for a game. The assets are images used to render the game's fish characters, specifically their bodies and lips.

The function starts by checking which "skin" is being used for the game - either "remake" or "classic." Depending on the skin, different image files are loaded for each of the fish characters, which come in two different sizes.

For each character and animation type, three images are loaded, with filenames containing suffixes "00," "01," and "02." These are loaded using the Love2D game engine's love.graphics.newImage function, which returns a new image object based on the provided file data. The file data is loaded using the love.filesystem.newFileData function, which reads a file's contents into memory.

The image files are in the WebP image format, which is a compressed image format that is efficient for storing high-resolution images. The WebP.loadImage function is used to load each image file into memory, which returns an image object that can be used for rendering.

For the lips of the fish characters, six different images are loaded - one for the base image and five for different mouth positions. Each image is loaded and stored in a new image object.

Overall, this function loads all of the ultra high-resolution images needed to render the fish characters in the game, with different image files being loaded based on the selected skin.
--]]
function loadultraassets()
	--ultra
			
		--[[	--rest ultra
			fish4x2clulrest00 = loadAsset("/externalassets/classic/fishes/ultra/body_rest_00.webp", "Image")
			fish4x2clulrest01 = loadAsset("/externalassets/classic/fishes/ultra/body_rest_01.webp", "Image")
			fish4x2clulrest02 = loadAsset("/externalassets/classic/fishes/ultra/body_rest_02.webp", "Image")
			
			fish3x1clulrest00 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_rest_00.webp", "Image")
			fish3x1clulrest01 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_rest_01.webp", "Image")
			fish3x1clulrest02 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_rest_02.webp", "Image")
			--]]
			--talking ultra
			
			if skin=="remake" then
				fish4x2clultalk00 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_00_remake.webp", "Image")
				fish4x2clultalk01 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_00_remake.webp", "Image")
				fish4x2clultalk02 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_00_remake.webp", "Image")
				
				fish3x1clultalk00 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_00_remake.webp", "Image")
				fish3x1clultalk01 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_00_remake.webp", "Image")
				fish3x1clultalk02 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_00_remake.webp", "Image")
			
			elseif skin=="classic" then
			
				fish4x2clultalk00 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_00.webp", "Image")
				fish4x2clultalk01 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_01.webp", "Image")
				fish4x2clultalk02 = loadAsset("/externalassets/classic/fishes/ultra/body_talk_02.webp", "Image")
				
				fish3x1clultalk00 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_00.webp", "Image")
				fish3x1clultalk01 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_01.webp", "Image")
				fish3x1clultalk02 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_talk_02.webp", "Image")
			end
			
			--lips ultra
			body_talk_base4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/base.webp", "Image")
			body_talka4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/a.webp", "Image")
			body_talkb4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/b.webp", "Image")
			body_talkc4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/c.webp", "Image")
			body_talkd4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/d.webp", "Image")
			body_talke4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/e.webp", "Image")
			body_talkf4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/f.webp", "Image")
			body_talkg4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/g.webp", "Image")
			body_talkh4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/h.webp", "Image")
			body_talkx4x2ul = loadAsset("/externalassets/classic/fishes/ultra/lips/x.webp", "Image")
			
			body_talk_base3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/base.webp", "Image")
			body_talka3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/a.webp", "Image")
			body_talkb3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/b.webp", "Image")
			body_talkc3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/c.webp", "Image")
			body_talkd3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/d.webp", "Image")
			body_talke3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/e.webp", "Image")
			body_talkf3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/f.webp", "Image")
			body_talkg3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/g.webp", "Image")
			body_talkh3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/h.webp", "Image")
			body_talkx3x1ul = loadAsset("/externalassets/classic/fishes/ultra/fish2/lips/x.webp", "Image")
			
			
			--[[
			--turning ultra
			fish4x2clulturn00 = loadAsset("/externalassets/classic/fishes/ultra/body_turn_00.webp", "Image")
			fish4x2clulturn01 = loadAsset("/externalassets/classic/fishes/ultra/body_turn_01.webp", "Image")
			fish4x2clulturn02 = loadAsset("/externalassets/classic/fishes/ultra/body_turn_02.webp", "Image")
			fish4x2clulturn03 = loadAsset("/externalassets/classic/fishes/ultra/body_turn_03.webp", "Image")
			
			
			fish3x1clulturn00 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_turn_00.webp", "Image")
			fish3x1cluturn01 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_turn_01.webp", "Image")
			fish3x1clulpturn02 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_turn_02.webp", "Image")
			fish3x1clulturn03 = loadAsset("/externalassets/classic/fishes/ultra/fish2/body_turn_03.webp", "Image")
			--]]
		
end
--[[
The loadultrastatue() function loads 19 different image files of an "ultra statue" into memory using the Love2D game engine's love.graphics.newImage() function. The images are loaded from WebP format files located in specific file paths within the game's file system using love.filesystem.newFileData(). The loaded images are assigned to variables named statue1 through statue19.
--]]
function loadultrastatue()
			
			statue1 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/1.webp", "Image")
			statue2 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/2.webp", "Image")
			statue3 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/3.webp", "Image")
			statue4 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/4.webp", "Image")
			statue5 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/5.webp", "Image")
			statue6 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/6.webp", "Image")
			statue7 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/7.webp", "Image")
			statue8 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/8.webp", "Image")
			statue9 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/9.webp", "Image")
			statue10 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/10.webp", "Image")
			statue11 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/11.webp", "Image")
			statue12 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/12.webp", "Image")
			statue13 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/13.webp", "Image")
			statue14 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/14.webp", "Image")
			statue15 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/15.webp", "Image")
			statue16 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/16.webp", "Image")
			statue17 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/17.webp", "Image")
			statue18 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/18.webp", "Image")
			statue19 = loadAsset("/externalassets/classic/level21/objects/statue/ultra/19.webp", "Image")
end

--The loadultralinuxusers() function loads a number of different images of a character named "linuxak" into memory using the love.graphics.newImage() function. The images are loaded from WebP format files located in specific file paths within the game's file system using love.filesystem.newFileData(). The specific images loaded depend on the value of the skin variable (which is not defined in this code snippet), and are assigned to variables with names such as linuxak1_00 and linuxak2_02.
function loadultralinuxusers()

			if skin=="classic" then
				linuxak1_00 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_01 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_01.webp", "Image")
				linuxak1_02 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_02.webp", "Image")
				linuxak1_03 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_03.webp", "Image")
				linuxak1_04 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_04.webp", "Image")
				linuxak1_05 = loadAsset("/externalassets/classic/level77/ultra/linuxak1_05.webp", "Image")
				
				linuxak2_00 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_01 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_01.webp", "Image")
				linuxak2_02 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_02.webp", "Image")
				linuxak2_03 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_03.webp", "Image")
				linuxak2_04 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_04.webp", "Image")
				linuxak2_05 = loadAsset("/externalassets/classic/level77/ultra/linuxak2_05.webp", "Image")
		elseif skin=="remake" then
				linuxak1_00 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_01 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_02 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_03 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_04 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				linuxak1_05 = loadAsset("/externalassets/objects/level77/ultra/linuxak1_00.webp", "Image")
				
				linuxak2_00 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_01 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_02 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_03 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_04 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
				linuxak2_05 = loadAsset("/externalassets/objects/level77/ultra/linuxak2_00.webp", "Image")
		end
end



