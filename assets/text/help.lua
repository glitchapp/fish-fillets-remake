help = [[
Controls:
Move: Arrows
Switch character: Space

backspace: restart level
f1: help
f2: load game
f3: save game
f4: music on/off
f5: sound on/off
f6: show/hide subtitles
f7: Caustics effects on / off
f8: CRT effect on / off
f9: switch remade / classic graphics
f10: game menu
f11: switch resolution (1080p / 720p)
f12: switch to retro mode / color schemes
1: Select fish 1
2: Select fish 2
3: 3d engine
4: diorama
5: touch controls
o: options menu
p: Pause
g: Guide
escape: quit

Debug keys:
6: debug mode
7: background on / off
8: objects on / off
9: players on / off
]]

helpright= [[
1: Select fish 1
2: Select fish 2
3: 3d engine
4: diorama
5: touch controls
o: options menu
p: Pause
g: Guide
escape: quit

Debug keys:
6: debug mode
7: background on / off
8: objects on / off
9: players on / off

mouse wheel: Zoom in / out
middle mouse button: Reset zoom
]]

effectshelp=[[
]]


