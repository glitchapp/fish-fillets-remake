zonetext={"Fiskehus",				--1
"Skibsvrak",						--2
"Silvers skip",					--3
"Byen i Dybet",					--4
"UFO",								--5
"Korallrev",						--6
"Skrin av skatter",					--7
"Dump",								--8
"Hemmelig datamaskin",					--9
"NG",								--1
"Gren av den nye generasjonen",		--1
"Test",
}
fishhouse0={"Hvordan alt begynte",	--1
"Melding i attachemappen",				--2
"Øvelse i kjelleren",				--3
"Flytende bibliotek",					--4
"Planter på trappene",				--5
"Uorden i kjelerommet",		--6
"Under korallrevene",					--7
"Låst inne i skapet",				--8
}

shipwrecks1={"Nedsenket ubåt",	--9
"Picnicbåt",						--10
"Stor krig",						--11
"Kaptein Silvers skip",		--12
"Den siste reisen",					--13
"Høyde: Minus 9000 fot",		--14
"Bathyskaph",						--15
"Amfibisk tank",					--16
"Åtte vikinger i en båt",			--17
"Returnerer fra festen",			--18
" Gudene må være gale",			--19
}
silversship2={"Førstestyrmannens lugar",--45
"Vintermesse",				--46
"Ild!",							--47
"Skipets kjøkken",						--48
"Andrestyrmannens lugar",				--49
"Kapteinens lugar",					--50
"Silvers skjulested",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"Hus med heis",			--20
"Velkommen til vår by",				--21
"Uavhengighetsdag",					--22
"Søylene",						--23
"Ujevn fortau",					--24
"Mr. Cheops hus",				--25
"Litt musikk",					--26
"Krabbe-freakshow",					--27
"Enda en heis",					--28
"Og hvordan det var",					--29
"",
"",
"",
"",
"",
}

ufo4={"Kraftverk",				--52
"Rare krefter",					--53
"Brm... Brm...",					--54
"Intet annet enn stål",				--55
"Voktet korridor",					--56
"Biologiske eksperimenter",			--57
"Den virkelige fremdriften",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"De første merkelige tingene",	--30
"Labyrint",						--31
"Fengslet",						--32
"Lukket samfunn",					--33
"Sovende skapninger",				

	--34
"Cancan-krabber",						--35
"Enda en perle, takk!",			--36
"Telepatisk djevel",					--37
}

treasurecave6={"Aztek kunstsal",	--59
"Skinnende huleinnstyrtningssted",					--60
"Kjempestort bryst",					--61
"Ali-Babas hall",				--62
"Den dypeste hulen",					--63
"Hva ville kong Arthur si?",		--64
"",}

dump7={"Den dype serveren",			--38
"Nesten ingen vegg",					--39
"Rørleggerens motbevis",				--40
"Eventyr med rosa and",		--41
"Revnet strekmann",				--42
"Virkelig kaos",						--43
"Raste Greenpeace",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulator",							--66
"Krigens hage",					--67
"Favoritter",						--68
"Et hardwareproblem",				--69
"Kun lese",						--70
}

secret9={"Elektromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hull",								--74
"nøkkel",								--75
"nøkler",								--76
"linux",							--77
"rotere",							--78
"skynde",								--79
}

test10={"test",						--101
"test2",							--102
"test3",							--103
"test4",							--104
"test5",							--105
"test6",							--106
"test7",							--107
"test8",							--108
}
