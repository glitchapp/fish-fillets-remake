zonetext = {
  "Kalatalo",								--1
  "Haaksirikot",							--2
  "Silverin laiva",							--3
  "Kaupunki syvyyksissä",					--4
  "UFO",									--5
  "Koralliriutta",							--6
  "Aarrekätkö",								--7
  "Jätealue",								--8
  "Salainen tietokone",						--9
  "NG",										--1
  "Uuden sukupolven osasto",					--1
  "Testi",
}

fishhouse0 = {
  "Kuinka kaikki alkoi",					--1
  "Käsilaukun viesti",						--2
  "Harjoitus kellariin",					--3
  "Kirjastossa kelluvaa tavaraa",			--4
  "Kasveja portaita pitkin",				--5
  "Sotku kattilahuoneessa",					--6
  "Riutan alla",							--7
  "Suljettu komerossa",						--8
}

shipwrecks1 = {
  "Uponnut sukellusvene",					--9
  "Piknik-vene",							--10
  "Suuri sota",								--11
  "Kapteeni Silverin laiva",				--12
  "Viimeinen matka",						--13
  "Korkeus: Miinus 9000 jalkaa",			--14
  "Bathyscaph",								--15
  "Amfibiotankki",							--16
  "Kahdeksan viikinkiä veneessä",			--17
  "Paluu juhlista",							--18
  "Jumalat ovat hulluja",					--19
}

silversship2 = {
  "Perämiehen hytti",						--45
  "Talvisetti ruokasali",					--46
  "Tuli!",									--47
  "Laivan keittiö",							--48
  "Toisen perämiehen hytti",				--49
  "Kapteenin hytti",						--50
  "Silverin piilopaikka",					--51
}

cityinthedeep3 = {
  "Talo hissillä",							--20
  "Tervetuloa kaupunkiimme",				--21
  "Itsenäisyyspäivä",						--22
  "Pylväät",								--23
  "Epätasainen jalkakäytävä",				--24
  "Herra Cheopin talo",						--25
  "Pala musiikkia",							--26
  "Rapujen näytös",							--27
  "Toinen hissi",							--28
  "Ja kuinka se tapahtui",					--29
}

ufo4 = {
  "Voimalaitos",							--52
  "Kummalliset voimat",						--53
  "Brm... Brm...",							--54


  "Vain terästä",							--55
  "Vartioitu käytävä",						--56
  "Biologiset kokeet",						--57
  "Oikea voimanlähde",						--58
}

coralreef5 = {
  "Ensimmäiset oudot asiat",				--30
  "Labyrintti",								--31
  "Vankina",								--32
  "Suljettu yhteiskunta",					--33
  "Nukkuvat olennot",						--34
  "Kävelevät ravut",						--35
  "Yksi helmi lisää, kiitos!",				--36
  "Telepaattinen paholainen",				--37
}

treasurecave6 = {
  "Atsteekkien taidetila",					--59
  "Kiiltävä luolavajoama",					--60
  "Jättiläisen aarrearkku",					--61
  "Ali-Baban halli",						--62
  "Syvin luola",							--63
  "Mitä kuningas Arthur sanoisi?",			--64
}

dump7 = {
  "Syvä palvelin",							--38
  "Melkein olematon seinä",					--39
  "Putkimiehen kiistäminen",				--40
  "Seikkailu vaaleanpunaisen ankkan kanssa",	--41
  "Revitty tikkuhahmo",						--42
  "Oikea kaaos",							--43
  "Järkyttynyt Greenpeace",					--44
}

secretcomputer8 = {
  "Tetris",									--65
  "Emulaattori",							--66
  "Sodan puutarha",							--67
  "Suosikit",								--68
  "Laiteongelma",							--69
  "Vain luku",								--70
}

secret9 = {
  "Sähkömagneetti",							--71
  "fdto",									--72
  "hanoi",									--73
  "reikä",									--74
  "avain",									--75
  "avaimet",								--76
  "linux",									--77
  "kierto",									--78
  "kiire",									--79
}

test10 = {
  "testi",									--101
  "testi2",									--102
  "testi3",									--103
  "testi4",									--104
  "testi5",									--105
  "testi6",									--106
  "testi7",									--107
  "testi8",									--108
}
