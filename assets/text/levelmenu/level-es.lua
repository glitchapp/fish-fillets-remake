zonetext={"Casa Pez",
"Ruinas del Barco",
"El Barco de Silver",
"Ciudad en lo Profundo",
"OVNI",
"Arrecife de Coral",
"Caverna del Tesoro",
"Vertedero",
"Computador Secreto",
"NG",
"",
}

fishhouse0={"Como Empezó Todo",	--1
"Mensaje de un Maletín",				--2
"Ensayo en el Sótano",				--3
"La Biblioteca",					--4
"Plantas en las Escaleras",				--5
"Un Desorden en la Caldera",		--6
"Bajo el Arrecife",					--7
"Encerrado en el W.C.",				--8
}

shipwrecks1={"Submarino Ahogado",	--9
"Bote Picnic",						--10
"Gran Guerra",						--11
"El Barco del Capitán Silver",		--12
"El Ultimo Viaje",					--13
"Altitud: Bajo 3000 Metros",		--14
"Batiscafo",						--15
"Tanque Anfibio",					--16
"Ocho Vikingos en un Bote",			--17
"Regreso de la Fiesta",			--18
"Los Dioses Deben de Estar Locos",	--19
}
silversship2={"La Cabina del Primer Oficial",--45
"El Lío en el Salón de Invierno",				--46
"Fuego!",							--47
"Cocina del Barco",						--48
"La Cabina del Segundo Oficial",				--49
"La Cabina del Capitán",					--50
"El Escondite de Silver",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"Casa con Ascensor",			--20
"Bienvenido a nuestra Ciudad",				--21
"Día de la Independencia",					--22
"Las Columnas",						--23
"Pavimento Irregular",					--24
"La Casa de Mr. Cheops",				--25
"Un Poco de Música",					--26
"Loco Show del Cangrejo",					--27
"Otro Ascensor",					--28
"Y Cómo Era",					--29
"",
"",
"",
"",
"",
}

ufo4={"Central Eléctrica",				--52
"Fuerzas Extrañas",					--53
"Brum... Brum...",					--54
"Nada mas que Acero",				--55
"Pasillo Resguardado",					--56
"Experimentos Biológicos",			--57
"La Propulsión Real",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"Primeras cosas Bizarras",	--30
"Laberinto",						--31
"En Prisión",						--32
"Sociedad Cerrada",					--33
"Criaturas Durmientes",				--34
"Cangrejos Cancan",						--35
"Una Perla Más; ¡Por Favor!",			--36
"Diablo Telepático",					--37
}

treasurecave6={"Salón de Arte Azteca",	--59
"Gruta Luminosa",					--60
"El Pecho del Gigante",					--61
"El Salón de Ali Baba",				--62
"La Caverna mas Profunda",					--63
"¿Qué Diría el Rey Arturo?",		--64
"",}

dump7={"El Servidor en lo Profundo",			--38
"Casi Ninguna Muralla",					--39
"El Rechazo del Plomero",				--40
"Aventura con el Patito Rosado",		--41
"Dispersión",				--42
"Caos Real",						--43
"Greenpeace Ultrajado",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulador",							--66
"Jardín de Guerra",					--67
"Favoritos",						--68
"Un Problema de Hardware",				--69
"Sólo Lectura",						--70
}

secret9={"Electromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hole",								--74
"key",								--75
"keys",								--76
"linux",							--77
"rotate",							--78
"rush",								--79
}
