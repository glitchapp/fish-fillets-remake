zonetext={"Fischhaus",
"Scheepswrakken",
"Silvers Schiff",
"Stadt in der Tiefe",
"UFO",
"Korallenriff",
"Schatzhöhle",
"Halde",
"Geheimer Rechner",
"NG",
"Zweig der neuen Generation",
}
fishhouse0={"Wie alles anfing",	--1
"Die Nachricht in der Aktentasche",				--2
"Die Probe im Keller",				--3
"Bibliothek Strandgut",					--4
"Pflanzen auf der Treppe",				--5
"Unordnung im Kesselraum",		--6
"Unter dem Riff",					--7
"Eingeschlossen auf Toilette",				--8
}

shipwrecks1={"Versunkenes U-Boot",	--9
"Picknickboot",						--10
"Erster Weltkrieg",						--11
"Das Schiff von Kapitän Silver",		--12
"Die letzte Reise",					--13
"Höhe: Minus 3000 Meter",		--14
"Bathyscaph",						--15
"Schwimmpanzer",					--16
"Acht Wikinger in einem Boot",			--17
"Rückkehr von der Feier",			--18
"Die Götter müssen verrückt sein",			--19
}
silversship2={"The First Mate's Cabin",--45
"The Winter Mess Hall",				--46
"Fire!",							--47
"Ship Kitchen",						--48
"Second Mate's Cabin",				--49
"Captain's Cabin",					--50
"Silver's Hideout",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"Haus mit einem Fahrstuhl",			--20
"Willkommen in unserer Stadt",				--21
"Unabhängigkeitstag",					--22
"Die Säulen",						--23
"Unebener Bürgersteig",					--24
"Das Haus des Herrn Cheops",				--25
"Ein bisschen Musik",					--26
"Krabben-Monster-Schau",					--27
"Noch ein Fahrstuhl",					--28
"Wie es war",					--29
"",
"",
"",
"",
"",
}

ufo4={"Power Plant",				--52
"Strange Forces",					--53
"Brm... Brm...",					--54
"Nothing But Steel",				--55
"Guarded Corridor",					--56
"Biological Experiments",			--57
"The Real Propulsion",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"First Bizarre Things",	--30
"Labyrinth",						--31
"Imprisoned",						--32
"Closed Society",					--33
"Sleeping Creatures",				--34
"Cancan Crabs",						--35
" One More Pearl; Please!",			--36
"Telepathic Devil",					--37
}

treasurecave6={"Aztec Art Hall",	--59
"Shiny Cave-In",					--60
"Giant's Chest",					--61
"The Hall of Ali-Baba",				--62
"The Deepest Cave",					--63
"What Would King Arthur Say?",		--64
"",}

dump7={"The Deep Server",			--38
"Almost No Wall",					--39
"Plumbman's Refute",				--40
"Adventure With Pink Duckie",		--41
"Shredded Stickman",				--42
"Real Chaos",						--43
"Outraged Greenpeace",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulator",							--66
"Garden of War",					--67
"Favorites",						--68
"A hardware problem",				--69
"Read only",						--70
}

secret9={"Electromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hole",								--74
"key",								--75
"keys",								--76
"linux",							--77
"rotate",							--78
"rush",								--79
}
