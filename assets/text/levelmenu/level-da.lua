zonetext={"Fiskehus",				--1
"Skibsvrag",						--2
"Silvers Skib",					--3
"Byen i Dybet",					--4
"UFO",								--5
"Koralrev",						--6
"Schatzhule",					--7
"Dump",								--8
"Hemmelig Computer",					--9
"NG",								--1
"Grener af den Nye Generation",		--1
"Test",
}
fishhouse0={"Hvordan det hele startede",	--1
"Besked i attachemappen",				--2
"Rehearsal i kælderen",				--3
"Flydende bibliotek",					--4
"Planter på trapperne",				--5
"Uorden i kedelrummet",		--6
"Under koralrevene",					--7
"Lukket inde i skabet",				--8
}

shipwrecks1={"Nedsænket ubåd",	--9
"Picnic-båd",						--10
"Stor krig",						--11
"Kaptajn Silvers skib",		--12
"Den sidste rejse",					--13
"Højde: Minus 9000 fod",		--14
"Bathyscaph",						--15
"Amfibisk tank",					--16
"Otte vikinger i en båd",			--17
"Tilbage fra festen",			--18
" De guder må være tossede",			--19
}
silversship2={"Førstestyrmannens kahyt",--45
"Vintermessen",				--46
"Ild!",							--47
"Skibskøkken",						--48
"Anden styrmands kahyt",				--49
"Kaptajnens kahyt",					--50
"Silvers skjulested",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"Hus med elevator",			--20
"Velkommen til vores by",				--21
"Uafhængighedsdag",					--22
"Søjlerne",						--23
"Ujævn fortov",					--24
"Mr. Cheops hus",				--25
"Lidt musik",					--26
"Krabbefreakshow",					--27
"Endnu en elevator",					--28
"Og hvordan det var",					--29
"",
"",
"",
"",
"",
}

ufo4={"Kraftværk",				--52
"Mærkelige kræfter",					--53
"Brm... Brm...",					--54
"Intet andet end stål",				--55
"Bevogtet korridor",					--56
"Biologiske eksperimenter",			--57
"Den virkelige fremdrift",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"De første bizarre ting",	--30
"Labyrint",						--31
"Fængslet",						--32
"Lukket samfund",					--33
"Sovende væsener",				--34
"Cancan-krabber",						--35
"Endnu en perle, tak!",			--36
"Telepatisk djævel",					--37
}

treasurecave6={"Aztec kunstsal",	--59
"Skinnende huleindstyrtningssted",					--60
"Kæmpens bryst",					--61
"Ali-Babas hall",				--62
"Den dybeste hule",					--63
"Hvad ville kong Arthur sige?",		--64
"",}

dump7={"Den dybe server",			--38
"Næsten ingen væg",					--39
"VVS-mands modbevise",				--40
"Eventyr med pink and",		--41
"Strimlet stokfigur",				--42
"Rigtig kaos",						--43
"Raserede Greenpeace",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulator",							--66
"Krigens have",					--67
"Favoritter",						--68
"Et hardwareproblem",				--69
"Kun læsning",						--70
}

secret9={"Elektromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hul",								--74
"nøgle",								--75
"nøgler",								--76
"linux",							--77
"roter",							--78
"rush",								--79
}

test10={"test",						--101
"test2",							--102
"test3",							--103
"test4",							--104
"test5",							--105
"test6",							--106
"test7",							--107
"test8",							--108
}
