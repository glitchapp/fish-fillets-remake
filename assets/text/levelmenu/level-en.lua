zonetext={"Fish House",				--1
"Ship Wrecks",						--2
"Silver's Ship",					--3
"City In the Deep",					--4
"UFO",								--5
"Coral reef",						--6
"Treasure Cave",					--7
"Dump",								--8
"Secret Computer",					--9
"NG",								--1
"Branch of the New Generation",		--1
"Test",
}
fishhouse0={"How It All Started",	--1
"Briefcase Message",				--2
"Rehearsal in Cellar",				--3
"Library Flotsam",					--4
"Plants on the Stairs",				--5
"A Mess in the Boiler Room",		--6
"Under the Reef",					--7
"Closed in the Closet",				--8
}

shipwrecks1={"Drowned Submarine",	--9
"Picnic Boat",						--10
"Great War",						--11
"The Ship of Captain Silver",		--12
"The Last Voyage",					--13
"Altitude: Minus 9000 Feet",		--14
"Bathyscaph",						--15
"Amphibious Tank",					--16
"Eight Vikings in a Boat",			--17
"Return from the Party",			--18
" The Gods Must Be Mad",			--19
}
silversship2={"The First Mate's Cabin",--45
"The Winter Mess Hall",				--46
"Fire!",							--47
"Ship Kitchen",						--48
"Second Mate's Cabin",				--49
"Captain's Cabin",					--50
"Silver's Hideout",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"House With an Elevator",			--20
"Welcome to Our City",				--21
"Independence Day",					--22
"The Columns",						--23
"Uneven Pavement",					--24
"Mr. Cheop's House",				--25
"A Bit of Music",					--26
"Crab Freak Show",					--27
"Another Elevator",					--28
"And How It Was",					--29
"",
"",
"",
"",
"",
}

ufo4={"Power Plant",				--52
"Strange Forces",					--53
"Brm... Brm...",					--54
"Nothing But Steel",				--55
"Guarded Corridor",					--56
"Biological Experiments",			--57
"The Real Propulsion",				--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"First Bizarre Things",	--30
"Labyrinth",						--31
"Imprisoned",						--32
"Closed Society",					--33
"Sleeping Creatures",				--34
"Cancan Crabs",						--35
" One More Pearl; Please!",			--36
"Telepathic Devil",					--37
}

treasurecave6={"Aztec Art Hall",	--59
"Shiny Cave-In",					--60
"Giant's Chest",					--61
"The Hall of Ali-Baba",				--62
"The Deepest Cave",					--63
"What Would King Arthur Say?",		--64
"",}

dump7={"The Deep Server",			--38
"Almost No Wall",					--39
"Plumbman's Refute",				--40
"Adventure With Pink Duckie",		--41
"Shredded Stickman",				--42
"Real Chaos",						--43
"Outraged Greenpeace",				--44
"",
"",
"",
}

secretcomputer8={"Tetris",			--65
"Emulator",							--66
"Garden of War",					--67
"Favorites",						--68
"A hardware problem",				--69
"Read only",						--70
}

secret9={"Electromagnet",			--71
"fdto",								--72
"hanoi",							--73
"hole",								--74
"key",								--75
"keys",								--76
"linux",							--77
"rotate",							--78
"rush",								--79
}

test10={"test",						--101
"test2",							--102
"test3",							--103
"test4",							--104
"test5",							--105
"test6",							--106
"test7",							--107
"test8",							--108
}
