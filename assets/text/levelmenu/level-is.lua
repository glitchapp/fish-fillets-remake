zonetext={"Fiskahús",						--1
"Höfrungavröð",						--2
"Skipaleifar",						--3
"Borgin í djúpinu",					--4
"Flaugaland",						--5
"Korallrif",						--6
"Fjöruhelli",						--7
"Úrgangsdepla",						--8
"Leynileg tölvu",					--9
"N.G.",								--1
"Grein af Nýju kynslóðinni",			--1
"Prófun",
}
fishhouse0={"Hvernig þetta byrjaði",	--1
"Skjótbútaskilaboð",				--2
"Æfing í kjallara",					--3
"Bókasafnsvrak",					--4
"Plöntur á stigunum",				--5
"Rúsin í hitaveitunni",				--6
"Undir korallrifinu",				--7
"Lokað í skápnum",					--8
}

shipwrecks1={"Sökknuð undirfarþotufar",	--9
"Piknikbáturinn",					--10
"Mikla stríðið",					--11
"Kapteins Silfursskipið",			--12
"Seinasta siglingin",				--13
"Altúíþhættir: -9000 fætur",		--14
"Þaðbútur",							--15
"Land- og sjófaratankur",			--16
"Átta víkingar í báti",				--17
"Heimkomu frá veislu",				--18
"Gudirnir verða heimafullir",		--19
}
silversship2={"Fyrsta ráðsmannaherbergið",--45
"Veislaíþróttahöllin",				--46
"Eldur!",							--47
"Skipstrússinn",					--48
"Annars ráðsmanns herbergið",		--49
"Kapteins herbergið",				--50
"Silfurstóllinn",					--51
"",
"",
"",
"",
"",
}

cityinthedeep3={
"Hús með lyftu",					--20
"Velkomin í borgina okkar",			--21
"Dagur sjálfstæðis",				--22
"Dálkar",							--23
"Ójöfn gata",						--24
"Húsið hjá Herra Cheop",			--25
"Smá tónlist",						--26
"Krabbafríkótínan",					--27
"Aðra lyftu",						--28
"Og hvernig það var",				--29
"",
"",
"",
"",
"",
}

ufo4={"Orkuver",						--52
"Fyndnar kraftar",					--53
"Brm... Brm...",					--54
"Ekkert annað en stál",				--55
"Hernum verndaður gangur",			--56
"Líffræðilegar tilraunir",			--57
"Raunverulegur ferðamótor",			--58
"",
"",
"",
"",
"",
"",
}

coralreef5={"Fyrstu undarlegu hlutirnir",--30
"Þokuhellir",						--31
"Lokuð samfélög",					--32
"Syfja skapninga",					--33
"Kanskanadökkur",					--34
"Cancan-krabbar",					--35
"Aðeins enn ein perluminni, takk!",	--36
"Fjarlægðartengdur djöfull",			--37
}

treasurecave6={"Azteklistasalur",		--59
"Mjög bráðna helli",				--60
"Risaherbergið",					--61
"Alla-Baba salurinn",				--62
"Djúpastan hella",					--63
"Hvað myndi konungur Arthúr segja?",	--64
"",}

dump7={"Djúpir forritastöðvar",		--38
"Nánast engin veggur",				--39
"Rör Plumbmans",					--40
"Ævintýri með bleikan önd",			--41
"Klikk-klikk-sellulítið",			--42
"Eiginleglegu kaos",				--43
"Hvatvíðingur umhverfissamtaka",		--44
"",
"",
"",
}

secretcomputer8={"Tetris",				--65
"Eftirlíking",						--66
"Stríðsgarður",					--67
"Uppáhalds síður",					--68
"Vandamál með vélina",				--69
"Aðeins til að lesa",				--70
}

secret9={"Rafmagnsmagneti",			--71
"fdto",								--72
"hanoi",							--73
"hol",								--74
"lykill",							--75
"lyklar",							--76
"linux",							--77
"snúa",								--78
"hraða",							--79
}

test10={"prófun",					--101
"prófun2",							--102
"prófun3",							--103
"prófun4",							--104
"prófun5",							--105
"prófun6",							--106
"prófun7",							--107
"prófun8",							--108
}
