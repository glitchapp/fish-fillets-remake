--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This code block sets up the configuration parameters for the game. These parameters can be modified to adjust the game settings.

The parameters are divided into several categories such as voice dubs, languages, graphics, shaders, audio, and controls.

Some of the configurable options include whether voice dubs are enabled or not, the language used for text and voice, the graphic mode selected, resolution, whether full-screen mode is enabled, whether shaders and chromatic aberration effects are enabled, and whether the game will have sound effects and music.

There are also control options such as touch interface, vibration, and two-player mode. The parameters can be changed by modifying the variables below the comments.
--]]

-- To set up your configuration parameters permanently, modify the variables below
     
     	--Voice dubs
    talkies=true				-- Set this to false if you want to disable the voice dubs
    
	-- languages
	language="en"
    language2="en"
    accent="us"
    accent2="us"
	success = love.filesystem.getInfo("language.txt")		-- check if language.txt exist
	if success then autoload=true else autoload=false end	-- if exist then autoload
	--autoload=false
	
    --Graphics
    
	selectedTile=nil			-- This option changes the tiles in the retro mode and level editor
    threeD=false				-- 3d Engine
    skin="remake"				-- Select the graphic mode here
								-- Options: "remake, "retro" & "classic"
    skinupscaled=true			-- This option switch between normal and upscaled graphics (classic mode)
    hdr=false					-- This option is not working yet
    res="1440p"					-- Select resolution: "720p",1080p","1440p","4k" & "8k"
    fullscreen=false			-- Select full screen
    vsyncstatus=false			-- Vsync
    limitframerate=nil			-- default limited framerate (nil = unlimited)
    
    
    --HUB
	showfps=false				-- show FPS
	showbattery=false			-- show battery
	showtimedate=false			-- Show data / time
    
	--shaders
	--caustics=true
	shader1=false				-- 	Caustic effects
    shader2=true				--	2nd shader
    shader1type="crt"			--	Options: false, "crt", "godsgrid" ,"gameboy"
    
    adjustr=1					-- RGB color adjustements
    adjustg=1
    adjustb=1
    
    chromaab=false				-- Chromatic aberration effect

	--Audio (sound and music)
    soundon=true				--	Audio effects
    musicison=true				--	Music
    musictype="new"				--	Music type: "new" or "classic"


    --controls
    touchinterfaceison=false	-- Set this to true to play on touch devices
    vibration=true				-- Set this to false if you want to disable vibration
	networkcontrol=false		-- Network controls
	radialmenu=false
	controllerLayout="default"
	--twoplayer=true				-- Set this to true for 2 player mode
	
     
--[[
settings = binser.serialize {
			-- graphics
				{threeD, skin, skinupscaled, hdr, res, fullscreen},
			-- voice dubs
				{talkies},
			-- languages
				{language, language2, accent, accent2},
            --advanced effects
                 {shader1,shader2},
            -- Audio (sound and music)
                 {soundon,musicson,musictype},
            --controls
                 {touchinterfaceison, vibration},
             }
---settings = binser.deserialize(settings)
--print(#binser.s(settings.threeD))
--]]


--[[
                 	--Voice dubs
    talkies=settings.talkies					-- Set this to false if you want to disable the voice dubs

	-- languages
	language=settings.language
    language2=settings.language2
    accent=settings.accent
    accent2=settings.accent2

    --Graphics
    
    threeD=settings.threeD						-- 3d Engine
    skin=settings.skin							-- Select the graphic mode here
												-- Options: "new, "retro" & "classic"
    skinupscaled=settings.skinupscaled			-- This option switch between normal and upscaled graphics (classic mode)
    hdr=settings.hdr							-- This option is not working yet
    res=settings.res							-- Select resolution: "720p",1080p","1440p","4k" & "8k"
    fullscreen=settings.fullscreen				-- Select full screen
        
	--shaders
	caustics=settings.caustics
	shader1=settings.shader1					-- 	Caustic effects
    shader2=settings.shader2					--	CRT Effect

	--Audio (sound and music)
    soundon=settings.soundon					--	Audio effects
    musicison=settings.musicison				--	Music
    musictype=settings.musictype				--	Music type: "new" or "classic"


    --controls
    touchinterfaceison=settings.touchinterfaceison	-- Set this to true to play on touch devices
    vibration=settings.vibration					-- Set this to false if you want to disable vibration
    --]]         


