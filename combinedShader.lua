 normalsWarp = love.graphics.newShader([[
 
#define P 0.051 // Derivative precision for warp effect
#define SIZE vec2(24.0, 16.0) // Warp function scale
#define WATER vec4(0.9, 0.9, 1.0, 1.0) // Water color (applied to warped areas)

uniform float time;                   // Time for wave animation
uniform sampler2D u_texture;           // Diffuse map
uniform sampler2D u_normals;           // Normal map
uniform vec2 Resolution;               // Screen resolution
uniform vec3 LightPos1;                // First light position
uniform vec4 LightColor1;              // First light color (RGBA)
uniform vec3 LightPos2;                // Second light position
uniform vec4 LightColor2;              // Second light color (RGBA)
uniform vec4 AmbientColor;             // Ambient color
uniform vec3 Falloff;                  // Light falloff coefficients
uniform float GrainIntensity;          // Intensity of noise grain

// Wave height function
float func(vec2 coords, float offset) {
    return (sin(coords.x - offset) * sin(coords.y + offset) + 1.0) / 10.0;
}

// X-derivative for wave height
float xDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(P, 0.0), offset) - func(coords - vec2(P, 0.0), offset)) / (2.0 * P);
}

// Y-derivative for wave height
float yDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(0.0, P), offset) - func(coords - vec2(0.0, P), offset)) / (2.0 * P);
}

// Simple noise function
float noise(vec2 uv) {
    return fract(sin(dot(uv, vec2(12.9898, 78.233)) + time * 0.1) * 43758.5453);
}

vec4 effect(vec4 vColor, Image texture, vec2 vTexCoord, vec2 pixcoord) {
    // Wave displacement calculations
    float waveHeight = func(vTexCoord * SIZE, time);
    float dx = xDerivative(vTexCoord * SIZE, time);
    float dy = yDerivative(vTexCoord * SIZE, time);
    vec2 warpOffset = vec2(dx, dy) * waveHeight;

    // Adjust texture coordinates by wave offset
    vec2 warpedTexCoord = vTexCoord + warpOffset;

    // Get texture color and normal map at warped coordinates
    vec4 DiffuseColor = Texel(texture, warpedTexCoord);
    vec3 NormalMap = Texel(u_normals, warpedTexCoord).rgb;

    // Calculate lighting for Light 1
    vec3 LightDir1 = vec3(LightPos1.xy - (pixcoord.xy / Resolution.xy), LightPos1.z);
    LightDir1.x *= Resolution.x / Resolution.y;
    float D1 = length(LightDir1);
    vec3 N = normalize(NormalMap * 2.0 - 1.0);
    vec3 L1 = normalize(LightDir1);
    vec3 Diffuse1 = (LightColor1.rgb * LightColor1.a) * max(dot(N, L1), 0.0);

    // Calculate lighting for Light 2
    vec3 LightDir2 = vec3(LightPos2.xy - (pixcoord.xy / Resolution.xy), LightPos2.z);
    LightDir2.x *= Resolution.x / Resolution.y;
    float D2 = length(LightDir2);
    vec3 L2 = normalize(LightDir2);
    vec3 Diffuse2 = (LightColor2.rgb * LightColor2.a) * max(dot(N, L2), 0.0);

    // Calculate ambient and attenuation
    vec3 Ambient = AmbientColor.rgb * AmbientColor.a;
    float Attenuation1 = 1.0 / (Falloff.x + (Falloff.y * D1) + (Falloff.z * D1 * D1));
    float Attenuation2 = 1.0 / (Falloff.x + (Falloff.y * D2) + (Falloff.z * D2 * D2));

    vec3 Intensity = Ambient + Diffuse1 * Attenuation1 + Diffuse2 * Attenuation2;
    vec3 FinalColor = DiffuseColor.rgb * Intensity;

    // Apply grain noise
    float grain = noise(warpedTexCoord * 10.0) * GrainIntensity;
    FinalColor += grain;

    // Blend with water color based on wave height
    vec4 finalOutput = vColor * vec4(clamp(FinalColor, 0.0, 1.0), DiffuseColor.a);
    return mix(finalOutput, finalOutput * WATER, waveHeight);
}


]])
