# Fish fillets remake
Attempting to implement fish fillets game logic with lua

![Screenshot 2](newsc.avif)
![Screenshot 2](newsc2.avif)

![Screenshot 1](screenshots/spectrumLevel.webp)

# 0. Releases

## Game launcher

A game launcher is being tested and can now be downloaded from [![this repository](https://codeberg.org/glitchapp/love-Builder]

## Fish fillets remake launcher packaged

[![Fish fillets remake Launcher](assets/icons/download-appimage-banner.svg)](https://codeberg.org/glitchapp/fish-fillets-remake-launcher/releases)

[![Fish fillets Os-X](assets/icons/download-for-mac-osx.png)](https://codeberg.org/glitchapp/fish-fillets-remake-launcher/releases)

Check other available releases [![here](https://codeberg.org/glitchapp/fish-fillets-remake/releases)]

# How to download and run the game from source (linux):
=====================================================

## 1. install requirements

Linux: sudo apt install love libwebp-dev

Windows: you need to install love-webp libraries from [![this repository](https://github.com/ImagicTheCat/love-webp/tree/master/dist)
Copy the files inside win64 / win 32 to the folder where love is installed / where binaries are (if you installed love you'll usually find the folder at "C:/programs/love/"

## 2. download game and assets with git

git clone https://codeberg.org/glitchapp/fish-fillets-remake
git clone https://codeberg.org/glitchapp/fish-fillets-remake-assets

## 3. unzip the game and the assets
unzip fish-fillets-remake-master.zip
unzip fish-fillets-remake-assets-master.zip

## 4. Rename the assets to externalassets
rn fish-fillets-remake-assets-master externalassets

## 5.Move the assets inside the game’s folder
mv externalassets fish-fillets-remake-master

## 6. Run the game
love fish-fillets-remake-master

# How to create a ".love" file you can double click to run the game:

## 1.go inside the game’s folder (to which you already moved the assets)
cd fish-fillets-remake-master

## 2.zip all files inside game’s folder
zip -q myfisfillets.zip *.*

## 3. rename the created zip
rn myfishfillets.zip myfishfillets.love

## 4. double click on it

# Windows

The commands above should work if you have git and InfoZip installed
The rename and move commands on windows are move and ren

Settings
========

To permantly change global options please edit config.lua

Controls
========
Keyboard:
Use arrows or WASD to move the current fish.
Use space bar to switch fish.

backspace: restart level

f1: help

f2: save game

f3: load game

f4: music on/off

f5: sound on/off

f6: show/hide subtitles

f7: Caustics effects on / off

f8: CRT effect on / off

f9: switch remade / classic graphics

f10: game menu

f11: switch resolution (1080p / 720p)

f12: switch to retro mode / color schemes


1: switch between normal and upscaled graphics

2: 3d engine

3: diorama

4: touch controls

o: options menu

escape: quit


Debug keys:

6: debug mode

7: background on / off

8: objects on / off

9: players on / off

Gamepad controls:
Layout 1 (Default):
In the default layout the left analog stick control any fish (you switch them by pressing the key A) and the right analog stick controls the camera.
![Default controls](https://codeberg.org/glitchapp/fish-fillets-remake/raw/branch/main/assets/interface/xboxgamepadDefault.webp)
Layout 2 (Alternative):
In the alternative layout the left analog stick controls the first fish and the right analog stick controls the second fish. There's no controls for the camera in the alternative mode.
![Alternative controls](https://codeberg.org/glitchapp/fish-fillets-remake/raw/branch/main/assets/interface/xboxgamepadAlternative.webp)

Mouse controls:

First button: select hovered option
Second button: Toggle mouse / thumbstick pointer
Third button: reset zoom
Wheel: Zoom in / out

## F.A.Q.

Frequently asked questions, known issues and workarounds will be added to [![the wiki](https://codeberg.org/glitchapp/fish-fillets-remake/wiki/Wiki)


## Contributing

I'm looking for help in the following areas:

- Development
  - Unresolved issues will be posted on issues, pull request to solve them are welcome, if an issue is not opened please open it first so that the provided solution can be explained and dicussed first.
  - Please check [![the following guidelines](https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/states/Modding_Guide.md) if you want to contribute / tweak the game.

- 2d graphic art
  - None of the current assets are definitive, contributions are welcome and apreciated.

- Modeling and texturing
  - The current engine supports 3d rendering, for more information please check [3DreamEngine ]( https://github.com/3dreamengine/3DreamEngine)
  - There is a Vr port of the game in development using Lövr which also requires 3d assets: [Fish fillets VR](https://codeberg.org/glitchapp/fishfillets-Vr)
  - Future ports to other 3d engines where the assets could be used are possible and relatively easy to implement, the project's game logic can be found here [game logic](https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/logic/push-blocks.lua)
  
- Testing
  - Betatesting, reporting bugs and providing feedback helps to improve this software.

- Feedback
  - Feedback regarding any part of the game is welcome and helpful, feel free to comment on any aspect you want to be improved.

- Distribution
	- The game has reached almost 800 Mb (683Mb compressed) and I have two binaries ready to be distributed for linux and windows with all required libraries and assets.
	Any help to distribute them wether by seeding them as a torrent or by hosting them and sharing them would be apreciated.
	

# Credits

	[![Credits](https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/AUTHORS.md)]

# License

 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

The libraries on the lib folder belong to their authors and have their own licenses which can be found on their folders.

# Donations

If you like this remake and you want to support the development - please consider to donate. Any donations are greatly appreciated.

![Zcash address:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/Zc.webp) t1h9UejxbLwJjCy1CnpbYpizScFBGts5J3N
![Zcash Qr Code:](https://codeberg.org/glitchapp/pages/raw/branch/main/otherProjects/img/ZCashQr160.webp)
