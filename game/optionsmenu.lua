--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This code defines a function called "optionsload" which is responsible for loading different options related to the game. The options loaded include languages, audio, controls, and graphics.

The function starts by setting the values for "xspace" and "yspace" based on the value of "res" (resolution). The values of "xspace" and "yspace" are used to create space between the different UI elements displayed on the screen.

Next, the function loads the languages, audio, controls, and graphics by calling four different functions: "optionsloadlanguages()", "optionsloadaudio()", "optionsloadcontrols()", and "optionsloadgraphics()".

The code then creates several UI elements, such as buttons, tabs, and their respective positions, text, colors, and fonts.
--]]

function optionsload()
		if res=="1080p" then xspace = 200 yspace = 50
	elseif res=="1440p" then xspace = 200 yspace = 50
	elseif res=="4k" then xspace = 350 yspace = 100
	else  xspace = 250 yspace = 75
	end

optionsloadlanguages()	-- load languages
optionsloadaudio()		-- load sounds
optionsloadcontrols()	-- load controls
optionsloadgraphics()	-- load graphics
optionsloadgraphicshub()	-- load graphics
optionsmenupressed=false	--The variable "optionsmenupressed" is set to false.

	scolor1={1,1,1,0.95} scolor2={1,1,1,0.7}  scolor3={1,1,1,1}
	if chromaab==nil then chromaab=false end		-- avoid chromaab to be nil
	
optionsReturnButton = {
	text = returntext,
	x = xspace*6,
	y = yspace,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

savesettingsButton = {
	text = "Yes",
	x = xspace*2,
	y = yspace*7,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

donotsavesettingsButton = {
	text = "No",
	x = xspace*4,
	y = yspace*7,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

-- Tabs

languagetabButton = {
	text = "Language",
	x = xspace*2,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


graphicstabButton = {
	text = "Graphics",
	x = xspace*3,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

audiotabButton = {
	text = "Audio",
	x = xspace*4,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

controlstabButton = {
	text = "Controls",
	x = xspace*4.8,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
--[[

Finally, if the resolution is 1080p or 1440p, the font size of the UI elements is reduced to "poorfishsmall".
--]]
if res=="1080p" or res=="1440p" then	-- reduce font size below 4k
	optionsReturnButton.font=poorfishsmall 
	savesettingsButton.font=poorfishsmall 
	donotsavesettingsButton.font=poorfishsmall 
	languagetabButton.font=poorfishsmall 
	graphicstabButton.font=poorfishsmall 
	audiotabButton.font=poorfishsmall 
	controlstabButton.font=poorfishsmall 
end

end


function optionssupdate(dt)
	
end




function drawOptionsFocusLanguages()


	--if joystick then 
		love.graphics.setColor(0,0,1,1)
		buttonfocusanimations()		-- focus color and size animations
	--options menu
			if bfocus=="languages" then love.graphics.rectangle ('line', languagetabButton.x, languagetabButton.y, 150, 50)
		elseif bfocus=="langarrowup" then love.graphics.rectangle ('line', LanguageFocusUpButton.x, LanguageFocusUpButton.y, 150, 50)
		elseif bfocus=="currentlanguage" then love.graphics.rectangle ('line', 1000,300,150, 50)
		elseif bfocus=="langarrowdown" then love.graphics.rectangle ('line', LanguageFocusDownButton.x, LanguageFocusDownButton.y, 150, 50)
		elseif bfocus=="actor1" then love.graphics.rectangle ('line', JohnButton.x, JohnButton.y, 150, 50)
		elseif bfocus=="actor2" then love.graphics.rectangle ('line', snoozyButton.x, snoozyButton.y, 150, 50)
		elseif bfocus=="accent1" then love.graphics.rectangle ('line', enusButton.x-200,enusButton.y, 150, 50)
		elseif bfocus=="accent2" then love.graphics.rectangle ('line', enusButton.x,enusButton.y, 150, 50)
		elseif bfocus=="graphics" then love.graphics.rectangle ('line', graphicstabButton.x, graphicstabButton.y, 150, 50)
		elseif bfocus=="audio" then love.graphics.rectangle ('line', audiotabButton.x, audiotabButton.y, 150, 50)
		elseif bfocus=="controls" then love.graphics.rectangle ('line', controlstabButton.x, controlstabButton.y, 150, 50)
		elseif bfocus=="optionsreturn" then love.graphics.rectangle ('line', optionsReturnButton.x, optionsReturnButton.y, 150, 50)
		elseif bfocus=="confirmsave" then love.graphics.rectangle ('line', savesettingsButton.x, savesettingsButton.y, 150, 50)
		elseif bfocus=="rejectsave" then love.graphics.rectangle ('line', donotsavesettingsButton.x, donotsavesettingsButton.y, 150, 50)
		end

	--end
end

    
--This is a Lua function called optionsdraw(dt) that draws the options menu in a game. It takes in one argument dt which stands for delta time.
function optionsdraw(dt)
			
--The function first checks if the options menu is not pressed. If it is not pressed, the function sets the color for chromatic aberration effect and then checks if chromatic aberration is false. If it is false, the function draws an image called covercd with a scale of 2 on the screen. If it is true, then the function draws the same image three times with different colors to create a chromatic aberration effect.
if optionsmenupressed==false then
	love.graphics.setColor(adjustr,adjustb,adjustb,1)
	-- Chromatic aberration
	if not (gamestatus=="gameplusoptions") then	-- do not render the background if the options are being called from the game screen so that the options remain transparent.
		if chromaab==false then love.graphics.draw (covercd, 0,0,0,2,2)
	elseif chromaab==true then
			love.graphics.setColor(1,0,0,0.3)
			love.graphics.draw (covercd, 0,0,0,2,2)
			love.graphics.setColor(0,1,0,0.3)
			love.graphics.draw (covercd, 0,5,-5,2,2)
			love.graphics.setColor(0,0,1,0.3)
			love.graphics.draw (covercd, 0,10,-10,2,2)
		end
	end
			
	--Next, the function checks the current tab selected by the user. If the current tab is "languages", then it calls another function called optionsdrawlanguages(dt) to draw the language options. If the current tab is "graphics", then it checks if the graphics button is being hovered by the mouse cursor and whether the shader is enabled. If the button is hovered and the shader is not enabled, then it enables a blur effect on the image and then draws the covercd image and the graphics options using a shader. Otherwise, it just draws the graphics options. The function also draws some information about the graphics options. If the current tab is "audio", then it calls another function called optionsdrawaudio(dt) to draw the audio options. If the current tab is "controls", then it calls another function called optionsdrawcontrols(dt) to draw the controls options.
		-- languages
	if bfocustab=="languages" then	optionsdrawlanguages(dt) 	end 
		-- Picture
    if bfocustab=="graphics" then 
		
		if graphbuttonhovered==true and not shader1 and not (gamestatus=="gameplusoptions") then
			
			if timer>1 and timer<4 then
				--blur effect
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
				effect.enable("boxblur")
					effect.boxblur.radius=2
					--taps 	odd number >= 3 	7
					--offset 	number 	1
					--sigma 	number 	-1
				love.graphics.setShader()
				effect(function()
				love.graphics.setColor(1,1,1,1)
					love.graphics.draw (covercd, 0,0,0,2,2)
					optionsdrawgraphics(dt)
				end)
				graphicsdrawinformation(graphmes1,graphmes2,dt)
			elseif timer>4 then timer=0 graphbuttonhovered=false
			
			else	optionsdrawgraphics(dt)
			end
		else		optionsdrawgraphics(dt)
		end
        
		if graphbuttonhovered and not shader1 and gamestatus=="gameplusoptions" then
			if timer>1 then
				
				love.graphics.setColor(1,1,1,1)
					optionsdrawgraphics(dt)
				graphicsdrawinformation(graphmes1,graphmes2,dt)
			end
		end
    end
    
    if bfocustab=="graphics" then
    	drawGraphicTabs()
	end
		
		
		if bfocus=="skins" then	 DrawSkins()
	elseif bfocus=="colors" then DrawGraphicRGBPresets()
	elseif bfocus=="retroscheme" then DrawColorSchemes()
	elseif bfocus=="graphicshub" then  
			if not gamestatus=="gameplusoptions" then love.graphics.draw (covercd, 0,0,0,2,2) end
				optionsdrawgraphicshub(dt)
			if graphbuttonhovered then 
				graphicsdrawinformation(graphmes1,graphmes2,dt)
			end
	elseif bfocus=="performance" then 			
			DrawGraphicProfiles()
			DrawlimitFPS()
			DrawBenchmark()
			DrawVSync()
	elseif bfocus=="shaders" then
			DrawGraphicChromaticAberration()
			DrawGraphicShaders()
			DrawthreeDGraphics()
	elseif bfocus=="resolution" then
			DrawGraphicResolution()
			DrawGraphicFullScreen()
	elseif bfocus=="graphicsAlignements" then
			DrawAlignementsButtons()
		end
    
    --The function then draws a rectangle based on the current tab selected by the user. It also sets the font for the menu text to a font called "poorfish".
        if joystick then
			love.graphics.draw(leftshoulder,languagetabButton.x-150,languagetabButton.y,0,0.2,0.2)
			love.graphics.draw(rightshoulder,controlstabButton.x+150,controlstabButton.y,0,0.2,0.2)
		else
			love.graphics.draw(Qkeyicon,languagetabButton.x-150,languagetabButton.y,0,0.3,0.3)
			love.graphics.draw(EKeyicon,controlstabButton.x+150,controlstabButton.y,0,0.3,0.3)
		end
        love.graphics.setColor(1,1,1,0.2)
		if bfocustab=="languages" then love.graphics.rectangle ('fill', languagetabButton.x,languagetabButton.y,150,50)
    elseif bfocustab=="graphics" then 
    love.graphics.setColor(1,1,0,0.2)
    love.graphics.rectangle ('fill', graphicstabButton.x,graphicstabButton.y,150,50)
    love.graphics.rectangle ('line', SkinsTabButton.x,SkinsTabButton.y,1300,30)
    love.graphics.setColor(1,1,1,0.2)
    elseif bfocustab=="audio" then love.graphics.rectangle ('fill', audiotabButton.x,audiotabButton.y,150,50)
    elseif bfocustab=="controls" then love.graphics.rectangle ('fill', controlstabButton.x,controlstabButton.y,150,50)
    end
    
    
    love.graphics.rectangle ('fill', xspace*1.7,yspace*2,1200,1200)		-- options rectangle
     love.graphics.setFont(poorfish)
     
      --language Tab
      --The function checks if the language tab button is being hovered by the mouse cursor. If it is, then it draws the button with a hover effect. If the button is clicked, then the current tab is set to "languages". The same is done for the other tabs: graphics, audio, and controls.
     local hovered = isButtonHovered (languagetabButton)
		drawButton (languagetabButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			love.timer.sleep( 0.3 )
			bfocustab="languages" bfocus="langarrowup"
	-- graphics tab
		end
		
		--If the user clicks on the "Graphics" tab, the code loads three images using the Love2D framework's love.graphics.newImage function and assigns them to variables. If these images have not yet been loaded, the function WebP.loadImage is used to load the image files in WebP format. Then, the current tab is changed to "graphics" so that the graphics options are displayed.
          local hovered = isButtonHovered (graphicstabButton)
		drawButton (graphicstabButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			love.timer.sleep( 0.3 )
			if level1bck==nil then
				level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
				level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
				level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
			end
			bfocustab="graphics"
			bfocus="skins"
			
			
		end
	
	-- audio tab
	--If the user clicks on the "Audio" tab, the current tab is changed to "audio" so that the audio options are displayed.
	     local hovered = isButtonHovered (audiotabButton)
		drawButton (audiotabButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			bfocustab="audio"
			bfocus="sound"
			bfocus2="on"
			love.timer.sleep( 0.3 )
		end
	
	-- controls tab
	--If the user clicks on the "Controls" tab, the current tab is changed to "controls" so that the control options are displayed.
	     local hovered = isButtonHovered (controlstabButton)
		drawButton (controlstabButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			love.timer.sleep( 0.3 )
			bfocustab="controls"
			bfocus="vibration"
			bfocus2="on"
		end
		
	--Finally, the code checks the current tab and calls a function to draw the appropriate options based on the current tab. If the current tab is "audio," it calls the optionsdrawaudio function. If the current tab is "controls," it calls the optionsdrawcontrols function.
		-- Audio
	if bfocustab=="audio" then  optionsdrawaudio(dt)  end

	if bfocustab=="controls" then optionsdrawcontrols(dt) end
    	
    --Lastly, the function checks if the return button is being hovered by the mouse cursor. If it is, then it draws the button with a hover effect. If the button is clicked, then the function sets the input time to zero and plays a sound effect. The current tab is then set to the previous screen.
	local hovered = isButtonHovered (optionsReturnButton)
	drawButton (optionsReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and gamestatus=="options" then
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			inputtime=0
			shader2=false
			love.timer.sleep( 0.3 )
	
			loadingprocess=false
			levelload()
			if languagehaschanged==true then loadborescript() end	-- load bore dubs if language has changed
			if languagehaschanged or musichaschanged or skinhaschanged then
				optionsmenupressed=true
			else
			gamestatus="levelselection"
			end
	elseif hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and gamestatus=="gameplusoptions" then
			gamestatus="game"
	end
end	
--[[
This section of the code checks if the options menu has been pressed. If it has been pressed, it will display a message asking if the user wants to save the settings they changed.
It then provides two options: "Save Settings" and "Do not Save Settings".
If the user clicks on "Save Settings", it will save the current settings by calling the function "savemygame()" and reset the flags that keep track of whether the language, music, or skin settings have changed. If the user clicks on "Do not Save Settings", it will simply reset the flags and move the game status to the level selection screen.
--]]
	if optionsmenupressed then
		love.graphics.print ("Settings has changed, would you like to save the settings?: ", 200,400)
	  --save settings
     local hovered = isButtonHovered (savesettingsButton)
		drawButton (savesettingsButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			love.timer.sleep( 0.3 )
			savemygame()
			languagehaschanged=false
			musichaschanged=false
			skinhaschanged=false
			optionsmenupressed=false
			gamestatus="levelselection"
		end
	
	  --Do not save settings
     local hovered = isButtonHovered (donotsavesettingsButton)
		drawButton (donotsavesettingsButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			inputtime=0
			love.timer.sleep( 0.3 )
			languagehaschanged=false
			musichaschanged=false
			skinhaschanged=false
			optionsmenupressed=false
			gamestatus="levelselection"
		end 
	end
	drawArcButton()	-- draw an arc when button is held to save changes
end

--This function reloads the assets used to draw a preview of level 1 with selected effects. 
--If the selected skin is "remake," it loads the assets using the loadlevelassets() function and assigns the foreground and background layers using assignfrgbck().
-- If the selected skin is "classic," it loads the classic assets using the loadlevelassetsclassic() function and assigns the foreground and background layers using assignfrgbckclassic()
function reloadpreviewassets()
		if skin=="remake" then
				loadlevelassets()			-- load assets
				assignfrgbck()				-- Assign layers
		elseif skin=="classic" then	
				loadlevelassetsclassic()	-- load classic assets
				assignfrgbckclassic()		-- Assign layers
		end
end
