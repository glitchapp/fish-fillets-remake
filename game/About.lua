
-- variables to keep track of dragging state
isDragging = false
isHelpDragging = false
WhatsNewInfo=false

-- Initialize the window and button positions
windowX, windowY = 800, 360  -- Initial window position
offsetX, offsetY = 400, 25  -- Offset for mouse dragging

-- Variables to keep track of dragging state for the help window
helpOffsetX, helpOffsetY = 0, 0
helpWindowX, helpWindowY = 100, 100  -- Initial help window position

		MainAboutButton = {
		text = "About",
		x = windowX + 440,
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

		FFilletsHyperlinkButton = {
		text = "Repository",
		x = windowX + 100,
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	
		FFilletsAuthorsButton = {
		text = "Authors",
		x = windowX + 300, 
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	
		
	WhatsNewButton = {
		text = "What's new?",
		x = windowX + 500, 
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

	DonateButton = {
		text = "Donate",
		x = windowX + 700, 
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	
	CloseDonateButton = {
		text = "Close",
		x = windowX + 700, 
		y = windowY + 540,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

		AboutFFilletsCloseButton = {
		text = "Close Window",
		x = windowX+500, 
		y = windowY,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	
		local paragraph = "Fish fillets remake <br>" ..
                          "Copyright (C) 2022 Glitchapp <br>" ..
                          ". <br>" ..
                          "This program is free software: you can redistribute it and/or modify " ..
                          "it under the terms of the GNU General Public License as published by " ..
                          "the Free Software Foundation, either version 2 of the License, or " ..
                          "(at your option) any later version.<br>" ..
                          ". <br>" ..
                          "This program is distributed in the hope that it will be useful, " ..
                          "but WITHOUT ANY WARRANTY; without even the implied warranty of " ..
                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " ..
                          "GNU General Public License for more details." ..
                          ". <br>"
		
		
		 local paragraphDonate ="Your support makes a world of difference! <br>" ..

							"By contributing, you are directly fueling the development of open-source software <br>" ..
							"Your generosity allows me to continue creating and sharing these exciting ventures <br>" ..
							"Your contribution makes you an integral part of these efforts.<br>" ..
							"Together, we foster creativity, share knowledge, and make our collective dreams a reality.<br>" ..

							"Thank you for being a part of this and for your invaluable support!<br>"
							
		 local paragraphWhatsNew =
							"- Added love-boids library (https://github.com/Jehadel/love-boids), a Reynolds'boids algorithm implementation for realistic-looking representation of school of fish.<br>" ..
							"- Added instructional guide with ilustrations and screenshots<br>" ..
							"- Added alternative controller layout<br>" ..
							"- Smooth fade in / out transitions between menu and levels<br>" ..
							"- Level editor <br>" ..
							"- Gamepad support featuring pointer control and radial menu<br>" ..
							"- GUI redesign<br>" ..
							"- Custom windows manager (used in this window) <br>" ..
							"- Error handler function <br>" ..
							"<br>" ..
							"To see other minor updates please check: NEWS.md <br>"
		  
			paragraphtext = string.gsub(paragraph, "<br>", "\n")
			paragraphWhatsNewtext = string.gsub(paragraphWhatsNew, "<br>", "\n")
		
			paragraphDonatetext = string.gsub(paragraphDonate, "<br>", "\n")
				 
	
		-- Define maxWidth
		local maxWidth = 800
		local lineHeight = love.graphics.getFont():getHeight()/2.5
	
		zCashIcon = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/icons/Zc.webp")))
		zCashQrCode = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/icons/ZCashQr.webp")))

-- function to update button positions
function updateButtonPositions()
	
	 windowX = love.mouse.getX() - offsetX
    windowY = love.mouse.getY() - offsetY

    MainAboutButton.x = windowX + 400
    MainAboutButton.y = windowY + 540

    AboutFFilletsCloseButton.x = windowX + 600
    AboutFFilletsCloseButton.y = windowY

    FFilletsHyperlinkButton.x = windowX + 100
    FFilletsHyperlinkButton.y = windowY + 540

    FFilletsAuthorsButton.x = windowX + 300
    FFilletsAuthorsButton.y = windowY + 540
    
    WhatsNewButton.x =  windowX + 500
    WhatsNewButton.y =  windowY + 540

    DonateButton.x = windowX + 700
    DonateButton.y = windowY + 540

    CloseDonateButton.x = windowX + 600
    CloseDonateButton.y = windowY + 540


end




function drawTextContent(paragraph, windowX, windowY, maxWidth, lineHeight)
    local lines = {}
    local currentLine = ""

    for line in paragraph:gmatch("[^\n]+") do
        local words = {}
        for word in line:gmatch("%S+") do
            table.insert(words, word)
        end

        local testLine = currentLine
        for _, word in ipairs(words) do
            local testLineWidth = love.graphics.getFont():getWidth(testLine .. word .. " ")
            if testLineWidth <= maxWidth then
                testLine = testLine .. word .. " "
            else
                table.insert(lines, testLine)
                testLine = word .. " "
            end
        end

        if testLine ~= "" then
            table.insert(lines, testLine)
        end
        currentLine = ""
    end

    local x, y = windowX + 50, windowY + 210
    for _, line in ipairs(lines) do
        love.graphics.print(line, x, y)
        y = y + lineHeight
    end
end



function drawAboutFFillets()
	
     if aboutInfo == true then
        love.graphics.setColor(0.2, 0.2, 0.2, 0.8)

        -- Conditionally draw the window based on dragging state
        
        if isDragging then
           
            love.graphics.rectangle("fill", windowX, windowY, 800 + 50, 50)
            love.graphics.rectangle("fill", windowX, windowY + 50, 800 + 50, 600)
             
             -- Call the function when the window is dragged
             love.graphics.setColor(1, 1, 1)
            --drawTextContent(paragraph, 800 - 25, 220, maxWidth, lineHeight)
				if donateInfo==false then
					if WhatsNewInfo==false then
						drawTextContent(paragraphtext, love.mouse.getX() - offsetX, love.mouse.getY() - offsetY-120, maxWidth, lineHeight)
					elseif WhatsNewInfo==true then
						drawTextContent(paragraphWhatsNewtext, love.mouse.getX() - offsetX, love.mouse.getY() - offsetY-120, maxWidth, lineHeight)
					end
			elseif donateInfo==true then
				drawTextContent(paragraphdonatetext, love.mouse.getX() - offsetX, love.mouse.getY() - offsetY-120, maxWidth, lineHeight)
			end
			
			--drawTextContent(paragraph, love.mouse.getX() - offsetX, love.mouse.getY() - offsetY-120, maxWidth, lineHeight)
			
        else
          
            love.graphics.rectangle("fill", windowX, windowY, 800 + 50, 50)
            love.graphics.rectangle("fill", windowX, windowY + 50, 800 + 50, 600)
        end

        love.graphics.setColor(1, 1, 1)
        love.graphics.rectangle("line", windowX, windowY, 800 + 50, 50)
        love.graphics.rectangle("line", windowX, windowY + 50, 800 + 50, 600)

        if isDragging then
            -- Update the window position based on mouse movement
            love.graphics.print("About", windowX, windowY, 0, 1)
            love.graphics.print("Drag to move", windowX + 50, windowY, 0, 1)
        else
            if (donateInfo == false) then
                love.graphics.print("About", windowX, windowY + 10, 0, 1)
                love.graphics.print("Drag to move", windowX + 200, windowY + 10, 0, 1)
            elseif donateInfo == true then
                love.graphics.print("Donate info", windowX, windowY + 10, 0, 1)
            end
            
        -- Replace "<br>" with newline characters "\n"
        if donateInfo==false then
		elseif donateInfo==true then
			love.graphics.draw( zCashIcon,windowX+400, windowY+450, 0, 1)  
			love.graphics.draw( zCashQrCode,windowX+200, windowY+450, 0, 0.5)          
		end	

       -- Call the function when the window is not dragged
		--drawTextContent(paragraph, windowX, windowY-120, maxWidth, lineHeight)
				if donateInfo==false then
						if WhatsNewInfo==false then
							drawTextContent(paragraphtext, windowX, windowY-120, maxWidth, lineHeight)
					elseif WhatsNewInfo==true then
							drawTextContent(paragraphWhatsNewtext, windowX, windowY-120, maxWidth, lineHeight)
					end
				
			elseif donateInfo==true then
				drawTextContent(paragraphDonatetext, windowX, windowY-120, maxWidth, lineHeight)
			end
    end
    
    if aboutInfo==true and donateInfo==false then
     	local hovered = isButtonHovered (AboutFFilletsCloseButton)
				drawButton (AboutFFilletsCloseButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
						aboutInfo=false
						donateInfo=false
						love.timer.sleep( 0.3 )	
					end
    
    	local hovered = isButtonHovered (FFilletsHyperlinkButton)
				drawButton (FFilletsHyperlinkButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
							love.system.openURL("https://codeberg.org/glitchapp/fish-fillets-remake")
						love.timer.sleep( 0.3 )	
					end
		local hovered = isButtonHovered (FFilletsAuthorsButton)
				drawButton (FFilletsAuthorsButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
							love.system.openURL("https://codeberg.org/glitchapp/fish-fillets-remake-assets/src/branch/main/Credits.md")
						love.timer.sleep( 0.3 )	
					end
		if 	WhatsNewInfo==false then
				local hovered = isButtonHovered (WhatsNewButton)
				drawButton (WhatsNewButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
						WhatsNewInfo=true
						--MainAboutButton.x= windowX + 500
						love.timer.sleep( 0.3 )	
					end
		end
				local hovered = isButtonHovered (DonateButton)
				drawButton (DonateButton, hovered)
					if hovered and (love.mouse.isDown(1)) then 
						donateInfo=true
						WhatsNewInfo=false
						love.timer.sleep( 0.3 )	
					end
					
					
		elseif donateInfo==true then
		
		local hovered = isButtonHovered (MainAboutButton)
			drawButton (MainAboutButton, hovered, MainAboutButton.text)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and aboutInfo==true then 
					donateInfo=false
					WhatsNewInfo=false
					--video effect
					effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
					effect.enable("boxblur")
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("About Fish fillets remake", MainAboutButton.x,MainAboutButton.y-100,0,1)
				end
		
					local hovered = isButtonHovered (CloseDonateButton)
					drawButton (CloseDonateButton, hovered, AboutFFilletsButton.text)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
						aboutInfo=false
						donateInfo=false
						WhatsNewInfo=false
						love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Close donations info", DonateButton.x,DonateButton.y+100,0,1)
				end
		end
		
			if WhatsNewInfo==true then
					donateInfo=false
					WhatsNewInfo=true
					
				local hovered = isButtonHovered (MainAboutButton)
					drawButton (MainAboutButton, hovered, MainAboutButton.text)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and aboutInfo==true then 
					donateInfo=false
					WhatsNewInfo=false
					--video effect
					effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
					effect.enable("boxblur")
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("About Fish fillets remake", MainAboutButton.x,MainAboutButton.y-100,0,1)
				end
			end
	end
	
end

function drawDonateInfo()

end
