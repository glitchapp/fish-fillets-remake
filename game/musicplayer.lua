
--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--The musicupdate(dt) function is used to update the state of the music player.
--If cdplaying is true, it will rotate the CD by decreasing the cdangle value based on cdspeed multiplied by the dt parameter which represents the time elapsed since the last update. If cdangle becomes greater than 360, it is reset to 0.
function musicupdate(dt)
		--[[
		if cdplaying==true then
			--timer = timer + dt
			creditsupdate(dt)
			creditsendcutsceneupdate(dt)			-- This is new in this version of the credits and will update animations behind the credits text
			cdangle = cdangle - (cdspeed * dt)  -- rotate cd
		
			--equalizerupdate(dt)
			if cdangle>360 then cdangle=0 end
		end
		--]]
		volume = love.audio.getVolume(music)
end




--[[This is a Lua function that appears to be a part of a music player GUI. It takes a single argument "dt" which is most likely the time in seconds since the last update.

The function first sets the color to a semi-transparent white and draws a CD cover image at a specific position. It then checks if the "creditsReturnButton" is being hovered over by the mouse and draws the button with the appropriate hover state. If the button is clicked, it stops the current audio track and either loads a new one or goes back to the level selection menu, depending on the value of the "author" variable.

The function then checks if the "stopbutton" is being hovered over and drawn it with the appropriate hover state. If the button is clicked, it stops the current audio track and sets various variables to their default values.

Finally, the function checks if the "kufriksongb" and "classicmenusongb" buttons are being hovered over and draws them with the appropriate hover state. If either button is clicked and the corresponding track is unlocked, it loads and plays the audio track and sets various variables to their respective values. If the corresponding track is locked, it plays a negative sound effect.
--]]
function musicdrawcd(dt)
	love.graphics.setColor(1,1,1,0.5)
	love.graphics.draw(covercd,900,0,0,1)
	love.graphics.setColor(1,1,1,1)
end

function musiccreditsdraw(dt)
		if author=="classictracks" and not (author==nil) then
		
					love.graphics.setFont(poorfish)
					love.graphics.print(classictrackst,100,100,0,1)
						
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(classictrackst2,100,150,0,1)
						
					
	elseif author=="pixelsphere" then
	
					love.graphics.setFont(poorfish)
					love.graphics.print(pixelspheret,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(pixelspheret2,100,150,0,1)
					love.graphics.print(pixelspheret3,300,150,0,1)
					
					
	elseif author=="isaiah658" then
					love.graphics.setFont(poorfish)
					love.graphics.print(isaiah658t,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(isaiah658t2,100,150,0,1)
					
					
	elseif author=="umplix" then
					
					love.graphics.setFont(poorfish)
					love.graphics.print(umplixt,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(umplixt2,100,150,0,1)
					
					
	elseif author=="ericmatyas" then
					love.graphics.setFont(poorfish)
					love.graphics.print(ericmatyast,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(ericmatyast2,100,160,0,1)
					love.graphics.print(ericmatyast3,100,190,0,1)
					
					
	elseif author=="cleytonkauffman" then
					love.graphics.setFont(poorfish)
					love.graphics.print(cleytonkauffmant,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(cleytonkauffmant2,100,160,0,1)
					love.graphics.print(cleytonkauffmant3,100,190,0,1)
					
					
	elseif author=="marcelofg55" then
				
					love.graphics.setFont(poorfish)
					love.graphics.print(marcelofgt,100,100,0,1)
					
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(marcelofgt2,100,160,0,1)
					love.graphics.print(marcelofgt3,100,190,0,1)
					
					
	elseif author=="springyspringo" then
					love.graphics.setFont(poorfish)
					love.graphics.print(springyspringot,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(springyspringot2,100,160,0,1)
					
					
	elseif author=="tokyogeisha" then
					love.graphics.setFont(poorfish)
					love.graphics.print(tokygeishat,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(tokygeishat2,100,160,0,1)
					
					
	elseif author=="poinl" then
					love.graphics.setFont(poorfish)
					love.graphics.print(poinlt,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(poinlt2,100,160,0,1)
					
					
	elseif author=="hectavex" then
					love.graphics.setFont(poorfish)
					love.graphics.print(hectavext,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(hectavext2,100,160,0,1)
					
					
	elseif author=="tedkerr" then
					love.graphics.setFont(poorfish)
					love.graphics.print(tedkerrt,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(tedkerrt2,100,160,0,1)
					
					
	elseif author=="isao" then
					love.graphics.setFont(poorfish)
					love.graphics.print(isaot,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(isaot2,100,160,0,1)
					
					
	elseif author=="viktorkraus" then
					love.graphics.setFont(poorfish)
					love.graphics.print(viktorkraus,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(viktorkraus2,100,160,0,1)
					
					
	elseif author=="glitchapp" then
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(glitchappt,100,100,0,1)
					love.graphics.setFont(poorfishsmall)
					love.graphics.print(glitchappt2,100,160,0,1)
					
	end
	if not (currenttrackname=="none") then
		love.graphics.print(currenttrackname,100,250,0,1)
		love.graphics.print(currenttrackdesc,100,750+bcspace,0,1)
		love.graphics.print("rate: " .. rate .. " channels: " .. channels,100,800+bcspace,0,1)
	end
end

function musicdraw(dt)

	if not cdplaying then
		local hovered = isButtonHovered (musicPlayButton)
		drawButton (musicPlayButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
			if playerplaying==true then
				playerplaying=false
				--love.audio.stop()
				love.audio.pause()
				love.timer.sleep( 0.3 )
			elseif playerplaying==false then
			playerplaying=true 
			--love.audio.play()
			love.timer.sleep( 0.3 )
			end
		end
	end

	local hovered = isButtonHovered (AboutMusicPlayerButton)
			drawButton (AboutMusicPlayerButton, hovered, AboutMusicPlayerButton.text)
				if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
					aboutInfo=true
				elseif hovered then
					love.graphics.print("About info", AboutMusicPlayerButton.x,AboutMusicPlayerButton.y+100,0,1)
				end
	
	drawAboutMusic()
	
local hovered = isButtonHovered (musicReturnButton)
	drawButton (musicReturnButton, hovered)
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	
	if author=="none" then
		currenttrackname=""
		currenttrackdesc=""
		love.audio.stop()
		
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
			if skin=="remake" then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
		elseif skin=="classic" then lovebpmload("/externalassets/music/classic/menu.ogg")
		end
		love.timer.sleep( 0.3 )
		gamestatus="levelselection"
	elseif not (author=="none") then
		currenttrackname=""
		currenttrackdesc=""
	TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
	love.timer.sleep( 1 )
	author="none"
	end
	end

	-- Music player controls
	local hovered = isButtonHovered (musicbackwardbutton)
	drawButton (musicbackwardbutton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and cdplaying then
	musicposition = music:tell( "seconds" )
	music:seek( musicposition-5, "seconds" )
	love.timer.sleep( 0.2 )
	end
	
	if cdplaying then 
		local hovered = isButtonHovered (musicstopbutton)
		drawButton (musicstopbutton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		love.audio.stop()
		cdplaying=false
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
			currenttrackname=""
			currenttrackdesc=""
			rate=0
			channels=0
		love.timer.sleep( 0.3 )
		end
	end
	
	if cdplaying and gamestatus=="music" then
		if res=="1080p" then
			-- Track position
			love.graphics.rectangle( "fill", 500, 800, music:tell( "seconds" )*2, 25 )
			love.graphics.rectangle( "line", 500, 795, music:getDuration( "seconds" )*2, 35 )
			love.graphics.print("Playing: " .. math.floor(music:tell( "seconds" )/ 60) .. " : " .. math.floor(music:tell( "seconds" )% 60),300,900,0, 0.8, 1) 
			love.graphics.print("Duration: " .. math.floor(music:getDuration( "seconds" )/ 60) .. " : " .. math.floor(music:getDuration( "seconds" )% 60),900,900,0, 0.8, 1) 
		else
			-- Track position
			love.graphics.rectangle( "fill", 1000, 1100, music:tell( "seconds" )*2, 25 )
			love.graphics.rectangle( "line", 1000, 1095, music:getDuration( "seconds" )*2, 35 )
			love.graphics.print("Playing: " .. math.floor(music:tell( "seconds" )/ 60) .. " : " .. math.floor(music:tell( "seconds" )% 60),600,1200,0, 0.8, 1) 
			love.graphics.print("Duration: " .. math.floor(music:getDuration( "seconds" )/ 60) .. " : " .. math.floor(music:getDuration( "seconds" )% 60),1400,1200,0, 0.8, 1) 

			
			--drawequalizer()			-- equalizer and volume controls are in musicequalizer.lua
			drawvolumecontrols()	
		end
	end
	
	local hovered = isButtonHovered (musicforwardbutton)
	drawButton (musicforwardbutton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and cdplaying then
	musicposition = music:tell( "seconds" )
	music:seek( musicposition+5, "seconds" )
	love.timer.sleep( 0.2 )
	end
	
	if  cdplaying then
	-- Volume controls
	local hovered = isButtonHovered (volumedownbutton)
	drawButton (volumedownbutton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and cdplaying then
		volume=volume-0.1
		love.audio.setVolume( volume )
	love.timer.sleep( 0.2 )
	end
	
	-- Volume controls
	local hovered = isButtonHovered (volumeupbutton)
	drawButton (volumeupbutton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and cdplaying then
		volume=volume+0.1
		love.audio.setVolume( volume )
	love.timer.sleep( 0.2 )
	end
	end
		if author=="classictracks" and not (author==nil) then
					
			if kufrik_song==false then love.graphics.draw (locksymbol, kufriksongb.x+150,kufriksongb.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (kufriksongb)
			if kufrik_song==true then drawButton (kufriksongb, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if kufrik_song==true then
					cdplaying=true
					kufriksong = love.audio.newSource( "/externalassets/music/classic/kufrik.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/kufrik.ogg") 
					end
					currenttrackname=kufriksongt
						currenttrackdesc=kufriksongt2
					love.timer.sleep( 0.3 )
				
				elseif kufrik_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			if classicmenu_song==false then love.graphics.draw (locksymbol, classicmenusongb.x+150,classicmenusongb.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (classicmenusongb)
			if classicmenu_song==true then drawButton (classicmenusongb, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if kufrik_song==true then
					cdplaying=true
					classicmenusong = love.audio.newSource( "/externalassets/music/classic/menu.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/menu.ogg") 
					end
					currenttrackname=kufriksongt
						currenttrackdesc=kufriksongt2
					love.timer.sleep( 0.3 )
				
				elseif classicmenu_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
		
			--rybky1
		
			if rybky1_song==false then love.graphics.draw (locksymbol, rybky1b.x+150,rybky1b.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (rybky1b)
			if rybky1_song==true then drawButton (rybky1b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky1_song==true then
					cdplaying=true
					rybky1 = love.audio.newSource( "/externalassets/music/classic/rybky01.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky01.ogg") 
					end
						currenttrackname=rybky1t
						currenttrackdesc=rybky1t2
						love.timer.sleep( 0.3 )
				
				elseif rybky1_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky2
			
			if rybky2_song==false then love.graphics.draw (locksymbol, rybky2b.x+150,rybky2b.y,0,0.1,0.1) end	--locked song
						
			local hovered = isButtonHovered (rybky2b)
			if rybky2_song==true then drawButton (rybky2b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky2_song==true then
					cdplaying=true
					rybky2 = love.audio.newSource( "/externalassets/music/classic/rybky02.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky02.ogg") 
					end
					currenttrackname=rybky2t
						currenttrackdesc=rybky2t2
					love.timer.sleep( 0.3 )
				
				elseif rybky2_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky3
			
				if rybky3_song==false then love.graphics.draw (locksymbol, rybky3b.x+150,rybky3b.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (rybky3b)
			if rybky3_song==true then drawButton (rybky3b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky3_song==true then
					cdplaying=true
					rybky3 = love.audio.newSource( "/externalassets/music/classic/rybky03.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky03.ogg") 
					end
					currenttrackname=rybky3t
						currenttrackdesc=rybky3t2
					love.timer.sleep( 0.3 )
				
				elseif rybky3_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky4
			
				if rybky4_song==false then love.graphics.draw (locksymbol, rybky4b.x+150,rybky4b.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (rybky4b)
			if rybky4_song==true then drawButton (rybky4b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky4_song==true then
					cdplaying=true
					rybky4 = love.audio.newSource( "/externalassets/music/classic/rybky04.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky04.ogg") 
					end
					currenttrackname=rybky4t
						currenttrackdesc=rybky4t2
					love.timer.sleep( 0.3 )
				
				elseif rybky4_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky5
			
				if rybky5_song==false then love.graphics.draw (locksymbol, rybky5b.x+150,rybky5b.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (rybky5b)
			if rybky5_song==true then drawButton (rybky5b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky5_song==true then
					cdplaying=true
					rybky5 = love.audio.newSource( "/externalassets/music/classic/rybky05.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky05.ogg") 
					end
					currenttrackname=rybky5t
						currenttrackdesc=rybky5t2
					love.timer.sleep( 0.3 )
				
				elseif rybky5_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky6
			
				if rybky1_song==false then love.graphics.draw (locksymbol, rybky6b.x+150,rybky6b.y,0,0.1,0.1) end	--locked song
			
			local hovered = isButtonHovered (rybky6b)
			if rybky6_song==true then drawButton (rybky6b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky6_song==true then
					cdplaying=true
					rybky6 = love.audio.newSource( "/externalassets/music/classic/rybky06.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky06.ogg") 
					end
					currenttrackname=rybky6t
						currenttrackdesc=rybky6t2
					love.timer.sleep( 0.3 )
				
				elseif rybky6_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky7
			
				if rybky7_song==false then love.graphics.draw (locksymbol, rybky7b.x+150,rybky7b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky7b)
			if rybky7_song==true then drawButton (rybky7b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky7_song==true then
					cdplaying=true
					rybky7 = love.audio.newSource( "/externalassets/music/classic/rybky07.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky07.ogg") 
					end
					currenttrackname=rybky7t
						currenttrackdesc=rybky7t2
					love.timer.sleep( 0.3 )
				
				elseif rybky7_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
					
			--rybky9
			
			if rybky9_song==false then love.graphics.draw (locksymbol, rybky9b.x+150,rybky9b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky9b)
			if rybky9_song==true then drawButton (rybky9b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky9_song==true then
					cdplaying=true
					rybky9 = love.audio.newSource( "/externalassets/music/classic/rybky09.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky09.ogg") 
					end
					currenttrackname=rybky9t
						currenttrackdesc=rybky9t2
					love.timer.sleep( 0.3 )
				
				elseif rybky9_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky10
			
				if rybky10_song==false then love.graphics.draw (locksymbol, rybky10b.x+150,rybky10b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky10b)
			if rybky10_song==true then drawButton (rybky10b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky10_song==true then
					cdplaying=true
					rybky10 = love.audio.newSource( "/externalassets/music/classic/rybky10.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky10.ogg") 
					end
					currenttrackname=rybky10t
						currenttrackdesc=rybky10t2
					love.timer.sleep( 0.3 )
				
				elseif rybky10_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky11
			
				if rybky11_song==false then love.graphics.draw (locksymbol, rybky11b.x+150,rybky11b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky11b)
			if rybky11_song==true then drawButton (rybky11b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky11_song==true then
					cdplaying=true
					rybky11 = love.audio.newSource( "/externalassets/music/classic/rybky11.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky11.ogg") 
					end
					currenttrackname=rybky11t
						currenttrackdesc=rybky11t2
					love.timer.sleep( 0.3 )
				
				elseif rybky11_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky13
			
				if rybky13_song==false then love.graphics.draw (locksymbol, rybky13b.x+150,rybky13b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky13b)
			if rybky13_song==true then drawButton (rybky13b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky13_song==true then
					cdplaying=true
					rybky13 = love.audio.newSource( "/externalassets/music/classic/rybky13.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky13.ogg") 
					end
					currenttrackname=rybky13t
						currenttrackdesc=rybky13t2
					love.timer.sleep( 0.3 )
				
				elseif rybky13_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky14
			
				if rybky14_song==false then love.graphics.draw (locksymbol, rybky14b.x+150,rybky14b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky14b)
			if rybky14_song==true then drawButton (rybky14b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky14_song==true then
					cdplaying=true
					rybky14 = love.audio.newSource( "/externalassets/music/classic/rybky14.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky14.ogg") 
					end
					currenttrackname=rybky14t
						currenttrackdesc=rybky14t2
					love.timer.sleep( 0.3 )
				
				elseif rybky14_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			--rybky15
			
				if rybky15_song==false then love.graphics.draw (locksymbol, rybky15b.x+150,rybky15b.y,0,0.1,0.1) end	--locked song

			local hovered = isButtonHovered (rybky15b)
			if rybky15_song==true then drawButton (rybky15b, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if rybky15_song==true then
					cdplaying=true
					rybky15 = love.audio.newSource( "/externalassets/music/classic/rybky15.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/classic/rybky15.ogg") 
					end
					currenttrackname=rybky15t
						currenttrackdesc=rybky15t2
					love.timer.sleep( 0.3 )
				
				elseif rybky15_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			end


		if author=="pixelsphere" then
			
	
			if icyrealm_song==false then love.graphics.draw (locksymbol, icyrealmb.x+150,icyrealmb.y,0,0.1,0.1) end		--locked song
		
			local hovered = isButtonHovered (icyrealmb)
			if icyrealm_song==true then drawButton (icyrealmb, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if icyrealm_song==true then
					cdplaying=true
					icyrealm = love.audio.newSource( "/externalassets/music/pixelsphere/019_seven_and_eight_7-8_combined.ogg","stream" )
					if musicison==true then love.audio.stop() --love.audio.play( icyrealm  ) 		
						equalizerload("/externalassets/music/pixelsphere/019_seven_and_eight_7-8_combinedloop.ogg") 
					end
					currenttrackname=pixelsevenandeight
					currenttrackdesc=pixelsevenandeight2
					love.timer.sleep( 0.3 )
				
				elseif icyrealm_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
	
		if anotheraugust_song==false then love.graphics.draw (locksymbol, anotheraugustb.x+150,anotheraugustb.y,0,0.1,0.1) end	--locked song
			local hovered = isButtonHovered (anotheraugustb)
			if anotheraugust_song==true then drawButton (anotheraugustb, hovered) end												--unlocked song
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if anotheraugust_song==true then
					cdplaying=true
					anotheraugust = love.audio.newSource( "/externalassets/music/pixelsphere/013_Another_August.ogg","stream" )
					if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/013_Another_August.ogg") end
					currenttrackname=pixelsevenandeight currenttrackdesc=pixelsevenandeight2
					love.timer.sleep( 0.3 )
					
				elseif anotheraugust_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
	
	if novemembersnow_song==false then love.graphics.draw (locksymbol, novembersnowb.x+150,novembersnowb.y,0,0.1,0.1) end				--locked song
		
		local hovered = isButtonHovered (novembersnowb)
		if novemembersnow_song==true then drawButton (novembersnowb, hovered)	end														--unlocked song
		
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			if novemembersnow_song==true then
				cdplaying=true
				novembersnow = love.audio.newSource( "/externalassets/music/pixelsphere/155 November_snow-33_tape_leveled.ogg","stream" )
				if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/155 November_snow-33_tape_leveled.ogg") end
					currenttrackname=pixelnovembersnow
					currenttrackdesc=pixelnovembersnow2
					love.timer.sleep( 0.3 )
			elseif icyrealm_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
			end
		end
	
	
	if ambientI_song==false then love.graphics.draw (locksymbol, synthwave421kb.x+150,synthwave421kb.y,0,0.1,0.1) end				--locked song
	local hovered = isButtonHovered (synthwave421kb)
	if ambientI_song==true then drawButton (synthwave421kb, hovered) end															--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if ambientI_song==true then
			cdplaying=true
			synthwave421k = love.audio.newSource( "/externalassets/music/pixelsphere/007_Synthwave_421k.ogg","stream" )
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/007_Synthwave_421k.ogg") end
				currenttrackname=pixelcalmrelax
				currenttrackdesc=pixelcalmrelax2
				love.timer.sleep( 0.3 )
		elseif ambientI_song==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	
	if song18_song==false then love.graphics.draw (locksymbol, synthwave4kb.x+150,synthwave4kb.y,0,0.1,0.1) end						--locked song
	local hovered = isButtonHovered (synthwave4kb)
	if song18_song==true then drawButton (synthwave4kb, hovered) end																--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if song18_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/001_Synthwave_4k.ogg") end
				currenttrackname=pixelcalmambientI
				currenttrackdesc=pixelcalmambientI2
				love.timer.sleep( 0.3 )
		elseif song18_song==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	if aquaria_song==false then love.graphics.draw (locksymbol, aquariab.x+150,aquariab.y,0,0.1,0.1) end							--locked song
	local hovered = isButtonHovered (aquariab)
	if aquaria_song==true then drawButton (aquariab, hovered) end																	--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if aquaria_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/TylerSong3_Normal_loop.ogg") end
			currenttrackname=pixelaquaria
			currenttrackdesc=pixelaquaria2
			love.timer.sleep( 0.3 )
		elseif aquaria_song==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end
	
	if ambientII_song==false then love.graphics.draw (locksymbol, synthwave15kb.x+150,synthwave15kb.y,0,0.1,0.1) end				--locked song
	local hovered = isButtonHovered (synthwave15kb)
	if ambientII_song==true then drawButton (synthwave15kb, hovered) end															--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if ambientII_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/002_Synthwave_15k.ogg") end
				currenttrackname=pixelcalmambientII
				currenttrackdesc=pixelcalmambientII2
				love.timer.sleep( 0.3 )
		elseif ambientII_song==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end
	
	
	if ambientIII_song==false then love.graphics.draw (locksymbol, lifewave2kb.x+150,lifewave2kb.y,0,0.1,0.1) end					--locked song
	local hovered = isButtonHovered (lifewave2kb)
	
	if ambientIII_song==true then drawButton (lifewave2kb, hovered) end																--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if ambientIII_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/006_lifeWave2k.ogg") end
				currenttrackname=pixelcalmambientIII
				currenttrackdesc=pixelcalmambientIII2
				love.timer.sleep( 0.3 )
		elseif ambientIII_song==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	if vapor_song==false then love.graphics.draw (locksymbol, vaporwareb.x+150,vaporwareb.y,0,0.1,0.1) end							--locked song
	local hovered = isButtonHovered (vaporwareb)
	if vapor_song==true then drawButton (vaporwareb, hovered) end																	--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if vapor_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/003_Vaporwareloop.ogg") end
				currenttrackname=pixelvapor
				currenttrackdesc=pixelvapor2
				love.timer.sleep( 0.3 )
		elseif vapor_song==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	if sirens_song==false then love.graphics.draw (locksymbol, sirensindarknessb.x+150,sirensindarknessb.y,0,0.1,0.1) end			--locked song
	local hovered = isButtonHovered (sirensindarknessb)
	if sirens_song==true then drawButton (sirensindarknessb, hovered) end															--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if sirens_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/012_Sirens_in_Darkness.ogg") end
				currenttrackname=pixelsirens
				currenttrackdesc=pixelsirens2
				love.timer.sleep( 0.3 )
		elseif sirens_song==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	if song18_song==false then love.graphics.draw (locksymbol, song18b.x+150,song18b.y,0,0.1,0.1) end								--locked song
	local hovered = isButtonHovered (song18b)
	if song18_song==true then drawButton (song18b, hovered) end																		--unlocked song
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if song18_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/song18loop.ogg") end
			currenttrackname=pixelsong18
			currenttrackdesc=pixelsong182
			love.timer.sleep( 0.3 )
		elseif song18_song==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end

	if song21_song==false then love.graphics.draw (locksymbol, song21b.x+150,song21b.y,0,0.1,0.1) end								--locked song
	local hovered = isButtonHovered (song21b)
	if song21_song==true then drawButton (song21b, hovered) end																		--unlocked song
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if song21_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/song21loop.ogg")	end
			currenttrackname=pixelsong21
			currenttrackdesc=pixelsong212
			love.timer.sleep( 0.3 )
		elseif song21==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
		end
	end

	if entchanted_song==false then love.graphics.draw (locksymbol, enchantedtikib.x+150,enchantedtikib.y,0,0.1,0.1) end				--locked song
	local hovered = isButtonHovered (enchantedtikib)
	
	if entchanted_song==true then drawButton (enchantedtikib, hovered) end															--unlocked song
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if entchanted_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/enchanted_tiki_86.ogg") end
				currenttrackname=pixelenchanted
				currenttrackdesc=pixelenchanted2
				love.timer.sleep( 0.3 )
			elseif enchanted_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
			end
		end
		
	if happylullaby_song==false then love.graphics.draw (locksymbol, happylullabyb.x+150,happylullabyb.y,0,0.1,0.1) end				--locked song
	local hovered = isButtonHovered (happylullabyb)
	
	if happylullaby_song==true then drawButton (happylullabyb, hovered) end															--unlocked song
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if happylullaby_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/song17.ogg") end
				currenttrackname=pixelHappyLullaby
				currenttrackdesc=pixelHappyLullaby2
				love.timer.sleep( 0.3 )
			elseif enchanted_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
			end
		end
		
			if thehex_song==false then love.graphics.draw (locksymbol, thehexb.x+150,thehexb.y,0,0.1,0.1) end				--locked song
	local hovered = isButtonHovered (thehexb)
	
	if thehex_song==true then drawButton (thehexb, hovered) end															--unlocked song
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if thehex_song==true then
			cdplaying=true
			if musicison==true then love.audio.stop() equalizerload("/externalassets/music/pixelsphere/the_hex_09.ogg") end
				currenttrackname=pixelTheHex
				currenttrackdesc=pixelTheHex2
				love.timer.sleep( 0.3 )
			elseif enchanted_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
			end
		end									
											--- isaiah658 ---
		end									
		if author=="isaiah658" then
			
					
			
			if ambient_song==false then love.graphics.draw (locksymbol, ambientrelaxingloopb.x+150,ambientrelaxingloopb.y,0,0.1,0.1) end
			
			local hovered = isButtonHovered (ambientrelaxingloopb)
			
			if ambient_song==true then drawButton (ambientrelaxingloopb, hovered) end
			
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if ambient_song==true then
					cdplaying=true
					if musicison==true then love.audio.stop() equalizerload("/externalassets/music/isaiah658/Ambient_Loop_isaiah658loop.ogg") end
					currenttrackname=isaiahambient
					currenttrackdesc=isaiahambient2
					love.timer.sleep( 0.3 )
				elseif ambient_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			if ambient_song==false then love.graphics.draw (locksymbol, underwaterambientpadb.x+150,underwaterambientpadb.y,0,0.1,0.1) end
			
			local hovered = isButtonHovered (underwaterambientpadb)
			if underwater2_song==true then drawButton (underwaterambientpadb, hovered) end
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if underwater2_song==true then
					cdplaying=true
					if musicison==true then love.audio.stop() equalizerload("/externalassets/music/isaiah658/Underwater-Ambient-Pad-isaiah658loop.ogg") end
					currenttrackname=isaiahunderwater
					currenttrackdesc=isaiahunderwater2
					love.timer.sleep( 0.3 )
				elseif underwater2_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
		end										--- umplix ---
		if author=="umplix" then
			

			if deepsea_song==false then love.graphics.draw (locksymbol, deepseab.x+150,deepseab.y,0,0.1,0.1) end
			local hovered = isButtonHovered (deepseab)
			if deepsea_song==true then drawButton (deepseab, hovered) end
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if deepsea_song==true then
					cdplaying=true
					if musicison==true then love.audio.stop() equalizerload("/externalassets/music/umplix/deep_sea.ogg") end
						currenttrackname=umplixdeepsea
						currenttrackdesc=umplixdeepsea2
						love.timer.sleep( 0.3 )
				elseif deepsea_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
			if sinkingfeeling_song==false then love.graphics.draw (locksymbol, sinkingfeelingb.x+150,sinkingfeelingb.y,0,0.1,0.1) end
			local hovered = isButtonHovered (sinkingfeelingb)
			if sinkingfeeling_song==true then drawButton (sinkingfeelingb, hovered) end
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				if sinkingfeeling_song==true then
					cdplaying=true
					if musicison==true then love.audio.stop() equalizerload("/externalassets/music/umplix/sinking_feeling_loop.ogg") end
						currenttrackname=umplixsinkingfeeling
						currenttrackdesc=umplixsinkingfeeling2
						love.timer.sleep( 0.3 )
					
				elseif sinkingfeeling_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
				end
			end
			
												--- Eric Matyas
		end
		if author=="ericmatyas" then
			
							
			if hyp_song==false then love.graphics.draw (locksymbol, hypnoticpuzzleb.x+150,hypnoticpuzzleb.y,0,0.1,0.1) end
		
			local hovered = isButtonHovered (hypnoticpuzzleb)
			if hyp_song==true then drawButton (hypnoticpuzzleb, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if hyp_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/EricMatyas/Hypnotic_Puzzle_loop.ogg") end
							currenttrackname=ericmatyashyp
							currenttrackdesc=ericmatyashyp2
							love.timer.sleep( 0.3 )
					elseif hyp_song==false then
					TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
					love.timer.sleep( 0.3 )
					end
				end
			
			if dream_song==false then love.graphics.draw (locksymbol, dreamingofreefsb.x+150,dreamingofreefsb.y,0,0.1,0.1) end

			local hovered = isButtonHovered (dreamingofreefsb)
			if dream_song==true then drawButton (dreamingofreefsb, hovered)	end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if dream_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() --love.audio.play( hypnoticpuzzle  ) 
							equalizerload("/externalassets/music/EricMatyas/Dreaming-of-Reefs_loop.ogg") 
						end
						currenttrackname=ericmatyasdream
						currenttrackdesc=ericmatyasdream2
						love.timer.sleep( 0.3 )
					elseif dream_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
			
			if islandofmysteries_song==false then love.graphics.draw (locksymbol, islandofmysteriesb.x+150,islandofmysteriesb.y,0,0.1,0.1) end
	
			local hovered = isButtonHovered (islandofmysteriesb)
			if islandofmysteries_song==true then drawButton (islandofmysteriesb, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if islandofmysteries_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/EricMatyas/island_of_mysteriesloop.ogg") end
						currenttrackname=ericmatyastislandofmysteries
						currenttrackdesc=ericmatyastislandofmysteries
						love.timer.sleep( 0.3 )
					elseif islandofmysteries_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
			
			if monkeyislandband_song==false then love.graphics.draw (locksymbol, monkeyislandbandb.x+150,monkeyislandbandb.y,0,0.1,0.1) end
			
			local hovered = isButtonHovered (monkeyislandbandb)
			if monkeyislandband_song==true then drawButton (monkeyislandbandb, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if monkeyislandband_song==true then
					cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
							currenttrackname=ericmatyastMonkeyIslandBand
							currenttrackdesc=ericmatyastMonkeyIslandBand2
							love.timer.sleep( 0.3 )
					elseif monkeyislandband_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
			
				if puzzlegame_song==false then love.graphics.draw (locksymbol, puzzlegame2b.x+150,puzzlegame2b.y,0,0.1,0.1) end
				local hovered = isButtonHovered (puzzlegame2b)
				if puzzlegame_song==true then drawButton (puzzlegame2b, hovered) end
				
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if puzzlegame_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/EricMatyas/PuzzleGame2.ogg") end
						currenttrackname=ericmatyastPuzzleGame 	currenttrackdesc=ericmatyastPuzzleGame2
						love.timer.sleep( 0.3 )
					elseif puzzlegame_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
			
					if theyarehere_song==false then love.graphics.draw (locksymbol, theyarehereb.x+150,theyarehereb.y,0,0.1,0.1) end
							local hovered = isButtonHovered (theyarehereb)
					if theyarehere_song==true then drawButton (theyarehereb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if theyarehere_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/EricMatyas/theyarehereloop.ogg")
							end
							currenttrackname=ericmatyastTheyarehere currenttrackdesc=ericmatyastTheyarehere2
							love.timer.sleep( 0.3 )
						elseif theyarehere_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
							
					end
			
									--- Cleyton Kauffman ---
			end
			if author=="cleytonkauffman" then
				
			
			if underwaterI_song==false then love.graphics.draw (locksymbol, underwaterthemeIb.x+150,underwaterthemeIb.y,0,0.1,0.1) end
			local hovered = isButtonHovered (underwaterthemeIb)
			if underwaterI_song==true then drawButton (underwaterthemeIb, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if underwaterI_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
						currenttrackname=cleytonkauffmanunderwaterI currenttrackdesc=cleytonkauffmanunderwaterI2
						love.timer.sleep( 0.3 )
						
					elseif underwaterI_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end	
				
			if underwaterII_song==false then
				love.graphics.draw (locksymbol, underwaterthemeIIb.x+150,underwaterthemeIIb.y,0,0.1,0.1) 
			end
						
			local hovered = isButtonHovered (underwaterthemeIIb)
			if underwaterII_song==true then
				drawButton (underwaterthemeIIb, hovered)
			end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if underwaterII_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/cleytonKauffman/Underwater_Theme_II_loop.ogg") end
							currenttrackname=cleytonkauffmanunderwaterII currenttrackdesc=cleytonkauffmanunderwaterII2
							love.timer.sleep( 0.3 )
					elseif underwaterII_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
				
								--- marcelog ---
			end						
				if author=="marcelofg55" then
					
					
					if airy_song==false then
						love.graphics.draw (locksymbol, airyambienceb.x+150,airyambienceb.y,0,0.1,0.1) 
					end
					
					local hovered = isButtonHovered (airyambienceb)
					if airy_song==true then
						drawButton (airyambienceb, hovered)
					end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if airy_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() equalizerload("/externalassets/music/marcelofg55/AiryAmbience.ogg") end
								currenttrackname=marcelofgairy
								currenttrackdesc=marcelofgairy2
								love.timer.sleep( 0.3 )
							
						elseif airy_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
								---  springyspringo  ---
				end	
					if author=="springyspringo" then
						
						
					if water_song==false then love.graphics.draw (locksymbol, waterambienceb.x+150,waterambienceb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (waterambienceb)
					if water_song==true then drawButton (waterambienceb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if water_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() equalizerload("/externalassets/music/SpringySpringo/thalassophobia_loop.ogg") end
							currenttrackname=springywater
							currenttrackdesc=springywater2
							love.timer.sleep( 0.3 )
						elseif water_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
							---  Tokyo Geisha  ---
						
					end	
					if author=="tokyogeisha" then
						
					
					if ambience_song==false then love.graphics.draw (locksymbol, dreamambienceb.x+150,dreamambienceb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (dreamambienceb)
					if ambience_song==true then drawButton (dreamambienceb, hovered) end
							
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if ambience_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() equalizerload("/externalassets/music/tokyogeisha/DreamAmbience.ogg") end
								currenttrackname=tokygeishaambience
								currenttrackdesc=tokygeishaambience2
								love.timer.sleep( 0.3 )
						elseif ambience_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
				if ambienceII_song==false then love.graphics.draw (locksymbol, dreamambience2b.x+150,dreamambience2b.y,0,0.1,0.1) end
				local hovered = isButtonHovered (dreamambience2b)
				if ambienceII_song==true then drawButton (dreamambience2b, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if ambienceII_song== true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/tokyogeisha/DreamAmbience2.ogg") end
							currenttrackname=tokygeishaambienceII
							currenttrackdesc=tokygeishaambienceII2
							love.timer.sleep( 0.3 )
					elseif ambienceII_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
				
				if creep_song==false then love.graphics.draw (locksymbol, creepb.x+150,creepb.y,0,0.1,0.1) end
				local hovered = isButtonHovered (creepb)
				if creep_song==true then drawButton (creepb, hovered) end
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					if creep_song==true then
						cdplaying=true
						if musicison==true then love.audio.stop() equalizerload("/externalassets/music/tokyogeisha/creep.ogg") end
							currenttrackname=tokygeishacreep
							currenttrackdesc=tokygeishacreep2
							love.timer.sleep( 0.3 )
					elseif creep_song==false then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
						love.timer.sleep( 0.3 )
					end
				end
			
						--- poinl ---
			end
				if author=="poinl" then
					
									
					if nautilus_song==false then love.graphics.draw (locksymbol, nautilusb.x+150,nautilusb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (nautilusb)
					if nautilus_song==true then drawButton (nautilusb, hovered) end
							
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if nautilus_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/poinl/ambient2_Nautilus.ogg") end
								currenttrackname=poinlnautilus
								currenttrackdesc=poinlnautilus2
								love.timer.sleep( 0.3 )
						elseif nautilus_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
					
						--- hectavex ---
					end
					if author=="hectavex" then
						
					
					if rupture_song==false then love.graphics.draw (locksymbol, hectavexrupture.x+150,hectavexrupture.y,0,0.1,0.1) end
					local hovered = isButtonHovered (hectavexrupture)
					if rupture_song==true then drawButton (hectavexrupture, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if rupture_song==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/hectavex/rupture.ogg") end
								currenttrackname=hectavexrupture
								currenttrackdesc=hectavexrupture2
								love.timer.sleep( 0.3 )
						elseif rupture_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
					if ova_song==false then love.graphics.draw (locksymbol, hectavexova.x+150,hectavexova.y,0,0.1,0.1) end
					local hovered = isButtonHovered (hectavexova)
					if ova_song==true then drawButton (hectavexova, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if ova_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/hectavex/ova.ogg") end
								currenttrackname=hectavexova
								currenttrackdesc=hectavexova2
								love.timer.sleep( 0.3 )
						elseif ova_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
				
					if vanish_song==false then love.graphics.draw (locksymbol, hectavexvanish.x+150,hectavexvanish.y,0,0.1,0.1) end
					local hovered = isButtonHovered (hectavexvanish)
					if vanish_song==true then drawButton (hectavexvanish, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if vanish_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/hectavex/vanish.ogg") end
								currenttrackname=hectavexvanish
								currenttrackdesc=hectavexvanish2
								love.timer.sleep( 0.3 )
						elseif vanish_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
			
					
									
										---	Tedkerr	---
		end
		if author=="tedkerr" then
					
		
		
					if crashedship_song==false then love.graphics.draw (locksymbol, tedkerrcrashedship.x+150,tedkerrcrashedship.y,0,0.1,0.1) end					
					local hovered = isButtonHovered (tedkerrcrashedship)
					if crashedship_song==true then drawButton (tedkerrcrashedship, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if crashedship_song==true then
								cdplaying=true
								if musicison==true then love.audio.stop() equalizerload("/externalassets/music/TedKerr/CrashedShip.ogg") end
									currenttrackname=tedkerrcrashedship
									currenttrackdesc=tedkerrcrashedship2
									love.timer.sleep( 0.3 )
						elseif crashedship_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
					if scifi_song==false then love.graphics.draw (locksymbol, tedkerrscifib.x+150,tedkerrscifib.y,0,0.1,0.1) end
					local hovered = isButtonHovered (tedkerrscifib)
					if scifi_song==true then drawButton (tedkerrscifib, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if scifi_song==true then
							cdplaying=true
							if musicison==true then love.audio.stop() equalizerload("/externalassets/music/TedKerr/scifiambient_loop.ogg") end
								currenttrackname=tedkerrscifi
								currenttrackdesc=tedkerrscifi2
								love.timer.sleep( 0.3 )
						elseif scifi_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
												--- isao ---
				end								
				if author=="isao" then
												
					
					if upbeat_song==false then love.graphics.draw (locksymbol, isaoupbeatb.x+150,isaoupbeatb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (isaoupbeatb)
					if upbeat_song==true then drawButton (isaoupbeatb, hovered) end 
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if upbeat_song==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/ISAo/dova_STARRY NEW HYPE_master_loop.ogg") end
								currenttrackname=isaoupbeat
								currenttrackdesc=isaoupbeat2
								love.timer.sleep( 0.3 )
						elseif upbeat_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
					if epicdance_song==false then love.graphics.draw (locksymbol, isaoepicb.x+150,isaoepicb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (isaoepicb)
					if epicdance_song==true then drawButton (isaoepicb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if epicdance_song==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/ISAo/dova_instavista_master_loop.ogg") end
								currenttrackname=isaoepicdance
								currenttrackdesc=isaoepicdance2
								love.timer.sleep( 0.3 )
						elseif epicdance_song==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
					
												--- Viktor Kraus ---
				end								
				if author=="viktorkraus" then
					
				
				if arobotswaytoheavenunlocked==false then love.graphics.draw (locksymbol, arobotswaytoheavenb.x+150,arobotswaytoheavenb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (arobotswaytoheavenb)
					if arobotswaytoheavenunlocked==true then drawButton (arobotswaytoheavenb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if arobotswaytoheavenunlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/ViktorKraus/ARobotswaytoHeaven.ogg") end
								currenttrackname=arobotswaytoheaven
								currenttrackdesc=arobotswaytoheaven2
								love.timer.sleep( 0.3 )
						elseif arobotswaytoheavenunlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end
				
				end
											--- Glitchapp ---
											
				if author=="glitchapp" then
					
				
				if hexhappylullabyunlocked==false then love.graphics.draw (locksymbol, hexhappylullabyb.x+150,hexhappylullabyb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (hexhappylullabyb)
					if hexhappylullabyunlocked==true then drawButton (hexhappylullabyb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if hexhappylullabyunlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/hexhappylullabymix.ogg") end
								currenttrackname=hexhappylullaby
								currenttrackdesc=hexhappylullaby2
								love.timer.sleep( 0.3 )
						elseif hexhappylullabyunlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end					
		
				if remix1unlocked==false then love.graphics.draw (locksymbol, remix1b.x+150,remix1b.y,0,0.1,0.1) end
					local hovered = isButtonHovered (remix1b)
					if remix1unlocked==true then drawButton (remix1b, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if remix1unlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/remix1.ogg") end
								currenttrackname=glitchappremix1
								currenttrackdesc=glitchappremix1_2
								love.timer.sleep( 0.3 )
						elseif remix1unlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end		
		
			if remix2unlocked==false then love.graphics.draw (locksymbol, remix2b.x+150,remix2b.y,0,0.1,0.1) end
					local hovered = isButtonHovered (remix2b)
					if remix2unlocked==true then drawButton (remix2b, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if remix2unlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/remix2.ogg") end
								currenttrackname=glitchappremix2
								currenttrackdesc=glitchappremix2_2
								love.timer.sleep( 0.3 )
						elseif remix2unlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end											
		
			if remix35unlocked==false then love.graphics.draw (locksymbol, remix35b.x+150,remix35b.y,0,0.1,0.1) end
					local hovered = isButtonHovered (remix35b)
					if remix35unlocked==true then drawButton (remix35b, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if remix35unlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/remix3.5.ogg") end
								currenttrackname=glitchappremix35
								currenttrackdesc=glitchappremix35_2
								love.timer.sleep( 0.3 )
						elseif remix35unlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end	
					
			if rupturemunlocked==false then love.graphics.draw (locksymbol, rupturemb.x+150,rupturemb.y,0,0.1,0.1) end
					local hovered = isButtonHovered (rupturemb)
					if rupturemunlocked==true then drawButton (rupturemb, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if rupturemunlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/rupturem1.ogg") end
								currenttrackname=glitchapprupturem1
								currenttrackdesc=glitchapprupturem1_2
								love.timer.sleep( 0.3 )
						elseif rupturemunlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end	
			
			if remix4unlocked==false then love.graphics.draw (locksymbol, remix4b.x+150,remix4b.y,0,0.1,0.1) end
					local hovered = isButtonHovered (remix4b)
					if remix4unlocked==true then drawButton (remix4b, hovered) end
					if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
						if remix4unlocked==true then 
							cdplaying=true
							if musicison==true then love.audio.stop() 
								equalizerload("/externalassets/music/mixes/remix4hectapoinl.ogg") end
								currenttrackname=glitchappremix4_1
								currenttrackdesc=glitchappremix4_2
								love.timer.sleep( 0.3 )
						elseif rupturemunlocked==false then
							TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
							love.timer.sleep( 0.3 )
						end
					end	
					
			local hovered = isButtonHovered (tylermixb)
			drawButton (tylermixb, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
					cdplaying=true
					if musicison==true then love.audio.stop() 
						equalizerload("/externalassets/music/mixes/TylerMix.ogg")
					 end
						currenttrackname=glitchappaquariaremix
						currenttrackdesc=glitchappaquariaremix2
						love.timer.sleep( 0.3 )
					love.timer.sleep( 0.3 )
			end	
	end
--[[
This code fragment contains an if statement that checks if the value of the author variable is equal to the string "none". If this condition is true, the code checks if the boolean variable classictracksunlocked is true. If it is, it checks if the mouse cursor is hovering over a button (classictracksb) using the isButtonHovered function, and then draws the button using the drawButton function. If the button is hovered over and the left mouse button is pressed (love.mouse.isDown(1)), the code plays a sound effect using the TEsound.play function, sleeps for 1 second using the love.timer.sleep function, and finally sets the value of the author variable to the string "classictracks".

The code then repeats this process for two other buttons (pixelsphereb and isaiah658b) if their respective boolean variables (pixelsphereunlocked and isaiah658unlocked) are true. If the condition in the initial if statement is not true (i.e., if author is not "none"), the code will skip over all of these button-related statements.
--]]			
	if author=="none" or (cdplaying==false) then
	
	if classictracksunlocked==true then
		local hovered = isButtonHovered (classictracksb)
		drawButton (classictracksb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="classictracks"
		end
	end

	if pixelsphereunlocked==true then
		local hovered = isButtonHovered (pixelsphereb)
		drawButton (pixelsphereb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="pixelsphere"
		end
	end
	
	if isaiah658unlocked==true then
		local hovered = isButtonHovered (isaiah658b)
		drawButton (isaiah658b, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="isaiah658"
		end
	end
	
	if umplixunlocked==true then
		local hovered = isButtonHovered (umplixb)
		drawButton (umplixb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="umplix"
		end
	end
	
	if ericmatyasunlocked==true then
		local hovered = isButtonHovered (ericmatyasb)
		drawButton (ericmatyasb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="ericmatyas"
		end
	end
	
	if cleytonkauffmanunlocked==true then
		local hovered = isButtonHovered (cleytonkauffmanb)
		drawButton (cleytonkauffmanb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="cleytonkauffman"
		end
	end
	
	if marcelofunlocked==true then
		local hovered = isButtonHovered (marcelofgb)
		drawButton (marcelofgb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="marcelofg55"
		end
	end
	
	if springyspringounlocked==true then
		local hovered = isButtonHovered (springyspringob)
		drawButton (springyspringob, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="springyspringo"
		end
	end
	
	if tokyogeishaunlocked==true then
		local hovered = isButtonHovered (tokyogeishab)
		drawButton (tokyogeishab, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="tokyogeisha"
		end
	end
	
	if poinlunlocked==true then
		local hovered = isButtonHovered (poinlb)
		drawButton (poinlb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="poinl"
		end
	end
	
	if hectavexunlocked==true then
		local hovered = isButtonHovered (hectavexb)
		drawButton (hectavexb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="hectavex"
		end
	end
	
	if tedkerrunlocked==true then
		local hovered = isButtonHovered (tedkerrb)
		drawButton (tedkerrb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="tedkerr"
		end
	end
	
	if isaounlocked==true then
		local hovered = isButtonHovered (isaob)
		drawButton (isaob, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="isao"
		end
	end
	
	if viktorkrausunlocked==true then
		local hovered = isButtonHovered (viktorkrausb)
		drawButton (viktorkrausb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="viktorkraus"
		end
	end
	
	if glitchappmixesunlocked==true then
		local hovered = isButtonHovered (glitchappb)
		drawButton (glitchappb, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_024.ogg","static")
			love.timer.sleep( 1 )
			author="glitchapp"
		end
	end

end
--[[
The first line uses the love.graphics.draw function to draw an image of a CD asset. The image is positioned at coordinates (400, 500) with a rotation angle of -cdangle. The last three arguments adjust the image's scale and origin point.

The following lines of code conditionally render text to the screen based on the value of author. If author is equal to the string "glitchapp", then several strings are printed to the screen using different font sizes and positions. The text includes glitchappt, currenttrackname, glitchappt2, and currenttrackdesc. These variables likely contain text descriptions or labels related to the current state of the game.
--]]

				
end

function drawrotatingcd(dt)

	love.graphics.draw(cdasset,400,500,-cdangle,0.3,0.3,712.5,712.5)

end


function drawAboutMusic()
    if aboutInfo == true then
        love.graphics.setColor(0.2, 0.2, 0.2, 0.8)
        love.graphics.rectangle("fill", 800, 350, 800, 50)
        love.graphics.rectangle("fill", 800, 400, 800, 600)
        love.graphics.setColor(1, 1, 1)
        love.graphics.rectangle("line", 800, 350, 800, 50)
        love.graphics.rectangle("line", 800, 400, 800, 600)
        love.graphics.print("About",  800, 360, 0, 1)

        local maxWidth = 800
        local lineHeight = love.graphics.getFont():getHeight()

        local paragraph = "Music player <br>" ..
                          "Copyright (C) 2023 Glitchapp <br>" ..
                          ". <br>" ..
                          "This program is free software: you can redistribute it and/or modify " ..
                          "it under the terms of the GNU General Public License as published by " ..
                          "the Free Software Foundation, either version 2 of the License, or " ..
                          "(at your option) any later version.<br>" ..
                          ". <br>" ..
                          "This program is distributed in the hope that it will be useful, " ..
                          "but WITHOUT ANY WARRANTY; without even the implied warranty of " ..
                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " ..
                          "GNU General Public License for more details." ..
                          ". <br>"

        -- Replace "<br>" with newline characters "\n"
        paragraph = string.gsub(paragraph, "<br>", "\n")

        -- Split the paragraph into lines respecting the maximum width
        local lines = {}
        local currentLine = ""
        for line in paragraph:gmatch("[^\n]+") do
            local words = {}
            for word in line:gmatch("%S+") do
                table.insert(words, word)
            end

            local testLine = currentLine
            for _, word in ipairs(words) do
                local testLineWidth = love.graphics.getFont():getWidth(testLine .. word .. " ")
                if testLineWidth <= maxWidth then
                    testLine = testLine .. word .. " "
                else
                    table.insert(lines, testLine)
                    testLine = word .. " "
                end
            end

            if testLine ~= "" then
                table.insert(lines, testLine)
            end
            currentLine = ""
        end

        local x, y = 800, 410
        for _, line in ipairs(lines) do
            love.graphics.print(line, x, y)
            y = y + lineHeight
        end
    end
    
    if aboutInfo==true then
     	local hovered = isButtonHovered (AboutMusicCloseButton)
				drawButton (AboutMusicCloseButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
						aboutInfo=false
						love.timer.sleep( 0.3 )	
					end
    
    	local hovered = isButtonHovered (AboutMusicHyperlinkButton)
				drawButton (AboutMusicHyperlinkButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
							love.system.openURL("https://codeberg.org/glitchapp/Music-Player")
						love.timer.sleep( 0.3 )	
					end
    end
end

