
function fishhouseendload()

fishhousegraph  = love.graphics.newImage("externalassets/ends/0fishhouse/fishhousegraph.png")

if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[
Fish house area completed

]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		fishhouseendtext=fishouseendtexten
		elseif language=="es" then  fishhouseendtext=fishhouseendtextes
		elseif language=="de" then  fishhouseendtext=fishhouseendtextde
		elseif language=="fr" then  fishhouseendtext=fishhouseendtextfr
		elseif language=="pl" then  fishhouseendtext=fishhouseendtextpl
		elseif language=="sv" then	fishhouseendtext=fishhouseendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function fishhouseendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end





function fishhouseenddraw(dt)
	
	--graphics
	if timer >0 then
			love.graphics.setColor(1,1,1,1*(timer/10)-1)
			love.graphics.draw (fishhousegraph, 0,0,0,1,1)

	end 
	
	--text
	love.graphics.setColor(1,1,1)
	
	love.graphics.setFont(font3)
	love.graphics.print(fishhouseendtext,300,textposition,0,1)

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

