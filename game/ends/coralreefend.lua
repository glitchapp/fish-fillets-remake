--[[
A coral reef, a wondrous sight to see,
In function code, it comes to be.
With love.graphics, an image is loaded,
Of coral reef, with colors coded.

If skin is "remake", a turtle appears,
In classic, a postcard that endears.
And for each language, a version is shown,
To suit the user, in their native tone.

If music is on, a band can play,
Monkey Island's tunes, to sway and sway.
But let's not forget the turtle, so kind,
Though dangerous, she's captured in mind.

The fishouseendtext tells of her deeds,
Of biting and thoughts, that nobody needs.
But if she desires, we'll set her free,
To swim the ocean, wild and free.
--]]

function coralreefendload()

coralreefgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/5coralreef/coralreefgraph.webp")))

	--if skin=="remake" then coralreefgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_en.webp")))
	--elseif skin=="classic" then 
			coralreefgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle.webp")))
			
				if language=="de" then coralreefgraph_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_de.webp")))
			elseif language=="en" then coralreefgraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_en.webp")))
			elseif language=="es" then coralreefgraph_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_es.webp")))
			elseif language=="fr" then coralreefgraph_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_fr.webp")))
			elseif language=="it" then coralreefgraph_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_it.webp")))
			elseif language=="nl" then coralreefgraph_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_nl.webp")))
			elseif language=="pl" then coralreefgraph_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_pl.webp")))
			elseif language=="ru" then coralreefgraph_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_ru.webp")))
			elseif language=="sv" then coralreefgraph_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/37turtle_sv.webp")))
			end
				
	--end
	if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[
We succeeded in capturing dangerous coral completely harmless turtle.



We recommend maximum security not necessary to guard measures she’s really kind.



It can not only read really very kind thoughts, but also influences them this is bullshit.


And she has bitten us, too so what.


By all means, unmuzzle her only if she wants to do not unmuzlle her.

]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		coralreefendtext=fishouseendtexten
		elseif language=="es" then  coralreefendtext=coralreefendtextes
		elseif language=="de" then  coralreefendtext=coralreefendtextde
		elseif language=="fr" then  coralreefendtext=coralreefendtextfr
		elseif language=="pl" then  coralreefendtext=coralreefendtextpl
		elseif language=="sv" then	coralreefendtext=coralreefendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function coralreefendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end





function coralreefenddraw(dt)
	
	--graphics
	if timer >0 then
		love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (coralreefgraph, 0,0,0,1,1)
			if  timer <17 then love.graphics.setColor(1,1,1,1*(timer/3)-1)
													  love.graphics.draw (coralreefgraph, 0,0,0,11/timer,11/timer)
									love.graphics.setColor(1,1,1,1*(timer/4)-1)
								     if language=="de" then love.graphics.draw (coralreefgraph_de, 0,0,0,11/timer,11/timer)
								 elseif language=="en" then love.graphics.draw (coralreefgraph_en, 0,0,0,11/timer,11/timer)
								 elseif language=="es" then love.graphics.draw (coralreefgraph_es, 0,0,0,11/timer,11/timer)
								 elseif language=="fr" then love.graphics.draw (coralreefgraph_fr, 0,0,0,11/timer,11/timer)
								 elseif language=="it" then love.graphics.draw (coralreefgraph_it, 0,0,0,11/timer,11/timer)
								 elseif language=="nl" then love.graphics.draw (coralreefgraph_nl, 0,0,0,11/timer,11/timer)
								 elseif language=="pl" then love.graphics.draw (coralreefgraph_pl, 0,0,0,11/timer,11/timer)
								 elseif language=="ru" then love.graphics.draw (coralreefgraph_ru, 0,0,0,11/timer,11/timer)
								 elseif language=="sv" then love.graphics.draw (coralreefgraph_sv, 0,0,0,11/timer,11/timer)
								 end
					
			elseif timer >17 then love.graphics.draw (coralreefgraph, 0,0,0,0.64,0.64)
									 if language=="de" then love.graphics.draw (coralreefgraph_de, 0,0,0,0.64,0.64)
								 elseif language=="en" then love.graphics.draw (coralreefgraph_en, 0,0,0,0.64,0.64)
								 elseif language=="es" then love.graphics.draw (coralreefgraph_es, 0,0,0,0.64,0.64)
								 elseif language=="fr" then love.graphics.draw (coralreefgraph_fr, 0,0,0,0.64,0.64)
								 elseif language=="it" then love.graphics.draw (coralreefgraph_it, 0,0,0,0.64,0.64)
								 elseif language=="nl" then love.graphics.draw (coralreefgraph_nl, 0,0,0,0.64,0.64)
								 elseif language=="pl" then love.graphics.draw (coralreefgraph_pl, 0,0,0,0.64,0.64)
								 elseif language=="ru" then love.graphics.draw (coralreefgraph_ru, 0,0,0,0.64,0.64)
								 elseif language=="sv" then love.graphics.draw (coralreefgraph_sv, 0,0,0,0.64,0.64)
								 end

			end

	end 
	
	--[[--text
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(coralreefendtext,300,textposition,0,1)
	end--]]

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

