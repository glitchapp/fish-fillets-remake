--[[
A function called secretcomputerendload,
With graphics and music, a story is told.
If musicison is set to be true,
The Monkey Island band will come through.

The skin can be classic or a remake,
And the images are loaded to make,
The secret computer and its readonly screen,
Or the city in the deep with its ominous gleam.

The text in English reveals a plot,
Of a criminal organization caught,
With a waterproof disk and shocking news,
Games have changed, and we must diffuse,

The panic that may ensue,
As we keep this secret from public view.
Dragon's Lair Plus, a game of the future,
New possibilities, new occupations to nurture.

21st-century gameplay, elegant and refined,
Where skills grow, and lives are left behind.
This function has a story to tell,
Of secrets and games that will cast a spell.
--]]

function secretcomputerendload()

if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 
		
		
		--if skin=="remake" then --cityinthedeepgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/3cityinthedeep/cityinthedeepgraph.jpg")
	--elseif skin=="classic" then 
			secretcomputergraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/70readonly.webp")))
				if language=="en" then secretcomputergraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/70readonly_en.webp")))
				end
	--end

secretcomputerendtexten=[[
"THE GREATEST EARTHQUAKE SINCE 1990"

"when Dragon’s Lair was issued, Rules for beginners, version 1.0."

"DRAGON’S LAIR PLUS"

"The world of role-playing games will soon change forever."

"Dragon’s Lair Plus is not only a new version."

"It’s a new game."

"Plenty of new possibilities. New occupations. New system of character creation, where, instead of your lives, your skills grow. New elegant mechanism of playing."

"A game for 21. century."

"GAMES HAVE CHANGED"

"This is what we get from the computer after inserting the waterproof diskette that was found during our investigation of secret criminal organisation."

"We are shocked. Do not let journalists know it."

"You must prevent the panic."

"And subscribe two copies for us."

]]

secretcomputerendtextfr=[[
]]

secretcomputerendtextde=[[
]]

secretcomputerendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
			if language=="en" then 
		secretcomputerendtext=secretcomputerendtexten
		elseif language=="es" then  secretcomputerendtext=secretcomputerendtextes
		elseif language=="de" then  secretcomputerendtext=secretcomputerendtextde
		elseif language=="fr" then  secretcomputerendtext=secretcomputerendtextfr
		elseif language=="pl" then  secretcomputerendtext=secretcomputerendtextpl
		elseif language=="sv" then	secretcomputerendtext=secretcomputerendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function secretcomputerendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function secretcomputerenddraw(dt)
	
--graphics
	if timer >0 then
	love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (secretcomputergraph, 0,0,0,1,1)
				if timer <17 then love.graphics.draw (secretcomputergraph, 0,0,0,11/timer,11/timer)
								 if language=="en" then love.graphics.draw (secretcomputergraph_en, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (secretcomputergraph, 0,0,0,0.64,0.64)
								 if language=="en" then love.graphics.draw (secretcomputergraph_en, 0,0,0,0.64,0.64)
								 end
			end

	end 
	
	
	Talkies.draw()
	
	--text
	--love.graphics.setColor(1,1,1)
	
	--love.graphics.setFont(font3)
	--love.graphics.print(secretcomputerendtext,300,textposition,0,1)

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

