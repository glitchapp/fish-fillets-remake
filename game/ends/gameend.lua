
function gameendload()

--gameendgraph  = love.graphics.newImage("externalassets/ends/5linux/gameendgraph.png")

		--if skin=="remake" then gameendgraph  = love.graphics.newImage("externalassets/classic/postcards/postend_en.png")
	--elseif skin=="classic" then 
			gameendgraph  = love.graphics.newImage("externalassets/classic/postcards/postend.png")
			
				if language=="de" then gameendgraph_de  = love.graphics.newImage("externalassets/classic/postcards/postend_de.png")
			elseif language=="en" then gameendgraph_en  = love.graphics.newImage("externalassets/classic/postcards/postend_en.png")
			elseif language=="es" then gameendgraph_es  = love.graphics.newImage("externalassets/classic/postcards/postend_es.png")
			elseif language=="fr" then gameendgraph_fr  = love.graphics.newImage("externalassets/classic/postcards/postend_fr.png")
			elseif language=="it" then gameendgraph_it  = love.graphics.newImage("externalassets/classic/postcards/postend_it.png")
			elseif language=="nl" then gameendgraph_nl  = love.graphics.newImage("externalassets/classic/postcards/postend_nl.png")
			elseif language=="pl" then gameendgraph_pl  = love.graphics.newImage("externalassets/classic/postcards/postend_pl.png")
			elseif language=="ru" then gameendgraph_ru  = love.graphics.newImage("externalassets/classic/postcards/postend_ru.png")
			elseif language=="sv" then gameendgraph_sv  = love.graphics.newImage("externalassets/classic/postcards/postend_sv.png")
			end
				
	--end
	if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[Good morning, fish!

Again, you didn’t disappoint us. General Committee decided to decorate you with the highest orders. They are made of milk chocolate. Due to confidentiality, eat them immediately.

BOSS

PS: I understand this little pld issue, but next time please tell me in advance, so that we can provide an adoption permission.

PPS: Tell me, where did you find such a good player that he managed it all? I wish he won the computer or at least some of the other prizes.
]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		gameendtext=gameendtexten
		elseif language=="es" then  gameendtext=gameendtextes
		elseif language=="de" then  gameendtext=gameendtextde
		elseif language=="fr" then  gameendtext=gameendtextfr
		elseif language=="pl" then  gameendtext=gameendtextpl
		elseif language=="sv" then	gameendtext=gameendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function gameendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function gameenddraw(dt)
	--graphics
	if timer >0 then
	love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (gameendgraph, 0,0,0,1,1)
			if timer <17 then love.graphics.draw (gameendgraph, 0,0,0,11/timer,11/timer)
								 if language=="en" then love.graphics.draw (gameendgraph_en, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (gameendgraph, 0,0,0,0.64,0.64)
								 if language=="en" then love.graphics.draw (gameendgraph_en, 0,0,0,0.64,0.64)
								 end
			end
	end 
	
	Talkies.draw()
	
	--[[--text
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(gameendtext,300,textposition,0,1)
	end--]]

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

