
--[[
English:

A function to load images and sounds,
For a game that sure astounds.
It checks if the skin is classic or remake,
And loads images for a shipwreck's sake.

If classic, it loads a postcard graphic,
And in the language specified, it's specific.
German, English, Spanish, and more,
To ensure players have what they're looking for.

If music is on, a source is created,
And Monkey Island Band is what's slated.
But the function does more than just that,
It also loads a text with secrets at.

The text speaks of an ocean god,
And crimes that he's caused abroad.
From moving continents to Tunguska's blast,
He's a villain that's not to be surpassed.

But fear not, for the captive is caught,
And a brand new game has been brought.
With the records of sea battles attached,
The player's adventure is sure to be unmatched.
--]]

function shipwrecksendload()
		--if skin=="remake" then shipwrecksgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/1shipwrecks/shipwrecksgraph.webp")))
	--elseif skin=="classic" then 
			shipwrecksgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods.webp")))
			
				if language=="de" then shipwrecks_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_de.webp")))
			elseif language=="en" then shipwrecks_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_en.webp")))
			elseif language=="es" then shipwrecks_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_es.webp")))
			elseif language=="fr" then shipwrecks_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_fr.webp")))
			elseif language=="it" then shipwrecks_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_it.webp")))
			elseif language=="nl" then shipwrecks_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_nl.webp")))
			elseif language=="pl" then shipwrecks_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_pl.webp")))
			elseif language=="ru" then shipwrecks_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_ru.webp")))
			elseif language=="sv" then shipwrecks_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/19gods_sv.webp")))
			end
			
	--end
if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

shipwrecksendtexten=[[
Occupation of the captive we are sending to you is an ocean god.
Except of the plane and ship disappearings (so called Sea Battle case)
he is responsible for the other crimes as well,
moving the continents (code name Run, continent, run)
and meteorite in Tunguzka (code Jumping Jack) are among them.
We managed to intervene just in the nick of time:
we have found a brand new unwrapped box 
with a table game called STAR WARS in the captive’s house.
You can find the records of his sea battles in the attachement.
]]

shipwrecksendtextfr=[[
]]

shipwrecksendtextde=[[
]]

shipwrecksendtextes=[[
shipwrecksendtextes=La profesión del prisionero que te estamos enviando es un dios del océano.
Excepto por las desapariciones del avión y de la nave (llamados el caso de la Batalla Marina)
él es responsable por los otros crímenes también.
Moviendo los continentes (nombre clave Corre, continente, corre)
y el meteorito en Tunguzka (nombre código Jack Saltador)
están entre éstos.
Conseguimos intervenir justo en el límite del tiempo:
encontramos una nueva caja desempaquetada con un juego de mesa llamado STAR WARS en la casa del prisionero.
Puedes encontrar los registros de sus batallas de mar en los adjuntos.
]]

silversshipendtextnl=[[
Het dagelijkse beroep van een arrestant die we naar jullie toe sturen is zeegod. Behalve voor de verdwijningen van de vliegtuigen en het schepen (de Zeeslag zaak) is hij ook verantwoordelijk voor andere misdaden, onder andere het verplaatsen van de continenten (codenaam “Ren, continentje, ren”) en de meteoor in Tunguzka (codenaam “Duveltje uit een Doosje”).
Het is ons op het nippertje gelukt om in te grijpen: we hebben in het huis van de verdachte een gloednieuwe, zojuist uitgepakte doos gevonden met een bordspel genaamd STAR WARS.
Voor details over zijn zeeslagen, zie het bijvoegsel.
]]

silversshipendtextpl=[[
Jeniec, którego wam przesyłamy, jest bogiem oceanu. Poza zaginionymi statkami i samolotami (tzw. sprawa Statków), jest odpowiedzialny za inne przestępstwa, między innymi wędrówkę kontynentów (kryptonim Lądy Przez Morza) i Meteoryt Tunguski (kryptonim Hop Siup).
Zdążyliśmy z naszą interwencją w ostatniej chwili. W domu zatrzymanego odkryliśmy nowiutkie, jeszcze nie rozpakowane pudełko zawierające grę planszową o nazwie Gwiezdne Wojny.
W załączeniu przesyłamy zapis rozegranych przez niego partii Statków.
--]]

silversshipendtextit=[[
L'occupazione abituale del prigioniero che vi stiamo mandando è fare la divinità oceanica. Oltre alle sparizioni di navi ed aerei (il cosiddetto caso Battaglia Navale), è responsabile di altri crimini, della deriva dei continenti (nome in codice Corri, continente, corri) e del meteorite della Tunguska (nome in codice Saltapicchio) tra gli altri.
Siamo riusciti ad intervenire appena in tempo: nell'alloggio del prigioniero abbiamo trovato una scatola nuova non ancora aperta contenente un gioco da tavolo chiamato GUERRE STELLARI.
Troverete il rapporto completo delle sue battaglie navali in allegato.
]]

	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		shipwrecksendtext=shipwrecksendtexten
		elseif language=="es" then  shipwrecksendtext=shipwrecksendtextes
		elseif language=="de" then  shipwrecksendtext=shipwrecksendtextde
		elseif language=="fr" then  shipwrecksendtext=shipwrecksendtextfr
		elseif language=="pl" then  shipwrecksendtext=shipwrecksendtextpl
		elseif language=="sv" then	shipwrecksendtext=shipwrecksendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function shipwrecksendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function shipwrecksenddraw(dt)
		
	--graphics
	if timer >0 then
			love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (shipwrecksgraph, 0,0,0,1,1)
				if timer <17 then love.graphics.draw (shipwrecksgraph, 0,0,0,11/timer,11/timer)
			
									 if language=="de" then love.graphics.draw (shipwrecks_de, 0,0,0,11/timer,11/timer)
								 elseif language=="en" then love.graphics.draw (shipwrecks_en, 0,0,0,11/timer,11/timer)
								 elseif language=="es" then love.graphics.draw (shipwrecks_es, 0,0,0,11/timer,11/timer)
								 elseif language=="fr" then love.graphics.draw (shipwrecks_fr, 0,0,0,11/timer,11/timer)
								 elseif language=="it" then love.graphics.draw (shipwrecks_it, 0,0,0,11/timer,11/timer)
								 elseif language=="nl" then love.graphics.draw (shipwrecks_nl, 0,0,0,11/timer,11/timer)
								 elseif language=="pl" then love.graphics.draw (shipwrecks_pl, 0,0,0,11/timer,11/timer)
								 elseif language=="ru" then love.graphics.draw (shipwrecks_ru, 0,0,0,11/timer,11/timer)
								 elseif language=="sv" then love.graphics.draw (shipwrecks_sv, 0,0,0,11/timer,11/timer)
								 end
			
							
							
			elseif timer >17 then love.graphics.draw (shipwrecksgraph, 0,0,0,0.64,0.64)
									
									 if language=="de" then love.graphics.draw (shipwrecks_de, 0,0,0,0.64,0.64)
								 elseif language=="en" then love.graphics.draw (shipwrecks_en, 0,0,0,0.64,0.64)
								 elseif language=="es" then love.graphics.draw (shipwrecks_es, 0,0,0,0.64,0.64)
								 elseif language=="fr" then love.graphics.draw (shipwrecks_fr, 0,0,0,0.64,0.64)
								 elseif language=="it" then love.graphics.draw (shipwrecks_it, 0,0,0,0.64,0.64)
								 elseif language=="nl" then love.graphics.draw (shipwrecks_nl, 0,0,0,0.64,0.64)
								 elseif language=="pl" then love.graphics.draw (shipwrecks_pl, 0,0,0,0.64,0.64)
								 elseif language=="ru" then love.graphics.draw (shipwrecks_ru, 0,0,0,0.64,0.64)
								 elseif language=="sv" then love.graphics.draw (shipwrecks_sv, 0,0,0,0.64,0.64)
								end
			end
	end 
	
	--text
	--[[if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(shipwrecksendtext,300,textposition,0,1)
	end--]]
Talkies.draw()

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

