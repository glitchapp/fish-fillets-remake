--[[
In this code we see a function called linuxendload(),
Which loads images of Linux OS to be shown.
If the skin is "remake", a different image is loaded,
Else if it's "classic", a different image is coded.

The language selected affects the image that's displayed,
Different webp files for each language are made.
If music is on, a Monkey Island Band track can play,
But the volume is set at 0.2, not too loud, some say.

The fishouseendtexten variable stores some text,
About Linux users who at first seemed perplexed.
But they eventually united and agreed,
That they're all part of one big family indeed.
--]]

function linuxendload()

--linuxgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/5linux/linuxgraph.webp")))

		--if skin=="remake" then linuxgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_en.webp")))
	--elseif skin=="classic" then 
			linuxgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux.webp")))
			
				if language=="de" then linuxgraph_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_de.webp")))
			elseif language=="en" then linuxgraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_en.webp")))
			elseif language=="es" then linuxgraph_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_es.webp")))
			elseif language=="fr" then linuxgraph_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_fr.webp")))
			elseif language=="it" then linuxgraph_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_it.webp")))
			elseif language=="nl" then linuxgraph_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_nl.webp")))
			elseif language=="pl" then linuxgraph_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_pl.webp")))
			elseif language=="ru" then linuxgraph_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_ru.webp")))
			elseif language=="sv" then linuxgraph_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/77linux_sv.webp")))
			end
				
	--end
	if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[When we saw the linux users we didn’t know what we should do with them.
We didn’t understand why we got them and it seemed they would never agree with each other.
But finally we got them to calm down, by confronting them with a windows user.
From then on they kept the motto: Gentoo or Mandriva, we’re all one family.
"Although you didn’t solve any problem which we assigned to you 
I wouldn’t have left them in one game together either and they are quite useful here.
]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		linuxendtext=linuxendtexten
		elseif language=="es" then  linuxendtext=linuxendtextes
		elseif language=="de" then  linuxendtext=linuxendtextde
		elseif language=="fr" then  linuxendtext=linuxendtextfr
		elseif language=="pl" then  linuxendtext=linuxendtextpl
		elseif language=="sv" then	linuxendtext=linuxendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function linuxendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function linuxenddraw(dt)
	--graphics
	if timer >0 then
	love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (linuxgraph, 0,0,0,1,1)
				if timer <17 then love.graphics.draw (linuxgraph, 0,0,0,11/timer,11/timer)
								 if language=="en" then love.graphics.draw (linuxgraph_en, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (linuxgraph, 0,0,0,0.64,0.64)
								 if language=="en" then love.graphics.draw (linuxgraph_en, 0,0,0,0.64,0.64)
								 end
			end
	end 
	
	Talkies.draw()
	
	--text
	--[[if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(linuxendtext,300,textposition,0,1)
	end--]]

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

