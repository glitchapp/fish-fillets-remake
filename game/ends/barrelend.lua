--[[
In this code, Barrelendload is the name,
It loads an image with a certain aim,
If skin is "remake", then it will choose,
A different image to avoid confusion blues.

If skin is "classic", then it will load,
An image that is different than the first code,
Depending on the language, it will select,
An image to load and display without defect.

If musicison is true, then it will create,
A source of audio, that will elate,
The user with some background sound,
And set the volume to avoid it being too loud.

Finally, it loads a text with great zeal,
A message to the boss, that they feel,
It's great to be in the rubbish heap,
And swim in the sea, and irradiate deep.
--]]


function barrelendload()

--barrelgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/5coralreef/44barrel.webp")))

	--if skin=="remake" then barrelgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_en.webp")))
	--elseif skin=="classic" then 
			barrelgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel.webp")))
			
				if language=="de" then barrelgraph_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_de.webp")))
			elseif language=="en" then barrelgraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_en.webp")))
			elseif language=="es" then barrelgraph_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_es.webp")))
			elseif language=="fr" then barrelgraph_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_fr.webp")))
			elseif language=="it" then barrelgraph_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_it.webp")))
			elseif language=="nl" then barrelgraph_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_nl.webp")))
			elseif language=="pl" then barrelgraph_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_pl.webp")))
			elseif language=="ru" then barrelgraph_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_ru.webp")))
			elseif language=="sv" then barrelgraph_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/44barrel_sv.webp")))
			end
				
	--end
	if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

barrelendtexten=[["Greetings from the rubbish heap to our beloved boss."

"Yours sincerely"

"Agents"

"PS: It’s great to be here. We irradiate ourselves and swim in the sea a lot. We have many new friends."
]]

barrelendtextfr=[["Salutations depuis le tas d'immondices ŕ notre patron bien-aimé.")

"Vos trčs dévoués")

"Agents")

"PS: C'est formidable d'ętre ici. Nous nous sommes irradiés et nous nageons beaucoup. Nous avons beaucoup de nouveaux amis."

]]

barrelendtextde=[["Viele Grüße vom Müllhaufen an unseren geliebten Chef."

"Mit freundlichen Grüßen"

"Agenten"

"PS: Hier ist es großartig. Wir strahlen übers ganze Gesicht und schwimmen oft im Meer. Wir haben viele neue Freunde."

]]

barrelendtextsv=[["Hälsningar från skräphögen till vår älskade chef."

"Med vänliga hälsningar"

"Agenterna"

"PS: Det är toppen att vara här. Vi solar och badar i havet. Vi har mött många nya vänner."

]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
			if language=="en" then	barrelendtext=barrelendtexten
		elseif language=="es" then  barrelendtext=barrelendtextes
		elseif language=="de" then  barrelendtext=barrelendtextde
		elseif language=="fr" then  barrelendtext=barrelendtextfr
		elseif language=="pl" then  barrelendtext=barrelendtextpl
		elseif language=="sv" then	barrelendtext=barrelendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function barrelendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
end


function barrelenddraw(dt)
	
	--graphics
	if timer >0 then
		love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (barrelgraph, 0,0,0,1,1)
			if timer <17 then love.graphics.setColor(1,1,1,1*(timer/3)-1)
													  love.graphics.draw (barrelgraph, 0,0,0,9.5/timer,9.5/timer)
									love.graphics.setColor(1,1,1,1*(timer/4)-1)
								     if language=="de" then love.graphics.draw (barrelgraph_de, 0,0,0,11/timer,11/timer)
								 elseif language=="en" then love.graphics.draw (barrelgraph_en, 0,0,0,9.5/timer,9.5/timer)
								 elseif language=="es" then love.graphics.draw (barrelgraph_es, 0,0,0,11/timer,11/timer)
								 elseif language=="fr" then love.graphics.draw (barrelgraph_fr, 0,0,0,11/timer,11/timer)
								 elseif language=="it" then love.graphics.draw (barrelgraph_it, 0,0,0,11/timer,11/timer)
								 elseif language=="nl" then love.graphics.draw (barrelgraph_nl, 0,0,0,11/timer,11/timer)
								 elseif language=="pl" then love.graphics.draw (barrelgraph_pl, 0,0,0,11/timer,11/timer)
								 elseif language=="ru" then love.graphics.draw (barrelgraph_ru, 0,0,0,11/timer,11/timer)
								 elseif language=="sv" then love.graphics.draw (barrelgraph_sv, 0,0,0,11/timer,11/timer)
								 end
					
			elseif timer >17 then love.graphics.draw (barrelgraph, 0,0,0,0.54,0.54)
									 if language=="de" then love.graphics.draw (barrelgraph_de, 0,0,0,0.64,0.64)
								 elseif language=="en" then love.graphics.draw (barrelgraph_en, 0,0,0,0.54,0.54)
								 elseif language=="es" then love.graphics.draw (barrelgraph_es, 0,0,0,0.64,0.64)
								 elseif language=="fr" then love.graphics.draw (barrelgraph_fr, 0,0,0,0.64,0.64)
								 elseif language=="it" then love.graphics.draw (barrelgraph_it, 0,0,0,0.64,0.64)
								 elseif language=="nl" then love.graphics.draw (barrelgraph_nl, 0,0,0,0.64,0.64)
								 elseif language=="pl" then love.graphics.draw (barrelgraph_pl, 0,0,0,0.64,0.64)
								 elseif language=="ru" then love.graphics.draw (barrelgraph_ru, 0,0,0,0.64,0.64)
								 elseif language=="sv" then love.graphics.draw (barrelgraph_sv, 0,0,0,0.64,0.64)
								 end

			end

	end 
	
	--[[--text
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(barrelendtext,300,textposition,0,1)
	end--]]

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

