--[[
A function named "ufoendload",
Loads images to give the game more bode,
If the skin is "remake",
Multiple images it will take,
Else if it's "classic", a language-dependent image code.

A squirrel notion is the fishouseendtexten,
About interspace propulsion, talked about now and then,
UFO visits to Earth may be connected,
And squirrel kidnaps in certain areas detected,
The appalling truth now revealed, technical description included, amen.
--]]

function ufoendload()
	if skin=="remake" then
		ufograph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/4ufo/ufograph1denoised.webp")))
		ufographspheres  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/4ufo/ufograph1spheres.webp")))
		level3bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/level3bck1.webp")))
		level3bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/level3bck2.webp")))
		ufograph2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/4ufo/ufograph2.webp")))
		ufograph3  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/4ufo/ufograph3.webp")))
	
	elseif skin=="classic" then 
			ufograph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion.webp")))
				if language=="de" then ufograph_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_de.webp")))
			elseif language=="en" then ufograph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_en.webp")))
			elseif language=="es" then ufograph_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_es.webp")))
			elseif language=="fr" then ufograph_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_fr.webp")))
			elseif language=="it" then ufograph_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_it.webp")))
			elseif language=="nl" then ufograph_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_nl.webp")))
			elseif language=="pl" then ufograph_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_pl.webp")))
			elseif language=="ru" then ufograph_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_ru.webp")))
			elseif language=="sv" then ufograph_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/58propulsion_sv.webp")))
			end
				
	end
if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[

Squirrel


this is a notion that will be effective today widely talked about in the next few years


in connection with interspace propulsion.


This is also possible reason of UFO visits of the Earth.


Yes, a few cases of squirrel kidnaps were reported in past 5 years,


specially from Arizona, Utah and Southern Moravia,


but no one really took them seriously. Now we know the whole appalling truth.


Technical description is included."

]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		ufoendtext=fishouseendtexten
		elseif language=="es" then  ufoendtext=ufoendtextes
		elseif language=="de" then  ufoendtext=ufoendtextde
		elseif language=="fr" then  ufoendtext=ufoendtextfr
		elseif language=="pl" then  ufoendtext=ufoendtextpl
		elseif language=="sv" then	ufoendtext=ufoendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32
playerspeed=1

playufoButton = {
	text = playbuttontext,
	x = 800,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function ufoendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	playertime=timer*playerspeed
end




function ufoenddraw(dt)
	--print(playerspeed)
	--print(1.1-subbeat/5)

	if skin=="remake" then
		lovebpmdraw()
		--graphics
				if playertime >0 and playertime <23 then
					love.graphics.setColor(1,1,1,1*(playertime/10)-0.5)
					love.graphics.draw (level3bck2, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
					love.graphics.draw (level3bck, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
					
					love.graphics.setColor(1*(playertime/10)-1,1*(playertime/10)-2+(1.1-subbeat/2),1*(playertime/10)-2+(1.1-subbeat/2),1)
					love.graphics.draw (ufograph, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)
					love.graphics.draw (ufographspheres, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)
			elseif playertime >23 and playertime <30 then
					love.graphics.setColor(1,1,1,1)
					--water warp shader
					love.graphics.setCanvas(canvas)
					warpshader4:send("time", playertime)
					love.graphics.setShader(warpshader4)
					love.graphics.setCanvas()
					love.graphics.draw (level3bck2, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
					love.graphics.draw (level3bck, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
					love.graphics.setShader()
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,1)
						
					love.graphics.draw (ufograph, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)
					love.graphics.draw (ufographspheres, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)

		
			elseif playertime >30 and playertime <50 then
					love.graphics.setColor(1,1,1,4-playertime/10)
			
					love.graphics.draw (level3bck2, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
					love.graphics.draw (level3bck, 0-playertime*2,-500-playertime*6,0,0.4+playertime/240,0.4+playertime/240)
	
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,4-playertime/10)
						
					love.graphics.draw (ufograph, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)
					love.graphics.draw (ufographspheres, -playertime*10,-playertime*2,0,0.4+playertime/120,0.4+playertime/120)
	
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,-3+playertime/10)
					love.graphics.draw (ufograph2, -playertime*10,-playertime*8,0,0.25+playertime/120,0.25+playertime/120)
			
	
			elseif playertime >50 then
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,6-playertime/10)
					love.graphics.draw (ufograph2, -playertime*10,-playertime*8,0,0.25+playertime/120,0.25+playertime/120)
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,-5+playertime/10)
					love.graphics.draw (ufograph3, -1400+playertime*10,-500,0,0.25+(playertime/400),0.25+(playertime/400))
			end 
	
			if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
				textspeed=232
			else
				textspeed=32
			end
		elseif skin=="classic" and timer <17 then
			love.graphics.draw (ufograph, 0,0,0,11/timer,11/timer)
			love.graphics.setColor(1,1,1,1*(timer/2)-1)
			
					 if language=="de" then love.graphics.draw (ufograph_de, 0,0,0,11/timer,11/timer)
				 elseif language=="en" then love.graphics.draw (ufograph_en, 0,0,0,11/timer,11/timer)
				 elseif language=="es" then love.graphics.draw (ufograph_es, 0,0,0,11/timer,11/timer)
				 elseif language=="fr" then love.graphics.draw (ufograph_fr, 0,0,0,11/timer,11/timer)
				 elseif language=="it" then love.graphics.draw (ufograph_it, 0,0,0,11/timer,11/timer)
				 elseif language=="nl" then love.graphics.draw (ufograph_nl, 0,0,0,11/timer,11/timer)
				 elseif language=="pl" then love.graphics.draw (ufograph_pl, 0,0,0,11/timer,11/timer)
				 elseif language=="ru" then love.graphics.draw (ufograph_ru, 0,0,0,11/timer,11/timer)
				 elseif language=="sv" then love.graphics.draw (ufograph_sv, 0,0,0,11/timer,11/timer)
				 end
				 
		elseif skin=="classic" and timer >17 then love.graphics.draw (ufograph, 0,0,0,0.64,0.64)
					 if language=="de" then love.graphics.draw (ufograph_de, 0,0,0,0.64,0.64)
				 elseif language=="en" then love.graphics.draw (ufograph_en, 0,0,0,0.64,0.64)
				 elseif language=="es" then love.graphics.draw (ufograph_es, 0,0,0,0.64,0.64)
				 elseif language=="fr" then love.graphics.draw (ufograph_fr, 0,0,0,0.64,0.64)
				 elseif language=="it" then love.graphics.draw (ufograph_it, 0,0,0,0.64,0.64)
				 elseif language=="nl" then love.graphics.draw (ufograph_nl, 0,0,0,0.64,0.64)
				 elseif language=="pl" then love.graphics.draw (ufograph_pl, 0,0,0,0.64,0.64)
				 elseif language=="ru" then love.graphics.draw (ufograph_ru, 0,0,0,0.64,0.64)
				 elseif language=="sv" then love.graphics.draw (ufograph_sv, 0,0,0,0.64,0.64)
				 end
			end
		Talkies.draw()
end
function drawscenetext()
	
	local hovered = isButtonHovered (ReturnButton)
		drawButton (ReturnButton, hovered)
		if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
			--if soundon==true then end
				love.audio.stop()
				love.timer.sleep( 0.3 )
				levelload()
				gamestatus="levelselection"
		end
		
			local hovered = isButtonHovered (playufoButton)
	drawButton (playufoButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		if playerplaying==true then
			playerplaying=false
			love.timer.sleep( 0.3 )
		elseif playerplaying==false then
		 playerplaying=true 
		 love.timer.sleep( 0.3 )
		 end
	end
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)

	--drawsceneplayer()		--draw the media player interface

	--text
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		--love.graphics.print(ufoendtext,300,textposition,0,1)
	end

end


