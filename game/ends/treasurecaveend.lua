--[[
In treasure's cave, a function lies
Loaded with images for your eyes
If music is on, Monkey Island Band
Will play their tunes across the land

For the remake skin, a city in deep
A graph to view and forever keep
But for the classic skin, a grail
An image of treasure that shall never fail

And in the text, a warning they give
The enclosed material, holiness to live
Tests performed, with promising results
The holy grail, withstanding insults

Santa Claus and Holy Service, they say
Encountered in this treasure's cave
Thorough tests they recommend
To understand the power they apprehend.
--]]

function treasurecaveendload()

if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 
		
		
		--if skin=="remake" then --cityinthedeepgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/3cityinthedeep/cityinthedeepgraph.jpg")
	--elseif skin=="classic" then 
			treasurecavegraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/64grail.webp")))
				if language=="en" then treasurecavegraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/64grail_en.webp")))
				end
	--end

treasurecaveendtexten=[[
"We recommend subject the enclosed material to thorough tests of holiness. Due to the lack of equipment we performed only the basic tests and the results are more than promising."

"Bombing some of the grails with ultradirty words, we measured sometimes even 3 Santa Clauses, some of them withstanded the circumstances pressure of 0.8 jobs for the period of one minute!"

"It is also possible that we have encountered the whole Holy Service."

]]

treasurecaveendtextfr=[["Notre succès dans la capture de la dangereuse et entièrement inoffensive tortue corail est complet.")

"Nous recommandons la sécurité maximale pas forcément pour la garder, étant donné qu'elle est vraiment calme. Elle n'est même pas capable de lire la moindre pensée, alors les contrôler c'est tout bêtement n'importe quoi.")

"Et elle nous a mordu, d'où le paquet. De tout manière, détachez la uniquement si elle ne veut pas qu'on la détache.")

]]

treasurecaveendtextde=[["Wir haben erfolgreich die gefährliche Koralle komplett harmlose Schildkröte gefangen."

"Wir empfehlen höchste Sicherheitsvorkehrungen nicht nötig bei der Verwahrung, da sie sehr liebenswürdig ist. Sie kann nicht nur wirklich sehr nette Gedanken lesen, sondern sie auch beeinflussen das ist Blödsinn."

"Außerdem hat sie uns gebissen na und. Nehmen sie ihr den Maulkorb nur ab, wenn sie will unter keinen Umständen ab."

]]

treasurecaveendtextes=[[
"Tuvimos éxito en capturar peligrosamente a la completamente inofensiva tortuga de coral."

"Recomendamos la seguridad máxima no necesaria para prevenir medidas ella es realmente bondadosa. No sólamente puede leer muy hermosos pensamientos, sino que también los influencia esto es basura."

"Y ella nos ha mordido, también qué importa. Por todos los medios, sáquenle el bozal sólo si ella quiere que no se lo saquen."

]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
			if language=="en" then 
		treasurecaveendtext=treasurecaveendtexten
		elseif language=="es" then  treasurecaveendtext=treasurecaveendtextes
		elseif language=="de" then  treasurecaveendtext=treasurecaveendtextde
		elseif language=="fr" then  treasurecaveendtext=treasurecaveendtextfr
		elseif language=="pl" then  treasurecaveendtext=treasurecaveendtextpl
		elseif language=="sv" then	treasurecaveendtext=treasurecaveendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function treasurecaveendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function treasurecaveenddraw(dt)
	
--graphics
	if timer >0 then
	love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (treasurecavegraph, 0,0,0,1,1)
				if timer <17 then love.graphics.draw (treasurecavegraph, 0,0,0,11/timer,11/timer)
								 if language=="en" then love.graphics.draw (treasurecavegraph_en, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (treasurecavegraph, 0,0,0,0.64,0.64)
								 if language=="en" then love.graphics.draw (treasurecavegraph_en, 0,0,0,0.64,0.64)
								 end
			end

	end 
	
	
	Talkies.draw()
	
	--text
	--love.graphics.setColor(1,1,1)
	
	--love.graphics.setFont(font3)
	--love.graphics.print(treasurecaveendtext,300,textposition,0,1)

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

