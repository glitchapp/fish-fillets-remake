--[[
In this function, graphics load with ease,
City in the Deep, Atlantis in its decrees.
With skin set to "remake" or "classic" old,
Different images load, new or so we're told.

If music is on, it's Monkey Island's tune,
Playing in the background to set the mood.
But the fish house end text is the real deal,
A warning of the past, a story to reveal.

The sinking of the city was no mistake,
It was an artifact that caused the wake.
Called Atlantic Relief, misunderstood by all,
Depicting extraterrestrial visits, a wrong call.

Increase the surveillance of plugs, the message was sent,
To prevent similar catastrophes, a crucial event.
The city may be lost, but lessons were learned,
To protect what we have, we must stay concerned.
--]]

function cityinthedeependload()



	--if skin=="remake" then cityinthedeepgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/3cityinthedeep/cityinthedeepgraph.webp")))
	--elseif skin=="classic" then 
			cityinthedeepgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/29atlantis.webp")))
				if language=="en" then cityinthedeepgraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/29atlantis_en.webp")))
				end
	--end

if musicison==true then 
			--monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			--love.audio.play( monkeyislandband )
			--monkeyislandband:setVolume(0.2)
		end 

fishouseendtexten=[[
"We succeeded in discovering the right cause of sinking the city. 



It was a well known artifact called atlantic relief,



up to now considered by mistake to be a depiction of extraterrestrial visit,



that gave us a hint. 



At the same time, we recommend increasing the surveillance 



of the plugs on all continents and bigger islands



in order not to repeat similar awkward catastrophe again.")


]]

fishouseendtextfr=[[
]]

fishouseendtextde=[[
]]

fishouseendtextsv=[[
]]


	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		cityinthedeependtext=fishouseendtexten
		elseif language=="es" then  cityinthedeependtext=cityinthedeependtextes
		elseif language=="de" then  cityinthedeependtext=cityinthedeependtextde
		elseif language=="fr" then  cityinthedeependtext=cityinthedeependtextfr
		elseif language=="pl" then  cityinthedeependtext=cityinthedeependtextpl
		elseif language=="sv" then	cityinthedeependtext=cityinthedeependtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function cityinthedeependupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)
	
end




function cityinthedeependdraw(dt)
	
	--graphics
	if timer >0 then
	love.graphics.setColor(1,1,1,1*(timer/2)-1)
				--if skin=="remake" then love.graphics.draw (cityinthedeepgraph, 0,0,0,1,1)
			if timer <17 then love.graphics.draw (cityinthedeepgraph, 0,0,0,11/timer,11/timer)
								 if language=="en" then love.graphics.draw (cityinthedeepgraph_en, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (cityinthedeepgraph, 0,0,0,0.64,0.64)
								 if language=="en" then love.graphics.draw (cityinthedeepgraph_en, 0,0,0,0.64,0.64)
								 end
			end

	end 
	
	--[[--text
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(cityinthedeependtext,300,textposition,0,1)
	end
	--]]
Talkies.draw()
	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
		
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

