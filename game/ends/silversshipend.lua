--[[
English

In this code there lies a tale,
Of a ship called Silver's and a quest to prevail,
Images are loaded based on skin,
Language determines which to bring in.

Equalizer2 is required for sound,
Monkey Island Band can then be found,
If musicison is true, play the tune,
Volume is set to 0.2, not too soon.

Silversshipendtexten is a string,
Containing a story, let me sing,
Of a map that led to disappointment,
And a parrot with a failing appointment.

Captain Silver's treasure was the aim,
But the map only led to a parrot's domain,
With no memory left to recall,
The location of the treasure's haul.
--]]

--[[
Espanol: 

Tras navegar sin cesar en el mar,
el mapa de Capitán Silver pudimos encontrar.
Si el skin es "remake", la imagen será
la nave en un webp para poder cargar.

En caso de ser "classic", la imagen a mostrar
será la de un mapa, para el tesoro ubicar.
Y si el idioma es otro, también se cargará
la imagen en ese idioma, para poder cambiar.

Luego se usa un ecualizador, ¡qué sonido genial!
Y si la música está prendida, empieza el festival.
Se carga la canción en ogg, un formato ideal,
y al volumen le ponemos un toque personal.

Y por último, el texto en español,
cuenta de nuestra triste decepción.
El mapa no muestra la ubicación
del tesoro de plata, ¡qué desilusión!

Solo el lugar del último loro sobreviviente,
que tiene esclerosis, ¡qué inconveniente!
No puede recordar dónde está el tesoro brillante,
y con eso nuestra aventura termina de repente.
--]]

--[[
Deutsch:

Funktion Silversshipendload(), wie fein,
wird aufgerufen und lädt ein,
je nach Skin, ein and'res Bild,
das wird geladen, das ist mild.

Ist es der "Remake"-Skin, so fein,
kommt "Silversshipgraph" als Bild herein,
ansonsten gibt's ein and'res Werk,
wie die "51map", die ist stark und klerikal.

Doch nicht genug, es geht noch weiter,
für Sprachen gibt's 'ne eig'ne Leiter,
Deutsch, Englisch, Spanisch und Französisch,
sowie Italienisch, Niederländisch und Polnisch.

Doch das ist nicht alles, was passiert,
denn es wird auch Musik aktiviert,
wenn "musicison" als wahr erkannt,
dann wird Musik geladen und angestart'.

Der "Monkey Island Band" wird dann gehört,
wenn alles da ist und es nicht stört,
denn die Musik spielt auf und klingt,
wenn das Spiel geladen und gestartet wird, das stimmt.

Zum Schluss gibt's noch Text, der kommt daher,
in Deutsch geschrieben, das ist nicht schwer,
beschreibt, wie man Silvers' Karte fand,
doch dann war's ein Papagei, das ist bekannt.

Der Papagei leidet leider an Sklerose,
erinnert sich nicht an den Schatz, das ist die Dose,
doch das Spiel geht weiter, das ist klar,
wenn man es spielt, dann wird's wunderbar.
--]]

--[[
Francais:

Silvershipendload, un code à exécuter,
Qui charge des images pour nous montrer.
Si la peau est un remake, alors c'est clair,
Silvershipgraph est l'image à afficher.

Mais si la peau est classique à la place,
Alors la carte 51map prendra sa place.
Et si la langue est allemande,
Silvershipgraph_de sera l'image la plus attendue.

Si c'est l'anglais qui est sélectionné,
Silvershipgraph_en sera chargé.
Et si l'espagnol est choisi,
Silvershipgraph_es sera requis.

Pour la langue française, silversshipgraph_fr,
Pour l'italien, silversshipgraph_it est un must.
Et pour les Néerlandais, silversshipgraph_nl sera le mieux,
Pour le polonais, silversshipgraph_pl sera comme il se doit.

Le texte de fin de Silvership en français,
Nous raconte une histoire pleine de regrets.
La carte du capitaine Silver nous a menés à la déception,
Elle ne révélait pas le trésor, mais l'emplacement d'un perroquet en dépression.

Le pauvre volatile souffre de sclérose,
Et ne se souvient plus de l'emplacement de la chose.
Notre aventure s'achève ainsi sur une note triste,
Mais au moins, nous avons vu des paysages exquis.
--]]



function silversshipendload()

		--if skin=="remake" then silversshipgraph = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/2silversship/silversshipgraph.webp")))
	--elseif skin=="classic" then 
		silversshipgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map.webp")))
				if language=="de" then silversshipgraph_de  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_de.webp")))
			elseif language=="en" then silversshipgraph_en  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_en.webp")))
			elseif language=="es" then silversshipgraph_es  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_es.webp")))
			elseif language=="fr" then silversshipgraph_fr  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_fr.webp")))
			elseif language=="it" then silversshipgraph_it  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_it.webp")))
			elseif language=="nl" then silversshipgraph_nl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_nl.webp")))
			elseif language=="pl" then silversshipgraph_pl  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_pl.webp")))
			elseif language=="ru" then silversshipgraph_ru  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_ru.webp")))
			elseif language=="sv" then silversshipgraph_sv  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/postcards/51map_sv.webp")))
			end
	--end

require ("lib/equalizer/equalizer2")
if musicison==true then 
			monkeyislandband = love.audio.newSource( "externalassets/music/EricMatyas/Monkey_Island_Band.ogg","stream" )
			love.audio.play( monkeyislandband )
			monkeyislandband:setVolume(0.2)
			
		end 

silversshipendtexten=[[
After much hardship we succeeded in finding captain Silver’s map.
Initial enthusiasm afterwards changed into bitter disappointment, 
when we found out that the above mentioned map 
does not indicate the location of the treasure,
but the place of residence of the last surviving Silver’s parrot that, unfortunately,
suffers from sclerosis and cannot remember, where the treasure is.
]]

silversshipendtextfr=[[
Après de nombreuses épreuves, nous avons réussi à localiser la carte du capitaine Silver.
Notre enthousiasme du départ a vite tourné à l'amère déception,
quand nous avons réalisé que la susdite carte
n'indiquait pas l'emplacement du trésor,
mais le lieu de résidence du dernier perroquet survivant au capitaine Silver.
Malheureusement, celui-ci souffre de sclérose 
et est incapable de se souvenir où est enterré le trésor."
]]

silversshipendtextde=[[
Nach langer Mühe haben wir Kapitän Silvers Karte gefunden. 
Die anfängliche Begeisterung verkehrte sich schnell in bittere Enttäuschung,
nachdem wir herausgefunden hatten, 
dass auf der Karte nicht das Versteck des Schatzes markiert war,
sondern der Wohnsitz von Silvers letztem überlebenden Papagei,
der leider an Sklerose leidet
und sich nicht daran erinnern kann, wo der Schatz versteckt ist."
]]

silversshipendtextes=[[
Después de mucho navegar tuvimos éxito en encontrar el mapa de capitán Silver.
Pero después de todo, el entusiasmo inicial cambió a una profunda decepción,
cuando nos dimos cuenta que el mapa mencionado
no indica la localización del tesoro,
sinó el lugar de residencia del último perico sobreviviente del capitán Silver
el cual, desafortunadamente,
sufre de esclerosis y no puede recordar, donde está el tesoro.
]]

silversshipendtextsv=[[
„Efter mycket hårt arbete lyckade vi hitta kapten Silver karta. 
Den första entusiasmen efteråt förbyttes mot bitter besvikelse
när vi fann att tidigare nämnda kartan 
inte pekar ut var skatten finns
utan platsen där de sista överlevande av Silvers papegojor som,
oturligt nog,
lider av skleros och kan inte minnas var skatten finns.
]]

silversshipendtextnl=[[
Met veel moeite zijn we erin geslaagd de schatkaart van kapitein Silver te vinden. 
Ons enthousiasme veranderde al snel in teleurstelling toen we erachter kwamen dat de bovengenoemde kaart niet de positie van de schat aangeeft,
maar de positie van de enige nog levende papegaai van Silver.
Helaas lijdt het diertje aan sclerose en heeft het geen idee waar de schat is.
--]]
silversshipendtextit=[[
Dopo tanto impegno siamo riusciti a trovare la mappa di Cap. Silver.
L'entusiasmo iniziale si è trasformato in amara delusione, quando abbiamo scoperto che la succitata mappa non indica il luogo del tesoro,
bensì il luogo di residenza dell'ultimo pappagallo di Silver ancora in vita il quale, purtroppo, soffre di arteriosclerosi e non riesce a ricordare dove sia il tesoro.
--]]
	shader2=false
	
	
	   --briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		--if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic ) end
		if language=="en" then 
		
		silversshipendtext=silversshipendtexten
		elseif language=="es" then  silversshipendtext=silversshipendtextes
		elseif language=="de" then  silversshipendtext=silversshipendtextde
		elseif language=="fr" then  silversshipendtext=silversshipendtextfr
		elseif language=="pl" then  silversshipendtext=silversshipendtextpl
		elseif language=="sv" then	silversshipendtext=silversshipendtextsv
		end
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
	
textposition=900
textspeed = 32


ReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

ForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end

--function creditsbutton()
	
--end

function silversshipendupdate(dt)
	textposition = textposition - (textspeed * dt)  -- x will increase by 32 for every second right is held down
	Talkies.update(dt)

end




function silversshipenddraw(dt)

	--graphics
	if timer >0 then
			
				--if skin=="remake" then  love.graphics.setColor(1,1,1,1*(timer/10)-1)
										--love.graphics.draw (silversshipgraph, 0,0,0,1,1)
				if timer <17 then love.graphics.setColor(1,1,1,1*(timer/8)-1)
													  love.graphics.draw (silversshipgraph, 0,0,0,11/timer,11/timer)
								 love.graphics.setColor(1,1,1,1*(timer/2)-1)
									 if language=="de" then love.graphics.draw (silversshipgraph_de, 0,0,0,11/timer,11/timer)
								 elseif language=="en" then love.graphics.draw (silversshipgraph_en, 0,0,0,11/timer,11/timer)
								 elseif language=="es" then love.graphics.draw (silversshipgraph_es, 0,0,0,11/timer,11/timer)
								 elseif language=="fr" then love.graphics.draw (silversshipgraph_fr, 0,0,0,11/timer,11/timer)
								 elseif language=="it" then love.graphics.draw (silversshipgraph_it, 0,0,0,11/timer,11/timer)
								 elseif language=="nl" then love.graphics.draw (silversshipgraph_nl, 0,0,0,11/timer,11/timer)
								 elseif language=="pl" then love.graphics.draw (silversshipgraph_pl, 0,0,0,11/timer,11/timer)
								 elseif language=="ru" then love.graphics.draw (silversshipgraph_ru, 0,0,0,11/timer,11/timer)
								 elseif language=="sv" then love.graphics.draw (silversshipgraph_sv, 0,0,0,11/timer,11/timer)
								 end
			elseif timer >17 then love.graphics.draw (silversshipgraph, 0,0,0,0.64,0.64)
									 if language=="de" then love.graphics.draw (silversshipgraph_de, 0,0,0,0.64,0.64)
								 elseif language=="en" then love.graphics.draw (silversshipgraph_en, 0,0,0,0.64,0.64)
								 elseif language=="es" then love.graphics.draw (silversshipgraph_es, 0,0,0,0.64,0.64)
								 elseif language=="fr" then love.graphics.draw (silversshipgraph_fr, 0,0,0,0.64,0.64)
								 elseif language=="it" then love.graphics.draw (silversshipgraph_it, 0,0,0,0.64,0.64)
								 elseif language=="nl" then love.graphics.draw (silversshipgraph_nl, 0,0,0,0.64,0.64)
								 elseif language=="pl" then love.graphics.draw (silversshipgraph_pl, 0,0,0,0.64,0.64)
								 elseif language=="ru" then love.graphics.draw (silversshipgraph_ru, 0,0,0,0.64,0.64)
								 elseif language=="sv" then love.graphics.draw (silversshipgraph_sv, 0,0,0,0.64,0.64)
								 
								 end
			end

	end 
	
	--text
	--[[
	if skin=="remake" then
		love.graphics.setColor(1,1,1)
		love.graphics.setFont(font3)
		love.graphics.print(silversshipendtext,300,textposition,0,1)
	end--]]
Talkies.draw()

 --equalizerdraw(dt)

	
local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			love.audio.stop()
			--lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
			love.graphics.setBackgroundColor( 0,0,0,1 )
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (ForwardButton)
	drawButton (ForwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then 
		  textspeed=232
	else
	textspeed=32
	end

	--love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	
end

