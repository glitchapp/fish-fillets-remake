
--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

function musicequalizerload()

love.audio.setEffect( "myEqualizer", {type="equalizer"} )


-- Equalizer values
if volume==nil then volume=0.6 end
lowgain=1
lowmidgain=1
highmidgain=1
highgain=1

volumedownbutton = {
	text = "-",
	x = 2300,
	y = 1400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

volumeupbutton = {
	text = "+",
	x = 2300,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

--lowgain	
lowgaindownbutton = {
	text = "-",
	x = 2100,
	y = 1400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

lowgainupbutton = {
	text = "+",
	x = 2100,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

--lowmidgain
lowmidgaindownbutton = {
	text = "-",
	x = 2150,
	y = 1400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

lowmidgainupbutton = {
	text = "+",
	x = 2150,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

--highmidgain
highmidgaindownbutton = {
	text = "-",
	x = 2200,
	y = 1400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

highmidgainupbutton = {
	text = "+",
	x = 2200,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

--highgain
highgaindownbutton = {
	text = "-",
	x = 2250,
	y = 1400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

highgainupbutton = {
	text = "+",
	x = 2250,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

end

function drawvolumecontrols()

	-- Volume
			love.graphics.print( "Volume:", 2300, 1050, 0, 1 )
			love.graphics.print(volume,2350,1150,0, 0.8, 1) 
			love.graphics.rectangle( "fill", 2300, 1300, 25, volume*(-200))
			love.graphics.rectangle( "line", 2300, 1095, 25, 200 )
			--love.graphics.print("Volume: ",2400,1100,0, 0.8, 1) 
			

	if  cdplaying then
		-- Volume -
		local hovered = isButtonHovered (volumedownbutton)
		drawButton (volumedownbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
			volume=volume-0.1
			love.audio.setVolume( volume )
		love.timer.sleep( 0.2 )
		end
	
		-- Volume +
		local hovered = isButtonHovered (volumeupbutton)
		drawButton (volumeupbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
			volume=volume+0.1
			love.audio.setVolume( volume )
		love.timer.sleep( 0.2 )
		end
	end
end

function drawequalizer()
			--love.graphics.print( "Equalizer", 2000, 1000, 0, 1 )
		
			-- Equalizer
	
			--lowgain
			love.graphics.print( "Lowgain", 2100, 850, 0, 1 )
			love.graphics.print( "|", 2100, 900, 0, 1 )
			love.graphics.rectangle( "fill", 2100, 1300, 25, lowgain*(-200))
			love.graphics.rectangle( "line", 2100, 1095, 25, 200 )
			--lowmidgain
			love.graphics.print( "Lowhmidgain", 2150, 900, 0, 1 )
			love.graphics.print( "|", 2150, 950, 0, 1 )
			love.graphics.rectangle( "fill", 2150, 1300, 25, lowmidgain*(-200))
			love.graphics.rectangle( "line", 2150, 1095, 25, 200 )
			--highmidgai
			love.graphics.print( "Highmidgain", 2200, 950, 0, 1 )
			love.graphics.print( "|", 2200, 1000, 0, 1 )
			love.graphics.rectangle( "fill", 2200, 1300, 25, highmidgain*(-200))
			love.graphics.rectangle( "line", 2200, 1095, 25, 200 )
			--highgain
			love.graphics.print( "Highgain", 2250, 1000, 0, 1 )
			love.graphics.print( "|", 2250, 1050, 0, 1 )
			love.graphics.rectangle( "fill", 2250, 1300, 25, highgain*(-200))
			love.graphics.rectangle( "line", 2250, 1095, 25, 200 )
			

	if  cdplaying then

		-- Equalizer
		-- lowgain -
		local hovered = isButtonHovered (lowgaindownbutton)
		drawButton (lowgaindownbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		lowgain=lowgain-0.1
		love.timer.sleep( 0.2 )
		end
	
		-- lowgain +
		local hovered = isButtonHovered (lowgainupbutton)
		drawButton (lowgainupbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		lowgain=lowgain+0.1
		love.timer.sleep( 0.2 )
		end
		

		-- lowmidgain -
		local hovered = isButtonHovered (lowmidgaindownbutton)
		drawButton (lowmidgaindownbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		lowmidgain=lowmidgain-0.1
		love.timer.sleep( 0.2 )
		end
	
		-- lowmidgain +
		local hovered = isButtonHovered (lowmidgainupbutton)
		drawButton (lowmidgainupbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		lowmidgain=lowmidgain+0.1	
		love.timer.sleep( 0.2 )
		end

		-- highmidgain -
		local hovered = isButtonHovered (highmidgaindownbutton)
		drawButton (highmidgaindownbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		highmidgain=highmidgain-0.1
		love.timer.sleep( 0.2 )
		end
	
		-- highmidgain +
		local hovered = isButtonHovered (highmidgainupbutton)
		drawButton (highmidgainupbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		highmidgain=highmidgain+0.1
		love.timer.sleep( 0.2 )
		end
	
		-- highgain -
		local hovered = isButtonHovered (highgaindownbutton)
		drawButton (highgaindownbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		highgain=highgain-0.1
		love.timer.sleep( 0.2 )
		end
	
		-- highgain +
		local hovered = isButtonHovered (highgainupbutton)
		drawButton (highgainupbutton, hovered)
		if hovered and love.mouse.isDown(1) and cdplaying then
		highgain=highgain+0.1
		love.timer.sleep( 0.2 )
		end
		
	
	end

end
