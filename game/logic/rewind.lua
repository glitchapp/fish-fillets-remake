-- Rewind feature written

function loadRewindFeature()
    gameHistory = {} -- Clear the game history when loading a new level
    saveGameState(self) -- Save the initial state to the game history

-- Define gameState as a global variable
gameState = {
    agents = {},
    blocks = {}
}

-- Initialize gameHistory as a global variable
gameHistory = {}

end

-- Function to save the current state to the game history
function saveGameState(gameState)
    -- Create a copy of gameState to store in the history
    local state = {
        agents = {},
        blocks = {}
    }

    -- Save agent positions
    for _, agent in ipairs(gameState.agents) do
        table.insert(state.agents, {name = agent.name, x = agent.x, y = agent.y})
    end

    -- Save block positions
    for _, block in ipairs(gameState.blocks) do
        table.insert(state.blocks, {name = block.name, x = block.x, y = block.y})
    end

    table.insert(gameHistory, state) -- Add the current state to the history
end


-- Function to rewind the game by one step
function rewindGame()
    if #gameHistory > 0 then
        local state = table.remove(gameHistory) -- Remove the last state from history
        restoreGameState(state) -- Restore the game state
    end
end

-- Function to fast forward the game by one step
function fastForwardGame()
    -- Implement fast forward logic here
    -- You can add additional data to the game history to support fast forward
end


-- Function to restore the game state
function restoreGameState(gameState)
    -- Restore agent positions
    for i, agent in ipairs(gameState.agents) do
        local savedAgent = gameState.agents[i]
        savedAgent.name = agent.name
        savedAgent.x = agent.x
        savedAgent.y = agent.y
    end

    -- Restore block positions
    for i, block in ipairs(gameState.blocks) do
        local savedBlock = gameState.blocks[i]
        savedBlock.name = block.name
        savedBlock.x = block.x
        savedBlock.y = block.y
    end
end
