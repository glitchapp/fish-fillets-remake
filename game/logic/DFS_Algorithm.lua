-- Algorithm written by chatGPT.
--
-- Depth-first search (DFS) algorithm to find an exit in a maze.

local numRows = 0
local numCols = 0

function DFS_find_exit()
    numRows, numCols = DFS_maze_dimensions()
    findexit()
end

function DFS_maze_dimensions()
    -- Maze dimensions
    local rows = #level.map
    local cols = #level.map[1]
    return rows, cols
end

-- Function to check if a cell is valid (not a wall and within bounds)
local function isValidCell(row, col)
    return row >= 1 and row <= numRows and col >= 1 and col <= numCols and level.map[row][col] ~= 3
end


-- Depth-first search (DFS) algorithm to find an exit in a maze.

local numRows = 0
local numCols = 0

function DFS_find_exit()
    numRows, numCols = DFS_maze_dimensions()
    local exitFound = find_exit()
    
    if not exitFound then
        print("Exit not found!")
    end
end

function DFS_maze_dimensions()
    -- Maze dimensions
    local rows = #level.map
    local cols = #level.map[1]
    return rows, cols
end

-- Function to check if a cell is valid (not a wall and within bounds)
local function isValidCell(row, col)
    return row >= 1 and row <= numRows and col >= 1 and col <= numCols and level.map[row][col] ~= 3
end

-- DFS function to find the exit
local function dfs(row, col)
    if not isValidCell(row, col) or level.map[row][col] == 1 then
        return false
    end
    
    -- Mark the current cell as visited
    level.map[row][col] = 1
    
    print("Visiting cell:", row, col) -- Print the cell being visited
    
    -- Check if this is the exit
    if row == 1 or row == numRows or col == 1 or col == numCols then
        print("Exit found at coordinates:", row, col)
        return true
    end
    
    -- Recurse in all four directions
    local directions = {
        {-1, 0}, {1, 0}, {0, -1}, {0, 1}
    }
    
    for _, dir in ipairs(directions) do
        local newRow, newCol = row + dir[1], col + dir[2]
        if dfs(newRow, newCol) then
            return true
        end
    end
    
    return false
end

function find_exit()
    -- Find the exit starting from the perimeter
    for i = 1, numRows do
        if level.map[i][1] == 0 then
            if dfs(i, 1) then
                return true
            end
        end
        if level.map[i][numCols] == 0 then
            if dfs(i, numCols) then
                return true
            end
        end
    end
    for j = 1, numCols do
        if level.map[1][j] == 0 then
            if dfs(1, j) then
                return true
            end
        end
        if level.map[numRows][j] == 0 then
            if dfs(numRows, j) then
                return true
            end
        end
    end
print("Visiting cell:", row, col) -- Print the cell being visited
    return false
end

