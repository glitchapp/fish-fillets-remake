

function drawobjectsassets30(pb, block,x,y,dx,dy,tileSize,i,gridSize,dividx,dividy)

						--level 30
						if nLevel==30 then
							if i==1 then
									if block.name=="crab" then love.graphics.draw (crab, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="piece1" then love.graphics.draw (piece1, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="piece2" then love.graphics.draw (piece2, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="piece3" then love.graphics.draw (piece3, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end

						--level 31
						elseif nLevel==31 then
							if i==1 then
									if block.name=="labyrinth" then love.graphics.draw (labyrinth, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 32
						elseif nLevel==32 then
							if i==1 then
									if block.name=="coral_cerv" then love.graphics.draw (coral_cerv, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="horsefish" and skin=="classic" then
										
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (seahorse00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (seahorse01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (seahorse02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (seahorse03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
								elseif block.name=="horsefish" and skin=="remake" then
										
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (seahorse00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (seahorse01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (seahorse02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (seahorse03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												end
																
								elseif block.name=="korala" then love.graphics.draw (korala, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koralb" then love.graphics.draw (koralb, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral_bily" then love.graphics.draw (koral_bily, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koralc" then love.graphics.draw (koralc, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="korald" then love.graphics.draw (korald, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral_dlouhy" then love.graphics.draw (koral_dlouhy, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral_zel" then love.graphics.draw (koral_zel, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="musle_troj" then love.graphics.draw (musle_troj, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="sasanka_00" then
													if sasankaframe>0   and sasankaframe<0.25 then love.graphics.draw (sasanka00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif sasankaframe>0.25 and sasankaframe<0.5 then love.graphics.draw (sasanka01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.5 and sasankaframe<0.75 then love.graphics.draw (sasanka02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.75 and sasankaframe<1 then love.graphics.draw (sasanka03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1 and sasankaframe<1.25 then love.graphics.draw (sasanka04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.25 and sasankaframe<1.5 then love.graphics.draw (sasanka05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.5 and sasankaframe<1.75 then love.graphics.draw (sasanka06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.75 and sasankaframe<2 then love.graphics.draw (sasanka07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="snail" then
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								end
							end
							
						--level 33
						elseif nLevel==33 then
							if i==1 then
									if block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="seahorse" then
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (seahorse00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (seahorse01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (seahorse02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (seahorse03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/10,dividy/10)
												end
								
								elseif block.name=="crab00" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (crab00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (crab01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (crab02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (crab03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="crab200" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (crab201, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (crab202, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (crab203, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (crab200, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="crab300" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (crab302, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (crab303, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (crab300, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (crab301, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="crab400" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (crab403, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (crab400, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (crab401, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (crab402, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="rybicka" then love.graphics.draw (rybicka01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (rybicka00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (rybicka01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (rybicka02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (rybicka03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="snail" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="sepia" then
												love.graphics.draw (sepia, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 34
						elseif nLevel==34 then
							if i==1 then
									if block.name=="krab" then 
													if crabframe>0   and crabframe<0.25 then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.25 and crabframe<0.5 then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>0.5 and crabframe<0.75 then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>0.75 and crabframe<1 then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.25 then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.25 and crabframe<1.5 then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<1.75 then love.graphics.draw (krab_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.75 and crabframe<2 then love.graphics.draw (krab_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2 and crabframe<2.25 then love.graphics.draw (krab_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2.25 and crabframe<2.5 then love.graphics.draw (krab_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="balal" then
													if balalframe>0   and balalframe<1 then love.graphics.draw (balal_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif balalframe>1 and balalframe<2 then love.graphics.draw (balal_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>2 and balalframe<3 then love.graphics.draw (balal_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>3 and balalframe<4 then love.graphics.draw (balal_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>4 and balalframe<5 then love.graphics.draw (balal_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>5 and balalframe<6 then love.graphics.draw (balal_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>6 and balalframe<7 then love.graphics.draw (balal_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>7 and balalframe<8 then love.graphics.draw (balal_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>8 and balalframe<9 then love.graphics.draw (balal_08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>9 and balalframe<10 then love.graphics.draw (balal_09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balalframe>10 and balalframe<11 then love.graphics.draw (balal_10, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
								elseif block.name=="maly_snek" then love.graphics.draw (maly_snek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if snekframe>0   and snekframe<1 then love.graphics.draw (maly_snek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snekframe>1 and snekframe<2 then love.graphics.draw (maly_snek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snekframe>2 and snekframe<3 then love.graphics.draw (maly_snek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snekframe>3 and snekframe<4 then love.graphics.draw (maly_snek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="sasanka" then
													if sasankaframe>0   and sasankaframe<0.25 then love.graphics.draw (sasanka_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif sasankaframe>0.25 and sasankaframe<0.5 then love.graphics.draw (sasanka_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.5 and sasankaframe<0.75 then love.graphics.draw (sasanka_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.75 and sasankaframe<1 then love.graphics.draw (sasanka_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1 and sasankaframe<1.25 then love.graphics.draw (sasanka_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.25 and sasankaframe<1.5 then love.graphics.draw (sasanka_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.5 and sasankaframe<1.75 then love.graphics.draw (sasanka_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.75 and sasankaframe<2 then love.graphics.draw (sasanka_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								
								elseif block.name=="sepie_00" then love.graphics.draw (sepie_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if sepieframe>0   and sepieframe<0.25 then love.graphics.draw (sepie_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif sepieframe>0.25 and sepieframe<0.5 then love.graphics.draw (sepie_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>0.5 and sepieframe<0.75 then love.graphics.draw (sepie_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>0.75 and sepieframe<1 then love.graphics.draw (sepie_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1 and sepieframe<1.25 then love.graphics.draw (sepie_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.25 and sepieframe<1.5 then love.graphics.draw (sepie_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.5 and sepieframe<1.75 then love.graphics.draw (sepie_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.75 and sepieframe<2 then love.graphics.draw (sepie_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2 and sepieframe<2.25 then love.graphics.draw (sepie_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.25 and sepieframe<2.5 then love.graphics.draw (sepie_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.5 and sepieframe<2.75 then love.graphics.draw (sepie_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.75 and sepieframe<3 then love.graphics.draw (sepie_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>3 and sepieframe<3.25 then love.graphics.draw (sepie_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="shell1" then love.graphics.draw (shell1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 35
						elseif nLevel==35 then
							if i==1 then
									if block.name=="kankan7" then love.graphics.draw (kankan7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan8" then love.graphics.draw (kankan8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan9" then love.graphics.draw (kankan9, ((x+dx-7))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan12" then love.graphics.draw (kankan12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan16" then love.graphics.draw (kankan16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan17" then love.graphics.draw (kankan17, ((x+dx-6))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kankan21" then love.graphics.draw (kankan21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="klavir" then
													if klavirframe>0    and klavirframe<0.25 then love.graphics.draw (klavir_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif klavirframe>0.25 and klavirframe<0.5 then love.graphics.draw (klavir_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>0.5  and klavirframe<0.75 then love.graphics.draw (klavir_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>0.75 and klavirframe<1 then love.graphics.draw (klavir_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>1    and klavirframe<1.25 then love.graphics.draw (klavir_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif klavirframe>1.25 and klavirframe<1.5 then love.graphics.draw (klavir_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>1.5  and klavirframe<1.75 then love.graphics.draw (klavir_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>1.75 and klavirframe<2 then love.graphics.draw (klavir_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>2    and klavirframe<2.25 then love.graphics.draw (klavir_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif klavirframe>2.25 and klavirframe<2.5 then love.graphics.draw (klavir_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>2.5  and klavirframe<2.75 then love.graphics.draw (klavir_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>2.75 and klavirframe<3 then love.graphics.draw (klavir_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>3 	and klavirframe<3.25 then love.graphics.draw (klavir_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>3.25 and klavirframe<3.5 then love.graphics.draw (klavir_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>3.5  and klavirframe<3.75 then love.graphics.draw (klavir_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>3.75 and klavirframe<4 then love.graphics.draw (klavir_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>4 	and klavirframe<4.25 then love.graphics.draw (klavir_08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>4.25 and klavirframe<4.5 then love.graphics.draw (klavir_09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>4.5 	and klavirframe<4.75 then love.graphics.draw (klavir_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>4.75 and klavirframe<5 then love.graphics.draw (klavir_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>5 	and klavirframe<5.25 then love.graphics.draw (klavir_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>5.25 and klavirframe<5.5 then love.graphics.draw (klavir_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>5.5 	and klavirframe<5.75 then love.graphics.draw (klavir_08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>5.75 and klavirframe<6 then love.graphics.draw (klavir_09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>6 	and klavirframe<6.25 then love.graphics.draw (klavir_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>6.25 and klavirframe<6.5 then love.graphics.draw (klavir_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>6.5 	and klavirframe<6.75 then love.graphics.draw (klavir_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>6.75 and klavirframe<7 then love.graphics.draw (klavir_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>7 	and klavirframe<7.25 then love.graphics.draw (klavir_08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>7.25 and klavirframe<7.5 then love.graphics.draw (klavir_09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>7.5 	and klavirframe<7.75 then love.graphics.draw (klavir_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>7.75 and klavirframe<8 then love.graphics.draw (klavir_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>8 	and klavirframe<8.25 then love.graphics.draw (klavir_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>8.25 and klavirframe<8.5 then love.graphics.draw (klavir_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>8.5 	and klavirframe<8.75 then love.graphics.draw (klavir_08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif klavirframe>8.75 and klavirframe<9 then love.graphics.draw (klavir_09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end

								elseif block.name=="korals" then love.graphics.draw (korals, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="krab" then
													if crabframe>0   and crabframe<0.25 then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.25 and crabframe<0.5 then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>0.5 and crabframe<0.75 then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>0.75 and crabframe<1 then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.25 then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.25 and crabframe<1.5 then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<1.75 then love.graphics.draw (krab_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.75 and crabframe<2 then love.graphics.draw (krab_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2 and crabframe<2.25 then love.graphics.draw (krab_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2.25 and crabframe<2.5 then love.graphics.draw (krab_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="muslicka" then love.graphics.draw (muslicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="rejnok" then
													if rejnokframe>0   	and rejnokframe<0.25 then love.graphics.draw (rejnok_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif rejnokframe>0.25 and rejnokframe<0.5	then love.graphics.draw (rejnok_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>0.5 	and rejnokframe<0.75 then love.graphics.draw (rejnok_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>0.75 and rejnokframe<1 	then love.graphics.draw (rejnok_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>1 	and rejnokframe<1.25 then love.graphics.draw (rejnok_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>1.25 and rejnokframe<1.5 then love.graphics.draw (rejnok_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>1.5 	and rejnokframe<1.75 then love.graphics.draw (rejnok_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>1.75 and rejnokframe<2 then love.graphics.draw (rejnok_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>2 	and rejnokframe<2.25 then love.graphics.draw (rejnok_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>2.25 and rejnokframe<2.5 then love.graphics.draw (rejnok_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>2.5 	and rejnokframe<2.75 then love.graphics.draw (rejnok_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif rejnokframe>2.75 and rejnokframe<3 then love.graphics.draw (rejnok_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
								elseif block.name=="sasanka" then
													if sasankaframe>0   and sasankaframe<0.25 then love.graphics.draw (sasanka_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif sasankaframe>0.25 and sasankaframe<0.5 then love.graphics.draw (sasanka_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.5 and sasankaframe<0.75 then love.graphics.draw (sasanka_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>0.75 and sasankaframe<1 then love.graphics.draw (sasanka_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1 and sasankaframe<1.25 then love.graphics.draw (sasanka_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.25 and sasankaframe<1.5 then love.graphics.draw (sasanka_05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.5 and sasankaframe<1.75 then love.graphics.draw (sasanka_06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sasankaframe>1.75 and sasankaframe<2 then love.graphics.draw (sasanka_07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="sepie" then
													if sepieframe>0   and sepieframe<0.25 then love.graphics.draw (sepie_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif sepieframe>0.25 and sepieframe<0.5 then love.graphics.draw (sepie_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>0.5 and sepieframe<0.75 then love.graphics.draw (sepie_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>0.75 and sepieframe<1 then love.graphics.draw (sepie_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1 and sepieframe<1.25 then love.graphics.draw (sepie_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.25 and sepieframe<1.5 then love.graphics.draw (sepie_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.5 and sepieframe<1.75 then love.graphics.draw (sepie_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>1.75 and sepieframe<2 then love.graphics.draw (sepie_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2 and sepieframe<2.25 then love.graphics.draw (sepie_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.25 and sepieframe<2.5 then love.graphics.draw (sepie_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.5 and sepieframe<2.75 then love.graphics.draw (sepie_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>2.75 and sepieframe<3 then love.graphics.draw (sepie_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sepieframe>3 and sepieframe<3.25 then love.graphics.draw (sepie_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="shell1" then love.graphics.draw (shell1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="tecko" then love.graphics.draw (tecko, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 36
						elseif nLevel==36 then
							if i==1 then
									if block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral" then love.graphics.draw (koral, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="musla" then love.graphics.draw (musla, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="musle_troj" then love.graphics.draw (musle_troj, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="perla" then love.graphics.draw (perla_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="zeva" then love.graphics.draw (zeva_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 37
						elseif nLevel==37 then
							if i==1 then
									--if block.name=="turtle" then love.graphics.draw (z_00, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									if block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-5))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel5" then love.graphics.draw (ocel5, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel14" then love.graphics.draw (ocel14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel15" then love.graphics.draw (ocel15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel16" then love.graphics.draw (ocel16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel17" then love.graphics.draw (ocel17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel20" then love.graphics.draw (ocel20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								--elseif block.name=="ocel25" then love.graphics.draw (ocel25, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral0" then love.graphics.draw (koral0, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral1" then love.graphics.draw (koral1, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral2" then love.graphics.draw (koral2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral3" then love.graphics.draw (koral3, ((x+dx-7))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral4" then love.graphics.draw (koral4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral5" then love.graphics.draw (koral5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral6" then love.graphics.draw (koral6, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral7" then love.graphics.draw (koral7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral8" then love.graphics.draw (koral8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koral9" then love.graphics.draw (koral9, ((x+dx-6))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)

								elseif block.name=="musle_troj" then love.graphics.draw (musle_troj, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="perla" then love.graphics.draw (perla_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="rybicka" then love.graphics.draw (rybicka00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="turtle" then
												
												-- anim 1 (idle)
										if turtlestatus=="idle" then
													if zframe>0   	and zframe<0.5 	then love.graphics.draw (z_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>0.5	and zframe<1	then love.graphics.draw (z_01, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1 	and zframe<1.5 	then love.graphics.draw (z_02, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.5	and zframe<2 	then love.graphics.draw (z_03, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2  	and zframe<2.5 	then love.graphics.draw (z_04, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>2.5 	and zframe<3 	then love.graphics.draw (z_05, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3 	and zframe<3.5 	then love.graphics.draw (z_06, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.5 	and zframe<4 	then love.graphics.draw (z_07, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										elseif turtlestatus=="telepatic" then		
												-- telepatic eyes animation
													if zframe>0 	and zframe<0.25 then love.graphics.draw (z_08, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>0.25  and zframe<0.5 	then love.graphics.draw (z_09, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>0.5 	and zframe<0.75 then love.graphics.draw (z_10, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>0.75  and zframe<1 	then love.graphics.draw (z_11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												elseif zframe>1    and zframe<1.25 	then love.graphics.draw (z_08, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>1.25 and zframe<1.5 	then love.graphics.draw (z_09, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.5  and zframe<1.75 	then love.graphics.draw (z_10, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.75 and zframe<2 	then love.graphics.draw (z_11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												elseif zframe>2    and zframe<2.25 	then love.graphics.draw (z_08, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>2.25 and zframe<2.5 	then love.graphics.draw (z_09, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2.5  and zframe<2.75 	then love.graphics.draw (z_10, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2.75 and zframe<3 	then love.graphics.draw (z_11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												elseif zframe>3    and zframe<3.25 	then love.graphics.draw (z_08, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif zframe>3.25 and zframe<3.5 	then love.graphics.draw (z_09, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.5  and zframe<3.75 	then love.graphics.draw (z_10, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.75 and zframe<4 	then love.graphics.draw (z_11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												elseif zframe>4		and zframe<5 	then love.graphics.draw (z_11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif turtlestatus=="lookaround" then		
												-- look around animation
													if zframe>0 	and zframe<0.5 	then love.graphics.draw (z_13, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>0.5	and zframe<1 	then love.graphics.draw (z_14, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1 	and zframe<1.5 	then love.graphics.draw (z_15, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.5 	and zframe<2 	then love.graphics.draw (z_16, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2 	and zframe<2.5 	then love.graphics.draw (z_17, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2.5 	and zframe<3 	then love.graphics.draw (z_18, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3 	and zframe<3.5 	then love.graphics.draw (z_19, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.5 	and zframe<4 	then love.graphics.draw (z_20, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>4 	and zframe<4.5 	then love.graphics.draw (z_21, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>4.5 	and zframe<5 	then love.graphics.draw (z_22, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>5 	and zframe<5.5 	then love.graphics.draw (z_23, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>5.5 	and zframe<6 	then love.graphics.draw (z_24, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>6 	and zframe<6.5 	then love.graphics.draw (z_25, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>6.5 	and zframe<7 	then love.graphics.draw (z_26, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>7 	and zframe<7.5 	then love.graphics.draw (z_13, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>7.5 	and zframe<8 	then love.graphics.draw (z_14, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>8 	and zframe<8.5 	then love.graphics.draw (z_15, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>8.5 	and zframe<9 	then love.graphics.draw (z_16, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>9 	and zframe<9.5 	then love.graphics.draw (z_17, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>9.5 	and zframe<10 	then love.graphics.draw (z_18, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>10 	and zframe<10.5 then love.graphics.draw (z_19, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>10.5 	and zframe<11 	then love.graphics.draw (z_20, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>11 	and zframe<11.5 then love.graphics.draw (z_21, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif turtlestatus=="hide" then
												-- hide animation
													if zframe>0 	and zframe<0.5 	then love.graphics.draw (z_27, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>0.5	and zframe<1 	then love.graphics.draw (z_28, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1 	and zframe<1.5 	then love.graphics.draw (z_29, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.5 	and zframe<2 	then love.graphics.draw (z_30, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2 	and zframe<2.5 	then love.graphics.draw (z_31, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2.5 	and zframe<3 	then love.graphics.draw (z_32, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3 	and zframe<3.5 	then love.graphics.draw (z_33, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.5 	and zframe<4 	then love.graphics.draw (z_34, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>4 	and zframe<4.5 	then love.graphics.draw (z_35, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif turtlestatus=="lookunder" then			
													-- look around under animation
													if zframe>0 	and zframe<0.5 	then love.graphics.draw (z_36, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>0.5	and zframe<1 	then love.graphics.draw (z_37, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1 	and zframe<1.5 	then love.graphics.draw (z_38, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>1.5 	and zframe<2 	then love.graphics.draw (z_39, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2 	and zframe<2.5 	then love.graphics.draw (z_40, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>2.5 	and zframe<3 	then love.graphics.draw (z_41, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3 	and zframe<3.5 	then love.graphics.draw (z_42, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>3.5 	and zframe<4 	then love.graphics.draw (z_43, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>4 	and zframe<4.5 	then love.graphics.draw (z_44, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif zframe>4.5 	and zframe<5 	then love.graphics.draw (z_45, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									end		
								
								end
							end
							
						--level 38
						elseif nLevel==38 then
							if i==1 then
										
										if block.name=="keyboard" then love.graphics.draw (keyboard, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel4" then love.graphics.draw (ocel4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="cola" then love.graphics.draw (cola, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kanystr" then love.graphics.draw (kanystr, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="roura_st" then love.graphics.draw (roura_st, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="roura_st_a" then love.graphics.draw (roura_st_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="vyvrtka" then love.graphics.draw (vyvrtka, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="computerbase" and skin=="classic" then love.graphics.draw (computerbase, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="screen" and skin=="classic"  then love.graphics.draw (screen, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)									
									elseif block.name=="computerbase" and skin=="remake" then love.graphics.draw (computerbase, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2.55,dividy/2.55)
									elseif block.name=="screen" and skin=="remake"  then love.graphics.draw (screen, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2.55,dividy/2.55)			
									scolor1={1,1,1,0.85}
											if timer <39 then love.graphics.setColor(beat/15,beat/15,beat/15,0.85)
										elseif timer>39 and timer <74 then love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.5)
										elseif timer>77 and timer <97 then love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.5)
											
											if subbeat<0.01 then
												countercomputer=countercomputer+1
												countercomputer2=countercomputer2+1
											end
											if countercomputer>64 then
												disk5:play()
												countercomputer=0
											end
											
											if countercomputer2>4 then
												disk6:play()
												countercomputer2=0
											end
									
										elseif timer>97 and timer <117 then love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.5)
												
											if subbeat<0.01 then
												countercomputer=countercomputer+1
												countercomputer2=countercomputer2+1
											end
											if countercomputer>64 then
												disk3:play()
												countercomputer=0
											end
											
											if countercomputer2>4 then
												disk6:play()
												countercomputer2=0
											end
										elseif timer>117 and timer <136 then love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.5)
										end
											if skin=="classic" then love.graphics.draw (screenlight, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif skin=="remake" then love.graphics.draw (screenlight, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2.55,dividy/2.55)
										end
									end
									
									

							end
						--level 39
						elseif nLevel==39 then
							if i==1 then
									if block.name=="drak_m_00" then  love.graphics.draw (drak_m_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								--elseif block.name=="hvezdy2" then  love.graphics.draw (hvezdy2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="krab_00" then  love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="matrace" then  love.graphics.draw (matrace, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="mocel" then  love.graphics.draw (mocel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="netopejr_00" then  love.graphics.draw (netopejr_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="perla_00" then  love.graphics.draw (perla_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="plz_00" then  love.graphics.draw (plz_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="zlato3" then  love.graphics.draw (zlato3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="vocel" then  love.graphics.draw (vocel, ((x+dx-4))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
								
						--level 40
						elseif nLevel==40 then
							if i==1 then
										if block.name=="ocel20" then  love.graphics.draw (ocel20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="balonek" then  love.graphics.draw (balonek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hajzl" then  love.graphics.draw (hajzl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hajzlak" then  love.graphics.draw (hajzlak, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="koulea" then  love.graphics.draw (koulea, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pracka" then
									
													if washmachineframe>0   and washmachineframe<0.5 then love.graphics.draw (pracka_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif washmachineframe>0.5 and washmachineframe<1 then love.graphics.draw (pracka_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>1 and washmachineframe<1.5 then love.graphics.draw (pracka_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>1.5 and washmachineframe<2 then love.graphics.draw (pracka_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>2 and washmachineframe<2.5 then love.graphics.draw (pracka_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>2.5 and washmachineframe<3 then love.graphics.draw (pracka_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>3 and washmachineframe<3.5 then love.graphics.draw (pracka_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>3.5 and washmachineframe<4 then love.graphics.draw (pracka_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>4 and washmachineframe<4.5 then love.graphics.draw (pracka_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif washmachineframe>4.5 and washmachineframe<5 then love.graphics.draw (pracka_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="radio" then  love.graphics.draw (radio, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kufr" then  love.graphics.draw (kufr, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="bedna" then  love.graphics.draw (bedna, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kos" then  love.graphics.draw (kos, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ploutve" then  love.graphics.draw (ploutve, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kartac" then  love.graphics.draw (kartac, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="musla" then  love.graphics.draw (musla, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="klobouk" then  love.graphics.draw (klobouk, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kvetinac" then  love.graphics.draw (kvetinac, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="bota" then  love.graphics.draw (bota, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="botka" then  love.graphics.draw (botka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="shell" then  love.graphics.draw (shell, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="sprcha" then  love.graphics.draw (sprcha, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="sapon" then  love.graphics.draw (sapon, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zidle" then  love.graphics.draw (zidle, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
								end
							end
						--level 41
						elseif nLevel==41 then
							if i==1 then
										if block.name=="cola" 		then  love.graphics.draw (cola, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kachna" 	then  love.graphics.draw (kachna, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kohoutek"	then  love.graphics.draw (kohoutek, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="odpadky11" 	then  love.graphics.draw (odpadky11, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="prkno" 		then  love.graphics.draw (prkno, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="roura_m" 	then  love.graphics.draw (roura_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="roura_st" 	then  love.graphics.draw (roura_st, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="roura_v" 	then  love.graphics.draw (roura_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end	
						
							--level 42
						elseif nLevel==42 then
							if i==1 then
										if block.name=="pinkthing" and skin=="remake" then love.graphics.draw (pinkthing, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pinkthing" and skin=="classic" then
										
												if pinkmonsterframe>0 and pinkmonsterframe<0.5 then love.graphics.draw (pink1, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>0.5 and pinkmonsterframe<1 then love.graphics.draw (pink2, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>1 and pinkmonsterframe<1.5 then love.graphics.draw (pink3, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>1.5 and pinkmonsterframe<2 then love.graphics.draw (pink4, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>2 and pinkmonsterframe<2.5 then love.graphics.draw (pink5, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>2.5 and pinkmonsterframe<3 then love.graphics.draw (pink6, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>3 and pinkmonsterframe<3.5 then love.graphics.draw (pink7, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>3.5 and pinkmonsterframe<4 then love.graphics.draw (pink8, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>4 and pinkmonsterframe<4.5 then love.graphics.draw (pink9, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>4.5 and pinkmonsterframe<5 then love.graphics.draw (pink10, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>5 and pinkmonsterframe<5.5 then love.graphics.draw (pink11, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>5.5 and pinkmonsterframe<6 then love.graphics.draw (pink12, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>6 and pinkmonsterframe<6.5 then love.graphics.draw (pink13, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>6.5 and pinkmonsterframe<7 then love.graphics.draw (pink14, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif pinkmonsterframe>7 and pinkmonsterframe<7.5 then love.graphics.draw (pink15, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											end
																					
									elseif block.name=="01-mona_00" then  love.graphics.draw (mona1_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="02-mona_00" then  love.graphics.draw (mona2_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="03-mona_00" then  love.graphics.draw (mona3_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="04-mona_00" then  love.graphics.draw (mona4_00, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="05-mona_00" then  love.graphics.draw (mona5_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="06-mona_00" then  love.graphics.draw (mona6_00, ((x+dx-1.5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="07-mona_00" then  love.graphics.draw (mona7_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="08-mona_00" then  love.graphics.draw (mona8_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="09-mona_00" then  love.graphics.draw (mona9_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="10-mona_00" then  love.graphics.draw (mona10_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="11-mona_00" then  love.graphics.draw (mona11_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="12-mona_00" then  love.graphics.draw (mona12_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="13-mona_00" then  love.graphics.draw (mona13_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="14-mona_00" then  love.graphics.draw (mona14_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="15-mona_00" then  love.graphics.draw (mona15_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="16-mona_00" then  love.graphics.draw (mona16_00, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="17-mona_00" then  love.graphics.draw (mona17_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="18-mona_00" then  love.graphics.draw (mona18_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="19-mona_00" then  love.graphics.draw (mona19_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="20-mona_00" then  love.graphics.draw (mona20_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									elseif block.name=="puclik-21" then  love.graphics.draw (puclik21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-22" then  love.graphics.draw (puclik22, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-23" then  love.graphics.draw (puclik23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-26" then  love.graphics.draw (puclik26, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-27" then  love.graphics.draw (puclik27, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-28" then  love.graphics.draw (puclik28, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-30" then  love.graphics.draw (puclik30, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="puclik-31" then  love.graphics.draw (puclik31, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									
									
									end
							end
						--level 43
						elseif nLevel==43 then
								if i==1 then
										if block.name=="amfora_zelena" then	love.graphics.draw (amfora_zelena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="balonek" then
													if balonekframe>0   and balonekframe<0.5 then love.graphics.draw (balonek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif balonekframe>0.5 and balonekframe<1 then love.graphics.draw (balonek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balonekframe>1 and balonekframe<1.5 then love.graphics.draw (balonek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif balonekframe>1.5 and balonekframe<2 then love.graphics.draw (balonek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="bota" 		then love.graphics.draw (bota, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="budik" 		then
													if budikframe>0   and budikframe<1 then love.graphics.draw (budik_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif budikframe>1 and budikframe<2 then love.graphics.draw (budik_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="charon"		then love.graphics.draw (charon, ((x+dx-6))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="cola" 		then love.graphics.draw (cola, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="flaska" 	then  love.graphics.draw (flaska, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="harmonika" 	then love.graphics.draw (harmonika, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kotva" 		then love.graphics.draw (kotva, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lahev" 		then love.graphics.draw (lahev, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lodnisroub" then love.graphics.draw (lodnisroub, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="matka" 	then love.graphics.draw (matka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="medusa" 	then
													if medusaframe>0   and medusaframe<1 then love.graphics.draw (medusa_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif medusaframe>1 and medusaframe<2 then love.graphics.draw (medusa_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="meduza" 	then
													if meduzaframe>0   and meduzaframe<1 then love.graphics.draw (meduza_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif meduzaframe>1 and meduzaframe<2 then love.graphics.draw (meduza_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="meduzaz" 	then
													if meduzaframe>0   and meduzaframe<1 then love.graphics.draw (meduzaz_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif meduzaframe>1 and meduzaframe<2 then love.graphics.draw (meduzaz_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="mnohonozka" then
													if mnohonozkaframe>0   and mnohonozkaframe<1 then love.graphics.draw (mnohonozka_00, ((x+dx-8))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif mnohonozkaframe>1 and mnohonozkaframe<2 then love.graphics.draw (mnohonozka_01, ((x+dx-8))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									elseif block.name=="musla" 		then love.graphics.draw (musla, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ostnatec" 	then
													if strangecreatureframe>0   and strangecreatureframe<1 then love.graphics.draw (ostnatec_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif strangecreatureframe>1 and strangecreatureframe<2 then love.graphics.draw (ostnatec_01, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif strangecreatureframe>2 and strangecreatureframe<3 then love.graphics.draw (ostnatec_02, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="pohr" 		then love.graphics.draw (pohr, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="retez" 		then love.graphics.draw (retez, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="savle" 		then love.graphics.draw (savle, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="sekyrka" 	then love.graphics.draw (sekyrka, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="slekicka" 	then love.graphics.draw (slekicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="smetak13" 	then love.graphics.draw (smetak13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="smetak19" 	then love.graphics.draw (smetak19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="smetak20" 	then love.graphics.draw (smetak20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stozar" 	then love.graphics.draw (stozar, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stozarv" 	then love.graphics.draw (stozarv, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="tenisak" 	then love.graphics.draw (tenisak, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="uhor" 		then 
													if uhorframe>0   and uhorframe<1 then love.graphics.draw (uhor_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif uhorframe>1 and uhorframe<2 then love.graphics.draw (uhor_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="zavora" 	then love.graphics.draw (zavora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zebrik" 	then love.graphics.draw (zebrik, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zralok" 	then love.graphics.draw (zralok, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
						--level 44
						elseif nLevel==44 then
								if i==1 then
										if block.name=="ocel12" then	love.graphics.draw (ocel12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel18" then love.graphics.draw (ocel18, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="barel" then
													if barelframe>0 and barelframe<0.5 then love.graphics.draw (barel_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>0.5 and barelframe<1 then love.graphics.draw (barel_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>1 and barelframe<1.5 then love.graphics.draw (barel_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>1.5 and barelframe<2 then love.graphics.draw (barel_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>2 and barelframe<2.5 then love.graphics.draw (barel_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>2.5 and barelframe<3 then love.graphics.draw (barel_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>3 and barelframe<3.5 then love.graphics.draw (barel_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>3.5 and barelframe<4 then love.graphics.draw (barel_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif barelframe>4 and barelframe<10 then love.graphics.draw (barel_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="eye" then
													if mutanteyeframe>0 and mutanteyeframe<0.5 then love.graphics.draw (oko_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutanteyeframe>0.5 and mutanteyeframe<1 then love.graphics.draw (oko_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutanteyeframe>1 and mutanteyeframe<1.5 then love.graphics.draw (oko_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutanteyeframe>1.5 and mutanteyeframe<2 then love.graphics.draw (oko_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutanteyeframe>2 and mutanteyeframe<2.5 then love.graphics.draw (oko_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="double" then 
													if doubleframe>0 and doubleframe<0.5 then 	love.graphics.draw (double1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>0.5 and doubleframe<1 then 	love.graphics.draw (double1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>1 and doubleframe<1.5 then 	love.graphics.draw (double1_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>1.5 and doubleframe<2 then 	love.graphics.draw (double1_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>2 and doubleframe<2.5 then 	love.graphics.draw (double1_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>2.5 and doubleframe<3 then 	love.graphics.draw (double1_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>3 and doubleframe<3.5 then 	love.graphics.draw (double1_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
																								love.graphics.draw (double2_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="baget" then 
													if bagetframe>0 and bagetframe<0.5 then love.graphics.draw (baget_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif bagetframe>0.5 and bagetframe<1 then love.graphics.draw (baget_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									--[[elseif block.name=="double2" then 
													if doubleframe>0 and double2frame<0.5 then love.graphics.draw (double2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>0.5 and double2frame<1 then love.graphics.draw (double2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>1 and double2frame<1.5 then love.graphics.draw (double2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>1.5 and double2frame<2 then love.graphics.draw (double2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>2 and double2frame<2.5 then love.graphics.draw (double2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>2.5 and double2frame<3 then love.graphics.draw (double2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif doubleframe>3 and double2frame<3.5 then love.graphics.draw (double2_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									--]]
									elseif block.name=="had" then
													if hadframe>0 and hadframe<0.5 then love.graphics.draw (had_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>0.5 and hadframe<1 then love.graphics.draw (had_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>1 and hadframe<1.5 then love.graphics.draw (had_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>1.5 and hadframe<2 then love.graphics.draw (had_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>2 and hadframe<2.5 then love.graphics.draw (had_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>2.5 and hadframe<3 then love.graphics.draw (had_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>3 and hadframe<3.5 then love.graphics.draw (had_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>3.5 and hadframe<4 then love.graphics.draw (had_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>4 and hadframe<4.5 then love.graphics.draw (had_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>4.5 and hadframe<5 then love.graphics.draw (had_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>5 and hadframe<5.5 then love.graphics.draw (had_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hadframe>5.5 and hadframe<6 then love.graphics.draw (had_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="hlubinna" then
													if hlubinnaframe>0 and hlubinnaframe<0.5 then love.graphics.draw (hlubinna_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>0.5 and hlubinnaframe<1 then love.graphics.draw (hlubinna_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>1 and hlubinnaframe<1.5 then love.graphics.draw (hlubinna_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>1.5 and hlubinnaframe<2 then love.graphics.draw (hlubinna_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>2 and hlubinnaframe<2.5 then love.graphics.draw (hlubinna_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>2.5 and hlubinnaframe<3 then love.graphics.draw (hlubinna_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>3 and hlubinnaframe<3.5 then love.graphics.draw (hlubinna_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>3.5 and hlubinnaframe<4 then love.graphics.draw (hlubinna_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif hlubinnaframe>4 and hlubinnaframe<4.5 then love.graphics.draw (hlubinna_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="kachna" then 	
													if kachnaframe>0 and kachnaframe<0.5 then love.graphics.draw (kachna_00, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>0.5 and kachnaframe<1 then love.graphics.draw (kachna_01, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>1 and kachnaframe<1.5 then love.graphics.draw (kachna_02, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>1.5 and kachnaframe<2 then love.graphics.draw (kachna_03, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>2 and kachnaframe<2.5 then love.graphics.draw (kachna_04, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>2.5 and kachnaframe<3 then love.graphics.draw (kachna_05, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>3 and kachnaframe<3.5 then love.graphics.draw (kachna_06, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>3.5 and kachnaframe<4 then love.graphics.draw (kachna_07, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kachnaframe>4 and kachnaframe<4.5 then love.graphics.draw (kachna_08, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="pinkmonster" then 
													if pinkmonsterframe>0 and pinkmonsterframe<0.5 then love.graphics.draw (pink1, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>0.5 and pinkmonsterframe<1 then love.graphics.draw (pink2, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1 and pinkmonsterframe<1.5 then love.graphics.draw (pink3, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1.5 and pinkmonsterframe<2 then love.graphics.draw (pink4, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2 and pinkmonsterframe<2.5 then love.graphics.draw (pink5, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2.5 and pinkmonsterframe<3 then love.graphics.draw (pink6, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3 and pinkmonsterframe<3.5 then love.graphics.draw (pink7, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3.5 and pinkmonsterframe<4 then love.graphics.draw (pink8, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4 and pinkmonsterframe<4.5 then love.graphics.draw (pink9, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4.5 and pinkmonsterframe<5 then love.graphics.draw (pink10, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5 and pinkmonsterframe<5.5 then love.graphics.draw (pink11, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5.5 and pinkmonsterframe<6 then love.graphics.draw (pink12, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>6 and pinkmonsterframe<6.5 then love.graphics.draw (pink13, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>6.5 and pinkmonsterframe<7 then love.graphics.draw (pink14, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>7 and pinkmonsterframe<7.5 then love.graphics.draw (pink15, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="killer" then
													if killerframe>0 and killerframe<0.5 then love.graphics.draw (killer_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif killerframe>0.5 and killerframe<1 then love.graphics.draw (killer_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif killerframe>1 and killerframe<1.5 then love.graphics.draw (killer_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif killerframe>1.5 and killerframe<2 then love.graphics.draw (killer_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif killerframe>2 and killerframe<2.5 then love.graphics.draw (killer_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif killerframe>2.5 and killerframe<3 then love.graphics.draw (killer_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="kukajda" then
													if kukajdaframe>0 and kukajdaframe<0.5 then love.graphics.draw (kukajda_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>0.5 and kukajdaframe<1 then love.graphics.draw (kukajda_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>1 and kukajdaframe<1.5 then love.graphics.draw (kukajda_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>1.5 and kukajdaframe<2 then love.graphics.draw (kukajda_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>2 and kukajdaframe<2.5 then love.graphics.draw (kukajda_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>2.5 and kukajdaframe<3 then love.graphics.draw (kukajda_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>3 and kukajdaframe<3.5 then love.graphics.draw (kukajda_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>3.5 and kukajdaframe<4 then love.graphics.draw (kukajda_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>4 and kukajdaframe<4.5 then love.graphics.draw (kukajda_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>4.5 and kukajdaframe<5 then love.graphics.draw (kukajda_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>5 and kukajdaframe<5.5 then love.graphics.draw (kukajda_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>5.5 and kukajdaframe<6 then love.graphics.draw (kukajda_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>6 and kukajdaframe<6.5 then love.graphics.draw (kukajda_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>6.5 and kukajdaframe<7 then love.graphics.draw (kukajda_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>7 and kukajdaframe<7.5 then love.graphics.draw (kukajda_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>7.5 and kukajdaframe<8 then love.graphics.draw (kukajda_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif kukajdaframe>8 and kukajdaframe<8.5 then love.graphics.draw (kukajda_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="krab" then
													if mutantkrabframe>0 and mutantkrabframe<0.5 then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantkrabframe>0.5 and mutantkrabframe<1 then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantkrabframe>1 and mutantkrabframe<1.5 then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantkrabframe>1.5 and mutantkrabframe<2 then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantkrabframe>2 and mutantkrabframe<2.5 then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantkrabframe>2.5 and mutantkrabframe<3 then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="noha" then
													if nohaframe>0 and nohaframe<0.5 then love.graphics.draw (noha_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>0.5 and nohaframe<1 then love.graphics.draw (noha_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>1 and nohaframe<1.5 then love.graphics.draw (noha_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>1.5 and nohaframe<2 then love.graphics.draw (noha_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>2 and nohaframe<2.5 then love.graphics.draw (noha_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>2.5 and nohaframe<3 then love.graphics.draw (noha_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>3 and nohaframe<3.5 then love.graphics.draw (noha_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>3.5 and nohaframe<4 then love.graphics.draw (noha_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>4 and nohaframe<4.5 then love.graphics.draw (noha_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>4.5 and nohaframe<5 then love.graphics.draw (noha_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif nohaframe>5 and nohaframe<5.5 then love.graphics.draw (noha_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									--elseif block.name=="baget" then love.graphics.draw (, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pldik" then 
													if pinkmonsterframe>0 and pinkmonsterframe<0.5 then love.graphics.draw (pldik_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>0.5 and pinkmonsterframe<1 then love.graphics.draw (pldik_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1 and pinkmonsterframe<1.5 then love.graphics.draw (pldik_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1.5 and pinkmonsterframe<2 then love.graphics.draw (pldik_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2 and pinkmonsterframe<2.5 then love.graphics.draw (pldik_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2.5 and pinkmonsterframe<3 then love.graphics.draw (pldik_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3 and pinkmonsterframe<3.5 then love.graphics.draw (pldik_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3.5 and pinkmonsterframe<4 then love.graphics.draw (pldik_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4 and pinkmonsterframe<4.5 then love.graphics.draw (pldik_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4.5 and pinkmonsterframe<5 then love.graphics.draw (pldik_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5 and pinkmonsterframe<5.5 then love.graphics.draw (pldik_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5.5 and pinkmonsterframe<6 then love.graphics.draw (pldik_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="shark" then 
													if mutantsharkframe>0 and mutantsharkframe<0.5 then love.graphics.draw (shark_00, ((x+dx-7))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantsharkframe>0.5 and mutantsharkframe<1 then love.graphics.draw (shark_01, ((x+dx-7))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantsharkframe>1 and mutantsharkframe<1.5 then love.graphics.draw (shark_02, ((x+dx-7))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mutantsharkframe>1.5 and mutantsharkframe<2 then love.graphics.draw (shark_03, ((x+dx-7))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									end
								end
							
						--level 45
						elseif nLevel==45 or nLevel==49 then
							if i==1 then
										if block.name=="chobotnice" then
													if chobotniceframe>0 and chobotniceframe<0.5 then love.graphics.draw (chobotnice_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>0.5 and chobotniceframe<1 then love.graphics.draw (chobotnice_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>1 and chobotniceframe<1.5 then love.graphics.draw (chobotnice_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>1.5 and chobotniceframe<2 then love.graphics.draw (chobotnice_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>2 and chobotniceframe<2.5 then love.graphics.draw (chobotnice_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>2.5 and chobotniceframe<3 then love.graphics.draw (chobotnice_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>3 and chobotniceframe<3.5 then love.graphics.draw (chobotnice_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>3.5 and chobotniceframe<4 then love.graphics.draw (chobotnice_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif chobotniceframe>4 and chobotniceframe<4.5 then love.graphics.draw (chobotnice_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											
									elseif block.name=="lampa" then love.graphics.draw (lampa, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lebzna" then love.graphics.draw (lebzna, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="papoucha" then
													if papouchaframe>0 and papouchaframe<1 then love.graphics.draw (papoucha_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif papouchaframe>1 and papouchaframe<2 then love.graphics.draw (papoucha_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="trubka1" then love.graphics.draw (trubka1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="trubka2" then love.graphics.draw (trubka2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="truhla" then love.graphics.draw (truhla, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="papzivy" then love.graphics.draw (papzivy_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end
							
						--level 46
						elseif nLevel==46 then
							if i==1 then
										if block.name=="snehulak" then
													if snehulakframe>0 and snehulakframe<1 then love.graphics.draw (snehulak_00, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif snehulakframe>1 and snehulakframe<2 then love.graphics.draw (snehulak_01, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif snehulakframe>2 and snehulakframe<3 then love.graphics.draw (snehulak_01, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												end
										 
									elseif block.name=="stolekm" then love.graphics.draw (stolekm, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stolekv" then love.graphics.draw (stolekv, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="posun1" then love.graphics.draw (posun1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 47
							
						elseif nLevel==47 then
								if i==1 then
											
											if block.name=="delo" then
													if deloframe>0 and deloframe<0.5 then love.graphics.draw (delo_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif deloframe>0.5 and deloframe<1 then love.graphics.draw (delo_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif deloframe>1 and deloframe<1.5 then love.graphics.draw (delo_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											
										elseif block.name=="delorev" then
													if deloframe>0 and deloframe<0.5 then love.graphics.draw (delorev_00, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif deloframe>0.5 and deloframe<1 then love.graphics.draw (delorev_01, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif deloframe>1 and deloframe<1.5 then love.graphics.draw (delorev_02, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end

										elseif block.name=="koulea" then love.graphics.draw (koulea, ((x+dx))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
										elseif block.name=="kouleb" then love.graphics.draw (kouleb, ((x+dx))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
										elseif block.name=="koulec" then love.graphics.draw (koulec, ((x+dx))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
										elseif block.name=="kouled" then love.graphics.draw (kouled, ((x+dx))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
										elseif block.name=="mec" then love.graphics.draw (mec, ((x-4+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="sekera" then love.graphics.draw (sekera, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="sud" then love.graphics.draw (sud, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										end
								end
							
						--level 48
						elseif nLevel==48 then
								if i==1 then
											if block.name=="kreslo" then love.graphics.draw (kreslo, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="hrnec" then love.graphics.draw (hrnec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="hrnecek" then love.graphics.draw (hrnecek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="ocel17" then love.graphics.draw (ocel17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="ocel18" then love.graphics.draw (ocel18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
										elseif block.name=="mapa_v" then love.graphics.draw (mapa_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="mec" then love.graphics.draw (mec, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pohar" then love.graphics.draw (pohar, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="rahno_m" then love.graphics.draw (rahno_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="rahno_v" then love.graphics.draw (rahno_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
										elseif block.name=="stolek" then love.graphics.draw (stolek, ((x-0.3+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="stoleka" then love.graphics.draw (stoleka, ((x-0.5+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="stolekm" then love.graphics.draw (stolekm, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="stolekv" then love.graphics.draw (stolekv, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
										elseif block.name=="stozar_v_l" then love.graphics.draw (stozar_v_l, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="stul" then love.graphics.draw (stul, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="sud" then love.graphics.draw (sud, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="vejce" then love.graphics.draw (vejce, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										end
								end
			
								
						--level 50
						elseif nLevel==50 then
									if i==1 then
												if block.name=="drahokam" then
														if drahokamframe>0 and drahokamframe<0.5 then love.graphics.draw (drahokam_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif drahokamframe>0.5 and drahokamframe<1 then love.graphics.draw (drahokam_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif drahokamframe>1 and drahokamframe<1.5 then love.graphics.draw (drahokam_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif drahokamframe>1.5 and drahokamframe<2 then love.graphics.draw (drahokam_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif drahokamframe>2 and drahokamframe<2.5 then love.graphics.draw (drahokam_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif drahokamframe>2.5 and drahokamframe<3 then love.graphics.draw (drahokam_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
												 
											elseif block.name=="hak" then love.graphics.draw (hak_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="kreslo" then love.graphics.draw (kreslo, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="krystal_c" then 
													if krystalcframe>0 and krystalcframe<0.5 then love.graphics.draw (krystal_c_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>0.5 and krystalcframe<1 then love.graphics.draw (krystal_c_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1 and krystalcframe<1.5 then love.graphics.draw (krystal_c_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1.5 and krystalcframe<2 then love.graphics.draw (krystal_c_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											 
											elseif block.name=="krystal_f" then
													if krystalcframe>0 and krystalcframe<0.5 then love.graphics.draw (krystal_f_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>0.5 and krystalcframe<1 then love.graphics.draw (krystal_f_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1 and krystalcframe<1.5 then love.graphics.draw (krystal_f_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1.5 and krystalcframe<2 then love.graphics.draw (krystal_f_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											 
											elseif block.name=="krystal_m" then
													if krystalcframe>0 and krystalcframe<0.5 then love.graphics.draw (krystal_m_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>0.5 and krystalcframe<1 then love.graphics.draw (krystal_m_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1 and krystalcframe<1.5 then love.graphics.draw (krystal_m_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif krystalcframe>1.5 and krystalcframe<2 then love.graphics.draw (krystal_m_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											 
											elseif block.name=="lebza" then 
											
													if lebzaframe>0 and lebzaframe<0.5 and timer>25.5 then love.graphics.draw (lebza_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lebzaframe>0.5 and lebzaframe<1 and timer>25.5 then love.graphics.draw (lebza_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lebzaframe>1 and lebzaframe<1.5 and timer>25.5 then love.graphics.draw (lebza_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lebzaframe>1.5 and lebzaframe<2 and timer>25.5 then love.graphics.draw (lebza_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												else love.graphics.draw (lebza_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											
											elseif block.name=="mapa_m" then love.graphics.draw (mapa_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="mapa_v" then love.graphics.draw (mapa_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="nuz" then love.graphics.draw (nuz, ((x+dx-6))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="oko" then 
													if okoframe>0 and okoframe<0.5 then love.graphics.draw (oko_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okoframe>0.5 and okoframe<1 then love.graphics.draw (oko_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okoframe>1 and okoframe<1.5 then love.graphics.draw (oko_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okoframe>1.5 and okoframe<2 then love.graphics.draw (oko_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okoframe>2 and okoframe<2.5 then love.graphics.draw (oko_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
											
											elseif block.name=="pohar" then love.graphics.draw (pohar, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="rahno_m" then love.graphics.draw (rahno_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="rahno_v" then love.graphics.draw (rahno_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="stozar_m" then love.graphics.draw (stozar_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="stozar_v" then love.graphics.draw (stozar_v, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="stozar_v_l" then love.graphics.draw (stozar_v_l, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="stul" then love.graphics.draw (stul, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="vladova2" then love.graphics.draw (vladova2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="vladova10" then love.graphics.draw (vladova10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="vladova18" then love.graphics.draw (vladova18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="vladova21" then love.graphics.draw (vladova21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											
											--[[if block.name=="skelblock" then love.graphics.draw (skelblock, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="metaldoghnut" then love.graphics.draw (metaldoghnut, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="armchair" then love.graphics.draw (armchair, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2hor" then love.graphics.draw (pipe2hor, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="ost" then love.graphics.draw (ost, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="map2" then love.graphics.draw (map2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="map3" then love.graphics.draw (map3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="wood2" then love.graphics.draw (wood2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="woodhor4" then love.graphics.draw (woodhor4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="woodhor4_2" then love.graphics.draw (woodhor4_2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="woodver4" then love.graphics.draw (woodver4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="woodver4_2" then love.graphics.draw (woodver4_2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="wood5" then love.graphics.draw (wood5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="wood52" then love.graphics.draw (wood52, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="wood8" then love.graphics.draw (wood8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="diamond" then love.graphics.draw (diamond, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="diamond2" then love.graphics.draw (diamond2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="diamond3" then love.graphics.draw (diamond3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="eye" then love.graphics.draw (eye, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="cup" then love.graphics.draw (cup, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="table" then love.graphics.draw (table1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="hook" then love.graphics.draw (hook, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="sword" then love.graphics.draw (sword, ((x-5+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										--]]
										end
									end
									
						--level 51
						elseif nLevel==51 then
							if i==1 then
									if block.name=="ocel3" then love.graphics.draw (ocel3, ((x-0.1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ocel19"  then love.graphics.draw (ocel19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="klobouk" then love.graphics.draw (klobouk, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kouleb" then love.graphics.draw (kouleb, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="koulec"  then love.graphics.draw (koulec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kouled"  then love.graphics.draw (kouled, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="krystal_c"  then love.graphics.draw (krystal_c, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="krystal_m"  then love.graphics.draw (krystal_m, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="maly_snek"  then love.graphics.draw (maly_snek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="malysnek"  then love.graphics.draw (malysnek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="mapab"  then love.graphics.draw (mapab, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="oko"  then love.graphics.draw (oko, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pistole"  then love.graphics.draw (pistole, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="stolekm"  then love.graphics.draw (stolekm, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
								
						--level 52
						elseif nLevel==52 then
							if i==1 then
							if block.name=="p1" or block.name=="p2" or block.name=="p3" or  block.name=="p4" or block.name=="p5" or block.name=="p7" or block.name=="p8" then
								local lightPos1 = {0.1, 0.1, 0.075}
	local lightColor = {red, green, blue, 5}

		-- Secondary light
				-- Light position 2
				local lightPos2 = { 0.5, 0.5, 0.075}

				local lightColor2 = {red, green, blue, 5}
					
		 vectorNormalsNoiseShadowEnvmapShader:send("LightPos1", lightPos1)
		 vectorNormalsNoiseShadowEnvmapShader:send("LightColor1", lightColor)
		
		vectorNormalsNoiseShadowEnvmapShader:send("LightPos2", lightPos2)
		vectorNormalsNoiseShadowEnvmapShader:send("LightColor2", lightColor2)
		
        
        -- Set other shader parameters for normal mapping
        vectorNormalsNoiseShadowEnvmapShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        vectorNormalsNoiseShadowEnvmapShader:send("Falloff", {0.2, 1, 0.5})
        -- Ambient color
		vectorNormalsNoiseShadowEnvmapShader:send("AmbientColor", {red * 0.2, green * 0.2, blue * 0.3, 1.5})

		-- Reflection intensity
		vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", 1.8)

		vectorNormalsNoiseShadowEnvmapShader:send("u_envMap", envMap)
		
							love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
							end
										if block.name=="pinkthing" and skin=="remake" then love.graphics.draw (pinkthing, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pinkthing" and skin=="classic" then
										
													if pinkmonsterframe>0 and pinkmonsterframe<0.5 then love.graphics.draw (pink1, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>0.5 and pinkmonsterframe<1 then love.graphics.draw (pink2, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1 and pinkmonsterframe<1.5 then love.graphics.draw (pink3, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>1.5 and pinkmonsterframe<2 then love.graphics.draw (pink4, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2 and pinkmonsterframe<2.5 then love.graphics.draw (pink5, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>2.5 and pinkmonsterframe<3 then love.graphics.draw (pink6, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3 and pinkmonsterframe<3.5 then love.graphics.draw (pink7, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>3.5 and pinkmonsterframe<4 then love.graphics.draw (pink8, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4 and pinkmonsterframe<4.5 then love.graphics.draw (pink9, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>4.5 and pinkmonsterframe<5 then love.graphics.draw (pink10, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5 and pinkmonsterframe<5.5 then love.graphics.draw (pink11, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>5.5 and pinkmonsterframe<6 then love.graphics.draw (pink12, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>6 and pinkmonsterframe<6.5 then love.graphics.draw (pink13, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>6.5 and pinkmonsterframe<7 then love.graphics.draw (pink14, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pinkmonsterframe>7 and pinkmonsterframe<7.5 then love.graphics.draw (pink15, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												end
									
									elseif block.name=="p1" and skin=="remake" then
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p1n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p1h)
											love.graphics.draw (p1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="p2" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p2n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p2h)
											love.graphics.draw (p2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="p3" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p3n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p3h)
											love.graphics.draw (p3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="p4" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p4n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p4h)
											love.graphics.draw (p4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="p5" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p5n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p5h)
											love.graphics.draw (p5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="p7" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p7n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p7h)
											love.graphics.draw (p7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)	
									elseif block.name=="p8" and skin=="remake" then 
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", p8n)
											vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", p8h)
											love.graphics.draw (p8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									elseif block.name=="p1" and skin=="classic" and skinupscaled==false then love.graphics.draw (p1, ((x+dx-1))*tileSize+frgx, (y-0.75+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p2" and skin=="classic" and skinupscaled==false then love.graphics.draw (p2, ((x+dx-1))*tileSize+frgx, (y+0.4+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p3" and skin=="classic" and skinupscaled==false then love.graphics.draw (p3, ((x+dx-1))*tileSize+frgx, (y+0.4+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p4" and skin=="classic" and skinupscaled==false then love.graphics.draw (p4, ((x+dx-1))*tileSize+frgx, (y+0.65+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p5" and skin=="classic" and skinupscaled==false then love.graphics.draw (p5, ((x+dx-1))*tileSize+frgx, (y+0.65+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p7" and skin=="classic" and skinupscaled==false then love.graphics.draw (p7, ((x+dx-1))*tileSize+frgx, (y+1.1+dy-1)*tileSize,0,dividx/2,dividy/2)	
									elseif block.name=="p8" and skin=="classic" and skinupscaled==false then love.graphics.draw (p8, ((x+dx-1))*tileSize+frgx, (y+1.2+dy-1)*tileSize,0,dividx/2,dividy/2)
									
															
									elseif block.name=="p1" and skin=="classic" and skinupscaled==true then love.graphics.draw (p1, ((x+dx-1))*tileSize+frgx, (y-1+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p3" and skin=="classic" and skinupscaled==true then love.graphics.draw (p3, ((x+dx-1))*tileSize+frgx, (y-0.2+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p4" and skin=="classic" and skinupscaled==true then love.graphics.draw (p4, ((x+dx-1))*tileSize+frgx, (y-0.2+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p5" and skin=="classic" and skinupscaled==true then love.graphics.draw (p5, ((x+dx-1))*tileSize+frgx, (y-0.2+dy-1)*tileSize,0,dividx/2,dividy/2)
									elseif block.name=="p7" and skin=="classic" and skinupscaled==true then love.graphics.draw (p7, ((x+dx-1))*tileSize+frgx, (y-0.5+dy-1)*tileSize,0,dividx/2,dividy/2)	
									elseif block.name=="p8" and skin=="classic" and skinupscaled==true then love.graphics.draw (p8, ((x+dx-1))*tileSize+frgx, (y-0.5+dy-1)*tileSize,0,dividx/2,dividy/2)
									
									elseif block.name=="cube" then love.graphics.draw (cube, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
									end
							end
							
						--level 53
						elseif nLevel==53 then
							if i==1 then
										if block.name=="magnet" then love.graphics.draw (magnet, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="scifigun" then love.graphics.draw (scifigun, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lpipe" then love.graphics.draw (lpipe, ((x-5+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="tpipe" then love.graphics.draw (tpipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="radio" then love.graphics.draw (radio, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 54
						elseif nLevel==54 then
							if i==1 then
							
							
								if enginepushed then
										love.graphics.setColor(1/subbeat/2,0.5,0.5,1)
										if block.name=="alienengine" then
												love.graphics.setColor(1/subbeat/2,0,0,1)
												love.graphics.draw (alienengine, math.random(0,2)+((x-3+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginey= block.y		-- y position of the alien engine
										
										elseif block.name=="enginekey" then	
												positionenginekeyx= block.x			-- x position of the alien engine key
												positionenginekeyy= block.y			-- y position of the alien engine key
													if skin=="remake" then love.graphics.draw (key, math.random(0,2)+((x+dx-1))*tileSize+frgx,math.random(0,2)+ (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif skin=="classic" then 
														if alienenginekeyframe>0   and alienenginekeyframe<0.5 then love.graphics.draw (key, math.random(0,2)+((x+dx-1))*tileSize+frgx,math.random(0,2)+ (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif alienenginekeyframe>0.5 and alienenginekeyframe<1 then love.graphics.draw (key2, math.random(0,2)+((x+dx-1))*tileSize+frgx,math.random(0,2)+ (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif alienenginekeyframe>1 and alienenginekeyframe<1.5 then love.graphics.draw (key3, math.random(0,2)+((x+dx-1))*tileSize+frgx,math.random(0,2)+ (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif alienenginekeyframe>1.5 and alienenginekeyframe<2 then love.graphics.draw (key2, math.random(0,2)+((x+dx-1))*tileSize+frgx,math.random(0,2)+ (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
												end
												
												
										elseif block.name=="pipe1" 	   then	love.graphics.draw (pipe1, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="nut" 	   then love.graphics.draw (nut, ((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="screw" 	   then love.graphics.draw (screw, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2horizontal" then love.graphics.draw (pipe2horizontal, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2" 	   then love.graphics.draw (pipe2, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe3" 	   then love.graphics.draw (pipe3, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="xalien"    then love.graphics.draw (xalien, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										end
								elseif not (enginepushed) then

											if block.name=="alienengine" then
												love.graphics.draw (alienengine, ((x-3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginey= block.y		-- y position of the alien engine
										elseif block.name=="enginekey" then											
												positionenginekeyx= block.x			-- x position of the alien engine key
												positionenginekeyy= block.y			-- y position of the alien engine key
												love.graphics.draw (key, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe1" then	love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="nut" then love.graphics.draw (nut, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="screw" then love.graphics.draw (screw, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2horizontal" then love.graphics.draw (pipe2horizontal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="xalien" then love.graphics.draw (xalien, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
										end
								end
							end
							

						--level 55
						elseif nLevel==55 then
							if i==1 then
								
							if pipepushed==true then love.graphics.setColor(1/subbeat/3+0.35,0.5,0.5,1) end
							
										if block.name=="pipe1" then	love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe1horizontal" then love.graphics.draw (pipe1horizontal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3" or block.name=="pipe3triggeralarm" then
										love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3horizontal" then love.graphics.draw (pipe3horizontal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipesnail" then love.graphics.draw (pipesnail, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipebig" then love.graphics.draw (pipebig, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
									end
							end
							
						--level 56
						elseif nLevel==56 then
							if i==1 then
										if lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,1) end
										
										if lightswitchon==true then
										
											if aproachrobodog==false then
														if block.name=="dogleft" then love.graphics.draw (robodog, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
													elseif block.name=="dogright" then love.graphics.draw (robodog, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
											elseif aproachrobodog==true then
														if block.name=="dogleft" then love.graphics.draw (robodogeyeon, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
													elseif block.name=="dogright" then love.graphics.draw (robodogeyeon, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
											end
												if block.name=="lightswitch" then love.graphics.draw (lightswitch, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="nut" then love.graphics.draw (nut, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="pipehorizontal" then love.graphics.draw (pipehorizontal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="door" then love.graphics.draw (dverea_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif block.name=="sciplatform" then love.graphics.draw (sciplatform, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										elseif lightswitchon==false then
												love.graphics.setColor(1/subbeat/3+0.35,0,0,1)
														if block.name=="dogleft" then love.graphics.draw (robodoglightoff, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
													elseif block.name=="dogright" then love.graphics.draw (robodoglightoff, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif block.name=="lightswitch" then love.graphics.draw (lightswitchoff, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
										end
										love.graphics.setColor(0,0,0,1)
										
								end
							
						
						--level 57
						elseif nLevel==57 then
							if i==1 then
										if block.name=="zk_b" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_b_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_b_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_b_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="zk_c" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_c_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_c_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_c_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="zk_f" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_f_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_f_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_f_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="zk_lezici" then love.graphics.draw (zk_lezici, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zk_m" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_m_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_m_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_m_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="zk_o" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_o_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_o_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_o_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="zk_r" then
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_r_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_r_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_r_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end

									elseif block.name=="zk_y" then love.graphics.draw (zk_y, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zk_z" then love.graphics.draw (zk_z, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zk_zd" then 
														if thinjarframe>0   and thinjarframe<0.5 then love.graphics.draw (zk_zd_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>0.5 and thinjarframe<1 then love.graphics.draw (zk_zd_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													elseif thinjarframe>1 and thinjarframe<1.5 then love.graphics.draw (zk_zd_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													end
									elseif block.name=="p" then
													if porganismframe>0   and porganismframe<0.25 then love.graphics.draw (p_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif porganismframe>0.25 and porganismframe<0.5 then love.graphics.draw (p_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>0.5 and porganismframe<0.75 then love.graphics.draw (p_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>0.75 and porganismframe<1 then love.graphics.draw (p_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>1 and porganismframe<1.25 then love.graphics.draw (p_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>1.25 and porganismframe<1.5 then love.graphics.draw (p_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>1.5 and porganismframe<1.75 then love.graphics.draw (p_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>1.75 and porganismframe<2 then love.graphics.draw (p_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>2 and porganismframe<2.25 then love.graphics.draw (p_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>2.25 and porganismframe<2.5 then love.graphics.draw (p_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>2.5 and porganismframe<2.75 then love.graphics.draw (p_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>2.75 and porganismframe<3 then love.graphics.draw (p_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>3 and porganismframe<3.25 then love.graphics.draw (p_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>3.25 and porganismframe<3.5 then love.graphics.draw (p_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>3.5 and porganismframe<3.75 then love.graphics.draw (p_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>3.75 and porganismframe<4 then love.graphics.draw (p_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>4 and porganismframe<4.25 then love.graphics.draw (p_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>4.25 and porganismframe<4.5 then love.graphics.draw (p_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>4.5 and porganismframe<4.75 then love.graphics.draw (p_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>4.75 and porganismframe<5 then love.graphics.draw (p_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>5 and porganismframe<5.25 then love.graphics.draw (p_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>5.25 and porganismframe<5.5 then love.graphics.draw (p_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>5.5 and porganismframe<5.75 then love.graphics.draw (p_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>5.75 and porganismframe<6 then love.graphics.draw (p_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif porganismframe>6 and porganismframe<6.25 then love.graphics.draw (p_24, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="q" then
													if qorganismframe>0   and qorganismframe<0.25 then love.graphics.draw (q_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif qorganismframe>0.25 and qorganismframe<0.5 then love.graphics.draw (q_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>0.5 and qorganismframe<0.75 then love.graphics.draw (q_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>0.75 and qorganismframe<1 then love.graphics.draw (q_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>1 and qorganismframe<1.25 then love.graphics.draw (q_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>1.25 and qorganismframe<1.5 then love.graphics.draw (q_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>1.5 and qorganismframe<1.75 then love.graphics.draw (q_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif qorganismframe>1.75 and qorganismframe<2 then love.graphics.draw (q_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe6" then love.graphics.draw (pipe6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mutant" then
													if mutantframe>0 and mutantframe<0.5 then love.graphics.draw (mutant_00, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>0.5 and mutantframe<1 then love.graphics.draw (mutant_01, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>1 and mutantframe<1.5 then love.graphics.draw (mutant_02, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>1.5 and mutantframe<2 then love.graphics.draw (mutant_03, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>2 and mutantframe<2.5 then love.graphics.draw (mutant_04, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>2.5 and mutantframe<3 then love.graphics.draw (mutant_05, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>3 and mutantframe<3.5 then love.graphics.draw (mutant_06, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>3.5 and mutantframe<4 then love.graphics.draw (mutant_07, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>4 and mutantframe<4.5 then love.graphics.draw (mutant_08, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												elseif mutantframe>4.5 and mutantframe<5 then love.graphics.draw (mutant_09, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="lahvac" and lahvacstate=="complete" then 
												--complete
													if lahvacframe>0   and lahvacframe<0.25 then love.graphics.draw (lahvac_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif lahvacframe>0.25 and lahvacframe<0.5 then love.graphics.draw (lahvac_01, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>0.5 and lahvacframe<0.75 then love.graphics.draw (lahvac_02, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>0.75 and lahvacframe<1 then love.graphics.draw (lahvac_03, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>1 and lahvacframe<1.25 then love.graphics.draw (lahvac_04, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>1.25 and lahvacframe<1.5 then love.graphics.draw (lahvac_05, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>1.5 and lahvacframe<1.75 then love.graphics.draw (lahvac_06, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>1.75 and lahvacframe<2 then love.graphics.draw (lahvac_07, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>2 and lahvacframe<2.25 then love.graphics.draw (lahvac_08, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>2.25 and lahvacframe<2.5 then love.graphics.draw (lahvac_09, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>2.5 and lahvacframe<2.75 then love.graphics.draw (lahvac_10, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>2.75 and lahvacframe<3 then love.graphics.draw (lahvac_11, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>3 and lahvacframe<3.25 then love.graphics.draw (lahvac_12, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>3.25 and lahvacframe<3.5 then love.graphics.draw (lahvac_13, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>3.5 and lahvacframe<3.75 then love.graphics.draw (lahvac_14, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>3.75 and lahvacframe<4 then love.graphics.draw (lahvac_15, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>4 and lahvacframe<4.25 then love.graphics.draw (lahvac_16, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>4.25 and lahvacframe<4.5 then love.graphics.draw (lahvac_17, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>4.5 and lahvacframe<4.75 then love.graphics.draw (lahvac_18, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>4.75 and lahvacframe<5 then love.graphics.draw (lahvac_19, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>5 and lahvacframe<5.25 then love.graphics.draw (lahvac_20, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>5.25 and lahvacframe<5.5 then love.graphics.draw (lahvac_21, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>5.5 and lahvacframe<5.75 then love.graphics.draw (lahvac_22, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvacframe>5.75 and lahvacframe<6 then love.graphics.draw (lahvac_23, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										elseif block.name=="lahvac" and lahvacstate=="crash" then 		
												--crash
													if lahvaccrashframe>0   and lahvaccrashframe<0.5 then love.graphics.draw (lahvac_24, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif lahvaccrashframe>0.5 and lahvaccrashframe<1 then love.graphics.draw (lahvac_25, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashframe>1 and lahvaccrashframe<1.5 then love.graphics.draw (lahvac_26, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										elseif block.name=="lahvac" and lahvacstate=="crashed" then 
												--crashed
													if lahvaccrashedframe>0   and lahvaccrashedframe<0.25 then love.graphics.draw (lahvac_27, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif lahvaccrashedframe>0.25 and lahvaccrashedframe<0.5 then love.graphics.draw (lahvac_28, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashedframe>0.5 and lahvaccrashedframe<0.75 then love.graphics.draw (lahvac_29, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashedframe>0.75 and lahvaccrashedframe<1 then love.graphics.draw (lahvac_30, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashedframe>1 and lahvaccrashedframe<1.25 then love.graphics.draw (lahvac_31, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashedframe>1.25 and lahvaccrashedframe<1.5 then love.graphics.draw (lahvac_32, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif lahvaccrashedframe>1.5 and lahvaccrashedframe<1.75 then love.graphics.draw (lahvac_33, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="mala" then 
													if malaframe>0 and malaframe<0.5 then love.graphics.draw (mala_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>0.5 and malaframe<1 then love.graphics.draw (mala_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>1 and malaframe<1.5 then love.graphics.draw (mala_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>1.5 and malaframe<2 then love.graphics.draw (mala_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>2 and malaframe<2.5 then love.graphics.draw (mala_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>2.5 and malaframe<3 then love.graphics.draw (mala_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif malaframe>3 and malaframe<3.5 then love.graphics.draw (mala_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="oka" then 
													if okaframe>0 and okaframe<0.5 then love.graphics.draw (oka_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>0.5 and okaframe<1 then love.graphics.draw (oka_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>1 and okaframe<1.5 then love.graphics.draw (oka_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>1.5 and okaframe<2 then love.graphics.draw (oka_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>2 and okaframe<2.5 then love.graphics.draw (oka_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>2.5 and okaframe<3 then love.graphics.draw (oka_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>3 and okaframe<3.5 then love.graphics.draw (oka_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>3.5 and okaframe<4 then love.graphics.draw (oka_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>4 and okaframe<4.5 then love.graphics.draw (oka_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>4.5 and okaframe<5 then love.graphics.draw (oka_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>5 and okaframe<5.5 then love.graphics.draw (oka_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>5.5 and okaframe<6 then love.graphics.draw (oka_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>6 and okaframe<6.5 then love.graphics.draw (oka_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>6.5 and okaframe<7 then love.graphics.draw (oka_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif okaframe>7 and okaframe<7.5 then love.graphics.draw (oka_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="sklena" then
													if sklenaframe>0 and sklenaframe<0.5 then love.graphics.draw (sklena_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>0.5 and sklenaframe<1 then love.graphics.draw (sklena_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>1 and sklenaframe<1.5 then love.graphics.draw (sklena_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>1.5 and sklenaframe<2 then love.graphics.draw (sklena_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>2 and sklenaframe<2.5 then love.graphics.draw (sklena_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>2.5 and sklenaframe<3 then love.graphics.draw (sklena_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>3 and sklenaframe<3.5 then love.graphics.draw (sklena_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>3.5 and sklenaframe<4 then love.graphics.draw (sklena_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>4 and sklenaframe<4.5 then love.graphics.draw (sklena_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>4.5 and sklenaframe<5 then love.graphics.draw (sklena_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>5 and sklenaframe<5.5 then love.graphics.draw (sklena_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif sklenaframe>5.5 and sklenaframe<6 then love.graphics.draw (sklena_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="horni_tvor" then
													if horni_tvorframe>0 and horni_tvorframe<0.5 then love.graphics.draw (horni_tvor_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>0.5 and horni_tvorframe<1 then love.graphics.draw (horni_tvor_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>1 and horni_tvorframe<1.5 then love.graphics.draw (horni_tvor_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>1.5 and horni_tvorframe<2 then love.graphics.draw (horni_tvor_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>2 and horni_tvorframe<2.5 then love.graphics.draw (horni_tvor_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>2.5 and horni_tvorframe<3 then love.graphics.draw (horni_tvor_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
												end
									elseif block.name=="spodni_tvor" then
													if horni_tvorframe>0 and horni_tvorframe<0.5 then love.graphics.draw (spodni_tvor_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>0.5 and horni_tvorframe<1 then love.graphics.draw (spodni_tvor_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>1 and horni_tvorframe<1.5 then love.graphics.draw (spodni_tvor_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>1.5 and horni_tvorframe<2 then love.graphics.draw (spodni_tvor_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>2 and horni_tvorframe<2.5 then love.graphics.draw (spodni_tvor_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif horni_tvorframe>2.5 and horni_tvorframe<3 then love.graphics.draw (spodni_tvor_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									 
									elseif block.name=="eye" then love.graphics.draw (oko_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lobster" then love.graphics.draw (lobster, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mrtvolka" then love.graphics.draw (mrtvolka_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="nuz" then love.graphics.draw (nuz, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pilka" then love.graphics.draw (pilka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="switch" then love.graphics.draw (switch, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="syringe" then love.graphics.draw (syringe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								
								end
							end
							
						--level 58
						elseif nLevel==58 then
								if i==1 then
										if block.name=="ocel7" then love.graphics.draw (ocel7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel8" then love.graphics.draw (ocel8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel11" then love.graphics.draw (ocel11, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel12" then love.graphics.draw (ocel12, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel15" then love.graphics.draw (ocel15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel16" then love.graphics.draw (ocel16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="cola" then love.graphics.draw (cola, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="draty" then love.graphics.draw (draty, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hadice" then 
													if hadiceframe>0   and hadiceframe<1 then love.graphics.draw (hadice_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif hadiceframe>1 and hadiceframe<2 then love.graphics.draw (hadice_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="kamna" then love.graphics.draw (kamna, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="matka_a" then love.graphics.draw (matka_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="plutonium1" then love.graphics.draw (plutonium1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="plutonium4" then 
													if plutonium4frame>0   and plutonium4frame<1 then love.graphics.draw (plutonium4_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif plutonium4frame>1 and plutonium4frame<2 then love.graphics.draw (plutonium4_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif plutonium4frame>2 and plutonium4frame<3 then love.graphics.draw (plutonium4_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="podstavec" then
													if podstavecframe>0   and podstavecframe<1 then love.graphics.draw (podstavec_00, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif podstavecframe>1 and podstavecframe<2 then love.graphics.draw (podstavec_01, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif podstavecframe>2 and podstavecframe<3 then love.graphics.draw (podstavec_02, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif podstavecframe>3 and podstavecframe<4 then love.graphics.draw (podstavec_03, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												end

									elseif block.name=="pohon" then 
													if pohonframe>0   and pohonframe<1 then love.graphics.draw (pohon_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>1 and pohonframe<2 then love.graphics.draw (pohon_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pohonframe>2 and pohonframe<3 then love.graphics.draw (pohon_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif pohonframe>3 and pohonframe<4 then love.graphics.draw (pohon_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>4 and pohonframe<5 then love.graphics.draw (pohon_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>5 and pohonframe<6 then love.graphics.draw (pohon_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>6 and pohonframe<7 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>7 and pohonframe<8 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>8 and pohonframe<9 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>9 and pohonframe<10 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>10 and pohonframe<11 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>11 and pohonframe<12 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>12 and pohonframe<13 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>13 and pohonframe<14 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>14 and pohonframe<15 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>15 and pohonframe<16 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>16 and pohonframe<17 then love.graphics.draw (pohon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>17 and pohonframe<18 then love.graphics.draw (pohon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>18 and pohonframe<19 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>19 and pohonframe<20 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>20 and pohonframe<21 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>21 and pohonframe<22 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>22 and pohonframe<23 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>23 and pohonframe<24 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>24 and pohonframe<25 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>25 and pohonframe<26 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>26 and pohonframe<27 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>27 and pohonframe<28 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>28 and pohonframe<29 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>29 and pohonframe<30 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>30 and pohonframe<31 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>31 and pohonframe<32 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>32 and pohonframe<33 then love.graphics.draw (pohon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif pohonframe>33 and pohonframe<34 then love.graphics.draw (pohon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												--elseif pohonframe>1.0 and pohonframe<1.1 then love.graphics.draw (pohon_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												--elseif pohonframe>1.1 and pohonframe<1.2 then love.graphics.draw (pohon_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												
												end
									elseif block.name=="rura" then love.graphics.draw (rura, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ufo" then 
													if ufoframe>0   and ufoframe<1 then love.graphics.draw (ufo_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>1 and ufoframe<2 then love.graphics.draw (ufo_01, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ufoframe>2 and ufoframe<3 then love.graphics.draw (ufo_02, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ufoframe>3 and ufoframe<4 then love.graphics.draw (ufo_03, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>4 and ufoframe<5 then love.graphics.draw (ufo_04, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>5 and ufoframe<6 then love.graphics.draw (ufo_05, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>6 and ufoframe<7 then love.graphics.draw (ufo_06, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>7 and ufoframe<8 then love.graphics.draw (ufo_07, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>8 and ufoframe<9 then love.graphics.draw (ufo_08, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>9 and ufoframe<10 then love.graphics.draw (ufo_09, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>10 and ufoframe<11 then love.graphics.draw (ufo_10, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												end
									
									elseif block.name=="ufon" then
													if ufoframe>0   and ufoframe<1 then love.graphics.draw (ufon_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>1 and ufoframe<2 then love.graphics.draw (ufon_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ufoframe>2 and ufoframe<3 then love.graphics.draw (ufon_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ufoframe>3 and ufoframe<4 then love.graphics.draw (ufon_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>4 and ufoframe<5 then love.graphics.draw (ufon_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>5 and ufoframe<6 then love.graphics.draw (ufon_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>6 and ufoframe<7 then love.graphics.draw (ufon_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>7 and ufoframe<8 then love.graphics.draw (ufon_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>8 and ufoframe<9 then love.graphics.draw (ufon_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ufoframe>9 and ufoframe<11 then love.graphics.draw (ufon_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												end
									elseif block.name=="volant" then love.graphics.draw (volant, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end

								
									
										--[[	if timer<10 then love.graphics.draw (vendingmachine, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif timer>8 and timer<14 then
											
													if transitionmixdone==false then love.audio.play( transitionmix ) transitionmixdone=true end	--sound of transition
											
															love.graphics.setColor(math.random(0.5,1),math.random(0.5,1),math.random(0.5,1))
															love.graphics.draw (vendingmachine, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+(y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
											elseif timer>14 and timer<20 then
											
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											
													if laundrystarted==false then love.audio.play( washingmachinesound ) laundrystarted=true end		--sound of washingmachine
													if transitionmixdone2==false then love.graphics.draw (laundry, ((x+0.5+1.2+dx-1.1))*tileSize, (y+0.5+1.8+dy-1)*tileSize,washmachinedrumframe*8,0.5,0.5,25,25)
													end
													
											elseif timer>20 and timer<22 then 
												if transitionmixdone2==false then love.audio.play( transitionmix ) transitionmixdone2=true end	--sound of transition
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,0.5,0.5)			
													
													love.graphics.setColor(math.random(0.5,1),math.random(0.5,1),math.random(0.5,1))
													love.graphics.draw (laundry, (math.random(0,0.1)+(x+0.5+1.2+dx-1.1))*tileSize,math.random(0,0.5)+ (y+0.5+1.8+dy-1)*tileSize,washmachinedrumframe*8,0.5,0.5,25,25)
												
											elseif timer>22 then
													
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,0.5,0.5)			
													
													if squirrelframe>0   and squirrelframe<0.1 then love.graphics.draw (wormhole1, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.1 and squirrelframe<0.2 then love.graphics.draw (wormhole2, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.2 and squirrelframe<0.3 then love.graphics.draw (wormhole3, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.3 and squirrelframe<0.4 then love.graphics.draw (wormhole4, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.4 and squirrelframe<0.5 then love.graphics.draw (wormhole5, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												end
											elseif timer>22 then
											
											end--]]
							
						--level 59
						elseif nLevel==59 then
							if i==1 then
								if block.name=="amphora" then
								love.graphics.setColor(1,1,0.5)
										if skin=="classic" then love.graphics.draw (amfora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif skin=="remake" then love.graphics.draw (amfora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/32,dividy/32)
									end
										
										love.graphics.setColor(cs1block)
								elseif block.name=="amphora2" then
										love.graphics.setColor(1,dividx/32,dividy/32)
											if skin=="classic" then love.graphics.draw (amfora_cervena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif skin=="remake" then love.graphics.draw (amfora_cervena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/32,dividy/32)
										end
										love.graphics.setColor(cs1block)
								elseif block.name=="amphora3" then
										if skin=="classic" then love.graphics.draw (amfora_zelena, ((x-0.15+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif skin=="remake" then love.graphics.draw (amfora_zelena, ((x-0.15+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/32,dividy/32)
									end
								 
								elseif block.name=="amphora4" then
									if skin=="classic" then love.graphics.draw (amfora_cervena, ((x-0.15+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/2,dividy/2)
								 elseif skin=="remake" then love.graphics.draw (amfora_cervena, ((x-0.15+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/32,dividy/32)
								 end
								elseif block.name=="seahorse" then
										if skin=="classic" then 
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (konik_00, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (konik_01, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (konik_02, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (konik_03, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif skin=="remake" then
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (seaHorse_00, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/8,dividy/8) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (seaHorse_01, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/8,dividy/8)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (seaHorse_02, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/8,dividy/8)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (seaHorse_03, ((x+dx-1))*tileSize+frgx, (y+dy)*tileSize+frgy,0,dividx/8,dividy/8)
												end
								end
								
								elseif block.name=="mobject" then love.graphics.draw (drak, ((x-2.3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="skullupleft" or block.name=="skullupleft+" or block.name=="skullupright" or block.name=="skullmidright" or block.name=="skullbottom" then
											if skin=="classic" then love.graphics.draw (hlavicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif skin=="remake" then	love.graphics.draw (hlavicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/32,dividy/32)
										end
								elseif block.name=="cross"  then
										if skin=="classic" then
													if totemframe>0   and totemframe<1 then love.graphics.draw (totem_00, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif totemframe>1 and totemframe<2 then love.graphics.draw (totem_01, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif totemframe>2 and totemframe<3 then love.graphics.draw (totem_02, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif totemframe>3 and totemframe<4 then love.graphics.draw (totem_03, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif totemframe>4 and totemframe<5 then love.graphics.draw (totem_04, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif totemframe>5 and totemframe<6 then love.graphics.draw (totem_05, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif skin=="remake" then
													if totemframe>0   and totemframe<1 then love.graphics.draw (totem_00, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8) 
												elseif totemframe>1 and totemframe<2 then love.graphics.draw (totem_01, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8)
												elseif totemframe>2 and totemframe<3 then love.graphics.draw (totem_02, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8)
												elseif totemframe>3 and totemframe<4 then love.graphics.draw (totem_03, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8) 
												elseif totemframe>4 and totemframe<5 then love.graphics.draw (totem_04, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8)
												elseif totemframe>5 and totemframe<6 then love.graphics.draw (totem_05, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/8,dividy/8)
												end
								end
								elseif block.name=="pipe"  then love.graphics.draw (bottles3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2hor"  then love.graphics.draw (bottles35, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="bigskull"  then
										if skin=="classic" then love.graphics.draw (skull_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif skin=="remake" then love.graphics.draw (skull_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/34,dividy/34)
									end
								elseif block.name=="totem2"  then love.graphics.draw (sloupek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="d1"  then love.graphics.draw (drak_m_00, ((x-3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
						
						elseif nLevel>59 then drawobjectsassets60(pb, block,x,y,dx,dy,tileSize,i,gridSize,dividx,dividy)
						end
						
					
					
end
