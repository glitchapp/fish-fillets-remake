-- License CC0 (Creative Commons license) (c) darkfrei, 2022
-- push-blocks

local path = 'assets/players/'

local pb = {}

local function setBlockOutline (block)
	local m = {} -- map of outlines
	local tiles = block.tiles -- list of tiles as {x1,y1, x2,y2, x3,y3 ...}
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if not m[y] then m[y] = {} end
		if not m[y][x] then 
			m[y][x] = {v=true, h=true} 
		else
			m[y][x].v = not m[y][x].v
			m[y][x].h = not m[y][x].h
		end
		
		if not m[y][x+1] then 
			m[y][x+1] = {v=true, h=false} 
		else
			m[y][x+1].v = not m[y][x+1].v
		end
		
		if not m[y+1] then m[y+1] = {} end
		if not m[y+1][x] then 
			m[y+1][x] = {v=false, h=true} 
		else
			m[y+1][x].h = not m[y+1][x].h
		end
	end
	local lines = {}
	for y, xs in pairs (m) do
		for x, tabl in pairs (xs) do
			if m[y][x].v then
				table.insert (lines, {x,y, x,y+1})
			end
			if m[y][x].h then
				table.insert (lines, {x,y, x+1,y})
			end
		end
	end
	block.lines = lines
end

function pb:load (level)
--	print (level.name)
	local width, height = love.graphics.getDimensions()
	self.map = level.map
	self.gridWidth = level.w
	self.gridHeight = level.h
	self.gridSize = math.min(width/(level.w), height/(level.h))
	--[[print ('gridWidth: ' .. self.gridWidth, 
		'gridHeight: ' .. self.gridHeight,
		'gridSize: ' .. self.gridSize)--]]
	
	self.blocks = level.blocks
	for i, block in ipairs (self.blocks) do
		setBlockOutline (block)
	end
	self.agents = level.agents
	for i, agent in ipairs (self.agents) do
		setBlockOutline (agent)
		local filename = path..agent.name..'.png'
		local info = love.filesystem.getInfo(filename)
		if info then
			local image = love.graphics.newImage(filename)
			agent.image = image
			local width, height = image:getDimensions( )
			local scale = self.gridSize/(width/agent.w)
			
			agent.image_scale = scale
			
			agent.image_sx = scale
			agent.image_sy = scale
			agent.image_ox = image:getWidth()/2
			agent.image_oy = image:getHeight()/2
			agent.image_dx = agent.image_ox*agent.image_scale
			agent.image_dy = agent.image_oy*agent.image_scale
		end
	end
	self.activeAgentIndex = 1
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
		
		--silhouette1= love.graphics.newImage("/assets/players/silhouette1.png")
		--silhouette2= love.graphics.newImage("/assets/players/silhouette2.png")
		
end

function pb:switchAgent ()
	self.agent.active = false
	local index = self.activeAgentIndex + 1
	if index > #self.agents then
		index = 1
	end
	self.activeAgentIndex = index
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
end


local function isValueInList (value, list)
	for i, element in ipairs (list) do
		if element == value then return true end
	end
	return false
end

function pb:isBlockToMapCollision (block, dx, dy)
	local x, y = block.x, block.y
	local map = self.map
	for i = 1, #block.tiles-1, 2 do
		local mapX = x + block.tiles[i]   + dx
		local mapY = y + block.tiles[i+1] + dy
		if map[mapY][mapX] then return true end
	end
end

function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end


	
function pb:getCollisionBlocks (blockA, blocks, dx, dy)
	-- agent to map or block to map collision
	if self:isBlockToMapCollision (blockA, dx, dy) then
		return false
	end
	
	for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return false -- cannot move any agent
		end
	end
	
	for i, block in ipairs (self.blocks) do
		if block == blockA then
			-- self collision: do nothing
		elseif isValueInList (block, blocks) then
			-- block is already in list: do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			-- checks if the agent is strong
			if block.heavy and not self.agent.heavy then
				return false
			end
			table.insert (blocks, block)
			
			-- make it deeper!
			if not self:getCollisionBlocks (block, blocks, dx, dy) then
				return false
			end
		end
	end
	return true
end



function pb:getBlocksToMove (agent, dx, dy)
	local blocks = {}
	local canMove = self:getCollisionBlocks (agent, blocks, dx, dy)
	return blocks, canMove
end


function pb:moveAgent (agent, dx, dy)
	self.agent.x = self.agent.x + dx
	self.agent.y = self.agent.y + dy
end

function pb:moveBlocks (blocks, dx, dy)
	for i, block in ipairs (blocks) do
		block.x = block.x + dx
		block.y = block.y + dy
	end
end

function pb:isCollisionBlockToAllBlocks (blockA, dx, dy)
	for i, block in ipairs (self.blocks) do
		if not (block == blockA) 
		and self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

function pb:isCollisionBlockToAllAgents (blockA, dx, dy)
	for i, agent in ipairs (self.agents) do
		if self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return agent -- dead agent :(
		end
	end
	return false
end

function pb:fallBlocks (blocks)
	local dx, dy = 0, 1 -- no horizontal speed, but positive (down) vertical
	for i = 1, self.gridWidth do
		for i, block in ipairs (blocks) do
			if self:isBlockToMapCollision (block, dx, dy) then
				-- not falling
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllBlocks (block, dx, dy) then
				block.deadly = false
				-- collision to block: probably not falling
			elseif block.deadly and self:isCollisionBlockToAllAgents (block, dx, dy) then
				local deadAgent = self:isCollisionBlockToAllAgents (block, dx, dy)
				deadAgent.dead = true
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllAgents (block, dx, dy) then
				-- the block is on fish
			else
				-- sure falling
				block.x = block.x + dx -- never changes
				block.y = block.y + dy
				block.deadly = true
			end
		end
	end
end

function pb:mainMoving (dx, dy)
	local agent = self.agent -- active agent
	if dx > 0 then 
		agent.direction = "right"
	elseif dx < 0 then
		agent.direction = "left"
	end
	local blocks, canMove = self:getBlocksToMove (agent, dx, dy)
	if canMove then
		self:moveAgent (agent, dx, dy)
		self:moveBlocks (blocks, dx, dy)
		self:fallBlocks (self.blocks, dx, dy)
	end
end


function pb:keypressedMoving (scancode)
	if scancode == 'w' or scancode == 'a' or scancode == 's' or scancode == 'd' then
		-- d means 1; a means -1; otherwise 0
		local dx = scancode == 'd' and 1 or scancode == 'a' and -1 or 0
		-- s means 1; w means -1; otherwise 0
		local dy = scancode == 's' and 1 or scancode == 'w' and -1 or 0
		pb:mainMoving (dx, dy)
	end
	if scancode == 'right' or scancode == 'left' or scancode == 'up' or scancode == 'down' then
		local dx = scancode == 'right' and 1 or scancode == 'left' and -1 or 0
		local dy = scancode == 'down' and 1 or scancode == 'up' and -1 or 0
		pb:mainMoving (dx, dy)
	end
end

function love.gamepadpressed(joystick,button)

		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		
			if joystick:isGamepadDown("dpleft") then
				dx = -1
				dy = 0
			elseif joystick:isGamepadDown("dpright") then
				dx = 1
				dy = 0
			elseif  joystick:isGamepadDown("dpup") then
				dx = 0
				dy = -1
			elseif joystick:isGamepadDown("dpdown") then
				dx = 0
				dy = 1
			elseif joystick:isGamepadDown("a")  then
			pb:switchAgent ()
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("b")  then
			Talkies.clearMessages() talkies=false
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("y")  then
					if helpison=="yes" then helpison="no"
				elseif helpison=="no" then helpison="yes"
				end
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("start")  then
			dx = 0
			dy = 0
			gamestatus="levelselection"
			elseif joystick:isGamepadDown("back")  then
			dx = 0
			dy = 0
			gamestatus="options"
			end
		    pb:mainMoving (dx, dy)
		end
		
		 if joystick:isGamepad() then
			isgamepad=true
		end
		
end

---------------------------------------------------------------------------------------------------
-- draw
---------------------------------------------------------------------------------------------------

function pb:drawBackgroundGrid ()
	local gridSize = self.gridSize
	local gridWidth = self.gridWidth
	local gridHeight = self.gridHeight
	love.graphics.setLineWidth(1)
	love.graphics.setColor(0.3,0.4,0.4)
	for i = 0, gridWidth do
		love.graphics.line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
	end
	for i = 0, gridHeight do
		love.graphics.line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
	end

end


function pb:drawMap ()
	local map = self.map
	local tileSize = self.gridSize
	love.graphics.setLineWidth(2)
if background=="yes" then
	for y, xs in ipairs (map) do
		for x, value in ipairs (xs) do
			-- value is boolean: true or false
			if value==true then -- map tile
				-- beware of -1
				--love.graphics.setColor(0.5,0.5,0.5)
					if palette==1 then love.graphics.setColor(cs1tile1) 
				elseif palette==2 then love.graphics.setColor(cs2tile1)
				elseif palette==3 then love.graphics.setColor(cs3tile1)
				elseif palette==4 then love.graphics.setColor(cs4tile1)
				elseif palette==5 then love.graphics.setColor(cs5tile1)
				elseif palette==6 then love.graphics.setColor(cs6tile1)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
				else
					if not(nLevel==3) and not(nLevel==4) and not(nLevel==5) and not(nLevel==11) then
						--love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
						love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
					end
				end
				
				--love.graphics.setColor(0.6, 0.58, 0.2)
					if palette==1 then love.graphics.setColor(cs1tile2) 
				elseif palette==2 then love.graphics.setColor(cs2tile2)
				elseif palette==3 then love.graphics.setColor(cs3tile2)
				elseif palette==4 then love.graphics.setColor(cs4tile2)
				elseif palette==5 then love.graphics.setColor(cs5tile2)
				elseif palette==6 then love.graphics.setColor(cs6tile2)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				else
					--if assets==1 or nLevel>1 then
					if not(nLevel==3) and not(nLevel==4) and not(nLevel==5) and not(nLevel==11) then
						--love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
						love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					end
				end
				--love.graphics.setColor(0,0,0)
				
					if debugmode=="yes" and palette==1 then 
            love.graphics.setColor(0,0,0)
					--love.graphics.print("#",(x-1)*tileSize, (y-1)*tileSize) end
					love.graphics.print ((x)..' '..(y), (x-1)*tileSize, (y-1)*tileSize) end
			elseif value=="exit" then -- map tile
			love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)


			--borders
			elseif value=="border" then -- map tile
					if palette==1 then love.graphics.setColor(cs1border1)
                elseif palette==2 then love.graphics.setColor(cs2border1)
                elseif palette==3 then love.graphics.setColor(cs3border1)
                elseif palette==4 then love.graphics.setColor(cs4border1)
                elseif palette==5 then love.graphics.setColor(cs5border1)
                elseif palette==6 then love.graphics.setColor(cs6border1)
                end
                if palette==7 then love.graphics.setColor(cs6border1)
                
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)	
                else
					if not(nLevel==3) and not(nLevel==4) and not(nLevel==5) and not(nLevel==11) then
						--love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
						love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
					end
								--dream:draw(cube1,  (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
              end
              if threeD==true then
                						--dream:draw(cube1,  1, 1,-60, 10,10,10)
              end
				
					if palette==1 then love.graphics.setColor(cs1border2)
                elseif palette==2 then love.graphics.setColor(cs2border2)
                elseif palette==3 then love.graphics.setColor(cs3border2)
                elseif palette==4 then love.graphics.setColor(cs4border2)
                elseif palette==5 then love.graphics.setColor(cs5border2)
                elseif palette==6 then love.graphics.setColor(cs6border2)
                end
                if palette==7 then love.graphics.setColor(cs6border1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				else
					if not(nLevel==3) and not(nLevel==4) and not(nLevel==5) and not(nLevel==11) then
						--love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
						love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					elseif assets==2 and nLevel==1 then
						--love.graphics.draw (level1style1, (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					end
				end
			end
		end
	end
	end
				
				--[[print("Tilesize :" .. tileSize)
				print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight)
				print("=========================================================")--]]

				if extradebug==true and debugmode=="yes" then
				love.graphics.setFont(love.graphics.newFont(20))
				love.graphics.setColor(1,1,1)
								
				love.graphics.print("Tilesize :" .. tileSize ,50,100)
				love.graphics.print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight,50,150)
				end

end

function pb:drawOutline  (block)

	local lines = block.lines
	local tileSize = self.gridSize
	local x, y = block.x-1, block.y-1
	for i, line in ipairs (lines) do
		if nLevel>1 and objectscells=="yes" then
			love.graphics.line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
		end
	end
	
	
end

function pb:drawBlock (block)

	local x, y = block.x, block.y
	local tileSize = self.gridSize
	for i = 1, #block.tiles-1, 2 do
		local dx, dy = block.tiles[i], block.tiles[i+1]
				if objectscells=="yes" then
		-- beware of -1
					if palette==1 then love.graphics.setColor(cs1block) 
				elseif palette==2 then love.graphics.setColor(cs2block)
				elseif palette==3 then love.graphics.setColor(cs3block)
				elseif palette==4 then love.graphics.setColor(cs4block)
				elseif palette==5 then love.graphics.setColor(cs5block)
				elseif palette==6 then love.graphics.setColor(cs6block)
				end
				
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
				else
					if assets==1 then
						love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
					elseif assets==2 then
					
							--if nLevel>1 then
							--love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
					
						if nLevel==1 then
							--[[if block.name=="chair1" then
								love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
							end--]]
							if block.name=="chair1" then
								if i==1 then
									love.graphics.draw (chair1, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
								end
							end
							if block.name=="chair2" then
								if i==1 then
									love.graphics.draw (chair2, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
								end
							end
							if block.name=="steel-pipe-1x81" then
								if i==1 then
								love.graphics.setColor(cs1block) 
									love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
								end
							end
							
							if block.name=="cushion1" then
								if i==1 then
									love.graphics.draw (cushion1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							
							if block.name=="table1" then
								if i==1 then
									love.graphics.draw (table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
						--level 2
						elseif nLevel==2 then
							if block.name=="warning" then
								if i==1 then
									love.graphics.draw (warning, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							-- level 4
						elseif nLevel==4 then
						if block.name=="cpipe" then
								if i==1 then
									love.graphics.draw (cpipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="book1" then
								if i==1 then
									love.graphics.draw (book1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="book2" then
								if i==1 then
									love.graphics.draw (book2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
								if block.name=="book3" then
								if i==1 then
									love.graphics.draw (book3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
								if block.name=="book4" then
								if i==1 then
									love.graphics.draw (book4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
									if block.name=="book5" then
								if i==1 then
									love.graphics.draw (book5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="book6" then
								if i==1 then
									love.graphics.draw (book6, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
								if block.name=="book7" then
								if i==1 then
									love.graphics.draw (book7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="book8" then
								if i==1 then
									love.graphics.draw (book8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="book9" then
								if i==1 then
									love.graphics.draw (book9, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
						if block.name=="bookbig1" then
								if i==1 then
									love.graphics.draw (bookbig1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end

							if block.name=="booksmall1" then
								if i==1 then
									love.graphics.draw (booksmall1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="booksmall2" then
								if i==1 then
									love.graphics.draw (booksmall2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="booksmall3" then
								if i==1 then
									love.graphics.draw (booksmall3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="bookhoriz1" then
								if i==1 then
									love.graphics.draw (bookhoriz1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="bookhoriz2" then
								if i==1 then
									love.graphics.draw (bookhoriz2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end


						elseif nLevel==7 then
						if block.name=="snail7" then
								if i==1 then
									love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="snail72" then
								if i==1 then
									love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
						elseif nLevel==11 then
							if block.name=="pipe11" then
								if i==1 then
									love.graphics.draw (pipe11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="pipe4_11" then
								if i==1 then
									love.graphics.draw (pipe4_11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="snail11" then
								if i==1 then
									love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="bullet11" then
								if i==1 then
									love.graphics.draw (bullet11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							if block.name=="bullet112" then
								if i==1 then
									love.graphics.draw (bullet11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							
						end
					
					end
				end
			
			    end

			
			if threeD==true then
		dream:draw(cube1, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5)
    end
--dream:draw(cube1,  1, 1,-60, 10,10,10)
		
		--dream:draw(cube1, ((x+dx-1)*threeDcellwidth+1) , (y+dy-1)*threeDcellheight, threeDcellz,threeDcellwidth-0.5,threeDcellwidth-0.5,threeDcellwidth-0.5)
	end
end

function pb:drawBlocks ()
	love.graphics.setLineWidth(2)
	love.graphics.setColor(1,1,0.5)
	for i, block in ipairs (self.blocks) do
		-- draw filled block
		love.graphics.setLineWidth(1)
		--love.graphics.setColor(1,1,0.5)
		self:drawBlock (block)
		
		-- outline
		love.graphics.setLineWidth(3)
		if block.heavy then
			love.graphics.setColor(0,1,1)
		else
			love.graphics.setColor(0,1,0)
		end
		self:drawOutline  (block)
	end
	
	-- different colors for different status (screensaver, normal game)
	 
	

		if threeD==true then
			if screensaver==true then 
				love.graphics.setColor(0,1,0.5)
				diorama:rotateY(0.0001)
				dream:draw(diorama, 0, 0, -100,15,15,15)
				
			elseif screensaver==false then
		love.graphics.setColor(blockscolor)
			
    end
    dream:present()
		end
		
		love.graphics.setColor(1,1,1)		
end

function pb:drawDeadAgent (agent)
	local tileSize = self.gridSize 
	local x = (agent.x-1)*tileSize
	local y = (agent.y-1)*tileSize
	local w = agent.w*tileSize
	local h = agent.h*tileSize
	
	love.graphics.line (x, y, x+w, y+h)
	love.graphics.line (x, y+h, x+w, y)
end

local function drawTexture (agent, tileSize)
	
	local sx = agent.image_sx
	if agent.direction and agent.direction == "left" then
		sx = -agent.image_sx
	end

	if shader2==true then 	love.graphics.setColor(0.09,0.27,0.41)
	end
		love.graphics.draw (agent.image, 
		(agent.x-1)*tileSize+agent.image_dx, 
		(agent.y-1)*tileSize+agent.image_dy,
		0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
end

local function drawagentrectangle(agent, tileSize)
	local sx = agent.image_sx
	love.graphics.rectangle ('fill', (agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)

end

function pb:drawAgents ()

	local activeAgent = self.agent
	local tileSize = self.gridSize
	gridSize=self.gridSize
	for i, agent in ipairs (self.agents) do

		if agent.image then
			if agent == activeAgent then
				--love.graphics.setColor(1,1,1)
					if palette==1 then love.graphics.setColor(cs1agent1) 
				elseif palette==2 then love.graphics.setColor(cs2agent1)
				elseif palette==3 then love.graphics.setColor(cs3agent1)
				elseif palette==4 then love.graphics.setColor(cs4agent1)
				elseif palette==5 then love.graphics.setColor(cs5agent1)
				elseif palette==7 then 
				end
				drawTexture (agent, tileSize)
			else
				love.graphics.setColor(0.7,0.7,0.7)
					if palette==1 then love.graphics.setColor(cs1agent2) 
				elseif palette==2 then love.graphics.setColor(cs2agent2)
				elseif palette==3 then love.graphics.setColor(cs3agent2)
				elseif palette==4 then love.graphics.setColor(cs4agent2)
				elseif palette==5 then love.graphics.setColor(cs5agent2)
				elseif palette==6 then love.graphics.setColor(cs6agent2)
				elseif palette==7 then 
				end
				drawTexture (agent, tileSize)
			end
		else
			if agent == activeAgent then
				love.graphics.setColor(1,1,1)
				self:drawBlock (agent)
				
				local x, y = agent.x, agent.y
				love.graphics.setColor (0, 0, 0)
				love.graphics.print (agent.x..' '..agent.y, (agent.x-1)*tileSize, (agent.y-1)*tileSize)
			else
				love.graphics.setColor(0.75,0.75,0.5)
				self:drawBlock (agent)
			end
			
			-- outline
			love.graphics.setLineWidth(3)
			if agent.heavy then
				love.graphics.setColor(0,1,1)
			else
				love.graphics.setColor(0,1,0)
			end
			self:drawOutline  (agent)
		end

		if agent.dead then
			love.graphics.setColor(0,0,0)
			self:drawDeadAgent (agent)
		end
		

	end
	love.graphics.setColor(1,1,1)
end

function pb:drawMouse ()
	local mx, my = love.mouse.getPosition()
	local tileSize = self.gridSize 
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1
	--love.graphics.setColor (0, 1, 0)
	--love.graphics.print (x..' '..y, (x-1)*tileSize, (y-1)*tileSize) -- beware of -1
	--love.graphics.print (mx..' '..my, (x-1)*tileSize, (y-1)*tileSize)
	
		-- control coordinates:	Left: 	X:	1045 - 1145		right 	1175 - 1270	
										--Y: 810 - 905
								--		X_	1113 - 1200		
								--Up	Y:	750 - 840		down	875 - 965
		
		--Player selection coor: Big fish:		X:50 - 242
										--		Y:900 - 995
								--Small fish	X:250 - 440
										
		
		-- Configuration button:X:	1150 - 1226
								--Y:48 - 122
       if love.mouse.isDown(1) then
		if mx>1045 and mx<1145 and my>810 and my<905 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --left 
            dx = -1
            dy = 0
            			pb:mainMoving (dx, dy)
        
        elseif mx>1175 and mx<1270 and my>810 and my<905 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --right
            dx = 1
            dy = 0
            			pb:mainMoving (dx, dy)
        
        elseif mx>631 and mx<1200  and my>875 and my<965 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --down
            dx=0
            dy = 1
            			pb:mainMoving (dx, dy)
        
        elseif mx>1113 and mx<1200 and my>750 and my<840 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --up
            dx=0
            dy = -1
            			pb:mainMoving (dx, dy)
      
      elseif mx>250 and mx<448 and my>900 and my<995 and touchinterfaceison==true and ispaneldown==false then love.timer.sleep( 0.2 ) --Select small fish
      self.agent = self.agents[1]
      elseif mx>50 and mx<242 and my>900 and my<995 and touchinterfaceison==true and ispaneldown==false then love.timer.sleep( 0.2 ) --Select big fish
      self.agent = self.agents[2]
      elseif mx>1150 and mx<1226 and my>48 and my<122 and touchinterfaceison==true and panelright==true then --Options menu
      gamestatus="options"
      elseif mx>1150 and mx<1226 and my>149 and my<224 and touchinterfaceison==true and panelright==true then --levelselection menu
      shader2=false
      gamestatus="levelselection"
      love.timer.sleep( 0.3 )
    
		
  elseif mx>1170 and mx<1225 and my>315 and my<395 and touchinterfaceison==true then if  panelright==true then  touchinterfaceison=false love.timer.sleep( 0.3 ) elseif  touchinterfaceison==false and panelright==true then  touchinterfaceison=true love.timer.sleep( 0.3 ) end --Touch controls on / off
  
  elseif mx>1170 and mx<1225 and my>400 and my<470 and touchinterfaceison==true then if panelright==true then  love.timer.sleep( 0.3 )  panelright=false elseif  panelright==false and touchinterfaceison==true then  love.timer.sleep( 0.3 ) panelright=true end --panel right show / hide
		
elseif mx>500 and mx<600 and my>900 and my<1000 and touchinterfaceison==true then if ispaneldown==true then  love.timer.sleep( 0.3 )  ispaneldown=false elseif  ispaneldown==false and touchinterfaceison==true then  love.timer.sleep( 0.3 ) ispaneldown=true end --panel down show / hide

 --dialogs off
		elseif mx>35 and mx<95 and my>730 and my<807 and talkies==true then talkies=false

--read dialogs
		elseif mx>265 and mx and my>730 and my<807 and talkies==true then Talkies.onAction() love.timer.sleep( 0.2 )
       
		--dialogs on
		elseif  mx>200 and mx<288 and my>830 and my<985 and touchinterfaceison==true and ispaneldown==true and talkies==false then
		talkies=true createcanvas() 
        if nLevel==1 then  Obey.lev1()
		elseif nLevel==2 then  Obey.lev2()
    elseif nLevel==3 then  Obey.lev3()
    elseif nLevel==4 then  Obey.lev4()
    elseif nLevel==5 then  Obey.lev5()
    elseif nLevel==6 then  Obey.lev6()
    elseif nLevel==7 then  Obey.lev7()
		end
      
     
     --sound
    elseif mx>300 and mx<375 and my>900 and my<980 and touchinterfaceison==true and soundon==true and ispaneldown==true and talkies==false then soundon=false  love.timer.sleep( 0.2 ) nLevel=nLevel-1 changelevel() --turn sounndon on
  elseif mx>300 and mx<375 and my>900 and my<980 and touchinterfaceison==true and soundon==false and ispaneldown==true and talkies==false then soundon=true  love.timer.sleep( 0.2 ) nLevel=nLevel-1 changelevel()--turn sounndon on
     
     --music
      elseif mx>400 and mx<473 and my>900 and my<970 and touchinterfaceison==true and ispaneldown==true and talkies==false and musicison==true then musicison=false --turn music off
          --if player:hasEnded() then player:play(true) else player:stop() end
          love.timer.sleep( 0.2 )
          if assets==2 then
          love.audio.stop()
          end
      elseif mx>400 and mx<473 and my>900 and my<970 and touchinterfaceison==true and ispaneldown==true and talkies==false and musicison==false then musicison=true --turn music on
          --if player:hasEnded() then player:play(true) else player:stop()  end
          love.timer.sleep( 0.2 )
          if assets==2 then
          nLevel=nLevel-1
          changelevel()
          end
			

  
  elseif mx>1150 and mx<1226 and my>249 and my<306 and touchinterfaceison==true then if screenoptions==false then screenoptions=true	end --screen options menu
		
      --crteffect
      elseif mx>200 and mx<290 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true then love.timer.sleep( 0.3 ) if shader1==true then shader1=false elseif shader1==false then shader1=true end
      --shadertoy
      elseif mx>400 and mx<490 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true then love.timer.sleep( 0.3 ) if shader2==true then shader2=false elseif shader2==false then shader2=true end
      --change color
      elseif mx>600 and mx<690 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true  then love.timer.sleep( 0.3 ) palette=palette+1 createcanvas() if palette>7 then palette=0 end
      --3d on off
      elseif mx>200 and mx<290 and my>500 and my<560 and touchinterfaceison==true and screenoptions==true then love.timer.sleep( 0.3 ) if threeD==true then threeD=false elseif threeD==false then threeD=true end
      --diorama
      elseif mx>400 and mx<490 and my>500 and my<560 and touchinterfaceison==true and screenoptions==true then love.timer.sleep( 0.3 ) if screensaver==false then screensaver=true shader2=true threeD=true elseif screensaver==true then screensaver=false shader2=false end
      -- screen resolution
      elseif mx>600 and mx<690 and my>500 and my<560 and touchinterfaceison==true then --res=res+1 love.timer.sleep( 0.3 ) changeresolution()
      love.graphics.setColor(1,1,1)
	  love.graphics.rectangle('fill',600,600,300,150)
	  love.graphics.setColor(0,0,0)
      love.graphics.print("This option is still under development",600,620) 
      love.graphics.print("Use the key -R- to change resolution",600,650) 

      -- level editor
      elseif mx>200 and mx<290 and my>700 and my<760 and touchinterfaceison==true then 
       essenceeditor = require ('leveleditor')
      
      touchinterfaceison=false
      leveleditor=true
      gamestatus="game"
      
      -- Close screen options
      elseif mx>400 and mx<470 and my>700 and my<760 and touchinterfaceison==true then screenoptions=false
      end

end
end




                if debugmode=="yes" then
                	love.graphics.setFont(love.graphics.newFont(10))
                love.graphics.print("player1 position: x= " ..atx .. "y=:" .. aty,50,50)						
					if adjacentcells=="yes" then
					--adjayencent cells
					love.graphics.setColor(1,1,0,0.5)
					love.graphics.rectangle('fill', (atx) * 23,(aty) * 23, 23*5, 23*4)
					--love.graphics.rectangle('fill', (f2x) * 23,(f2y) * 23, 23*5, 23*3)
					love.graphics.setColor(1,1,1)
					end
                end



return pb

