function drawlips(agent, tileSize,sx,dt)
--print(lipsiter)
--print(lipstotalpos)
--print(lipsline)
--print(lipsvalue[lipsiter])
--print(lipsitercompleted)
--print(lipstime)
--print(lipstime[lipsiter])

	--fishs talking lipsync
	
		--for i = 1,lipslines, 1 do				-- loop through every line
			for i=lipsiter, lippositions[lipslines], 1 do	-- loop through every mouth position
				if lipstimer<lipstime[lipsiter] then	-- if lipstimer is less than current mouth time
					if fish2status=="talking" then	-- if not swiming and talking
					love.graphics.draw (body_talk_base3x1up, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							if lipsvalue[lipsiter]=="A" then love.graphics.draw (body_talk_base3x1aup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="B" then love.graphics.draw (body_talk_base3x1bup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="C" then love.graphics.draw (body_talk_base3x1cup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="D" then love.graphics.draw (body_talk_base3x1dup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="E" then love.graphics.draw (body_talk_base3x1eup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="F" then love.graphics.draw (body_talk_base3x1fup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="G" then love.graphics.draw (body_talk_base3x1gup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="H" then love.graphics.draw (body_talk_base3x1hup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="X" then love.graphics.draw (body_talk_base3x1xup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						end
					elseif fish1status=="talking" then	-- if not swiming and talking
					love.graphics.draw (body_talk_base4x2up, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							if lipsvalue[lipsiter]=="A" then love.graphics.draw (body_talk_base4x2aup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="B" then love.graphics.draw (body_talk_base4x2bup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="C" then love.graphics.draw (body_talk_base4x2cup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="D" then love.graphics.draw (body_talk_base4x2dup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="E" then love.graphics.draw (body_talk_base4x2eup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="F" then love.graphics.draw (body_talk_base4x2fup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="G" then love.graphics.draw (body_talk_base4x2gup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="H" then love.graphics.draw (body_talk_base4x2hup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif lipsvalue[lipsiter]=="X" then love.graphics.draw (body_talk_base4x2xup, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
						end
					end
				else lipsiter=lipsiter+1					
					if lipsiter>lippositions[lipsline] then lipsiter=lippositions[lipsline] lipstimer=0 end
				end
				if lipsiter==lippositions[lipslines] or lipsiter>lippositions[lipslines] then lipsitercompleted=true end		-- if the lipsync has been completed draw talking fish without lipsync
			end
		--end
		
		if lipsitercompleted==true then
				--draw talking fish
						if fish1status=="talking" then	-- if not swiming and idle
								if bigfishframe>0 and bigfishframe<0.5		then love.graphics.draw (fish4x2cluptalk00, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							elseif bigfishframe>0.5 and bigfishframe<1 		then love.graphics.draw (fish4x2cluptalk01, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							elseif bigfishframe>1 and bigfishframe<1.5 		then love.graphics.draw (fish4x2cluptalk02, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							end
					elseif fish2status=="talking" then
								if smallfishframe>0 and smallfishframe<0.5 	then love.graphics.draw (fish3x1cluptalk00, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							elseif smallfishframe>0.5 and smallfishframe<1 	then love.graphics.draw (fish3x1cluptalk01, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							elseif smallfishframe>1 and smallfishframe<1.5 	then love.graphics.draw (fish3x1cluptalk02, (agent.x-1)*tileSize+agent.image_dx, (agent.y-1)*tileSize+agent.image_dy, 0, -sx, agent.image_sy, agent.image_ox,  agent.image_oy)
							end
					end
		end
	end
	
function drawlipscutscenes(dt)
--print(lipsiter)
--print(lipstimer)
--print(lipsitercompleted)
		
			for i=lipsiter, lippositions[lipslines], 1 do	-- loop through every mouth position
				if lipstimer<lipstime[lipsiter] then	-- if lipstimer is less than current mouth time
					if fish2status=="talking" then	-- if not swiming and talking
															 love.graphics.draw (body_talk_base3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
							if lipsvalue[lipsiter]=="A" then love.graphics.draw (body_talka3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="B" then love.graphics.draw (body_talkb3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="C" then love.graphics.draw (body_talkc3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="D" then love.graphics.draw (body_talkd3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="E" then love.graphics.draw (body_talke3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="F" then love.graphics.draw (body_talkf3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="G" then love.graphics.draw (body_talkg3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="H" then love.graphics.draw (body_talkh3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif lipsvalue[lipsiter]=="X" then love.graphics.draw (body_talkx3x1ul, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						end
					elseif fish1status=="talking" then	-- if not swiming and talking
															 love.graphics.draw (body_talk_base4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
							if lipsvalue[lipsiter]=="A" then love.graphics.draw (body_talka4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="B" then love.graphics.draw (body_talkb4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="C" then love.graphics.draw (body_talkc4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="D" then love.graphics.draw (body_talkd4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="E" then love.graphics.draw (body_talke4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="F" then love.graphics.draw (body_talkf4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="G" then love.graphics.draw (body_talkg4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="H" then love.graphics.draw (body_talkh4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif lipsvalue[lipsiter]=="X" then love.graphics.draw (body_talkx4x2ul, 1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						end
					end
				else lipsiter=lipsiter+1					
					if lipsiter>lippositions[lipsline] then lipsiter=lippositions[lipsline] lipstimer=0 end
					
				end
				if lipsiter==lippositions[lipslines] or lipsiter>lippositions[lipslines] then lipsitercompleted=true end		-- if the lipsync has been completed draw talking fish without lipsync
			end
		--end
			if lipsitercompleted==true then
				--draw talking fish
					if fish1status=="talking" then	-- if not swiming and idle
							if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clultalk00,  1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2clultalk01,  1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clultalk02,  1400-timer*2,250-timer*6,0,0.25+timer/240,0.25+timer/240)
						end
					elseif fish2status=="talking" then
							if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clultalk00, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1clultalk01, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clultalk02, 800-timer*2,250-timer*6,0,(0.25+timer/240)*(-1),0.25+timer/240)
						end
					end
		end
	end
