--This function is entirily created with chatGPT and has not been tested yet.

--[[
This function generates a Sokoban level (a type of puzzle game) with specified dimensions (width and height), numbers of blocks (numBlocks), heavy blocks (numHeavyBlocks), agents (numAgents), and big agents (numBigAgents).

The function initializes the game state by setting the width and height of the game, and creating empty tables for blocks, agents, and big agents.

It then adds the specified number of blocks, agents, and big agents to the game state. The positions of these game elements are determined randomly, but the function checks to ensure that the positions are valid before adding them to the game state.

The function also randomly designates some of the blocks as "heavy" with a 50% chance of being heavy.

After the game state is initialized and the game elements are added, the function applies gravity to the game state, and then attempts to solve the level to ensure that it is solvable. If the level is not solvable, the function retries with a new random configuration until a solvable level is generated.
--]]

function generateLevel(width, height, numBlocks, numHeavyBlocks, numAgents, numBigAgents)
  -- Initialize the game state
  game.width = width
  game.height = height
  game.blocks = {}
  game.agents = {}
  game.bigAgents = {}

  -- Add the blocks and agents to the game state
  local numHeavyBlocksPlaced = 0
  for i = 1, numBlocks do
    local block = {x=0, y=0}
    while not isValidBlockPosition(game, block) do
      block.x = math.random(1, game.width)
      block.y = math.random(1, game.height)
    end
    table.insert(game.blocks, block)
    if numHeavyBlocksPlaced < numHeavyBlocks and math.random() < 0.5 then
      block.heavy = true
      numHeavyBlocksPlaced = numHeavyBlocksPlaced + 1
    end
  end
  for i = 1, numAgents do
    local agent = {x=0, y=0}
    while not isValidAgentPosition(game, agent) do
      agent.x = math.random(1, game.width)
      agent.y = math.random(1, game.height)
    end
    table.insert(game.agents, agent)
  end
  for i = 1, numBigAgents do
    local bigAgent = {x=0, y=0}
    while not isValidAgentPosition(game, bigAgent) do
      bigAgent.x = math.random(1, game.width)
      bigAgent.y = math.random(1, game.height)
    end
    table.insert(game.bigAgents, bigAgent)
  end

  -- Apply gravity to the game state
  applyGravity(game)

  -- Solve the level to ensure that it is solvable
  local solution = solveSokoban()
  while not solution do
    -- Retry with a new random configuration
    for _, block in pairs(game.blocks) do
      block.x = 0
      block.y = 0
    end
    for _, agent in pairs(game.agents) do
      agent.x = 0
      agent.y = 0
    end
    for _, bigAgent in pairs(game.bigAgents) do
      bigAgent.x = 0
      bigAgent.y = 0
    end
    numHeavyBlocksPlaced = 0
    for i = 1, numBlocks do
      local block = {x=0, y=0}
      while not isValidBlockPosition(game, block) do
        block.x = math.random(1, game.width)
        block.y = math.random(1, game.height)
      end
      table.insert(game.blocks, block)
      if numHeavyBlocksPlaced < numHeavyBlocks and math.random() < 0.5 then
        block.heavy = true
        numHeavyBlocksPlaced = numHeavyBlocksPlaced + 1
      end
    end
   
