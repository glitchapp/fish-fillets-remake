-- Function to draw Holy Grails and accept the pb object as an argument
function drawHolyGrails(pb, level, totalHolyGrails, x, dx, y, dy, tileSize, frgx, frgy, dividx, dividy)
    -- Save the current color
    local r, g, b, a = love.graphics.getColor()

    -- Check each Holy Grail
    for i = 1, totalHolyGrails do
        local grailName = "gral" .. i
        local isGrailInExitArea = pb:isObjectInExitArea(grailName, level.blocks)
		
        -- Debug: Print grail name and whether it's in the exit area
        --print("Grail Name:", grailName)
        --print("In Exit Area:", isGrailInExitArea)

        -- Set the color based on whether the Holy Grail is in the exit area
        local drawColor = {1, 1, 1}  -- Default color is white

        if isGrailInExitArea then
            --drawColor = {1, 0, 0}  -- Set color to red if in the exit area
        end

        -- Debug: Print the color
        --print("Draw Color:", drawColor[1], drawColor[2], drawColor[3])

        -- Set the color for drawing
        love.graphics.setColor(unpack(drawColor))

        -- Debug: Print that we are about to draw the Holy Grail
        --print("Drawing Holy Grail:", grailName)

        -- Draw the Holy Grail
        for _, block in ipairs(level.blocks) do
            if block.name == grailName then
          
                if isGrailInExitArea then
                --debug statements
					--[[print("Actually Drawing Holy Grail:", grailName)
					print("the grail " .. grailName .."is ")
					print("In Exit Area:", isGrailInExitArea)
					--]]
					love.graphics.draw(gral_00, ((x + dx - 1)) * tileSize + frgx, (y + dy - 1) * tileSize + frgy, 0, dividx, dividy)
                end
            end
        end

        -- Reset the color to white after drawing each Holy Grail
        love.graphics.setColor(1, 1, 1)
    end

    -- Restore the previous color
    love.graphics.setColor(r, g, b, a)
end
















function drawobjectsassets60(pb, block,x,y,dx,dy,tileSize,i,gridSize,dividx,dividy)
							
						--level 60
						if nLevel==60 then
						
	local lightPos1 = {0.1, 0.1, 0.075}
	local lightColor = {red, green, blue, 8}

		-- Secondary light
				-- Light position 2
				local lightPos2 = { 0.5, 0.5, 0.075}

				local lightColor2 = {red, green, blue, 8}
					
		 vectorNormalsNoiseShadowEnvmapShader:send("LightPos1", lightPos1)
		 vectorNormalsNoiseShadowEnvmapShader:send("LightColor1", lightColor)
		
		vectorNormalsNoiseShadowEnvmapShader:send("LightPos2", lightPos2)
		vectorNormalsNoiseShadowEnvmapShader:send("LightColor2", lightColor2)
		
        
        -- Set other shader parameters for normal mapping
        vectorNormalsNoiseShadowEnvmapShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        vectorNormalsNoiseShadowEnvmapShader:send("Falloff", {0.2, 1, 0.5})
        -- Ambient color
		vectorNormalsNoiseShadowEnvmapShader:send("AmbientColor", {red * 0.2, green * 0.2, blue * 0.3, 1.5})

		-- Reflection intensity
		vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", 5.8)
		vectorNormalsNoiseShadowEnvmapShader:send("u_normals", gem1n)
		vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", gem1h)
		vectorNormalsNoiseShadowEnvmapShader:send("u_envMap", envMap)
		
							love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
								if i==1 then
											if block.name=="krystal_00" then love.graphics.setColor(1,1,1,1) 
										elseif block.name=="krystal_04" then love.graphics.setColor(1,0,1,1) 
										elseif block.name=="krystal_08" then love.graphics.setColor(1,1,0,1) 
										elseif block.name=="krystal_12" then love.graphics.setColor(1,0,0,1) 
										elseif block.name=="krystal_16" then love.graphics.setColor(0,1,1,1) 
										elseif block.name=="krystal_20" then love.graphics.setColor(0,1,0) 
										elseif block.name=="krystal_24" then love.graphics.setColor(0.5,1,1,0.5) 
										end
										if block.name=="krystal_00" or block.name=="krystal_04" or block.name=="krystal_08" or block.name=="krystal_12" or block.name=="krystal_16" or block.name=="krystal_20" or block.name=="krystal_24" then
												--if skin=="classic" then love.graphics.draw (gem1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											--elseif skin=="remake" then
											love.graphics.draw (gem1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
											--end
										end
										love.graphics.setColor(1,1,1,1) 
										
										if block.name=="krystal_00" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_04" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_08" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_12" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_16" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_20" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_24" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_26, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_27, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_24, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_25, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									end
									--]]
									love.graphics.setShader(causticsAndNormals)
								end
								
						--level 61
						elseif nLevel==61 then
								if i==1 then
										if block.name=="krystal_00" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_04" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_08" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_12" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_16" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_20" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="krystal_24" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_26, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_27, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_24, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_25, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16)
												end
									elseif block.name=="bigcup"	 and skin=="classic"  then love.graphics.draw (gral, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="bigcup"	 and skin=="remake"  then love.graphics.draw (gral, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="cupsmall" and skin=="classic"  then love.graphics.draw (vazavh, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="cupsmall" and skin=="remake"  then love.graphics.draw (vazavh, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="bigobject"	and skin=="classic" then love.graphics.draw (truhla2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="bigobject"	and skin=="remake" then love.graphics.draw (truhla2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="crown" and skin=="classic"			  then love.graphics.draw (korunka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="crown" and skin=="remake"			  then love.graphics.draw (korunka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="crown2"	and skin=="classic"			  then love.graphics.draw (korunka1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="crown2"	and skin=="remake"			  then love.graphics.draw (korunka1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="crownsmall"	and skin=="classic"		  then 
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (koruna_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (koruna_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (koruna_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (koruna_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (koruna_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (koruna_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="crownsmall"	and skin=="remake"		  then 
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (koruna_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (koruna_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (koruna_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (koruna_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (koruna_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (koruna_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="pipe" and skin=="classic" then love.graphics.draw (pipe, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="pipe" and skin=="remake" then love.graphics.draw (pipe, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipehor" and skin=="classic"  then love.graphics.draw (pipehor, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="pipehor" and skin=="remake"  then love.graphics.draw (pipehor, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="coins"  and skin=="classic"		  then love.graphics.draw (coins, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="coins"  and skin=="remake"		  then love.graphics.draw (coins, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="prsten2"	and skin=="classic"	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="prsten2"	and skin=="remake"	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="prsten3"	and skin=="classic"	 	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten3_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten3_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten3_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten3_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten3_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="prsten3"	and skin=="classic"	 	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten3_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten3_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten3_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten3_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten3_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									--love.graphics.draw (ring, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stolek2" 	and skin=="classic" then love.graphics.draw (stolek2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="stolek2" 	and skin=="remake" then love.graphics.draw (stolek2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
								
						--level 62
						elseif nLevel==62 then
								if i==1 then
											if block.name=="krystal_00" then love.graphics.setColor(1,1,1,1) 
										elseif block.name=="krystal_04" then love.graphics.setColor(1,0,1,1) 
										elseif block.name=="krystal_08" then love.graphics.setColor(1,1,0,1) 
										elseif block.name=="krystal_12" then love.graphics.setColor(1,0,0,1) 
										elseif block.name=="krystal_16" then love.graphics.setColor(0,1,1,1) 
										elseif block.name=="krystal_20" then love.graphics.setColor(0,1,0) 
										elseif block.name=="krystal_24" then love.graphics.setColor(0.5,1,1,0.5) 
										end
										if block.name=="krystal_00" or block.name=="krystal_04" or block.name=="krystal_08" or block.name=="krystal_12" or block.name=="krystal_16" or block.name=="krystal_20" or block.name=="krystal_24" then love.graphics.draw (gem1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/16,dividy/16) end
										love.graphics.setColor(1,1,1,1) 
										
								
										if block.name=="koruna" then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (koruna_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (koruna_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (koruna_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (koruna_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (koruna_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (koruna_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="candlestick" then love.graphics.draw (candlestick, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
									elseif block.name=="amfora" then love.graphics.draw (amfora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="amfora_a" then love.graphics.draw (amfora_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="amfora_cervena_a" then love.graphics.draw (amfora_cervena_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="amfora_zelena"  then love.graphics.draw (amfora_zelena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="amfora_zelena_a"  then love.graphics.draw (amfora_zelena_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="drahokam"  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (drahokam_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (drahokam_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (drahokam_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (drahokam_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (drahokam_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (drahokam_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end			
									elseif block.name=="drahokam_b_00"  then love.graphics.draw (drahokam_b_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="drahokam_b_01"  then love.graphics.draw (drahokam_b_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel3"  then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel7"  then love.graphics.draw (ocel7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="krystal_00" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_04" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_08" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_12" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_16" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_20" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="krystal_24" then
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_26, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_27, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_24, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_25, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="prsten2"		 	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									elseif block.name=="prsten3"		 	  then
													if korunaframe>0   and korunaframe<0.25 then love.graphics.draw (prsten3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif korunaframe>0.25 and korunaframe<0.5 then love.graphics.draw (prsten3_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.5 and korunaframe<0.75 then love.graphics.draw (prsten3_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>0.75 and korunaframe<1 then love.graphics.draw (prsten3_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1 and korunaframe<1.25 then love.graphics.draw (prsten3_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif korunaframe>1.25 and korunaframe<1.5 then love.graphics.draw (prsten3_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									end
								end
						
						--level 63
						elseif nLevel==63 then
								if i==1 then
										if block.name=="amfora"  then love.graphics.draw (amfora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="das"  then
													if dasfishframe>0   and dasfishframe<0.5 then love.graphics.draw (das_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif dasfishframe>0.5 and dasfishframe<1 then love.graphics.draw (das_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>1 and dasfishframe<1.5 then love.graphics.draw (das_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>1.5 and dasfishframe<2 then love.graphics.draw (das_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>2 and dasfishframe<2.5 then love.graphics.draw (das_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>2.5 and dasfishframe<3 then love.graphics.draw (das_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>3 and dasfishframe<3.5 then love.graphics.draw (das_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>3.5 and dasfishframe<4 then love.graphics.draw (das_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif dasfishframe>4 and dasfishframe<4.5 then love.graphics.draw (das_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									
									elseif block.name=="krapnik3"  then love.graphics.draw (krapnik3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="muslicka"  then love.graphics.draw (muslicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="netopejr"  then love.graphics.draw (netopejr_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="rybicka_h"  then love.graphics.draw (rybicka_h_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="tyc"  then love.graphics.draw (tyc_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="vaza_cervena"  then love.graphics.draw (vaza_cervena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="vazav"  then love.graphics.draw (vazav_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="zahavec"  then love.graphics.draw (zahavec_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									end

								end
						
						--level 64
						elseif nLevel==64 then
								if i==1 then
										if block.name=="ocel8"  then love.graphics.draw (ocel8, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel12"  then love.graphics.draw (ocel12, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel16"  then love.graphics.draw (ocel16, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel22"  then love.graphics.draw (ocel22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel27"  then love.graphics.draw (ocel27, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel29"  then love.graphics.draw (ocel29, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel30"  then love.graphics.draw (ocel30, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel33"  then love.graphics.draw (ocel33, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel35"  then love.graphics.draw (ocel35, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel40"  then love.graphics.draw (ocel40, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="ocel43"  then love.graphics.draw (ocel43, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="aura"  then love.graphics.draw (aura_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									end
									
									
									--draw holy grails
									
									--[[
										if block.name=="gral1"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral2"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral3"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral4"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral5"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral6"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral7"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral8"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral9"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral10"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral11"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral12"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral13"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral14"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral15"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral16"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral17"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral18"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral19"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral20"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral21"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral22"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral23"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral24"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="gral25"  then love.graphics.draw (gral_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									end
									--]]

									if block.name=="gral1" 
									or block.name=="gral2"
									or block.name=="gral3"
									or block.name=="gral4"
									or block.name=="gral5"
									or block.name=="gral6"
									or block.name=="gral7"
									or block.name=="gral8"
									or block.name=="gral9"
									or block.name=="gral10"
									or block.name=="gral11"
									or block.name=="gral12"
									or block.name=="gral13"
									or block.name=="gral14"
									or block.name=="gral15"
									or block.name=="gral16"
									or block.name=="gral17"
									or block.name=="gral18"
									or block.name=="gral19"
									or block.name=="gral20"
									or block.name=="gral21"
									or block.name=="gral22"
									or block.name=="gral23"
									or block.name=="gral24"
									or block.name=="gral25"
									then	drawHolyGrails(pb, level, totalHolyGrails, x, dx, y, dy, tileSize, frgx, frgy, dividx, dividy)
									
													if krystalframe>0   and krystalframe<0.25 then love.graphics.draw (krystal_00, ((x+dx-1))*tileSize+frgx+krystalMovement, (y+dy-1)*tileSize+frgy,0,dividx*4,dividy*4) 
												elseif krystalframe>0.25 and krystalframe<0.5 then love.graphics.draw (krystal_01, ((x+dx-1))*tileSize+frgx+krystalMovement, (y+dy-1)*tileSize+frgy,0,dividx*4,dividy*4)
												elseif krystalframe>0.5 and krystalframe<0.75 then love.graphics.draw (krystal_02, ((x+dx-1))*tileSize+frgx+krystalMovement, (y+dy-1)*tileSize+frgy,0,dividx*4,dividy*4)
												elseif krystalframe>0.75 and krystalframe<1 then love.graphics.draw (krystal_03, ((x+dx-1))*tileSize+frgx+krystalMovement, (y+dy-1)*tileSize+frgy,0,dividx*4,dividy*4)
												end
									end
								end
								
						--level 65
						elseif nLevel==65 then
								if i==1 then
										if block.name=="ocel12" then love.graphics.draw (ocel12, ((x+dx-10))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ocel13" then love.graphics.draw (ocel13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="vozik" then love.graphics.draw (vozik, ((x+dx-15))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="elko1o" then love.graphics.draw (elko1o, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lodo" then love.graphics.draw (lodo, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lods" then love.graphics.draw (lods, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zidle1l" then love.graphics.draw (zidle1l, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zidle1s" then love.graphics.draw (zidle1s, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="zidle2l" then love.graphics.draw (zidle2l, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="dlouha" then love.graphics.draw (dlouha, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ctverec" then love.graphics.draw (ctverec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
						
						--level 66
						elseif nLevel==66 then
								if i==1 then
										if block.name=="jpak1" then love.graphics.draw (jpak1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="jpak2" then love.graphics.draw (jpak2, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="jpak3" then love.graphics.draw (jpak3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="jphero" then love.graphics.draw (jphero, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ball" then love.graphics.draw (ball, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="ball2" then love.graphics.draw (ball2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hwhero" then love.graphics.draw (hwhero, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hwlife" then love.graphics.draw (hwlife, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hwtron" then love.graphics.draw (hwtron, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)

									elseif block.name=="pois" then love.graphics.draw (pois, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									elseif block.name=="mminer" and timer2<0.5 then love.graphics.draw (mminer, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mminer" and timer2>0.5 and timer2<1 then love.graphics.draw (mminer2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mminer" and timer2>1 and timer2<1.5 then love.graphics.draw (mminer, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mminer" and timer2>1.5 and timer2<2 then love.graphics.draw (mminer4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									elseif block.name=="hwzone" then love.graphics.draw (hwzone, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="jppodkl" then love.graphics.draw (jppodkl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hwscore" then love.graphics.draw (hwscore, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									elseif block.name=="robot" and timer2<0.5 then love.graphics.draw (robot, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="robot" and timer2>0.5 and timer2<1 then love.graphics.draw (robot2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="robot" and timer2>1 and timer2<1.5 then love.graphics.draw (robot3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="robot" and timer2>1.5 and timer2<2 then love.graphics.draw (robot4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)

									elseif block.name=="knight" and timer <6 then love.graphics.draw (knight, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >6 	and timer<7 then love.graphics.draw (knight2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >7 and timer<8 then love.graphics.draw (knight, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >8 	and timer<8.5 then love.graphics.draw (knight2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >8.5  and timer<9 then love.graphics.draw (knight3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >9 	and timer<9.5 then love.graphics.draw (knight4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >9.5 	and timer<10 then love.graphics.draw (knight5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >10 	and timer<10.5 then love.graphics.draw (knight6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="knight" and timer >10.5 then love.graphics.draw (knight7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									
									end
								end
								
						--level 67
						elseif nLevel==67 then
								if i==1 then
										if block.name=="balista" then love.graphics.draw (balista, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="dul" then love.graphics.draw (dul, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="exit" then love.graphics.draw (exitwarcraft, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="hradt" then love.graphics.draw (hradt, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="jezdec" then love.graphics.draw (jezdec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="kopi" then love.graphics.draw (kopi, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="lucistnik" then love.graphics.draw (lucistnik, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="peasant" then love.graphics.draw (peasant, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="peasantl" then love.graphics.draw (peasantl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="surrend" then love.graphics.draw (surrend, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="vez" then love.graphics.draw (vez, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
								
						elseif nLevel==68 then
								if i==1 then
											if block.name=="icon1" then love.graphics.draw (icon1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon2" then love.graphics.draw (icon2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon3" then love.graphics.draw (icon3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon4" then love.graphics.draw (icon4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon5" then love.graphics.draw (icon5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon6" then love.graphics.draw (icon6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon7" then love.graphics.draw (icon7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon8" then love.graphics.draw (icon8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon9" then love.graphics.draw (icon9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon10" then love.graphics.draw (icon10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon11" then love.graphics.draw (icon11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon12" then love.graphics.draw (icon12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon13" then love.graphics.draw (icon13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon14" then love.graphics.draw (icon14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon15" then love.graphics.draw (icon15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon16" then love.graphics.draw (icon16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon17" then love.graphics.draw (icon17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon18" then love.graphics.draw (icon18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon19" then love.graphics.draw (icon19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon20" then love.graphics.draw (icon20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="icon21" then love.graphics.draw (icon21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										
										elseif block.name=="altar" then love.graphics.draw (altar, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/1.9,dividy/2.2)
										elseif block.name=="bonus" then love.graphics.draw (bonus, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/1.9,dividy/1.9)
										elseif block.name=="color" then love.graphics.draw (color, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2.2,dividy/2.2)
										elseif block.name=="dotaz" then love.graphics.draw (dotaz, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/1.9,dividy/2.2)
										elseif block.name=="notepad" then love.graphics.draw (notepad, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2.35,dividy/1.9)
										elseif block.name=="tento" then love.graphics.draw (tento, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/1.9,dividy/1.9)
										elseif block.name=="oldsvicka" then love.graphics.draw (oldsvicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/1.9,dividy/1.9)
																				
										
										elseif block.name=="fish1-2" then love.graphics.draw (fish4x2_100, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="fish2-2" then love.graphics.draw (fish3x1_100, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										
										end
								end
					elseif nLevel==69 then
								if i==1 then
											if block.name=="procesor1" then love.graphics.draw (procesor1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor2" then love.graphics.draw (procesor2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor3" then love.graphics.draw (procesor3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor4" then love.graphics.draw (procesor4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor5" then love.graphics.draw (procesor5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor6" then love.graphics.draw (procesor6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor7" then love.graphics.draw (procesor7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor8" then love.graphics.draw (procesor8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="procesor9" then love.graphics.draw (procesor9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										
										elseif block.name=="val0" then love.graphics.draw (val0, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val1" then love.graphics.draw (val1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val2" then love.graphics.draw (val2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val3" then love.graphics.draw (val3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val4" then love.graphics.draw (val4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val5" then love.graphics.draw (val5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val6" then love.graphics.draw (val6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="val7" then love.graphics.draw (val7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="valspec" then love.graphics.draw (valspec, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										end
								end
					elseif nLevel==70 then			
									if i==1 then
											if block.name=="diskette" then love.graphics.draw (diskette, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="ocel11" then love.graphics.draw (ocel11, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="ocel13" then love.graphics.draw (ocel13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="ocel14" then love.graphics.draw (ocel14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="chip1" then love.graphics.draw (chip1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="klika" then love.graphics.draw (klika, ((x+dx-3))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="souc" then love.graphics.draw (souc, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="svab" then love.graphics.draw (svab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif block.name=="vir" then love.graphics.draw (vir_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										--elseif block.name=="virus" then love.graphics.draw (virus_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										end
									end
					-- level 71
					elseif nLevel==71 then
						if i==1 then
									if block.name=="alienmagnet" then love.graphics.draw (alienmagnet_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="can" then love.graphics.draw (can, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="cristall" then love.graphics.draw (cristall, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="damagewall" then love.graphics.draw (damagewall_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="itemnameda" then love.graphics.draw (itemnameda, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="itemnamedb" then love.graphics.draw (itemnamedb, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="itemnamedc" then love.graphics.draw (itemnamedc, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="itemnamedd" then love.graphics.draw (itemnamedd, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="itemnamede" then love.graphics.draw (itemnamede, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="killozap" then love.graphics.draw (killozap, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="lever" then love.graphics.draw (lever_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="magnetsmall" then love.graphics.draw (magnetsmall, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="plutonium" then love.graphics.draw (plutonium_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="powerline" then love.graphics.draw (powerline, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="screwnut" then love.graphics.draw (screwnut, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="steel1" then love.graphics.draw (steel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="steelbig" then love.graphics.draw (steelbig, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="thingy" then love.graphics.draw (thingy, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
						end
					--level 72
					elseif nLevel==72 then
						if i==1 then
									if block.name=="antena" then love.graphics.draw (antena, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="f_00" 	then love.graphics.draw (f_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="konik"	then
														--if konikframe==0 then love.graphics.draw (konik_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
													--elseif konikframe==1 then love.graphics.draw (konik_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													--elseif konikframe==2 then love.graphics.draw (konik_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													--end
													if seahorseframe>0   and seahorseframe<0.5 then love.graphics.draw (seahorse00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/5,dividy/3) 
												elseif seahorseframe>0.5 and seahorseframe<1 then love.graphics.draw (seahorse01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/5,dividy/3)
												elseif seahorseframe>1 and seahorseframe<1.5 then love.graphics.draw (seahorse02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/5,dividy/3)
												elseif seahorseframe>1.5 and seahorseframe<2 then love.graphics.draw (seahorse03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/5,dividy/3)
												end
								elseif block.name=="koral" 	then love.graphics.draw (koral, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="krab" 	then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek" then love.graphics.draw (micek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								--[[elseif block.name=="micek_00" then love.graphics.draw (micek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek_01" then love.graphics.draw (micek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek_02" then love.graphics.draw (micek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek_03" then love.graphics.draw (micek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek_04" then love.graphics.draw (micek_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="micek_05" then love.graphics.draw (micek_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								--]]
								elseif block.name=="dole" then
														if nahoreframe==0 then love.graphics.draw (dole_00, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
													elseif nahoreframe==1 then love.graphics.draw (dole_01, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==2 then love.graphics.draw (dole_02, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==3 then love.graphics.draw (dole_03, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==4 then love.graphics.draw (dole_04, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==5 then love.graphics.draw (dole_05, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==6 then love.graphics.draw (dole_06, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==7 then love.graphics.draw (dole_07, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==8 then love.graphics.draw (dole_08, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==9 then love.graphics.draw (dole_09, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==10 then love.graphics.draw (dole_10, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==11 then love.graphics.draw (dole_11, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==12 then love.graphics.draw (dole_12, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==13 then love.graphics.draw (dole_13, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==14 then love.graphics.draw (dole_14, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==15 then love.graphics.draw (dole_15, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==16 then love.graphics.draw (dole_16, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==17 then love.graphics.draw (dole_17, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==18 then love.graphics.draw (dole_18, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==19 then love.graphics.draw (dole_19, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==20 then love.graphics.draw (dole_20, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==21 then love.graphics.draw (dole_21, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==22 then love.graphics.draw (dole_22, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==23 then love.graphics.draw (dole_23, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													end
								elseif block.name=="nahore" then
														if nahoreframe==0 then love.graphics.draw (nahore_00, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
													elseif nahoreframe==1 then love.graphics.draw (nahore_01, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==2 then love.graphics.draw (nahore_02, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==3 then love.graphics.draw (nahore_03, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==4 then love.graphics.draw (nahore_04, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==5 then love.graphics.draw (nahore_05, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==6 then love.graphics.draw (nahore_06, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==7 then love.graphics.draw (nahore_07, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==8 then love.graphics.draw (nahore_08, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==9 then love.graphics.draw (nahore_09, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==10 then love.graphics.draw (nahore_10, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==11 then love.graphics.draw (nahore_11, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==12 then love.graphics.draw (nahore_12, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==13 then love.graphics.draw (nahore_13, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==14 then love.graphics.draw (nahore_14, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==15 then love.graphics.draw (nahore_15, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==16 then love.graphics.draw (nahore_16, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==17 then love.graphics.draw (nahore_17, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==18 then love.graphics.draw (nahore_18, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==19 then love.graphics.draw (nahore_19, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==20 then love.graphics.draw (nahore_20, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==21 then love.graphics.draw (nahore_21, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==22 then love.graphics.draw (nahore_22, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==23 then love.graphics.draw (nahore_23, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													end
												elseif block.name=="dt" then love.graphics.draw (dt, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif block.name=="f_object" then
														if nahoreframe==0 then love.graphics.draw (f_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
													elseif nahoreframe==1 then love.graphics.draw (f_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==2 then love.graphics.draw (f_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==3 then love.graphics.draw (f_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==4 then love.graphics.draw (f_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==5 then love.graphics.draw (f_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==6 then love.graphics.draw (f_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==7 then love.graphics.draw (f_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==8 then love.graphics.draw (f_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==9 then love.graphics.draw (f_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==10 then love.graphics.draw (f_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==11 then love.graphics.draw (f_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==12 then love.graphics.draw (f_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==13 then love.graphics.draw (f_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==14 then love.graphics.draw (f_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==15 then love.graphics.draw (f_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==16 then love.graphics.draw (f_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==17 then love.graphics.draw (f_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==18 then love.graphics.draw (f_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==19 then love.graphics.draw (f_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==20 then love.graphics.draw (f_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==21 then love.graphics.draw (f_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==22 then love.graphics.draw (f_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==23 then love.graphics.draw (f_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													end
												
												elseif block.name=="o_object" then
														if nahoreframe==0 then love.graphics.draw (o_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
													elseif nahoreframe==1 then love.graphics.draw (o_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==2 then love.graphics.draw (o_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==3 then love.graphics.draw (o_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==4 then love.graphics.draw (o_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==5 then love.graphics.draw (o_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==6 then love.graphics.draw (o_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==7 then love.graphics.draw (o_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==8 then love.graphics.draw (o_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==9 then love.graphics.draw (o_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==10 then love.graphics.draw (o_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==11 then love.graphics.draw (o_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==12 then love.graphics.draw (o_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==13 then love.graphics.draw (o_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==14 then love.graphics.draw (o_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==15 then love.graphics.draw (o_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==16 then love.graphics.draw (o_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==17 then love.graphics.draw (o_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==18 then love.graphics.draw (o_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==19 then love.graphics.draw (o_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==20 then love.graphics.draw (o_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==21 then love.graphics.draw (o_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==22 then love.graphics.draw (o_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													elseif nahoreframe==23 then love.graphics.draw (o_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
													end
										elseif block.name=="semafor" then		 
										--print(semaforframe) 
													if semaforframe==0  then love.graphics.draw (semafor_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif semaforframe==1  then love.graphics.draw (semafor_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif semaforframe==2  then love.graphics.draw (semafor_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif semaforframe==3  then love.graphics.draw (semafor_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="velryb" then
													if velrybframe>0 then love.graphics.draw (velryb_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif velrybframe>1 then love.graphics.draw (velryb_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>2 then love.graphics.draw (velryb_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>3 then love.graphics.draw (velryb_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>4 then love.graphics.draw (velryb_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>5 then love.graphics.draw (velryb_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>6 then love.graphics.draw (velryb_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>7 then love.graphics.draw (velryb_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>8 then love.graphics.draw (velryb_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>9 then love.graphics.draw (velryb_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>10 then love.graphics.draw (velryb_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif velrybframe>11 then love.graphics.draw (velryb_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								 
								elseif block.name=="vrsek" then
													if nahoreframe==0 then love.graphics.draw (vrsek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif nahoreframe==1 then love.graphics.draw (vrsek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==2 then love.graphics.draw (vrsek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==3 then love.graphics.draw (vrsek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==4 then love.graphics.draw (vrsek_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==5 then love.graphics.draw (vrsek_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==6 then love.graphics.draw (vrsek_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==7 then love.graphics.draw (vrsek_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==8 then love.graphics.draw (vrsek_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==9 then love.graphics.draw (vrsek_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==10 then love.graphics.draw (vrsek_10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==11 then love.graphics.draw (vrsek_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==12 then love.graphics.draw (vrsek_12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==13 then love.graphics.draw (vrsek_13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==14 then love.graphics.draw (vrsek_14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==15 then love.graphics.draw (vrsek_15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==16 then love.graphics.draw (vrsek_16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==17 then love.graphics.draw (vrsek_17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==18 then love.graphics.draw (vrsek_18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==19 then love.graphics.draw (vrsek_19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==20 then love.graphics.draw (vrsek_20, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==21 then love.graphics.draw (vrsek_21, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==22 then love.graphics.draw (vrsek_22, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif nahoreframe==23 then love.graphics.draw (vrsek_23, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								end
						end
						
					--level 73
					elseif nLevel==73 then
						if i==1 then
								if block.name=="dole" then love.graphics.draw (dole, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="dolez" then love.graphics.draw (dolez, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nahore" then love.graphics.draw (nahore, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nahorez" then love.graphics.draw (nahorez, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nejnize" then love.graphics.draw (nejnize, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nejnizez" then love.graphics.draw (nejnizez, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nejvyse" then love.graphics.draw (nejvyse, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="nejvysez" then love.graphics.draw (nejvysez, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ocel" then love.graphics.draw (ocel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="styc" then love.graphics.draw (styc, ((x+dx-1))*tileSize+frgx, (y+dy-2)*tileSize,0,dividx,dividy)
							elseif block.name=="tyc" then love.graphics.draw (tyc, ((x+dx-1))*tileSize+frgx, (y+dy-2)*tileSize,0,dividx,dividy)
							elseif block.name=="uprostred" then love.graphics.draw (uprostred, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="uprostredz" then love.graphics.draw (uprostredz, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="vocel" then love.graphics.draw (vocel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							end
						end
					
					--level 74
					elseif nLevel==74 then
						if i==1 then
								if block.name=="nindpar" then love.graphics.draw (nindpar, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="kreveta" then love.graphics.draw (kreveta, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="kriz" then love.graphics.draw (kriz, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="shrimp" then love.graphics.draw (shrimp_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="zsluch" then love.graphics.draw (zsluch_00, ((x+dx-2))*tileSize, (y+dy-2)*tileSize,0,dividx,dividy)
							elseif block.name=="ocel" then love.graphics.draw (ocel_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="lebka" then love.graphics.draw (lebka_00, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							
							end
						
						end
					--level 75
					elseif nLevel==75 then
						if i==1 then
								if block.name=="3steel" 		then love.graphics.draw (steel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="animal" 		then love.graphics.draw (animal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="barrellay" 		then love.graphics.draw (barrellay, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="barrelstand" 	then love.graphics.draw (barrelstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="broom" 			then love.graphics.draw (broom, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="cactus" 		then love.graphics.draw (cactus, ((x+dx-4))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="canister" 		then love.graphics.draw (canister, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="coral" 			then love.graphics.draw (coral, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="hammer" 		then love.graphics.draw (hammer, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="icicle" 		then love.graphics.draw (icicle, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="key" 			then love.graphics.draw (key, ((x+dx-5))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ladder" 		then love.graphics.draw (ladder, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							--elseif block.name=="leftsteel"		then love.graphics.draw (leftsteel,  ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="locksteel" 		then love.graphics.draw (locksteel,  ((x+dx-9))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							--elseif block.name=="middlesteel" 	then love.graphics.draw (middlesteel,((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							--elseif block.name=="rightsteel" 	then love.graphics.draw (rightsteel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="pearl" 			then love.graphics.draw (pearl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="steelangle" 	then love.graphics.draw (steelangle, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="stone" 			then love.graphics.draw (stone, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="stones" 		then love.graphics.draw (stones, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							
							elseif block.name=="leftsteel"		then love.graphics.draw (leftsteel1,  ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
																	 love.graphics.draw (leftsteel2,  ((x+dx+20))*tileSize+frgx, (y+dy+15)*tileSize+frgy,0,dividx,dividy)
																	 
							elseif block.name=="middlesteel" 	then love.graphics.draw (middlesteel1,  ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
																	 love.graphics.draw (middlesteel2,  ((x+dx+22))*tileSize+frgx, (y+dy+15)*tileSize+frgy,0,dividx,dividy)
							
							elseif block.name=="rightsteel" 	then love.graphics.draw (rightsteel1,((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
																	 love.graphics.draw (rightsteel2,((x+dx+25))*tileSize+frgx, (y+dy+15)*tileSize+frgy,0,dividx,dividy)
							
							end
						end
					
					--level 76
					elseif nLevel==76 then
						if i==1 then
								if block.name=="klic" then love.graphics.draw (klic_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="mvalec" then love.graphics.draw (mvalec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="ocel4" then love.graphics.draw (ocel4, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="szamek" then love.graphics.draw (szamek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="vocel" then love.graphics.draw (vocel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="vvalec" then love.graphics.draw (vvalec, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							elseif block.name=="vzamek" then love.graphics.draw (vzamek, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
							end
						end
					
					--level 77
					elseif nLevel==77 then
						if i==1 then
									if block.name=="bubble1" then love.graphics.draw (bubble1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bubble2" then love.graphics.draw (bubble2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bubble3" then love.graphics.draw (bubble3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								--elseif block.name=="cursor" then love.graphics.draw (cursor, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="linuxak1" and linuser1status=="idle" then
													if linuxakframe>0   and linuxakframe<0.25 then love.graphics.draw (linuxak1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif linuxakframe>0.25 and linuxakframe<0.5 then love.graphics.draw (linuxak1_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.5 and linuxakframe<0.75 then love.graphics.draw (linuxak1_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.75 and linuxakframe<1 then love.graphics.draw (linuxak1_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1 and linuxakframe<1.25 then love.graphics.draw (linuxak1_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1.25 and linuxakframe<1.5 then love.graphics.draw (linuxak1_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="linuxak1" and linuser1status=="talking" then
											
													if linuxakframe>0   and linuxakframe<0.25 then love.graphics.draw (linuxak1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif linuxakframe>0.25 and linuxakframe<0.5 then love.graphics.draw (linuxak1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.5 and linuxakframe<0.75 then love.graphics.draw (linuxak1_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.75 and linuxakframe<1 then love.graphics.draw (linuxak1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1 and linuxakframe<1.25 then love.graphics.draw (linuxak1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1.25 and linuxakframe<1.5 then love.graphics.draw (linuxak1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
															 
								elseif block.name=="linuxak2" and linuser2status=="idle" then
													if linuxakframe>0   and linuxakframe<0.25 then love.graphics.draw (linuxak2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif linuxakframe>0.25 and linuxakframe<0.5 then love.graphics.draw (linuxak2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.5 and linuxakframe<0.75 then love.graphics.draw (linuxak2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.75 and linuxakframe<1 then love.graphics.draw (linuxak2_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1 and linuxakframe<1.25 then love.graphics.draw (linuxak2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1.25 and linuxakframe<1.5 then love.graphics.draw (linuxak2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="linuxak2" and linuser2status=="talking" then
											
													if linuxakframe>0   and linuxakframe<0.25 then love.graphics.draw (linuxak2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif linuxakframe>0.25 and linuxakframe<0.5 then love.graphics.draw (linuxak2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.5 and linuxakframe<0.75 then love.graphics.draw (linuxak2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>0.75 and linuxakframe<1 then love.graphics.draw (linuxak2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1 and linuxakframe<1.25 then love.graphics.draw (linuxak2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif linuxakframe>1.25 and linuxakframe<1.5 then love.graphics.draw (linuxak2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
												
								elseif block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel4" then love.graphics.draw (ocel4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="python" then love.graphics.draw (python, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="lintext" then love.graphics.draw (lintext, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="wilber" then love.graphics.draw (wilber, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
						end
					
						--level 79
						elseif nLevel==79 then
						if i==1 then
									if block.name=="ceauto" then love.graphics.draw (ceauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="cyauto" then love.graphics.draw (cyauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="fiauto" then love.graphics.draw (fiauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="hnauto" then love.graphics.draw (hnauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="maauto" then love.graphics.draw (maauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="moauto" then love.graphics.draw (moauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="orauto" then love.graphics.draw (orauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="podlozka" then love.graphics.draw (podlozka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ssauto" then love.graphics.draw (ssauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="tsauto" then love.graphics.draw (tsauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="zauto" then love.graphics.draw (zeauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="zlauto" then love.graphics.draw (zlauto_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
						end
					
						--level 78
						elseif nLevel==78 then
						if i==1 then
									if block.name=="ocel1" then love.graphics.draw (ocel1, ((x+dx-2))*tileSize, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel2" then love.graphics.draw (ocel2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel3" then love.graphics.draw (ocel3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="svetlo" then love.graphics.draw (svetlo_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="tyc" then love.graphics.draw (tyc_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="valec" then love.graphics.draw (valec_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
						end
					end
end
