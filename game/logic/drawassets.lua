
--[[
The first function drawassetsmap(block,x,y,tileSize) is a function that draws the assets map, which is a grid of tiles that represents the different assets available in the game. It takes four arguments: block which is the block of assets to draw, x and y which are the coordinates of the tile to draw, and tileSize which is the size of each tile in pixels.

The function first sets the color of the tile based on the current palette being used. There are six possible palettes, each with two different colors for the tile. If the current palette is 7, which represents the selection of a tile, the function sets the color to the first color and draws a rectangle with a line. Otherwise, if the current palette is not 1, which represents the background color, the function fills a rectangle with the first color.

The function then sets the color of the tile based on the current palette being used again, this time for the second color. If the current palette is 7, the function sets the color to the first color and draws a rectangle with a line. Otherwise, if the current palette is not 1, the function fills a rectangle with the second color.

Finally, if debugmode is enabled and the current palette is 1, the function prints the coordinates of the tile onto the tile.
--]]
function drawassetsmap(block,x,y,tileSize)	

				-- map tile
				-- beware of -1
					if palette==1 then love.graphics.setColor(cs1tile1) 
				elseif palette==2 then love.graphics.setColor(cs2tile1)
				elseif palette==3 then love.graphics.setColor(cs3tile1)
				elseif palette==4 then love.graphics.setColor(cs4tile1)
				elseif palette==5 then love.graphics.setColor(cs5tile1)
				elseif palette==6 then love.graphics.setColor(cs6tile1)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize)
				else
					--if not(palette==1) and selectedTile==nil or selectedTile==0 then
					if selectedTile==nil or selectedTile==0 then
						--love.graphics.rectangle ('fill', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize)
						--love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize)
					--elseif not(palette==1) and not (selectedTile==nil) and not (selectedTile==0) then
					elseif selectedTile==nil and not (selectedTile==0) then
					--print(selectedTile)
						love.graphics.draw (tileTextures[selectedTile], (x-1)*tileSize+frgx, (y-1)*tileSize+frgy,0,  tileSize/120, tileSize/120)
						
					end
				end
				
				--love.graphics.setColor(0.6, 0.58, 0.2)
					if palette==1 then love.graphics.setColor(cs1tile2) 
				elseif palette==2 then love.graphics.setColor(cs2tile2)
				elseif palette==3 then love.graphics.setColor(cs3tile2)
				elseif palette==4 then love.graphics.setColor(cs4tile2)
				elseif palette==5 then love.graphics.setColor(cs5tile2)
				elseif palette==6 then love.graphics.setColor(cs6tile2)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize-15)
				else
					--if assets==1 or nLevel>1 then
					--if not(palette==1) and selectedTile==nil or selectedTile==0 then
					if selectedTile==nil or selectedTile==0 then
						--love.graphics.rectangle ('fill', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize-15)
						--love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize-15)
					end
				end
				--love.graphics.setColor(0,0,0)
				
					if debugmode=="yes" and palette==1 then 
            love.graphics.setColor(0,0,0)
					--love.graphics.print("#",(x-1)*tileSize+frgx, (y-1)*tileSize) end
					love.graphics.print ((x)..' '..(y), (x-1)*tileSize, (y-1)*tileSize) end
end

--The second function drawexitarea(block,x,y,tileSize) is an empty function that takes the same arguments as drawassetsmap(). It is likely intended to draw an exit area on the game map, but it appears to be unfinished.
function drawexitarea(block,x,y,tileSize)

end

function drawexitmap()
			--if not(palette==1) then 
					love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize-15)
				--end
end

--The third function drawexitmap() is also intended to draw an exit area on the game map, but it only draws a rectangle with a line if the current palette is not 1.
function drawbordermap(block,x,y,tileSize)
	--print(threeD)
	--dream:draw(cube1, 10 , 10, 10,1,1)	-- not working
	-- map tile
						if palette==1 then love.graphics.setColor(cs1border1)
					elseif palette==2 then love.graphics.setColor(cs2border1)
					elseif palette==3 then love.graphics.setColor(cs3border1)
					elseif palette==4 then love.graphics.setColor(cs4border1)
					elseif palette==5 then love.graphics.setColor(cs5border1)
					elseif palette==6 then love.graphics.setColor(cs6border1)
					end
                if palette==7 then love.graphics.setColor(cs6border1)
					love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize)	
                else
					--if not(palette==1) and selectedTile==nil then
					if  selectedTile==nil then
						--love.graphics.rectangle ('fill', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize)
				--elseif not(palette==1) and not (selectedTile==nil) and not (selectedTile==0) then
				elseif selectedTile==nil and not (selectedTile==0) then
						love.graphics.draw (tileTextures[selectedTile], (x-1)*tileSize+frgx, (y-1)*tileSize+frgy,0,  tileSize/120, tileSize/120)
					end
								--dream:draw(cube1,  (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize)
              end
              if threeD==true then
                						--dream:draw(cube1,  1, 1,-60, 10,10,10)
              end
				
					if palette==1 then love.graphics.setColor(cs1border2)
                elseif palette==2 then love.graphics.setColor(cs2border2)
                elseif palette==3 then love.graphics.setColor(cs3border2)
                elseif palette==4 then love.graphics.setColor(cs4border2)
                elseif palette==5 then love.graphics.setColor(cs5border2)
                elseif palette==6 then love.graphics.setColor(cs6border2)
                end
                if palette==7 then love.graphics.setColor(cs6border1)
					love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize-15)
				else
					if not(palette==1) and selectedTile==nil then
						--love.graphics.rectangle ('fill', (x-1)*tileSize+frgx, (y-1)*tileSize+frgy, tileSize, tileSize-15)
						--love.graphics.rectangle ('line', (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize-15)
					--elseif assets==2 and nLevel==1 then
						--love.graphics.draw (level1style1, (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize-15)
					end
				end
end

--The drawgridmap() function is responsible for drawing the grid information on the game screen. It first checks if debugmode is set to "yes" and extradebug is set to true. If so, it sets the font to size 20, and prints the current tileSize and gridWidth and gridHeight of the map. These values are important for debugging and testing the game. If debug mode is not enabled, the function does nothing. The commented out print statements serve the same purpose but print the debug information to the console instead of drawing it on the screen.
function drawgridmap()
				if extradebug==true and debugmode=="yes" then
				love.graphics.setFont(love.graphics.newFont(20))
				love.graphics.setColor(1,1,1)
								
				love.graphics.print("Tilesize :" .. tileSize ,50,100)
				love.graphics.print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight,50,150)
				end
					--[[print("Tilesize :" .. tileSize)
				print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight)
				print("=========================================================")--]]
end

--[[
The function drawobjectsassets() seems to be a function that takes in a set of arguments such as block, x, y, dx, dy, tileSize, i, gridSize, dividx, and dividy and then based on certain conditions, it sets the color of the object's cells to be displayed and draws different types of objects in different colors using the love.graphics.setColor() function. The function contains a series of if-else statements that set the color based on different conditions. It also has conditions that check for the level of the game and the type of the palette being used to set the colors of the objects. The function seems to be part of a larger game or application that involves drawing and displaying objects on a canvas or screen.
--]]
function drawobjectsassets(pb, block,x,y,dx,dy,tileSize,i,gridSize,dividx,dividy)

if objectscells=="yes" then
		-- beware of -1
		
					if palette==1 then
							if nLevel==15 then love.graphics.setColor(beat/40,beat/30,beat/40,1)
						elseif nLevel==20 then love.graphics.setColor(beat/80,beat/20,beat/25,1)
						elseif nLevel==20 then love.graphics.setColor(beat/80,beat/20,beat/25,1)
						elseif nLevel==52 then love.graphics.setColor(beat/40,beat/30+0.2,beat/40,1) 
						else
								if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(cs1block)		-- adjust colors
							else cs1block={adjustr,adjustg,adjustb,1}
							love.graphics.setColor(cs1block)
							end
						end
				elseif palette==2 then love.graphics.setColor(cs2block)
				elseif palette==3 then love.graphics.setColor(cs3block)
				elseif palette==4 then love.graphics.setColor(cs4block)
				elseif palette==2 then love.graphics.setColor(1,1,1)
				elseif palette==3 then love.graphics.setColor(1,1,1)
				elseif palette==4 then love.graphics.setColor(1,1,1)
				elseif palette==5 then love.graphics.setColor(cs5block)
				elseif palette==6 then love.graphics.setColor(cs6block)
				end
				
					if palette==7 and block.heavy 		then love.graphics.setColor(0.2,0.2,1)								-- color the heavy objects with a different color
						love.graphics.rectangle ('line', (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy, tileSize, tileSize)
				elseif palette==7 and not (block.heavy)	then love.graphics.setColor(cs7tile1)							-- color the light objects diferently
						love.graphics.rectangle ('line', (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy, tileSize, tileSize)
				else
					
						if not (palette==1) then 
							if block.heavy then
								love.graphics.setColor(0,1,1)
								love.graphics.rectangle ('line', (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy, tileSize, tileSize)
							else
								love.graphics.setColor(0,1,0)
								love.graphics.rectangle ('line', (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy, tileSize, tileSize)
							end
						--love.graphics.rectangle ('fill', (x+dx-1)*tileSize+frgx+2.5, (y+dy-1)*tileSize+frgy+2.5, tileSize-5, tileSize-5) 
					--elseif not (palette==1) and not (nLevel==60) then love.graphics.rectangle ('fill', (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgx, tileSize, tileSize) 
					
					end
					
--[[
This code is part of a Love2D program that renders a game world in a 2D tile-based style. The code is within an if statement that checks whether the current level is 1 and whether the current palette is 1. It then proceeds to draw various images using the Love2D graphics library depending on the name of the block.

If the block is a chair1, it draws an image of the chair using the appropriate palette. The same logic applies to chair2, cushion1, table1, and steel-pipe-1x81.

For each image, the draw function is called with several arguments that determine the position, rotation, and scaling of the image. These arguments are computed based on the position of the block in the game world, the current palette, and other factors.

There are also some commented-out code blocks that are used for debugging purposes. They draw rectangles around specific blocks to help with positioning and debugging.

 Finally, there is some commented-out code that is used for drawing 3D models of certain blocks, but this code is not currently being used.
--]]
					
						if nLevel==1 and palette==1 then
							
							-- exception created to prevent game from crashing. It initialize variables and start parameters to load the needed assets
							if frgx==nil then 
								timer2=0
								timer3=0
								isloading=true
								loadingprocess=true
								loadbuttonpressed=false
								savebuttonpressed=false
								levelcompletedsentencesaid=false
								bfocus=nil
								--This line sets the value of the nLevel variable to be equal to the current level.
									nLevel=level
									loadcurrenttrackname()	-- Load the current music track name (function on loading.lua)
							end
							
							if i==1 then
								if block.name=="chair1" then
									causticsAndNormals:send("u_normals", chair1Normal)
									causticsAndNormals:send("u_heightmap", chair1Height)
										--if palette==1 then 	love.graphics.draw (chair1, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgx,0, dividx,dividy)
										if palette==1 then 	love.graphics.draw (chair1, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx,dividy)
															--love.graphics.rectangle ('fill', (x-1)*tileSize+frgx, (y-1)*tileSize+frgx, tileSize, tileSize)
									elseif palette==2 then love.graphics.draw (c64chair1, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
									elseif palette==3 then love.graphics.draw (neschair1, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
									elseif palette==4 then love.graphics.draw (z80chair1, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
										--if threeD==true then dream:draw(woodenchair3d, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5) end
									end
								elseif block.name=="chair2" then
									causticsAndNormals:send("u_normals", chair2Normal)
									causticsAndNormals:send("u_heightmap", chair2Height)
										if palette==1 then love.graphics.draw (chair2, ((x+dx-1)-3)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx,dividy)
									elseif palette==2 then love.graphics.draw (c64chair2, ((x+dx-1)-3)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
									elseif palette==3 then love.graphics.draw (neschair2, ((x+dx-1)-3)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
									elseif palette==4 then love.graphics.draw (z80chair2, ((x+dx-1)-3)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,5.4)
									end
								elseif block.name=="steel-pipe-1x81" then
									--causticsAndNormals:send("u_normals", pipe1Normal)
									--causticsAndNormals:send("u_heightmap", pipe1Height)
									
									local lightPos1 = {0.1, 0.1, 0.075}
	local lightColor = {red, green, blue, 10}

		-- Secondary light
				-- Light position 2
				local lightPos2 = { 0.5, 0.5, 0.075}

				local lightColor2 = {red, green, blue, 10}
					
		 vectorNormalsNoiseShadowEnvmapShader:send("LightPos1", lightPos1)
		 vectorNormalsNoiseShadowEnvmapShader:send("LightColor1", lightColor)
		
		vectorNormalsNoiseShadowEnvmapShader:send("LightPos2", lightPos2)
		vectorNormalsNoiseShadowEnvmapShader:send("LightColor2", lightColor2)
		
        
        -- Set other shader parameters for normal mapping
        vectorNormalsNoiseShadowEnvmapShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        vectorNormalsNoiseShadowEnvmapShader:send("Falloff", {0.2, 1, 1.5})
        -- Ambient color
		vectorNormalsNoiseShadowEnvmapShader:send("AmbientColor", {red * 0.2, green * 0.2, blue * 0.3, 1.0})

		-- Reflection intensity
		vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", 1.8)

		vectorNormalsNoiseShadowEnvmapShader:send("u_envMap", envMap)
											vectorNormalsNoiseShadowEnvmapShader:send("u_normals", pipe1Normal)
									vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", pipe1Height)
		love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
							love.graphics.setShader(causticsAndNormals)
										if palette==1 then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx,dividy)
									elseif palette==2 then love.graphics.draw (c64pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,6)
									elseif palette==3 then love.graphics.draw (nespipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,6)
									elseif palette==4 then love.graphics.draw (z80pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, 6,6)
									end
									
									love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
								elseif block.name=="cushion1" then
									causticsAndNormals:send("u_normals", cushion1Normal)
									causticsAndNormals:send("u_heightmap", cushion1Height)
											if palette==1 then love.graphics.draw (cushion1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
										elseif palette==2 then love.graphics.draw (c64cushion1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
										elseif palette==3 then love.graphics.draw (nescushion1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
										elseif palette==4 then love.graphics.draw (z80cushion1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
										end
								elseif block.name=="table1" then
									causticsAndNormals:send("u_normals", table1Normal)
									causticsAndNormals:send("u_heightmap", table1Height)
										if palette==1 then love.graphics.draw (table1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif palette==2 then love.graphics.draw (c64table1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
									elseif palette==3 then love.graphics.draw (nestable1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
									elseif palette==4 then love.graphics.draw (z80table1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,6,5.4)
									end
								end
								
								
							end
						--level 2
						elseif nLevel==2 then
							if i==1 then
							if block.name=="trunk" and palette==1 then   
									if briefcaseclosed==true and block.name=="trunk" then
										causticsAndNormals:send("u_normals", briefcaseNormal)
										causticsAndNormals:send("u_heightmap", briefcaseHeight)
										love.graphics.draw (briefcase, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
								elseif briefcaseclosed==false and block.name=="trunk" then
										love.graphics.draw (briefcaseopen, ((x+dx-1))*tileSize+frgx, (y-4.2+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
							
									if block.name=="warning" then if palette==1 then
										causticsAndNormals:send("u_normals", warningNormal)
										causticsAndNormals:send("u_heightmap", warningHeight)
									 love.graphics.draw (warning, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) end
								elseif block.name=="bottle" then  	  if palette==1 then
										causticsAndNormals:send("u_normals", bottleNormal)
										causticsAndNormals:send("u_heightmap", bottleHeight)
								 love.graphics.draw (bottle, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="L" then 	  if palette==1 then
										causticsAndNormals:send("u_normals", LNormal)
										causticsAndNormals:send("u_heightmap", LHeight)
								 love.graphics.draw (L, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="nut" then	  if palette==1 then
								
										causticsAndNormals:send("u_normals", hexagonalnutNormal)
										causticsAndNormals:send("u_heightmap", hexagonalnutHeight)
								 love.graphics.draw (hexagonalnut, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="hammer" then  if palette==1 then
										
										causticsAndNormals:send("u_normals", hammerNormal)
										causticsAndNormals:send("u_heightmap", hammerHeight)
								 love.graphics.draw (hammer, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="pliers" then  if palette==1 then
								
										causticsAndNormals:send("u_normals", pliersNormal)
										causticsAndNormals:send("u_heightmap", pliersHeight)
								 love.graphics.draw (pliers, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="pipehorizontal" then if palette==1 then
								
										causticsAndNormals:send("u_normals", pipehorizontalNormal)
										causticsAndNormals:send("u_heightmap", pipehorizontalHeight)
								 love.graphics.draw (pipehorizontal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								end
							end
							--level 3
						elseif nLevel==3 then
							if i==1 then
									if block.name=="shortpipe1" then if palette==1 then love.graphics.draw (shortpipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="shortpipe2" then if palette==1 then love.graphics.draw (shortpipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="minipipe" then if palette==1 then love.graphics.draw (minipipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="pipelonghorizontal" then if palette==1 then love.graphics.draw (pipelonghorizontal, ((x-3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="booklvl3" then   if palette==1 then love.graphics.draw (booklv3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="longpipehorizontal" then  	  if palette==1 then love.graphics.draw (longpipehorizontal, ((x+dx-4))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="apricosemarmelade" then 	  if palette==1 then love.graphics.draw (apricosemarmelade, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="honey" then	  if palette==1 then love.graphics.draw (honey, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="strawberrymarmelade" then  if palette==1 then love.graphics.draw (strawberrymarmelade, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="oillamp" then  if palette==1 then love.graphics.draw (oillamp, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="snail" then  if palette==1 then love.graphics.draw (snail, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="shell" then  if palette==1 then love.graphics.draw (shell, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="coffee" then  if palette==1 then love.graphics.draw (coffee, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="plate" then  if palette==1 then love.graphics.draw (plate, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								elseif block.name=="axe" then  if palette==1 then love.graphics.draw (axe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								--elseif block.name=="" then  if palette==1 then love.graphics.draw (, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								--elseif block.name=="" then if palette==1 then love.graphics.draw (, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) end
								end
							end
							-- level 4
						elseif nLevel==4 then
							if i==1 then
									if block.name=="strangecreature" then
													if strangecreatureframe>0   and strangecreatureframe<1 then love.graphics.draw (ostnatec_00, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif strangecreatureframe>1 and strangecreatureframe<2 then love.graphics.draw (ostnatec_01, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif strangecreatureframe>2 and strangecreatureframe<3 then love.graphics.draw (ostnatec_02, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="cpipe" then love.graphics.draw (cpipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book1" then love.graphics.draw (book1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book2" then love.graphics.draw (book2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book3" then love.graphics.draw (book3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book4" then love.graphics.draw (book4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book5" then	love.graphics.draw (book5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book6" then	love.graphics.draw (book6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book7" then	love.graphics.draw (book7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book8" then	love.graphics.draw (book8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="book9" then	love.graphics.draw (book9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="map1" then	love.graphics.draw (book9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bookbig1" then love.graphics.draw (bookbig1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="booksmall1" then love.graphics.draw (booksmall1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="booksmall2" then love.graphics.draw (booksmall2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="booksmall3" then love.graphics.draw (booksmall3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bookhoriz1" then love.graphics.draw (bookhoriz1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bookhoriz2" then love.graphics.draw (bookhoriz2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
							end
							--level 5
						elseif nLevel==5 then
							if i==1 then
									if block.name=="chair1upside" then love.graphics.draw (chair1upside, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="chair2upside" then love.graphics.draw (chair2upside, ((x+dx-1)-3)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="table1" then love.graphics.draw (table1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="plant" and skin=="classic" then love.graphics.draw (plant, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="plant" and skin=="remake" then love.graphics.draw (plant, ((x+dx-2.3))*tileSize+frgx, (y+dy-1.7)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="obj1" then
													if crabframe>0   and crabframe<0.25 then love.graphics.draw (plz00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.25 and crabframe<0.5 then love.graphics.draw (plz01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.5 and crabframe<0.75 then love.graphics.draw (plz02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.75 and crabframe<1 then love.graphics.draw (plz03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1 and crabframe<1.25 then love.graphics.draw (plz04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1.25 and crabframe<1.5 then love.graphics.draw (plz05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (plz06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								
								elseif block.name=="snak" then
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								end
							end
							
						--level 6
						elseif nLevel==6 then
							if i==1 then
										--if block.name=="broom" then love.graphics.draw (koste_00, (x-1+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									
										if block.name=="broom" and broommoving==false then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="broom" and broommoving==true then													
											
													if broomframe>0 and broomframe<0.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>0.25 and broomframe<0.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>0.5 and broomframe<0.75 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>0.75 and broomframe<1 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>1 and broomframe<1.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>1.25 and broomframe<1.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>1.5 and broomframe<1.75 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>1.75 and broomframe<2 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>2 and broomframe<2.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>2.25 and broomframe<2.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>2.5 and broomframe<2.75 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>2.75 and broomframe<3 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>3 and broomframe<3.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>3.25 and broomframe<3.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>3.5 and broomframe<3.75 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>3.75 and broomframe<4 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>4 and broomframe<4.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>4.25 and broomframe<4.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>4.5 and broomframe<4.75 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>4.75 and broomframe<5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>5 and broomframe<5.25 then love.graphics.draw (koste_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>5.25 and broomframe<5.5 then love.graphics.draw (koste_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif broomframe>5.5 and broomframe<6 then love.graphics.draw (koste_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									elseif block.name=="rock" then love.graphics.draw (rock, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="rock2" then love.graphics.draw (rock2, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="rock3" then love.graphics.draw (rock3, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="rock4" then love.graphics.draw (rock4, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="coal" then love.graphics.draw (coal, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="longpipe" then love.graphics.draw (longpipe, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="wood" then love.graphics.draw (wood, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
									elseif block.name=="wood2" then love.graphics.draw (wood2, (x+dx-1)*tileSize+frgx, (y+dy-1)*tileSize+frgy,0, dividx/2,dividy/2)
								end
							end
							
							--level 7
						elseif nLevel==7 then
							if i==1 then
									if block.name=="snak" then
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="matrace" then love.graphics.draw (matrace, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="musla" then love.graphics.draw (musla, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="ocel" then love.graphics.draw (ocel, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="plz_01" then 
													if crabframe>0   and crabframe<0.25 then love.graphics.draw (plz00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.25 and crabframe<0.5 then love.graphics.draw (plz01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.5 and crabframe<0.75 then love.graphics.draw (plz02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>0.75 and crabframe<1 then love.graphics.draw (plz03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1 and crabframe<1.25 then love.graphics.draw (plz04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1.25 and crabframe<1.5 then love.graphics.draw (plz05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (plz06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								end
							end
							
							--level 8
						elseif nLevel==8 then
							if i==1 then
									if block.name=="paper" then love.graphics.draw (paper, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="wc" then love.graphics.draw (wc, ((x-5+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
							--level 9
						elseif nLevel==9 then
							if i==1 then
									if block.name=="mirror" then love.graphics.draw (mirror, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kriz" then love.graphics.draw (kriz, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="lahev" then love.graphics.draw (lahev, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="matka_a" then love.graphics.draw (matka_a, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="mirror" then love.graphics.draw (mirror, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="naboj" then love.graphics.draw (naboj, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="peri" then 
													if periframe>0   and periframe<0.25 then love.graphics.draw (peri00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>0.25 and periframe<0.5 then love.graphics.draw (peri01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>0.5 and periframe<0.75 then love.graphics.draw (peri02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>0.75 and periframe<1 then love.graphics.draw (peri03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>1 and periframe<1.25 then love.graphics.draw (peri04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>1.25 and periframe<1.5 then love.graphics.draw (peri05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>1.5 and periframe<1.75 then love.graphics.draw (peri06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>1.75 and periframe<3 then love.graphics.draw (peri07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>3 and periframe<3.25 then love.graphics.draw (peri06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>3.25 and periframe<3.5 then love.graphics.draw (peri05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>3.5 and periframe<3.75 then love.graphics.draw (peri04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>3.75 and periframe<4 then love.graphics.draw (peri03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>4 and periframe<4.25 then love.graphics.draw (peri02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>4.25 and periframe<4.5 then love.graphics.draw (peri01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif periframe>4.5 and periframe<6 then love.graphics.draw (peri00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										mirrorpositionx=x+dx-1
									end
							end
								--level 10
						elseif nLevel==10 or nLevel==18 then
							if i==1 then 
							
										if block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="cruiseship" then 
												--left window
													if boatwindowframe>0   and boatwindowframe<0.25 then love.graphics.draw (dama00, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.25 and boatwindowframe<0.5 then love.graphics.draw (dama01, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.5 and boatwindowframe<0.75 then love.graphics.draw (dama02, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.75 and boatwindowframe<1 then love.graphics.draw (dama03, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1 and boatwindowframe<1.25 then love.graphics.draw (dama04, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.25 and boatwindowframe<1.5 then love.graphics.draw (dama05, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.5 and boatwindowframe<1.75 then love.graphics.draw (dama06, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.75 and boatwindowframe<3 then love.graphics.draw (dama07, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3 and boatwindowframe<3.25 then love.graphics.draw (dama08, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.25 and boatwindowframe<3.5 then love.graphics.draw (dama09, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.5 and boatwindowframe<3.75 then love.graphics.draw (dama10, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.75 and boatwindowframe<4 then love.graphics.draw (dama11, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4 and boatwindowframe<4.25 then love.graphics.draw (dama12, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4.25 and boatwindowframe<4.5 then love.graphics.draw (dama13, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4.5 and boatwindowframe<5 then love.graphics.draw (dama14, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>5 and boatwindowframe<5.25 then love.graphics.draw (frkavec00, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5.25 and boatwindowframe<5.5 then love.graphics.draw (frkavec01, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5.5 and boatwindowframe<5.75 then love.graphics.draw (frkavec02, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5.75 and boatwindowframe<6 then love.graphics.draw (frkavec03, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6 and boatwindowframe<6.25 then love.graphics.draw (frkavec04, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6.25 and boatwindowframe<6.5 then love.graphics.draw (frkavec05, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6.5 and boatwindowframe<7 then love.graphics.draw (frkavec06, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>7 and boatwindowframe<7.25 then love.graphics.draw (kap00, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.25 and boatwindowframe<7.5 then love.graphics.draw (kap01, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.5 and boatwindowframe<7.75 then love.graphics.draw (kap02, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.75 and boatwindowframe<8 then love.graphics.draw (kap03, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8 and boatwindowframe<8.25 then love.graphics.draw (kap04, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.25 and boatwindowframe<8.5 then love.graphics.draw (kap05, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.5 and boatwindowframe<8.75 then love.graphics.draw (kap06, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.75 and boatwindowframe<9 then love.graphics.draw (kap07, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9 and boatwindowframe<9.25 then love.graphics.draw (kap08, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9.25 and boatwindowframe<9.5 then love.graphics.draw (kap09, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9.5 and boatwindowframe<9.75 then love.graphics.draw (kap10, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9.75 and boatwindowframe<10 then love.graphics.draw (kap11, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10 and boatwindowframe<10.25 then love.graphics.draw (kap12, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.25 and boatwindowframe<10.5 then love.graphics.draw (kap13, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.5 and boatwindowframe<10.75 then love.graphics.draw (kap14, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.75 and boatwindowframe<11 then love.graphics.draw (kap15, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11 and boatwindowframe<11.25 then love.graphics.draw (kap16, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11.25 and boatwindowframe<11.5 then love.graphics.draw (kap17, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11.5 and boatwindowframe<12 then love.graphics.draw (kap18, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>12 and boatwindowframe<12.25 then love.graphics.draw (lodnik00, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.25 and boatwindowframe<12.5 then love.graphics.draw (lodnik01, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.5 and boatwindowframe<12.75 then love.graphics.draw (lodnik02, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.75 and boatwindowframe<13 then love.graphics.draw (lodnik03, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13 and boatwindowframe<13.25 then love.graphics.draw (lodnik04, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.25 and boatwindowframe<13.5 then love.graphics.draw (lodnik05, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.5 and boatwindowframe<13.75 then love.graphics.draw (lodnik06, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.75 and boatwindowframe<14 then love.graphics.draw (lodnik07, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14 and boatwindowframe<14.25 then love.graphics.draw (lodnik08, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14.25 and boatwindowframe<14.5 then love.graphics.draw (lodnik09, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14.5 and boatwindowframe<14.75 then love.graphics.draw (lodnik10, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14.75 and boatwindowframe<15 then love.graphics.draw (lodnik11, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15 and boatwindowframe<15.25 then love.graphics.draw (lodnik12, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.25 and boatwindowframe<15.5 then love.graphics.draw (lodnik13, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.5 and boatwindowframe<15.75 then love.graphics.draw (lodnik14, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.75 and boatwindowframe<16 then love.graphics.draw (lodnik15, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16 and boatwindowframe<16.25 then love.graphics.draw (lodnik16, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16.25 and boatwindowframe<16.5 then love.graphics.draw (lodnik17, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16.5 and boatwindowframe<16.75 then love.graphics.draw (lodnik18, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16.75 and boatwindowframe<17 then love.graphics.draw (lodnik19, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>17 and boatwindowframe<17.25 then love.graphics.draw (lodnik20, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>17.25 and boatwindowframe<17.5 then love.graphics.draw (lodnik21, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>17.5 and boatwindowframe<18 then love.graphics.draw (lodnik22, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
												
												--right window
													if boatwindowframe>0 and boatwindowframe<0.25 then love.graphics.draw (lodnik00, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.25 and boatwindowframe<0.5 then love.graphics.draw (lodnik01, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.5 and boatwindowframe<0.75 then love.graphics.draw (lodnik02, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>0.75 and boatwindowframe<1 then love.graphics.draw (lodnik03, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1 and boatwindowframe<1.25 then love.graphics.draw (lodnik04, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.25 and boatwindowframe<1.5 then love.graphics.draw (lodnik05, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.5 and boatwindowframe<1.75 then love.graphics.draw (lodnik06, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>1.75 and boatwindowframe<2 then love.graphics.draw (lodnik07, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>2 and boatwindowframe<2.25 then love.graphics.draw (lodnik08, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>2.25 and boatwindowframe<2.5 then love.graphics.draw (lodnik09, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>2.5 and boatwindowframe<2.75 then love.graphics.draw (lodnik10, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>2.75 and boatwindowframe<3 then love.graphics.draw (lodnik11, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3 and boatwindowframe<3.25 then love.graphics.draw (lodnik12, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.25 and boatwindowframe<3.5 then love.graphics.draw (lodnik13, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.5 and boatwindowframe<3.75 then love.graphics.draw (lodnik14, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>3.75 and boatwindowframe<4 then love.graphics.draw (lodnik15, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4 and boatwindowframe<4.25 then love.graphics.draw (lodnik16, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4.25 and boatwindowframe<4.5 then love.graphics.draw (lodnik17, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4.5 and boatwindowframe<4.75 then love.graphics.draw (lodnik18, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>4.75 and boatwindowframe<5 then love.graphics.draw (lodnik19, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5 and boatwindowframe<5.25 then love.graphics.draw (lodnik20, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5.25 and boatwindowframe<5.5 then love.graphics.draw (lodnik21, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>5.5 and boatwindowframe<6 then love.graphics.draw (lodnik22, ((x+0.8+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>6   and boatwindowframe<6.25 then love.graphics.draw (dama00, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6.25 and boatwindowframe<6.5 then love.graphics.draw (dama01, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6.5 and boatwindowframe<6.75 then love.graphics.draw (dama02, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>6.75 and boatwindowframe<7 then love.graphics.draw (dama03, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7 and boatwindowframe<7.25 then love.graphics.draw (dama04, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.25 and boatwindowframe<7.5 then love.graphics.draw (dama05, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.5 and boatwindowframe<7.75 then love.graphics.draw (dama06, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>7.75 and boatwindowframe<8 then love.graphics.draw (dama07, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8 and boatwindowframe<8.25 then love.graphics.draw (dama08, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.25 and boatwindowframe<8.5 then love.graphics.draw (dama09, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.5 and boatwindowframe<8.75 then love.graphics.draw (dama10, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8.75 and boatwindowframe<9 then love.graphics.draw (dama11, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>8 and boatwindowframe<9.25 then love.graphics.draw (dama12, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9.25 and boatwindowframe<9.5 then love.graphics.draw (dama13, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>9.5 and boatwindowframe<10 then love.graphics.draw (dama14, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>10 and boatwindowframe<10.25 then love.graphics.draw (kap00, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.25 and boatwindowframe<10.5 then love.graphics.draw (kap01, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.5 and boatwindowframe<10.75 then love.graphics.draw (kap02, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>10.75 and boatwindowframe<11 then love.graphics.draw (kap03, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11 and boatwindowframe<11.25 then love.graphics.draw (kap04, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11.25 and boatwindowframe<11.5 then love.graphics.draw (kap05, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11.5 and boatwindowframe<11.75 then love.graphics.draw (kap06, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>11.75 and boatwindowframe<12 then love.graphics.draw (kap07, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12 and boatwindowframe<12.25 then love.graphics.draw (kap08, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.25 and boatwindowframe<12.5 then love.graphics.draw (kap09, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.5 and boatwindowframe<12.75 then love.graphics.draw (kap10, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>12.75 and boatwindowframe<13 then love.graphics.draw (kap11, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13 and boatwindowframe<13.25 then love.graphics.draw (kap12, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.25 and boatwindowframe<13.5 then love.graphics.draw (kap13, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.5 and boatwindowframe<13.75 then love.graphics.draw (kap14, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>13.75 and boatwindowframe<14 then love.graphics.draw (kap15, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14 and boatwindowframe<14.25 then love.graphics.draw (kap16, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14.25 and boatwindowframe<14.5 then love.graphics.draw (kap17, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>14.5 and boatwindowframe<15 then love.graphics.draw (kap18, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												elseif boatwindowframe>15 and boatwindowframe<15.25 then love.graphics.draw (frkavec00, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.25 and boatwindowframe<15.5 then love.graphics.draw (frkavec01, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.5 and boatwindowframe<15.75 then love.graphics.draw (frkavec02, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>15.75 and boatwindowframe<16 then love.graphics.draw (frkavec03, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16 and boatwindowframe<16.25 then love.graphics.draw (frkavec04, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16.25 and boatwindowframe<16.5 then love.graphics.draw (frkavec05, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatwindowframe>16.5 and boatwindowframe<18 then love.graphics.draw (frkavec06, ((x+7+dx-1))*tileSize+frgx, (y+0.8+dy-1)*tileSize+frgy,0,dividx,dividy)
												
												
												
											
												end
												
											love.graphics.draw (cruiseship, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
											
									elseif block.name=="glass" or block.name=="glass2" or block.name=="glass3" or block.name=="glass4" or block.name=="glass5" or block.name=="glass6" or block.name=="glass7" or block.name=="glass8"   and glassmoving==false then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="glass" or block.name=="glass2" or block.name=="glass3" or block.name=="glass4" or block.name=="glass5" or block.name=="glass6" or block.name=="glass7" or block.name=="glass8" and glassmoving==true then													
											
													if boatglassframe>0 and boatglassframe<0.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.25 and boatglassframe<0.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.5 and boatglassframe<0.75 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.75 and boatglassframe<1 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1 and boatglassframe<1.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.25 and boatglassframe<1.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.5 and boatglassframe<1.75 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.75 and boatglassframe<2 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2 and boatglassframe<2.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.25 and boatglassframe<2.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.5 and boatglassframe<2.75 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.75 and boatglassframe<3 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3 and boatglassframe<3.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.25 and boatglassframe<3.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.5 and boatglassframe<3.75 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.75 and boatglassframe<4 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4 and boatglassframe<4.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.25 and boatglassframe<4.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.5 and boatglassframe<4.75 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.75 and boatglassframe<5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5 and boatglassframe<5.25 then love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5.25 and boatglassframe<5.5 then love.graphics.draw (glass2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5.5 and boatglassframe<6 then love.graphics.draw (glass3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
											
									
									elseif block.name=="glassstand" and glassstandmoving==false then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
									elseif block.name=="glassstand" and glassstandmoving==true then
									
													if boatglassframe>0 and boatglassframe<0.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.25 and boatglassframe<0.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.5 and boatglassframe<0.75 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>0.75 and boatglassframe<1 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1 and boatglassframe<1.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.25 and boatglassframe<1.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.5 and boatglassframe<1.75 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>1.75 and boatglassframe<2 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2 and boatglassframe<2.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.25 and boatglassframe<2.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.5 and boatglassframe<2.75 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>2.75 and boatglassframe<3 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3 and boatglassframe<3.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.25 and boatglassframe<3.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.5 and boatglassframe<3.75 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>3.75 and boatglassframe<4 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4 and boatglassframe<4.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.25 and boatglassframe<4.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.5 and boatglassframe<4.75 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>4.75 and boatglassframe<5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5 and boatglassframe<5.25 then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5.25 and boatglassframe<5.5 then love.graphics.draw (glassstand2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif boatglassframe>5.5 and boatglassframe<6 then love.graphics.draw (glassstand3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
									
													
									
									end
									
							end
							--level 11
						elseif nLevel==11 then
							if i==1 then
									if block.name=="pipe11" then love.graphics.draw (pipe11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="pipe4_11" then love.graphics.draw (pipe4_11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="snail11" then
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (maly_snek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (maly_snek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (maly_snek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (maly_snek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="bullet11" then love.graphics.draw (bullet11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="bullet112" then love.graphics.draw (bullet11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="elk" then
													if elkframe>0   and elkframe<0.5 then love.graphics.draw (los_00, ((x+-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif elkframe>0.5 and elkframe<1 then love.graphics.draw (los_01, ((x+-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif elkframe>1 and elkframe<1.5 then love.graphics.draw (los_02, ((x+-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif elkframe>1.5 and elkframe<2 then love.graphics.draw (los_03, ((x+-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="parrot" then
													if deadparrotframe>0   and deadparrotframe<1 then love.graphics.draw (papoucha_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif deadparrotframe>1 and deadparrotframe<2 then love.graphics.draw (papoucha_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								end
							end
							
							elseif nLevel==12 then
							if i==1 then
									if block.name=="cepicka" then love.graphics.draw (cepicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="rybicka" then
													if rybickaframe>0   and rybickaframe<0.5 then love.graphics.draw (rybicka_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif rybickaframe>0.5 and rybickaframe<1 then love.graphics.draw (rybicka_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif rybickaframe>1 and rybickaframe<1.5 then love.graphics.draw (rybicka_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif rybickaframe>1.5 and rybickaframe<2 then love.graphics.draw (rybicka_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="hat" then love.graphics.draw (hat, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="medusa" then
													if medusaframe>0   and medusaframe<0.5 then love.graphics.draw (medusa_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy) 
												elseif medusaframe>0.5 and medusaframe<1 then love.graphics.draw (medusa_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												elseif medusaframe>1 and medusaframe<1.5 then love.graphics.draw (medusa_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
												end
								elseif block.name=="muslicka" then love.graphics.draw (muslicka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx,dividy)
								end
							
							end
							
							--level 13
								elseif nLevel==13 then
							if i==1 then
										if block.name=="vik1" then 
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik1_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik1_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik1_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="vik2" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik2_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="vik3" then 
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik3_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik3_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik3_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="vik4" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik4_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik4_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik4_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik4_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik4_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="vik5" then 
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik5_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik5_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik5_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik5_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik5_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="vik6" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik6_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik6_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik6_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik6_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik6_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="axe1" then love.graphics.draw (axe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="axe2" then love.graphics.draw (axe2, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="shield1" then love.graphics.draw (shield1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="shield2" then love.graphics.draw (shield2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="snail" then 
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
							--level 14
								elseif nLevel==14 then
							if i==1 then
									if block.name=="eye" then 
													if eyeframe>0   and eyeframe<0.5 then love.graphics.draw (oko_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif eyeframe>0.5 and eyeframe<1 then love.graphics.draw (oko_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif eyeframe>1 and eyeframe<1.5 then love.graphics.draw (oko_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif eyeframe>1.5 and eyeframe<2 then love.graphics.draw (oko_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif eyeframe>2 and eyeframe<2.5 then love.graphics.draw (oko_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="chairleft" then love.graphics.draw (chairleft, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="chairright" then love.graphics.draw (chairright, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="chairup" then love.graphics.draw (chairup, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="chairdown" then love.graphics.draw (chairdown, ((x-3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
						--level 15
						elseif nLevel==15 then
							if i==1 then
										if block.name=="pipe" then  love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mikroskop" then
													if mikroskopframe>0   and mikroskopframe<0.5 then love.graphics.draw (mikroskop_00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif mikroskopframe>0.5 and mikroskopframe<1 then love.graphics.draw (mikroskop_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif mikroskopframe>1 and mikroskopframe<1.5 then love.graphics.draw (mikroskop_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)	
												end
									
									--phone not ringing
									elseif block.name=="phone" and phoneringing==false then love.graphics.draw (phone, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="phone2" and phoneringing==false then love.graphics.draw (phone2, ((x+dx-1))*tileSize+frgx, (y-1+dy)*tileSize+frgy,0,dividx/2,dividy/2)
									-- phone ringing
									elseif block.name=="phone" and phoneringing==true  then love.graphics.draw (phone,  math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+((y+dy-1))*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="phone2" and phoneringing==true then love.graphics.draw (phone2, math.random(0,2)+((x+dx-1))*tileSize+frgx, math.random(0,2)+((y+dy-1))*tileSize+frgy,0,dividx/2,dividy/2)
									
									
									elseif block.name=="basephone" then love.graphics.draw (basephone, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="basephone2" then love.graphics.draw (basephone2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="snail" then 
													if snailframe>0   and snailframe<1 then love.graphics.draw (snek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snailframe>1 and snailframe<2 then love.graphics.draw (snek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="alarmclock" then
													--if budikframe>0   and budikframe<1 then love.graphics.draw (budik_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												--elseif budikframe>1 and budikframe<2 then love.graphics.draw (budik_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												--end
												love.graphics.draw (alarmbig, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,(dividx/2)/16.84,(dividy/2)/16.84)
									
									  -- Draw hour numbers
									for hour = 1, 12 do
										angle = math.rad((hour - 3) * 30)
									end
									
												local radianAngle = math.rad(angle - 90)
												local x2 = x + 100 * math.cos(radianAngle)
												local y2 = y - 10 * math.sin(radianAngle)
												--love.graphics.setLineWidth(0.1)
												
												love.graphics.setColor(0,0,0,1)
												
													--width=0.5
												    -- Hands lengths
													hourHandLength = clockRadius * 0.12 * dividx
													minuteHandLength = clockRadius * 0.14 * dividx
													secondHandLength = clockRadius * 0.14 * dividx
												
												 -- Draw clock hands
											    drawHand(((x+dx-1))*tileSize*1.035+frgx, (y+dy-1)*tileSize*1.035+frgy, time.hour % 12 / 12 * 360, hourHandLength, 10)
												drawHand(((x+dx-1))*tileSize*1.035+frgx, (y+dy-1)*tileSize*1.035+frgy, time.min / 60 * 360, minuteHandLength, 5)
												drawHand(((x+dx-1))*tileSize*1.035+frgx, (y+dy-1)*tileSize*1.035+frgy, time.sec / 60 * 360, secondHandLength, 2)
												love.graphics.setColor(1,1,1,1)
									elseif block.name=="binoculars" then 	
													if dalekohledframe>0   and dalekohledframe<1 then love.graphics.draw (dalekohled_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif dalekohledframe>1 and dalekohledframe<2 then love.graphics.draw (dalekohled_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									--elseif block.name=="" then love.graphics.draw (, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
									if timer2>6 then timer2=0 end		-- restart counter for the ringing phone animation
							end
							
							--level 16
								elseif nLevel==16 then
							if i==1 then
										if block.name=="bullet" then love.graphics.draw (bullet, ((x-1+dx))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="bulletleft" then love.graphics.draw (bulletleft, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="bulletright" then love.graphics.draw (bulletright, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="robothand" then love.graphics.draw (robothand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="snail" then 
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="stairs" then love.graphics.draw (stairs, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end
							
							--level 17
								elseif nLevel==17 then
							if i==1 then
										if block.name=="viking1" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik1_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik1_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik1_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik1_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik1_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="viking2" then 
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik2_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik2_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik2_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik2_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									elseif block.name=="viking3" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik3_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik3_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik3_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik3_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="viking4" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik4_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik4_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik4_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik4_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik4_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="viking5" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik5_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik5_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik5_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik5_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik5_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="viking6" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik6_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik6_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik6_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik6_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik6_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="viking7" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (vik7_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (vik7_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<1.5 then love.graphics.draw (vik7_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1.5 and vikingframe<2 then love.graphics.draw (vik7_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>2 and vikingframe<2.5 then love.graphics.draw (vik7_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="dog" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (pesos_00, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (pesos_01, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<2.5 then love.graphics.draw (pesos_02, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									elseif block.name=="drakar" then
													if vikingframe>0   and vikingframe<0.5 then love.graphics.draw (drakar00, ((x-2+dx-1))*tileSize+frgx, (y-1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>0.5 and vikingframe<1 then love.graphics.draw (drakar01, ((x-2+dx-1))*tileSize+frgx, (y-1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif vikingframe>1 and vikingframe<2.5 then love.graphics.draw (drakar02, ((x-2+dx-1))*tileSize+frgx, (y-1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									
									 
									elseif block.name=="shield" then love.graphics.draw (shield, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="korunka" then love.graphics.draw (korunka, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x-3+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end
							--[[--level 18
						elseif nLevel==18 then
							if i==1 then 
							
										if block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="cruiseship" then love.graphics.draw (cruiseship, ((x+13+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
									elseif block.name=="chimney" then love.graphics.draw (chimney, ((x+1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,-dividx/2,dividy/2)
									elseif block.name=="glass" then love.love.graphics.draw (glass, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="glassstand" then love.graphics.draw (glassstand, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end
							--]]
							--level 19
						elseif nLevel==19 then
							if i==1 then 
									if block.name=="bota"	 then love.graphics.draw (bota, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="domino"	 then love.graphics.draw (domino, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="hul" 	 then love.graphics.draw (hul, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="kuzelka" then love.graphics.draw (kuzelka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								
								elseif block.name=="palka" then love.graphics.draw (palka, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="ping" then love.graphics.draw (ping, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x-2+1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="poseidon" then
													if poseidonframe>0 		and poseidonframe<0.25 	then love.graphics.draw (poseidon00, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>0.25	and poseidonframe<0.5 	then love.graphics.draw (poseidon01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>0.5 	and poseidonframe<0.75 	then love.graphics.draw (poseidon02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>0.75	and poseidonframe<1 	then love.graphics.draw (poseidon03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>1 		and poseidonframe<1.25 	then love.graphics.draw (poseidon04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>1.25 	and poseidonframe<1.5 	then love.graphics.draw (poseidon05, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>1.5 	and poseidonframe<1.75	then love.graphics.draw (poseidon06, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>1.75	and poseidonframe<2 	then love.graphics.draw (poseidon07, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>2 		and poseidonframe<2.25 	then love.graphics.draw (poseidon08, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>2.25	and poseidonframe<2.5 	then love.graphics.draw (poseidon09, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>2.5 	and poseidonframe<2.75 	then love.graphics.draw (poseidon10, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>2.75	and poseidonframe<3 	then love.graphics.draw (poseidon11, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>3 		and poseidonframe<3.25	then love.graphics.draw (poseidon12, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>3.25 	and poseidonframe<3.5 	then love.graphics.draw (poseidon13, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>3.5 	and poseidonframe<3.75 	then love.graphics.draw (poseidon14, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>3.75 	and poseidonframe<4 	then love.graphics.draw (poseidon15, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>4 		and poseidonframe<4.25 	then love.graphics.draw (poseidon16, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>4.25 	and poseidonframe<4.5 	then love.graphics.draw (poseidon17, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>4.5 	and poseidonframe<4.75 	then love.graphics.draw (poseidon18, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>4.75	and poseidonframe<5 	then love.graphics.draw (poseidon19, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>5 		and poseidonframe<5.25 	then love.graphics.draw (poseidon20, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>5.25	and poseidonframe<5.5 	then love.graphics.draw (poseidon21, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>5.5 	and poseidonframe<5.75 	then love.graphics.draw (poseidon22, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>5.75	and poseidonframe<6 	then love.graphics.draw (poseidon23, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>6 		and poseidonframe<6.25 	then love.graphics.draw (poseidon24, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>6.25 	and poseidonframe<6.5 	then love.graphics.draw (poseidon25, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>6.5 	and poseidonframe<6.75	then love.graphics.draw (poseidon26, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>6.75	and poseidonframe<7 	then love.graphics.draw (poseidon27, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>7		and poseidonframe<7.25	then love.graphics.draw (poseidon28, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>7.25	and poseidonframe<7.5 	then love.graphics.draw (poseidon29, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>7.5 	and poseidonframe<7.75 	then love.graphics.draw (poseidon30, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>7.75	and poseidonframe<8 	then love.graphics.draw (poseidon31, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>8		and poseidonframe<8.25 	then love.graphics.draw (poseidon32, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>8.25 	and poseidonframe<8.5 	then love.graphics.draw (poseidon33, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>8.5	and poseidonframe<8.75	then love.graphics.draw (poseidon34, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>8.75	and poseidonframe<9 	then love.graphics.draw (poseidon35, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>9		and poseidonframe<9.25	then love.graphics.draw (poseidon36, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>9.25	and poseidonframe<9.5 	then love.graphics.draw (poseidon37, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>9.5 	and poseidonframe<9.75	then love.graphics.draw (poseidon38, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>9.75	and poseidonframe<10 	then love.graphics.draw (poseidon39, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>10		and poseidonframe<10.25 then love.graphics.draw (poseidon40, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>10.25 	and poseidonframe<10.5 	then love.graphics.draw (poseidon41, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>10.5  	and poseidonframe<10.75	then love.graphics.draw (poseidon42, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>10.75 	and poseidonframe<11 	then love.graphics.draw (poseidon43, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>11 	and poseidonframe<11.25 then love.graphics.draw (poseidon44, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>11.25 	and poseidonframe<11.5	then love.graphics.draw (poseidon45, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>11.5 	and poseidonframe<11.75	then love.graphics.draw (poseidon46, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>11.75 	and poseidonframe<12	then love.graphics.draw (poseidon47, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif poseidonframe>12 	and poseidonframe<12.25	then love.graphics.draw (poseidon48, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												
												end
												
								elseif block.name=="neptun" then
													if neptunframe>0 	and neptunframe<0.25 	then love.graphics.draw (neptun00, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>0.25 and neptunframe<0.5 	then love.graphics.draw (neptun01, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>0.5 	and neptunframe<0.75 	then love.graphics.draw (neptun02, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>0.75 and neptunframe<1 		then love.graphics.draw (neptun03, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>1 	and neptunframe<1.25 	then love.graphics.draw (neptun04, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>1.25 and neptunframe<1.5 	then love.graphics.draw (neptun05, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>1.5 	and neptunframe<1.75	then love.graphics.draw (neptun06, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>1.75 and neptunframe<2 		then love.graphics.draw (neptun07, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>2 	and neptunframe<2.25 	then love.graphics.draw (neptun08, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>2.25 and neptunframe<2.5 	then love.graphics.draw (neptun09, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>2.5 	and neptunframe<2.75 	then love.graphics.draw (neptun10, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>2.75 and neptunframe<3 		then love.graphics.draw (neptun11, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>3 	and neptunframe<3.25	then love.graphics.draw (neptun12, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>3.25 and neptunframe<3.5 	then love.graphics.draw (neptun13, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>3.5 	and neptunframe<3.75 	then love.graphics.draw (neptun14, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>3.75 and neptunframe<4 		then love.graphics.draw (neptun15, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>4 	and neptunframe<4.25 	then love.graphics.draw (neptun16, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>4.25 and neptunframe<4.5 	then love.graphics.draw (neptun17, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>4.5 	and neptunframe<4.75 	then love.graphics.draw (neptun18, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>4.75 and neptunframe<5 		then love.graphics.draw (neptun19, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>5 	and neptunframe<5.25 	then love.graphics.draw (neptun20, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>5.25 and neptunframe<5.5 	then love.graphics.draw (neptun21, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>5.5 	and neptunframe<5.75 	then love.graphics.draw (neptun22, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>5.75	and neptunframe<6 		then love.graphics.draw (neptun23, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>6 	and neptunframe<6.25 	then love.graphics.draw (neptun24, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>6.25 and neptunframe<6.5 	then love.graphics.draw (neptun25, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>6.5 	and neptunframe<6.75	then love.graphics.draw (neptun26, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>6.75	and neptunframe<7 		then love.graphics.draw (neptun27, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>7	and neptunframe<7.25	then love.graphics.draw (neptun28, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>7.25	and neptunframe<7.5 	then love.graphics.draw (neptun29, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>7.5 	and neptunframe<7.75 	then love.graphics.draw (neptun30, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>7.75	and neptunframe<8 		then love.graphics.draw (neptun31, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>8	and neptunframe<8.25 	then love.graphics.draw (neptun32, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>8.25 and neptunframe<8.5 	then love.graphics.draw (neptun33, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>8.5	and neptunframe<8.75	then love.graphics.draw (neptun34, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>8.75	and neptunframe<9 		then love.graphics.draw (neptun35, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>9	and neptunframe<9.25	then love.graphics.draw (neptun36, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>9.25	and neptunframe<9.5 	then love.graphics.draw (neptun37, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>9.5 	and neptunframe<9.75	then love.graphics.draw (neptun38, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>9.75	and neptunframe<10 		then love.graphics.draw (neptun39, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>10	and neptunframe<10.25 	then love.graphics.draw (neptun40, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>10.25 and neptunframe<10.5 	then love.graphics.draw (neptun41, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>10.5  and neptunframe<10.75	then love.graphics.draw (neptun42, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>10.75 and neptunframe<11 	then love.graphics.draw (neptun43, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>11 	 and neptunframe<11.25 	then love.graphics.draw (neptun44, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif neptunframe>11.25 and neptunframe<11.5 	then love.graphics.draw (neptun45, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
											
												end
								
								elseif block.name=="tenisak" then love.graphics.draw (tenisak, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
							--level 20
								elseif nLevel==20 then
							if i==1 then
								if block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="amphora" then love.graphics.setColor(0,dividx,dividy)
										love.graphics.draw (amphora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										love.graphics.setColor(cs1block)
								elseif block.name=="shell" then love.graphics.draw (shell, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="sculpture" and skin=="remake" then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="sculpture" and skin=="classic" then
													if faceframe>0   and faceframe<0.5 then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif faceframe>0.5 and faceframe<1 then love.graphics.draw (sculpture1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif faceframe>1 and faceframe<1.5 then love.graphics.draw (sculpture2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="elevator1" then love.graphics.draw (elevator1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="elevator2" then love.graphics.draw (elevator2, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="cross" and skin=="remake" then love.graphics.draw (cross, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="cross" and skin=="classic" then
												
													if crossframe>0   and crossframe<0.5 then love.graphics.draw (cross, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crossframe>0.5 and crossframe<1 then love.graphics.draw (cross1, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crossframe>1 and crossframe<1.5 then love.graphics.draw (cross2, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								 --love.graphics.draw (cross, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
							--level 21
								elseif nLevel==21 then
							if i==1 then
									if block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="crab" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2 and crabframe<2.5 then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2.5 and crabframe<3 then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3 and crabframe<3.5 then love.graphics.draw (krab_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3.5 and crabframe<4 then love.graphics.draw (krab_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4 and crabframe<4.5 then love.graphics.draw (krab_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4.5 and crabframe<5 then love.graphics.draw (krab_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										
								elseif block.name=="pedestal" then love.graphics.draw (pedestal, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipelong" then love.graphics.draw (pipelong, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipeshort" then love.graphics.draw (pipeshort, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="arm" then love.graphics.draw (arm, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform" then love.graphics.draw (platform, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="statue" and skin=="remake" then love.graphics.draw (statue, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="statue" and skin=="classic" and statuestatus=="talking" then 
													if statueframe>0   and statueframe<0.2 then love.graphics.draw (statuecl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>0.2 and statueframe<0.4 then love.graphics.draw (statue1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>0.4 and statueframe<0.6 then love.graphics.draw (statue2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>0.6 and statueframe<0.8 then love.graphics.draw (statue3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>0.8 and statueframe<1.0 then love.graphics.draw (statue4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>1.0 and statueframe<1.2 then love.graphics.draw (statue5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.2 and statueframe<1.4 then love.graphics.draw (statue6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.4 and statueframe<1.6 then love.graphics.draw (statue7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.6 and statueframe<1.8 then love.graphics.draw (statue8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.8 and statueframe<2.0 then love.graphics.draw (statue9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.0 and statueframe<2.2 then love.graphics.draw (statue10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.2 and statueframe<2.4 then love.graphics.draw (statue11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.4 and statueframe<2.6 then love.graphics.draw (statue12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.6 and statueframe<2.8 then love.graphics.draw (statue13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.8 and statueframe<3.0 then love.graphics.draw (statue14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.0 and statueframe<3.2 then love.graphics.draw (statue15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.2 and statueframe<3.4 then love.graphics.draw (statue16, ((x+dx-1))*tileSize+frgy, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
												elseif statueframe>3.4 and statueframe<3.6 then love.graphics.draw (statue17, ((x+dx-1))*tileSize+frgy, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
												elseif statueframe>3.6 and statueframe<3.8 then love.graphics.draw (statue18, ((x+dx-1))*tileSize+frgy, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.8 and statueframe<4.0 then love.graphics.draw (statue19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								elseif block.name=="statue" and skin=="classic" and statuestatus=="idle" then love.graphics.draw (statuecl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2) 
								
								--love.graphics.draw (statuecl, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
								end
							end
							
							--level 22
								elseif nLevel==22 then
							if i==1 then
									if block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe7" then love.graphics.draw (pipe7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe8" then love.graphics.draw (pipe8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe9" then love.graphics.draw (pipe9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe10" then love.graphics.draw (pipe10, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform" then love.graphics.draw (platform, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform2" then love.graphics.draw (platform2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform3" then love.graphics.draw (platform3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="stairs" then love.graphics.draw (stairs, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="sculpture" then
													if faceframe>0   and faceframe<0.5 then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif faceframe>0.5 and faceframe<1 then love.graphics.draw (sculpture1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif faceframe>1 and faceframe<1.5 then love.graphics.draw (sculpture2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								end
							end
							
							--level 23
							
							elseif nLevel==23 then
							if i==1 then
									if block.name=="column1" then love.graphics.draw (column1, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="column2" then love.graphics.draw (column2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="column3" then love.graphics.draw (column3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="zeus" and skin=="remake" then love.graphics.draw (zeus, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="zeus" and skin=="classic" then love.graphics.draw (zeus, ((x-1+dx-1))*tileSize+frgx, (y-1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="temple1" then love.graphics.draw (temple1, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="temple2" then love.graphics.draw (temple2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="stairs" then love.graphics.draw (stairs, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								end
							end
							
							--level 24
							
							elseif nLevel==24 then
							if i==1 then
									if block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="col" then love.graphics.draw (col, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="col2" then love.graphics.draw (col2, ((x-1+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="col3" then love.graphics.draw (col3, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="col4" then love.graphics.draw (col4, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="col5" then love.graphics.draw (col5, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="columns" then love.graphics.draw (columns, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x-1+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform" then love.graphics.draw (platform, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform2" then love.graphics.draw (platform2, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform3" then love.graphics.draw (platform3, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="platform4" then love.graphics.draw (platform4, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="pedestal" then love.graphics.draw (pedestal, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="octopus" then love.graphics.draw (octopus, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
							
								end
							end
								
								--level 25
								elseif nLevel==25 then
								if i==1 then
										if block.name=="egyptianwoman" then love.graphics.draw (egyptianwoman, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="scarab" then love.graphics.draw (scarab, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pharaon" then love.graphics.draw (faraon, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mummy" then love.graphics.draw (mummy, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="mummycat" then love.graphics.draw (mumycat, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stul" then love.graphics.draw (stul, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="desticka" then love.graphics.draw (desticka, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)

									end
								end
							
							--level 26
								elseif nLevel==26 then
								if i==1 then
									if block.name=="crab" then
									if (math.fmod(beat,2) ==0) then love.graphics.draw (crab, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)	
										elseif (math.fmod(beat,3) ==0) then love.graphics.draw (craba2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										else love.graphics.draw (craba3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										end
										
									elseif block.name=="speaker" then
										if (math.fmod(beat,2) ==0) then love.graphics.draw (speaker, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,0.4,0.4)
										else love.graphics.draw (speaker, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										end
										
									elseif block.name=="bigpipe" then love.graphics.draw (bigpipe, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="skull" then love.graphics.draw (skull, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="face" then love.graphics.draw (face, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column1" then love.graphics.draw (column1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column2" then love.graphics.draw (column2, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column3" then love.graphics.draw (column3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column4" then love.graphics.draw (column4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize+frgx, (y-0.1+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
								end
							
							--level 27
								elseif nLevel==27 then
							if i==1 then
										if block.name=="ball" then love.graphics.draw (balonek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if ballframe>0   and ballframe<0.5 	then love.graphics.draw (balonek_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif ballframe>0.5 and ballframe<1 	then love.graphics.draw (balonek_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ballframe>1 	 and ballframe<1.5	then love.graphics.draw (balonek_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif ballframe>1.5 and ballframe<2 	then love.graphics.draw (balonek_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="anticka" then --love.graphics.draw (anticka_hlava_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if antickaframe>0   and antickaframe<0.5 	then love.graphics.draw (anticka_hlava_01, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif antickaframe>0.5 and antickaframe<1 		then love.graphics.draw (anticka_hlava_02, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif antickaframe>1 	and antickaframe<1.5	then love.graphics.draw (anticka_hlava_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												--elseif antickaframe>1.5 and antickaframe<2 		then love.graphics.draw (anticka_hlava_03, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="anticka2" then love.graphics.draw (anticka_hlava_04, ((x+dx-2))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="crab" then
													if crabframe>0   and crabframe<0.5 	then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 	then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 	 and crabframe<1.5	then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 	then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2 	 and crabframe<2.5 	then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2.5 and crabframe<3 	then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3 	 and crabframe<3.5 	then love.graphics.draw (krab_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3.5 and crabframe<4 	then love.graphics.draw (krab_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4 	 and crabframe<4.5 	then love.graphics.draw (krab_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4.5 and crabframe<5 	then love.graphics.draw (krab_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
												
									elseif block.name=="crab2" then --love.graphics.draw (kr_00, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
													if crab2frame>0   and crab2frame<0.5 	then love.graphics.draw (kr_00, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crab2frame>0.5 and crab2frame<1 		then love.graphics.draw (kr_01, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>1   and crab2frame<1.5	then love.graphics.draw (kr_02, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>1.5 and crab2frame<2 		then love.graphics.draw (kr_03, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>2   and crab2frame<2.5 	then love.graphics.draw (kr_04, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>2.5 and crab2frame<3 		then love.graphics.draw (kr_05, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>3   and crab2frame<3.5 	then love.graphics.draw (kr_06, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>3.5 and crab2frame<4 		then love.graphics.draw (kr_07, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>4   and crab2frame<4.5 	then love.graphics.draw (kr_08, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>5	  and crab2frame<5.5 	then love.graphics.draw (kr_09, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>5.5 and crab2frame<6 		then love.graphics.draw (kr_10, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>6	  and crab2frame<6.5 	then love.graphics.draw (kr_11, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>6.5 and crab2frame<7 		then love.graphics.draw (kr_12, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>7	  and crab2frame<7.5 	then love.graphics.draw (kr_13, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>7.5 and crab2frame<8 		then love.graphics.draw (kr_14, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>8	  and crab2frame<8.5 	then love.graphics.draw (kr_15, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>8.5 and crab2frame<9 		then love.graphics.draw (kr_16, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crab2frame>9	 and crab2frame<9.5 	then love.graphics.draw (kr_17, ((x+dx-3))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="shrimp" then -- love.graphics.draw (shrimp_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
													if shrimpframe>0   and shrimpframe<0.5 	then love.graphics.draw (shrimp_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif shrimpframe>0.5 and shrimpframe<1 	then love.graphics.draw (shrimp_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif shrimpframe>1   and shrimpframe<1.5	then love.graphics.draw (shrimp_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif shrimpframe>1.5 and shrimpframe<2 	then love.graphics.draw (shrimp_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif shrimpframe>2   and shrimpframe<2.5 	then love.graphics.draw (shrimp_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="statue" then --love.graphics.draw (statue1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgx,0,dividx/2,dividy/2)
													if statueframe>0   and statueframe<0.2 then love.graphics.draw (statuecl, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>0.2 and statueframe<0.4 then love.graphics.draw (statue1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>0.4 and statueframe<0.6 then love.graphics.draw (statue2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>0.6 and statueframe<0.8 then love.graphics.draw (statue3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>0.8 and statueframe<1.0 then love.graphics.draw (statue4, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif statueframe>1.0 and statueframe<1.2 then love.graphics.draw (statue5, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.2 and statueframe<1.4 then love.graphics.draw (statue6, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.4 and statueframe<1.6 then love.graphics.draw (statue7, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.6 and statueframe<1.8 then love.graphics.draw (statue8, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>1.8 and statueframe<2.0 then love.graphics.draw (statue9, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.0 and statueframe<2.2 then love.graphics.draw (statue10, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.2 and statueframe<2.4 then love.graphics.draw (statue11, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.4 and statueframe<2.6 then love.graphics.draw (statue12, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.6 and statueframe<2.8 then love.graphics.draw (statue13, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>2.8 and statueframe<3.0 then love.graphics.draw (statue14, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.0 and statueframe<3.2 then love.graphics.draw (statue15, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.2 and statueframe<3.4 then love.graphics.draw (statue16, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.4 and statueframe<3.6 then love.graphics.draw (statue17, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.6 and statueframe<3.8 then love.graphics.draw (statue18, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif statueframe>3.8 and statueframe<4.0 then love.graphics.draw (statue19, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="stone" then love.graphics.draw (stone, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="stonesmall" then love.graphics.draw (stonesmall, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									end
							end
							--level 28
								elseif nLevel==28 then
							if i==1 then
										if block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="amphora" then
										--love.graphics.setColor(0.5,1,1)
					
											love.graphics.draw (amphora, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											love.graphics.setColor(cs1block)
										elseif block.name=="shell" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
											love.graphics.draw (shell, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
											elseif block.name=="sculpture" and skin=="remake" then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="sculpture" and skin=="classic" then
													if faceframe>0   and faceframe<0.5 then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif faceframe>0.5 and faceframe<1 then love.graphics.draw (sculpture1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif faceframe>1 and faceframe<1.5 then love.graphics.draw (sculpture2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
										elseif block.name=="elevator1" then love.graphics.draw (elevator1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="elevator2" then love.graphics.draw (elevator2, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
										elseif block.name=="cross" and skin=="remake" then love.graphics.draw (cross, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
								elseif block.name=="cross" and skin=="classic" then
												
													if crossframe>0   and crossframe<0.5 then love.graphics.draw (cross, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crossframe>0.5 and crossframe<1 then love.graphics.draw (cross1, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crossframe>1 and crossframe<1.5 then love.graphics.draw (cross2, ((x-1+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								end
							end
							
							--level 29
								elseif nLevel==29 then
							if i==1 then
										if block.name=="plug" then love.graphics.draw (plug, ((x-2+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe1block" then love.graphics.draw (pipe1block, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="pipe2blocks" then love.graphics.draw (pipe2blocks, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column" then love.graphics.draw (column, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="column2" then love.graphics.draw (column2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
									elseif block.name=="crab" then
													if crabframe>0   and crabframe<0.5 then love.graphics.draw (krab_00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif crabframe>0.5 and crabframe<1 then love.graphics.draw (krab_01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1 and crabframe<1.5 then love.graphics.draw (krab_02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>1.5 and crabframe<2 then love.graphics.draw (krab_03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2 and crabframe<2.5 then love.graphics.draw (krab_04, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>2.5 and crabframe<3 then love.graphics.draw (krab_05, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3 and crabframe<3.5 then love.graphics.draw (krab_06, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>3.5 and crabframe<4 then love.graphics.draw (krab_07, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4 and crabframe<4.5 then love.graphics.draw (krab_08, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif crabframe>4.5 and crabframe<5 then love.graphics.draw (krab_09, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
													if snailframe>0   and snailframe<0.5 then love.graphics.draw (snail00, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif snailframe>0.5 and snailframe<1 then love.graphics.draw (snail01, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1 and snailframe<1.5 then love.graphics.draw (snail02, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif snailframe>1.5 and snailframe<2 then love.graphics.draw (snail03, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
									elseif block.name=="face" then
													if faceframe>0   and faceframe<0.5 then love.graphics.draw (sculpture, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2) 
												elseif faceframe>0.5 and faceframe<1 then love.graphics.draw (sculpture1, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												elseif faceframe>1 and faceframe<1.5 then love.graphics.draw (sculpture2, ((x+dx-1))*tileSize+frgx, (y+dy-1)*tileSize+frgy,0,dividx/2,dividy/2)
												end
								end
							end
						elseif nLevel>29 then drawobjectsassets30(pb, block,x,y,dx,dy,tileSize,i,gridSize,dividx,dividy)
						end
				end
			
			    end

			
			if threeD==true then
			--if threeD==true and not (block.name=="chair1") then
			dream:draw(cube1, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5)
			--dream:draw(player1_3d, -10,-8, -50 , -1.5,1.5,1.5)
			--dream:draw(player2_3d, -38,-6, -50 , 1.1,1.5,1.5)
			
		   end
--dream:draw(cube1,  1, 1,-60, 10,10,10)
		
		--dream:draw(cube1, ((x+dx-1)*threeDcellwidth+1) , (y+dy-1)*threeDcellheight, threeDcellz,threeDcellwidth-0.5,threeDcellwidth-0.5,threeDcellwidth-0.5)
	end
	
--[[
The drawblockcolors() function appears to be a drawing function for blocks that are colored differently based on the value of the screensaver and threeD variables. If threeD is true and screensaver is true, it sets the color to green and rotates a "diorama" object before drawing a "dream" object. If threeD is true and screensaver is false, it sets the color to a variable called blockscolor before drawing the "dream" object. Finally, the function sets the color back to white and returns nothing.
--]]
function drawblockcolors()
	

		if threeD==true then
			if screensaver==true then 
				love.graphics.setColor(0,1,0.5)
				diorama:rotateY(0.0001)
				dream:draw(diorama, 0, 0, -100,15,15,15)
				
			elseif screensaver==false then
		love.graphics.setColor(blockscolor)
			
    end
    dream:present()
		end
		
		love.graphics.setColor(1,1,1)		
	end

--The drawbigfishaura(agent, tileSize, sx) function is a drawing function that draws a transparent aura around a "bigfish" object. It sets the color to white with an alpha value of 0.1 before drawing a "fish4x2clupaura" object based on the position and direction of the agent object. The tileSize and sx parameters control the scale of the image. Finally, the function sets the color back to white with an alpha value of 1 and returns nothing.
function drawbigfishaura(agent, tileSize,sx)
love.graphics.setColor(1,1,1,0.1)
		if agent.direction=="left" then	love.graphics.draw (fish4x2clupaura,  (agent.x-3)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
	elseif agent.direction=="right" then love.graphics.draw (fish4x2clupaura, (agent.x+1)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
	end
	love.graphics.setColor(1,1,1,1)
end

--The drawsmallfishaura(agent, tileSize, sx) function is a drawing function that draws a transparent aura around a "smallfish" object. It sets the color to white with an alpha value of 0.1 before drawing a "fish3x1clupaura" object based on the position and direction of the agent object. The tileSize and sx parameters control the scale of the image. Finally, the function sets the color back to white with an alpha value of 1 and returns nothing.
function drawsmallfishaura(agent, tileSize,sx)
love.graphics.setColor(1,1,1,0.1)
		if agent.direction=="left"  then love.graphics.draw (fish3x1clupaura, (agent.x-1.5)*tileSize+agent.image_dx+frgx, (agent.y-1.5)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
	elseif agent.direction=="right" then love.graphics.draw (fish3x1clupaura, (agent.x)*tileSize+agent.image_dx+frgx, (agent.y-1.5)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
	end
	love.graphics.setColor(1,1,1,1)
end

--[[
This function is a Lua function used in a video game. Its purpose is to draw the assets of the game's player character based on various conditions such as the level number and whether certain switches or buttons have been pushed.

The function takes three parameters: agent, tileSize, and sx. agent is an object representing the player character in the game, tileSize is a numeric value representing the size of the tiles used in the game, and sx is also a numeric value used in the calculation of the player character's image.

The function uses a series of conditional statements to determine which color to set for the player character's assets. Depending on the level number and other factors, the color of the assets may change. The function also sets the color to be used when adjusting the colors of the player character's assets.

After setting the appropriate colors, the function uses another conditional statement to determine whether to draw a silhouette of the player character or the full player character image.

Finally, the function draws the appropriate player character image based on its current state, such as whether it is dead or which direction it is facing.
--]]
function drawplayerassets(agent, tileSize,sx)
	--if shader2==true then 	--love.graphics.setColor(0.09,0.27,0.41)
	--end
	--if threeD then
		--dream:draw(player1_3d, (agent.x-3)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, -50 , -1.5,1.5,1.5)
		--dream:draw(player2_3d, (agent.x-3)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, -50 , 1.1,1.5,1.5)
	--end
	if palette==1 then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(1,1,1)		-- adjust colors
					else cs1block={adjustr+0.3,adjustg+0.3,adjustb+0.3,1}
					love.graphics.setColor(cs1block)
					end
				--special colors and light effects for the fish on specific levels
				if nLevel==54 and enginepushed==false then												--engine level 54
				--love.graphics.setColor(1,1,1,1)   						
					if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(0.3,1,0.3,1)		-- adjust colors
					else cs1block={adjustr,adjustg,adjustb,1}
					love.graphics.setColor(cs1block)
					end
			elseif nLevel==54 and enginepushed==true then 		love.graphics.setColor(subbeat/3+0.35,0,0,1)
			--elseif nLevel==55 and pipepushed==false then												--steel level 55
					--[[if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(1,1,1)		-- adjust colors
					else cs1block={adjustr,adjustg,adjustb,1}
					love.graphics.setColor(cs1block)
					end--]]
			elseif nLevel==15 then love.graphics.setColor(beat/40+0.7,beat/30,beat/40+0.7,1)
			elseif nLevel==20 then love.graphics.setColor(beat/80+0.3,beat/20,beat/25+0.5,1)
			elseif nLevel==22 then love.graphics.setColor(agent.x/10-2.5,agent.x/10-3.5,agent.x/10,1)
			elseif nLevel==52 then love.graphics.setColor(beat/40,beat/30+0.2,beat/40,1)
			elseif nLevel==53 then love.graphics.setColor(-agent.x/10+3,0.7,-agent.x/10+3,1)
			elseif nLevel==55 then
					if pipepushed==false then love.graphics.setColor(0.3,1,0.3,1)
				elseif pipepushed==true	 then love.graphics.setColor(1/subbeat/3+0.35,0,0,1)
				end
			elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==false then	love.graphics.setColor(1/subbeat/3.2,1,1,1)	 --lights level 56
			elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==false then	love.graphics.setColor(0.3,0.8,0.8,1)		-- lights are on
			elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2-0.5,0.8,0.8,1)	-- lights are on
			elseif nLevel==57 or nLevel==58 then love.graphics.setColor(0.3,1,0.3,1)
			end
			
			
		if not (nLevel==66 or nLevel==110 ) then			-- draw default assets on all levels excepting the special ones
		if nLevel==56 and lightswitchon==false then love.graphics.setColor(0,0,0,1) end	-- only a silhouete will be drawn
		
			--[[if threeD==true then
					if agent.name=="fish-4x2" then 
																									   --dream:draw(cube1, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5)
							if not agent.dead and agent.direction=="right" or agent.direction==nil 	then --dream:draw(player1_3d, agent.x, agent.y, -50 , 50,50,50)
						elseif not agent.dead and agent.direction=="left"  							then --dream:draw(player1_3d, agent.x, agent.y, -50 , 50,50,50)
						elseif agent.dead==true 													then --dream:draw (fish1dead, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
					if agent.name=="fish-3x1" then
							if not agent.dead and agent.direction=="right" or agent.direction==nil 	then --dream:draw (player2_3d, agent.x, agent.y, -14.5 , 0.5, 0.5, 0.5)
						elseif not agent.dead and agent.direction=="left" 							then --dream:draw (player2_3d, agent.x, agent.y, -14.5 , 0.5, 0.5, 0.5)
						elseif agent.dead==true 													then --love.graphics.draw (fish2dead, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
				
			end
			--]]
			if skin=="remake" then
				--if nLevel==1 then	-- if there is zoom then experimental zoom draw (only on level 1 yet)
			
					if agent.name=="fish-4x2" then
					causticsAndNormals:send("u_normals", fish4x2Normal)
					causticsAndNormals:send("u_heightmap", fish4x2Height)
							if not agent.dead and agent.direction=="right" or agent.direction==nil 	then love.graphics.draw (fish4x2, (agent.x-2.5)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, 0, sx*scalepl/4, agent.image_sy*scalepl/4, agent.image_ox,  agent.image_oy)
						elseif not agent.dead and agent.direction=="left"  							then love.graphics.draw (fish4x2, (agent.x+0.5)*tileSize+agent.image_dx+frgx, (agent.y-2)*tileSize+agent.image_dy+frgy, 0, sx*scalepl/4, agent.image_sy*scalepl/4, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true 													then love.graphics.draw (fish1dead, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
					if agent.name=="fish-3x1" then
						causticsAndNormals:send("u_normals", fish3x1Normal)
						causticsAndNormals:send("u_normals", fish3x1Height)
							if not agent.dead and agent.direction=="right" or agent.direction==nil 	then love.graphics.draw (fish3x1, (agent.x-2.1)*tileSize+agent.image_dx+frgx, (agent.y-1.5)*tileSize+agent.image_dy+frgy, 0, sx*scalepl/4, agent.image_sy*scalepl/4, agent.image_ox,  agent.image_oy)
						elseif not agent.dead and agent.direction=="left" 							then love.graphics.draw (fish3x1, (agent.x+0.1)*tileSize+agent.image_dx+frgx, (agent.y-1.5)*tileSize+agent.image_dy+frgy, 0, sx*scalepl/4, agent.image_sy*scalepl/4, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true 													then love.graphics.draw (fish2dead, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end

				
			elseif skin=="classic" then
				if skinupscaled==false then
					if agent.name=="fish-4x2" then
							if not agent.dead then 
								if fish1status=="moving" then	--if swiming and not idle
											if bigfishframe>0 and bigfishframe<0.25 then 	love.graphics.draw (fish4x2clswam00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.25 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clswam01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.5 and bigfishframe<0.75 then	love.graphics.draw (fish4x2clswam02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.75 and bigfishframe<1 then	love.graphics.draw (fish4x2clswam03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.25 then	love.graphics.draw (fish4x2clswam04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.25 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clswam05, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--write contour
										if fish2highlight==true then
												if bigfishframe>0 and bigfishframe<0.25 then 	love.graphics.draw (fish4x2clswam00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.25 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clswam01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.5 and bigfishframe<0.75 then	love.graphics.draw (fish4x2clswam02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.75 and bigfishframe<1 then	love.graphics.draw (fish4x2clswam03co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1 and bigfishframe<1.25 then	love.graphics.draw (fish4x2clswam04co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1.25 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clswam05co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								elseif fish1status=="pushing" then love.graphics.draw (fish4x2clpush00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
								--pushing contour
								if fish2highlight==true then love.graphics.draw (fish4x2clpush00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
								end
								
								elseif fish1status=="turningleft" then		-- turning left
											if bigfishframe>0 and bigfishframe<0.33 then 	love.graphics.draw (fish4x2clturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.33 and bigfishframe<0.66 then	love.graphics.draw (fish4x2clturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.66 and bigfishframe<1 then	love.graphics.draw (fish4x2clturn02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.33 then	love.graphics.draw (fish4x2clturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.33 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.5 then fish1status="idle"
										end
								elseif fish1status=="turningright" then		-- turning right
											if bigfishframe>0 and bigfishframe<0.33 then 	love.graphics.draw (fish4x2clturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.33 and bigfishframe<0.66 then	love.graphics.draw (fish4x2clturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.66 and bigfishframe<1 then	love.graphics.draw (fish4x2clturn02l, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.33 then	love.graphics.draw (fish4x2clturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.33 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.5 then fish1status="idle"
										end
								elseif fish1status=="talking" then		-- if not swiming and idle
											if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cltalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2cltalk01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2cltalk02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
											--write contour
										if fish2highlight==true then
												if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cltalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end
										end
								elseif fish1status=="switch" then	-- if switching fish
										--fish 1
										love.graphics.setColor(1,1,1,1)
											if bigfishframe>0 and bigfishframe<4 then	love.graphics.draw (fish4x2cltalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
													--write contour
													if fish2highlight==true then
															if bigfishframe>0 and bigfishframe<4 then	love.graphics.draw (fish4x2cltalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end			
													end
										elseif bigfishframe<4 then fish1status="idle"	
										end			
										
								elseif fish1status=="idle" then	-- if not swiming and idle
										if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clrest00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2clrest01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clrest02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									end
									--write contour
										if fish2highlight==true then
												if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clrest00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2clrest01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clrest02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								end
							--love.graphics.draw (fish4x2cl, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true then love.graphics.draw (fish1deadcl, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
					if agent.name=="fish-3x1" then
							if not agent.dead then 
									if fish2status=="moving" then		--if swiming and not idle
											if smallfishframe>0 and smallfishframe<0.25 then 	love.graphics.draw (fish3x1clswam00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.25 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clswam01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<0.75 then	love.graphics.draw (fish3x1clswam02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.75 and smallfishframe<1 then	love.graphics.draw (fish3x1clswam03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.25 then	love.graphics.draw (fish3x1clswam04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.25 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clswam05, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--write contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.25 then 	love.graphics.draw (fish3x1clswam00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.25 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clswam01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.5 and smallfishframe<0.75 then	love.graphics.draw (fish3x1clswam02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.75 and smallfishframe<1 then	love.graphics.draw (fish3x1clswam03co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1 and smallfishframe<1.25 then	love.graphics.draw (fish3x1clswam04co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1.25 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clswam05co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								elseif fish2status=="pushing" then love.graphics.draw (fish3x1clpush00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
								--pushing contour
								if fish1highlight==true then love.graphics.draw (fish3x1clpush00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
								end
								elseif fish2status=="turningleft" then		-- turning left
											if smallfishframe>0 and smallfishframe<0.33 then 	love.graphics.draw (fish3x1clturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.33 and smallfishframe<0.66 then	love.graphics.draw (fish3x1clturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.66 and smallfishframe<1 then	love.graphics.draw (fish3x1clturn02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.33 then	love.graphics.draw (fish3x1clturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.33 and smallfishframe<1.4 then	love.graphics.draw (fish3x1clturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.4 then fish2status="idle"
										end
								elseif fish2status=="turningright" then		-- turning right
											if smallfishframe>0 and smallfishframe<0.33 then 	love.graphics.draw (fish3x1clturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.33 and smallfishframe<0.66 then	love.graphics.draw (fish3x1clturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.66 and smallfishframe<1 then	love.graphics.draw (fish3x1clturn02l, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.33 then	love.graphics.draw (fish3x1clturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.33 and smallfishframe<1.4 then	love.graphics.draw (fish3x1clturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.4 then fish2status="idle"
										end
								elseif fish2status=="talking" then	-- if not swiming and talking
											if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1cltalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1cltalk01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1cltalk02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--write contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1cltalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end
										end
								elseif fish2status=="switch" then	-- if switching fish						
										--fish 2
										love.graphics.setColor(1,1,1,1)
											if smallfishframe>0 and smallfishframe<4 then	love.graphics.draw (fish3x1cltalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
													--write contour
													if fish1highlight==true then
															if smallfishframe>0 and smallfishframe<4 then	love.graphics.draw (fish3x1cltalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end	
													end
										elseif smallfishframe<4 then fish1status="idle"	
										end	
										
								elseif fish2status=="idle" then		-- if not swiming and idle
											if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clrest00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1clrest01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clrest02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--write contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clrest00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1clrest01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clrest02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								end
							--love.graphics.draw (fish3x1cl, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true then love.graphics.draw (fish2deadcl, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
				elseif skinupscaled==true then
					if agent.name=="fish-4x2" then
							if not agent.dead then 
									if fish1status=="moving" then	--if swiming and not idle
										--draw aura
										if fish2highlight==true then drawbigfishaura(agent, tileSize,sx*scalepl) end
											--draw fish
											if bigfishframe>0 and bigfishframe<0.25 then 	love.graphics.draw (fish4x2clupswam00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.25 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clupswam01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.5 and bigfishframe<0.75 then	love.graphics.draw (fish4x2clupswam02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.75 and bigfishframe<1 then	love.graphics.draw (fish4x2clupswam03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.25 then	love.graphics.draw (fish4x2clupswam04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.25 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clupswam05, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										
										if fish2highlight==true then
											--draw contour
												if bigfishframe>0 and bigfishframe<0.25 then 	love.graphics.draw (fish4x2clupswam00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.25 and bigfishframe<0.5 then	love.graphics.draw (fish4x2clupswam01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.5 and bigfishframe<0.75 then	love.graphics.draw (fish4x2clupswam02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.75 and bigfishframe<1 then	love.graphics.draw (fish4x2clupswam03co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1 and bigfishframe<1.25 then	love.graphics.draw (fish4x2clupswam04co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1.25 and bigfishframe<1.5 then	love.graphics.draw (fish4x2clupswam05co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								elseif fish1status=="pushing" then
									--draw pushing aura
										if fish2highlight==true then drawbigfishaura(agent, tileSize,sx*scalepl) end
									--draw pushing fish
										love.graphics.draw (fish4x2cluppush00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									--draw pushing contour
										if fish2highlight==true then 		
										love.graphics.draw (fish4x2cluppush00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									end
								--[[elseif fish1status=="turningleft" then		-- turning left
											if bigfishframe>0 and bigfishframe<0.33 then 	love.graphics.draw (fish4x2clupturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.33 and bigfishframe<0.66 then	love.graphics.draw (fish4x2clupturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.66 and bigfishframe<1 then	love.graphics.draw (fish4x2clupturn02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.33 then	love.graphics.draw (fish4x2clupturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.33 and bigfishframe<1.4 then	love.graphics.draw (fish4x2clupturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.4 then fish1status="idle"
										end
								elseif fish1status=="turningright" then		-- turning right
											if bigfishframe>0 and bigfishframe<0.33 then 	love.graphics.draw (fish4x2clupturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.33 and bigfishframe<0.66 then	love.graphics.draw (fish4x2clupturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.66 and bigfishframe<1 then	love.graphics.draw (fish4x2clupturn02l, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.33 then	love.graphics.draw (fish4x2clupturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.33 and bigfishframe<1.4 then	love.graphics.draw (fish4x2clupturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1.4 then fish1status="idle"
										end
								--]]
								elseif fish1status=="talking" then	-- if not swiming and idle
											--draw talking aura
											if fish2highlight==true then drawbigfishaura(agent, tileSize,sx*scalepl) end
									

											--draw talking fish
									--if not (nLevel==1) then
											if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cluptalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2cluptalk01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2cluptalk02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
									--else	drawlips(agent, tileSize,sx*scalepl)
									--end
										--draw talking contour
											if fish1highlight==true then
												if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cluptalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end
											end
										
										elseif fish1status=="switch" then	-- if switching fish
												--draw switch aura
													if fish2highlight==true then drawbigfishaura(agent, tileSize,sx*scalepl) end
												--draw switch
												if bigfishframe>0 and bigfishframe<4 then	love.graphics.draw (fish4x2cluptalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe<4 then fish1status="idle"	
											end			
																							
													--draw switch contour
													if fish2highlight==true then
															if bigfishframe>0 and bigfishframe<4 then	love.graphics.draw (fish4x2cluptalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end			
													end
											
										
								elseif fish1status=="idle" then	-- if not swiming and idle
										--draw idle aura
											if fish2highlight==true then drawbigfishaura(agent, tileSize,sx*scalepl) end
										--draw idle
											if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cluprest00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2cluprest01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2cluprest02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--draw contour
										if fish2highlight==true then
												if bigfishframe>0 and bigfishframe<0.5 then	love.graphics.draw (fish4x2cluprest00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>0.5 and bigfishframe<1 then	love.graphics.draw (fish4x2cluprest01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif bigfishframe>1 and bigfishframe<1.5 then	love.graphics.draw (fish4x2cluprest02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								end
							--love.graphics.draw (fish4x2clup, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true then love.graphics.draw (fish1deadclup, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
					if agent.name=="fish-3x1" then
							if not agent.dead then 
								if fish2status=="moving" then		--if swiming and not idle
										--draw moving aura
											if fish1highlight==true then drawsmallfishaura(agent, tileSize,sx*scalepl) end
										--draw moving fish
											if smallfishframe>0 and smallfishframe<0.25 then 	love.graphics.draw (fish3x1clupswam00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.25 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clupswam01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<0.75 then	love.graphics.draw (fish3x1clupswam02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.75 and smallfishframe<1 then	love.graphics.draw (fish3x1clupswam03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.25 then	love.graphics.draw (fish3x1clupswam04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.25 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clupswam05, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
										--write contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.25 then 	love.graphics.draw (fish3x1clupswam00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.25 and smallfishframe<0.5 then	love.graphics.draw (fish3x1clupswam01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.5 and smallfishframe<0.75 then	love.graphics.draw (fish3x1clupswam02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.75 and smallfishframe<1 then	love.graphics.draw (fish3x1clupswam03co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1 and smallfishframe<1.25 then	love.graphics.draw (fish3x1clupswam04co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1.25 and smallfishframe<1.5 then	love.graphics.draw (fish3x1clupswam05co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
											
											
							elseif fish2status=="pushing" then love.graphics.draw (fish3x1cluppush00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									--draw pushing aura
											if fish1highlight==true then drawsmallfishaura(agent, tileSize,sx*scalepl) end
									--draw pushing fish
									love.graphics.draw (fish3x1cluppush00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
									--pushing contour 
										if fish1highlight==true then
											love.graphics.draw (fish3x1cluppush00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
								--[[elseif fish2status=="turningleft" then		-- turning left
											if smallfishframe>0 and smallfishframe<0.33 then 	love.graphics.draw (fish3x1clupturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.33 and smallfishframe<0.66 then	love.graphics.draw (fish3x1clupturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.66 and smallfishframe<1 then	love.graphics.draw (fish3x1clupturn02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.33 then	love.graphics.draw (fish3x1clupturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.33 and smallfishframe<1.4 then	love.graphics.draw (fish3x1clupturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.4 then fish2status="idle"
										end
								elseif fish2status=="turningright" then		-- turning right
											if smallfishframe>0 and smallfishframe<0.33 then 	love.graphics.draw (fish3x1clupturn04, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.33 and smallfishframe<0.66 then	love.graphics.draw (fish3x1clupturn03, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.66 and smallfishframe<1 then	love.graphics.draw (fish3x1clupturn02l, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.33 then	love.graphics.draw (fish3x1clupturn01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.33 and smallfishframe<1.4 then	love.graphics.draw (fish3x1clupturn00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1.4 then fish2status="idle"
										end
								--]]
							elseif fish2status=="talking" then		-- if not swiming and idle
											--draw talking aura
											if fish1highlight==true then drawsmallfishaura(agent, tileSize,sx*scalepl) end
									--if not (nLevel==1) then
											--draw talking fish
											if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1cluptalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1cluptalk01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1cluptalk02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
									--else	drawlips(agent, tileSize,sx*scalepl)
									--end
											--write contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.5 then
													love.graphics.draw (fish3x1cluptalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
												end
										end
							elseif fish2status=="switch" then	-- if switching fish						
										--draw switch aura
											if fish1highlight==true then drawsmallfishaura(agent, tileSize,sx*scalepl) end
										--draw switch 2
										love.graphics.setColor(1,1,1,1)
											if smallfishframe>0 and smallfishframe<4 then	love.graphics.draw (fish3x1cluptalk00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
													
										elseif smallfishframe<4 then fish1status="idle"	
										end
										--draw switch contour
											if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<4 then	love.graphics.draw (fish3x1cluptalk00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy) end
											end

								elseif fish2status=="idle" then		-- if not swiming and idle
											--draw idle aura
											if fish1highlight==true then drawsmallfishaura(agent, tileSize,sx*scalepl) end
											--draw idle fish
											if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1cluprest00, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1cluprest01, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1cluprest02, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
										end
											--draw idle contour
										if fish1highlight==true then
												if smallfishframe>0 and smallfishframe<0.5 then	love.graphics.draw (fish3x1cluprest00co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>0.5 and smallfishframe<1 then	love.graphics.draw (fish3x1cluprest01co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											elseif smallfishframe>1 and smallfishframe<1.5 then	love.graphics.draw (fish3x1cluprest02co, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
											end
										end
								end
							--love.graphics.draw (fish3x1clup, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.dead==true then love.graphics.draw (fish2deadclup, (agent.x-1)*tileSize+agent.image_dx+frgx, (agent.y-1)*tileSize+agent.image_dy+frgy, 0, -sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
					end
				end
			end
		
						
		end
		-- special colors and effects of the player's assets for different levels
		
		if nLevel==9 then 										--mirror level 9		  	
			--mirror
		--[[	love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
			--]]	

				if agent.x==7  and not (agent.y==2) then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
							(agent.y-1)*tileSize+agent.image_dy+frgy,
							0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.direction=="left" then											-- mirror left
							love.graphics.draw (player1mirrorleft, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
							(agent.y-1)*tileSize+agent.image_dy+frgy,
							0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
				elseif agent.x==7 and agent.y==2 then
							if agent.direction=="left" then	
								love.graphics.draw (player1mirror3left, 
								(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
								(agent.y-1)*tileSize+agent.image_dy+frgy,
								0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
							end
				
				elseif agent.x==8 and not (agent.y==2) then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror2, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
							(agent.y-1)*tileSize+agent.image_dy+frgy,
							0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						elseif agent.direction=="left" then	
							love.graphics.draw (player1mirror2left, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
							(agent.y-1)*tileSize+agent.image_dy+frgy,
							0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						end
				elseif agent.x==8 and agent.y==2 then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror3, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx+frgx, 
							(agent.y-1)*tileSize+agent.image_dy+frgy,
							0, sx*scalepl*-1, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
						
						end

				end
								
		
			
		elseif nLevel==56 and lightswitchon==false and agent.name=='fish-3x1'  then						-- lights are off, show only eyes
			love.graphics.setColor(1,1,1,1)
			love.graphics.draw (fish3x1_nolight, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==56 and lightswitchon==false and agent.name=='fish-4x2'  then						-- lights are off, show only eyes
			love.graphics.setColor(1,1,1,1)
			love.graphics.draw (fish4x2_nolight, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==65 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==65 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==66 and agent.name=='fish-3x1' then love.graphics.draw (z80fish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==66 and agent.name=='fish-4x2' then love.graphics.draw (z80fish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		elseif nLevel==110 and agent.name=='fish-3x1' then 										--???
			--mirror
			love.graphics.draw (fish3x1space, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif nLevel==110 and agent.name=='fish-4x2' then 										--???
			--mirror
			love.graphics.draw (fish4x2space, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		end
		
		--c64
		elseif palette==2 and agent.name=='fish-3x1' then love.graphics.draw (c64fish3x1, 
	
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==2 and agent.name=='fish-4x2' then love.graphics.draw (c64fish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		--nes
		elseif palette==3 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==3 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		--sinclair
		elseif palette==4 and agent.name=='fish-3x1' then love.graphics.draw (z80fish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==4 and agent.name=='fish-4x2' then love.graphics.draw (z80fish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
	
		--megadrive
		elseif palette==5 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==5 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		
		--high contrast
		elseif palette==6 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==6 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
		
		--grid
		elseif palette==7 and agent.name=='fish-3x1' then love.graphics.draw (gridfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		elseif palette==7 and agent.name=='fish-4x2' then love.graphics.draw (gridfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx+frgx, 
			(agent.y-1)*tileSize+agent.image_dy+frgy,
			0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox,  agent.image_oy)
		
	
		end
		
		drawplayerassetshighlight(agent, tileSize,sx)
end

--[[
This is a Lua function that takes three parameters: agent, tileSize, and sx. It draws highlighted player assets, specifically fish images, on a game screen. The function checks if the skin is set to "remake" and applies an experimental zoom draw on level 1. The highlighted fish are drawn with a yellow color, while non-highlighted fish are drawn with white or light yellow color.

The function first checks which type of fish asset needs to be drawn by checking the agent parameter's name. If it's a fish-3x1, it will either draw the highlighted or non-highlighted version of the gridfish3x1 asset based on the fish1highlight boolean value. If it's a fish-4x2, it will either draw the highlighted or non-highlighted version of the gridfish4x2 asset based on the fish2highlight boolean value.

The sx parameter is used to scale the image's x-coordinate, while the tileSize parameter is used to determine the size of the tiles on the game screen. The function uses several other parameters such as agent.direction, agent.x, agent.y, agent.image_dx, agent.image_dy, agent.image_sy, agent.image_ox, agent.image_oy, scalepl, frgx, and frgy to determine the position and orientation of the asset on the game screen.
--]]
function drawplayerassetshighlight(agent, tileSize,sx)
		if skin=="remake" then	-- if there is zoom then experimental zoom draw (only on level 1 yet)
				if agent.name=='fish-3x1' then
						if fish1highlight==true and (agent.direction=="right" or agent.direction==nil) then love.graphics.setColor (1,1,0)
								love.graphics.draw (gridfish3x1, 		-- draw grid fish
								(agent.x-1.1)*tileSize+agent.image_dx+frgx, 
								(agent.y-1.2)*tileSize+agent.image_dy+frgy,
								0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox+0.1,  agent.image_oy)
					elseif fish1highlight==true and agent.direction=="left" then love.graphics.setColor (1,1,0)
								love.graphics.draw (gridfish3x1, 		-- draw grid fish
								(agent.x-0.9)*tileSize+agent.image_dx+frgx, 
								(agent.y-1.2)*tileSize+agent.image_dy+frgy,
								0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox+0.1,  agent.image_oy)
					elseif fish1highlight==false then love.graphics.setColor (1,1,1)
					end
			elseif agent.name=='fish-4x2' then		
					if fish2highlight==true then love.graphics.setColor (1,1,0)
								love.graphics.draw (gridfish4x2, 			-- draw grid fish
								(agent.x-1)*tileSize+agent.image_dx+frgx, 
								(agent.y-1.2)*tileSize+agent.image_dy+frgy,
								0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox+0.1,  agent.image_oy)
					elseif fish2highlight==false then love.graphics.setColor (.9,.9,.8)
					end
			end
		else		-- if not remake skin draw assets as normal (without camera movement or zoom)
				if agent.name=='fish-3x1' then
						if fish1highlight==true then love.graphics.setColor (1,1,0)
								if skin=="remake" then
									love.graphics.draw (gridfish3x1, 		-- draw grid fish
									(agent.x-1)*tileSize+agent.image_dx, 
									(agent.y-1)*tileSize+agent.image_dy,
									0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox+0.1,  agent.image_oy)
							end
					elseif fish1highlight==false then love.graphics.setColor (1,1,1)
					end
			elseif agent.name=='fish-4x2' then		
					if fish2highlight==true then love.graphics.setColor (1,1,0)
							if skin=="remake" then
								love.graphics.draw (gridfish4x2, 			-- draw grid fish
								(agent.x-1)*tileSize+agent.image_dx, 
								(agent.y-1)*tileSize+agent.image_dy,
								0, sx*scalepl, agent.image_sy*scalepl, agent.image_ox+0.1,  agent.image_oy)
							end
					elseif fish2highlight==false then love.graphics.setColor (.9,.9,.8)
					end
			end
		end
end

--The first function changeagentcolor() is a conditional statement that sets the color of the agent based on the current value of the palette variable. Depending on the value of palette, the function sets the color to one of the pre-defined color values (cs1agent1, cs2agent1, cs3agent1, cs4agent1, cs5agent1, or cs6agent2). If palette is 7, the function doesn't change the color.
function changeagentcolor()
	
					if palette==1 then love.graphics.setColor(cs1agent1) 
				elseif palette==2 then love.graphics.setColor(cs2agent1)
				elseif palette==3 then love.graphics.setColor(cs3agent1)
				elseif palette==4 then love.graphics.setColor(cs4agent1)
				elseif palette==5 then love.graphics.setColor(cs5agent1)
				elseif palette==6 then love.graphics.setColor(cs6agent2)
				elseif palette==7 then 
				end
end

--he second function pb:drawMouse(block) defines three local variables mx, my, and tileSize. The function then calculates the x and y coordinates of the block that the mouse is currently hovering over based on the position of the mouse cursor and the size of the grid blocks.
function pb:drawMouse (block)

	local mx, my = love.mouse.getPosition()
	local tileSize = self.gridSize 
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1

end


