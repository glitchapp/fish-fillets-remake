-- License CC0 (Creative Commons license) (c) darkfrei, 2022
-- push-blocks

-- Description / resume of the functionality of each function written with assistance from ChatGPT
-- Most comments and resumes of this code has been created with chatGPT and although they are quite accurate they may contain errors.

--[[
 This code defines a function that sets the outline of a push-block in a Sokoban-like game.
 The function takes in a "block" table containing a list of tiles, each representing a grid position occupied by the block.
 The function creates a map of outlines for the tiles and generates a list of lines based on the presence or absence of tiles adjacent to each other.
  Finally, the function sets the "lines" property of the block to this list of lines. The code also defines several file paths for different types of push-block sprites.
--]]
local path = 'assets/players/'							-- remake fish
local pathcl = 'assets/players/classic/'				-- classic fish
local pathclup = 'assets/players/classic/upscaled/'		-- classic fish upscaled


local pb = {}
local function setBlockOutline (block)
	local d = 1/20
	local m = {} -- map of outlines
	local tiles = block.tiles -- list of tiles as {x1,y1, x2,y2, x3,y3 ...}
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if not m[y] then 
			m[y] = {} 
		end
		if not m[y][x] then 
			m[y][x] = true
		end
	end
	local lines = {}
	for y, xs in pairs (m) do
		for x, tabl in pairs (xs) do
			local h1 = m[y-1] and m[y-1][x]
			local v1 = m[y]   and m[y][x-1]
			local h2 = m[y+1] and m[y+1][x]
			local v2 = m[y]   and m[y][x+1]
			
			if not h1 then
				table.insert (lines, {x+d,y+d, x+1-d, y+d})
			end
			if not h2 then
				table.insert (lines, {x+d,y+1-d, x+1-d, y+1-d})
			end			
			if not v1 then
				table.insert (lines, {x+d,y+d, x+d, y+1-d})
			end
			if not v2 then
				table.insert (lines, {x+1-d,y+d, x+1-d, y+1-d})
			end

		end
	end
	block.lines = lines
end

--[[This code defines a function called "remapTiles" which takes a "block" object as input. 
The purpose of this function is to remap the coordinates of the tiles in the block so that the top-left tile has coordinates (0,0).
 It does this by calculating the minimum x and y values of the tiles, subtracting these values from the block's x and y coordinates,
 and then updating the tile coordinates accordingly.
 Finally, the function sets the block's width and height based on the maximum x and y values of the tiles, respectively.--]]
local function remapTiles (block)
	local x0, y0 = block.x, block.y
	local dx, dy = 0, 0
	local tiles = block.tiles
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if x < dx then dx = x end
		if y < dy then dy = y end
	end
	block.x, block.y = block.x - dx, block.y - dy
	local w, h = 0, 0
	for i = 1, #tiles, 2 do
		local x, y = tiles[i]+dx, tiles[i+1]+dy
		tiles[i] = x
		tiles[i+1] = y
		if x > w then w = x end
		if y > h then h = y end
	end
	block.w = w+1
	block.h = h+1
end

--This code loads a game level in the push-blocks game. It sets the game map, grid size, block sizes and outlines, and areas. It also remaps the tiles, sets the block outline, and loads the agents for the level.
function pb:load (level)
	local width, height = love.graphics.getDimensions()
	self.map = level.map
	self.gridWidth = level.w
	self.gridHeight = level.h
	--self.gridSize = math.floor(math.min(width/(level.w), height/(level.h)))
	self.gridSize = (math.min(width/(level.w), height/(level.h)))
	tileSize = self.gridSize
	gridSize=self.gridSize				-- turns self.gridSize into a global variables needed to reset zoom values on game/mainfunctions/mouseinput.lua
	self.blocks = level.blocks
	self.areas = level.areas
	for i, block in ipairs (self.blocks) do
		remapTiles (block)
		setBlockOutline (block)
	end
	pb:agentsload(level)
end

--[[This code loads agents for the game, where an agent can have an image associated with it. 
The function loads the agents from the level data, sets the block outline, and loads the image file for each agent,
scaling it according to the grid size. If the image file is not found, then the agent is loaded without an image.
Finally, the function sets the active agent to the first agent in the list.--]]

function pb:agentsload(level)
	
	self.agents = level.agents
	for i, agent in ipairs (self.agents) do
		setBlockOutline (agent)
		if skin=="remake" or skin=="retro" then
			filename = path..agent.name..'.webp'
		elseif skin=="classic" then
				if skinupscaled==false then
					filename = pathcl..agent.name..'.webp'
			elseif skinupscaled==true then
					filename = pathclup..agent.name..'.webp'
			end
		end
		local info = love.filesystem.getInfo(filename)
		if info then
			local image = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData(filename)))
			agent.image = image
			local width, height = image:getDimensions( )
			scale = self.gridSize/(width/agent.w)
			agent.image_scale = scale
			agent.image_sx = scale
			agent.image_sy = scale
			agent.image_ox = image:getWidth()/2
			agent.image_oy = image:getHeight()/2
			agent.image_dx = agent.image_ox*agent.image_scale
			agent.image_dy = agent.image_oy*agent.image_scale
		end
	end
	self.activeAgentIndex = 1
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
end

--[[The code defines a function pb:switchAgent() which switches the active agent in the game. The function first sets the active flag of the current agent to false, then increments the active agent index, and if it is greater than the number of agents, sets the index to 1. The function then sets the new agent as the active agent and sets its active flag to true.

Additionally, the code includes a local function isValueInList which checks if a given value is present in a list, and returns true if it is, and false otherwise.--]]

function pb:switchAgent ()
	self.agent.active = false
	local index = self.activeAgentIndex + 1
	if index > #self.agents then
		index = 1
	end
	self.activeAgentIndex = index
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
	
	
	-- trigger switch aura
	if not (fish1status=="dead") and not (fish2status=="dead") then
				--if self.activeAgentIndex==1 and not (fish1status=="talking") and not (fish2status=="talking") then
				if self.activeAgentIndex==1 then
					activefish=2 fish1status="idle" 	fish2status="switch"  	smallfishframe=0
			--elseif self.activeAgentIndex==2 and not (fish1status=="talking") and not (fish2status=="talking") then
			elseif self.activeAgentIndex==2 then
					activefish=1 fish1status="switch" 	fish2status="idle" 		bigfishframe=0
				end

		
				if self.activeAgentIndex==1 then fish1highlight=true  fish2highlight=false	-- high light fish when switch
			elseif  self.activeAgentIndex==2 then fish2highlight=true fish1highlight=false
			end
			timer2=0
	end
end

function pb:selectFish1()
	fish1highlight=true  fish2highlight=false
	timer2=0
	self.agent = self.agents[1]
end

function pb:selectFish2()
	fish2highlight=true  fish1highlight=false
	timer2=0
	self.agent = self.agents[2]
end

--[[This code defines a local function called isValueInList that takes two parameters: value and list. It checks if the value is present in the list by iterating through the list using ipairs and comparing each element to the value. If the element is found to be equal to value, the function immediately returns true. If the function completes the loop without finding the value, it returns false.--]]
local function isValueInList (value, list)
	for i, element in ipairs (list) do
		if element == value then 
			return true 
		end
	end
	return false
end

--[[The pb:isBlockToMapCollision function is used to determine if there is a collision between a block and the game map when the block is moved by a certain amount in the x and y directions (dx and dy). The function takes a block parameter, which is an object that represents the block, and extracts its x and y coordinates and a list of tiles that make up the block (block.tiles). The function then checks if any of the tiles in the block overlap with a tile in the game map after the block is moved by dx and dy. If there is a collision, the function returns true, otherwise it returns nil.--]]
function pb:isBlockToMapCollision(block, dx, dy)
    local x, y = block.x, block.y
    local map = self.map
    for i = 1, #block.tiles - 1, 2 do
        local mapX = x + block.tiles[i] + dx
        local mapY = y + block.tiles[i + 1] + dy
        if map[mapY] and map[mapY][mapX] and map[mapY][mapX] ~= 6 then	-- do not return true for value 6 which are exit areas
            return true -- Collision with a non-exit area
        end
    end
end


--This function checks if there is a rough collision between two blocks, where rough collision means that the function is only interested in knowing if the two blocks overlap in any way, but it does not return information about the specific area of overlap. The function receives two blocks as input, blockA and blockB, and also the values dx and dy that indicate a displacement of blockA in the x and y directions, respectively. The function calculates the positions and dimensions of the two blocks based on these values, and then checks if the two blocks overlap in any way. If there is an overlap, the function returns true, otherwise, it returns false.
function pb:isBlockToBlockCollisionRough (blockA, blockB, dx, dy)
	local x1, y1 = blockA.x + dx, blockA.y + dy
	local h1, w1 = blockA.h, blockA.w
	local x2, y2 = blockB.x, blockB.y
	local h2, w2 = blockB.h, blockB.w
	if x1 < x2+w2 and x2 < x1+w1 and
		y1 < y2+h2 and y2 < y1+h1 then
		return true	
	end
	return false
end

--[[This code defines a function pb:isBlockToBlockCollisionFine which detects fine-grained collisions between two blocks in a game. It takes in two blocks (blockA and blockB) and the change in x and y direction (dx and dy) of blockA, and returns true if blockA collides with blockB after moving by dx and dy.

The function checks for collisions between each tile in blockA and each tile in blockB by comparing their x and y positions. If any two tiles have the same x and y position, the function returns true to indicate a collision. If no collisions are found, the function returns false.
--]]
function pb:isBlockToBlockCollisionFine (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end

--[[This is a function in a game logic that checks if two blocks collide after one of them moves by a specified amount of displacement (dx, dy). The function first checks for a rough collision using the isBlockToBlockCollisionRough function. If there is no rough collision, it returns false to indicate that there is no collision. If there is a rough collision, the function checks for fine-grained tile-to-tile collision using the isBlockToBlockCollisionFine function. If there is a collision, the function returns true, otherwise it returns false.--]]
function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- check if blockA moves to dx, dy an collides with blockB
	-- rough
	if not self:isBlockToBlockCollisionRough (blockA, blockB, dx, dy) then
		return false
	elseif self:isBlockToBlockCollisionFine (blockA, blockB, dx, dy) then
	-- fine
		return true
	end
	return false
end


--get collision with exit areas from the grid.
function pb:getCollisionExit()
    for i, agent in ipairs(self.agents) do
        -- Get the coordinates of the cell where the agent is located.
        local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

        -- Check if the cell has a value of 6 in the grid map.
        if self.map[agentY] and self.map[agentY][agentX] == 6 then
            return true -- Agent collision with exit
        end
    end
    return false -- No collision with exit area
end

--[[The code defines a function getCollisionBlocks that performs collision detection between a given block and all other blocks and agents in the game.
The function first checks if the block collides with the game map and returns false if it does.
It then checks for collision with all other agents, and if a collision is detected, returns false.
If the block collides with any other block, the function recursively checks if the other block can be moved, and if not, returns false.
If all collisions are successfully detected, the function returns true.
--]]
function pb:getCollisionBlocks (blockA, blocks, dx, dy)
	-- agent to map or block to map collision
	-- deep function
	-- recursive detects all blocks to move, pushed by blockA;
	-- returns nothing if the move is not possible

	if self:isBlockToMapCollision (blockA, dx, dy) then
		return false
	end
	for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return false -- cannot move any agent
		end
	end
	for i, block in ipairs (self.blocks) do
		if block == blockA then
			-- self collision: do nothing
		elseif isValueInList (block, blocks) then
			-- block is already in list: do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			-- checks if the agent is strong
			if block.heavy and not self.agent.heavy then
				--script that trigger dialogs when small fish try to push the pipe in the first level
				eventcollisionblocks(block)				-- trigger evetns when an object collides
				return false
			end
			table.insert (blocks, block)
			-- make it deeper!
			if not self:getCollisionBlocks (block, blocks, dx, dy) then
				-- if one of deep blocks cannot be moved
				return false
			end
		end
	end
	return true
end

--This function returns a list of blocks that can be moved with the agent and also a boolean value indicating whether the movement is possible or not. It does this by calling the getCollisionBlocks function and passing in the agent and a list to store the blocks. It then returns the list of blocks and the boolean value.
function pb:getBlocksToMove (agent, dx, dy)
	local blocks = {}
	local canMove = self:getCollisionBlocks (agent, blocks, dx, dy)	
	return blocks, canMove
		
end

--This function moves a given block by updating its x and y coordinates by the specified dx and dy values.
function pb:moveBlock (block, dx, dy)
	block.x = block.x + dx
	block.y = block.y + dy
end

-- This function moves a list of blocks by calling the moveBlock function for each block in the list. It also triggers an event to indicate that the object has been pushed.
function pb:moveBlocks(blocks, dx, dy)
    for i, block in ipairs(blocks) do
        self:moveBlock(block, dx, dy)
        eventobbjectpushed(block) -- Triggers events when objects are pushed
		
		--debug
		--[[print (block.name)
        if nLevel == 64 then
            if block.name == "gral18" then
                local isTheHolyGrailInExitArea = pb:isObjectInExitArea("gral18", blocks)
                print("Checking if gral is in exit area:", isTheHolyGrailInExitArea)
                print ("x: " .. block.x)
                print (" y: " .. block.y)
                if isTheHolyGrailInExitArea then
                    print("The holy Grail is in the exit area, level completed!")
                    levelComplete()
                end
            end
        end
        --]]
    end
end

--This function moves a list of lists of blocks (metablocks) by calling the moveBlocks function for each list of blocks in the list of metablocks.
function pb:moveMetablocks (metablocks, dx, dy)
	for i, blocks in ipairs (metablocks) do
		self:moveBlocks (blocks, dx, dy)
	end
end

-- This function checks whether a given block collides with any of the other blocks on the map by calling the isBlockToBlockCollision function for each block in the list of blocks. If a collision is detected, it returns true. Otherwise, it returns false.
function pb:isCollisionBlockToAllBlocks (blockA, dx, dy)
	for i, block in ipairs (self.blocks) do
		if (block == blockA) then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

--This function checks whether a given block collides with any of the other blocks on the map, except for those in the list of exception blocks. It does this by calling the isBlockToBlockCollision function for each block in the list of blocks, and checking if the block is not in the list of exception blocks. If a collision is detected, it returns true. Otherwise, it returns false.
function pb:isCollisionBlockToAllBlocksExcept (blockA, dx, dy, exceptionBlocks)
	for i, block in ipairs (self.blocks) do
		if (block == blockA) then
			-- do nothing
		elseif isValueInList (block, exceptionBlocks) then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

-- This function takes a block blockA and two integers dx and dy as inputs. It then checks for a collision between blockA and all agents in the game, assuming blockA moves by dx and dy. If a collision is found, it returns the dead agent. Otherwise, it returns false.
function pb:isCollisionBlockToAllAgents (blockA, dx, dy)
   -- (if blockA moves by dx dy)
    for i, agent in ipairs(self.agents) do
        if self:isBlockToBlockCollision(blockA, agent, dx, dy) then
            return agent -- dead agent :(
        end
    end
    return false -- No collision with agents
end

-- Function to check if a specific object is in an exit area based on its name.
function pb:isObjectInExitArea(objectName, blocks)
    -- Find the object by its name
    for i, block in ipairs(self.blocks) do
        if block.name == objectName then
            local gridX = block.x
            local gridY = block.y

            -- Debug statements
            --[[print("ObjectName:", objectName)
            print("Block X:", block.x)
            print("Block Y:", block.y)
            print("Grid Size:", self.gridSize)
            print("GridX:", gridX)
            print("GridY:", gridY)
			--]]
            -- Check if the grid cell contains an exit area (value 6)
            if self.map[gridY] and self.map[gridY][gridX] == 6 then
                return true -- Object is in an exit area
            end
        end
    end

    return false -- Object is not in an exit area or not found
end





function pb:isCollisionAgentToAnyExitArea(agent)
    -- Get the coordinates of the cell where the agent is located.
    --local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)
    

    -- Debugging: Print agent coordinates and grid value.
    --print("Agent Coordinates (X, Y):", agent.x, agent.y)
    
    if self.map[agent.y] and self.map[agent.y][agent.x] == 6 then
        --print("Agent is in an exit area with value 6")
        return true -- Agent is in an exit area with value 6
    else
        --print("Agent is NOT in an exit area")
        return false
    end
        
end


--check if all agents are in exit areas from the grid.
function pb:areAllAgentsInExitAreas()
    for i, agent in ipairs(self.agents) do
        -- Get the coordinates of the cell where the agent is located.
        --local agentX, agentY = math.floor(agent.x / self.gridSize), math.floor(agent.y / self.gridSize)

        -- Check if the cell has a value of 6 in the grid map.
        if not (self.map[agent.y] and self.map[agent.y][agent.x] == 6) then
            return false -- At least one agent is not in an exit area with value 6
        end
    end
    return true
end

-- This function  takes an agent agent and an area area as inputs, and checks if the agent is currently in the area. It returns true if the agent is in the area, and false otherwise.
function pb:isAgentInArea (agent, area)
	return self:isBlockToBlockCollisionRough (agent, area, 0, 0) -- returns true if AABB 
	-- see https://love2d.org/wiki/BoundingBox.lua
end

-- This function  is a local function that takes a block block and a list of metablocks metablocks as inputs. It checks if the block is in any of the metablocks and returns true if it is, false otherwise.
local function isBlockInMetablocks (block, metablocks)
--	metablock is list of blocks
	for i, metablock in ipairs (metablocks) do
		if isValueInList (block, metablock) then
			return true
		end
	end
end

-- This function akes a block and its displacement dx and dy and returns a list of blocks that collide with the given block after the displacement. It loops over all blocks in the game and checks if they collide with the given block, and if they do, it adds them to the list of colliding blocks.
function pb:getCollisionBlocksFlat (blockA, dx, dy)
	local blocks = {}
	for i, blockB in ipairs (self.blocks) do 
		if isValueInList (blockB, blocks) then
			-- do nothing
		elseif blockA == blockB then
			-- do nothing
		elseif self:isBlockToBlockCollision (blockA, blockB, dx, dy) then
			table.insert (blocks, blockB)
			
		end
	end
	return blocks
end

-- This function  takes a block and a list of blocks called metablock and returns a list of blocks that form a connected "metablock" with the given block. It recursively checks the block's neighbors and their neighbors to see if they are connected and adds them to the metablock list if they are. If any of the blocks in the metablock list cannot be connected, the function returns false.
function pb:metablockCreation (blockA, metablock)
	-- falling
	local dx, dy = 0, 1
	if self:isBlockToMapCollision (blockA, dx, dy) then 
		-- not falling :)
		--blockA.text = "m1"			-- text disabled
		return false 
	end
	if self:isCollisionBlockToAllAgents (blockA, dx, dy) then 
		-- not falling, on the fish
		--blockA.text = "f1"			-- text disabled
		return false 
	end
	-- neighbour collision blocks
	local blocks = pb:getCollisionBlocksFlat (blockA, dx, dy)
		
	for i, blockB in ipairs (blocks) do
		if isValueInList (blockB, metablock) then
--			-- do nothing
		else
			table.insert (metablock, blockB)
			if not self:metablockCreation (blockB, metablock) then
--				blockA.text = "d1"
				return false
			end
		end
	end
	return metablock
end

-- This function takes a block and returns a new list of blocks that form a falling "metablock" with the given block. It calls pb:metablockCreation to create the metablock list and returns it only if the block is falling, i.e., it is not colliding with the map or any agents.
function pb:newMetablock (block)
	local metablock = {block}
	-- returns falling metablocks only
	if self:metablockCreation (block, metablock) then
		return metablock
	end
end

--[[
The function pb:fallBlocks() implements the falling mechanism for blocks in the game.
It first creates a list of metablocks by calling the self:newMetablock() method for each block that is not already in a metablock.
The function then iterates over the metablocks in reverse order, checking if the metablock can continue to fall by testing for collisions with the game map, other blocks, and agents.
If the metablock can continue to fall, it is moved downwards by calling the self:moveBlocks() method.
If the metablock cannot fall anymore, it is removed from the list of metablocks.
The function also triggers events when an object falls by calling eventsblockfalling().
--]]

function pb:fallBlocks ()
-- no horizontal speed, but positive (down) vertical speed
	local dx, dy = 0, 1
	local metablocks = {}
	for i, block in ipairs (self.blocks) do
		block.text = nil
	end
	for i, block in ipairs (self.blocks) do
		if not isBlockInMetablocks (block, metablocks) then
			-- new metablock
			local metablock = self:newMetablock (block)
			if metablock then
				table.insert(metablocks, metablock)
			end
		end
	end
--	print ('#metablocks', #metablocks)
	
	for i = 1, self.gridHeight do
		local somethingMoved = false
		for iMetablock = #metablocks, 1, -1 do -- backwards
			local metablock = metablocks[iMetablock]
--			print ('metablock', iMetablock, 'blocks:', #metablock)
			local metablockFalling = true
			
			for j, block in ipairs (metablock) do
				if self:isBlockToMapCollision (block, dx, dy) then
					-- not falling
					--block.text = "map"				-- text disabled
					metablockFalling = false
				elseif self:isCollisionBlockToAllBlocksExcept (block, dx, dy, metablock) then
					-- collision to block: not falling
					--block.text = "fest"				-- text disabled
					metablockFalling = false
				elseif self:isCollisionBlockToAllBlocks (block, dx, dy) then
					-- collision to metablock
					--block.text = "fallM"			-- text disabled
				elseif self:isCollisionBlockToAllAgents (block, dx, dy) then
					local deadAgent = self:isCollisionBlockToAllAgents (block, dx, dy)
					--block.text = 'on dead'			-- text disabled
					deadAgent.dead = true
					metablockFalling = false
				else
					--block.text = "fallS"			-- text disabled
--					block moved only if metablock moved
					
					eventsblockfalling(block)			--trigger events when an object falls
				end
			end
			if metablockFalling then
				self:moveBlocks (metablock, dx, dy)
				somethingMoved = true
			else
				-- the metablock is not falling anymore
				table.remove(metablocks, iMetablock)
			end
		end
		if not somethingMoved then 
			-- nothing to move
			return 
		end
	end
end


-- Function to smoothly move the agent with linear interpolation
function pb:smoothMoveAgent(agent, targetX, targetY, duration)
	
    local startX, startY = agent.x, agent.y
    local t = 0

  if duration <= 0 then
        return
    end

    while t < 1 do
        t = t + love.timer.getDelta() / duration  -- Adjust speed using love.timer.getDelta()

        agent.x = lerp(startX, targetX, t)
        agent.y = lerp(startY, targetY, t)

        coroutine.yield()  -- Yield control to the main game loop

           -- Debug statements
        --print("Smooth Move - t:", t, "Agent Position:", agent.x, agent.y)
        --print("Smooth Move - Target Position:", targetX, targetY)
        --print("Smooth Move - Start Position:", startX, startY)
        love.graphics.print("Smooth Move - t: " .. t, 10, 10)
        love.graphics.print("Agent Position: " .. agent.x .. ", " .. agent.y, 10, 30)
        love.graphics.print("Target Position: " .. targetX .. ", " .. targetY, 10, 50)
        love.graphics.print("Start Position: " .. startX .. ", " .. startY, 10, 70)
    end

    -- Set moveCoroutine to nil when the movement is completed
    moveCoroutine = nil
end

-- Linear interpolation function
function lerp(a, b, t)
    return a + (b - a) * t
end

-- Your existing pb:mainMoving function
function pb:mainMoving(dx, dy)
    -- Your existing logic for mainMoving
	local x, y = self.agent.x, self.agent.y
			--local lightPos1 = {0.1 + math.cos(sunAngle)/3, 0.1 + math.sin(sunAngle), 0.075 + math.sin(sunAngle)}
			--if agent.name=="fish-4x2" then  lightPos1 = {1.1 - y/25, 0.6 + x/100, 0.075 + math.sin(sunAngle)} causticsAndNormals:send("LightPos1", lightPos1)
		--elseif agent.name=="fish-3x1" then  lightPos2 = {1.1 - y/25, 0.4 + x/100, 0.075 + math.sin(sunAngle)} causticsAndNormals:send("LightPos2", lightPos2)
		--end
   
end


--[[
This function handles the main movement of the active agent in the game. It takes in a delta x and y and a delta time.

The function starts by setting the direction of the agent depending on the delta x value. It then gets the blocks that need to move and checks if the agent can move to the desired position. If the agent can move, the agent and the blocks are moved and an event is triggered. The function then checks for falling blocks and if all agents are in the exit area. If all agents are in the exit area, the level is completed.

The function also handles animations for the agent such as moving and pushing, and checks the agent's direction to determine the appropriate animation. Lastly, an event is triggered for the agent's position.
--]]

local moveCoroutine = coroutine.create(function () end) -- Initialize moveCoroutine with a dummy coroutine
shouldPerformSmoothMove=true

function pb:mainMoving (dx, dy,dt)		-- same function with interpolation
	
	  -- Debug statements for input
    --print("dx:", dx, "dy:", dy)
            -- Clear the debug text
            debugText = {}
            
	
	local agent = self.agent -- active agent
	
	-- if in alternative gamepad layout mode
		if not (radialmenu) and not (zoomtriggered==true) and (controllerLayout=="alternative") then
				--if the second axis is moving the move the second fish
				if ((rightxaxis<-0.4) or (rightxaxis>0.4) or (rightyaxis<-0.2) or (rightyaxis>0.4)) then 
					self.agent = self.agents[1]
				--if the first axis is moving the move the first fish
			elseif ((leftxaxis<-0.4) or (leftxaxis>0.4) or (leftyaxis<-0.2) or (leftyaxis>0.4)) then
				self.agent = self.agents[2]
			end
		end
		
		local x, y = self.agent.x, self.agent.y
			--local lightPos1 = {0.1 + math.cos(sunAngle)/3, 0.1 + math.sin(sunAngle), 0.075 + math.sin(sunAngle)}
			if agent.name=="fish-4x2" then
			  lightPos1 = { 0.1 + x/75,0.0 + y/25, 0.075 }
			  --lightColor1 = { 1.0, 0.0 , 0.0, 0.7 }
			  causticsAndNormals:send("LightPos1", lightPos1)
			  --causticsAndNormals:send("LightColor1", lightColor1)
			
		elseif agent.name=="fish-3x1" then  
		lightPos2 = { 0.1 + x/75,0.0 + y/25, 0.075 }
		--lightColor2 = { 0.0, 0.0 , 1.0, 0.7 }
		causticsAndNormals:send("LightPos2", lightPos2)
		--causticsAndNormals:send("LightColor2", lightColor2)
		end
	
	if dx==nil then dx=0 end	-- prevent game from crashing due variable not initialized
	if dy==nil then dy=0 end
	
	if dx > 0 then
        agent.direction = "right"
    elseif dx < 0 then
        agent.direction = "left"
    end
	
	local blocks, canMove = self:getBlocksToMove (agent, dx, dy)
	 if canMove then
   
	--[[
  -- Make sure you're calling pb:smoothMoveAgent
    if shouldPerformSmoothMove then
        local targetX, targetY = agent.targetX, agent.targetY
        local duration = 2.0
		--pb:smoothMoveAgent(agent, targetX, targetY, duration)
		-- Debug statements for debugging coroutine initialization
		
		 table.insert(debugText, "Initializing coroutine")
         table.insert(debugText, "moveCoroutine status: " .. coroutine.status(moveCoroutine))

    
    if not moveCoroutine or coroutine.status(moveCoroutine) == "dead" then
          table.insert(debugText, "Before creating the coroutine - self.agent: " .. tostring(self.agent))
        -- Start the movement coroutine and store it in moveCoroutine
        moveCoroutine = coroutine.create(pb.smoothMoveAgent)
        
         table.insert(debugText, "Before resuming the coroutine - self.agent: " .. tostring(self.agent))
        local success, message = coroutine.resume(moveCoroutine, self.agent, targetX, targetY, duration)

        -- Debug statements after coroutine initialization
        table.insert(debugText, "Coroutine initialized")
        table.insert(debugText, "After resuming the coroutine - self.agent: " .. tostring(self.agent))
        table.insert(debugText, "moveCoroutine status after initialization: " .. coroutine.status(moveCoroutine))


        if not success then
            print("Error:", message)
        end
    end
    
  -- Debug statement for coroutine status before resuming
   table.insert(debugText, "Before resuming moveCoroutine status: " .. coroutine.status(moveCoroutine))


    -- Wait for the coroutine to finish (smooth movement)
    if coroutine.status(moveCoroutine) == "suspended" then
        coroutine.resume(moveCoroutine) -- Resume the coroutine

        -- Debug statement after resuming
        table.insert(debugText, "After resuming moveCoroutine status: " .. coroutine.status(moveCoroutine))
    end

    
end
--]]


     -- Debug statements for agent's position and direction
    --print("Agent Position:", agent.x, agent.y)
    --print("Agent Direction:", agent.direction)
    
		self:moveBlock (agent, dx, dy) -- agent is block too
		self:moveBlocks (blocks, dx, dy)
		eventobbjectpushed(blocks)			--trigers events when objects are pushed

		local fallingBlocks = self:fallBlocks ( ) 
		
		local allFishOnExit=self:areAllAgentsInExitAreas () -- true if both are in area
		if allFishOnExit then
			-- on some levels some task need to be fulfilled in order to complete the level,
			-- we make sure we are not on such levels before checking if agents are in exit areas
				if not (nLevel==19) and not (nLevel==29) and not (nLevel==37) and not (nLevel==44) and not (nLevel==51) and not (nLevel==58) and not (nLevel==70) and not (nLevel==77) then
					levelComplete () 
			elseif nLevel==19 or nLevel==29 or nLevel==37 or nLevel==44 or nLevel==51 or nLevel==58 or nLevel==70 or nLevel==77 then	-- do nothing, the level is completed if a god is in the exit area (see condition below)
				eventAgentInExitButObjectNot()	-- this function plays a random border sentence when the fish tries to exit the level without completing a task
			end
		end
		
		if nLevel==19 then	-- on level 19 Poseidon or neptun needs to be in the exit area in order to complete the level
		--print(isNeptunInExitArea)
			local isNeptunInExitArea = pb:isObjectInExitArea(neptun)
			local isPoseidonInExitArea = pb:isObjectInExitArea(poseidon)
			-- Check each God
			for i = 1, 2 do
				if pb:isObjectInExitArea("neptun", level.blocks) then
					print("Neptun is in the exit area, level completed")
					isNeptunInExitArea=true
					levelComplete ()
				end
				if pb:isObjectInExitArea("poseidon", level.blocks) then
					--print("Poseidon is in the exit area, level completed")
					isPoseidonInExitArea=true
					levelComplete ()
				end
					
			end
		
					if pb:isCollisionAgentToAnyExitArea(agent) then
						eventAgentInExitButObjectNot()
					end
					
				
			
		elseif nLevel==29 then
			local isThePlugInExitArea = pb:isObjectInExitArea(plug)
			if isThePlugInExitArea then
				print(objectName .. " The plug is in the hole, well done!")
				levelComplete ()
			elseif pb:isCollisionAgentToAnyExitArea(agent) then
						eventAgentInExitButObjectNot()
			end
		elseif nLevel==37 then
			local isTelepaticTurtleInExitArea = pb:isObjectInExitArea(turtle)
				if isTelepaticTurtleInExitArea then
					print(objectName .. " The telepatic turtle is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==44 then
			local isRadioactiveWasteInExitArea = pb:isObjectInExitArea(barel)
				if isRadioactiveWaste then
					print(objectName .. " The radioactive waste is in exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==51 then
			local isTheMapInExitArea = pb:isObjectInExitArea(mapab)
				if isTheMapInExitArea then
					print(objectName .. " The map is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==58 then
			local isTheSquirrelExitArea = pb:isObjectInExitArea(pohon)
				if isTheSquirrelExitArea then
					print(objectName .. " The squirrel is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel == 64 then
			totalHolyGrails = 25  -- Total number of Holy Grails
			holyGrailsInExitArea = 0  -- Initialize count to zero

			-- Check each Holy Grail
			for i = 1, totalHolyGrails do
				local grailName = "gral" .. i
				if pb:isObjectInExitArea(grailName, level.blocks) then
					holyGrailsInExitArea = holyGrailsInExitArea + 1  -- Increment count if in exit area
				end
			end

			--print("Number of Holy Grails in the exit area:", holyGrailsInExitArea)

			if holyGrailsInExitArea == totalHolyGrails then
				print("All Holy Grails are in the exit area, level completed!")
				levelComplete()
			elseif pb:isCollisionAgentToAnyExitArea(agent) then
				eventAgentInExitButObjectNot()
			end
		

		elseif nLevel==70 then
			local isTheFloppyDiskInExitArea = pb:isObjectInExitArea(floppy)
				if isTheFloppyDiskInExitArea then
					print(objectName .. " The floppy disk is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		elseif nLevel==77 then
			local isTheLinuxUserInExitArea = pb:isObjectInExitArea(linuxak1)
			local isTheLinuxUser2InExitArea = pb:isObjectInExitArea(linuxak2)
				if isTheLinuxUserInExitArea or isTheLinuxUser2InExitArea then
					print(objectName .. " The linux user is in the exit area, level completed!")
					levelComplete ()
				elseif pb:isCollisionAgentToAnyExitArea(agent) then
					eventAgentInExitButObjectNot()
				end
		end

				-- moving animation
					if agent.name=="fish-4x2" and not (dx==0) and not (dy==1) then fish1status="moving"
				elseif agent.name=="fish-3x1" and not (dx==0) and not (dy==1) then fish2status="moving"
				end

	else			--pushing animation
					if agent.name=="fish-4x2" then fish1status="pushing"
				elseif agent.name=="fish-3x1" then fish2status="pushing"
				end
	end
	
	
	

		
				lastagentdirection=agent.direction
		
	eventagentposition(block,agent)				-- trigger events when on a specific position
end

--[[
The function pb:keypressedMoving(scancode) is triggered when a key is pressed on the keyboard. It takes in the scancode of the key pressed as its parameter. The function checks if neither fish1status nor fish2status are "dead".

If the pressed key is one of 'w', 'a', 's', or 'd', the function sets the dx and dy values accordingly based on the scancode and then calls the pb:mainMoving function passing in these values as parameters.

If the pressed key is one of 'right', 'left', 'up', or 'down', the function also sets dx and dy accordingly based on the scancode and then calls the pb:mainMoving function passing in these values as parameters.

If nLevel is 37 and turtlestatus is "telepatic", then the function randomly sets the dx and dy values within the range of -1 to 1 and calls the pb:mainMoving function passing in these values as parameters.

Finally, if the variable boresentencestriggered is true and boresentencesinterrupted is false, the function sets boresentencesinterrupted to true. The variable inputtime is set to 0.
--]]




function pb:keypressedMoving (scancode)
if not (fish1status=="dead") and not (fish2status=="dead") and gamestatus=="game" and not (pauseisactive==true) then
	if scancode == 'w' or scancode == 'a' or scancode == 's' or scancode == 'd' then
		
		if gamestatus=="game" then steps=steps+1 end	--add steps to the movement
		
		-- d means 1; a means -1; otherwise 0
		local dx = scancode == 'd' and 1 or scancode == 'a' and -1 or 0
		-- s means 1; w means -1; otherwise 0
		local dy = scancode == 's' and 1 or scancode == 'w' and -1 or 0
		pb:mainMoving (dx, dy)
		
		if boresentencestriggered==true and boresentencesinterrupted==false then boresentencesinterrupted=true end	-- end bore script
		inputtime=0
	end
	if scancode == 'right' or scancode == 'left' or scancode == 'up' or scancode == 'down' then
		
		if gamestatus=="game" and not (pauseisactive==true) then steps=steps+1 end	--add steps to the movement
		
		local dx = scancode == 'right' and 1 or scancode == 'left' and -1 or 0
		local dy = scancode == 'down' and 1 or scancode == 'up' and -1 or 0
		pb:mainMoving (dx, dy)
		
		if boresentencestriggered==true and boresentencesinterrupted==false then boresentencesinterrupted=true end	-- end bore script
		inputtime=0
	end

		if nLevel==37 and turtlestatus=="telepatic" then
			dx=math.random(-1,1) dy=math.random(-1,1) pb:mainMoving (dx, dy)
		 end -- random moves telepatic turtle movement

end
end


---------------------------------------------------------------------------------------------------
-- draw
---------------------------------------------------------------------------------------------------
--pb:drawBackgroundGrid(): Draws a background grid with a given grid size, width, and height. It uses Love2D graphics functions to draw the grid lines.
function pb:drawBackgroundGrid ()
	local gridSize = self.gridSize
	local gridWidth = self.gridWidth
	local gridHeight = self.gridHeight
	love.graphics.setLineWidth(1)
	love.graphics.setColor(0.3,0.4,0.4)
	for i = 0, gridWidth do
		love.graphics.line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
	end
	for i = 0, gridHeight do
		love.graphics.line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
	end
end

--pb:drawMap(): Draws the game map, which is a 2D array of boolean and string values representing the game elements (blocks, borders, etc.). It uses Love2D graphics functions to draw the game elements' assets and borders.
function pb:drawMap ()
--print(lightPos2[2])
	local map = self.map
	--local tileSize = self.gridSize
	love.graphics.setLineWidth(2)
if background=="yes" then
	for y, xs in ipairs (map) do
		for x, value in ipairs (xs) do

			-- value 
			-- is boolean: true or false
			-- or string
			if value==true then 
				-- map tile
				-- beware of -1

				drawassetsmap(block,x,y,tileSize)			-- Draw the assets for the blocks of the grid's map
				
	
			elseif value==6 then 
				-- map tile
				if not(palette==1) then
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				elseif palette==1 then
					love.graphics.setColor(1,1,0,0.2)
					love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				end
				--borders
			elseif value=="border" then 
			
			drawbordermap(block,x,y,tileSize) -- Draw the assets for the borders of the grid's map
			
			end
		end
	end
end
	if extradebug==true and debugmode=="yes" then
		love.graphics.setFont(love.graphics.newFont(20))
		love.graphics.setColor(1,1,1)
		love.graphics.print("Tilesize :" .. tileSize ,50,100)
		love.graphics.print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight,50,150)
	end
end

--pb:drawOutline(block): Draws the outline of the game object (block) passed as an argument. The function gets the lines that define the outline, and it uses Love2D graphics functions to draw the lines. It skips drawing the outlines for certain levels if the variable objectscells is set to "yes."
function pb:drawOutline	(block)
	local lines = block.lines
	local tileSize = self.gridSize
	local x, y = block.x-1, block.y-1
	local exceptions = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 , 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 59, 60, 61, 62, 64, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77}

	local exception = isValueInList (nLevel, exceptions) -- bool
	--do not draw Outlines if levels are:
	if objectscells=="yes" and not exception then
		for i, line in ipairs (lines) do
			love.graphics.line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
		end
	end
end

--pb:drawBlock(block), is a function that takes in a single block parameter and draws it on the screen using the Love2D graphics library. It sets the color to a shade of gray and iterates through the block.tiles table to draw each tile using the drawobjectsassets() function. If the block has a text field, it sets the color to white and prints the text to the screen.
function pb:drawBlock (block)

	--love.graphics.setColor(0.5,0.5,0.5)
		love.graphics.setColor(red,green,blue,1.0)
	love.graphics.setShader(causticsAndNormals)
	local x, y = block.x, block.y
	--tileSize = self.gridSize
	for i = 1, #block.tiles-1, 2 do
		local dx, dy = block.tiles[i], block.tiles[i+1]
		-- beware of -1
		
		if shader1type=="bluenoise" then
			-- Set the dithering shader
			love.graphics.setShader(blueNoiseDitherShader)
			-- Pass the intensity value to the shader
			blueNoiseDitherShader:send("intensity", ditherIntensity)
		
			
		drawobjectsassets(pb,block,x,y,dx,dy,tileSize,i,tilesize,dividx,dividy)	-- draw object inside the blue noise shader
		
		-- Reset the shader
		--love.graphics.setShader()
		else	love.graphics.setShader(causticsAndNormals) drawobjectsassets(pb, block,x,y,dx,dy,tileSize,i,tilesize,dividx,dividy)	-- draw object
		end
		
	end
	if block.text then
		love.graphics.setColor(1,1,1)
		love.graphics.print (block.text, (x-1)*tileSize, (y+1)*tileSize)
	end
end

--pb:drawBlocks(), is a function that iterates through all the blocks in self.blocks and calls pb:drawBlock(block) to draw each block on the screen. It also sets the line width and color to draw an outline around the block. Additionally, it calls drawblockcolors() to change the colors of the blocks if the diorama is enabled.
function pb:drawBlocks ()
	love.graphics.setLineWidth(2)
	love.graphics.setColor(1,1,0.5)
	for i, block in ipairs (self.blocks) do
		-- draw filled block
		love.graphics.setLineWidth(1)
		--love.graphics.setColor(1,1,0.5)
		self:drawBlock (block)
		-- outline
		love.graphics.setLineWidth(3)
		if block.heavy then
			love.graphics.setColor(0,1,1)
		else
			love.graphics.setColor(0,1,0)
		end
		self:drawOutline (block)
	end
	
	drawblockcolors()		-- change the colors if the diorama is enabled

	--love.graphics.print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight,50,150)
end

--pb:drawDeadAgent(agent), is a function that takes in a single agent parameter and draws a diagonal line across the agent's position using the Love2D graphics library. It calculates the position and size of the agent based on its x, y, w, and h fields.
function pb:drawDeadAgent (agent)
	local tileSize = self.gridSize 
	local x = (agent.x-1)*tileSize
	local y = (agent.y-1)*tileSize
	local w = agent.w*tileSize
	local h = agent.h*tileSize
	love.graphics.line (x, y, x+w, y+h)
	love.graphics.line (x, y+h, x+w, y)
end

--[[
The drawTexture function takes in two parameters, an agent object and a tileSize value. The function is used to draw a texture or image onto the screen for the specified agent.

The function first retrieves the image to be drawn from a table of images named images, using a string variable name. It then sets the scale of the image in both the x and y directions based on the agent object's image_sx and image_sy values. If the agent object has a direction value of "left", the x scale is negated to flip the image horizontally.

The function then calls another function named drawplayerassets with the agent, tileSize, and sx parameters. This function is responsible for drawing additional assets related to the agent object, such as text or shapes.
--]]
local function drawTexture (agent, tileSize)

	local sx = agent.image_sx
	if agent.direction and agent.direction == "left" then
		sx = -agent.image_sx
	end
	
	drawplayerassets(agent, tileSize,sx)
end

--[[The drawagentrectangle(agent, tileSize) function takes an agent object and a tile size as input parameters. It then draws a rectangle using the love.graphics.rectangle function with the fill color set to the current color and the position, width, and height of the rectangle calculated based on the agent's position and image offset values.--]]
local  function drawagentrectangle(agent, tileSize)
	local sx = agent.image_sx
	love.graphics.rectangle ('fill', (agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)
end

--[[The pb:drawAgents() function is responsible for drawing all the agents on the game grid. It first sets the active agent to be the one that the player is currently controlling. It then loops through all agents and checks whether the agent has an image or not. If it does, the function calls the drawTexture(agent, tileSize) function to draw the agent's image. If not, the function draws a block at the agent's position and prints the agent's position coordinates. The function also sets the outline color based on whether the agent is heavy or not, and calls the drawOutline(agent) function to draw the outline around the agent. If an agent is dead, the function sets the color to black, calls the drawDeadAgent(agent) function to draw a diagonal line across the agent, sets the status of the corresponding fish to "dead," calls the gameover() function, and exits the loop.--]]
function pb:drawAgents ()
	local activeAgent = self.agent
	--local tileSize = self.gridSize
	-- make it local
	local gridSize=self.gridSize
	for i, agent in ipairs (self.agents) do
		if agent.image then
			-- instead of
--			cs2agent1={0.313,0.270,0.607}
--			cs2agent2={0.623,0.305,0.266}
			-- nice colors are
			local cs2agent1={.9,.9,.8}
			local cs2agent2={0.4,0.4,0.3}

			if agent == activeAgent then
				--love.graphics.setColor(cs2agent1)
				changeagentcolor()
				drawTexture (agent, tileSize)
			else
				--love.graphics.setColor(cs2agent2)
				changeagentcolor()
				drawTexture (agent, tileSize)
			end
		else
			if agent == activeAgent then
				love.graphics.setColor(1,1,1)
				self:drawBlock (agent)
				local x, y = agent.x, agent.y
				love.graphics.setColor (0, 0, 0)
				love.graphics.print (agent.x..' '..agent.y, (agent.x-1)*tileSize, (agent.y-1)*tileSize)
			else
				love.graphics.setColor(0.75,0.75,0.5)
				self:drawBlock (agent)
			end
			-- outline
			love.graphics.setLineWidth(3)
			if agent.heavy then
				love.graphics.setColor(0,1,1)
			else
				love.graphics.setColor(0,1,0)
			end
			self:drawOutline	(agent)
		end
		if agent.dead then
			love.graphics.setColor(0,0,0)
			self:drawDeadAgent (agent)
				if agent.name=="fish-4x2" then fish1status="dead"
			elseif agent.name=="fish-3x1" then fish2status="dead"
			end
			gameover()
			--gamestatus="gameover"
		end
	end
	love.graphics.setColor(1,1,1)
end

--The drawArea(area) function takes an area object as an input parameter and checks whether all the fish are on the exit. If they are, it calls the levelComplete() function to trigger the level completion logic.
function drawArea (area)
 -- no fishes inside
 -- one fish inside, 
 -- all fishes inside
	if allFishOnExit then
		self:levelComplete () -- your logic for level completing here
	end
end



-- this function is in exclusively in charge of loading the position of agents and blocks when load game function is triggered
function pb:loadlevel ()
 -- Load level logic here
    
	-- this section loads the position of blocks
	local tempblockname
	
		for i, block in ipairs (self.blocks) do
			--check if blocks are saved and assign the values from the files to the local variable "tempblockname / tempblocknamex tempblocknamey)
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. block.name .. '.txt') then
				tempblockname =  nLevel .. block.name .. '.txt'
				tempblockname = love.filesystem.read( tempblockname ) 
				block.name = tempblockname
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. block.name .. 'x' .. '.txt') then
				tempblocknamex = nLevel .. block.name .. 'x' .. '.txt'
				tempblocknamex = love.filesystem.read( tempblocknamex )
				block.x = tonumber(tempblocknamex)
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. block.name .. 'y' .. '.txt') then
				tempblocknamey = nLevel .. block.name .. 'y' .. '.txt'
				tempblocknamey = love.filesystem.read( tempblocknamey )
				block.y = tonumber(tempblocknamey)
			end
			
		end
	
	
	-- this section loads the position of agents (fish)
	local tempagentname
		for i, agent in ipairs (self.agents) do
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. agent.name .. '.txt') then
				tempagentname =  nLevel .. agent.name .. '.txt'
				tempagentname = love.filesystem.read( tempagentname )
				agent.name = tempagentname
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. agent.name .. 'x' .. '.txt') then
				tempagentnamex = nLevel ..agent.name .. 'x' .. '.txt'
				tempagentnamex = love.filesystem.read( tempagentnamex )
				agent.x = tonumber(tempagentnamex)	
			end
			
			--check if the file exist to prevent the game from crashing due to missing saved data
			if love.filesystem.getInfo(nLevel .. agent.name .. 'y' .. '.txt') then
				tempagentnamey = nLevel ..agent.name .. 'y' .. '.txt'
				tempagentnamey = love.filesystem.read( tempagentnamey )
				agent.y = tonumber(tempagentnamey)
			end

		end
		
		if love.filesystem.getInfo('1chair1.txt') then	-- this checks that game has been saved and then plays the dialogs
				if nLevel==1 then
					pipefalled=true	-- this prevents the dialogs from not playing on level 1 when a game is saved
					Obey.lev1()
				end
		end
end


-- this function is in exclusively in charge of saving the position of agents and blocks when load game function is triggered
function pb:savelevel ()
	-- this section saves the position of blocks
	for i, block in ipairs (self.blocks) do
		 success, message = love.filesystem.write( nLevel .. block.name .. ".txt", block.name)
		 success, message = love.filesystem.write( nLevel .. block.name .. "x" .. ".txt", block.x)
		 success, message = love.filesystem.write( nLevel .. block.name .. "y" .. ".txt", block.y)
	end
	
	-- this section saves the position of agents
	for i, agent in ipairs (self.agents) do
		 success, message = love.filesystem.write( nLevel .. agent.name .. ".txt", agent.name)
		 success, message = love.filesystem.write( nLevel .. agent.name .. "x" .. ".txt", agent.x)
		 success, message = love.filesystem.write( nLevel .. agent.name .. "y" .. ".txt", agent.y)
	end
	
	    -- Save level logic here
    --saveGameState(self) -- Save the current state to the game history
end




--[[
This function pb:drawMouse() is used to update the position of the mouse cursor and store its x and y coordinates. It uses the LÖVE game engine's love.mouse.getPosition() function to get the current position of the mouse cursor on the screen. It then calculates the corresponding grid cell position by dividing the x and y coordinates by the tileSize value and rounding down to the nearest integer using math.floor(). The tileSize variable is passed in as an argument to the function, and is assumed to be the size of each grid cell in pixels.

This function does not actually draw anything on the screen, but rather updates the internal state of the pb object with the current position of the mouse cursor. The drawMouse() function could be called within the love.draw() function to draw a cursor sprite at the current mouse position, or it could be used to detect when the mouse is hovering over a particular grid cell and trigger an event.
--]]
function pb:drawMouse ()
	local mx, my = love.mouse.getPosition()
	local tileSize = self.gridSize 
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1
	--drawMouse(block)
end
return pb

