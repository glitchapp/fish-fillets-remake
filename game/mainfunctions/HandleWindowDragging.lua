-- Define global variables for dragging
local isDragging = false
local offsetX, offsetY = 0, 0

local isHelpDragging = false
local helpOffsetX, helpOffsetY = 0, 0

-- Add a helper function to check if a point is inside a button
function isPointInsideButton(x, y, button)
    return x > button.x and x < button.x + button.width and y > button.y and y < button.y + button.height
end

-- Centralized function to handle window dragging
function handleWindowDragging(windowX, windowY, width, height, isDraggingVar, offsetXVar, offsetYVar)
    if isDraggingVar then
        -- Update the window position based on mouse movement
        if gamestatus=="game" or gamestatus=="levelselection" then
			windowX = love.mouse.getX() - offsetXVar
			windowY = love.mouse.getY() - offsetYVar
		elseif gamestatus=="levelEditor" then
		    levelEditorContainer.x = love.mouse.getX() - offsetXVar
			levelEditorContainer.y = love.mouse.getY() - offsetYVar
		end
    end

    -- Check if the mouse click is within the draggable area
    local clicked = love.mouse.isDown(1)
    if clicked and not isDraggingVar then
        local x, y = love.mouse.getPosition()
        if isPointInsideButton(x, y, { x = windowX, y = windowY, width = width, height = height }) then
            isDraggingVar = true
             if gamestatus=="game" or gamestatus=="levelselection" then
				offsetXVar = x - windowX
				offsetYVar = y - windowY
			elseif gamestatus=="levelEditor" then
				offsetXVar = x - levelEditorContainer.x
				offsetYVar = y - levelEditorContainer.y
			end
        end
    end

    -- Reset dragging state only if the left mouse button is released
    if not clicked then
        isDraggingVar = false
    end
	if gamestatus=="game" or gamestatus=="levelselection" then
		return windowX, windowY, isDraggingVar, offsetXVar, offsetYVar
	elseif gamestatus=="levelEditor" then
		return levelEditorContainer.x, levelEditorContainer.y, isDraggingVar, offsetXVar, offsetYVar
	end
end

function love.mousemoved(x, y, dx, dy, istouch)
    if gamestatus == "levelselection" then
        -- Update the window position for level selection
        windowX, windowY, isDragging, offsetX, offsetY = handleWindowDragging(windowX, windowY, 400 + 50, 50, isDragging, offsetX, offsetY)
        if isDragging==true then updateButtonPositions() end
    elseif gamestatus == "game" then
        -- Update the help window position for the game
        helpWindowX, helpWindowY, isHelpDragging, helpOffsetX, helpOffsetY = handleWindowDragging(helpWindowX, helpWindowY, 400, 100, isHelpDragging, helpOffsetX, helpOffsetY)
        updateHelpButtonPosition()
    elseif gamestatus == "levelEditor" then
       levelEditorContainer.x, levelEditorContainer.y, isDragging, offsetX, offsetY =
            handleWindowDragging(levelEditorContainer.x, levelEditorContainer.y, 400 + 50, 50, isDragging, offsetX, offsetY)
        if isDragging == true then
            updateLevelEditorButtonPositions()
        end
    end
end

function love.mousepressed(x, y, button, istouch, presses)
    if gamestatus == "levelselection" then
        -- Update the window position for level selection
        windowX, windowY, isDragging, offsetX, offsetY = handleWindowDragging(windowX, windowY, 400 + 50, 50, isDragging, offsetX, offsetY)
        
    elseif gamestatus == "game" then
        -- Update the help window position for the game
        helpWindowX, helpWindowY, isHelpDragging, helpOffsetX, helpOffsetY = handleWindowDragging(helpWindowX, helpWindowY, 400, 100, isHelpDragging, helpOffsetX, helpOffsetY)
    elseif gamestatus == "levelEditor" then
         levelEditorButtonsContainer.isDragging = isMouseInButton(levelEditorButtonsContainer, x, y)
    end
end

function love.mousereleased(x, y, button, istouch, presses)
    if gamestatus == "levelselection" then
        -- Reset dragging state for level selection
        isDragging = false
    elseif gamestatus == "game" then
        -- Reset dragging state for game
        isHelpDragging = false
    elseif gamestatus == "levelEditor" then
          levelEditorButtonsContainer.isDragging = false
    end
end
