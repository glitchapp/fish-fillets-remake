function mouseinputload()
	--require ('/game/mainfunctions/mouse/mouseLevelEditor')
end

function HideShowmouse()
	--if love.mouse.isDown(2) and not (gamestatus=="levelEditor") then
	if love.mouse.isDown(2) then
		
			mousestate = not love.mouse.isVisible()
			love.mouse.setVisible(mousestate)
		
			if love.mouse.isVisible() then
				bfocus="none"
				axistouchedonce=nil

		elseif not love.mouse.isVisible() then
				axistouchedonce=true
		end
		
		love.timer.sleep( 0.2 )
	end
end

    
function love.wheelmoved(x, y)
	if not (nLevel==101 or nLevel==102 or nLevel==103) and not (gamestatus=="levelselection") then
		if skin=="remake" then
			if y > 0  and scalepl<10 then 
				mousetext = "Mouse wheel moved up"
				if nLevel==13 then  
									expx=expx+0.1
									expy=expy+0.1
									dividx=dividx+0.05
									dividy=dividy+0.05
									tileSize=tileSize+6.9
									scalepl=scalepl+0.1	--35x29 cells
				elseif nLevel==15 then 
									expx=expx+0.1
									expy=expy+0.1
									dividx=dividx+0.17
									dividy=dividy+0.17
									tileSize=tileSize+9
									scalepl=scalepl+0.1
				elseif nLevel==32 then 
									expx=expx+0.1
									expy=expy+0.1
									dividx=dividx+0.15
									dividy=dividy+0.15
									tileSize=tileSize+5.2
									scalepl=scalepl+0.1
				elseif nLevel==59 then 
									expx=expx+0.1
									expy=expy+0.1
									dividx=dividx+0.20
									dividy=dividy+0.20
									tileSize=tileSize+5.2
									scalepl=scalepl+0.1
				else 
					expx=expx+0.1 expy=expy+0.1			--increase map blocks size
					--bckx=bckx-100 bcky=bcky-100 bck2x=bck2x-100 bck2y=bck2y-100 frgx=frgx-100 frgy=frgy-100		-- center image while zooming
					dividx=dividx+0.2 dividy=dividy+0.2	--increase objects size
					tileSize=tileSize+7.7				--increase distance between map blocks
					scalepl=scalepl+0.2
				end
			elseif y < 0 and scalepl>1 then
				mousetext = "Mouse wheel moved down"
				if nLevel==13 then
									expx=expx-0.1
									expy=expy-0.1
									dividx=dividx-0.05
									dividy=dividy-0.05
									tileSize=tileSize-6.9
									scalepl=scalepl-0.1	--35x29 cells
				elseif nLevel==15 then 
									expx=expx-0.1
									expy=expy-0.1
									dividx=dividx-0.17
									dividy=dividy-0.17
									tileSize=tileSize-9
									scalepl=scalepl-0.1
				elseif nLevel==32 then 
									expx=expx-0.1
									expy=expy-0.1
									dividx=dividx-0.15
									dividy=dividy-0.15
									tileSize=tileSize-5.2
									scalepl=scalepl-0.1
				elseif nLevel==59 then 
									expx=expx-0.1
									expy=expy-0.1
									dividx=dividx-0.20
									dividy=dividy-0.20
									tileSize=tileSize-5.2
									scalepl=scalepl-0.1
				else
					expx=expx-0.1 expy=expy-0.1			-- decrease map blocks size
					--bckx=bckx+100 bcky=bcky+100 bck2x=bck2x+100 bck2y=bck2y+100 frgx=frgx+100 frgy=frgy+100		-- center image while zooming
					dividx=dividx-0.2 dividy=dividy-0.2	-- decrease	objects size
					tileSize=tileSize-7.7				--decrease distance between map blocks
					scalepl=scalepl-0.2
				end
			end
		end
	end
end



function updatecamerafrommouse()
	if scalepl>1 and not joystick then
		mousex, mousey = love.mouse.getPosition()
		bckx=-(mousex*scalepl) bck2x=-(mousex*scalepl)	frgx=-(mousex*scalepl)
		bcky=-(mousey*scalepl) bck2y=-(mousey*scalepl)	frgy=-(mousey*scalepl)
	end
	if love.mouse.isDown( 3 ) then resetallzoomvalues() end
end

function resetallzoomvalues()
assignremakesize()
scalepl=1
tileSize = gridSize
bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

love.timer.sleep( 0.2 )
end



-- about and showhelp functions
--[[
function love.mousepressed(x, y, button, istouch, presses)
    if gamestatus == "levelselection" then
   
        -- Check if the mouse click is within the draggable area, considering the updated window position
        if aboutInfo and x >= windowX and x <= windowX + 400 + 50 and y >= windowY and y <= windowY + 50 and not isDragging then
            isDragging = true
            --offsetX = x - windowX
            --offsetY = y - windowY
            updateButtonPositions()
        else
            isDragging = false
        end

        
        
    elseif gamestatus == "game" then
		-- Check if the mouse click is within the draggable area of the help window
		if helpison == "yes" and x >= helpWindowX and x <= helpWindowX + 400 and y >= helpWindowY and y <= helpWindowY + 100 then
			isHelpDragging = true
			helpOffsetX = x - helpWindowX
			helpOffsetY = y - helpWindowY
		else
			isHelpDragging = false
		end

		-- Check if the mouse click is within the help button
		local helpButtonClicked = isButtonHovered(HelpButton)
		if helpison == "no" and helpButtonClicked and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick, button)) then
			helpison = "yes"
		end
	end
	
end

function love.mousereleased(x, y, button, istouch, presses)
if gamestatus=="levelselection" then
     -- Reset dragging state only if the left mouse button is released
    if button == 1 then
        isDragging = false

        -- Call the function to update button positions
        --updateButtonPositions()
    end
 elseif gamestatus == "game" then
   -- Reset dragging state only if the left mouse button is released
    if button == 1 then
        isHelpDragging = false

        -- Update the help window position based on mouse release coordinates
        helpWindowX = x - helpOffsetX
        helpWindowY = y - helpOffsetY

        -- Update the position of the help button
        updateHelpButtonPosition()
    end
 
end

end
--]]
