-- Constants for button sizes and positions
local BUTTON_SIZE = 160
local BUTTON_MARGIN = 40

-- Button positions
local BUTTON_LEFT_X = BUTTON_MARGIN
local BUTTON_RIGHT_X = BUTTON_MARGIN + BUTTON_SIZE + BUTTON_MARGIN
local BUTTON_UP_X = BUTTON_MARGIN + (BUTTON_SIZE + BUTTON_MARGIN)
local BUTTON_DOWN_X = BUTTON_MARGIN + (BUTTON_SIZE + BUTTON_MARGIN)

local BUTTON_SWITCH_X = BUTTON_MARGIN + (BUTTON_SIZE + BUTTON_MARGIN)

--local BUTTON_UP_X = love.graphics.getWidth() - BUTTON_MARGIN - BUTTON_SIZE
--local BUTTON_DOWN_X = love.graphics.getWidth() - BUTTON_MARGIN - BUTTON_SIZE - BUTTON_MARGIN - BUTTON_SIZE

-- Button states
local isLeftPressed = false
local isRightPressed = false
local isUpPressed = false
local isDownPressed = false
local isSwitchPressed = false

function touchcontrolsload()
    -- Load assets and initialize game
    
    -- ...
end

--function love.touchpressed( id, x, y, dx, dy, pressure )
	--print(pressure)
--end

function touchcontrolsdraw()
    -- Draw game graphics
    
    -- ...

    -- Draw virtual game controller buttons
    love.graphics.setColor(1, 1, 1, 0.5) -- Set transparency for buttons
    love.graphics.rectangle("fill", BUTTON_LEFT_X, love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN, BUTTON_SIZE, BUTTON_SIZE)
    love.graphics.rectangle("fill", BUTTON_RIGHT_X, love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN, BUTTON_SIZE, BUTTON_SIZE)
    love.graphics.rectangle("fill", BUTTON_UP_X*0.5, love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN*3.5, BUTTON_SIZE, BUTTON_SIZE)
    love.graphics.rectangle("fill", BUTTON_DOWN_X*0.5, love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN*(-1.5), BUTTON_SIZE, BUTTON_SIZE)
    
    love.graphics.rectangle("fill", BUTTON_SWITCH_X*6, love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN, BUTTON_SIZE, BUTTON_SIZE)
end

function touchcontrolsupdate(dt)
    -- Update game logic
    if isLeftPressed or isRightPressed or isUpPressed or isDownPressed or isSwitchPressed then
		
			if isLeftPressed then
				dx = -1
				dy = 0
			elseif isRightPressed then
				dx = 1
				dy = 0
			elseif  isUpPressed then
				dx = 0
				dy = -1
			elseif isDownPressed then
				dx = 0
				dy = 1
			elseif isSwitchPressed  then
			pb:switchAgent ()
			dx = 0
			dy = 0
			end
    end
end

function love.touchpressed(id, x, y, dx, dy, pressure)
    -- Handle touch press events
    
    -- Check if the touch press is within the left button
    if x >= BUTTON_LEFT_X and x <= BUTTON_LEFT_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isLeftPressed = true
    end

    -- Check if the touch press is within the right button
    if x >= BUTTON_RIGHT_X and x <= BUTTON_RIGHT_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isRightPressed = true
    end

    -- Check if the touch press is within the up button
    if x >= BUTTON_UP_X and x <= BUTTON_UP_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isUpPressed = true
    end

    -- Check if the touch press is within the Down button
    if x >= BUTTON_DOWN_X and x <= BUTTON_DOWN_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isDownPressed = true
    end
    
    -- Check if the touch press is within the Switch button
    if x >= BUTTON_SWITCH_X and x <= BUTTON_SWITCH_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isSwitchPressed = true
    end
end

function love.touchreleased(id, x, y, dx, dy, pressure)
    -- Handle touch release events
    
    -- Check if the touch release is within the left button
    if x >= BUTTON_LEFT_X and x <= BUTTON_LEFT_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isLeftPressed = false
    end

    -- Check if the touch release is within the right button
    if x >= BUTTON_RIGHT_X and x <= BUTTON_RIGHT_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isRightPressed = false
    end

    -- Check if the touch release is within the up button
    if x >= BUTTON_UP_X and x <= BUTTON_UP_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isUpPressed = false
    end

    -- Check if the touch release is within the down button
    if x >= BUTTON_DOWN_X and x <= BUTTON_DOWN_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isDownPressed = false
    end
    
       -- Check if the touch release is within the Switch button
    if x >= BUTTON_SWITCH_X and x <= BUTTON_SWITCH_X + BUTTON_SIZE and y >= love.graphics.getHeight() - BUTTON_SIZE - BUTTON_MARGIN and y <= love.graphics.getHeight() - BUTTON_MARGIN then
        isSwitchPressed = false
    end
end
