local zip = require("lib/zip")
local File = require("lib/zip.File")
local Archive = require("lib/zip.Archive")
local Disk = require("lib/zip.Disk")

-- Function to create a file with the specified path and content
function createZipFile(filePath, content)
    local newFile = File:new()
    newFile:SetPath(filePath)
    newFile:open()
    newFile:write(content)
    newFile:close()
    return newFile
end

function zipMyGame()
    local filesToZip = {
    
     ["skin.txt"] = tostring(skin),
     ["skinupscaled.txt" ] = tostring(skinupscaled),
     ["palette.txt" ] = tostring(palette),
     ["hdr.txt" ] = tostring(hdr),
     ["res.txt" ] = tostring(res),
     ["fullscreen.txt" ] = tostring(fullscreen),
     ["vsyncstatus.txt" ] = tostring(vsyncstatus),
     ["limitframerate.txt" ] = tostring(limitframerate),
     
     
     -- HUB
     	  ["showfps.txt" ] = tostring(showfps),
     	  ["showbattery.txt" ] = tostring(showbattery),
          ["showtimedate.txt" ] = tostring(showtimedate),
     
     -- RGB color adjustements
			  ["adjustr.txt" ] = tostring(adjustr),
			  ["adjustg.txt" ] = tostring(adjustg),
			  ["adjustb.txt" ] = tostring(adjustb),
    -- Chromatic aberration
		  ["chromaab.txt" ] = tostring(chromaab),
    
    -- advanced graphic effects
       ["shader1.txt" ] = tostring(shader1),
       ["shader2.txt" ] = tostring(shader2),
       ["caustics.txt" ] = tostring(caustics),
    
    --voice dubs
       ["talkiestxt" ] = tostring(talkies),
    
    --language
      	["language.txt" ] = tostring(language),
       ["language2.txt" ] = tostring(language2),
      	["accent.txt" ] = tostring(accent),
      	["accent2.txt" ] = tostring(accent2),
    
    --audio
      	["soundon.txt" ] = tostring(soundon),
       	["musicon.txt" ] = tostring(musicon),
      	["musictype.txt"  ] = tostring(musictype),
    
    --controls
		["touchinterfaceison.txt" ] = tostring(touchinterfaceison),
      	["vibration.txt" ] = tostring(vibration),
     	["controllerLayout.txt" ] = tostring(controllerLayout),
     --if controllerLayout==nil then controllerLayout="default" end	-- prevent controllerLayout from being unasigned
     --[[
      convertsettingsstringtobooleans()
    convertsettingsstringtonumbers()
    
    -- load game progress
    convertunlockedlevelbooleantostring()
		for i = 1,79,1
			do 
			unlockedlevels[i] = love.filesystem.read( "unlockedlevels".. i ..".txt" )	
		end
        convertunlockedlevelstringtoboolean()
        --]]
        
--extra menus
  ["leveleditorunlocked.txt" ] = tostring(leveleditorunlocked),
  ["extrasunlocked.txt" ] = tostring(extrasunlocked),
  ["cutscenesunlocked.txt" ] = tostring(cutscenesunlocked),
 
--unlocked ends
  ["endmenuunlocked.txt" ] = tostring(endmenuunlocked),
  ["fishhouseunlockedend.txt" ] = tostring(fishhouseunlockedend),
  ["shipwrecksunlockedend.txt" ] = tostring(shipwrecksunlockedend),
  ["silversshipunlockedend.txt" ] = tostring(silversshipunlockedend),
  ["cityinthedeepunlockedend.txt" ] = tostring(cityinthedeepunlockedend),
  ["ufounlockedend.txt" ] = tostring(ufounlockedend),
  ["coralreefunlockedend.txt" ] = tostring(coralreefunlockedend),
  ["treasurecaveunlockedend.txt" ] = tostring(treasurecaveunlockedend),
  ["dumpunlockedend.txt" ] = tostring(dumpunlockedend),
  ["secretcomputerunlockedend.txt" ] = tostring(secretcomputerunlockedend),
  ["nextgenerationunlockedend.txt" ] = tostring(nextgenerationunlockedend),
  ["gameendunlockedend.txt" ] = tostring(gameendunlockedend),
          
--unlocked music
  ["musicplayerunlocked.txt" ] = tostring(musicplayerunlocked),

--unlocked music authors and tracks

-- classic tracks
 ["classictracksunlocked.txt" ] = tostring(classictracksunlocked),
 ["kufrik_song.txt" ] = tostring(kufrik_song),
 ["classicmenu_song.txt" ] = tostring(classicmenu_song),
 ["rybky1_song.txt" ] = tostring(rybky1_song),
 ["rybky2_song.txt" ] = tostring(rybky2_song),
 ["rybky3_song.txt" ] = tostring(rybky3_song),
 ["rybky4_song.txt" ] = tostring(rybky4_song),
 ["rybky5_song.txt" ] = tostring(rybky5_song),
 ["rybky6_song.txt" ] = tostring(rybky6_song),
 ["rybky7_song.txt" ] = tostring(rybky7_song),
 ["rybky8_song.txt" ] = tostring(rybky8_song),
 ["rybky9_song.txt" ] = tostring(rybky9_song),
 ["rybky10_song.txt" ] = tostring(rybky10_song),
 ["rybky11_song.txt" ] = tostring(rybky11_song),
 ["rybky12_song.txt" ] = tostring(rybky12_song),
 ["rybky13_song.txt" ] = tostring(rybky13_song),
 ["rybky14_song.txt" ] = tostring(rybky14_song),
 ["rybky15_song.txt" ] = tostring(rybky15_song),

--pixelsphere
  ["pixelsphereunlocked.txt" ] = tostring(pixelsphereunlocked),

  ["enchanted_song.txt" ] = tostring(enchanted_song),
  ["aquaria_song.txt" ] = tostring(aquaria_song),
  ["sirens_song.txt" ] = tostring(sirens_song),
  ["song18_song.txt" ] = tostring(song18_song),
  ["song21_song.txt" ] = tostring(song21_song),
  ["vapor_song.txt" ] = tostring(vapor_song),
  ["calmrelax_song.txt" ] = tostring(calmrelax_song),
  ["ambientI_song.txt" ] = tostring(ambientI_song),
  ["ambientII_song.txt" ] = tostring(ambientII_song),
  ["ambientIII_song.txt" ] = tostring(ambientIII_song),
  ["anotheraugust_song.txt" ] = tostring(anotheraugust_song),
  ["sevenandeight_song.txt" ] = tostring(sevenandeight_song),
  ["novemembersnow_song.txt" ] = tostring(novemembersnow_song),
  ["icyrealm_song.txt" ] = tostring(icyrealm_song),
  ["happylullaby_song.txt" ] = tostring(happylullaby_song),
  ["thehex_song.txt" ] = tostring(thehex_song),
 
--isaiah658
  ["isaiah658unlocked.txt" ] = tostring(isaiah658unlocked),
  ["ambient_song.txt" ] = tostring(ambient_song),
  ["underwater2_song.txt" ] = tostring(underwater2_song),
--umplix
  ["umplixunlocked.txt" ] = tostring(umplixunlocked),
  ["deepsea_song.txt" ] = tostring(deepsea_song),
  ["sinkingfeeling_song.txt"  ] = tostring(sinkingfeeling_song),
--ericmatyas
  ["ericmatyasunlocked.txt"  ] = tostring(ericmatyasunlocked),
  ["hyp_song.txt"  ] = tostring(hyp_song),
  ["dream_song.txt"  ] = tostring(dream_song),
  ["islandofmysteries_song.txt"  ] = tostring(islandofmysteries_song),
  ["monkeyislandband_song.txt"  ] = tostring(monkeyislandband_song),
  ["puzzlegame_song.txt"  ] = tostring(puzzlegame_song),
  ["theyarehere_song.txt"  ] = tostring(theyarehere_song),

--cleytonkauffman
  ["cleytonkauffmanunlocked.txt" ] = tostring(cleytonkauffmanunlocked),
  ["underwaterI_song.txt"  ] = tostring(underwaterI_song),
  ["underwaterII_song.txt" ] = tostring(underwaterII_song),

--marcelof
  ["marcelofunlocked.txt" ] = tostring(marcelofunlocked),
  ["airy_song.txt"  ] = tostring(airy_song),

--springyspringo
  ["springyspringounlocked.txt" ] = tostring(springyspringounlocked),
  ["water_song.txt" ] = tostring(water_song),

--tokygeisha
  ["tokyogeishaunlocked.txt" ] = tostring(tokyogeishaunlocked),
  ["ambience_song.txt" ] = tostring(ambience_song),
  ["ambienceII_song.txt"  ] = tostring(ambienceII_song),
  ["creep_song.txt"  ] = tostring(creep_song),

--poinl
  ["poinlunlocked.txt" ] = tostring(poinlunlocked),
  ["nautilus_song.txt"  ] = tostring(nautilus_song),

--hectavex
  ["hectavexunlocked.txt" ] = tostring(hectavexunlocked),
  ["rupture_song.txt" ] = tostring(rupture_song),
  ["ova_song.txt"  ] = tostring(ova_song),
 ["vanish_song.txt" ] = tostring(vanish_song),

--tedkerr
  ["tedkerrunlocked.txt" ] = tostring(tedkerrunlocked),
  ["crashedship_song.txt"  ] = tostring(crashedship_song),
  ["scifi_song.txt"  ] = tostring(scifi_song),

--isao
  ["isaounlocked.txt" ] = tostring(isaounlocked),
  ["upbeat_song.txt"  ] = tostring(upbeat_song),
  ["epicdance_song.txt" ] = tostring(epicdance_song),

--viktorkraus

  ["viktorkrausunlocked.txt" ] = tostring(viktorkrausunlocked),
  ["arobotswaytoheavenunlocked.txt" ] = tostring(arobotswaytoheavenunlocked),

--glitchapp mixes

 ["glitchappmixesunlocked.txt" ] = tostring(glitchappmixesunlocked),
 ["hexhappylullabyunlocked.txt" ] = tostring(hexhappylullabyunlocked),
 ["remix1unlocked.txt" ] = tostring(remix1unlocked),
 ["remix2unlocked.txt" ] = tostring(remix2unlocked),
 ["remix35unlocked.txt" ] = tostring(remix35unlocked),
 ["rupturemunlocked.txt" ] = tostring(rupturemunlocked),
 ["remix4unlocked.txt" ] = tostring(remix4unlocked),
}

    -- Create a new archive
    local newArchive = Archive:new()

    -- Create a new disk
    local newDisk = Disk:new()

    -- Add the disk to the archive
    newArchive:AddDisk(newDisk)

    -- Add the files to the disk
    for filePath, content in pairs(filesToZip) do
        local newFile = createZipFile(filePath, content)
        newDisk:PutEntry(newFile)
    end
    
    -- Define the absolute path to the file in the root folder
	--outputPath = love.filesystem.getRealDirectory("/") .. "savedgame.zip"

	-- Save the archive to the specified file path
	--newArchive:write(outputPath)

    -- Save the archive to a file
    newArchive:write("savedgame.zip")
end
