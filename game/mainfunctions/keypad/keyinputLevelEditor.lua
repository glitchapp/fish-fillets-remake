function keyinputLevelEditor(key, scancode, isrepeat)
	--[[
	if gamestatus=="levelEditor" and not (typingGridWidth==true) or not (typingGridWidth==true) then 
		-- Switch the currently selected tile with number keys (1, 2, 3)
		--if key == "0" or key == "1" or key == "2" or key == "3" then
			--currentTile = tonumber(key)
		--end
		
		-- Switch drawing modes with number keys
		if key == "1" then
			drawingMode = "wall"
		elseif key == "2" then
			drawingMode = "border"
		elseif key == "3" then
			drawingMode = "emptyCell"
		elseif key == "4" then
			drawingMode = "agent"
		elseif key == "5" then
			drawingMode = "agent2"
		elseif key == "6" then
			drawingMode = "exit"
		elseif key == "7" then
			drawingMode = "object"
			isDrawingObject = true
		elseif key == "8" then
			isDrawingHeavy = not isDrawingHeavy  -- Toggle between heavy and light objects
			love.timer.sleep( 0.3 )	
		end
	
		-- Export the level when the "e" key is pressed
		if key == "e" then
			exportLevel()
		end
	end
	--]]
if gamestatus == "levelEditor" then
    if typingGridWidth then
        if key == "return" then
            -- Attempt to convert gridWidthInput to a number
            local newWidth = tonumber(gridWidthInput)

            print("Grid width input:", gridWidthInput)  -- Add this line to print the input
            print("New width after tonumber:", newWidth)  -- Add this line to print newWidth

            -- Check if newWidth is a valid number and greater than 0
            if newWidth and newWidth > 0 then
                print("Setting grid width to:", newWidth)
                setGridWidth(newWidth)
            else
                print("Invalid grid width input:", gridWidthInput)
            end

            gridWidthInput = ""  -- Reset the input string
            typingGridWidth = false
        elseif key == "backspace" then
            gridWidthInput = gridWidthInput:sub(1, -2)
            print("Grid width input:", gridWidthInput)  -- Add this line to print the input
        end
    elseif typingGridHeight then
        if key == "return" then
            -- Attempt to convert gridHeightInput to a number
            local newHeight = tonumber(gridHeightInput)

            print("Grid height input:", gridHeightInput)  -- Add this line to print the input
            print("New height after tonumber:", newHeight)  -- Add this line to print newHeight

            -- Check if newHeight is a valid number and greater than 0
            if newHeight and newHeight > 0 then
                print("Setting grid height to:", newHeight)
                setGridHeight(newHeight)
            else
                print("Invalid grid height input:", gridHeightInput)
            end

            gridHeightInput = ""  -- Reset the input string
            typingGridHeight = false
        elseif key == "backspace" then
            gridHeightInput = gridHeightInput:sub(1, -2)
            print("Grid height input:", gridHeightInput)  -- Add this line to print the input
        end
    end
end


end

