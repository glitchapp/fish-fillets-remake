function keyinputPlusoptions(key, scancode, isrepeat)

	if gamestatus=="gameplusoptions" then
		if love.keyboard.isDown("left") or ("right") or ("up") or ("down") then
			inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		
						if love.keyboard.isDown("left") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="graphics" then bfocus="languages"
						elseif bfocus=="audio" then bfocus="graphics"
						elseif bfocus=="controls" then bfocus="audio"
						elseif bfocus=="optionsreturn" then bfocus="audio"
						end
				elseif love.keyboard.isDown("right") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="languages" then bfocus="graphics"
						elseif bfocus=="graphics" then bfocus="audio"
						elseif bfocus=="audio" then bfocus="controls"
						elseif bfocus=="controls" then bfocus="optionsreturn"
						end
						
				--[[
				--joystick shoulders
				elseif love.keyboard.isDown("leftshoulder") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="graphics" then bfocus="languages"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="languages"
						elseif bfocus=="audio" then bfocus="graphics"
								inputtime=0
								love.timer.sleep( 0.3 )
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								currenttab="graphics"
						elseif bfocus=="controls" then bfocus="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="optionsreturn" then bfocus="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						end
					
				elseif love.keyboard.isDown("rightshoulder") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="languages" then bfocus="graphics"
								inputtime=0
								love.timer.sleep( 0.3 )
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								currenttab="graphics"
						elseif bfocus=="graphics" then bfocus="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="audio" then bfocus="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						elseif bfocus=="controls" then
						end
				--]]
				elseif  love.keyboard.isDown("up") then
						if bfocus=="axis" then bfocus="languages" end
				elseif love.keyboard.isDown("down") then
						if bfocus=="axis" then bfocus="languages" end
				elseif love.keyboard.isDown("a")  then
							if bfocus=="languages" then 
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="languages"
						elseif bfocus=="graphics" then
								inputtime=0
								love.timer.sleep( 0.3 )
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								currenttab="graphics"
						elseif bfocus=="audio" then
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="controls" then
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						elseif bfocus=="optionsreturn" then 
							returntolevelselection()
						
		
				--[[elseif love.keyboard.isDown("start") or love.keyboard.isDown("back") then
						dx = 0
						dy = 0
						shader2=false
						mousestate = not love.mouse.isVisible()	-- show mouse pointer
						love.mouse.setVisible(mousestate)
						gamestatus="game"
						love.timer.sleep( 0.3 )
				--]]
				end
		end
		end
	end
	

end
