function keyinputLevelSelection(key, scancode, isrepeat)
-- gamepad selection on level selection menu
	if gamestatus=="levelselection" then 
	
		if love.keyboard.isDown("left") or ("right") or ("up") or ("down") then 
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
		
			if love.keyboard.isDown("left") then
						if bfocus=="axis" or bfocus=="none" then bfocus="exit"
					elseif bfocus=="exit" or bfocus=="extras" or bfocus=="ends" or bfocus=="save" or bfocus=="reset" then
							if musicplayerunlocked==true then bfocus="music" end
					elseif bfocus=="options" then bfocus="credits"
					elseif bfocus=="rejectexit" then bfocus="confirmexit"
					elseif bfocus=="rejectsave" then  bfocus="confirmsave" 
					end
			elseif love.keyboard.isDown("right") then
						if bfocus=="axis" or bfocus=="none" then bfocus="exit"
					elseif bfocus=="music" 	then bfocus="exit"
					elseif bfocus=="credits" then bfocus="options"
					elseif bfocus=="confirmexit" then bfocus="rejectexit"
					elseif bfocus=="confirmsave" then  bfocus="rejectsave" 
					end
			elseif  love.keyboard.isDown("up") then
						if bfocus=="axis" or bfocus=="none" then bfocus="exit"
					elseif bfocus=="credits" and musicplayerunlocked==true then bfocus="music"
					elseif bfocus=="options" then bfocus="reset"
					elseif bfocus=="reset" then bfocus="save"
					elseif bfocus=="save" then 
								if endmenuunlocked==true then bfocus="ends"
							elseif extrasunlocked==true then bfocus="extras"
							else bfocus="exit"
							end
					elseif bfocus=="ends" then bfocus="extras"
					elseif bfocus=="extras" then bfocus="exit"
					end
			elseif love.keyboard.isDown("down") then
						if bfocus=="axis" or bfocus=="none" then bfocus="exit"
					elseif bfocus=="music" then bfocus="credits"
					elseif bfocus=="exit" then
							if extrasunlocked==true then bfocus="extras"
						elseif endmenuunlocked==true then bfocus="ends"
						else bfocus="save"
						end
					elseif bfocus=="extras" then 
							if endmenuunlocked==true then bfocus="ends"bfocus="ends"
							else bfocus="options"
							end
					elseif bfocus=="ends" then bfocus="save"
					elseif bfocus=="save" then bfocus="reset"
					elseif bfocus=="reset" then bfocus="options"
					end
			elseif love.keyboard.isDown("a") and not (secretcodestep==6) then --secretcode prevents to select an option when typing the secret code
							gamepadselectanimation=true
						if bfocus=="exit" then 
							confirmexit()
					elseif bfocus=="confirmexit" then 
						exitconfirmpressed=true timer2=0 inputtime=0
						bfocus="exit"
						love.timer.sleep( 0.4 )	
					elseif bfocus=="rejectexit" then 
						exitbuttonpressed=false timer2=0 inputtime=0
						exitconfirmpressed=false
						bfocus="exit"
						love.timer.sleep( 0.5)	
					elseif bfocus=="confirmsave" then  savebuttonpressed=true saveconfirmpressed=true
							saveconfirmpressed=true timer2=0 inputtime=0
							savemygame()
							bfocus="exit"
							love.timer.sleep( 0.5 )	
					elseif bfocus=="reset" then
							resetcolors()
							savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
							--video effect
							effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
							effect.enable("boxblur")
							loadachievements()
							savebuttonpressed=true saveconfirmpressed=false timer2=0
							bfocus="confirmsave"
							love.timer.sleep( 0.5 )	
					elseif bfocus=="rejectsave" then  savebuttonpressed=false saveconfirmpressed=false timer2=0 inputtime=0
						bfocus="exit"
						love.timer.sleep( 0.5 )	
					elseif bfocus=="extras" then
						music:stop()
						gamestatus="extras" 
					elseif bfocus=="ends" then 
						music:stop()
						require ('game/ends/endmenu')		-- Load ends menu
						endmenuload()
						gamestatus="endmenu"
					elseif bfocus=="save" then
						savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
						bfocus="confirmsave"
						--video effect
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("boxblur")
						love.timer.sleep( 0.5 )	
					elseif bfocus=="reset" then 
						resetcolors()
						savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
						--video effect
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("boxblur")
						loadachievements()
						savebuttonpressed=true saveconfirmpressed=false timer2=0
						love.timer.sleep( 0.5 )	
					elseif bfocus=="music" then gamestatus="music"
					elseif bfocus=="options" then
						if level1bck==nil then
							level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
							level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
							level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
						end
						optionsloadlanguages()
						bfocus=nil
						bfocustab="languages"
						gamestatus="options"
					elseif bfocus=="credits" then gamestatus="credits"
					end
			elseif love.keyboard.isDown("b")  then
						confirmexit()
			end
		end
	end
end
