function keyinputOptionsLanguages(key, scancode, isrepeat)

	-- Keyinput selection on options menu
		if gamestatus=="options" then 
				if love.keyboard.isDown("a")  then
						gamepadselectanimation=true
						if bfocus=="confirmsave" then
							savebuttonpressed=false
							saveconfirmpressed=false
							optionsmenupressed=false
							languagehaschanged=false
							timer2=0 inputtime=0
							savemygame()
							gamestatus="options"
							bfocus=nil
							bfocustab="languages"
							love.timer.sleep( 0.5 )	
					elseif bfocus=="rejectsave" then 
						savebuttonpressed=false 
						saveconfirmpressed=false 
						optionsmenupressed=false
						languagehaschanged=false
						timer2=0 inputtime=0
						gamestatus="levelselection"
						bfocus="exit"
						love.timer.sleep( 0.5 )	
					end
				end
	
			--Tab selection
					if love.keyboard.isDown("q") then
								gamepadselectanimation=true
								
							if bfocus=="axis" then bfocustab="languages"
						elseif bfocustab=="graphics" then bfocustab="languages"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="languages"
						elseif bfocustab=="audio" then bfocustab="graphics"
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								inputtime=0
								love.timer.sleep( 0.3 )
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								currenttab="graphics"
						elseif bfocustab=="controls" then bfocustab="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="optionsreturn" then bfocustab="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						end
				elseif love.keyboard.isDown("e") then
								gamepadselectanimation=true
							if bfocus=="axis" then bfocustab="languages"
						elseif bfocustab=="languages" then bfocustab="graphics"
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								inputtime=0
								love.timer.sleep( 0.3 )
								if level1bck==nil then
									level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
									level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
									level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
								end
								currenttab="graphics"
						elseif bfocustab=="graphics" then bfocustab="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocustab=="audio" then bfocustab="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						elseif bfocustab=="controls" then
						end
					end
	
	end
	
	if currenttab=="languages" then
	
		if love.keyboard.isDown("left") or ("right") or ("up") or ("down") then
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
					if love.keyboard.isDown("left") then
							if bfocus==nil then bfocus="langarrowup"
						elseif bfocus=="langarrowup" then
						elseif bfocus=="langarrowdown" then
						--elseif bfocus=="currentlanguage" then
						elseif bfocus=="actor1" then
						elseif bfocus=="actor2" then bfocus="actor1"
						elseif bfocus=="accent1" then
						elseif bfocus=="accent2" then bfocus="accent1"
						elseif bfocus=="rejectsave" then bfocus="confirmsave"
						end
				elseif love.keyboard.isDown("right") then
							if bfocus==nil then bfocus="langarrowup"
						elseif bfocus=="langarrowup" then
						elseif bfocus=="langarrowdown" then
						--elseif bfocus=="currentlanguage" then
						elseif bfocus=="actor1" then bfocus="actor2"
						elseif bfocus=="actor2" then
						elseif bfocus=="accent1" then bfocus="accent2"
						elseif bfocus=="accent2" then
						elseif bfocus=="confirmsave" then bfocus="rejectsave"
						end
						
			
				
				elseif  love.keyboard.isDown("up") then
							if bfocus==nil then bfocus="langarrowup"
						--elseif bfocus=="langarrowup" then bfocus="languages"
						--elseif bfocus=="currentlanguage" then bfocus="langarrowup"
						elseif bfocus=="langarrowdown" then bfocus="langarrowup"
						elseif bfocus=="actor1" then bfocus="langarrowdown"
						elseif bfocus=="actor2" then bfocus="langarrowdown"
						elseif bfocus=="accent1" then bfocus="actor1"
						elseif bfocus=="accent2" then bfocus="actor2"
						end
				elseif love.keyboard.isDown("down") then
							if bfocus==nil then bfocus="langarrowup"
						elseif bfocus=="langarrowup" then bfocus="langarrowdown"
						--elseif bfocus=="currentlanguage" then bfocus="langarrowdown"
						elseif bfocus=="langarrowdown" then bfocus="actor1"
						elseif bfocus=="actor1" then bfocus="accent1"
						elseif bfocus=="actor2" then bfocus="accent2"
						elseif bfocus=="accent1" then
						elseif bfocus=="accent2" then
						end
				elseif love.keyboard.isDown("a")  then
							gamepadselectanimation=true
								if bfocus=="langarrowup" then 
									language=langlist[langlistnumber-1]
									language2=langlist[langlistnumber-1]
									love.timer.sleep( 0.1 )
									languagehaschanged=true
									optionsload()
							elseif bfocus=="langarrowdown" then
									language=langlist[langlistnumber+1]
									language2=langlist[langlistnumber+1]
									love.timer.sleep( 0.1 )
									languagehaschanged=true
									optionsload()
							elseif bfocus=="actor1" then
										if accent=="br" then accent="us" languagehaschanged=true
									elseif accent=="us" then accent="br" languagehaschanged=true
									end
								elseif bfocus=="actor2" then
										if accent2=="br" then accent2="us" languagehaschanged=true
									elseif accent2=="us" then accent2="br" languagehaschanged=true
									end
								elseif bfocus=="accent1" or bfocus=="accent2" then 
								if accent=="br" then accent="us" accent2="us"
							elseif accent=="us" then accent="br" accent2="br"
							elseif accent=="es" then accent="la" accent2="la"
							elseif accent=="la" then accent="la" accent2="la"
							elseif accent=="fr" then accent="ca" accent2="ca"
							elseif accent=="ca" then accent="fr" accent2="fr"
							elseif accent=="de" then accent="ch" accent2="ch"
							elseif accent=="ch" then accent="de" accent2="de"
							end
							end
						
						elseif love.keyboard.isDown("b")  then
						returntolevelselection()
						
					
				end
		end
	end
	

end
