function keyinputOptions(key, scancode, isrepeat)


	-- gamepad selection on options menu
	if gamestatus=="options" then 
		if love.keyboard.isDown("left") or ("right") or ("up") or ("down") then
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
					if love.keyboard.isDown("left") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="graphics" then bfocus="languages"
						elseif bfocus=="audio" then bfocus="graphics"
						
						elseif bfocus=="controls" then bfocus="audio"
						elseif bfocus=="optionsreturn" then bfocus="audio"
						end
				elseif love.keyboard.isDown("right") then
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="languages" then bfocus="graphics"
						elseif bfocus=="graphics" then bfocus="audio"
						elseif bfocus=="audio" then bfocus="controls"
						elseif bfocus=="controls" then bfocus="optionsreturn"
						end
						
				--joystick shoulders
				elseif love.keyboard.isDown("left") then
									gamepadselectanimation=true
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="graphics" then bfocus="languages"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="languages"
						elseif bfocus=="audio" then bfocus="graphics"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="graphics"
						elseif bfocus=="controls" then bfocus="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="optionsreturn" then bfocus="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						end
				elseif love.keyboard.isDown("right") then
								gamepadselectanimation=true
							if bfocus=="axis" then bfocus="languages"
						elseif bfocus=="languages" then bfocus="graphics"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="graphics"
						elseif bfocus=="graphics" then bfocus="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="audio" then bfocus="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						elseif bfocus=="controls" then
						end
				
				elseif  love.keyboard.isDown("up") then
						if bfocus=="axis" then bfocus="languages" end
				elseif love.keyboard.isDown("down") then
						if bfocus=="axis" then bfocus="languages" end
				elseif love.keyboard.isDown("a")  then
							gamepadselectanimation=true
							if bfocus=="languages" then 
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="languages"
						elseif bfocus=="graphics" then
								inputtime=0
								love.timer.sleep( 0.3 )
								
								currenttab="graphics"
						elseif bfocus=="audio" then
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="audio"
						elseif bfocus=="controls" then
								inputtime=0
								love.timer.sleep( 0.3 )
								currenttab="controls"
						elseif bfocus=="optionsreturn" then 
							returntolevelselection()
						end
						
						elseif love.keyboard.isDown("b") then
						returntolevelselection()
						
						elseif love.keyboard.isDown("x") then
							inputtime=0
				
							savemygame()
							languagehaschanged=false
							musichaschanged=false
							skinhaschanged=false
							optionsmenupressed=false
							love.timer.sleep( 0.3 )
						
				end
		end
	end
	

end
