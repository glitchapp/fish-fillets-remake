function resizeres(w, h)
  -- Call the resize function to update translation and scale
  -- mr.resize()

  if w == 1366 and h == 768 then
    expx = 0.4
    expy = 0.4
    dividx = 0.8
    dividy = 0.8
    love.window.setMode(1366, 768, {resizable = true, borderless = false})
  elseif w == 1920 and h == 1080 then
    expx = 0.5
    expy = 0.5
    love.window.setMode(1920, 1080, {resizable = true, borderless = false})
  elseif w == 2560 and h == 1440 then
    love.window.setMode(2560, 1440, {resizable = true, borderless = false})
  elseif w == 3840 and h == 2160 then
    expx = 1
    expy = 1
    love.window.setMode(3840, 2160, {resizable = true, borderless = false})
  elseif w == 7680 and h == 4320 then
    expx = 2
    expy = 2
    love.window.setMode(7680, 4320, {resizable = true, borderless = false})
  end
end



function changeresolution()
		game = {}
	 	 if res=="720p" then resizeres (1366,768, {resizable = true, borderless = false})
							game.screen_width = 1366
							game.screen_height = 768
	 elseif res=="1080p" then resizeres (1920,1080, {resizable = true, borderless = false})
							game.screen_width = 1920
							game.screen_height = 1080
	 elseif res=="1440p" then resizeres (2560,1440, {resizable = true, borderless = false})
							
							game.screen_width = 2560
							game.screen_height = 1440
							--HowToPlayButton.x=1600*scalefactor
							--HowToPlayButton.y=1200*scalefactor
							--AboutFFilletsButton.x=1700*scalefactor 
							--AboutFFilletsButton.y=1200*scalefactor 
								
								
	 elseif res=="4k" then resizeres (3840,2160, {resizable = true, borderless = false})
							game.screen_width = 3840
							game.screen_height = 2160
	 elseif res=="8k" then resizeres (7680,4320, {resizable = true, borderless = false})
							game.screen_width = 7680
							game.screen_height = 4320
 	end

end		
