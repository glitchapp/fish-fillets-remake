
function loadachievements()
--extra menus
leveleditorunlocked=true
extrasunlocked=true
cutscenesunlocked=false

--unlocked ends
endmenuunlocked=false
fishhouseunlockedend=false
shipwrecksunlockedend=false
silversshipunlockedend=false
cityinthedeepunlockedend=false
ufounlockedend=false
coralreefunlockedend=false
treasurecaveunlockedend=false
dumpunlockedend=false
secretcomputerunlockedend=false
nextgenerationunlockedend=false
gameendunlockedend=false

--solved levels

unlockedlevels={
--0.Fishhouse
true,--1
false,--2
false,--3
false,--4
false,--5
false,--6
false,--7
false,--8

--1.Ship Wrecks
false,--9
false,--10
false,--11
false,--12
false,--13
false,--14
false,--15
false,--16
false,--17
false,--18
false,--19

--3. City in the deep
false,--20
false,--21
false,--22
false,--23
false,--24
false,--25
false,--26
false,--27
false,--28
false,--29

--5. Coral reef
false,--30
false,--31
false,--32
false,--33
false,--34
false,--35
false,--36
false,--37

--7. Dump
false,--38
false,--39
false,--40
false,--41
false,--42
false,--43
false,--44

--2.Silver's ship
false,--45
false,--46
false,--47
false,--48
false,--49
false,--50
false,--51

--4.UFO
false,--52
false,--53
false,--54
false,--55
false,--56
false,--57
false,--58

--6. Treasure cave
false,--59
false,--60
false,--61
false,--62
false,--63
false,--64

--8. Secret computer
false,--65
false,--66
false,--67
false,--68
false,--69
false,--70

--9. NG
false,--71
false,--72
false,--73
false,--74
false,--75
false,--76
false,--77
false,--78
false,--79

--10. test
false,--80
false,--81
false,--82
false,--83
false,--84
false,--85
false,--86
false,--87
false,--88
}


--unlocked music

musicplayerunlocked=false

--classic
classictracksunlocked=false
kufrik_song=false
classicmenu_song=false
rybky1_song=false
rybky2_song=false
rybky3_song=false
rybky4_song=false
rybky5_song=false
rybky6_song=false
rybky7_song=false
rybky8_song=false
rybky9_song=false
rybky10_song=false
rybky11_song=false
rybky12_song=false
rybky13_song=false
rybky14_song=false
rybky15_song=false


--pixelsphere
pixelsphereunlocked=false
enchanted_song=false
aquaria_song=false
sirens_song=false
song18_song=false
song21_song=false
vapor_song=false
calmrelax_song=false
ambientI_song=false
ambientII_song=false
ambientIII_song=false
anotheraugust_song=false
sevenandeight_song=false
novemembersnow_song=false
icyrealm_song=false
happylullaby_song=false
thehex_song=false

--isaiah658
isaiah658unlocked=false
ambient_song=false
underwater2_song=false

--umplix
umplixunlocked=false
deepsea_song=false
sinkingfeeling_song=false

--ericmatyas
ericmatyasunlocked=false
hyp_song=false
dream_song=false
islandofmysteries_song=false
monkeyislandband_song=false
puzzlegame_song=false
theyarehere_song=false

--cleytonkauffman
cleytonkauffmanunlocked=false
underwaterI_song=false
underwaterII_song=false

--marcelofg
marcelofunlocked=false
airy_song=false

--springyspringo
springyspringounlocked=false
water_song=false

--tokyogeisha
tokyogeishaunlocked=false
ambience_song=false
ambienceII_song=false
creep_song=false

--poinl
poinlunlocked=false
nautilus_song=false

--hectavex
hectavexunlocked=false
rupture_song=false
ova_song=false
vanish_song=false


--tedkerr
tedkerrunlocked=false
crashedship_song=false
scifi_song=false

--isao
isaounlocked=false
upbeat_song=false
epicdance_song=false

--Vikto Kraus
viktorkrausunlocked=false
arobotswaytoheavenunlocked=false

--glitchapp
glitchappmixesunlocked=false
hexhappylullabyunlocked=false
remix1unlocked=false
remix2unlocked=false
remix35unlocked=false
rupturemunlocked=false
remix4unlocked=false

end

function unlockeverything()

unlockedlevels={
--0.Fishhouse
true,--1
true,--2
true,--3
true,--4
true,--5
true,--6
true,--7
true,--8

--1.Ship Wrecks
true,--9
true,--10
true,--11
true,--12
true,--13
true,--14
true,--15
true,--16
true,--17
true,--18
true,--19

--3. City in the deep
true,--20
true,--21
true,--22
true,--23
true,--24
true,--25
true,--26
true,--27
true,--28
true,--29

--5. Coral reef
true,--30
true,--31
true,--32
true,--33
true,--34
true,--35
true,--36
true,--37

--7. Dump
true,--38
true,--39
true,--40
true,--41
true,--42
true,--43
true,--44

--2.Silver's ship
true,--45
true,--46
true,--47
true,--48
true,--49
true,--50
true,--51

--4.UFO
true,--52
true,--53
true,--54
true,--55
true,--56
true,--57
true,--58

--6. Treasure cave
true,--59
true,--60
true,--61
true,--62
true,--63
true,--64

--8. Secret computer
true,--65
true,--66
true,--67
true,--68
true,--69
true,--70

--9. NG
true,--71
true,--72
true,--73
true,--74
true,--75
true,--76
true,--77
true,--78
true,--79

--10. test
true,--80
true,--81
true,--82
true,--83
true,--84
true,--85
true,--86
true,--87
true,--88
}


--extra menus
leveleditorunlocked=true
extrasunlocked=true
cutscenesunlocked=true

--unlocked ends
endmenuunlocked=true
fishhouseunlockedend=true
shipwrecksunlockedend=true
silversshipunlockedend=true
cityinthedeepunlockedend=true
ufounlockedend=true
coralreefunlockedend=true
treasurecaveunlockedend=true
dumpunlockedend=true
secretcomputerunlockedend=true
nextgenerationunlockedend=true
gameendunlockedend=true

--unlocked music

musicplayerunlocked=true

--classic
classictracksunlocked=true
kufrik_song=true
classicmenu_song=true
rybky1_song=true
rybky2_song=true
rybky3_song=true
rybky4_song=true
rybky5_song=true
rybky6_song=true
rybky7_song=true
rybky8_song=true
rybky9_song=true
rybky10_song=true
rybky11_song=true
rybky12_song=true
rybky13_song=true
rybky14_song=true
rybky15_song=true


--pixelsphere
pixelsphereunlocked=true
enchanted_song=true
aquaria_song=true
sirens_song=true
song18_song=true
song21_song=true
vapor_song=true
calmrelax_song=true
ambientI_song=true
ambientII_song=true
ambientIII_song=true
anotheraugust_song=true
sevenandeight_song=true
novemembersnow_song=true
icyrealm_song=true
happylullaby_song=true
thehex_song=true

--isaiah658
isaiah658unlocked=true
ambient_song=true
underwater2_song=true

--umplix
umplixunlocked=true
deepsea_song=true
sinkingfeeling_song=true

--ericmatyas
ericmatyasunlocked=true
hyp_song=true
dream_song=true
islandofmysteries_song=true
monkeyislandband_song=true
puzzlegame_song=true
theyarehere_song=true

--cleytonkauffman
cleytonkauffmanunlocked=true
underwaterI_song=true
underwaterII_song=true

--marcelofg
marcelofunlocked=true
airy_song=true

--springyspringo
springyspringounlocked=true
water_song=true

--tokyogeisha
tokyogeishaunlocked=true
ambience_song=true
ambienceII_song=true
creep_song=true

--poinl
poinlunlocked=true
nautilus_song=true

--hectavex
hectavexunlocked=true
rupture_song=true
ova_song=true
vanish_song=true


--tedkerr
tedkerrunlocked=true
crashedship_song=true
scifi_song=true

--isao
isaounlocked=true
upbeat_song=true
epicdance_song=true

--Vikto Kraus
viktorkrausunlocked=true
arobotswaytoheavenunlocked=true

--glitchapp
glitchappmixesunlocked=true
hexhappylullabyunlocked=true
remix1unlocked=true
remix2unlocked=true
remix35unlocked=true
rupturemunlocked=true
remix4unlocked=true

end
