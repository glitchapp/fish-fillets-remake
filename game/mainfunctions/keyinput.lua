--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]
function keyinputload()
	require ('/game/mainfunctions/keypad/keyinputIntro')  				-- Key input on intro screen
	require ('/game/mainfunctions/keypad/keyinputLevelSelection')  		-- Key input on level selection
	require ('/game/mainfunctions/keypad/keyinputLevelCompleted')  		-- Key input on level completed
	require ('/game/mainfunctions/keypad/keyinputBriefcase')  			-- Key input on briefcase message
	require ('/game/mainfunctions/keypad/keyinputOptions')  			-- Key input on options
	require ('/game/mainfunctions/keypad/keyinputOptionsLanguages')		-- Key input on options - Languages tab
	require ('/game/mainfunctions/keypad/keyinputPlusOptions')  		-- Key input on options
	require ('/game/mainfunctions/keypad/keyinputExtras')  				-- Key input on Extras
	require ('/game/mainfunctions/keypad/keyinputEnds')  				-- Key input on Ends
	require ('/game/mainfunctions/keypad/keyinputCredits')  			-- Key input on Credits
	require ('/game/mainfunctions/keypad/keyinputMusic')  				-- Key input on Music
	require ('/game/mainfunctions/keypad/keyinputGameOver') 		
	require ('/game/mainfunctions/keypad/TerminalGuide')
	--require ('/game/mainfunctions/keypad/keyinputLevelEditor')
	require ('/game/states/LevelEditor/levelEditor/keyinputLevelEditor')
	
end


--This line of Lua code sets the key repeat behavior of the Love2D game engine's keyboard input to true. This means that when a key is held down, the "love.keypressed" event will be triggered repeatedly until the key is released.
love.keyboard.setKeyRepeat( true )


-- This code defines a function named "skinhaschangedfunction" which is used to handle a change in the game's skin or appearance.
function skinhaschangedfunction()
	-- Check if the "skinhaschanged" variable is true.
	if skinhaschanged==true then
					--If "skin" is equal to "remake", the function loads the assets for the current level of the game using the "loadlevelassets()" function, loads the avatars with the "loadavatars()" function, loads the music with "loadlevelmusic()", assigns the foreground and background layers with "assignfrgbck()", and loads the agents with "pb:agentsload(level)".
				if skin=="remake" then
					--expx=0.5 expy=0.5
					loadlevelassets()			-- load assets
					loadavatars()				-- load avatars
					loadlevelmusic()
					assignfrgbck()				-- Assign layers
					pb:agentsload(level)
					--If the "skin" variable is equal to "classic", the function loads the classic assets with "loadlevelassetsclassic()", loads the avatars with "loadavatars()", assigns the foreground and background layers with "assignfrgbckclassic()", loads the music with "loadlevelmusicclassic()", and loads the agents with "pb:agentsload(level)". Additionally, if "skinupscaled" is false, the function assigns the normal size with "assignsizenormal()". If "skinupscaled" is true, it assigns the upscaled size with "assignsizeupscaled()".
			elseif skin=="classic" then	
					loadlevelassetsclassic()	-- load classic assets
					loadavatars()				-- load avatars
					assignfrgbckclassic()		-- Assign layers
					loadlevelmusicclassic()
					pb:agentsload(level)
							
					-- If "skinupscaled" is false, assign the normal size using "assignsizenormal()".
					if skinupscaled==false then assignsizenormal()
					--If "skinupscaled" is true, assign the upscaled size using "assignsizeupscaled()".
				elseif skinupscaled==true then assignsizeupscaled()
				end
			end
			--Finally, the "skinhaschanged" variable is set to false. This function seems to be part of a larger program or game, and it is possible that it is triggered by an event related to changes in the game's "skin" or appearance.
		skinhaschanged=false
	end
end
		-- If "skinhaschanged" variable is true, call the "skinhaschangedfunction" function.
		if skinhaschanged==true then skinhaschangedfunction() end

--This code is a function that is executed every time a key is pressed. It takes in three arguments: key, scancode, and isrepeat. The function contains a series of conditional statements that are evaluated based on the key that was pressed.
function love.keypressed(key, scancode, isrepeat)
	
	keyinputIntro(key, scancode, isrepeat)				-- keyinput input on intro screen
	keyinputLevelSelection(key, scancode, isrepeat)		-- keyinput input on level selection
	keyinputLevelCompleted(key, scancode, isrepeat)		-- keyinput input on level selection
	keyinputBriefcase(key, scancode, isrepeat)			-- keyinput input on briefcase message
	keyinputOptions(key, scancode, isrepeat)			-- keyinput input on options
	keyinputOptionsLanguages(key, scancode, isrepeat)	-- keyinput input on options
	keyinputPlusoptions(key, scancode, isrepeat)		-- keyinput input on options (transparent window over game screen)
	keyinputExtras(key, scancode, isrepeat)				-- keyinput input on extras
	keyinputEnds(key, scancode, isrepeat)				-- keyinput input on Ends
	keyinputCredits(key, scancode, isrepeat)			-- keyinput input on Credits
	keyinputMusic(key, scancode, isrepeat)				-- keyinput input on Credits
	keyinputGameOver(key, scancode, isrepeat)			-- keyinput input on Game Over screen
	keyinputLevelEditor(key, scancode, isrepeat)
	keyinputTernminalGuide(key, scancode, isrepeat)
	
	-- Menu button focus sounds
	if not (gamestatus=="game") and not (gamestatus=="tetris") then
		--gamepad sounds
			if not (love.keyboard.isDown("a")) and not (gamestatus=="game") then
				misc_menu_4:stop()
				misc_menu_4:play()
			elseif love.keyboard.isDown("a") and not (gamestatus=="game") then
				gui28:stop()
				gui28:play()
			end
		end
	
	--The first conditional checks if the variable tetris is false. If it is, the following code is executed
	if tetris==false and gamestatus~="levelEditor" then	--if tetris mini game is off
	-- Numeric keys
	-- if the '1' key is pressed and the skin variable is equal to "classic", the skinupscaled variable is toggled between true and false, and skinhaschanged and musichaschanged variables are set to true. 
	if key == '1' then
			--[[turnmessagesoff()
			upscaledbuttonpressed=true
				if skinupscaled==false then skinupscaled=true
			elseif skinupscaled==true then skinupscaled=false
			end
			skinhaschanged=true musichaschanged=true
			skinhaschangedfunction()
			music:stop()
			loadlevelmusicclassic()
			love.timer.sleep( 0.3 )--]]
			
			pb:selectFish1()
			love.timer.sleep( 0.3 )
	elseif key == '2' then
			pb:selectFish2()
			love.timer.sleep( 0.3 )
		--The second conditional checks if the '3' key is pressed it toggles the variable threeD.
	elseif key == '3' and threeD==true then
			turnmessagesoff()
			threedbuttonpressed=true
			threeD=false --createcanvas()
	elseif key == '3' and threeD==false then
		if threedassetsloaded==false then 
			require ("game/levels/3dlevel")		--3DreamEngine graphics
			load3dassets()
			threedassetsloaded=true
		end
			turnmessagesoff()
			threedbuttonpressed=true
			threeD=true
		--The fourth conditional checks if the '4' key is pressed and turns diorama on if conditions apply.
	elseif key == '4' and screensaver==false then
		if threedassetsloaded==false then 
			require ("game/levels/3dlevel")		--3DreamEngine graphics
			load3dassets()
			threedassetsloaded=true
		end
		turnmessagesoff()
		dioramabuttonpressed=true
		screensaver=true
		shader2=true
		threeD=true
	elseif key == '4' and screensaver==true then
		turnmessagesoff()
		dioramabuttonpressed=true
		screensaver=false
		shader2=false
		threeD=false
		-- toggles touch interface on / off
	elseif key == "5" and touchinterfaceison==false then
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
		turnmessagesoff()
		touchbuttonpressed=true
		touchinterfaceison=true
	elseif key == "5" and touchinterfaceison==true then
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
		turnmessagesoff()
		touchbuttonpressed=true
		touchinterfaceison=false
	--debug keys
	-- togles debug mode on / off
	elseif key == '6' and debugmode=="yes" then
		turnmessagesoff()
		debugbuttonpressed=true
		shader2=true
		debugmode="no"
	elseif key == '6' and debugmode=="no" then
		turnmessagesoff()
		debugbuttonpressed=true
		shader2=false
		debugmode="yes"
	
	-- toggles background on / off
	elseif key == '7' and background=="yes" then
		turnmessagesoff()
		backgroundbuttonpressed=true
		background="no"
	elseif key == '7' and background=="no" then 
		turnmessagesoff()
		backgroundbuttonpressed=true
		background="yes"
	-- toggles objects on / off
	elseif key == '8' and objectscells=="yes" then 
		turnmessagesoff()
		objectsbuttonpressed=true
		objectscells="no"
	elseif key == '8' and objectscells=="no" then 
		turnmessagesoff()
		objectsbuttonpressed=true
		objectscells="yes"
	-- toggles players on / off
	elseif key == '9' and playerscells=="yes" then
		turnmessagesoff()
		playersbuttonpressed=true
		playerscells="no"
	elseif key == '9' and playerscells=="no" then
		turnmessagesoff()
		playersbuttonpressed=true
		playerscells="yes"
	end
	--options
	-- return to the game while the briefcase message video plays with the key "r".
		if key=="r" and gamestatus=="briefcasemessage" then gamestatus="game" end

		--testing socket.http
		--[[if key=="r" and not (gamestatus=="briefcasemessage") then
			isfilefound= readFile("threeD2.txt")
				if isfilefound==nil then print("file not found")
				  -- download file
					local b,c,h = http.request("https://codeberg.org/glitchapp/fish-fillets-remake/raw/commit/6ac9a2e3fff10a26d9552a2dc2e0d130952fb68c/NEWS")
					love.filesystem.write("news.txt", b )
			else print ("file found")
			end
		end
		--]]
	
	-- open the options menu with the key "o"
	if key == "o" and gamestatus=="game" then
	inputtime=0
	if shader2type=="crt" then shader1=false end
		--shader1=false shader2=true
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
		if bfocustab==nil then bfocustab="languages" end -- prevent from not being on a tab the first time you open options
		optionsloadlanguages()
		gamestatus="gameplusoptions" 

		languagehaschanged=false
		skinhaschanged=false
  	
  	-- return to the game from the options menu when pressing "o"
	elseif key == "o" and gamestatus=="options" or gamestatus=="gameplusoptions" then gamestatus="game" love.graphics.setColor(1,1,1)
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
		if caustics==true and not (skin=="retro") then shader2=true end
		--if shader1type=="godsgrid" then shader1=true shader2=false end
		setuppaletteforshaderfilters()
		isloading=false
		
		if languagehaschanged==true then 
			love.audio.stop()
			music:stop()
			loadvoices()
			loadborescript()
			loadlevelmusic()
			loadsubtitles()
			optionsload()		-- reload the values for the languages on the menus
			loadtouchtext()		-- reload the touch interface
			loadavatars()		-- load subtitles avatars
			timer=0
			stepdone=0
			
				if skin=="remake" then 	assignfrgbck()				-- Assign layers
			elseif skin=="classic" then	assignfrgbckclassic()		-- Assign layers
						if skinupscaled==false then	assignsizenormal()	--rescale
					elseif skinupscaled==true then assignsizeupscaled() --rescale
					end
			end
		end
		
		if skinhaschanged==true then skinhaschangedfunction() end
		if shader2type=="crt" then shader1=true end
	
	--repeat level if "backspace" is pressed
	elseif key == 'backspace' then
	mousestate = not love.mouse.isVisible()	-- hide mouse pointer
	love.mouse.setVisible(mousestate)
	repeatleveltriggered=true
	stepdone=0
	changelevel()
	
	elseif key == "p" and gamestatus=="game" and not (secretcodestep==7) and not (secretcodestep==8) then
					if pauseisactive==false or pauseisactive==nil then
						pauseisactive=true
						love.audio.pause( mus )
				elseif pauseisactive then
						pauseisactive=false
						love.audio.play( mus )
					end
	
	
	
	--quit game if "q" is pressed
	elseif key == 'q' then --love.event.quit()
	
	elseif key == 'g' and (gamestatus=="game") then shader2=false gamestatus="instructions"
						in1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/1.webp")))
						in2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/2.webp")))
						in3  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/3.webp")))
						in4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/4.webp")))
						in5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/5.webp")))
						in6 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/6.webp")))
						in7 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/7.webp")))
						prepareInstructions()
						love.mouse.setCursor(cursorRetro)
	
	end
	
	--F keys
				--F1 toggles help info
				if key == 'f1' and helpison=="yes" then helpison="no" isloading=false
			elseif key == 'f1' and helpison=="no" then helpison="yes" isloading=false
			-- load saved game
			elseif key == 'f2' then
				turnmessagesoff()
				loadbuttonpressed=true
				changelevel()
				loadmygame()
			-- save game
			elseif key == 'f3' then
				turnmessagesoff()
				savebuttonpressed=true
				savemygame()
				palette=1
			
			-- toggles music on / off
			elseif key == "f4" and musicison==true then  --turn music off
				turnmessagesoff()
				musicbuttonpressed=true
				musicison=false mus:stop()
				--changelevel() nLevel=nLevel-1 
			elseif key == "f4" and musicison==false then	 --turn music on
				turnmessagesoff()
				musicbuttonpressed=true
				musicison=true mus:play()
				--nLevel=nLevel-1 changelevel() 
				--player:play(true)
					if assets==2 then
							if nLevel==1 then love.audio.stop() love.audio.play( TylerSong3_Normal )
						elseif nLevel==2 then love.audio.stop() love.audio.play( UnderwaterAmbientPadisaiah658 )
						elseif nLevel==3 then love.audio.stop() love.audio.play( pixelspheresong21 )
						elseif nLevel==4 then love.audio.stop() love.audio.play( song18 )
						elseif nLevel==5 then love.audio.stop() love.audio.play( Underwater_Theme_II )
						elseif nLevel==6 then love.audio.stop() love.audio.play( Ambient_Loop_isaiah658 )
						elseif nLevel==7 then love.audio.stop() love.audio.play( Hypnotic_Puzzle )
						elseif nLevel==11 then love.audio.stop() love.audio.play( enchanted_tiki_86 )
						elseif nLevel==14 then love.audio.stop() love.audio.play( lifeWave2k )
						elseif nLevel==16 then love.audio.stop() love.audio.play( Vaporware )
						elseif nLevel==20 then love.audio.stop() love.audio.play( Sirens_in_Darkness )
						elseif nLevel==9 then love.audio.stop() love.audio.play( umplixdeepsea )
						end
					end
			
			-- toggles sound on / off
			elseif key == "f5" and soundon== true then
				soundon=false
				turnmessagesoff()
				soundbuttonpressed=true
				-- nLevel=nLevel-1 changelevel()
			elseif key == "f5" and soundon==false then 
				soundon=true
				turnmessagesoff()
				soundbuttonpressed=true
								
				--nLevel=nLevel-1 changelevel()--turn sounndon and off
				
			-- toggles  subtitles and dubs on / off
			elseif key == "f6" and talkies==true then
				talkies=false
				turnmessagesoff()
				talkiesbuttonpressed=true
				
			elseif key == "f6" and talkies==false then
				Talkies.draw()
				talkies=true
				turnmessagesoff()
				talkiesbuttonpressed=true
			
				
	--shaders
	-- toggles shader2 on / off (shadertoy)
	elseif key == 'f7' and shader2==true then
		turnmessagesoff()
		shadersbuttonpressed=true
		shader2=false caustics=false
		
	elseif key == 'f7' and shader2==false then 
		turnmessagesoff()
		shadersbuttonpressed=true
		shader2=true caustics=true
	-- toggles moonshine shaders on / off
	elseif key == 'f8' and shader1==true then
		turnmessagesoff()
		crtbuttonpressed=true
		shader1=false 
		love.graphics.setBackgroundColor(0,0,0)
	elseif key == 'f8' and shader1==false then
		turnmessagesoff()
		crtbuttonpressed=true
		shader1=true 
		setuppaletteforshaderfilters()			-- function in game/interface/optionsgraphics.lua which set up the palette for every filter
		love.graphics.setBackgroundColor(0,0,0)
		love.graphics.setColor(1,1,1)
	
	-- Switch between remake assets and classic / assets
	elseif key == 'f9' and skin=="remake" then
	if mousestate==true then
		mousestate = not love.mouse.isVisible()	-- hide mouse pointer
		love.mouse.setVisible(mousestate)
	end
		if scalepl>1 then changelevel() end		-- if there's zoom run changelevel function to initialize all values (otherwise all the assets will be out of place)
		turnmessagesoff()
		if not (nLevel==15) then 
			skinbuttonpressed=true 
		else 
			shader2=true
		end
		skin="classic"
		skinhaschanged=true
		skinhaschangedfunction()
	-- Switch between remake assets and classic / assets
	elseif key == 'f9' and skin=="classic" then
	if mousestate==true then
		mousestate = not love.mouse.isVisible()	-- hide mouse pointer
		love.mouse.setVisible(mousestate)
	end
		turnmessagesoff()
		if not (nLevel==14) and not (nLevel==15) and not (nLevel==29) and not (nLevel==34) then
			skinbuttonpressed=true
		else
			shader2=true
		end
		skin="remake"
		skinhaschanged=true
		skinhaschangedfunction()
	
	-- returns to the level selection menu while in game
	elseif key == "f10" and gamestatus=="game" then shader1=false shader2=false screensaver=false
		love.audio.stop()
		fishstop()
		inputtime=0
		--cursor = love.mouse.newCursor("/assets/interface/ichtys_cursor100.png", 0, 0 )	--restore cursor
		--love.mouse.setCursor(cursor)
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
		--music:stop()
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") 
		gamestatus="levelselection"
	elseif key == "f10" and gamestatus=="levelselection" then 
		love.audio.stop()
		--:stop()
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
		gamestatus="game" 
		shader2=true
		nLevel=nLevel-1
		changelevel()
		love.graphics.setColor(1,1,1)
	
	-- toggles screen resolution
	elseif key == "f11" and gamestatus=="game" and res==1366 then -- resolution change
		turnmessagesoff()
		resolutionbuttonpressed=true
			res="1080p" expx=0.5 expy=0.5
			if nLevel==59 or nLevel==60 or nLevel==61 then expx=1 expy=1 end
			loadmultiresolution()
			changeresolution()
			changelevel()
			love.timer.sleep( 0.3 )
	
	elseif key == "f11" and gamestatus=="game" and res=="1080p" then -- resolution change
		turnmessagesoff()
		resolutionbuttonpressed=true
			res=1366 expx=0.5 expy=0.5
			if nLevel==59 or nLevel==60 or nLevel==61 then expx=1 expy=1 end
			loadmultiresolution()
			changeresolution()
			changelevel()
			love.timer.sleep( 0.3 )
			
	elseif key == "f11" and gamestatus=="levelselection" then 
	
	-- toggles palette pressets and switch to retro skin
	elseif key == 'f12' then
		turnmessagesoff()
		retrobuttonpressed=true
		palette=palette+1 shader2=false
			if palette>7 then palette=0 end 
			if palette==1 then shader2=true end
			if palette>7 then palette=0 end
	end

		pb:keypressedMoving (scancode)
		
		--switch fish
		if key == 'space' then 
			pb:switchAgent ()
		--elseif key == 'return' then changelevel()
	       
	      -- Close game
		elseif key == "escape" or key =="q" then
			--love.event.quit()
		end

	-- Switch fish
		if key == 'space' and talkies==false and currentplayer=="1" then currentplayer="2"
	elseif key == 'space' and talkies==false and currentplayer=="2" then currentplayer="1"
	
	elseif shakeDetector and currentplayer=="1" then currentplayer="2"
	elseif shakeDetector and currentplayer=="2" then currentplayer="1"

	
--[[
  -- toggles instructions from the level selection menu by pressing "i"
  elseif key == "i" and gamestatus=="instructions" then instructions="levelselection"
  elseif key == "i" and not (secretcodestep==2) and gamestatus=="levelselection" then shader2=false gamestatus="instructions"
	in1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/1.webp")))
	in2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/2.webp")))
	in3  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/3.webp")))
	in4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/4.webp")))
	in5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/5.webp")))
	in6 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/6.webp")))
	in7 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/7.webp")))
--]]
end
	
	-- if tetris mini game is running check keys to move the tetris pieces.
	elseif tetris==true then	--tetris keys
		if key == 'x' or key=="space" then local testRotation = pieceRotation + 1
			if testRotation > #pieceStructures[pieceType] then testRotation = 1 end
			if canPieceMove(pieceX, pieceY, testRotation) then pieceRotation = testRotation end

		elseif key == 'z' then local testRotation = pieceRotation - 1
			if testRotation < 1 then testRotation = #pieceStructures[pieceType] end

			if canPieceMove(pieceX, pieceY, testRotation) then pieceRotation = testRotation end

    elseif key == 'left' then local testX = pieceX - 1

        if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end

    elseif key == 'right' then local testX = pieceX + 1 
		if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end

    elseif key == 'c' or key =='down' then 
		while canPieceMove(pieceX, pieceY + 1, pieceRotation) do
            pieceY = pieceY + 1
            tetristimer = tetristimerLimit
        end
	elseif key == 'f10' then
	tetris=false
	talkies=true
	gamestatus="levelselection"
    end
	end
	secretcode(key, scancode, isrepeat)	-- secret code to type during level menu selection to unlock everything
end


function turnmessagesoff()
	timer2=0
	musichaschanged=false
	loadbuttonpressed=false
	savebuttonpressed=false
	musicbuttonpressed=false
	soundbuttonpressed=false
	talkiesbuttonpressed=false
	shadersbuttonpressed=false
	crtbuttonpressed=false
	skinbuttonpressed=false
	retrobuttonpressed=false
	upscaledbuttonpressed=false
	threedenginebuttonpressed=false
	dioramabuttonpressed=false
	touchbuttonpressed=false
	debugbuttonpressed=false
	backgroundbuttonpressed=false
	objectsbuttonpressed=false
	playersbuttonpressed=false
	isloading=false
end

--[[
Oh, this function looks innocent enough, but little do you know that it holds the key to unlocking everything! Yes, you heard it right, everything! And there's a secret code to do it!

The code is hidden in plain sight, but you need to have the eyes of a hawk to spot it. You see, if you type the letters "glitchapp" in the level menu selection, something amazing will happen. And if you miss a letter or type them in the wrong order, nothing will happen. So, be careful and make sure you get it right!

And once you've entered the secret code correctly, a magical sound will play, and everything will be unlocked. It's like winning the lottery, but without the money. So, what are you waiting for? Go type in the secret code and unlock the universe!
--]]
function secretcode(key, scancode, isrepeat)		-- secret code to type during level menu selection to unlock everything
	if gamestatus=="levelselection" then
			if key == 'g' and secretcodestep==0 then secretcodestep=1 
		elseif key == 'l' and secretcodestep==1 then secretcodestep=2
		elseif key == 'i' and secretcodestep==2 then secretcodestep=3 
		elseif key == 't' and secretcodestep==3 then secretcodestep=4 
		elseif key == 'c' and secretcodestep==4 then secretcodestep=5 
		elseif key == 'h' and secretcodestep==5 then secretcodestep=6 
		elseif key == 'a' and secretcodestep==6 then secretcodestep=7 
		elseif key == 'p' and secretcodestep==7 then secretcodestep=8 
		elseif key == 'p' and secretcodestep==8 then secretcodestep=9
			LoadSavesound:play()
			unlockeverything()
			secretcodestep=0
		else secretcodestep=0
		end
	end
end


