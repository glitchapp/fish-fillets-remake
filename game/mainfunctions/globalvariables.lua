    
    pauseisactive=false
    
    android=false
    currentfrg=false 	currentfrgcl=false
    currentfrg2=false 	currentfrg2cl=false
    currentbck=false	currentbckcl=false
    currentbck2=false	currentbck2cl=false
    
    fish1status="idle"
    fish2status="idle"
    fish1direction="right"
    fish2direction="right"
    lastagentdirection="right"
    statuestatus="idle"
    
    fish1highlight=false
    fish2highlight=false
    
    linuser1status="idle"
    linuser2status="idle"
    
    activefish=2
    
	-- initialize lip sync variables
    lipstimer=0	
    lipstime=0
	lipsiter=0
	lipsline=1
	lipsitercompleted=false
    
    -- Fish selection aura init values
    mirroraura=-1
    auradisplacement=1
    mirroraura2=1
    auradisplacement2=1
    
    currenttab="languages"
	skinhaschanged=false		
    
   
	musichaschanged=false
	
	--love.keyboard.setKeyRepeat(true)		-- allow key repeat
	
	--button pressed
	loadbuttonpressed=false
	savebuttonpressed=false
	saveconfirmpressed=false
	
	musicbuttonpressed=false
	soundbuttonpressed=false
	talkiesbuttonpressed=false
	shadersbuttonpressed=false
	crtbuttonpressed=false
	skinbuttonpressed=false
	resolutionbuttonpressed=false
	retrobuttonpressed=false
	upscaledbuttonpressed=false
	threeddbuttonpressed=false
	dioramabuttonpressed=false
	touchbuttonpressed=false
	debugbuttonpressed=false
	backgroundbuttonpressed=false
	objectsbuttonpressed=false
	playersbuttonpressed=false


	timer=0
	timer2=0
	xspace=0
	yspace=0
	
	fps=0							-- initialize fps variable
	
	playerplaying=true				-- video player
	moonshineeffect=0				-- moonshine effects on the scene player	(4 set of effects available)
	screenshake=false				-- screen is shaking
    
    diorama=false					--diorama
    screensaver=false				--screen saver
   
    scalepl=1						-- Player assets escalation factor (used for zoom)
    forgit=true

	rate=0
	channels=0
	
	-- events intro
	
	randomvideo=0	-- intro = 0 briefcase message video = 1
	-- events levels
	
	gameoversentencesaid=false			--gameover sentences
	levelcompletedsentencesaid=false	--level completed sentences
	boresentencestriggered=false		-- bore sentences (when no movement for a long time)
	boresentencesinterrupted=false
	inputtime=0							--time counter to trigger the bore scripts
	
	
	--level1
	--skinlevel1loaded=false
	threedassetsloaded=false
	pipetouched=false		-- Pipe on first level
	pipepushed=false		-- Pipe on first level
	chairtouched=false		-- Chair on first level

	--level2
	briefcaseclosed=true	-- self destructive message on level 2
	
	--level3
	shortpipetouched=false	-- Shortpipe on 3rd level
	shortpipepushed=false	-- Shortpipe on 3rd level
	strawberrymarmeladepushed=false --strawberrymarmelade on 3rd level
	longpipehorizontalpushed=false --longpipehorizontal on 3rd level
	booklvl3pushed=false	--booklvl3 on 3rd level
	uarestanding=false
	aproachaxe=false
	savingpositions=false	-- check if the saving positions script has been played

	
	--level 15
	phoneringing=false		-- triggers phone ringing animation
	
	--level30
	crabtouched=false		-- talking crab on level 30
	crabfalled=false		-- talking crab on level 30
	
	--extras
	advertising1playing=false
	advertising2playing=false

	--The following variables are used for the debug menu and hide / show different part of the game
	objectscells="yes"		--Hide / show objects
    background="yes"		--Hide / show background
    adjacentcells="no"		-- hide / show adjacent cells to the fishs
    msgbox=false			-- hide show messagebox
    msgbox2=false			-- hide show messagebox
    vrmode=false			-- hide show vr status
    tedit="no"				-- hide show Text editor
    problemdebug="2"		-- Used to switch between two codes for testing
    livereloaded="no"		-- Used to enable live code for main.lua only

    
    fish1 = '@'
    fish1body= '1'
    fish2 = '0'
    fish2body= '2'
    chair1 = 'a'
    cushion = 'c'
    --table = 'd'
    chair2 = 'b'
    steelbar = 'A'
    wall = '#'
    empty = ' '
    
    --mini games
    tetris=false
    
    --assets mode
    
    assetsmenupage=1
    
    --interface
    panelright=false
    ispaneldown=false
	-- game states
	helpison="no"							-- Show hide help
	
	   
    debugmode="no"							-- Enable debug mode (grid, and extra options for testing)
    extradebug=false						-- write resolution, tile size grid size etc
	--require ("livecode")
	mylivecode=false
   
    playerscells="yes"
    milestones="no"							-- Documentation tool

        
    --if mylivecode then livecodebugbackground="yes" end -- the live code library changes the background, this is a temporary solution till I find the reason

    leveleditor=false						-- Level editor disabled by default
        
    --scheme 1 default
    edgescolor={0,0.8,0}
    cs1tile1={0.5,0.5,0.5}
    cs1tile2={0.6, 0.58, 0.2}
	cs1border1={0.7, 0.68, 0.3}
	cs1border2={0.8, 0.78, 0.4}
	cs1agent1={1,1,1}
    cs1agent2={0.5,0.5,0.5}
    cs1block={1,1,1}
    cs1bck={0.146, 0.73, 0.73}
    
    --scheme 2 Commodore 64
    
    -- nice colors are
			cs2agent1={.9,.9,.8}
			cs2agent2={0.4,0.4,0.3}
    
    cs2tile1={0.36, 0.67, 0.36}
    cs2tile2={0.6,0.88,0.60}
    cs2border1={0.63, .40, 0.23}
	cs2border2={0.42, 0.32, 0.07}
    --cs2agent1={0.313,0.270,0.607}
    --cs2agent2={0.623,0.305,0.266}
    cs2block={0.788,0.831,0.529}
    cs2bck={0.415,0.749,0.776}
    
    --scheme 3 Nintendo
    
    cs3tile1={0.972,0.721,0}
    cs3tile2={0.674,0.486,0}
    cs3border1={0,0.721,0}
	cs3border2={0,0.658,0}
    cs3agent1={0,0.345,0.972}
    cs3agent2={0.972,0.219,0}
    cs3block={0.847,0.972,0.47}
    cs3bck={0,0.909,0.847}

    --scheme 4 zx spectrum
    
    cs4tile1={0,1,0}
    cs4tile2={0, 0.8, 0}
    cs4border1={0, 1, 1}
	cs4border2={0,0.8,0.8}
    cs4agent1={1,1,1}
    cs4agent2={1,1,1}
    cs4block={1,1,0}
    cs4bck={0,0,0.8}
    
    --scheme 5 Mega drive
    
    cs5tile1={0.023,0.470,0.149}
    cs5tile2={0,0.282,0.286}
    cs5border1={0.321,1,0}
	cs5border2={0,0.788,0.031}
    cs5agent1={0,0,1}
    cs5agent2={0.988,0.415,0}
    cs5block={0.611,0.047,0.611}
    cs5bck={0.403,0.803,0.988}
    
        --scheme 6 high contrast
    
    cs6tile1={1,0.5,0.5}
    cs6tile2={1, 0.58, 0.2}
    cs6border1={1, 0.68, 0.3}
	cs6border2={0.8, 0.78, 0.4}
    cs6agent1={1,0,0}
    cs6agent2={0,0,1}
    cs6block={0,0.5,1}
    cs6bck={0.146, 0.73, 0.73}
    
    --scheme 7 Tron
    
    cs7tile1={0.023,0.470,0.149}
    cs7tile2={0,0.282,0.286}
    cs7border1={0.321,1,0}
	cs7border2={0,0.788,0.031}
    cs7agent1={0,0,1}
    cs7agent2={0.988,0.415,0}
    cs7block={0.611,0.047,0.611}
    cs7bck={0,0,0}
    
     --resolutions
    
    dividx=0
    dividy=0
    --res="720p"
    --res="1080p"	

    --res="4k"
    --expx=0.5 expy=0.5
    --res="4k"
    --res="8k"

secretcodestep=0

