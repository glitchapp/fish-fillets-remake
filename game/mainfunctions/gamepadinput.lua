function loadcontrollermappings()
	if love.filesystem.getInfo("lib/GamepadMappings/gamecontrollerdb.txt") then
		love.joystick.loadGamepadMappings("lib/GamepadMappings/gamecontrollerdb.txt")
	end
	lastgamepadtouchedlevel="none"	-- this allows the gamepad to vibrate in first init (check line 68 of game/mainfunctions/gamepad/gamepadRadialMenu.lua where the condition is checked)
	
	movementTimer = 0
	movementDelay = 0.1  -- Adjust this value to control movement speed (in seconds)

	axisMouseAcceleration=10		-- acceleration value if thumbstick is moved

	loadGamePadFiles()
	
	
end

	-- Initialize variables for the updateButtonHeld(dt) function
	local buttonAHeldTime = 0
	local vibrationDelay = 0.75 -- Delay in seconds before vibration starts
	local lastVibrationTime = -vibrationDelay
	local vibrationInterval = 0.5 -- Vibration interval in seconds

function updateButtonHeld(dt)
    if joystick and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
		if joystick:isGamepadDown("a") then
			buttonAHeldTime = buttonAHeldTime + dt

			-- Check if it's time to start vibrating
			if buttonAHeldTime >= vibrationDelay then
				-- Check if it's time to vibrate
				if love.timer.getTime() - lastVibrationTime >= vibrationInterval then
					joystick:setVibration(0.2, 0.2, 0.1)
					lastVibrationTime = love.timer.getTime()
				end
			end
		else
			buttonAHeldTime = 0
		end
	
		if buttonAHeldTime > 3 and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
			print("Saving changes")
			savemygame()
			languagehaschanged = false
			musichaschanged = false
			skinhaschanged = false
			optionsmenupressed = false
			if gamestatus=="gameplusoptions" then palette=1 end
			love.timer.sleep(0.3)
		end
    end

end


function drawArcButton()

    if joystick and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
    
    			love.graphics.setFont( poorfish)
				love.graphics.print ("Hold ", 350,yspace*20)
				love.graphics.draw (Abutton, 500,yspace*20,0,0.5)
				love.graphics.print ("to save changes ", 650,yspace*20)
				love.graphics.setFont( poorfishsmall)
    
        local x, y = 500+55,yspace*20+55
        local radius = 55
        local startAngle = 0
        local endAngle = math.min(buttonAHeldTime, 3) / 3 * (2 * math.pi) -- Convert time to angle
        local segments = 100 -- Increase for smoother arc

        -- Draw the arc
        love.graphics.setColor(1, 0, 0) -- Red color
        love.graphics.arc("line", x, y, radius, startAngle, endAngle, segments)

        love.graphics.setColor(1, 1, 1) -- Reset color
    end
end



--[[
-- Function to check if a button has been released since the last frame
function isButtonReleased(joystick, button)
    local joystickID = joystick:getID()
    if prevButtonState[joystickID] and not joystick:isGamepadDown(button) then
        return true
    end
    return false
end

-- Function to update if a button has been released since the last frame
function updateButtonReleased(dt)
 for _, joystick in ipairs(love.joystick.getJoysticks()) do
        if isButtonReleased(joystick, "a") then
            print("A button released on joystick #" .. joystick:getID())
        end

        -- Update the previous button state
        prevButtonState[joystick:getID()] = joystick:isGamepadDown("a")
    end
end
--]]
function loadGamePadFiles()

require ('/game/mainfunctions/gamepad/gamepadZoom')					-- gamepad zoom functionality (thumbstick 2)
	require ('/game/mainfunctions/gamepad/gamepadRadialMenu')			-- gamepad radial menu (thumbstick 1)
	require ('/game/mainfunctions/gamepad/gamepadTetris')				-- gamepad input for the tetris mini game
	--love.filesystem.setIdentity("screenshot_id")					-- initialize identity for screenshot capture functionality
	--save_dir = love.filesystem.getSaveDirectory( )					-- assign directory path to save_dir
	require ('/game/mainfunctions/gamepad/gamepadGame')  				-- gamepad input on game screen
	require ('/game/mainfunctions/gamepad/gamepadIntro')  				-- gamepad input on intro screen
	require ('/game/mainfunctions/gamepad/gamepadLevelSelection')  		-- gamepad input on level selection
	require ('/game/mainfunctions/gamepad/gamepadLevelEditor')  		-- gamepad input on level editor
	require ('/game/mainfunctions/gamepad/gamepadLevelCompleted')  		-- gamepad input on level completed
	require ('/game/mainfunctions/gamepad/gamepadBriefcase')  			-- gamepad input on briefcase message
	require ('/game/mainfunctions/gamepad/gamepadOptions')  			-- gamepad input on options
	require ('/game/mainfunctions/gamepad/gamepadOptionsLanguages')		-- gamepad input on options - Languages tab
	require ('/game/mainfunctions/gamepad/gamepadGraphics')				-- gamepad input on Graphics menu
	require ('/game/mainfunctions/gamepad/gamepadAudio')				-- gamepad input on Audio menu
	require ('/game/mainfunctions/gamepad/gamepadControls')				-- gamepad input on controls menu
	require ('/game/mainfunctions/gamepad/gamepadPlusOptions')  		-- gamepad input on options
	require ('/game/mainfunctions/gamepad/gamepadExtras')  				-- gamepad input on Extras
	require ('/game/mainfunctions/gamepad/gamepadEnds')  				-- gamepad input on Ends
	require ('/game/mainfunctions/gamepad/gamepadCredits')  			-- gamepad input on Credits
	require ('/game/mainfunctions/gamepad/gamepadMusic')  				-- gamepad input on Music
	require ('/game/mainfunctions/gamepad/gamepadGameOver') 			-- gamepad input on Game over screen
	joysticks = love.joystick.getJoysticks()
	joystick = joysticks[1]

end

-- Function to update joystick axis and control emulated mouse movement
function updategetjoystickaxis()

	 -- Check if a joystick is connected
	 if joystick ~= nil then
          -- Get values of left joystick axes
        leftxaxis = joystick:getGamepadAxis("leftx")
        leftyaxis = joystick:getGamepadAxis("lefty")
          
          -- Get values of right joystick axes
        rightxaxis = joystick:getGamepadAxis("rightx")
        rightyaxis = joystick:getGamepadAxis("righty")
        
          -- Get values of triggers
        tleft = joystick:getGamepadAxis('triggerleft')
        tright = joystick:getGamepadAxis('triggerright')

          -- Check if joystick is moved enough to register
        if leftxaxis>0.2 or leftxaxis<-0.2 then 
			if not (radialmenu==true) and not (zoomtriggered==true) then
					if leftxaxis>0.2 and emulatedmousex>screenwidth/1.4 then -- do nothing
				elseif leftxaxis<-0.2 and emulatedmousex<100 then -- do nothing
				else
					
					if leftxaxis>0.6 or leftxaxis<-0.6 then
						axisMouseAcceleration=axisMouseAcceleration+0.2
					else
						axisMouseAcceleration=10
					end
					
						emulatedmousex=emulatedmousex+leftxaxis*axisMouseAcceleration
						--emulatedmousex=emulatedmousex+leftxaxis*10
					
					
					if not ((bfocustab=="graphics") and not (gamestatus=="levelselection")) then
						bfocus="axis"
					end
				end
			end
			
			-- Ensure the axis controller pointer is shown after being touched
			if axistouchedonce==nil and not (gamestatus=="game") then
				axistouchedonce=true
				
					-- hide the mouse pointer the first time you use the axis controller pointer
					mousestate = not love.mouse.isVisible()
					love.mouse.setVisible(mousestate)
					love.timer.sleep( 0.2 )
				
			end
		end
		if leftyaxis>0.2 or leftyaxis<-0.2 then
		-- Update emulated mouse y-coordinate
			if not (radialmenu==true) and not (zoomtriggered==true) then
					if leftyaxis>0.2 and emulatedmousey>screenheight/1.1 then -- do nothing
				elseif leftyaxis<-0.2 and emulatedmousey<75 then -- do nothing
				else
				
					if leftyaxis>0.6 or leftyaxis<-0.6 then
						axisMouseAcceleration=axisMouseAcceleration+0.2
					else
						if not (leftxaxis>0.6 or leftxaxis<-0.6) then
							axisMouseAcceleration=10
						end
						--emulatedmousey=emulatedmousey+leftyaxis*10
					end
					
					emulatedmousey=emulatedmousey+leftyaxis*axisMouseAcceleration
					
					if not ((bfocustab=="graphics") and not (gamestatus=="levelselection")) then
						bfocus="axis"
					end
				end
			end
			inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		end
 					
 
		-- activate zoom if right trigger is pressed
		if tright>0.2 then
			zoomtriggered=true
	elseif tright<0.2 then
			zoomtriggered=false
	end
     if gamestatus=="game" then
		if zoomtriggered==false then
			camerawithgamepad()		-- move camera with second thumbstick
		end
		if zoomtriggered==true then
			zoomwithgamepad()
		end
    end
    end
    if joystick ~= nil then	--prevents game from crashing because no joystick is connected
		triggerRadialMenuLeftTrigger()
	end
end



function triggerRadialMenuLeftTrigger()
   
    -- activate radial menu if left trigger is pressed
		-- if trigger is pressed beyond 0.2 then pop up the radial menu
			if tleft>0.4 then
					if radialmenu==false then radialmenu=true
					end
					
	-- if trigger is released then hide the radial menu and select hovered option
		elseif tleft>0.05 and tleft<0.4 then
			if radialmenu==true then
					if radialfocus=="close" then radialmenu=false
						love.timer.sleep( 0.3 )
				elseif radialfocus=="levelmenu" then 	-- return to level selection menu

					if mus then mus:stop() end
					if music then music:stop() end
					if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
						dx = 0
						dy = 0
						shader2=false
						timer2=0 inputtime=0
						bfocus="none"
						mousestate = not love.mouse.isVisible()	-- show mouse pointer
						love.mouse.setVisible(mousestate)
						gamestatus="levelselection"
						love.timer.sleep( 0.3 )
						radialmenu=false
				elseif radialfocus=="music" then 
					radialmenu=false
					gamestatus="music" 
				elseif radialfocus=="options" then	
					radialmenu=false
					inputtime=0
					languagehaschanged=false
					skinhaschanged=false
					bfocustab="languages"
					mousestate = not love.mouse.isVisible()	-- show mouse pointer
					love.mouse.setVisible(mousestate)
						if gamestatus=="game" then gamestatus="gameplusoptions"
					elseif gamestatus=="levelselection" then gamestatus="options"
					end
					optionsloadlanguages()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="restart" then --repeat level
					radialmenu=false
					mousestate = not love.mouse.isVisible()	-- hide mouse pointer
					love.mouse.setVisible(mousestate)
					repeatleveltriggered=true
					changelevel()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="return" then --return
						if gamestatus=="gameplusoptions" then gamestatus="game"
					elseif gamestatus=="options" then gamestatus="levelselection"
					end
				elseif radialfocus=="load" then --load game
					radialmenu=false
					turnmessagesoff()
					loadbuttonpressed=true
					changelevel()
					loadmygame()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="save" then --save game
					radialmenu=false
					turnmessagesoff()
					savebuttonpressed=true
					savemygame()
					palette=1
					love.timer.sleep( 0.1 )
				end
					radialmenu=false
			end
		end
end



-- Function to handle confirmation of exiting
function confirmexit()
					if bfocus=="exit" or bfocus=="extras" or bfocus=="ends" or bfocus=="save" or bfocus=="reset" or bfocus=="options" or bfocus=="credits" or bfocus=="music" or bfocus=="axis" then 
							bfocus="confirmexit"
							exitbuttonpressed=true
							exitconfirmpressed=false
							timer2=0
							inputtime=0
							
							-- Apply video effect
							effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
							effect.enable("boxblur")
							love.timer.sleep( 0.2 )	
			
					elseif bfocus=="confirmexit" then 
						exitconfirmpressed=true
						timer2=0
						inputtime=0
						bfocus="none"
						love.timer.sleep( 0.4 )	
					elseif bfocus=="rejectexit" then 
						exitbuttonpressed=false
						timer2=0
						inputtime=0
						exitconfirmpressed=false
						bfocus="none"
						love.timer.sleep( 0.3 )	
					end
end

function isjoystickbeingpressed(joystick,button)

	if joystick and joystick:isGamepadDown("a") then
		return true
		else
		return false
	end

end

-- Callback function when a gamepad button is pressed
function love.gamepadpressed(joystick,button)
  -- Handle sounds based on game status and button pressed
		--[[if not (gamestatus=="game") and not (gamestatus=="tetris") then
		--gamepad sounds
			if not (joystick:isGamepadDown("a")) and not (gamestatus=="game") then
				misc_menu_4:stop()
				misc_menu_4:play()
			elseif joystick:isGamepadDown("a") and not (gamestatus=="game") then
				gui28:stop()
				gui28:play()
			end
		end--]]


	  -- Handle different gamepad inputs for various screens
	if not (fish1status=="dead") and not (fish2status=="dead") then
		gamepadGame(joystick,button)				-- gamepad input on game screen
	end
	gamepadTetris(joystick,button)				-- gamepad input for the tetris mini game
	gamepadIntro(joystick,button)				-- gamepad input on intro screen
	gamepadLevelSelection(joystick,button)		-- gamepad input on level selection
	gamepadLevelCompleted(joystick,button)		-- gamepad input on level selection
	gamepadBriefcase(joystick,button)			-- gamepad input on briefcase message
	gamepadOptions(joystick,button)				-- gamepad input on options
	gamepadOptionsLanguages(joystick,button)
	gamepadGraphics(joystick,button)			-- gamepad input on graphics
	gamepadAudio(joystick,button)
	gamepadControls(joystick,button)
	--gamepadPlusoptions(joystick,button)			-- gamepad input on options (transparent window over game screen)
	gamepadExtras(joystick,button)				-- gamepad input on extras
	gamepadEnds(joystick,button)				-- gamepad input on Ends
	gamepadCredits(joystick,button)				-- gamepad input on Credits
	gamepadMusic(joystick,button)				-- gamepad input on Credits
	gamepadGameOver(joystick,button)			-- gamepad input on Game Over screen
	gamepadLevelEditor(joystick,button)			-- gamepad input on level editor
	
	
end


-- Function to return to level selection screen
function returntolevelselection()

							if soundon==true then
								TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
							end
							
							inputtime=0
							shader2=false
							love.audio.stop()
							love.timer.sleep( 0.3 )
							loadingprocess=false
							levelload()
							if languagehaschanged==true then
								loadborescript()	 -- Load bore dubs if language has changed
							 end
							if languagehaschanged or musichaschanged or skinhaschanged then
								optionsmenupressed=true
								bfocus="confirmsave"
							else
								bfocus="exit"
								gamestatus="levelselection"
							end
end

-- Function to draw the emulated mouse cursor
function drawemulatedmouse()
	
		love.graphics.setColor(0,1,1,1)
		love.graphics.draw(ichtys_cursor100,emulatedmousex,emulatedmousey,0,1,1)
		love.graphics.setColor(1,1,1,1)
		if timer<3 then
			love.graphics.draw(xboxgamepad1,emulatedmousex+100,emulatedmousey+100,0,0.1,0.1)
		end
	
	
end

