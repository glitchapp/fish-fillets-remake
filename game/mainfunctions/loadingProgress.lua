-- Table to store asset loading progress
local loadingProgress = {}
progress=0

-- Function to get the total number of assets to load for a level
function getTotalAssetsToLoad(level)
    -- Modify this function based on the assets you are loading for each level
    local totalAssets

    if level == 1 then
        totalAssets = 28  -- Update with the actual number of assets for Level 1
    elseif level == 2 then
        totalAssets = 6  -- Update with the actual number of assets for Level 2
    -- Add more conditions for other levels if needed
    else
        totalAssets = 0  -- Default case: no assets
    end

    return totalAssets
end


-- Function to update the loading bar
function updateLoadingBar(loadedAssetsCount, totalAssetsToLoad)
    progress = loadedAssetsCount / totalAssetsToLoad
    loadingProgress[nLevel] = progress
    -- Use the progress value to update your loading bar GUI or display
    --print("Loading Progress for Level " .. nLevel .. ": " .. progress * 100 .. "%")
    
    return loadedAssetsCount
end


-- Function to reset the loading bar
function resetLoadingBar()
    loadingProgress[nLevel] = nil
    -- Add any additional reset logic you need
end

function drawLoadingProgress()
	love.graphics.print("Loading Progress for Level " .. nLevel .. ": " .. progress * 100 .. "%",love.graphics.getWidth()/3.3, love.graphics.getHeight()/1.9)
end
