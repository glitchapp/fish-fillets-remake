function savegameload()

 Acceptsound = love.audio.newSource( "/externalassets/sounds/load-save/Accept.ogg","stream" )
 LoadSavesound = love.audio.newSource( "/externalassets/sounds/load-save/LoadSave.ogg","stream" )
 
	require ('game/mainfunctions/createZip')

--buttons

	profile1Button = {
		text = "profile1",
		x = 1000,
		y = 10, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

playerProgress = {
	[1] = "first", -- index is number, value is string
	playerName = "GlitchApp", -- index is string, value is string
	outlineColor = {0, 0.5, 1}, -- index is string, value is list of values
	gameFinished = false, -- index is string, value is boolean
	
	--extra menus
	leveleditorunlocked=true,
	extrasunlocked=true,
	musicplayerunlocked=true,
	
	--unlocked music
	classictracksunlocked=true,
	--pixelsphere
	pixelsphereunlocked=true,
	enchanted_song=true,
	aquaria_song=true,
	sirens_song=true,
	song18_song=true,
	song21_song=true,
	vapor_song=true,
	calmrelax_song=true,
	ambientI_song=true,
	ambientII_song=true,
	ambientIII_song=true,
	anotheraugust_song=true,
	sevenandeight_song=true,
	novemembersnow_song=true,
	icyrealm_song=true,
	happylullaby_song=true,
	thehex_song=true,

	--isaiah658
	isaiah658unlocked=true,
	ambient_song=true,
	underwater2_song=true,
	
	--umplix
	umplixunlocked=true,
	deepsea_song=true,
	sinkingfeeling_song=true,
	
	--ericmatyas
	ericmatyasunlocked=true,
	hyp_song=true,
	dream_song=true,
	islandofmysteries_song=true,
	monkeyislandband_song=true,
	puzzlegame_song=true,
	theyarehere_song=true,
	
	--cleytonkauffman
	cleytonkauffmanunlocked=true,
	underwaterI_song=true,
	underwaterII_song=true,
	
	--marcelofg
	marcelofunlocked=true,
	airy_song=true,

	--springyspringo
	springyspringounlocked=true,
	water_song=true,
	
	--tokyogeisha
	tokyogeishaunlocked=true,
	ambience_song=true,
	ambienceII_song=true,
	creep_song=true,

	--poinl
	poinlunlocked=true,
	nautilus_song=true,

	--hectavex
	hectavexunlocked=true,
	rupture_song=true,
	ova_song=true,
	vanish_song=true,


	--tedkerr
	tedkerrunlocked=true,
	crashedship_song=true,
	scifi_song=true,

	--isao
	isaounlocked=true,
	upbeat_song=true,
	epicdance_song=true,

	--Vikto Kraus
	viktorkrausunlocked=true,
	arobotswaytoheavenunlocked=true,

	--levels unlocked
	--0.Fishhouse
true,--1
true,--2
true,--3
true,--4
true,--5
true,--6
true,--7
true,--8

--1.Ship Wrecks
true,--9
true,--10
true,--11
true,--12
true,--13
true,--14
true,--15
true,--16
true,--17
true,--18
true,--19

--3. City in the deep
true,--20
true,--21
true,--22
true,--23
true,--24
true,--25
true,--26
true,--27
true,--28
true,--29

--5. Coral reef
true,--30
true,--31
true,--32
true,--33
true,--34
true,--35
true,--36
true,--37

--7. Dump
true,--38
true,--39
true,--40
true,--41
true,--42
true,--43
true,--44

--2.Silver's ship
true,--45
true,--46
true,--47
true,--48
true,--49
true,--50
true,--51

--4.UFO
true,--52
true,--53
true,--54
true,--55
true,--56
true,--57
true,--58

--6. Treasure cave
true,--59
true,--60
true,--61
true,--62
true,--63
true,--64

--8. Secret computer
true,--65
true,--66
true,--67
true,--68
true,--69
true,--70

--9. NG
true,--71
true,--72
true,--73
true,--74
true,--75
true,--76
true,--77
true,--78
true,--79

}


Gameoptions= {
	
	--	sound and music
	soundon=true,
	musicison=true,
	
	--Language
    language="en",
    language2="en",
    accent="br",
    accent2="br",
	
	--shaders
	shader1=false,	--crt effect
    shader2=true,	--caustics effect
    caustics=true,
	talkies=true,	--Subtitles
	palette=1,		-- Color scheme
	threeD=false,	--3d
	touchinterfaceison=false,	--touch interface
	res="1080p",		--resolution
	fullscreen=false,	--full screen mode
   
    adjustr=1,			-- RGB color adjustements
    adjustg=1,
    adjustb=1
}

end



function loadmygame()
	if gamestatus=="game" or gamestatus=="gameplusoptions" then
		pb:loadlevel()	-- function is in path game/logic/ push-blocks.lua
	end
--load settings
    
    -- graphics
     --threeD		  = love.filesystem.read( "threeD.txt")			-- 3d option load from save game deactivated to avoid problems
     if threeD==true then load3dassets() end
     skin		  = love.filesystem.read( "skin.txt")
     skinupscaled = love.filesystem.read( "skinupscaled.txt" )
     palette  	  = love.filesystem.read( "palette.txt" )
     hdr 		  = love.filesystem.read( "hdr.txt" )
     res		  = love.filesystem.read( "res.txt" )
     fullscreen	  = love.filesystem.read( "fullscreen.txt" )
     vsyncstatus  = love.filesystem.read( "vsyncstatus.txt" )
     limitframerate	  = love.filesystem.read( "limitframerate.txt" )
     
     
     -- HUB
     showfps	  = love.filesystem.read( "showfps.txt" )
     showbattery	  = love.filesystem.read( "showbattery.txt" )
     showtimedate     = love.filesystem.read( "showtimedate.txt" )
     showAnalogClock     = love.filesystem.read( "showAnalogClock.txt" )
     
     -- RGB color adjustements
		adjustr	  = love.filesystem.read( "adjustr.txt" )
		adjustg	  = love.filesystem.read( "adjustg.txt" )
		adjustb	  = love.filesystem.read( "adjustb.txt" )
    -- Chromatic aberration
		chromaab  = love.filesystem.read( "chromaab.txt" )
    
    -- advanced graphic effects
     shader1  = love.filesystem.read( "shader1.txt" )
     shader2  = love.filesystem.read( "shader2.txt" )
     caustics  = love.filesystem.read( "caustics.txt" )
    
    --voice dubs
     talkies  = love.filesystem.read( "talkiestxt" )
    
    --language
     language 	= love.filesystem.read( "language.txt" )
     language2  = love.filesystem.read( "language2.txt" )
     accent 	= love.filesystem.read( "accent.txt" )
     accent2 	= love.filesystem.read( "accent2.txt" )
    
    --audio
     soundon 	= love.filesystem.read( "soundon.txt" )
     musicon  	= love.filesystem.read( "musicon.txt" )
     musictype 	= love.filesystem.read( "musictype.txt"  )
    
    --controls
     touchinterfaceison = love.filesystem.read( "touchinterfaceison.txt" )
     vibration 			= love.filesystem.read( "vibration.txt" )
     controllerLayout	= love.filesystem.read( "controllerLayout.txt" )
     if controllerLayout==nil then controllerLayout="default" end	-- prevent controllerLayout from being unasigned
    convertsettingsstringtobooleans()
    convertsettingsstringtonumbers()
    
    -- load game progress
    convertunlockedlevelbooleantostring()
		for i = 1,79,1
			do 
			unlockedlevels[i] = love.filesystem.read( "unlockedlevels".. i ..".txt" )	
		end
        convertunlockedlevelstringtoboolean()
        
        
--extra menus
 leveleditorunlocked = love.filesystem.read( "leveleditorunlocked.txt" )
 extrasunlocked = love.filesystem.read( "extrasunlocked.txt" )
 cutscenesunlocked = love.filesystem.read( "cutscenesunlocked.txt" )
 
--unlocked ends
 endmenuunlocked = love.filesystem.read( "endmenuunlocked.txt" )
 fishhouseunlockedend = love.filesystem.read( "fishhouseunlockedend.txt" )
 shipwrecksunlockedend = love.filesystem.read( "shipwrecksunlockedend.txt" )
 silversshipunlockedend = love.filesystem.read( "silversshipunlockedend.txt" )
 cityinthedeepunlockedend = love.filesystem.read( "cityinthedeepunlockedend.txt" )
 ufounlockedend = love.filesystem.read( "ufounlockedend.txt" )
 coralreefunlockedend = love.filesystem.read( "coralreefunlockedend.txt" )
 treasurecaveunlockedend = love.filesystem.read( "treasurecaveunlockedend.txt" )
 dumpunlockedend = love.filesystem.read( "dumpunlockedend.txt" )
 secretcomputerunlockedend = love.filesystem.read( "secretcomputerunlockedend.txt" )
 nextgenerationunlockedend = love.filesystem.read( "nextgenerationunlockedend.txt" )
 gameendunlockedend = love.filesystem.read( "gameendunlockedend.txt" )
          
--unlocked music
 musicplayerunlocked = love.filesystem.read( "musicplayerunlocked.txt" )

--unlocked music authors and tracks

-- classic tracks
classictracksunlocked = love.filesystem.read( "classictracksunlocked.txt" )
kufrik_song = love.filesystem.read( "kufrik_song.txt" )
classicmenu_song = love.filesystem.read( "classicmenu_song.txt" )
rybky1_song = love.filesystem.read( "rybky1_song.txt" )
rybky2_song = love.filesystem.read( "rybky2_song.txt" )
rybky3_song = love.filesystem.read( "rybky3_song.txt" )
rybky4_song = love.filesystem.read( "rybky4_song.txt" )
rybky5_song = love.filesystem.read( "rybky5_song.txt" )
rybky6_song = love.filesystem.read( "rybky6_song.txt" )
rybky7_song = love.filesystem.read( "rybky7_song.txt" )
rybky8_song = love.filesystem.read( "rybky8_song.txt" )
rybky9_song = love.filesystem.read( "rybky9_song.txt" )
rybky10_song = love.filesystem.read( "rybky10_song.txt" )
rybky11_song = love.filesystem.read( "rybky11_song.txt" )
rybky12_song = love.filesystem.read( "rybky12_song.txt" )
rybky13_song = love.filesystem.read( "rybky13_song.txt" )
rybky14_song = love.filesystem.read( "rybky14_song.txt" )
rybky15_song = love.filesystem.read( "rybky15_song.txt" )

--pixelsphere
 pixelsphereunlocked = love.filesystem.read( "pixelsphereunlocked.txt" )

 enchanted_song = love.filesystem.read( "enchanted_song.txt" )
 aquaria_song = love.filesystem.read( "aquaria_song.txt" )
 sirens_song = love.filesystem.read( "sirens_song.txt" )
 song18_song = love.filesystem.read( "song18_song.txt" )
 song21_song = love.filesystem.read( "song21_song.txt" )
 vapor_song = love.filesystem.read( "vapor_song.txt" )
 calmrelax_song = love.filesystem.read( "calmrelax_song.txt" )
 ambientI_song = love.filesystem.read( "ambientI_song.txt" )
 ambientII_song = love.filesystem.read( "ambientII_song.txt" )
 ambientIII_song = love.filesystem.read( "ambientIII_song.txt" )
 anotheraugust_song = love.filesystem.read( "anotheraugust_song.txt" )
 sevenandeight_song = love.filesystem.read( "sevenandeight_song.txt" )
 novemembersnow_song = love.filesystem.read( "novemembersnow_song.txt" )
 icyrealm_song = love.filesystem.read( "icyrealm_song.txt" )
 happylullaby_song = love.filesystem.read( "happylullaby_song.txt" )
 thehex_song = love.filesystem.read( "thehex_song.txt" )
 
--isaiah658
 isaiah658unlocked = love.filesystem.read( "isaiah658unlocked.txt" )
 ambient_song = love.filesystem.read( "ambient_song.txt" )
 underwater2_song = love.filesystem.read( "underwater2_song.txt" )
--umplix
 umplixunlocked = love.filesystem.read( "umplixunlocked.txt" )
 deepsea_song = love.filesystem.read( "deepsea_song.txt" )
 sinkingfeeling_song = love.filesystem.read( "sinkingfeeling_song.txt"  )
--ericmatyas
 ericmatyasunlocked = love.filesystem.read( "ericmatyasunlocked.txt"  )
 hyp_song = love.filesystem.read( "hyp_song.txt"  )
 dream_song = love.filesystem.read( "dream_song.txt"  )
 islandofmysteries_song = love.filesystem.read( "islandofmysteries_song.txt"  )
 monkeyislandband_song = love.filesystem.read( "monkeyislandband_song.txt"  )
 puzzlegame_song = love.filesystem.read( "puzzlegame_song.txt"  )
 theyarehere_song = love.filesystem.read( "theyarehere_song.txt"  )

--cleytonkauffman
 cleytonkauffmanunlocked = love.filesystem.read( "cleytonkauffmanunlocked.txt" )
 underwaterI_song = love.filesystem.read( "underwaterI_song.txt"  )
 underwaterII_song = love.filesystem.read( "underwaterII_song.txt" )

--marcelof
 marcelofunlocked = love.filesystem.read( "marcelofunlocked.txt" )
 airy_song = love.filesystem.read( "airy_song.txt"  )

--springyspringo
 springyspringounlocked = love.filesystem.read( "springyspringounlocked.txt" )
 water_song = love.filesystem.read( "water_song.txt" )

--tokygeisha
 tokyogeishaunlocked = love.filesystem.read( "tokyogeishaunlocked.txt" )
 ambience_song = love.filesystem.read( "ambience_song.txt" )
 ambienceII_song = love.filesystem.read( "ambienceII_song.txt"  )
 creep_song = love.filesystem.read( "creep_song.txt"  )

--poinl
 poinlunlocked = love.filesystem.read( "poinlunlocked.txt" )
 nautilus_song = love.filesystem.read( "nautilus_song.txt"  )

--hectavex
 hectavexunlocked = love.filesystem.read( "hectavexunlocked.txt" )
 rupture_song = love.filesystem.read( "rupture_song.txt" )
 ova_song = love.filesystem.read( "ova_song.txt"  )
 vanish_song= love.filesystem.read( "vanish_song.txt" )

--tedkerr
 tedkerrunlocked = love.filesystem.read( "tedkerrunlocked.txt" )
 crashedship_song = love.filesystem.read( "crashedship_song.txt"  )
 scifi_song = love.filesystem.read( "scifi_song.txt"  )

--isao
 isaounlocked = love.filesystem.read( "isaounlocked.txt" )
 upbeat_song = love.filesystem.read( "upbeat_song.txt"  )
 epicdance_song = love.filesystem.read( "epicdance_song.txt" )

--viktorkraus

 viktorkrausunlocked = love.filesystem.read( "viktorkrausunlocked.txt" )
 arobotswaytoheavenunlocked = love.filesystem.read( "arobotswaytoheavenunlocked.txt" )

--glitchapp mixes

glitchappmixesunlocked = love.filesystem.read( "glitchappmixesunlocked.txt" )
hexhappylullabyunlocked = love.filesystem.read( "hexhappylullabyunlocked.txt" )
remix1unlocked = love.filesystem.read( "remix1unlocked.txt" )
remix2unlocked = love.filesystem.read( "remix2unlocked.txt" )
remix35unlocked = love.filesystem.read( "remix35unlocked.txt" )
rupturemunlocked = love.filesystem.read( "rupturemunlocked.txt" )
remix4unlocked = love.filesystem.read( "remix4unlocked.txt" )

          convertunlockedmusicstringtoboolean()
     
if talkies==true then shader2=true end		--prevents shader from affecting dialogs

end

function convertsettingsstringtobooleans()
 --convert string to booleans
    if threeD=="true"			then threeD=true 		elseif threeD=="false" 			then threeD=false end
    if skinupscaled=="true" 	then skinupscaled=true 	elseif skinupscaled=="false" 	then skinupscaled=false end
    if hdr=="true" 				then hdr=true 			elseif hdr=="false" 			then hdrend=false end
    if fullscreen=="true" 		then fullscreen=true 	elseif fullscreen=="false"		then fullscreen=false end
    if vsyncstatus=="true" 		then vsyncstatus=true 	elseif vsyncstatus=="false"		then vsyncstatus=false end
    if showfps=="true" 			then showfps=true 		elseif showfps=="false"			then showfps=false end
    if limitframerate=="30" 	then limitframerate=30 	elseif limitframerate=="60"		then limitframerate=60 end
    if limitframerate=="nil" 	then limitframerate=nil	end
    if showbattery=="true" 		then showbattery=true 	elseif showbattery=="false"		then showbattery=false end
    if showtimedate=="true" 	then showtimedate=true 	elseif showtimedate=="false"	then showtimedate=false end
if showAnalogClock=="true"	 then showAnalogClock=true 	elseif showAnalogClock=="false"	then showAnalogClock=false end
    if shader1=="true" 			then shader1=true 		elseif shader1=="false" 		then shader1=false end
    if shader2=="true" 			then shader2=true 		elseif shader2=="false" 		then shader2=false end
    if caustics=="true" 		then caustics=true 		elseif caustics=="false" 		then caustics=false end
    if chromaab=="true"			then chromaab=true		elseif chromaab=="false"		then chromaab=false end
    if talkies=="true" 			then talkies=true 		elseif talkies=="false"  		then talkies=false end
    if soundon=="true" 			then soundon=true 		elseif soundon=="false" 		then soundon=false end
    if musicon=="true" 			then musicon=true 		elseif musicon=="false" 		then musicon=false end
    if touchinterfaceison=="true" then touchinterfaceison=true elseif touchinterfaceison=="false" then touchinterfaceison=false end
    if vibration=="true" 		then vibration=true 	elseif vibration=="false" 		then vibration=false end
end

function convertsettingsstringtonumbers()

adjustr = tonumber(adjustr)
adjustg = tonumber(adjustg)
adjustb = tonumber(adjustb)
palette = tonumber(palette)

end



function convertsettingsbooleanstostrings()
--convert booleans to string
    if threeD 		then threeD="true" else threeD="false" end
    if skinupscaled then skinupscaled="true" else skinupscaled="false" end
    if hdr 			then hdr="true" else hdr="false" end
    if fullscreen 	then fullscreen="true" else fullscreen="false" end
    if vsyncstatus 	then vsyncstatus="true" else vsyncstatus="false" end
    if showfps 		then showfps="true" else showfps="false" end
    if limitframerate==30 		then limitframerate="30" end
    if limitframerate==60 		then limitframerate="60" end
    if limitframerate==nil 		then limitframerate="nil" end
    if showbattery	then showbattery="true" else showbattery="false" end
    if showtimedate	then showtimedate="true" else showtimedate="false" end
    if showAnalogClock	then showAnalogClock="true" else showAnalogClock="false" end
    if shader1		then shader1="true" else shader1="false" end
    if shader2 		then shader2="true" else shader2="false" end
    if caustics 	then caustics="true" else caustics="false" end
    if chromaab 	then chromaab="true" else chromaab="false" end
    if talkies 		then talkies="true" else talkies="false" end
    if soundon 		then soundon="true" else soundon="false" end
    if musicon 		then musicon="true" else musicon="false" end
    if touchinterfaceison then touchinterfaceison="true" else touchinterfaceison="false" end
    if vibration 	then vibration="true" else vibration="false" end
end

function convertunlockedlevelbooleantostring()
	for i = 1,79,1
		do 
			if unlockedlevels[i]==true then unlockedlevels[i]="true"
		elseif unlockedlevels[i]==false then unlockedlevels[i]="false"
		end
	end
end

function convertunlockedlevelstringtoboolean()
	for i = 1,79,1
		do 
			if unlockedlevels[i]=="true" then unlockedlevels[i]=true 
		elseif unlockedlevels[i]=="true" then unlockedlevels[i]=false
		end
	end
end

     
function convertsettingsnumbertostring()
	adjustr = tostring(adjustr)
	adjustg = tostring(adjustg)
	adjustb = tostring(adjustb)
	palette = tostring(palette)
end
function convertunlockedmusicbooleantostring()
     
		if musicplayerunlocked==false 	then musicplayerunlocked="false"
	elseif musicplayerunlocked==true 	then musicplayerunlocked="true"
	end
	
	--extra menus
if leveleditorunlocked==false 		then leveleditorunlocked="false" 		elseif leveleditorunlocked==true 	then leveleditorunlocked="true" end
if extrasunlocked==false		 	then extrasunlocked="false" 			elseif extrasunlocked==true			then extrasunlocked="true" end
if cutscenesunlocked==false		 	then cutscenesunlocked="false" 			elseif cutscenesunlocked==true		then cutscenesunlocked="true" end

--unlocked ends
if endmenuunlocked==false 			then endmenuunlocked="false" 			elseif endmenuunlocked==true then endmenuunlocked="true" end
if fishhouseunlockedend==false	 	then fishhouseunlockedend="false" 		elseif fishhouseunlockedend==true then fishhouseunlockedend="true" end
if shipwrecksunlockedend==false 	then shipwrecksunlockedend="false" 		elseif shipwrecksunlockedend==true then shipwrecksunlockedend="true" end
if silversshipunlockedend==false 	then silversshipunlockedend="false" 	elseif silversshipunlockedend==true then silversshipunlockedend="true" end
if cityinthedeepunlockedend==false 	then cityinthedeepunlockedend="false" 	elseif cityinthedeepunlockedend==true then cityinthedeepunlockedend="true" end
if ufounlockedend==false 			then ufounlockedend="false" 			elseif ufounlockedend==true then ufounlockedend="true" end
if coralreefunlockedend==false 		then coralreefunlockedend="false" 		elseif coralreefunlockedend==true then coralreefunlockedend="true" end
if treasurecaveunlockedend==false 	then treasurecaveunlockedend="false"	elseif treasurecaveunlockedend==true then treasurecaveunlockedend="true" end
if dumpunlockedend==false 			then dumpunlockedend="false" 			elseif dumpunlockedend==true then dumpunlockedend="true" end
if secretcomputerunlockedend==false then secretcomputerunlockedend="false" 	elseif secretcomputerunlockedend==true then secretcomputerunlockedend="true" end
if nextgenerationunlockedend==false then nextgenerationunlockedend="false" 	elseif nextgenerationunlockedend==true then nextgenerationunlockedend="true" end
if gameendunlockedend==false 		then gameendunlockedend="false" 		elseif gameendunlockedend==true then gameendunlockedend="true" end


--classic
if classictracksunlocked==false then classictracksunlocked="false" 	elseif classictracksunlocked==true then classictracksunlocked="true" end
if kufrik_song==false 		then kufrik_song="false" 				elseif kufrik_song==true 			then kufrik_song="true" end
if classicmenu_song==false 	then classicmenu_song="false" 			elseif classicmenu_song==true 		then classicmenu_song="true" end
if rybky1_song==false 		then rybky1_song="false" 				elseif rybky1_song==true 			then rybky1_song="true" end
if rybky2_song==false 		then rybky2_song="false" 				elseif rybky2_song==true 			then rybky2_song="true" end
if rybky3_song==false 		then rybky3_song="false" 				elseif rybky3_song==true 			then rybky3_song="true" end
if rybky4_song==false 		then rybky4_song="false" 				elseif rybky4_song==true	 		then rybky4_song="true" end
if rybky5_song==false 		then rybky5_song="false" 				elseif rybky5_song==true 			then rybky5_song="true" end
if rybky6_song==false 		then rybky6_song="false" 				elseif rybky6_song==true 			then rybky6_song="true" end
if rybky7_song==false 		then rybky7_song="false" 				elseif rybky7_song==true 			then rybky7_song="true" end
if rybky8_song==false 		then rybky8_song="false" 				elseif rybky8_song==true 			then rybky8_song="true" end
if rybky9_song==false 		then rybky9_song="false" 				elseif rybky9_song==true 			then rybky9_song="true" end
if rybky10_song==false 		then rybky10_song="false" 				elseif rybky10_song==true 			then rybky10_song="true" end
if rybky11_song==false 		then rybky11_song="false" 				elseif rybky11_song==true 			then rybky11_song="true" end
if rybky12_song==false 		then rybky12_song="false" 				elseif rybky12_song==true 			then rybky12_song="true" end
if rybky13_song==false 		then rybky13_song="false" 				elseif rybky13_song==true 			then rybky13_song="true" end
if rybky14_song==false 		then rybky14_song="false"	 			elseif rybky14_song==true 			then rybky14_song="true" end
if rybky15_song==false 		then rybky15_song="false" 				elseif rybky15_song==true 			then rybky15_song="true" end


--pixelsphere
if pixelsphereunlocked==false 	then pixelsphereunlocked="false" 	elseif pixelsphereunlocked==true 	then pixelsphereunlocked="true" end
if enchanted_song==false 		then enchanted_song="false" 		elseif enchanted_song==true 		then enchanted_song="true" end
if aquaria_song==false 			then aquaria_song="false" 			elseif aquaria_song==true 			then aquaria_song="true" end
if sirens_song==false 			then sirens_song="false" 			elseif sirens_song==true 			then sirens_song="true" end
if song18_song==false 			then song18_song="false" 			elseif song18_song==true 			then song18_song="true" end
if song21_song==false 			then song21_song="false" 			elseif song21_song==true 			then song21_song="true" end
if vapor_song==false 			then vapor_song="false" 			elseif vapor_song==true 			then vapor_song="true" end
if calmrelax_song==false		then calmrelax_song="false" 		elseif calmrelax_song==true			then calmrelax_song="true" end
if ambientI_song==false 		then ambientI_song="false" 			elseif ambientI_song==true 			then ambientI_song="true" end
if ambientII_song==false 		then ambientII_song="false" 		elseif ambientII_song==true 		then ambientII_song="true" end
if ambientIII_song==false 		then ambientIII_song="false" 		elseif ambientIII_song==true 		then ambientIII_song="true" end
if anotheraugust_song==false 	then anotheraugust_song="false" 	elseif anotheraugust_song==true 	then anotheraugust_song="true" end
if sevenandeight_song==false 	then sevenandeight_song="false" 	elseif sevenandeight_song==true 	then sevenandeight_song="true" end
if novemembersnow_song==false	then novemembersnow_song="false" 	elseif novemembersnow_song==true	then novemembersnow_song="true" end
if icyrealm_song==false 		then icyrealm_song="false" 			elseif icyrealm_song==true 			then icyrealm_song="true" end
if happylullaby_song==false 	then happylullaby_song="false" 		elseif happylullaby_song==true 		then happylullaby_song="true" end
if thehex_song==false 			then thehex_song="false" 			elseif thehex_song==true 			then thehex_song="true" end

--isaiah658
if isaiah658unlocked==false		then isaiah658unlocked="false" 		elseif isaiah658unlocked==true		then isaiah658unlocked="true" end
if ambient_song==false			then ambient_song="false" 			elseif ambient_song==true			then ambient_song="true" end
if underwater2_song==false		then underwater2_song="false" 		elseif underwater2_song==true		then underwater2_song="true" end

--umplix
if umplixunlocked==false		then umplixunlocked="false" 		elseif umplixunlocked==true			then umplixunlocked="true" end
if deepsea_song==false			then deepsea_song="false" 			elseif deepsea_song==true			then deepsea_song="true" end
if sinkingfeeling_song==false	then sinkingfeeling_song="false" 	elseif sinkingfeeling_song==true	then sinkingfeeling_song="true" end

--ericmatyas
if ericmatyasunlocked==false	then ericmatyasunlocked="false" 	elseif ericmatyasunlocked==true		then ericmatyasunlocked="true" end
if hyp_song==false				then hyp_song="false" 				elseif hyp_song==true				then hyp_song="true" end
if dream_song==false			then dream_song="false" 			elseif dream_song==true				then dream_song="true" end
if islandofmysteries_song==false then islandofmysteries_song="false" elseif islandofmysteries_song==true then islandofmysteries_song="true" end
if monkeyislandband_song==false	then monkeyislandband_song="false" 	elseif monkeyislandband_song==true	then monkeyislandband_song="true" end
if puzzlegame_song==false		then puzzlegame_song="false" 		elseif puzzlegame_song==true		then puzzlegame_song="true" end
if theyarehere_song==false		then theyarehere_song="false" 		elseif theyarehere_song==true		then theyarehere_song="true" end

--cleytonkauffman
if cleytonkauffmanunlocked==false then cleytonkauffmanunlocked="false" elseif cleytonkauffmanunlocked==true then cleytonkauffmanunlocked="true" end
if underwaterI_song==false		then underwaterI_song="false" 		elseif underwaterI_song==true		then underwaterI_song="true" end
if underwaterII_song==false		then underwaterII_song="false" 		elseif underwaterII_song==true		then underwaterII_song="true" end

--marcelofg
if marcelofunlocked==false		then marcelofunlocked="false" 		elseif marcelofunlocked==true		then marcelofunlocked="true" end
if airy_song==false				then airy_song="false" 				elseif airy_song==true				then airy_song="true" end

--springyspringo
if springyspringounlocked==false then springyspringounlocked="false" elseif springyspringounlocked==true then springyspringounlocked="true" end
if water_song==false			then water_song="false" 			elseif water_song==true				then water_song="true" end

--tokyogeisha
if tokyogeishaunlocked==false	then tokyogeishaunlocked="false" 	elseif tokyogeishaunlocked==true	then tokyogeishaunlocked="true" end
if ambience_song==false			then ambience_song="false" 			elseif ambience_song==true			then ambience_song="true" end
if ambienceII_song==false		then ambienceII_song="false" 		elseif ambienceII_song==true		then ambienceII_song="true" end
if creep_song==false			then creep_song="false" 			elseif creep_song==true				then creep_song="true" end

--poinl
if poinlunlocked==false			then poinlunlocked="false" 			elseif poinlunlocked==true			then poinlunlocked="true" end
if nautilus_song==false			then nautilus_song="false" 			elseif nautilus_song==true			then nautilus_song="true" end

--hectavex
if hectavexunlocked==false		then hectavexunlocked="false" 		elseif hectavexunlocked==true		then hectavexunlocked="true" end
if rupture_song==false			then rupture_song="false" 			elseif rupture_song==true			then rupture_song="true" end
if ova_song==false				then ova_song="false" 				elseif ova_song==true				then ova_song="true" end
if vanish_song==false			then vanish_song="false" 			elseif vanish_song==true			then vanish_song="true" end


--tedkerr
if tedkerrunlocked==false		then tedkerrunlocked="false" 		elseif tedkerrunlocked==true		then tedkerrunlocked="true" end
if crashedship_song==false		then crashedship_song="false" 		elseif crashedship_song==true		then crashedship_song="true" end
if scifi_song==false			then scifi_song="false" 			elseif scifi_song==true				then scifi_song="true" end

--isao
if isaounlocked==false			then isaounlocked="false" 			elseif isaounlocked==true			then isaounlocked="true" end
if upbeat_song==false			then upbeat_song="false" 			elseif upbeat_song==true			then upbeat_song="true" end
if epicdance_song==false		then epicdance_song="false" 		elseif epicdance_song==true			then epicdance_song="true" end

--Vikto Kraus
if viktorkrausunlocked==false	then viktorkrausunlocked="false" 	elseif viktorkrausunlocked==true	then viktorkrausunlocked="true" end
if arobotswaytoheavenunlocked==false then arobotswaytoheavenunlocked="false" elseif arobotswaytoheavenunlocked==true then arobotswaytoheavenunlocked="true" end

--glitchapp
if glitchappmixesunlocked==false then glitchappmixesunlocked="false" elseif glitchappmixesunlocked==true then glitchappmixesunlocked="true" end
if hexhappylullabyunlocked==false then hexhappylullabyunlocked="false" elseif hexhappylullabyunlocked==true then hexhappylullabyunlocked="true" end
if remix1unlocked==false		then remix1unlocked="false" 		elseif remix1unlocked==true			then remix1unlocked="true" end
if remix2unlocked==false		then remix2unlocked="false" 		elseif remix2unlocked==true			then remix2unlocked="true" end
if remix35unlocked==false		then remix35unlocked="false" 		elseif remix35unlocked==true		then remix35unlocked="true" end
if rupturemunlocked==false		then rupturemunlocked="false"		elseif rupturemunlocked==true		then rupturemunlocked="true" end
if remix4unlocked==false		then remix4unlocked="false" 		elseif remix4unlocked==true				then remix4unlocked="true" end

end

function convertunlockedmusicstringtoboolean()
	
		if musicplayerunlocked=="false" 	then musicplayerunlocked=false
	elseif musicplayerunlocked=="true" 		then musicplayerunlocked=true
	end
	
		--extra menus
if leveleditorunlocked=="false" 	then leveleditorunlocked=false 			elseif leveleditorunlocked=="true"	then leveleditorunlocked=true end
if extrasunlocked=="false"			then extrasunlocked=false 				elseif extrasunlocked=="true" 		then extrasunlocked=true end
if cutscenesunlocked=="false"		then cutscenesunlocked=false 			elseif cutscenesunlocked=="true" 		then cutscenesunlocked=true end

--unlocked ends
if endmenuunlocked=="false" 			then endmenuunlocked=false 				elseif endmenuunlocked=="true" 			then endmenuunlocked=true end
if fishhouseunlockedend=="false"	 	then fishhouseunlockedend=false 		elseif fishhouseunlockedend=="true" 	then fishhouseunlockedend=true end
if shipwrecksunlockedend=="false" 		then shipwrecksunlockedend=false 		elseif shipwrecksunlockedend=="true" 	then shipwrecksunlockedend=true end
if silversshipunlockedend=="false" 		then silversshipunlockedend=false 		elseif silversshipunlockedend=="true" 	then silversshipunlockedend=true end
if cityinthedeepunlockedend=="false" 	then cityinthedeepunlockedend=false		elseif cityinthedeepunlockedend=="true" then cityinthedeepunlockedend=true end
if ufounlockedend=="false" 				then ufounlockedend=false 				elseif ufounlockedend=="true" 			then ufounlockedend=true end
if coralreefunlockedend=="false" 		then coralreefunlockedend=false 		elseif coralreefunlockedend=="true" 	then coralreefunlockedend=true end
if treasurecaveunlockedend=="false" 	then treasurecaveunlockedend=false		elseif treasurecaveunlockedend=="true" 	then treasurecaveunlockedend=true end
if dumpunlockedend=="false" 			then dumpunlockedend=false 				elseif dumpunlockedend=="true" 			then dumpunlockedend=true end
if secretcomputerunlockedend=="false" 	then secretcomputerunlockedend=false 	elseif secretcomputerunlockedend=="true" then secretcomputerunlockedend=true end
if nextgenerationunlockedend=="false"	then nextgenerationunlockedend=false 	elseif nextgenerationunlockedend=="true" then nextgenerationunlockedend=true end
if gameendunlockedend=="false" 			then gameendunlockedend=false 			elseif gameendunlockedend=="true" 		then gameendunlockedend=true end


--classic
if classictracksunlocked=="false" then classictracksunlocked=false elseif classictracksunlocked=="true" then classictracksunlocked=true end
if kufrik_song=="false" 		then kufrik_song=false 				elseif kufrik_song=="true" 			then kufrik_song=true end
if classicmenu_song=="false" 	then classicmenu_song=false 		elseif classicmenu_song=="true" 	then classicmenu_song=true end
if rybky1_song=="false" 		then rybky1_song=false 				elseif rybky1_song=="true" 			then rybky1_song=true end
if rybky2_song=="false" 		then rybky2_song=false 				elseif rybky2_song=="true" 			then rybky2_song=true end
if rybky3_song=="false" 		then rybky3_song=false 				elseif rybky3_song=="true" 			then rybky3_song=true end
if rybky4_song=="false" 		then rybky4_song=false 				elseif rybky4_song=="true"	 		then rybky4_song=true end
if rybky5_song=="false" 		then rybky5_song=false 				elseif rybky5_song=="true" 			then rybky5_song=true end
if rybky6_song=="false" 		then rybky6_song=false 				elseif rybky6_song=="true" 			then rybky6_song=true end
if rybky7_song=="false" 		then rybky7_song=false 				elseif rybky7_song=="true" 			then rybky7_song=true end
if rybky8_song=="false" 		then rybky8_song=false 				elseif rybky8_song=="true" 			then rybky8_song=true end
if rybky9_song=="false" 		then rybky9_song=false 				elseif rybky9_song=="true" 			then rybky9_song=true end
if rybky10_song=="false" 		then rybky10_song=false 			elseif rybky10_song=="true" 		then rybky10_song=true end
if rybky11_song=="false" 		then rybky11_song=false 			elseif rybky11_song=="true" 		then rybky11_song=true end
if rybky12_song=="false" 		then rybky12_song=false 			elseif rybky12_song=="true" 		then rybky12_song=true end
if rybky13_song=="false" 		then rybky13_song=false 			elseif rybky13_song=="true" 		then rybky13_song=true end
if rybky14_song=="false" 		then rybky14_song=false 			elseif rybky14_song=="true" 		then rybky14_song=true end
if rybky15_song=="false" 		then rybky15_song=false 			elseif rybky15_song=="true" 		then rybky15_song=true end


--pixelsphere
if pixelsphereunlocked=="false" 	then pixelsphereunlocked=false 	elseif pixelsphereunlocked=="true" 	then pixelsphereunlocked=true end
if enchanted_song=="false" 		then enchanted_song=false 		elseif enchanted_song=="true" 		then enchanted_song=true end
if aquaria_song=="false" 			then aquaria_song=false 		elseif aquaria_song=="true" 		then aquaria_song=true end
if sirens_song=="false" 			then sirens_song=false 			elseif sirens_song=="true" 			then sirens_song=true end
if song18_song=="false" 			then song18_song=false 			elseif song18_song=="true" 			then song18_song=true end
if song21_song=="false" 			then song21_song=false 			elseif song21_song=="true" 			then song21_song=true end
if vapor_song=="false" 			then vapor_song=false 			elseif vapor_song=="true" 			then vapor_song=true end
if calmrelax_song=="false"			then calmrelax_song=false 		elseif calmrelax_song=="true"		then calmrelax_song=true end
if ambientI_song=="false" 			then ambientI_song=false 		elseif ambientI_song=="true" 		then ambientI_song=true end
if ambientII_song=="false" 		then ambientII_song=false 		elseif ambientII_song=="true" 		then ambientII_song=true end
if ambientIII_song=="false" 		then ambientIII_song=false 		elseif ambientIII_song=="true" 		then ambientIII_song=true end
if anotheraugust_song=="false" 	then anotheraugust_song=false 	elseif anotheraugust_song=="true" 	then anotheraugust_song=true end
if sevenandeight_song=="false" 	then sevenandeight_song=false 	elseif sevenandeight_song=="true" 	then sevenandeight_song=true end
if novemembersnow_song=="false"	then novemembersnow_song=false 	elseif novemembersnow_song=="true"	then novemembersnow_song=true end
if icyrealm_song=="false" 			then icyrealm_song=false 		elseif icyrealm_song=="true" 		then icyrealm_song=true end
if happylullaby_song=="false" 		then happylullaby_song=false 	elseif happylullaby_song=="true" 	then happylullaby_song=true end
if thehex_song=="false" 			then thehex_song=false 			elseif thehex_song=="true" 			then thehex_song=true end

--isaiah658
if isaiah658unlocked=="false"		then isaiah658unlocked=false 	elseif isaiah658unlocked=="true"	then isaiah658unlocked=true end
if ambient_song=="false"			then ambient_song=false 		elseif ambient_song=="true"			then ambient_song=true end
if underwater2_song=="false"		then underwater2_song=false 	elseif underwater2_song=="true"		then underwater2_song=true end

--umplix
if umplixunlocked=="false"		then umplixunlocked=false 			elseif umplixunlocked=="true"		then umplixunlocked=true end
if deepsea_song=="false"			then deepsea_song=false 		elseif deepsea_song=="true"			then deepsea_song=true end
if sinkingfeeling_song=="false"	then sinkingfeeling_song=false 	elseif sinkingfeeling_song=="true"	then sinkingfeeling_song=true end

--ericmatyas
if ericmatyasunlocked=="false"	then ericmatyasunlocked=false 		elseif ericmatyasunlocked=="true"		then ericmatyasunlocked=true end
if hyp_song=="false"				then hyp_song=false 			elseif hyp_song=="true"					then hyp_song=true end
if dream_song=="false"			then dream_song=false 				elseif dream_song=="true"				then dream_song=true end
if islandofmysteries_song=="false" then islandofmysteries_song=false elseif islandofmysteries_song=="true" then islandofmysteries_song=true end
if monkeyislandband_song=="false"	then monkeyislandband_song=false elseif monkeyislandband_song=="true"	then monkeyislandband_song=true end
if puzzlegame_song=="false"		then puzzlegame_song=false 		elseif puzzlegame_song=="true"			then puzzlegame_song=true end
if theyarehere_song=="false"		then theyarehere_song=false 	elseif theyarehere_song=="true"			then theyarehere_song=true end

--cleytonkauffman
if cleytonkauffmanunlocked=="false" then cleytonkauffmanunlocked=false elseif cleytonkauffmanunlocked=="true"  then cleytonkauffmanunlocked=true end
if underwaterI_song=="false"		then underwaterI_song=false 		elseif underwaterI_song=="true"			then underwaterI_song=true end
if underwaterII_song=="false"		then underwaterII_song=false 		elseif underwaterII_song=="true"		then underwaterII_song=true end

--marcelofg
if marcelofunlocked=="false"		then marcelofunlocked=false 		elseif marcelofunlocked=="true"			then marcelofunlocked=true end
if airy_song=="false"				then airy_song=false 				elseif airy_song=="true"				then airy_song=true end

--springyspringo
if springyspringounlocked=="false" then springyspringounlocked=false elseif springyspringounlocked=="true" then springyspringounlocked=true end
if water_song=="false"			then water_song=false 			elseif water_song=="true"					then water_song=true end

--tokyogeisha
if tokyogeishaunlocked=="false"	then tokyogeishaunlocked=false 	elseif tokyogeishaunlocked=="true"	then tokyogeishaunlocked=true end
if ambience_song=="false"			then ambience_song=false 		elseif ambience_song=="true"		then ambience_song=true end
if ambienceII_song=="false"		then ambienceII_song=false 		elseif ambienceII_song=="true"		then ambienceII_song=true end
if creep_song=="false"				then creep_song=false 			elseif creep_song=="true"			then creep_song=true end

--poinl
if poinlunlocked=="false"			then poinlunlocked=false 		elseif poinlunlocked=="true"		then poinlunlocked=true end
if nautilus_song=="false"			then nautilus_song=false 		elseif nautilus_song=="true"		then nautilus_song=true end

--hectavex
if hectavexunlocked=="false"		then hectavexunlocked=false 	elseif hectavexunlocked=="true"		then hectavexunlocked=true end
if rupture_song=="false"			then rupture_song=false 		elseif rupture_song=="true"			then rupture_song=true end
if ova_song=="false"				then ova_song=false 			elseif ova_song=="true"				then ova_song=true end
if vanish_song=="false"			then vanish_song=false 			elseif vanish_song=="true"			then vanish_song=true end


--tedkerr
if tedkerrunlocked=="false"		then tedkerrunlocked=false 		elseif tedkerrunlocked=="true"		then tedkerrunlocked=true end
if crashedship_song=="false"		then crashedship_song=false 	elseif crashedship_song=="true"		then crashedship_song=true end
if scifi_song=="false"				then scifi_song=false 			elseif scifi_song=="true"			then scifi_song=true end

--isao
if isaounlocked=="false"			then isaounlocked=false 		elseif isaounlocked=="true"			then isaounlocked=true end
if upbeat_song=="false"			then upbeat_song=false 			elseif upbeat_song=="true"			then upbeat_song=true end
if epicdance_song=="false"			then epicdance_song=false 		elseif epicdance_song=="true"		then epicdance_song=true end

--Vikto Kraus
if viktorkrausunlocked=="false"	then viktorkrausunlocked=false 	elseif viktorkrausunlocked=="true"	then viktorkrausunlocked=true end
if arobotswaytoheavenunlocked=="false" then arobotswaytoheavenunlocked=false elseif arobotswaytoheavenunlocked=="true" then arobotswaytoheavenunlocked=true end

--glitchapp
if glitchappmixesunlocked=="false" then glitchappmixesunlocked=false elseif glitchappmixesunlocked=="true" then glitchappmixesunlocked=true end
if hexhappylullabyunlocked=="false" then hexhappylullabyunlocked=false elseif hexhappylullabyunlocked=="true" then hexhappylullabyunlocked=true end
if remix1unlocked=="false"		then remix1unlocked=false 			elseif remix1unlocked=="true"		then remix1unlocked=true end
if remix2unlocked=="false"		then remix2unlocked=false 			elseif remix2unlocked=="true"		then remix2unlocked=true end
if remix35unlocked=="false"		then remix35unlocked=false 		elseif remix35unlocked=="true"		then remix35unlocked=true end
if rupturemunlocked=="false"		then rupturemunlocked=false		elseif rupturemunlocked=="true"		then rupturemunlocked=true end
if remix4unlocked=="false"		then remix4unlocked=false 			elseif remix4unlocked=="true"		then remix4unlocked=true end
end


function savemygame()
	pb:savelevel()	-- function is in path game/logic/ push-blocks.lua
	--stringlevelblocks = tostring(level)
	--success, message = love.filesystem.write( "level.blocks.txt", level)
    convertsettingsbooleanstostrings()
    convertsettingsnumbertostring()
        
    -- graphics
    success, message = love.filesystem.write( "threeD.txt", threeD)
    success, message = love.filesystem.write( "skin.txt", skin)
    success, message = love.filesystem.write( "skinupscaled.txt",skinupscaled )
    success, message = love.filesystem.write( "hdr.txt", hdr)
    success, message = love.filesystem.write( "palette.txt", palette)
    success, message = love.filesystem.write( "res.txt", res)
    success, message = love.filesystem.write( "fullscreen.txt", fullscreen)
    success, message = love.filesystem.write( "chromaab.txt", chromaab)
    success, message = love.filesystem.write( "vsyncstatus.txt", vsyncstatus)
    success, message = love.filesystem.write( "showfps.txt", showfps)
    success, message = love.filesystem.write( "limitframerate.txt", limitframerate)
    success, message = love.filesystem.write( "showbattery.txt", showbattery)
    success, message = love.filesystem.write( "showtimedate.txt", showtimedate)
    success, message = love.filesystem.write( "showAnalogClock.txt", showAnalogClock)
    
     
    --print(adjustr)

    success, message = love.filesystem.write( "adjustr.txt", adjustr)
    success, message = love.filesystem.write( "adjustg.txt", adjustg)
    success, message = love.filesystem.write( "adjustb.txt", adjustb)
         
    -- advanced graphic effects
    success, message = love.filesystem.write( "shader1.txt",shader1 )
    success, message = love.filesystem.write( "shader2.txt",shader2 )
    success, message = love.filesystem.write( "caustics.txt",caustics )
    
    --voice dubs
    success, message = love.filesystem.write( "talkiestxt",talkies )
    
    --language
    success, message = love.filesystem.write( "language.txt",language )
    success, message = love.filesystem.write( "language2.txt",language2 )
    success, message = love.filesystem.write( "accent.txt",accent )
    success, message = love.filesystem.write( "accent2.txt",accent2 )
    
    --audio
    success, message = love.filesystem.write( "soundon.txt",soundon )
    success, message = love.filesystem.write( "musicon.txt",musicon )
    success, message = love.filesystem.write( "musictype.txt",musictype )
    
    --controls
    success, message = love.filesystem.write( "touchinterfaceison.txt",touchinterfaceison )
    success, message = love.filesystem.write( "vibration.txt",vibration )
    success, message = love.filesystem.write( "controllerLayout.txt",controllerLayout )
    
    convertsettingsstringtobooleans()
    
    --save game progress
    convertunlockedlevelbooleantostring()
		for i = 1,79,1
			do 
			success, message = love.filesystem.write( "unlockedlevels".. i ..".txt",unlockedlevels[i] )	
		end
        convertunlockedlevelstringtoboolean()
        
     --save unlocked music
     --unlocked music
     convertunlockedmusicbooleantostring()
     
     
     --extra menus
     success, errormsg = love.filesystem.write( "leveleditorunlocked.txt", leveleditorunlocked  )
     success, errormsg = love.filesystem.write( "extrasunlocked.txt", extrasunlocked )
     success, errormsg = love.filesystem.write( "cutscenesunlocked.txt", cutscenesunlocked )
 
--unlocked ends
success, errormsg = love.filesystem.write( "endmenuunlocked.txt", endmenuunlocked )
success, errormsg = love.filesystem.write( "fishhouseunlockedend.txt", fishhouseunlockedend )
success, errormsg = love.filesystem.write( "shipwrecksunlockedend.txt", shipwrecksunlockedend )
success, errormsg = love.filesystem.write( "silversshipunlockedend.txt", silversshipunlockedend )
success, errormsg = love.filesystem.write( "silversshipunlockedend.txt", silversshipunlockedend )
success, errormsg = love.filesystem.write( "cityinthedeepunlockedend.txt", cityinthedeepunlockedend )
success, errormsg = love.filesystem.write( "ufounlockedend.txt", ufounlockedend )
success, errormsg = love.filesystem.write( "coralreefunlockedend.txt", coralreefunlockedend )
success, errormsg = love.filesystem.write( "treasurecaveunlockedend.txt", treasurecaveunlockedend )
success, errormsg = love.filesystem.write( "dumpunlockedend.txt", dumpunlockedend )
success, errormsg = love.filesystem.write( "secretcomputerunlockedend.txt", secretcomputerunlockedend )
success, errormsg = love.filesystem.write( "nextgenerationunlockedend.txt", nextgenerationunlockedend )
success, errormsg = love.filesystem.write( "gameendunlockedend.txt", gameendunlockedend )
 
     
--unlocked music
success, errormsg = love.filesystem.write( "musicplayerunlocked.txt", musicplayerunlocked )

--unlocked music authors and tracks

-- classic tracks
success, errormsg = love.filesystem.write( "classictracksunlocked.txt", classictracksunlocked )

success, errormsg = love.filesystem.write( "kufrik_song.txt", kufrik_song )
success, errormsg = love.filesystem.write( "classicmenu_song.txt", classicmenu_song )

success, errormsg = love.filesystem.write( "rybky1_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky2_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky3_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky4_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky5_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky6_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky7_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky8_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky9_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky10_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky11_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky12_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky13_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky14_song.txt", rybky1_song )
success, errormsg = love.filesystem.write( "rybky15_song.txt", rybky1_song )


--pixelsphere
success, errormsg = love.filesystem.write( "pixelsphereunlocked.txt", pixelsphereunlocked )

success, errormsg = love.filesystem.write( "enchanted_song.txt", enchanted_song )
success, errormsg = love.filesystem.write( "aquaria_song.txt", aquaria_song )
success, errormsg = love.filesystem.write( "sirens_song.txt", sirens_song )
success, errormsg = love.filesystem.write( "song18_song.txt", song18_song )
success, errormsg = love.filesystem.write( "song21_song.txt", song21_song )
success, errormsg = love.filesystem.write( "vapor_song.txt", vapor_song )
success, errormsg = love.filesystem.write( "calmrelax_song.txt", calmrelax_song )
success, errormsg = love.filesystem.write( "ambientI_song.txt", ambientI_song )
success, errormsg = love.filesystem.write( "ambientII_song.txt", ambientII_song )
success, errormsg = love.filesystem.write( "ambientIII_song.txt", ambientIII_song )
success, errormsg = love.filesystem.write( "anotheraugust_song.txt", anotheraugust_song )
success, errormsg = love.filesystem.write( "sevenandeight_song.txt", sevenandeight_song )
success, errormsg = love.filesystem.write( "novemembersnow_song.txt", novemembersnow_song )
success, errormsg = love.filesystem.write( "icyrealm_song.txt", icyrealm_song )
success, errormsg = love.filesystem.write( "happylullaby_song.txt",  happylullaby_song )
success, errormsg = love.filesystem.write( "thehex_song.txt", thehex_song )


--isaiah658
success, errormsg = love.filesystem.write( "isaiah658unlocked.txt", isaiah658unlocked )
success, errormsg = love.filesystem.write( "ambient_song.txt", ambient_song )
success, errormsg = love.filesystem.write( "underwater2_song.txt", underwater2_song )
--umplix
success, errormsg = love.filesystem.write( "umplixunlocked.txt", umplixunlocked )
success, errormsg = love.filesystem.write( "deepsea_song.txt",  deepsea_song, all )
success, errormsg = love.filesystem.write( "sinkingfeeling_song.txt",  sinkingfeeling_song )
--ericmatyas
success, errormsg = love.filesystem.write( "ericmatyasunlocked.txt",  ericmatyasunlocked )
success, errormsg = love.filesystem.write( "hyp_song.txt",  hyp_song )
success, errormsg = love.filesystem.write( "dream_song.txt",  dream_song )
success, errormsg = love.filesystem.write( "islandofmysteries_song.txt",  islandofmysteries_song )
success, errormsg = love.filesystem.write( "monkeyislandband_song.txt",  monkeyislandband_song )
success, errormsg = love.filesystem.write( "puzzlegame_song.txt",  puzzlegame_song )
success, errormsg = love.filesystem.write( "theyarehere_song.txt",  theyarehere_song )

--cleytonkauffman
success, errormsg = love.filesystem.write( "cleytonkauffmanunlocked.txt", cleytonkauffmanunlocked )
success, errormsg = love.filesystem.write( "underwaterI_song.txt",  underwaterI_song )
success, errormsg = love.filesystem.write( "underwaterII_song.txt", underwaterII_song )

--marcelof
success, errormsg = love.filesystem.write( "marcelofunlocked.txt", marcelofunlocked )
success, errormsg = love.filesystem.write( "airy_song.txt",  airy_song )

--springyspringo
success, errormsg = love.filesystem.write( "springyspringounlocked.txt", springyspringounlocked )
success, errormsg = love.filesystem.write( "water_song.txt", water_song )

--tokygeisha
success, errormsg = love.filesystem.write( "tokyogeishaunlocked.txt", tokyogeishaunlocked )
success, errormsg = love.filesystem.write( "ambience_song.txt", ambience_song )
success, errormsg = love.filesystem.write( "ambienceII_song.txt",  ambienceII_song )
success, errormsg = love.filesystem.write( "creep_song.txt",  creep_song )

--poinl
success, errormsg = love.filesystem.write( "poinlunlocked.txt", poinlunlocked )
success, errormsg = love.filesystem.write( "nautilus_song.txt",  nautilus_song )

--hectavex
success, errormsg = love.filesystem.write( "hectavexunlocked.txt", hectavexunlocked )
success, errormsg = love.filesystem.write( "rupture_song.txt", rupture_song )
success, errormsg = love.filesystem.write( "ova_song.txt",  ova_song )
success, errormsg = love.filesystem.write( "vanish_song.txt", vanish_song )

--tedkerr
success, errormsg = love.filesystem.write( "tedkerrunlocked.txt", tedkerrunlocked )
success, errormsg = love.filesystem.write( "crashedship_song.txt",  crashedship_song )
success, errormsg = love.filesystem.write( "scifi_song.txt",  scifi_song )

--isao
success, errormsg = love.filesystem.write( "isaounlocked.txt", isaounlocked )
success, errormsg = love.filesystem.write( "upbeat_song.txt",  upbeat_song )
success, errormsg = love.filesystem.write( "epicdance_song.txt", epicdance_song )

--viktorkraus

success, errormsg = love.filesystem.write( "viktorkrausunlocked.txt", viktorkrausunlocked )
success, errormsg = love.filesystem.write( "arobotswaytoheavenunlocked.txt", arobotswaytoheavenunlocked )

--mixes

success, errormsg = love.filesystem.write( "glitchappmixesunlocked.txt", glitchappmixesunlocked )
success, errormsg = love.filesystem.write( "hexhappylullabyunlocked.txt",  hexhappylullabyunlocked )
success, errormsg = love.filesystem.write( "remix1unlocked.txt", remix1unlocked )
success, errormsg = love.filesystem.write( "remix2unlocked.txt", remix2unlocked )
success, errormsg = love.filesystem.write( "remix35unlocked.txt", remix35unlocked )
success, errormsg = love.filesystem.write( "rupturemunlocked.txt", rupturemunlocked )
success, errormsg = love.filesystem.write( "remix4unlocked.txt", remix4unlocked )
     
     convertunlockedmusicstringtoboolean()


    
    	if success then 
			print ('file created')
			--zipMyGame()
	else 
			print ('file not created: ')
	end

Acceptsound:play()

end


--buttons

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
	and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end

local function drawButton (button, hovered,text)

	love.graphics.setFont( button.font )

	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(text,button.x,button.y,button.r,button.sx)
end

function savegamedraw()
	--

	hovered = isButtonHovered (Button)
	drawButton (Button, hovered,"Button")

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			music:stop()
			love.timer.sleep( 0.5 )
		end
	end

end

