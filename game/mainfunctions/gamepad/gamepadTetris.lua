
function gamepadTetris(joystick,button)
	
if gamestatus=="tetris" then

		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		
			if joystick:isGamepadDown("dpleft") then
				local testX = pieceX - 1
				if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end
			elseif joystick:isGamepadDown("dpright") then
			local testX = pieceX + 1
				if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end
			elseif  joystick:isGamepadDown("dpup") then
			
			elseif joystick:isGamepadDown("dpdown") then
				while canPieceMove(pieceX, pieceY + 1, pieceRotation) do
					pieceY = pieceY + 1
					tetristimer = tetristimerLimit
				end

			
		
			end
				
				if joystick:isGamepadDown("a")  then
					local testRotation = pieceRotation + 1
				if testRotation > #pieceStructures[pieceType] then testRotation = 1 end
				if canPieceMove(pieceX, pieceY, testRotation) then pieceRotation = testRotation end
			elseif joystick:isGamepadDown("x")  then
				local testRotation = pieceRotation - 1
				if testRotation < 1 then testRotation = #pieceStructures[pieceType] end
				if canPieceMove(pieceX, pieceY, testRotation) then pieceRotation = testRotation end
			
			elseif joystick:isGamepadDown("y")  then
				local testRotation = pieceRotation - 1
				if testRotation < 1 then testRotation = #pieceStructures[pieceType] end
				if canPieceMove(pieceX, pieceY, testRotation) then pieceRotation = testRotation end
			
			elseif joystick:isGamepadDown("b")  then
				music:stop()
				dx = 0
				dy = 0
				shader2=false
				timer2=0 inputtime=0
				bfocus="none"
				tetris=false
				mousestate = not love.mouse.isVisible()	-- show mouse pointer
				love.mouse.setVisible(mousestate)
				gamestatus="levelselection"
				love.timer.sleep( 0.3 )
			
			end
			
			if joystick:isGamepadDown("start") then
					if pauseisactive==false or pauseisactive==nil then
						pauseisactive=true
						love.audio.pause( mus )
				elseif pauseisactive then
						pauseisactive=false
						love.audio.play( mus )
					end
				
			elseif joystick:isGamepadDown("back")  then
				inputtime=0
				mousestate = not love.mouse.isVisible()	-- show mouse pointer
				love.mouse.setVisible(mousestate)
				tetris=false
				gamestatus="levelselection"
				love.timer.sleep( 0.3 )
			
			end
		    
		end
		
		 if joystick:isGamepad() then
			isgamepad=true
		end
	end
	

end
