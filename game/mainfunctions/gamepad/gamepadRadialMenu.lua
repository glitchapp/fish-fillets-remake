
function updateRadialMenu()
 -- Check if a joystick is connected
	 if joystick ~= nil then

          -- Get values of left joystick axes
        leftxaxis = joystick:getGamepadAxis("leftx")
        leftyaxis = joystick:getGamepadAxis("lefty")

		-- check radial option
			if leftxaxis>-0.2 and leftxaxis<0.2 and leftyaxis>-0.2 and leftyaxis<0.2 then radialfocus="close"	--center
		elseif leftxaxis>-0.4 and leftxaxis<0.4 and leftyaxis<-0.6  then
				radialfocus="levelmenu"	--up icon
		--elseif leftxaxis>-0.4 and leftxaxis<0.4 and leftyaxis<-0.6 and gamestatus=="levelselection" then
				--radialfocus="music"	--up icon
		elseif leftxaxis<-0.6 and leftyaxis<0 then radialfocus="options"	--left icon
		elseif leftxaxis>0.6 and leftyaxis<0 and gamestatus=="game" then radialfocus="restart"		--right icon
		elseif leftxaxis>0.6 and leftyaxis<0 and (gamestatus=="gameplusoptions" or gamestatus=="options" or gamestatus=="music") then
			radialfocus="return"		--right icon
		elseif leftxaxis<-0.6 and leftyaxis>0 then radialfocus="load"		--down left icon
		elseif leftxaxis>0.6 and leftyaxis>0 then radialfocus="save"		--down right icon
		
		else radialfocus="none"
		end
    
    -- check joystick shoulders
			if joystick:isGamepadDown("leftshoulder") then
					if radialfocustab==nil then radialfocustab="mainradialmenu"
				elseif radialfocustab=="languages" then radialfocustab="mainradialmenu"
				end
    	elseif joystick:isGamepadDown("rightshoulder") then
					if radialfocustab==nil then radialfocustab="mainradialmenu"
				elseif radialfocustab=="mainradialmenu" then radialfocustab="languages"
				end
    	end
    
    end
end

function drawLanguagesRadialemnu()



end

function drawradialmenu()
			-- Draw radial background
			love.graphics.setColor(0,0,0,0.4)
			love.graphics.circle("fill", love.graphics.getWidth()/4+50, (love.graphics.getHeight()/2)+100, 300)
			
			
		--[[	
			--left shoulder
			love.graphics.rectangle("fill", love.graphics.getWidth()/4-250, (love.graphics.getHeight()/2)-250, love.graphics.getWidth()/16,love.graphics.getWidth()/16)

			--right shoulder
			love.graphics.rectangle("fill", love.graphics.getWidth()/4+200, (love.graphics.getHeight()/2)-250, love.graphics.getWidth()/16,love.graphics.getWidth()/16)		
			
			love.graphics.setColor(1,1,1,1)

			-- draw radial shoulder tabs selection
			love.graphics.draw (leftshoulder,  love.graphics.getWidth()/4-250, (love.graphics.getHeight()/2)-300, 0, 0.3,0.3)
			love.graphics.draw (rightshoulder,  love.graphics.getWidth()/4+200, (love.graphics.getHeight()/2)-300, 0, 0.3,0.3)
			
			--center of tabs
			love.graphics.draw (helpicon,  love.graphics.getWidth()/4, (love.graphics.getHeight()/2)-30, 0, 0.20,0.20)
--]]		
			love.graphics.setColor(1,1,1,1)
			
			--draw center circle
			love.graphics.draw (closeicon,  love.graphics.getWidth()/3.95, love.graphics.getWidth()/3.3,0,0.1,0.1)	-- center
			
			
									
			
			--draw radial menu depending on the radialfocustab state
			if radialfocustab==nil or radialfocustab=="mainradialmenu" then drawmainradialemnu()
			end
				
				--vibration when hovering an option
				if lastradialfocus==radialfocus then
			elseif not (lastradialfocus==radialfocus) then
					if vibration==true and axistouchedonce and level~=lastgamepadtouchedlevel then
						joystickvibration = joystick:setVibration(0.05, 0.05,0.05)
						lastradialfocus=radialfocus
					end
			end
			
			
			love.graphics.setColor(1,1,1,1)
			--draw axis position in a circle
			axiscircle.x = joystick:getGamepadAxis("leftx") * 100
			axiscircle.y = joystick:getGamepadAxis("lefty") * 100
			love.graphics.circle("line", axiscircle.x+love.graphics.getWidth()/4+50, (axiscircle.y+love.graphics.getHeight()/2)+100, 50)
end

function drawmainradialemnu()
			
		
			--draw center edge circles
				if not (gamestatus=="levelselection") then
					love.graphics.draw (levelcontrol1,  love.graphics.getWidth()/4.2, love.graphics.getWidth()/5,0,0.2,0.2)	-- up
			elseif gamestatus=="levelselection" then
					--love.graphics.draw (musiconicon,  love.graphics.getWidth()/4.2, love.graphics.getWidth()/5,0,0.2,0.2)	-- up
			end
				love.graphics.draw (confcontrol1,  love.graphics.getWidth()/6, love.graphics.getWidth()/4,0,0.2,0.2)	-- left				options
			if not (gamestatus=="levelselection" or gamestatus=="music") then
				love.graphics.draw (restarticon,  love.graphics.getWidth()/3, love.graphics.getWidth()/4,0,0.2,0.2)		-- right			restart
			end
				love.graphics.draw (loadicon,  love.graphics.getWidth()/5.3, love.graphics.getWidth()/3,0,0.15,0.15)		-- down left
				love.graphics.draw (saveicon,  love.graphics.getWidth()/3.1, love.graphics.getWidth()/3,0,0.15,0.15)		-- down right
			
				if radialfocus==nil then radialfocus="none" end	-- prevents the game from crashing when triggering the radial menu on level selection menu
				love.graphics.print(radialfocus,love.graphics.getWidth()/4, love.graphics.getWidth()/2.65,0,1)
			
				-- focus selection circles
				love.graphics.setColor(0.5,0.5,0.5,0.4)
				
				if radialfocus=="close" then love.graphics.circle("fill", love.graphics.getWidth()/3.7, (love.graphics.getHeight()/1.8), 50,50)
			elseif radialfocus=="levelmenu" and not (gamestatus=="levelselection") then love.graphics.circle("fill", love.graphics.getWidth()/3.8, (love.graphics.getHeight()/2.5), 100,100)
			elseif radialfocus=="options" then love.graphics.circle("fill", love.graphics.getWidth()/5.2, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="restart" then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="return" and not gamestatus=="music" then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/2), 100,100)
			elseif radialfocus=="load" then love.graphics.circle("fill", love.graphics.getWidth()/5, (love.graphics.getHeight()/1.5), 100,100)
			elseif radialfocus=="save" then love.graphics.circle("fill", love.graphics.getWidth()/2.8, (love.graphics.getHeight()/1.5), 100,100)
			end

		-- if a is pressed
			if joystick:isGamepadDown("a")  then
					if radialfocus=="close" then radialmenu=false
					love.timer.sleep( 0.3 )
					
				elseif radialfocus=="levelmenu" and not (gamestatus=="levelselection") then 	-- return to level selection menu
					radialmenu=false
					--mus:stop()
					--music:stop()
					love.audio.stop()
					if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
						dx = 0
						dy = 0
						shader2=false
						timer2=0 inputtime=0
						bfocus="none"
						mousestate = not love.mouse.isVisible()	-- show mouse pointer
						love.mouse.setVisible(mousestate)
						gamestatus="levelselection"
						love.timer.sleep( 0.3 )
				elseif radialfocus=="return" then --repeat level
					radialmenu=false
						if gamestatus=="gameplusoptions" then gamestatus="game"
					elseif gamestatus=="options" then gamestatus="levelselection"
					end
					love.timer.sleep( 0.1 )		
				elseif radialfocus=="options" then	
					radialmenu=false
					inputtime=0
					languagehaschanged=false
					skinhaschanged=false
					bfocustab="languages"
					mousestate = not love.mouse.isVisible()	-- show mouse pointer
					love.mouse.setVisible(mousestate)
						if gamestatus=="game" then	gamestatus="gameplusoptions"
					elseif gamestatus=="levelselection" then gamestatus="options"
					end
					optionsloadlanguages()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="restart" then --repeat level
					radialmenu=false
					mousestate = not love.mouse.isVisible()	-- hide mouse pointer
					love.mouse.setVisible(mousestate)
					repeatleveltriggered=true
					changelevel()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="load" then --load game
					radialmenu=false
					turnmessagesoff()
					loadbuttonpressed=true
					changelevel()
					loadmygame()
					love.timer.sleep( 0.1 )
				elseif radialfocus=="save" then --save game
					radialmenu=false
					turnmessagesoff()
					savebuttonpressed=true
					savemygame()
					palette=1
					love.timer.sleep( 0.1 )
				end
				
			elseif joystick:isGamepadDown("b") then
				radialmenu=false
				love.timer.sleep( 0.3 )
			end
			

end
