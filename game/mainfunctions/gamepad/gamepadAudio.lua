-- Function to handle audio settings using a gamepad
function gamepadAudio(joystick,button)

	 -- Check if audio settings are being accessed in options or gameplusoptions mode
	if (gamestatus=="options"  or gamestatus=="gameplusoptions") and bfocustab=="audio" then 
				if joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						
						if bfocus=="confirmsave" then
							savebuttonpressed=false
							saveconfirmpressed=false
							optionsmenupressed=false
							languagehaschanged=false
							timer2=0 inputtime=0
							savemygame()
							gamestatus="options"
							bfocus=nil
							bfocustab="languages"
							love.timer.sleep( 0.5 )	
							
					elseif bfocus=="rejectsave" then 
						savebuttonpressed=false 
						saveconfirmpressed=false 
						optionsmenupressed=false
						languagehaschanged=false
						timer2=0 inputtime=0
							if gamestatus=="options" then 
								gamestatus="levelselection" 
								bfocus="exit"
						elseif 	gamestatus=="gameplusoptions" then
								gamestatus="game" bfocus=nil
						end
						
						love.timer.sleep( 0.5 )	
					end
				end
	end
	
	
	if (gamestatus=="options" or gamestatus=="gameplusoptions") and bfocustab=="audio" then
	
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
				inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
				timer2=0
				
					-- Handle left D-pad button press
					if joystick:isGamepadDown("dpleft") then
						-- Implement button focus changes based on direction and current focus
							if bfocus==nil or bfocus=="axis" then bfocus="sound"
						elseif bfocus=="sound" then
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
						elseif bfocus=="music" then	
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
							
						elseif bfocus=="musicScheme" then
								if bfocus2=="new" then
							elseif bfocus2=="classic" then bfocus2="new"
							end
							
						elseif bfocus=="dialogs" then
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then
							elseif bfocus2=="yes" then bfocus2="yes"
							end
						end	
				-- Handle right D-pad button press
				elseif joystick:isGamepadDown("dpright") then
							 -- Implement button focus changes based on direction and current focus
						if bfocus==nil or bfocus=="axis" then bfocus="sound"
						elseif bfocus=="sound" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
						elseif bfocus=="music" then	
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
							
						elseif bfocus=="musicScheme" then
								if bfocus2=="new" then  bfocus2="classic"
							elseif bfocus2=="classic" then
							end
							
						elseif bfocus=="dialogs" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then bfocus2="off"
							elseif bfocus2=="yes" then
							end
			
						end
				-- Handle up D-pad button press
				elseif  joystick:isGamepadDown("dpup") then
				  -- Implement button focus changes based on direction and current focus
								if bfocus==nil or bfocus=="axis" then bfocus="sound"
							elseif bfocus=="sound" then
							elseif bfocus=="music" then 		bfocus="sound" 			bfocus2="on"
							elseif bfocus=="musicScheme" then 	bfocus="music" 			bfocus2="on"
							elseif bfocus=="dialogs" then		bfocus="musicScheme"	bfocus2="new"
							end
				-- Handle down D-pad button press
				elseif joystick:isGamepadDown("dpdown") then
				 -- Implement button focus changes based on direction and current focus
								if bfocus==nil or bfocus=="axis" then bfocus="sound"
							elseif bfocus=="sound" then 		bfocus="music" 			bfocus2="on"
							elseif bfocus=="music" then 		bfocus="musicScheme"	bfocus2="new"
							elseif bfocus=="musicScheme" then 	bfocus="dialogs"		bfocus2="on"
							end
						
				-- Handle a button press
				elseif joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						love.timer.sleep( 0.3 )	
						-- Handle button focus actions based on the current focus
								if bfocus==nil or bfocus=="axis" then
								bfocustab="audio"
								currenttab="audio"
						elseif bfocus=="sound" then
								if bfocus2=="on" then	soundon=true
							elseif bfocus2=="off" then	soundon=false
							end
						elseif bfocus=="music" then	
								if bfocus2=="on" then 	musicison=true
							elseif bfocus2=="off" then	musicison=false
							end
							
						elseif bfocus=="musicScheme" then
								if bfocus2=="new" then	
										music:stop()
										musictype="new"  musichaschanged=true
										loadlevelmusic()
							elseif bfocus2=="classic" then
										music:stop()
										musictype="classic"  musichaschanged=true
										loadlevelmusicclassic()
							end
							
						elseif bfocus=="dialogs" then
								if bfocus2=="on" then 	talkies=true
							elseif bfocus2=="off" then 	talkies=false
							end
							
						end
			-- Handle b and start button press
				elseif joystick:isGamepadDown("b")  or joystick:isGamepadDown("start") then
					if savebuttonpressed==false then
									if gamestatus=="options" and not (testcontroller==true) then
									returntolevelselection()
								elseif gamestatus=="gameplusoptions" and not (testcontroller==true) then
									mousestate = not love.mouse.isVisible()	-- hide mouse pointer
									love.mouse.setVisible(mousestate)
									gamestatus="game"
								end
					end
				
				
	
				end
		
		end
	end


end


