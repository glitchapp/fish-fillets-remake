-- Handle gamepad input during the briefcase message
function gamepadBriefcase(joystick,button)

if gamestatus=="briefcasemessage" then 
		
		-- Check for directional pad input
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			inputtime=0	-- Reset the counter to prevent triggering the intro animation
				 
				 -- Check for specific button presses
				if joystick:isGamepadDown("a") or joystick:isGamepadDown("b") or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
					
					bfocus="none" -- Clear the current button focus
					gamestatus="game"	-- Change the game status back to "game"
					skipbriefcasevideo(dt)	 -- Function to skip the briefcase video
					love.timer.sleep(0.3)	 -- Pause to avoid quick consecutive button presses
				end
		end
	end
	

end
