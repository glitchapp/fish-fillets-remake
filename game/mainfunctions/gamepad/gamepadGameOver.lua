-- Handle gamepad input during the game over screen
function gamepadGameOver(joystick,button)

if gamestatus=="gameover" then 

	-- Check for directional pad input
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			inputtime=0	 -- Reset the input counter to prevent triggering intro animation
			-- Check for specific button presses
				if joystick:isGamepadDown("a") or joystick:isGamepadDown("b") or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
					
					dx = 0
					dy = 0
					  
					  -- Stop music and play sound effect if sound is enabled
					music:stop()
					if soundon==true then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
					end
					 -- Load underwater music if enabled
					if musicison==true then
						lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
					end
					
					 -- Reset shader and fish statuses
					shader2=false
					fish1status="idle" fish2status="idle"
					
					  -- Reset focus and change game status to level selection
					bfocus="none"
					gamestatus="levelselection"
				end
		end
	end
	

end
