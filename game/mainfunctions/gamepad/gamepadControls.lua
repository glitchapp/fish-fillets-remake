-- Function to handle audio settings using a gamepad
function gamepadControls(joystick,button)

	 -- Check if audio settings are being accessed in options or gameplusoptions mode
	if (gamestatus=="options"  or gamestatus=="gameplusoptions") and bfocustab=="controls" then 
				if joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						
						if bfocus=="confirmsave" then
							savebuttonpressed=false
							saveconfirmpressed=false
							optionsmenupressed=false
							languagehaschanged=false
							timer2=0 inputtime=0
							savemygame()
							gamestatus="options"
							bfocus=nil
							bfocustab="controls"
							love.timer.sleep( 0.5 )	
							
					elseif bfocus=="rejectsave" then 
						savebuttonpressed=false 
						saveconfirmpressed=false 
						optionsmenupressed=false
						languagehaschanged=false
						timer2=0 inputtime=0
							if gamestatus=="options" then 
								gamestatus="levelselection" 
								bfocus="exit"
						elseif 	gamestatus=="gameplusoptions" then
								gamestatus="game" bfocus=nil
						end
						
						love.timer.sleep( 0.5 )	
					end
				end
	end
	
	
	if (gamestatus=="options" or gamestatus=="gameplusoptions") and bfocustab=="controls" then
	
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
				inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
				timer2=0
				
					-- Handle left D-pad button press
					if joystick:isGamepadDown("dpleft") then
						-- Implement button focus changes based on direction and current focus
							if bfocus==nil or bfocus=="axis" then bfocus="vibration"
						elseif bfocus=="vibration" then
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
						elseif bfocus=="touchcontrols" then	
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
							
						elseif bfocus=="testgampad" then
						end	
				-- Handle right D-pad button press
				elseif joystick:isGamepadDown("dpright") then
							 -- Implement button focus changes based on direction and current focus
						if bfocus==nil or bfocus=="axis" then bfocus="vibration"
						elseif bfocus=="vibration" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
						elseif bfocus=="touchcontrols" then	
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
							
						elseif bfocus=="testgamepad" then
						end
				-- Handle up D-pad button press
				elseif  joystick:isGamepadDown("dpup") then
				  -- Implement button focus changes based on direction and current focus
								if bfocus==nil or bfocus=="axis" then bfocus="vibration"
							elseif bfocus=="vibration" then
							elseif bfocus=="touchcontrols" then 	bfocus="vibration"		bfocus2="on"
							elseif bfocus=="testgamepad" then 		bfocus="touchcontrols"	bfocus2="on"
							end
				-- Handle down D-pad button press
				elseif joystick:isGamepadDown("dpdown") then
				 -- Implement button focus changes based on direction and current focus
								if bfocus==nil or bfocus=="axis" then bfocus="vibration"
							elseif bfocus=="vibration" then 		bfocus="touchcontrols"	bfocus2="on"
							elseif bfocus=="touchcontrols" then		bfocus="testgamepad"
							elseif bfocus=="testgamepad" then
							end
						
				-- Handle a button press
				elseif joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						love.timer.sleep( 0.3 )	
						-- Handle button focus actions based on the current focus
								if bfocus==nil or bfocus=="axis" then
								bfocustab="controls"
								currenttab="controls"
						elseif bfocus=="vibration" then
								if bfocus2=="on" then	vibration=true  
							elseif bfocus2=="off" then	vibration=false 
							end
						elseif bfocus=="touchcontrols" then	
								if bfocus2=="on" then 	touchinterfaceison=true
							elseif bfocus2=="off" then	touchinterfaceison=false
							end
							
						elseif bfocus=="testgamepad" then	testcontroller=true
								
							
						end
			-- Handle b and start button press
				elseif joystick:isGamepadDown("b")  or joystick:isGamepadDown("start") then
					if savebuttonpressed==false then
									if gamestatus=="options" and not (testcontroller==true) then
									returntolevelselection()
								elseif gamestatus=="gameplusoptions" and not (testcontroller==true) then
									mousestate = not love.mouse.isVisible()	-- hide mouse pointer
									love.mouse.setVisible(mousestate)
									gamestatus="game"
								end
					end
				
				
	
				end
		
		end
	end


end


