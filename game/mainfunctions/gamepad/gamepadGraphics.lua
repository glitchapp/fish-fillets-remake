-- Handle gamepad input for graphics settings
function gamepadGraphics(joystick,button)

	-- Check if in options or gameplusoptions menu and graphics tab is focused
	if (gamestatus=="options"  or gamestatus=="gameplusoptions") and bfocustab=="graphics" then 
				 
				 -- Check for "A" button press
				if joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						
						    -- Handle different focus states
						if bfocus=="confirmsave" then
						 -- Save settings and transition back to options
							savebuttonpressed=false
							saveconfirmpressed=false
							optionsmenupressed=false
							languagehaschanged=false
							timer2=0 inputtime=0
							savemygame()
							gamestatus="options"
							bfocus=nil
							bfocustab="languages"
							love.timer.sleep( 0.5 )	
					elseif bfocus=="rejectsave" then 
					-- Cancel saving settings and transition back to appropriate menu
						savebuttonpressed=false 
						saveconfirmpressed=false 
						optionsmenupressed=false
						languagehaschanged=false
						timer2=0 inputtime=0
							if gamestatus=="options" then 
								gamestatus="levelselection" 
								bfocus="exit"
						elseif 	gamestatus=="gameplusoptions" then
								gamestatus="game" bfocus=nil
						end
						
						love.timer.sleep( 0.5 )	
					end
				end
	end
	
	-- Check for directional pad input
	if (gamestatus=="options" or gamestatus=="gameplusoptions") and bfocustab=="graphics" then
	
	   -- Handle different directional pad inputs
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
					-- Handle left direction
					if joystick:isGamepadDown("dpleft") then
							-- Update focus based on the current settings
							--if bfocus==nil or bfocus=="axis" then bfocus="hub"
							if bfocus=="hub" then
						elseif bfocus=="graphicprofile" then
								if bfocus2=="low" then
							elseif bfocus2=="standard" then bfocus2="low"
							elseif bfocus2=="high" then bfocus2="standard"
							end
						elseif bfocus=="colorscheme" then	
								if bfocus2=="scheme1" then
							elseif bfocus2=="scheme2" then bfocus2="scheme1"
							elseif bfocus2=="scheme3" then bfocus2="scheme2"
							elseif bfocus2=="scheme4" then bfocus2="scheme3"
							elseif bfocus2=="scheme5" then bfocus2="scheme4"
							elseif bfocus2=="scheme6" then bfocus2="scheme5"
							elseif bfocus2=="scheme7" then bfocus2="scheme6"
							end
							
						elseif bfocus=="3d" then
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
							
						elseif bfocus=="skin" then
								if bfocus2=="remake" then
							elseif bfocus2=="retro" then bfocus2="remake"
							elseif bfocus2=="classic" then bfocus2="retro"
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then
							elseif bfocus2=="yes" then bfocus2="yes"
							end
							
						elseif bfocus=="hdr" then
						elseif bfocus=="resolution" then
								if bfocus2=="1080p" then 
							elseif bfocus2=="1440p" then bfocus2="1080p"
							elseif bfocus2=="4k" then bfocus2="1440p"
							end
						elseif bfocus=="fullscreen" then
								if bfocus2=="yes" then
							elseif bfocus2=="no" then bfocus="yes"
							end
						elseif bfocus=="caustics" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
						elseif bfocus=="2ndeffect" then
								if bfocus2=="off" then
							elseif bfocus2=="CRT" then bfocus2="off"
							elseif bfocus2=="GodsrayGrid" then bfocus2="CRT"
							elseif bfocus2=="Gameboy" then bfocus2="GodsrayGrid"
							elseif bfocus2=="BlueNoise" then bfocus2="Gameboy"
							elseif bfocus2=="BlueNoise2" then bfocus2="BlueNoise"
							end
						
						elseif bfocus=="RGBpresets" then
								if bfocus2=="reset" then
							elseif bfocus2=="crimson" then bfocus2="reset"
							elseif bfocus2=="SeaGreen" then bfocus2="crimson"
							elseif bfocus2=="BlueViolet" then bfocus2="SeaGreen"
							end
						elseif bfocus=="R1" then
								if bfocus2=="+" then bfocus2="-"
							elseif bfocus2=="-" then
							end
						elseif bfocus=="G1" then
								if bfocus2=="+" then bfocus2="-"
							elseif bfocus2=="-" then 
							end
						elseif bfocus=="B1" then
								if bfocus2=="+" then bfocus2="-"
							elseif bfocus2=="-" then 
							end
						elseif bfocus=="ChromaticAberration" then
								if bfocus2=="off" then bfocus2="on"
							elseif bfocus2=="on" then
							end
						elseif bfocus=="VSync" then
								if bfocus2=="on" then
							elseif bfocus2=="off" then bfocus2="on"
							end
						elseif bfocus=="limitFPS" then
								if bfocus2=="30" then
							elseif bfocus2=="60" then	bfocus2="30"
							elseif bfocus2=="Unlimited" then bfocus2="60"
							elseif bfocus2=="StartBenchmark" then bfocus2="Unlimited"
							end
						end
						
				-- Handle right direction
				elseif joystick:isGamepadDown("dpright") then
							
							-- Update focus based on the current settings
							--if bfocus==nil or bfocus=="axis" then bfocus="hub"
							if bfocus=="hub" then
						elseif bfocus=="graphicprofile" then
								if bfocus2=="low" then bfocus2="standard"
							elseif bfocus2=="standard" then bfocus2="high"
							elseif bfocus2=="high" then 
							end
						elseif bfocus=="colorscheme" then	
								if bfocus2=="scheme1" then bfocus2="scheme2"
							elseif bfocus2=="scheme2" then bfocus2="scheme3"
							elseif bfocus2=="scheme3" then bfocus2="scheme4"
							elseif bfocus2=="scheme4" then bfocus2="scheme5"
							elseif bfocus2=="scheme5" then bfocus2="scheme6"
							elseif bfocus2=="scheme6" then bfocus2="scheme7"
							elseif bfocus2=="scheme7" then 
							end
							
						elseif bfocus=="3d" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then 
							end
							
						elseif bfocus=="skin" then
								if bfocus2=="remake" then bfocus2="retro"
							elseif bfocus2=="retro" then bfocus2="classic"
							elseif bfocus2=="classic" then 
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then bfocus2="yes"
							elseif bfocus2=="yes" then 
							end
							
						elseif bfocus=="hdr" then
						elseif bfocus=="resolution" then
								if bfocus2=="1080p" then bfocus2="1440p"
							elseif bfocus2=="1440p" then bfocus2="4k"
							elseif bfocus2=="4k" then 
							end
						elseif bfocus=="fullscreen" then
								if bfocus2=="yes" then bfocus2="no"
							elseif bfocus2=="no" then 
							end
						elseif bfocus=="caustics" then
								if bfocus2=="off" then bfocus2="on"
							elseif bfocus2=="on" then
							end
						elseif bfocus=="2ndeffect" then
								if bfocus2=="off" then bfocus2="CRT"
							elseif bfocus2=="CRT" then bfocus2="GodsrayGrid"
							elseif bfocus2=="GodsrayGrid" then bfocus2="Gameboy"
							elseif bfocus2=="Gameboy" then bfocus2="BlueNoise"
							elseif bfocus2=="BlueNoise" then bfocus2="BlueNoise2"
							elseif bfocus2=="BlueNoise2" then
							end
						
						elseif bfocus=="RGBpresets" then
								if bfocus2=="reset" then bfocus2="crimson"
							elseif bfocus2=="crimson" then bfocus2="SeaGreen"
							elseif bfocus2=="SeaGreen" then bfocus2="BlueViolet"
							elseif bfocus2=="BlueViolet" then
							end
						elseif bfocus=="R1" then
								if bfocus2=="+" then 
							elseif bfocus2=="-" then bfocus2="+"
							end
						elseif bfocus=="G1" then
								if bfocus2=="+" then 
							elseif bfocus2=="-" then bfocus2="+"
							end
						elseif bfocus=="B1" then
								if bfocus2=="+" then 
							elseif bfocus2=="-" then bfocus2="+"
							end
						elseif bfocus=="ChromaticAberration" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then
							end
						elseif bfocus=="VSync" then
								if bfocus2=="on" then bfocus2="off"
							elseif bfocus2=="off" then 
							end
						elseif bfocus=="limitFPS" then
								if bfocus2=="30" then bfocus2="60"
							elseif bfocus2=="60" then	bfocus2="Unlimited"
							elseif bfocus2=="Unlimited" then bfocus2="StartBenchmark"
							elseif bfocus2=="StartBenchmark" then
							end
						end
			
				
				-- Handle up direction
				elseif  joystick:isGamepadDown("dpup") then
					
					-- Update focus based on the current settings
								--if bfocus==nil or bfocus=="axis" then bfocus="hub"
								if bfocus=="hub" then
							elseif bfocus=="graphicprofile" then bfocus="hub"
							elseif bfocus=="colorscheme" then 	bfocus="graphicprofile" bfocus2="standard"
							elseif bfocus=="3d" then 			bfocus="colorscheme" 	bfocus2="scheme1"
							elseif bfocus=="skin" then 			bfocus="3d"				bfocus2="on"
							elseif bfocus=="Upscale" then 		bfocus="skin"			bfocus2="remake"
							elseif bfocus=="HDR" then 			bfocus="Upscale"		bfocus2="on"
							elseif bfocus=="resolution" then 	bfocus="HDR"			bfocus2="on"
							elseif bfocus=="fullscreen" then 	bfocus="resolution"		bfocus2="1440p"
							elseif bfocus=="caustics" then 		bfocus="fullscreen"		bfocus2="yes"
							elseif bfocus=="2ndeffect" then 	bfocus="caustics"		bfocus2="off"
							elseif bfocus=="RGBpresets" then 	bfocus="2ndeffect"		bfocus2="off"
							elseif bfocus=="R1" then 			bfocus="RGBpresets"		bfocus2="reset"
							elseif bfocus=="G1" then 			bfocus="R1"				bfocus2="+"
							elseif bfocus=="B1" then 			bfocus="G1"				bfocus2="+"
							elseif bfocus=="ChromaticAberration" then bfocus="B1"		bfocus2="+"
							elseif bfocus=="VSync" then 		bfocus="ChromaticAberration"	bfocus2="on"
							elseif bfocus=="limitFPS" then 		bfocus="VSync"			bfocus2="on" 
							end
						   
				-- Handle down direction
				elseif joystick:isGamepadDown("dpdown") then
							
							-- Update focus based on the current settings
								--if bfocus==nil or bfocus=="axis" then bfocus="hub"
								if bfocus=="hub" then 			bfocus="graphicprofile"	bfocus2="standard"
							elseif bfocus=="graphicprofile" then bfocus="colorscheme"	bfocus2="scheme1"
							elseif bfocus=="colorscheme" then 	bfocus="3d"				bfocus2="on"
							elseif bfocus=="3d" then 			bfocus="skin"			bfocus2="remake"
							elseif bfocus=="skin" then 			bfocus="Upscale"		bfocus2="on"
							elseif bfocus=="Upscale" then 		bfocus="HDR"			bfocus2="on"
							elseif bfocus=="HDR" then 			bfocus="resolution"		bfocus2="1440p"
							elseif bfocus=="resolution" then 	bfocus="fullscreen"		bfocus2="yes"
							elseif bfocus=="fullscreen" then 	bfocus="caustics"		bfocus2="off"
							elseif bfocus=="caustics" then 		bfocus="2ndeffect"		bfocus2="off"
							elseif bfocus=="2ndeffect" then 	bfocus="RGBpresets"		bfocus2="reset"
							elseif bfocus=="RGBpresets" then 	bfocus="R1"				bfocus2="+"
							elseif bfocus=="R1" then 			bfocus="G1"				bfocus2="+"
							elseif bfocus=="G1" then 			bfocus="B1"				bfocus2="+"
							elseif bfocus=="B1" then 			bfocus="ChromaticAberration" bfocus2="on"
							elseif bfocus=="ChromaticAberration" then bfocus="VSync"	bfocus2="on"
							elseif bfocus=="VSync" then 		bfocus="limitFPS"		bfocus2="30"
							elseif bfocus=="limitFPS" then
							end

				-- Check for "A" button press while navigating settings						
				elseif joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						 
						 -- Apply selected settings based on the current focus
						-- Transition between different settings
								if bfocus==nil  then
						elseif bfocus=="hub" then 
								bfocustab="graphicshub"
								graphbuttonhovered=false
								currenttab="graphicshub"
								love.timer.sleep( 0.3 )
						elseif bfocus=="graphicprofile" then
								if bfocus2=="low" then
							elseif bfocus2=="standard" then
							elseif bfocus2=="high" then
							end
						elseif bfocus=="colorscheme" then	
								love.timer.sleep( 0.3 )
								if bfocus2=="scheme1" then palette=1 skin="classic" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme2" then palette=2 skin="retro" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme3" then palette=3 skin="retro" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme4" then palette=4 skin="retro" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme5" then palette=5 skin="retro" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme6" then palette=6 skin="retro" skinhaschanged=true turnshadersoff()
							elseif bfocus2=="scheme7" then palette=7 skin="retro" skinhaschanged=true turnshadersoff()
							end
							
						elseif bfocus=="3d" then
								if bfocus2=="on" then
									require ("game/levels/3dlevel")		--3DreamEngine graphics
									load3dassets()
									threedassetsloaded=true
									threeD=true
							elseif bfocus2=="off" then
								love.timer.sleep( 0.3 )
								threeD=false
							end
							
						elseif bfocus=="skin" then
								if gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
							
								if bfocus2=="remake" then
										music:stop()
										skin="remake" musictype="new" palette=1 skinhaschanged=true musichaschanged=true
										loadlevelmusic()
										reloadpreviewassets()
										love.timer.sleep( 0.3 )
										
							elseif bfocus2=="retro" then
									love.timer.sleep( 0.3 )
									skin="retro" palette=2 caustics=false shader1=false shader2=false
							elseif bfocus2=="classic" then
									music:stop()
									skin="classic" musictype="classic" skinhaschanged=true musichaschanged=true
									loadlevelmusicclassic()
									reloadpreviewassets()
									palette=1
									love.timer.sleep( 0.3 )
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then
									music:stop()
									skinupscaled=false skinhaschanged=true musichaschanged=true
									loadlevelmusicclassic()
									reloadpreviewassets()
									love.timer.sleep( 0.3 )
							elseif bfocus2=="yes" then
									music:stop()
									skinupscaled=true  musictype="classic" skinhaschanged=true musichaschanged=true
									loadlevelmusicclassic()
									reloadpreviewassets()
									love.timer.sleep( 0.3 )
							end
							
						elseif bfocus=="hdr" then
						elseif bfocus=="resolution" then
								if bfocus2=="1080p" then 
									res="1080p"
									changeresolution()
									love.timer.sleep( 0.5 )
							elseif bfocus2=="1440p" then
									res="1440p"
									changeresolution()
									love.timer.sleep( 0.5 )
							elseif bfocus2=="4k" then
									res="4k"
									changeresolution()
									love.timer.sleep( 0.5 )
							end
						elseif bfocus=="fullscreen" then
								if bfocus2=="yes" then
										if gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
										love.timer.sleep( 0.3 )
										if res=="1080p" then dividx=1 dividy=1 		love.window.setMode(1920, 1080, {resizable=false, borderless=true})
									elseif res=="4k" then dividx=0.468 dividy=0.468 love.window.setMode(3840, 2160, {resizable=false, borderless=true})
									elseif res=="8k" then dividx=0.234 dividy=0.234 love.window.setMode(7680, 4320, {resizable=false, borderless=true})
									end
									love.timer.sleep( 1 )
							elseif bfocus2=="no" then
										if gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
										if res=="720p" then dividx=0.71 dividy=0.71 	love.window.setMode(1366, 768, {resizable=true, borderless=false})
									elseif res=="1080p" then dividx=1 dividy=1 	love.window.setMode(1920, 1080, {resizable=true, borderless=false})
									elseif res=="4k" then dividx=0.468 dividy=0.468 love.window.setMode(3840, 2160, {resizable=true, borderless=false})
									elseif res=="8k" then dividx=0.234 dividy=0.234 love.window.setMode(7680, 4320, {resizable=true, borderless=false})
									end
									love.timer.sleep( 1 )
							end
						elseif bfocus=="caustics" then
								love.timer.sleep( 0.3 )
								if bfocus2=="on" then caustics=true shader2=true
							elseif bfocus2=="off" then caustics=false shader2=false
							end
						elseif bfocus=="2ndeffect" then
								love.timer.sleep( 0.3 )
								if bfocus2=="off" then
									effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
									moonshineeffect=0
									palette=1
									shader1type=false
									shader1=false
									shader2=false
							elseif bfocus2=="CRT" then
								effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
								effect.enable("scanlines","crt","glow","filmgrain")
								effect.scanlines.opacity=0.1
								effect.glow.min_luma = 1
								effect.filmgrain.size=0.5
								effect.filmgrain.opacity=1
								shader1=true
							elseif bfocus2=="GodsrayGrid" then
								love.timer.sleep( 0.3 )
								effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
								effect.enable("godsray")
								palette=7
								shader1=true shader2=false shader1type="godsgrid" caustics=false
							elseif bfocus2=="Gameboy" then
								--gameboy
									effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
									effect.enable("scanlines","dmg")
								--palette=3
									effect.vignette.radius=1.2
									effect.vignette.opacity=3
									effect.dmg.palette=4
									effect.filmgrain.size=2.5
									effect.filmgrain.opacity=2
									effect.scanlines.width=1
									setuppaletteforshaderfilters()
									shader1=true	shader1type="gameboy" caustics=false
							elseif bfocus2=="BlueNoise" then
								shader1=false shader2=false
								-- Import blue noise dithering shader created by chatGPT
									bluenoise = require '/shaders/zblueNoise/bluenoise'
								-- Load the blue noise texture
									local blueNoiseTexture = love.graphics.newImage("shaders/zblueNoise/BlueNoise470.png")
								-- Set the blue noise dithering shader
									love.graphics.setShader(blueNoiseDitherShader)
									ditherIntensity = 0.5
								-- Pass the intensity value and blue noise texture to the shader
									blueNoiseDitherShader:send("intensity", ditherIntensity)
									blueNoiseDitherShader:send("blueNoiseTexture", blueNoiseTexture)
    
									setuppaletteforshaderfilters()
									shader1=true	shader1type="bluenoise" caustics=false
							elseif bfocus2=="BlueNoise2" then
									shader1=false shader2=false
								-- load the speccy color quantization + blue noise dithering shader
									bluenoise = require '/shaders/zblueNoise/bluenZx'
								-- Load the blue noise texture
									local blueNoiseTexture = love.graphics.newImage("shaders/zblueNoise/BlueNoise470.png")
								-- Load zx palette
									local zxpaletteTexture = love.graphics.newImage("shaders/zblueNoise/zxpaletteTexture.png")
							-- Set the blue noise dithering shader
									love.graphics.setShader(blueNoiseDitherShader)
									ditherIntensity = 0.5
							-- Pass the intensity value and blue noise texture to the shader
									blueNoiseDitherShader:send("intensity", ditherIntensity)
									blueNoiseDitherShader:send("blueNoiseTexture", blueNoiseTexture)
									setuppaletteforshaderfilters()
									shader1=true	shader1type="bluenoise" caustics=false
							end
						
						elseif bfocus=="RGBpresets" then
								love.timer.sleep( 0.3 )
								if bfocus2=="reset" then adjustr=1 adjustg=1 adjustb=1
							elseif bfocus2=="crimson" then adjustr=0.86 adjustg=0.07 adjustb=0.23
							elseif bfocus2=="SeaGreen" then adjustr=0.18 adjustg=0.70 adjustb=0.34
							elseif bfocus2=="BlueViolet" then adjustr=0.54 adjustg=0.16 adjustb=0.88
							end
						elseif bfocus=="R1" then
								love.timer.sleep( 0.3 )
								if bfocus2=="+" then adjustr=adjustr+0.1
									if adjustr>1 then adjustr=1 end
							elseif bfocus2=="-" then adjustr=adjustr-0.1
								if adjustr<0 then adjustr=0 end
							end
						elseif bfocus=="G1" then
								if bfocus2=="+" then  adjustg=adjustg+0.1
							elseif bfocus2=="-" then adjustg=adjustg-0.1
								if adjustg>1 then adjustg=1 end
							end
						elseif bfocus=="B1" then
								if bfocus2=="+" then adjustb=adjustb+0.1
							elseif bfocus2=="-" then adjustb=adjustb-0.1
								if adjustb<0 then adjustb=0 end
							end
						elseif bfocus=="ChromaticAberration" then
								love.timer.sleep( 0.3 )
								if bfocus2=="off" then chromaab=false
							elseif bfocus2=="on" then chromaab=true
							end
						elseif bfocus=="VSync" then
								love.timer.sleep( 0.3 )
								if bfocus2=="off" then
									love.window.setVSync(false)
									vsyncstatus=false
							elseif bfocus2=="on" then
									vsyncstatus=true
									love.window.setVSync(true)
							end
						elseif bfocus=="limitFPS" then
		
								love.timer.sleep( 0.3 )
								if bfocus2=="30" then limitframerate=30
							elseif bfocus2=="60" then limitframerate=60
							elseif bfocus2=="Unlimited" then 	limitframerate=nil
							elseif bfocus2=="StartBenchmark" then 
								benchmarkload()
								gamestatus="benchmark"
								love.timer.sleep( 0.3 )
							end
						end
						
						-- Check for "B" button or "Start" button press
						elseif joystick:isGamepadDown("b")  or joystick:isGamepadDown("start") then
										if savebuttonpressed==false then
										if gamestatus=="options" and not (testcontroller==true) then
										
										 -- Return to the level selection screen
										returntolevelselection()
									elseif gamestatus=="gameplusoptions" and not (testcontroller==true) then
										 
										 -- Toggle mouse visibility and transition back to the game
										mousestate = not love.mouse.isVisible()	-- hide mouse pointer
										love.mouse.setVisible(mousestate)
										gamestatus="game"
									end
									end
				
				
	
						end
		end
	end



end


