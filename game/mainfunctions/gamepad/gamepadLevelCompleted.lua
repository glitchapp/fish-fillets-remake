function gamepadLevelCompleted(joystick,button)

if gamestatus=="levelcompleted" then 
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
				if joystick:isGamepadDown("a") or joystick:isGamepadDown("b") or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
					
					dx = 0
					dy = 0
					if not (music==nil) then
						music:stop()
					end
					if not (mus==nil) then 
						mus:stop()
					end
					
					if PlayingFromLevelEditor==false then
						if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
						if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
						shader2=false
						bfocus="none"
						gamestatus="levelselection"
						love.timer.sleep(0.3)
				elseif PlayingFromLevelEditor==true then
				
						bfocus="none"
						gamestatus="levelEditor"
						love.timer.sleep(0.3)
					end
				end
		end
	end
	

end
