-- Handle gamepad input during the end menu
function gamepadEnds(joystick,button)

	if gamestatus=="endmenu" then 
	
	 -- Check for directional pad input
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
			
			-- Check for specific button presses
			if joystick:isGamepadDown("b")  or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
				returntolevelselection()	-- Return to the level selection screen
			end
		end
	end

end
