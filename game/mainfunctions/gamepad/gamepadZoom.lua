
function zoomwithgamepad()
--zoom

		if skin=="remake" then
			--zoom in
				if rightyaxis<-0.4 and not (radialmenu) and not (controllerLayout=="alternative") then 
						if nLevel==13 then expx=expx+0.1 expy=expy+0.1 dividx=dividx+0.05 dividy=dividy+0.05 tileSize=tileSize+6.9 scalepl=scalepl+0.1	--35x29 cells
					elseif nLevel==15 then expx=expx+0.1 expy=expy+0.1 dividx=dividx+0.17 dividy=dividy+0.17 tileSize=tileSize+9 scalepl=scalepl+0.1
					elseif nLevel==32 then expx=expx+0.1 expy=expy+0.1 dividx=dividx+0.15 dividy=dividy+0.15 tileSize=tileSize+5.2 scalepl=scalepl+0.1
					elseif nLevel==59 then expx=expx+0.1 expy=expy+0.1 dividx=dividx+0.20 dividy=dividy+0.20 tileSize=tileSize+5.2 scalepl=scalepl+0.1
					else 
						expx=expx+0.1/4 expy=expy+0.1/4			--increase map blocks size
						
						dividx=dividx+0.2/4 dividy=dividy+0.2/4	--increase objects size
						tileSize=tileSize+7.7/4				--increase distance between map blocks
						scalepl=scalepl+0.2/4
						
						-- try to center zoom
						bckx=bckx+rightyaxis*40
						bck2x=bck2x+rightyaxis*40
						frgx=frgx+rightyaxis*40
						
						bcky=bcky+rightyaxis*40
						bck2y=bck2y+rightyaxis*40
						frgy=frgy+rightyaxis*40
						
					end
			--zoom out
			elseif  rightyaxis>0.4 and not (radialmenu) and not (controllerLayout=="alternative") then 
						if nLevel==13 then expx=expx-0.1 expy=expy-0.1 dividx=dividx-0.05 dividy=dividy-0.05 tileSize=tileSize-6.9 scalepl=scalepl-0.1	--35x29 cells
					elseif nLevel==15 then expx=expx-0.1 expy=expy-0.1 dividx=dividx-0.17 dividy=dividy-0.17 tileSize=tileSize-9 scalepl=scalepl-0.1	
					elseif nLevel==32 then expx=expx-0.1 expy=expy+0.1 dividx=dividx-0.15 dividy=dividy-0.15 tileSize=tileSize-5.2 scalepl=scalepl-0.1
					elseif nLevel==59 then expx=expx-0.1 expy=expy+0.1 dividx=dividx-0.20 dividy=dividy-0.20 tileSize=tileSize-5.2 scalepl=scalepl-0.1
					else
						expx=expx-0.1/4 expy=expy-0.1/4			-- decrease map blocks size
						dividx=dividx-0.2/4 dividy=dividy-0.2/4	-- decrease	objects size
						tileSize=tileSize-7.7/4				--decrease distance between map blocks
						scalepl=scalepl-0.2/4
						
						
						-- try to center zoom
						bckx=bckx+rightyaxis*40
						bck2x=bck2x+rightyaxis*40
						frgx=frgx+rightyaxis*40
						
						bcky=bcky+rightyaxis*40
						bck2y=bck2y+rightyaxis*40
						frgy=frgy+rightyaxis*40
					end
			end

		end
end

function camerawithgamepad()

--camera
	if (rightxaxis<-0.4 or rightxaxis>0.4) and not (radialmenu) and not (zoomtriggered==true) and controllerLayout=="default" then 
		
		bckx=bckx-(rightxaxis*20) bck2x=bck2x-(rightxaxis*20)	frgx=frgx-(rightxaxis*20)
	end
	if (rightyaxis<-0.4 or rightyaxis>0.4) and not (radialmenu) and not (zoomtriggered==true) and controllerLayout=="default" then 
		bcky=bcky-(rightyaxis*scalepl*10) bck2y=bck2y-(rightyaxis*scalepl*10)		frgy=frgy-(rightyaxis*scalepl*10)
	end
	
	-- movement if alternative gamepad layout is selected
		if not (radialmenu) and not (zoomtriggered==true) and (controllerLayout=="alternative") then 
		
				if	rightxaxis<-0.4 then 
					dx = -1
					dy = 0
			elseif 	rightxaxis>0.4 then
					dx = 1
					dy = 0
			end
				if 	rightyaxis<-0.2 then
					dx = 0
					dy = -1
			elseif 	 rightyaxis>0.4 then
					dx = 0
					dy = 1
			end
		end
			if movementTimer >= movementDelay then
						steps=steps+1 --add steps to the movement
						movementTimer = 0
						if not (fish1status=="dead") and not (fish2status=="dead") then
							pb:mainMoving (dx, dy, dt)
						end
			end

end
