function gamepadOptionsLanguages(joystick,button)


	-- gamepad selection on options menu
		if (gamestatus=="options" or gamestatus=="gameplusoptions") and bfocustab=="languages" then 
				if joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
						if bfocus=="confirmsave" then
							savebuttonpressed=false
							saveconfirmpressed=false
							optionsmenupressed=false
							languagehaschanged=false
							timer2=0 inputtime=0
							savemygame()
							gamestatus="options"
							bfocus=nil
							bfocustab="languages"
							love.timer.sleep( 0.5 )	
					elseif bfocus=="rejectsave" then 
						savebuttonpressed=false 
						saveconfirmpressed=false 
						optionsmenupressed=false
						languagehaschanged=false
						timer2=0 inputtime=0
							if gamestatus=="options" then 
								gamestatus="levelselection" 
								bfocus="exit"
						elseif 	gamestatus=="gameplusoptions" then
								gamestatus="game" bfocus=nil
						end
						
						love.timer.sleep( 0.5 )	
					end
				end
		
	if (gamestatus=="options" or gamestatus=="gameplusoptions") and bfocustab=="languages" then
	
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
					if joystick:isGamepadDown("dpleft") then
							
							if bfocus==nil or bfocus=="axis" then bfocus="langarrowup"
						elseif bfocus=="langarrowup" then
						elseif bfocus=="langarrowdown" then
						--elseif bfocus=="currentlanguage" then
						elseif bfocus=="actor1" then
						elseif bfocus=="actor2" then bfocus="actor1"
						elseif bfocus=="accent1" then
						elseif bfocus=="accent2" then bfocus="accent1"
						elseif bfocus=="rejectsave" then bfocus="confirmsave"
						end
				elseif joystick:isGamepadDown("dpright") then
							
							if bfocus==nil or bfocus=="axis" then bfocus="langarrowup"	
						elseif bfocus=="langarrowup" then -- bfocustab="graphics"
						elseif bfocus=="langarrowdown" then --bfocustab="graphics"
						--elseif bfocus=="currentlanguage" then
						elseif bfocus=="actor1" then bfocus="actor2"
						elseif bfocus=="actor2" then 
						elseif bfocus=="accent1" then bfocus="accent2"
						elseif bfocus=="accent2" then
						elseif bfocus=="confirmsave" then bfocus="rejectsave"
						end
						
			
				
				elseif  joystick:isGamepadDown("dpup") then
							
							if bfocus==nil or bfocus=="axis" then bfocus="langarrowup"
						elseif bfocus=="langarrowup" then
						--elseif bfocus=="currentlanguage" then bfocus="langarrowup"
						elseif bfocus=="langarrowdown" then bfocus="langarrowup"
						elseif bfocus=="actor1" then bfocus="langarrowdown"
						elseif bfocus=="actor2" then bfocus="langarrowdown"
						elseif bfocus=="accent1" then bfocus="actor1"
						elseif bfocus=="accent2" then bfocus="actor2"
						end
				elseif joystick:isGamepadDown("dpdown") then
							
							if bfocus==nil or bfocus=="axis" then bfocus="langarrowdown"
						elseif bfocus=="langarrowup" then bfocus="langarrowdown"
						--elseif bfocus=="currentlanguage" then bfocus="langarrowdown"
						elseif bfocus=="langarrowdown" then bfocus="actor1"
						elseif bfocus=="actor1" then bfocus="accent1" 
						elseif bfocus=="actor2" then bfocus="accent2"
						elseif bfocus=="accent1" then
						elseif bfocus=="accent2" then
						end
				elseif joystick:isGamepadDown("a")  then
						gamepadselectanimation=true
							if bfocus=="langarrowup" then 
								if not (langlistnumber>19) then
									language=langlist[langlistnumber+1]
									language2=langlist[langlistnumber+1]
									love.timer.sleep( 0.1 )
									languagehaschanged=true
									optionsload()
								end
						elseif bfocus=="langarrowdown" then
								if not  (langlistnumber<1) then
									language=langlist[langlistnumber-1]
									language2=langlist[langlistnumber-1]
									love.timer.sleep( 0.1 )
									languagehaschanged=true
									optionsload()
								end
						elseif bfocus=="actor1" then
								if accent=="br" then accent="us" languagehaschanged=true
							elseif accent=="us" then accent="br" languagehaschanged=true
							end
						elseif bfocus=="actor2" then
								if accent2=="br" then accent2="us" languagehaschanged=true
							elseif accent2=="us" then accent2="br" languagehaschanged=true
							end
						elseif bfocus=="accent1" or bfocus=="accent2" then 
								if accent=="br" then accent="us" accent2="us"
							elseif accent=="us" then accent="br" accent2="br"
							elseif accent=="es" then accent="la" accent2="la"
							elseif accent=="la" then accent="la" accent2="la"
							elseif accent=="fr" then accent="ca" accent2="ca"
							elseif accent=="ca" then accent="fr" accent2="fr"
							elseif accent=="de" then accent="ch" accent2="ch"
							elseif accent=="ch" then accent="de" accent2="de"
							end
						
						end
						
					elseif joystick:isGamepadDown("b")  or joystick:isGamepadDown("start") then
								if savebuttonpressed==false then
										if gamestatus=="options" and not (testcontroller==true) then
										returntolevelselection()
									elseif gamestatus=="gameplusoptions" and not (testcontroller==true) then
										mousestate = not love.mouse.isVisible()	-- hide mouse pointer
										love.mouse.setVisible(mousestate)
										gamestatus="game"
									end
							elseif savebuttonpressed==true then
								
							end
		
					
					end		
				end
			end
		end
	
end
