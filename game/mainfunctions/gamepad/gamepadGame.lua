-- Handle gamepad input during the game
function gamepadGame(joystick,button)
	if (gamestatus=="game" or gamestatus=="intro") then
		if joystick:isGamepadDown("b") then
			if screensaver==true then
				gamestatus="extras"
				screensaver=false
			elseif gamestatus=="intro" then
				introvideo:pause()
				introvideoplaying=false
				love.audio.stop()
				nLevel=1
				loadlevelvariables()
				levelload()
			
			gamestatus="levelselection" 
			love.timer.sleep(0.3)
			
			end
		end
	end
		
	
if gamestatus=="game" and not screensaver==true and not (pauseisactive==true) then

    -- Check for directional pad input
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			
			if gamestatus=="game" then steps=steps+1 end	--add steps to the movement
		  
		  -- Determine directional movement based on D-pad input
			if joystick:isGamepadDown("dpleft") then
				dx = -1
				dy = 0
			elseif joystick:isGamepadDown("dpright") then
				dx = 1
				dy = 0
			elseif  joystick:isGamepadDown("dpup") then
				dx = 0
				dy = -1
			elseif joystick:isGamepadDown("dpdown") then
				dx = 0
				dy = 1
			
			    -- Check for specific button presses
			elseif joystick:isGamepadDown("a")  then
			pb:switchAgent ()
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("b") and radialmenu==false then
				radialmenu=false
				--	if mus then mus:stop()
				--elseif music then music:stop()
				--end
				love.audio.stop()
				if musicison==true then
					lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
					dx = 0
					dy = 0
					shader2=false
					timer2=0 inputtime=0
					--isloading=true
					bfocus="none"
					screensaver=false
					mousestate = not love.mouse.isVisible()	-- show mouse pointer
					love.mouse.setVisible(mousestate)
					savebuttonpressed=false saveconfirmpressed=false
					gamestatus="levelselection"
					love.timer.sleep( 0.3 )
			elseif joystick:isGamepadDown("y")  then
					if helpison=="yes" then helpison="no"
				elseif helpison=="no" then helpison="yes"
				end
				dx = 0
				dy = 0
			elseif joystick:isGamepadDown("leftstick")  then
				
				love.timer.sleep( 0.3 )
			elseif joystick:isGamepadDown("rightstick")  then
				resetallzoomvalues()
				love.timer.sleep( 0.3 )
			end
			
			-- Handle start button for pausing the game
			if joystick:isGamepadDown("start") then
					if pauseisactive==false or pauseisactive==nil then
						pauseisactive=true
						love.audio.pause( mus )
				elseif pauseisactive then
						pauseisactive=false
						love.audio.play( mus )
					end
			 
			 -- Handle back button for accessing game options
			elseif joystick:isGamepadDown("back")  then
				inputtime=0
				languagehaschanged=false
				skinhaschanged=false
				bfocustab="languages"
				mousestate = not love.mouse.isVisible()	-- show mouse pointer
				love.mouse.setVisible(mousestate)
				gamestatus="gameplusoptions"
				optionsloadlanguages()
				love.timer.sleep( 0.3 )
			
			 -- Handle x button for repeating the current level
			elseif joystick:isGamepadDown("x")  then	--repeat level
				mousestate = not love.mouse.isVisible()	-- hide mouse pointer
				love.mouse.setVisible(mousestate)
				repeatleveltriggered=true
				stepdone=0
				changelevel()
			
			elseif joystick:isGamepadDown("guide")  then
				 myscreenshot = love.graphics.captureScreenshot(os.time() .. ".png")
				 photosound:play()
				 timer3=0
				 screenshottaken=true
				 
			mousestate = not love.mouse.isVisible()	-- show mouse pointer
			love.mouse.setVisible(mousestate)
			
			--elseif joystick:isGamepadDown("leftshoulder") then
			   -- Call rewind function to move one step backwards
				--rewindGame()
				
			
			end
			-- Move the game agent
		    pb:mainMoving (dx, dy,dt)
		end
		
		-- Set gamepad state if any gamepad button is pressed
		 if joystick:isGamepad() then
			isgamepad=true
		end

elseif gamestatus=="game" and not screensaver==true and (pauseisactive==true) then

		-- Handle start button for pausing the game
			if joystick:isGamepadDown("start") then
						pauseisactive=false
						love.audio.play( mus )
			end


  -- Handle gamepad input during the gameplusoptions screen
elseif gamestatus=="gameplusoptions" then
	if joystick:isGamepadDown("back")  then
		mousestate = not love.mouse.isVisible()	-- hide mouse pointer
		love.mouse.setVisible(mousestate)
		gamestatus="game"	-- Return to the game screen
	end
end
	

end
