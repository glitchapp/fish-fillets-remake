-- Handle gamepad input during the credits screen
function gamepadCredits(joystick,button)
	
	if gamestatus=="credits" or gamestatus=="creditsend"  then 
	 
	 -- Check for directional pad input
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
			inputtime=0	 -- Reset the counter to prevent triggering the intro animation
				
				 -- Check for specific button presses
				if joystick:isGamepadDown("b") or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
					
					dx = 0	-- Reset x-direction movement
					dy = 0	-- Reset y-direction movement
					music:stop()	-- Stop playing the current music
					if soundon==true then
						TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
					end
					if musicison==true then
						lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
					end
					gamestatus="levelselection" -- Return to the level selection screen
				end
		end
	end
end
