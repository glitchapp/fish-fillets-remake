function gamepadOptions(joystick,button)

	-- gamepad selection on options menu
	if gamestatus=="options"  or gamestatus=="gameplusoptions" then 
		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		inputtime=0	-- sets the counter to 0 to prevent the intro animation to triggers while using the gamepad
		timer2=0
								

				--joystick shoulders
					if joystick:isGamepadDown("leftshoulder") then
									gamepadselectanimation=true
							if bfocus=="axis" then bfocustab="languages" bfocus="langarrowup"
						elseif bfocustab=="graphics" then bfocustab="languages" bfocus="langarrowup"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="languages"
						elseif bfocustab=="audio" then bfocustab="graphics" bfocus="hub"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="graphics"
						elseif bfocustab=="controls" then bfocustab="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="audio"
								bfocus="sound"
								bfocus2="on"
						elseif bfocustab=="optionsreturn" then bfocustab="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="controls"
								bfocus="vibration"
								bfocus2="on"
						end
				elseif joystick:isGamepadDown("rightshoulder") then
								gamepadselectanimation=true
							if bfocus=="axis" then bfocustab="languages" bfocus="langarrowup"
						elseif bfocustab=="languages" then bfocustab="graphics" bfocus="hub"
								inputtime=0
									-- load preview images for the graphic menu
									if level1bck==nil then
										level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
										level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
										level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
									end
								love.timer.sleep( 0.3 )
								bfocustab="graphics"
						elseif bfocustab=="graphics" then bfocustab="audio"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="audio"
								bfocus="sound"
								bfocus2="on"
						elseif bfocustab=="audio" then bfocustab="controls"
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="controls"
								bfocus="vibration"
								bfocus2="on"
						elseif bfocustab=="controls" then
						end
			end
		end
		if gamestatus=="options" and bfocustab=="languages" then 

					if  joystick:isGamepadDown("dpup") then
						if bfocus=="axis" then bfocustab="languages" end
				elseif joystick:isGamepadDown("dpdown") then
						if bfocustab=="axis" then bfocustab="languages" end
				elseif joystick:isGamepadDown("a")  then
							gamepadselectanimation=true
							if bfocustab=="languages" then 
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="languages"
						elseif bfocustab=="graphics" then
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="graphics"
						elseif bfocustab=="audio" then
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="audio"
						elseif bfocustab=="controls" then
								inputtime=0
								love.timer.sleep( 0.3 )
								bfocustab="controls"
						elseif bfocus=="optionsreturn" then 
							returntolevelselection()
						elseif bfocus=="confirmsave" then
							savemygame()
							love.timer.sleep( 0.5 )	
						elseif bfocus=="rejectsave" then
							savebuttonpressed=false timer2=0 inputtime=0
							saveconfirmpressed=false
							if joystick then bfocus="exit" end
							love.timer.sleep( 0.5 )	
						
						end
						
						
						elseif joystick:isGamepadDown("b")  or joystick:isGamepadDown("back") or joystick:isGamepadDown("start") then
								if savebuttonpressed==false then
									returntolevelselection()
							elseif savebuttonpressed==true then
								
							end
						elseif joystick:isGamepadDown("x") then
							inputtime=0
				
							savemygame()
							languagehaschanged=false
							musichaschanged=false
							skinhaschanged=false
							optionsmenupressed=false
							love.timer.sleep( 0.3 )
						
				end
		
		
		end

		
	end
	if gamestatus=="gameplusoptions" then 
		if joystick:isGamepadDown("x") then
							inputtime=0
							
							languagehaschanged=false
							musichaschanged=false
							skinhaschanged=false
							optionsmenupressed=false
							savemygame()
							palette=1
							love.timer.sleep( 0.3 )
		end
	end		

end






