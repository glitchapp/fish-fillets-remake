-- Table to store loaded assets
loadedAssets = {}

-- Function to load an asset if not already loaded
function loadAsset(assetPath, assetType)
    if not loadedAssets[assetPath] then
        if assetType == "Image" then
            local webpData = love.filesystem.newFileData(assetPath)
            loadedAssets[assetPath] = love.graphics.newImage(WebP.loadImage(webpData))
        -- Add more conditions for other asset types if needed
        end
    end
    return loadedAssets[assetPath]
end

-- Function to load a dummy asset if not already loaded
function loadDummyAsset()
    local dummyAssetPath = "path/to/dummy/asset.png"
    local assetType = "Image"

    if not loadedAssets[dummyAssetPath] then
        if assetType == "Image" then
            -- Load a dummy image
            loadedAssets[dummyAssetPath] = love.graphics.newImage("path/to/dummy/asset.png")
        
        end
    end

    return loadedAssets[dummyAssetPath]
end
