-- If gamestatus is "gameover", draw the game over screen
function drawGameover()
	drawgameover()
	-- Draw FPS metrics
	drawfpsmetrics()
end

function drawButtonFocusGameOver()
		--if joystick then 
			buttonfocusanimations()		-- focus color and size animations
			if bfocus=="restartgameover" 	then love.graphics.rectangle ('line', restartButton.x, restartButton.y, fwidth, fheight)		--fwidth 
			end
		--end
end

--The third function, drawgameover, does not take any input parameters. It calls the drawdefaultgameovertext() function to draw the default game over text. However, the code for the drawdefaultgameovertext() function is not provided in the code snippet.
function drawgameover()
			drawdefaultgameovertext()
			drawButtonFocusGameOver() 
			love.graphics.setColor(1,1,1,1)
end

--[[
This function, drawdefaultgameovertext(), is responsible for drawing the "Game Over" screen when the game ends.

It first disables several visual effects and enables a box blur effect. Then, it sets a shader and draws all the game content using this shader. After that, it sets the color of some variables and sets an audio effect for the music based on the current level of the game.

Finally, it draws the "Game Over" text in the center of the screen and a "Restart" button below it. It checks whether the mouse is hovering over the button and if the left mouse button is clicked, it stops the music, sets the reverb effect, changes the BPM of the music, changes the game status to "levelselection" and resets the status of the fish in the game.
--]]
function drawdefaultgameovertext()
			--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
			
			love.graphics.setShader()
			effect(function()
	
				drawallcontent()
				drawforshader2()
			end)
			scolor1={1,0.5,0.5}
			scolor2={1,0.5,0.5}
			scolor3={1,0,0}
			scolor4={1,0,0}
			--audio effect
			--setcompressoreffect()
			if not (nLevel==15 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==32 or nLevel==34 or nLevel==38  or nLevel==44 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==63 or nLevel==66 or nLevel==67 or nLevel==68 or nLevel==69 or nLevel==70) then 
				mus:setEffect('myEffect')
				mus:setVolume(0.3)
			elseif not (nLevel==68) then music:setVolume(0.3)
			end
			
			
			love.graphics.print("Game over",600,300,0, 0.8, 1)
			--love.graphics.print("Time:" .. timeneeded,600,400,0, 0.8, 1)
			--love.graphics.print("Steps:" .. steps,600,500,0, 0.8, 1)
			
						-- continue

				hovered = isButtonHovered (restartButton)
				drawButton (restartButton, hovered,"Restart")
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					if love.mouse.isDown(1) then 
						if not (nLevel==15 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==32 or nLevel==34 or nLevel==44 or nLevel==38 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==63 or nLevel==66 or nLevel==67 or nLevel==68 or nLevel==69 or nLevel==70) then 
							mus:stop()
							mus:setVolume(1)
						elseif not (nLevel==68) then music:stop() music:setVolume(1)
						end
						shader2=false
						gameoversentencesaid=false
						--[[
						setreverbeffect()
						mybpm=112
						lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
						effect.enable("scanlines","crt","glow","filmgrain")
						love.timer.sleep( 0.2 )
						--mus:setVolume(1)
						gamestatus="levelselection" 
						--]]
						mousestate = not love.mouse.isVisible()	-- hide mouse pointer
						love.mouse.setVisible(mousestate)
						changelevel()
						fish1status="idle"
						fish2status="idle"
						gamestatus="game" 
					end
				end
end

--[[
This function is a part of a larger program that involves dialogue between two characters, a big fish and a small fish, upon a game over event. The function starts by loading two images (webp files) which will be used as avatars for the two characters.

Next, it sets the font for the text displayed in the dialogue box based on the chosen language (English, Russian, Slovenian, or Chinese). It also sets the text to be displayed for the "small fish" and "big fish" variables depending on the language.

Then, the function uses the Talkies library to display two separate dialogues, one for the big fish and one for the small fish. The dialogues consist of a single message for each character, which is stored in an array (gameoverdarn_en) and chosen based on the language selected. The Talkies library also allows for avatar images and a sound effect to be associated with each character's dialogue box.

Overall, this function adds a bit of personality to the game over event by giving the two characters a chance to react to it and speak to each other.
--]]
function Obey.gameoverdarn()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	gameoverdarn_en[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverdarn_en[2],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

--The function Obey.gameoverparrot() displays a dialogue using the Talkies library. It sets three avatar images (avatar, avatar2, parrotavatar) and selects a font based on the language variable. If language is "chi", it sets the variables smallfisht and bigfisht to "小鱼" and "大鱼", respectively; otherwise, it sets them to "small fish" and "Big fish". The function then displays three lines of dialogue, spoken by "Parrot", "Big fish", and "small fish", respectively, using the Talkies.say() function. Each line of dialogue includes an image and a talk sound.
function Obey.gameoverparrot()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  parrotavatar = love.graphics.newImage("/externalassets/objects/level45/parrotavatar.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Parrot",
    { 
    	gameoverparrot_en[1],
    },
    {
      image=parrotavatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

Talkies.say( "Big fish",
    { 
    	gameoverparrot_en[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverparrot_en[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

--The function Obey.gameoverhereafter() is similar to Obey.gameoverparrot(), but it displays only two lines of dialogue, spoken by "Big fish" and "small fish", respectively. It sets two avatar images (avatar, avatar2) and selects a font based on the language variable. If language is "chi", it sets the variables smallfisht and bigfisht to "小鱼" and "大鱼", respectively; otherwise, it sets them to "small fish" and "Big fish". The function then displays two lines of dialogue, spoken by "Big fish" and "small fish", respectively, using the Talkies.say() function. Each line of dialogue includes an image and a talk sound.
function Obey.gameoverhereafter()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	gameoverhereafter_en[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverhereafter_en[2],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end
