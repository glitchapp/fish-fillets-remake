
function loadgameover()
	
gameoverdarn_en={"Darn!",
"Shhh, children can be playing this game!",
}
gameoverparrot_en={"Aye, caramba!",
"But there is no parrot here!",
"Shut up, you are dead.",
}
gameoverhereafter_en={"Hellooo? Can you hear me hereafter? Is there life after death?",
"I don’t know. I don’t think so.",
}

gameoverdarn_es={"Oh, mier...!",
"Shht, ¡Niños pueden estar jugando este juego!",
}
gameoverparrot_es={"Aye, caramba!",
"¡Pero no hay ningún perico aquí!",
"Cállate, estás muerto.",
}
gameoverhereafter_es={"¿Holaaa? ¿Puedes oirme por ahí? ¿Hay vida después de la muerte?",
"No lo sé. No lo creo.",
}

gameoverdarn_fr={"Oh, mer.. !",
"Chut, des enfants peuvent jouer à ce jeu !",
}

gameoverparrot_fr={"Aye, caramba!",
"Mais il n'y a pas de perroquet ici !",
"La ferme, tu es mort.",
}
gameoverhereafter_fr={"Saluuuut ? Pouvez-vous m'entendre de l'au-delà ? Y a-t-il une vie après la mort ?",
"Je ne sais pas. Je ne crois pas.",
}

poorfishmiddle = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 64) -- Thanks @hicchicc for the font

	restartButton = {
		text = "Restart",
		x = 500,
		y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	--buttons

end


function loadgameoversentences()
gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/border/empty.ogg","stream" )
	-- if big fish is dead
	if fish1status=="dead" and gameoversentencesaid==false then
		if language2=="en" then
				if accent2=="br" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,7)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/10somuchforhim.ogg","stream" )		--	"So much for him."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/102ohno.ogg","stream" )				--	"Oh, nooo."
					elseif aleatorygameoversentence==2 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/11andnowiamalone.ogg","stream" )	--	"And now I am alone."
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/12thatmusthavehurt.ogg","stream" ) 	--	"That must have hurt."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/13hedidnt.ogg","stream" )			--	"He didn’t deserve such a terrible end…"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/14soletsstart.ogg","stream" )		--	"So, let’s start again…"
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/25iamnotgoing.ogg","stream" )		-- "I am not going to solve it alone. Don’t trouble yourself with restart, I’ll do it myself."
															
					elseif aleatorygameoversentence==7 then	aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
															buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
															shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end -- end aleatorygameoversentence
			elseif accent2=="us" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,7) 
							--aleatorygameoversentence = 6 print(aleatorygameoversentence)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/10somuchforhim.ogg","stream" )		--	"So much for him."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/102ohno.ogg","stream" )				--	"Oh, nooo."
					elseif aleatorygameoversentence==2 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/11andnowiamalone.ogg","stream" )	--	"And now I am alone."
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/12thatmusthavehurt.ogg","stream" ) 	--	"That must have hurt."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/13hedidnt.ogg","stream" )			--	"He didn’t deserve such a terrible end…"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/14soletsstart.ogg","stream" )		--	"So, let’s start again…"
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/25iamnotgoing.ogg","stream" )		--  "I am not going to solve it alone. Don’t trouble yourself with restart, I’ll do it myself."
														--repeat level
														timer2=0
					elseif aleatorygameoversentence==7 then	aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
															buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
															shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
			end	-- end accent2
		end	-- end language2
	end -- end fish1status dead
	
	-- if small fish is dead
	
	if fish2status=="dead" and gameoversentencesaid==false then
		if language=="en" then
				if accent=="br" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
																shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shhh.ogg","stream" )		-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/7didsheknow.ogg","stream" )	--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg.ogg","stream" )			-- "Luckily, it’s possible to restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==12 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==13 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==14 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==15 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
					
			elseif accent=="us" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
																--	shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/7didsheknow.ogg","stream" )		--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	--end aleatorygameoversentence
			end	--end accent
	
		elseif language=="es" then
		
				if accent=="es" then
			elseif accent=="la" then
						math.randomseed(os.time())
						aleatorygameoversentence = math.random(0,26)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/1ohshoot.ogg","stream" )			-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then	--darn = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )							--	"Darn."
					--										shhchildren = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/2shhh.ogg","stream" )					-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/4dang.ogg","stream" )				--	"Dang!"
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/scheiße.ogg","stream" )				--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/6hmmandnow.ogg","stream" )			--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/7didsheknow.ogg","stream" )			--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/8terrible.ogg","stream" )			--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/9whatcanbe.ogg","stream" )			--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/20fortunately.ogg","stream" )	-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/21restartthelevel.ogg","stream" ) -- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/23canyouhearmehereafter.ogg","stream" ) -- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) 	-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/26andsheisgone.ogg","stream" )	-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
			end -- end accent
			
			elseif language=="fr" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
																--	shhchildren = love.audio.newSource( "/externalassets/dialogs/share/fr/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/7didsheknow.ogg","stream" )		--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/fr/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/fr/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	--end aleatorygameoversentence
			
			end	--end language
	end	-- end fish2status dead
	
		
--[[
This section of the code handles different scenarios for when the fish in the game die. It first checks which fish has died (big or small), and then selects a pre-recorded voice clip to play based on the language and accent chosen in the game settings.

For example, if the big fish dies, and the game is set to English (US accent), it will play two voice clips: one saying "But there is no parrot here!" and the other saying "Shut up, you are dead."

Similarly, if the small fish dies and the game is set to Spanish (Latin American accent), it will play a voice clip saying "Darn" and another saying "Shhh, children can be playing this game!".

If a particular game over scenario has already played once (gameoversentencesaid=true), it will not play again.
--]]
			if talkies==true then
					-- if big fish dies
					if fish1status=="dead" and aleatorygameoversentence==14 and gameoversentencesaid==false then
						stepdone=0
						aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
							if language=="en" then
									if accent=="br" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
								elseif accent=="us" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
								end	-- end accent
							elseif language=="es" then
									if accent=="es" then
								elseif accnet=="la" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/16butthereisnoparrot.ogg","stream" )	--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/shutupyouaredead.ogg","stream" )	--  "Shut up, you are dead."
								end	-- end accent
							elseif language=="fr" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/fr/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/fr/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
									Talkies.clearMessages()
									Obey.gameoverparrot()
							end	--end language
					-- if small fish dies
				elseif fish2status=="dead" and aleatorygameoversentence==2 and gameoversentencesaid==false then print("second")
							stepdone=0
							if language=="en" then
								if accent=="br" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								elseif accent=="us" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								end	-- end accent
							elseif language=="es" then
								if accent=="es" then
								elseif accent=="la" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								end	-- end accent
							elseif language=="fr" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/fr/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								Talkies.clearMessages()
								Obey.gameoverdarn()
							end	--end language
				-- if small fish dies
				elseif fish2status=="dead" and aleatorygameoversentence==12 and gameoversentencesaid==false then
						stepdone=0
							if language=="en" then
								if accent=="br" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) -- "I don’t know. I don’t think so."
								elseif accent=="us" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) 			-- "I don’t know. I don’t think so."
								end	--end accent
							elseif language=="es" then
								if accent=="es" then
								elseif accent=="la" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) -- "I don’t know. I don’t think so."
								end --end accent
							elseif language=="fr" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/fr/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/fr/black/24idontknow.ogg","stream" ) 			-- "I don’t know. I don’t think so."
								Talkies.clearMessages()
								Obey.gameoverhereafter()
							end	 --end language
				else
				
					gameoversentence:setEffect('myEffect')
					gameoversentence:play()
				end --end of fishstatus
			
			end --end of talkies==true
				
			gameoversentencesaid=true		
end
