function creditsload()
	
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
font6 = love.graphics.newFont(12*6) -- six times bigger
	
--lv1sketch= love.graphics.newImage("/assets/levels/level1/lv1sketch.webp")

require ('game/states/Credits/CreditsText')
--level34frg =  loadAsset("/externalassets/levels/level34/8k/level34bck8k.webp", "Image")
level25bck =  loadAsset("/externalassets/levels/level25/level25bck2.webp", "Image")

credposition=900
credspeed = 32






--[[
The code defines two button objects called "creditsReturnButton" and "creditsForwardButton". These buttons have several properties, including their text, position, rotation, scale, and font. The buttons are not initially hovered, and have a white color when not hovered and a yellow color when hovered.
--]]
creditsReturnButton = {
	text = "Return",
	x = 1000,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

creditsForwardButton = {
	text = "Forward",
	x = 1000,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end
