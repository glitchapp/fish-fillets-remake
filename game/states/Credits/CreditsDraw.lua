local minCredPosition = 800
local maxCredPosition = -35000


function creditsdraw(dt)

if gamestatus=="credits" then 
	-- Map credposition to a value between 0 and 1
	local colorFactor = (credposition - minCredPosition) / (maxCredPosition - minCredPosition)
	--print(colorFactor)	
	-- Limit the colorFactor to the range [0, 1]
	colorFactor = math.min(1, math.max(0, colorFactor))
	
	-- Use colorFactor to adjust the color
	local red = colorFactor-0.2
	local green = colorFactor-0.2
	local blue = colorFactor
	
	-- Set the color
	love.graphics.setColor(red, green, blue)
	love.graphics.draw(level25bck,0,0,0,1,1)
end
	
	if not (gamestatus=="creditsend") then 
	local hovered = isButtonHovered (creditsReturnButton)
	drawButton (creditsReturnButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  music:stop()
		  if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
		gamestatus="levelselection"
	end
	
	local hovered = isButtonHovered (creditsForwardButton)
	drawButton (creditsForwardButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  credspeed=1532
	else
	credspeed=32
	end
	end
	
	--credits
--	print(credposition)

		
		
	love.graphics.setColor(1,1,1)
	
	love.graphics.setFont(font3) --if gamestatus=="creditsend" then love.graphics.setFont(font6) end
	love.graphics.print(credit0,300,credposition,0,1)
	love.graphics.print(voiceactors,900,credposition,0,1)

	love.graphics.print(creditmusic1,300,credposition+15700,0,1)
	love.graphics.print(creditmusic2,1000,credposition+15700,0,1)

	love.graphics.setFont(font1)	--if gamestatus=="creditsend" then love.graphics.setFont(font3) end
	love.graphics.print(credit1,300,credposition-15,0,1)
	love.graphics.print(credit2,300,credposition+18400,0,1)

end
