
--shadertoy code implement
--modify shaderFileName to the code file you copied.
--if the shader has any channel file, for example channel0 then shaderChannel[1]=love.graphics.newImage(path to the file)


--[[
This function is setting up a Shadertoy implementation in Love2D game engine. It initializes some variables and loads shader files from the "shaders" directory.
The shader files can be modified by changing the value of the "shaderFileName" variable.
If there are any channel files used in the shader code, they can be loaded using the "shaderChannel" table.
In this example, there are three channels commented out, but they can be uncommented and replaced with appropriate image files.
The "resolution" table sets the resolution of the shader output to 8x6 pixels, which is a low resolution that can improve performance.
--]]
local resolution={8,6,1} --low resolution for better performance
local files = love.filesystem.getDirectoryItems("shaders")
local currentShader = 0
local shaderFileName="shaders/"..files[currentShader+1]
local shaderChannel={
  --[0]=love.graphics.newImage("channel/tex10.png"),
  --[1]=love.graphics.newImage("channel/tex14.png"),
  --[3]=love.graphics.newImage("channel/tex15.png"),
}

--[[
The function loadShader reads and modifies a shader file located at a specified path. It returns the modified shader code as a string and a table iSystem that contains information about the variables and functions used in the shader code.

The function first initializes an empty table iSystem. It then reads the contents of the shader file located at the specified path, modifies the code by replacing "texture2D" with "Texel", "iTime" with "iGlobalTime", and removes "precision highp float;" from the shader code.

The function then checks for the existence of variables and functions such as iGlobalTime, iChannel, iMouse, and iResolution in the shader code, and updates the iSystem table accordingly. If a variable or function is not defined in the shader code, the function adds an "extern" declaration to the header variable.

Finally, the function concatenates the header, modified shader code, and ender variables, and returns the modified shader code as a string and the iSystem table.
--]]

---- This function takes a path to a shader file as an argument and returns two tables: iSystem and shaderData.
local function loadShader(path)
    -- iSystem will be used to store values that the shader file refers to, such as time and mouse position.
    local iSystem={}
    
     -- header and ender are added to the shaderData to complete it.
    -- header will contain all the declarations of variables that the shader file uses,
    -- while ender contains the function that actually renders the shader.
    local header="\n"
    local ender=[[
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords){ 
vec2 fragCoord = texture_coords * iResolution.xy; 
mainImage( color, fragCoord ); 
return color; 
}]]
 -- Open the shader file and read its contents.
    local file = love.filesystem.newFile(path)
    file:open("r")
    local shaderData = file:read()
    
    -- Replace all instances of "texture2D" with "Texel".
    shaderData = string.gsub(shaderData,"texture2D","Texel")
    
     -- Replace all instances of "iTime" with "iGlobalTime".
	shaderData = string.gsub(shaderData,"iTime","iGlobalTime")
	
	-- Remove the "precision highp float;" line.
	shaderData = string.gsub(shaderData,"precision highp float;","")
	
	  -- Check if the shader file uses iGlobalTime, iChannel, iMouse, and iResolution,
    -- and add their declarations to the header if they are not already present in the shaderData.
    if string.find(shaderData,"iGlobalTime") then
      iSystem.iGlobalTime=0
	  if not string.find(shaderData,"number iGlobalTime") then
		header="\nextern number iGlobalTime;"..header
	  end
    end
    if string.find(shaderData,"iChannel") then
      iSystem.iChannel={}
	  if not string.find(shaderData,"Image iChannel") then
		for k,v in pairs(shaderChannel) do
			header="\nextern Image iChannel"..k..";"..header
		end
	  end
    end
    if string.find(shaderData,"iMouse") then
      iSystem.iMouse = {0,0,-1,-1}
	  if not string.find(shaderData,"vec4 iMouse") then
		header="\nextern vec4 iMouse;"..header
	  end
    end
    if string.find(shaderData,"iResolution") then
      iSystem.iResolution = {resolution[1],resolution[2],1}
	  if not string.find(shaderData,"vec3 iResolution") then
		header="\nextern vec3 iResolution;"..header
	  end
    end

-- Add the header and ender to the shaderData if they are not already present.
	shaderData=header..shaderData
	if not string.find(shaderData,"vec4 effect") then
		shaderData=shaderData.."\n"..ender
	end
	
	-- Return the modified shaderData and iSystem tables.
	return shaderData,iSystem
end
local shaderData	--local shaderData: A variable used to store the loaded shader code and other information such as iResolution, iGlobalTime, iChannel, and iMouse.
local iSystem		--local iSystem: A table used to store information about the system such as iResolution, iGlobalTime, iChannel, and iMouse.
local shader		--local shader: A variable used to store the current shader program.
local canvas		--local canvas: A variable used to create a new off-screen canvas to draw the shader on.


function nextShader(i)	--function nextShader(i): A function that changes the current shader to the next one in the files table. It takes an optional argument i which specifies the number of shaders to advance (positive to advance forward and negative to advance backward).
	-- Calculate the next shader index and set the shader file name accordingly
	currentShader = (currentShader+i)%#files
	shaderFileName="shader/"..files[currentShader+1]
	-- Load the shader and set it
	setShader(i)
end

--function setShader(i): A function that loads and sets the current shader program. It takes an optional argument i which specifies the index of the shader in the files table to load.
-- If the shader fails to load, the function calls nextShader and tries again. The function also sets the iResolution and iChannel uniforms for the shader program.
function setShader(i)	
	--takes an integer parameter called i, with a default value of 1		
	i = i or 1
	-- Load the shader and get the shader data and system variables
	shaderData,iSystem = loadShader(shaderFileName)
	--shaderData,iSystem = loadShader("shader/balls.glsl")
	
	-- Attempt to create a new shader with the shader data
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	
	-- If there was an error creating the shader, print debug information and try the next shader
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	
	-- Create a new canvas
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	
	-- If the shader requires the iResolution variable, set it
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	
	-- If the shader requires any channel textures, set them
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end
-- Call setShader with a default value of 1 to initialize the first shader
setShader(1)


--    function selectshadertoy(select): A function that loads and sets a shader program based on a path specified in the argument select. It behaves similarly to setShader. If the shader fails to load, the function calls nextShader and tries again. The function also sets the iResolution and iChannel uniforms for the shader program.

function selectshadertoy(select)	-- Define a function called selectshadertoy that takes an integer parameter called select
i = i or 1
	
		-- Load the selected shader and get the shader data and system variables
	shaderData,iSystem = loadShader(select)
	
	-- Attempt to create a new shader with the shader data
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	
	-- If there was an error creating the shader, print debug information and try the next shader
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	
	-- Create a new canvas
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	
	-- If the shader requires the iResolution variable, set it
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	-- If the shader requires any channel textures, set them
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end


--[[
The love.resize(w,h) function is a callback function that is called when the window is resized.

This function first checks if the current shader being used (iSystem.iResolution) uses the iResolution variable, which stores the resolution of the canvas. If it does, then the resolution variable is updated with the new width and height of the window.

The iSystem.iResolution variable is then updated with the new resolution value, and the new resolution is sent to the shader using shader:send("iResolution",iSystem.iResolution). Finally, the canvas is recreated with the new resolution using canvas = love.graphics.newCanvas(resolution[1],resolution[2]).

This ensures that the shader always has the correct resolution to work with, even if the window is resized during execution.
--]]

function love.resize(w,h)
	if iSystem.iResolution then
		resolution={love.graphics.getWidth(),love.graphics.getHeight(),1}
		iSystem.iResolution = {resolution[1],resolution[2],1}
		shader:send("iResolution",iSystem.iResolution)
		canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	end
end

--[[
This function is the main update function for the game and is executed every frame.
 The function begins with a series of conditionals to determine which part of the game is currently active, and then updates the corresponding game logic.
 
  
  
--]]
function love.update(dt)

updateStates(dt)

HideShowmouse()			-- Toggle mouse / thumbstick based pointer (second mouse button / thumbstick)


	--The following block of code handles the cutscenes, benchmarking, instructions, and intro states. After this, there is a framerate limiter that sleeps for a certain amount of time depending on the limit set (30 or 60 frames per second).
if gamestatus=="cutscenes" then cutscenesupdate(dt) 
updategetjoystickaxis()
updateButtonFocus(dt)	--sparkles animations


elseif gamestatus=="instructions" then instructionsupdate(dt)
elseif gamestatus=="intro" then introupdate(dt)
--framerate limit
		if limitframerate==30 then if dt < 1/15 then love.timer.sleep(1/17 - dt) end
	elseif limitframerate==60 then if dt < 1/60 then love.timer.sleep(1/40 - dt) end
	end

--The subsequent blocks of code handle various game states such as "briefcasemessage," "fishhouseend," "shipwrecksend," and "gameover," among others.
--These game states update the timer and subtitles as necessary and run different functions depending on the game status.



--[[
This code is part of a game and is executed when the game status is "gameover". It updates various game-related parameters such as timer, timer2, and camera position based on the elapsed time since the last update (dt). It also calls the function updatefpsmetrics to show frames-per-second (FPS) metrics on the screen.

The code checks if the screenshake parameter is set to true and if the timer2 is greater than 0.5 seconds, it sets screenshake to false.

Finally, it checks the status of three different fish in the game and displays a specific game-over subtitle depending on which fish has died and which aleatorygameoversentence has been randomly selected.

Specifically, if fish1status is "dead" and aleatorygameoversentence is 2, then the function subtitlegameoverdarn() is called, and so on for the other two fish and their respective aleatorygameoversentences.
--]]
elseif gamestatus=="gameover" then
	updategetjoystickaxis()
	timer=timer +dt
	timer2=timer2 +dt
	updatefpsmetrics(dt)			-- show FPS metrics
	updatecamerafrommouse()	-- move the camera position when zoomed (with the mouse)
	if screenshake==true then
		if timer2>0.5 then screenshake=false end
	end
	
		if fish1status=="dead" and aleatorygameoversentence==2 then subtitlegameoverdarn()
	elseif fish1status=="dead" and aleatorygameoversentence==14 then subtitlesgameoverparrot()
	elseif fish2status=="dead" and aleatorygameoversentence==12 then subtitlegameoverhereafter()
	end
	updategameover()
-- the "levelcompleted," "options," and "firststart" states.
elseif gamestatus=="levelcompleted" then 	timer = timer + dt 
											updategetjoystickaxis()
											updatecamerafrommouse()	-- move the camera position when zoomed (with the mouse)
											updateButtonFocus(dt)


end
  
--[[
This section of the code is responsible for handling the shader effects on the game screen. Shaders are a type of program that are run on the graphics card, and can be used to create visual effects such as distortion, lighting, and color manipulation.

The code first checks if the variable shader2 is set to true, which indicates that the shader effect is enabled. If it is, the code then checks if the iSystem.iGlobalTime variable is present, and increments it by the dt value (delta time) if screenshake is set to false. However, if screenshake is set to true, the time increment is slowed down to 0.0002.

The iGlobalTime variable is then passed to the shader using the shader:send() function, which sends a value to the shader program. This variable is used to create time-dependent effects, such as animations or color changes.

Next, the code checks if the iSystem.iMouse variable is present, which is used to pass mouse position and button data to the shader program. If the left mouse button is pressed, the iSystem.iMouse variable is updated with the current mouse position, and passed to the shader using the shader:send() function. If the left mouse button is not pressed, the iSystem.iMouse variable is reset to -1.

Finally, the code sets the shader effect using love.graphics.setShader(shader), and draws the canvas with the shader effect applied using love.graphics.draw(canvas).

The two commented lines at the end of the code are not executed, as they are preceded by the end statement which closes the if shader2==true then block. These lines are likely used for debugging or testing purposes, and can be uncommented if needed.
--]]
if shader2==true then
  if iSystem.iGlobalTime then
    if screenshake==false and not (pauseisactive) then
		iSystem.iGlobalTime=iSystem.iGlobalTime+dt
	elseif screenshake==true then							--slow down shader when impact
		iSystem.iGlobalTime=iSystem.iGlobalTime+0.0002
    end
    shader:send("iGlobalTime", iSystem.iGlobalTime)
  end
  if iSystem.iMouse then
    iSystem.iMouse[1],iSystem.iMouse[2] = love.mouse.getPosition()
	iSystem.iMouse[1],iSystem.iMouse[2] = resolution[1]-iSystem.iMouse[1],resolution[2]-iSystem.iMouse[2]
    if love.mouse.isDown(1) then
        if iSystem.iMouse[3]<0 then
          iSystem.iMouse[3],iSystem.iMouse[4] = love.mouse.getPosition()
        end
        shader:send("iMouse", iSystem.iMouse)
    else
        iSystem.iMouse[3],iSystem.iMouse[4] = -1, -1
    end
  end
  --love.window.setTitle(shaderFileName.."; FPS: "..love.timer.getFPS())
  --love.window.setTitle("Fish Fillets remake")
    love.graphics.setShader(shader)
    love.graphics.draw(canvas)
end
end


