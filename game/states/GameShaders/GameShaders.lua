local resolution={8,6,1} --low resolution for better performance
local files = love.filesystem.getDirectoryItems("shaders")
local currentShader = 0
local shaderFileName="shaders/"..files[currentShader+1]
local shaderChannel={
  --[0]=love.graphics.newImage("channel/tex10.png"),
  --[1]=love.graphics.newImage("channel/tex14.png"),
  --[3]=love.graphics.newImage("channel/tex15.png"),
}

local function loadShader(path)
    -- iSystem will be used to store values that the shader file refers to, such as time and mouse position.
    iSystem={}
    
     -- header and ender are added to the shaderData to complete it.
    -- header will contain all the declarations of variables that the shader file uses,
    -- while ender contains the function that actually renders the shader.
    local header="\n"
    local ender=[[
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords){ 
vec2 fragCoord = texture_coords * iResolution.xy; 
mainImage( color, fragCoord ); 
return color; 
}]]
 -- Open the shader file and read its contents.
    local file = love.filesystem.newFile(path)
    file:open("r")
    local shaderData = file:read()
    
    -- Replace all instances of "texture2D" with "Texel".
    shaderData = string.gsub(shaderData,"texture2D","Texel")
    
     -- Replace all instances of "iTime" with "iGlobalTime".
	shaderData = string.gsub(shaderData,"iTime","iGlobalTime")
	
	-- Remove the "precision highp float;" line.
	shaderData = string.gsub(shaderData,"precision highp float;","")
	
	  -- Check if the shader file uses iGlobalTime, iChannel, iMouse, and iResolution,
    -- and add their declarations to the header if they are not already present in the shaderData.
    if string.find(shaderData,"iGlobalTime") then
      iSystem.iGlobalTime=0
	  if not string.find(shaderData,"number iGlobalTime") then
		header="\nextern number iGlobalTime;"..header
	  end
    end
    if string.find(shaderData,"iChannel") then
      iSystem.iChannel={}
	  if not string.find(shaderData,"Image iChannel") then
		for k,v in pairs(shaderChannel) do
			header="\nextern Image iChannel"..k..";"..header
		end
	  end
    end
    if string.find(shaderData,"iMouse") then
      iSystem.iMouse = {0,0,-1,-1}
	  if not string.find(shaderData,"vec4 iMouse") then
		header="\nextern vec4 iMouse;"..header
	  end
    end
    if string.find(shaderData,"iResolution") then
      iSystem.iResolution = {resolution[1],resolution[2],1}
	  if not string.find(shaderData,"vec3 iResolution") then
		header="\nextern vec3 iResolution;"..header
	  end
    end

-- Add the header and ender to the shaderData if they are not already present.
	shaderData=header..shaderData
	if not string.find(shaderData,"vec4 effect") then
		shaderData=shaderData.."\n"..ender
	end
	
	-- Return the modified shaderData and iSystem tables.
	return shaderData,iSystem
end
local shaderData	--local shaderData: A variable used to store the loaded shader code and other information such as iResolution, iGlobalTime, iChannel, and iMouse.
local iSystem		--local iSystem: A table used to store information about the system such as iResolution, iGlobalTime, iChannel, and iMouse.
local shader		--local shader: A variable used to store the current shader program.
local canvas		--local canvas: A variable used to create a new off-screen canvas to draw the shader on.


function nextShader(i)	--function nextShader(i): A function that changes the current shader to the next one in the files table. It takes an optional argument i which specifies the number of shaders to advance (positive to advance forward and negative to advance backward).
	-- Calculate the next shader index and set the shader file name accordingly
	currentShader = (currentShader+i)%#files
	shaderFileName="shader/"..files[currentShader+1]
	-- Load the shader and set it
	setShader(i)
end

--function setShader(i): A function that loads and sets the current shader program. It takes an optional argument i which specifies the index of the shader in the files table to load.
-- If the shader fails to load, the function calls nextShader and tries again. The function also sets the iResolution and iChannel uniforms for the shader program.
function setShader(i)	
	--takes an integer parameter called i, with a default value of 1		
	i = i or 1
	-- Load the shader and get the shader data and system variables
	shaderData,iSystem = loadShader(shaderFileName)
	--shaderData,iSystem = loadShader("shader/balls.glsl")
	
	-- Attempt to create a new shader with the shader data
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	
	-- If there was an error creating the shader, print debug information and try the next shader
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	
	-- Create a new canvas
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	
	-- If the shader requires the iResolution variable, set it
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	
	-- If the shader requires any channel textures, set them
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end
-- Call setShader with a default value of 1 to initialize the first shader
setShader(1)


--    function selectshadertoy(select): A function that loads and sets a shader program based on a path specified in the argument select. It behaves similarly to setShader. If the shader fails to load, the function calls nextShader and tries again. The function also sets the iResolution and iChannel uniforms for the shader program.

function selectshadertoy(select)	-- Define a function called selectshadertoy that takes an integer parameter called select
i = i or 1
	
		-- Load the selected shader and get the shader data and system variables
	shaderData,iSystem = loadShader(select)
	
	-- Attempt to create a new shader with the shader data
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	
	-- If there was an error creating the shader, print debug information and try the next shader
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	
	-- Create a new canvas
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	
	-- If the shader requires the iResolution variable, set it
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	-- If the shader requires any channel textures, set them
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end


function love.resize(w,h)
	if iSystem.iResolution then
		resolution={love.graphics.getWidth(),love.graphics.getHeight(),1}
		iSystem.iResolution = {resolution[1],resolution[2],1}
		shader:send("iResolution",iSystem.iResolution)
		canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	end
end

