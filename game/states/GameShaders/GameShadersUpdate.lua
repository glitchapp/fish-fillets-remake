function updateGameStateShaders(dt)

	if showAnalogClock==true then
		updateAnalogClock(dt)
	end

  -- Increment the movement timer
    movementTimer = movementTimer + dt	-- for the movement delay of the gamepad
    
    if nLevel==19 or nLevel==29 or nLevel==37 or nLevel==44 or nLevel==51 or nLevel==58 or nLevel==70 or nLevel==77 then -- if you are in a level with border dialogs
		if not (aleatoryBordersentence==nil) and timer3>4 then	--restore dialogs volume after border dialogs triggered and enought time passed
			SetUpDialogsVolume(1)
			aleatoryBordersentence=nil
		end
	end

	if nLevel==13 or nLevel==20 or nLevel==28 or nLevel==45 or nLevel==59 then	-- if you are in a level with skulls
		if skullpushed==true and SkullDialogSaidOnce==nil and timer>5 then	--if the skull has been pushed and dialogs has not been played yet
			SetUpDialogsVolume(1)	-- restore dialogs volume
			SkullDialogSaidOnce=true	-- set up variable that states that dialogs of skull has been played
			print("dialogs volume restored")
		end
	end
    
	if not (screensaver) then
		updategetjoystickaxis()
		updateRadialMenu()						--update the radial menu
		keymapupdate(dt)
		updateButtonFocus(dt)	--sparkles animations
	end

	
	updatecamerafrommouse()	-- moves the camera position when zoomed in using the mouse.
	
		updatefpsmetrics(dt)			-- shows the frames per second (FPS) metrics.
		
		-- limits the frame rate to 30 or 60 FPS, respectively.
		if limitframerate==30 then if dt < 1/15 then love.timer.sleep(1/19 - dt) end
	elseif limitframerate==60 then if dt < 1/60 then love.timer.sleep(1/40 - dt) end
	end

	-- increases the timer when the screensaver is enabled.
	if screensaver==true then timer = timer +dt end
		
		--increases the timer when the screen is not shaking and the talkies feature is enabled.
		if screenshake==false and talkies==true then 
			
			--if the puase if false move timer forward
			if pauseisactive==false then
				timer = timer + dt 
			end
	--stops the screen shake after a set amount of time.
	elseif screenshake==true then
			if timer2>0.5 then screenshake=false 
			end
		end

		-- rotates the loading icon if the game is currently loading.
		if isloading==true then loadingupdate(dt) fadeloadingprocess(dt) end	-- rotate loading icon if loading
		--triggers the loading process.
		if loadingprocess==true then triggerloadingprocess(dt) end

		--sets the highlight for selected fish to false if the timer has exceeded one second.
		if timer2>1 then fish1highlight=false fish2highlight=false  end

if autoload then lovebpmupdate(dt) Talkies.update(dt) end -- timer for the water warp shader

--increases the lip sync timer when either fish1 or fish2 is talking.
if fish1status=="talking" or fish2status=="talking" then lipstimer=lipstimer+0.001
													
end	--lipsync timer

timer2=timer2 +dt
timer3 = timer3 + dt

smallfishupdate(dt)	-- update frames for the small fish animation
bigfishupdate(dt)	-- update frames for the big fish animation
		
		--bore scripts
		--inputtime increases by dt and if inputtime is greater than 180 and boresentencestriggered is false, then boresentencestriggered is set to true.
		inputtime=inputtime +dt
		if inputtime>180 and boresentencestriggered==false and boresentencesinterrupted==false and gamestatus=="game" then boresentencestriggered=true
					-- trigger subtitles
					timer=0
					stepdone=0
					if talkies==true then Talkies.clearMessages() Obey.borescript() end
		
					
		end
		--If boresentencestriggered is true and boresentencesinterrupted is false, subtitlesborescript() is called.
		if boresentencestriggered==true and boresentencesinterrupted==false then subtitlesborescript() end		-- subtitlesborescript
--[[
This is a section of the code for a game that updates the behavior of objects in different levels of the game. The variable nLevel is a numeric value that indicates the current level of the game. Each line of the code is an if statement that checks if nLevel matches a certain value. If it does, then a specific function is called with the argument dt.

For example, if nLevel is equal to 1, 34, or 36, then the whaleupdate(dt) function is called. If nLevel is equal to 3, 16, 32, 35, or 101, then the ffish1update(dt) and fishsupdate(dt) functions are called.

There are many more if statements in the code that update the behavior of different objects in different levels of the game. Some levels update the behavior of multiple objects, while others only update one object.

The last if statement in the code sets a condition for nLevel equal to 66 and timer2 greater than 2. If the condition is met, then timer2 is set to 0.
--]]
if not (pauseisactive==true) then 
if nLevel==1 or nLevel==34 or nLevel==36 then whaleupdate(dt) updateBoids(dt) end
if nLevel==2 then updateBoids(dt) end
if nLevel==3 or nLevel==16 or nLevel==32 or nLevel==35 or nLevel==101 then ffish1update(dt) fishsupdate(dt) 
updateBoids(dt)	-- update boid library
end
if nLevel==4 then strangecreatureupdate(dt) updateBoids(dt) end
if nLevel==5 then snailupdate(dt) crabupdate(dt) end
if nLevel==6 then broomupdate(dt) end
if nLevel==7 then crabupdate(dt) snailupdate(dt) dolphinupdate(dt) updateBoids(dt) end
if nLevel==9 then periscopeupdate(dt) end
if nLevel==10 or nLevel==18 then boatwindowupdate(dt) boatglassupdate(dt) glastimer=glastimer+dt end
if nLevel==11 then snailupdate(dt) updateBoids(dt) end
if nLevel==12 then medusaupdate(dt) rybickaupdate(dt) end
if nLevel==14 then eyeupdate(dt) end
if nLevel==15 then sharkupdate(dt) end
if nLevel==16 then snailupdate(dt) updateBoids(dt) end
if nLevel==11 then deadparrotupdate(dt) elkupdate(dt) end
if nLevel==13 then vikingupdate(dt) snailupdate(dt) updateBoids(dt) end
if nLevel==15 then dalekohledupdate(dt) budikupdate(dt) snailupdate(dt) mikroskopupdate(dt) updatebubbles(dt) updateAnalogClock(dt) updateBoids(dt)
--updateBoids(dt)
 end
if nLevel==17 then vikingupdate(dt) updateBoids(dt) end
if nLevel==19 then neptunupdate(dt) poseidonupdate(dt) updateBoids(dt) end
if nLevel==20 or nLevel==28 then crossupdate(dt) faceupdate(dt) updateBoids(dt) end	--animate face and cross level 20
if nLevel==21 then statueupdate(dt) crabupdatelong(dt) updateBoids(dt) end	--animate talking sculpture level 21
if nLevel==22 then faceupdate(dt) end --animate face sculpture level 22
if nLevel==7 or nLevel==14 or nLevel==19 or nLevel==20 or nLevel==21 or nLevel==29 or nLevel==30 or nLevel==31 or nLevel==34 or nLevel==36 or nLevel==102 then 
updateBoids(dt) --fishsupdate(dt)
 end
 if nLevel==25 then updateBoids(dt) end
if nLevel==27 then crabupdatelong(dt) crab2update(dt) shrimpupdate(dt) ballupdate(dt) antickaupdate(dt) statueupdate(dt) end
if nLevel==29 then crabupdatelong(dt) snailupdate(dt) faceupdate(dt) updateBoids(dt) end
if nLevel==32 then sasankaupdate(dt) seahorseupdate(dt) updateBoids(dt) end
if nLevel==33 then crabupdate(dt) seahorseupdate(dt) updateBoids(dt) end
if nLevel==34 then sasankaupdate(dt) crabupdatemiddle(dt) sepieupdate(dt) snaillongupdate(dt) balalupdate(dt) end
if nLevel==35 then mantarayupdate(dt)  sasankaupdate(dt) sepieupdate(dt) crabupdatemiddle(dt) klavirupdate(dt) rejnokupdate(dt) end
if nLevel==37 then zupdate(dt) end
if nLevel==38 then timer3=timer3+dt updateBoids(dt) end
--if nLevel==39 then updateBoids(dt) end
if nLevel==40 then washmachineupdate(dt) updateBoids(dt) end
if nLevel==41 then updateBoids(dt) end
if nLevel==42 then pinkmonsterupdate(dt) updateBoids(dt)  end
if nLevel==43 then medusaupdate(dt) mnohonozkaupdate(dt) budikupdate(dt) medusaupdate(dt) meduzaupdate(dt) strangecreatureupdate(dt) uhorupdate(dt) balonekupdate(dt) updateBoids(dt)  end
if nLevel==44 then pinkmonsterupdate(dt) barelupdate(dt) bagetupdate(dt) doubleupdate(dt) hadupdate(dt) hlubinnaupdate(dt) kachnaupdate(dt) mutantkrabupdate(dt) kukajdaupdate(dt) killerupdate(dt) mutantsharkupdate(dt) nohaupdate(dt) mutanteyeupdate(dt)
updateBoids(dt) 
 end
if nLevel==45 or nLevel==49 then chobotniceupdate(dt) papouchaupdate(dt) end
if nLevel==46 then snehulakupdate(dt) end
if nLevel==47 then deloupdate(dt) end
if nLevel==50 then krystalcupdate(dt) drahokamupdate(dt) lebzaupdate(dt) okoupdate(dt) end
if nLevel==52 or nLevel==54 or nLevel==56 or nLevel==57 or nLevel==58 then ffish1update(dt) fishsupdate(dt) updateBoids(dt) end
if nLevel==57 then thinjarupdate(dt) porganismupdate(dt) qorganismupdate(dt) okaupdate(dt) malaupdate(dt) mutantupdate(dt) sklenaupdate(dt) horni_tvorupdate(dt) lahvacupdate(dt) lahvaccrashupdate(dt) lahvaccrashedupdate(dt) end
if nLevel==58 then pohonupdate(dt) ufoupdate(dt) hadiceupdate(dt) plutonium4update(dt) podstavecupdate(dt) end
if nLevel==59 then totemupdate(dt) seahorseupdate(dt) end
if nLevel==60 then krystalupdate(dt) end
if nLevel==61 then korunaupdate(dt) krystalupdate(dt) updateBoids(dt) end
if nLevel==62 then korunaupdate(dt) krystalupdate(dt) end
if nLevel==63 then dasfishupdate(dt) end
if nLevel==64 then krystalupdate(dt) end
if nLevel==66 and timer2>2 then timer2=0 end	-- second timer to animate objects
if nLevel==72 then nahoreupdate(dt) velrybupdate(dt) semaforupdate(dt) konikupdate(dt) seahorseupdate(dt) timer3=timer3+dt end
if nLevel==77 then linuxakupdate(dt) end
if nLevel==103 or nLevel==103 then love.graphics.setColor(1,1,1,0.85) love.graphics.setColor(1,1,1,0.85) whaleupdate(dt) fishsupdate(dt) end

--[[This code block checks whether the boresentencestriggered and boresentencesinterrupted variables are false. If they are, it updates the subtitles based on the level and the player's progress. If talkies is true, and the nLevel is 1, it syncs the subtitles with the player's progress in level 1. If the chair is not touched yet, it checks the state of the pipe, and plays the corresponding subtitle message. If the chair is touched, it plays a different subtitle message. --]]
	if boresentencestriggered==false and boresentencesinterrupted==false then
		--update subtitles
		if talkies==true then
		if nLevel==1 then 			-- sync subtitles for level 1
				if chairtouched==false then
				
							if pipetouched==false and pipepushed==false and pipefalled==true then subtitles1() 
						elseif pipetouched==true and pipepushed==false then subtitles1Icantgetthrough() 
						elseif pipepushed==true and wowsaidonce==false then subtitles1wowyoumovedit()
						end
			
			elseif chairtouched==true then subtitles1damnit()
			end
			
						
			
--[[
This code is a series of conditional statements that check for the current level of the game, and then call the corresponding function to update the subtitles and dubs for that level. Each level has its own specific function for syncing the subtitles and dubs, which is called based on the value of the nLevel variable. The code also updates the animation of the pink monster and the alien engine key for levels 52 and 54, respectively.

For example, the subtitlessync2() function is called when nLevel is 2 and the briefcaseclosed flag is set to true, indicating that the briefcase has been closed. On the other hand, the subtitle2thedisk() function is called when nLevel is 2 and the briefcaseclosed flag is set to false, indicating that the briefcase has been opened.
	--]]	 
		 
	
	elseif nLevel==2 and briefcaseclosed==true then subtitlessync2() -- sync subtitles for level 2
	elseif nLevel==2 and briefcaseclosed==false then subtitle2thedisk() -- sync subtitles for level 2 after briefcase
	elseif nLevel==3 and uarestanding==false and booklvl3pushed==false and shortpipepushed==false then subtitle3()
	elseif nLevel==3 and uarestanding==true and booklvl3pushed==false then subtitle3youarestanding()
	elseif nLevel==3 and booklvl3pushed==true then subtitle3wewillgiveyouahint()
	elseif nLevel==3 and savingpositions==true then subtitle3savingpositions()
	elseif nLevel==3 and aproachaxe==true then subtitle3aproachaxe()
	elseif nLevel==4 then subtitle4()
	elseif nLevel==5 then subtitle5()
	elseif nLevel==6 then subtitle6()
	elseif nLevel==7 and matresspushed==false then subtitle7()
	elseif nLevel==7 and matresspushed==true then subtitle7part2()
	elseif nLevel==8 then subtitle8()
	elseif nLevel==9 then subtitle9()
	elseif nLevel==10 then subtitle10()
	elseif nLevel==11 then subtitle11()
	elseif nLevel==12 then subtitle12()
	elseif nLevel==13 and (skullpushed==false or SkullDialogSaidOnce==true) then subtitle13()
	elseif nLevel==14 then subtitle14()
	elseif nLevel==13 and skullpushed==true then subtitle13skull()
	elseif nLevel==15 then subtitle15()
	elseif nLevel==16 then subtitle16()
	elseif nLevel==17 then subtitle17()
	elseif nLevel==18 then subtitle18()
	elseif nLevel==19 then subtitle19()
	elseif nLevel==20 and (skullpushed==false or SkullDialogSaidOnce==true) then subtitle20()
	elseif nLevel==20 and skullpushed==true then subtitle20skull()
	elseif nLevel==21 then subtitle21()
	elseif nLevel==22 then subtitle22()
	elseif nLevel==23 and zeusfalled==false then subtitle23()
	elseif nLevel==23 and zeusfalled==true then subtitle23zeus()
	elseif nLevel==24 then subtitle24()
	elseif nLevel==25 then subtitle25()
	elseif nLevel==26 then subtitle26() lovebpmupdate(dt)
	elseif nLevel==27 and dooropened==false then subtitle27()
	elseif nLevel==27 and dooropened==true then subtitle27door()
	elseif nLevel==28 and (skullpushed==false or SkullDialogSaidOnce==true) then subtitle28()
	elseif nLevel==28 and skullpushed==true then subtitle28skull()
	elseif nLevel==29 then subtitle29()
	elseif nLevel==30 and crabtouched==true and not (crabfalled) then subtitle30crab()
	elseif nLevel==30 and crabfalled==true then subtitle30crabups()
	elseif nLevel==30 then subtitle30()
	elseif nLevel==31 then subtitle31()
	elseif nLevel==32 then subtitle32()
	elseif nLevel==33 then subtitle33()
	elseif nLevel==34 then subtitle34()
	elseif nLevel==35 then subtitle35()
	elseif nLevel==36 then subtitle36()
	elseif nLevel==37 then subtitle37()
	elseif nLevel==38 then subtitle38()
	elseif nLevel==39 then subtitle39()
	elseif nLevel==40 then subtitle40()
	elseif nLevel==41 then subtitle41()
	elseif nLevel==42 then subtitle42()
	elseif nLevel==43 then subtitle43()
	elseif nLevel==44 then subtitle44()
	elseif nLevel==45 then subtitle45()
	elseif nLevel==45 and (skullpushed==false or SkullDialogSaidOnce==true) then subtitle45()
	elseif nLevel==45 and skullpushed==true then subtitle45skull()
	elseif nLevel==46 then subtitle46()
	elseif nLevel==47 then subtitle47()
	elseif nLevel==48 then subtitle48()
	elseif nLevel==49 then subtitle49()
	elseif nLevel==50 then subtitle50()
	elseif nLevel==51 then subtitle51()
	elseif nLevel==52 then subtitle52()	pinkmonsterupdate(dt) -- animate pink monster
	elseif nLevel==53 then subtitle53()
	elseif nLevel==54 and enginepushed==false and engineononce==false and aproachengineonce==false then subtitle54()
	elseif nLevel==54 and enginepushed==true then subtitle54engineon() alienenginekeyupdate(dt) -- animate key
	elseif nLevel==54 and enginepushed==false and engineononce==true then subtitle54engineoff()
	elseif nLevel==54 and enginepushed==false and aproachengineonce==true then subtitle54aproachengine()
	elseif nLevel==55 then subtitle55()
	elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==true and aproachrobodog==false then subtitle56lightswitchon()
	elseif nLevel==56 and lightswitchon==false then subtitle56lightswitchoff()
	elseif nLevel==56 and aproachrobodog==true and lightswitchon==true then subtitle56aproachrobodog()
	elseif nLevel==57 and aproach3eyes==false then subtitle57()
	elseif nLevel==57 and aproach3eyes==true then subtitle57_3eyes()
	elseif nLevel==58 then subtitle58()
	elseif nLevel==59 and skullbottom_pushed==true then subtitle59skull()
	elseif nLevel==59 and skullupleft_pushed==true then subtitle59skull2()
	elseif nLevel==59 and skullupright_pushed==true then subtitle59skull3()
	elseif nLevel==59 and skullmidright_pushed==true then subtitle59skull4()
	elseif nLevel==59 and bigskull_pushed==false or SkullDialogSaidOnce==true then subtitle59()
	elseif nLevel==60 then subtitle60() 
	elseif nLevel==61 then subtitle61() 
	elseif nLevel==62 then subtitle62() 
	elseif nLevel==63 then subtitle63() 
	elseif nLevel==64 then subtitle64()
	elseif nLevel==65 then subtitle65()
	elseif nLevel==66 then subtitle66()
	elseif nLevel==67 then subtitle67()
	elseif nLevel==68 then subtitle68()
	elseif nLevel==69 then subtitle69()
	elseif nLevel==70 then subtitle70()
	elseif nLevel==71 then subtitle71()
	elseif nLevel==72 then subtitle72()
	elseif nLevel==73 then subtitle73()
	elseif nLevel==74 then subtitle74()
	elseif nLevel==75 then subtitle75()
	elseif nLevel==76 then subtitle76()
	elseif nLevel==77 then subtitle77()
	--elseif nLevel==78 then subtitle78()
	elseif nLevel==79 then subtitle79()
	end	
	end
	end
end
end
