
function updateStates(dt)

		if gamestatus == "levelEditor" then loveUpdateLevelEditor(dt)
	elseif gamestatus=="game" then updateGameStateShaders(dt) 
	elseif gamestatus=="gameplusoptions" then 
	updategetjoystickaxis()
	updateRadialMenu()	
	if screenshottaken==true then			--if a screenshot has been taken
		updatetakescreenshot()
	end
	updateButtonHeld(dt)
	elseif gamestatus=="options" then updateOptions(dt)
	elseif gamestatus=="splashScreen" then updateSplashScreen(dt)
	elseif gamestatus=="firststart" then updateFirstStart(dt)
	elseif gamestatus=="briefcasemessage" then updateBriefcaseMessage(dt)
	elseif gamestatus == "credits" then updateCredits(dt)
	elseif gamestatus == "creditsend" then updateCreditsEnd(dt)
	elseif gamestatus == "music" then updateMusic(dt)
	elseif gamestatus == "tetris" then updateTetris(dt)
	elseif gamestatus=="levelselection" or gamestatus=="levelselection2" then updateLevelSelection(dt)
	elseif gamestatus=="endmenu" or gamestatus=="fishhouseend" or gamestatus=="shipwrecksend" or gamestatus=="silversshipend" or gamestatus=="cityinthedeepend" or gamestatus=="ufoend" or gamestatus=="coralreefend" or gamestatus=="treasurecaveend" or gamestatus=="barrelend" or gamestatus=="secretcomputerend" or gamestatus=="linuxend" or gamestatus=="gameend" then updateEnds(dt)
	elseif gamestatus=="extras" then updateExtras(dt)
	elseif gamestatus=="benchmark" then benchmarkupdate(dt)
	elseif gamestatus=="levelcompleted" then updateLevelCompleted(dt)
	
	end
end




