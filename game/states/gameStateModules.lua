-- main.lua
function loadStates()
    local stateFolders = {
        "SplashScreen",
        "FirstStart",
        "Tetris",
        -- Add more state folders here
    }

    for _, folderName in ipairs(stateFolders) do
        local stateModule = require("game/states/" .. folderName .. "/" .. folderName)
        registerGameStateModule(folderName, stateModule)
    end
end

function updateStates(dt)
    if gameStateModules[gamestatus] then
        gameStateModules[gamestatus].update(dt)
    end
end

function drawStates()
    if gameStateModules[gamestatus] then
        gameStateModules[gamestatus].draw()
    end
end

-- gameStateModules.lua
gameStateModules = {}

function registerGameStateModule(name, module)
    gameStateModules[name] = module
end
