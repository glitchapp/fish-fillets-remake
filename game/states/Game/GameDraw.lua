
function drawGamestate()

	-- Draw background with music visualizer
	lovebpmdraw()

	-- Draw 3D graphics if enabled
	if threeD==true and android==false then
		threeDdraw()
	end

-- draw the blue noise shader if enabled
if shader1type=="bluenoise" then
 -- Set the dithering shader
    love.graphics.setShader(blueNoiseDitherShader)

    -- Pass the intensity value to the shader
    blueNoiseDitherShader:send("intensity", ditherIntensity)
				if skin=="remake" then
					drawallcontent()
					drawforshader2()
			elseif skin=="retro" then
					love.graphics.setShader()
					drawforretro()
			elseif skin=="classic" then
					drawallcontentclassic()
					drawforshader2classic()
			end
	  -- Reset the shader
    love.graphics.setShader()
else

-- Draw first shader if enabled (Moonshine)
	if shader1==true and shader2==false then
		love.graphics.setShader()
		effect(function()
			-- Draw different content depending on the selected skin
				if skin=="remake" then
					drawallcontent()
					drawforshader2()
			elseif skin=="retro" then
					love.graphics.setShader()
					drawforretro()
			elseif skin=="classic" then
					drawallcontentclassic()
					drawforshader2classic()
			end
		end)

	end
	
-- Draw two shaders if enabled
	if shader1==true and shader2==true then
		effect(function()
			-- Draw different content depending on the selected skin
			if skin=="remake" then
				drawallcontent()
				love.graphics.setShader(shader)
				drawforshader2()
			elseif skin=="retro" then
				drawforretro()
			elseif skin=="classic" then
				drawallcontentclassic()
				love.graphics.setShader(shader)
				drawforshader2classic()
			end

			love.graphics.setShader()
						

		end)
		
	end

-- Draw second shader if enabled (shadertoy)
	if shader1==false and shader2==true then
		-- Draw different content depending on the selected skin
		if skin=="remake" then
			drawallcontent()
			love.graphics.setShader(shader)
			drawforshader2()
		elseif skin=="retro" then
			drawforretro()
		elseif skin=="classic" then
			drawallcontentclassic()
			love.graphics.setShader(shader)
			drawforshader2classic()
		end
		love.graphics.setShader()
		-- If talkies are enabled and shader1 is disabled, draw dialogs

	end
	
	-- If both shaders are disabled
	if shader1==false and shader2==false then
		-- Draw different content depending on the selected skin
		if skin=="remake" then
			drawforshader2()
		elseif skin=="retro" then
			drawforretro()
		elseif skin=="classic" then
			drawforshader2classic()
			drawallcontentclassic()
		end
		-- Set color if palette is 7
		if diorama==false then
			if palette==7 then
				love.graphics.setColor(0,0,0)
			end
		end
		
		love.graphics.setShader()


	end
end



	-- Draw touch interface
if touchinterfaceison==true then drawtouchinterface() end
--if touchinterfaceison==true then touchcontrolsdraw() end	-- render touch controls

-- If the game is loading, draw the loading icon
if isloading==true then loadingdraw(dt) end

fadehint()	-- Draw hints on specific levels (check game/interface/loading.lua & hint.lua)
fadescreenshotinformation()	-- information when screenshot is taken

if pauseisactive then 
	love.graphics.setFont(poorfish)
	love.graphics.print("Pause", 800, 900, 0, 0.8, 1)
end

if gamestatus == "gameplusoptions" then		-- Render the options menu over the game screen
		love.graphics.setShader()
		optionsdraw(dt)
	end
end
