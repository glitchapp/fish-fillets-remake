
function updateGameState(dt)
	
lovebpmupdate(dt)                     -- update LoveBPM library
updatefpsmetrics(dt)                  -- update FPS metrics
updategetjoystickaxis()
updateRadialMenu()						--update the radial menu
if networkcontrol then					-- update routines if network controls enabled
	update_controller_state(data)			-- update controls from network control standalone app (remote control)
	receiveincomingmessages()				-- receive network control messages
end

if limitframerate==30 then            -- if frame rate limit is 30 fps, sleep if necessary
    if dt < 1/15 then love.timer.sleep(1/17 - dt) end
elseif limitframerate==60 then        -- if frame rate limit is 60 fps, sleep if necessary
    if dt < 1/60 then love.timer.sleep(1/40 - dt) end
end

if showbattery then                   -- if battery status is enabled, get power info
    powerstate, powerpercent, powerseconds = love.system.getPowerInfo( )
    if powerpercent==nil then         -- if power percentage is nil, set it to infinity
        powerpercent="∞" 
        powerseconds="∞" 
    end
end

if isloading==true then               -- if loading is in progress, update loading icon and fade
    loadingupdate(dt) 
    fadeloadingprocess(dt) 
end
if loadingprocess==true then          -- if loading process is triggered, run the process
    triggerloadingprocess(dt) 
end
if screenshottaken==true then			--if a screenshot has been taken
	updatetakescreenshot()
end

if talkies==true then                 -- if Talkies is enabled, update dialogs
    Talkies.update(dt)
end
if dt > 0.05 then 						-- set a maximum delta time of 0.05 seconds
	dt = 0.05 
end
if threeD==true and android==false then -- if 3D graphics is enabled and not running on Android, update graphics from 3DreamEngine
    dream:update()
end

	if not (aleatoryBordersentence==nil) and time3r>4 then	--restore dialogs volume after border dialogs triggered and enought time passed
		SetUpDialogsVolume(1)
	end

end
