-- If gamestatus is "intro", draw the intro scene
function drawIntro()
-- If the intro video is playing, draw it with the correct aspect ratio
		if introvideoplaying==true then
		if res=="1080p" then love.graphics.draw(introvideo, 0, 0,0,1.33,1)
	elseif res=="1440p" then love.graphics.draw(introvideo, 0, 0,0,1.78,1.33)
	end
	-- If the timer is between 0 and 0.5 seconds and no mouse key has been pressed, prompt to start with a message
	if timer2>0 and timer2<0.5 and inputtime==0 then love.graphics.print("Press any key to start",600,800,0, 0.8, 1) end
	-- Draw FPS metrics
	drawfpsmetrics()
	end
	-- Draw the intro scene player interface
	drawintrosceneplayer()

	
	-- Check if CD is playing and draw equalizer
	if cdplaying then
		equalizerdraw(dt)
	end
end



function drawintrosceneplayer()

pb:drawMouse ()
if hideplayercontrols==false then

	local hovered = isButtonHovered (introReturnButton)
	drawButton (introReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		introvideoplaying=false
		introvideo:pause()
		gamestatus="extras"
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (rewindcutsceneButton)
	drawButton (rewindcutsceneButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		timer=timer-5
		introvideo:seek( timer )
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (playintroButton)
	drawButton (playintroButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		if introvideoplaying==true then
			--introvideoplaying=false
				if introvideo:isPlaying() then introvideo:pause()
			elseif not (introvideo:isPlaying()) then introvideo:play()
			end
			love.timer.sleep( 0.3 )
		elseif introvideoplaying==false then
		 introvideoplaying=true 
		 love.timer.sleep( 0.3 )
		 end
	end
	
	local hovered = isButtonHovered (forwardcutsceneButton)
	drawButton (forwardcutsceneButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		timer=timer+5
		introvideo:seek( timer )
		love.timer.sleep( 0.3 )
	end
		currentplayingtime=string.format("%.2f", introvideo:tell() / 60)
		love.graphics.setFont( poorfishsmall )
		love.graphics.print("Time:" .. currentplayingtime,1250,940,0,1)
	end -- end of hide player controls conditional


	if hideplayercontrols==false then
		local hovered = isButtonHovered (hideplayercontrolsButton)
		drawButton (hideplayercontrolsButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
				hideplayercontrols=true
			love.timer.sleep( 0.3 )
		end
	elseif hideplayercontrols==true then
		local hovered = isButtonHovered (showplayercontrolsButton)
		drawButton (showplayercontrolsButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
				hideplayercontrols=false
			love.timer.sleep( 0.3 )
		end
	
	end
end
