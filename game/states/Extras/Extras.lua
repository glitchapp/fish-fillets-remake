
function extrasload()
	moonshine = require 'assets/shaders/moonshine' -- moonshine shaders
	require("game/states/Cutscenes/ScenePlayer/sceneplayer")
	require("game/states/DragCustomLevel/DragCustomLevel")
	loadsceneplayer()						-- load the scene player
	

local xspace = 150
local yspace = 50
	if language=="de" then returntext="zurück"
elseif language=="es" then returntext="Volver"
elseif language=="en" then returntext="Return"
end
love.graphics.setFont(poorfishsmall)
returntext="Return"

extrasReturnButton = {
	text = returntext,
	x = 1600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


stopbutton = {
	text = "stop",
	x = 100,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

advertising1Button = {
	text = "advertising 1",
	x = 650,
	y = 200+yspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

advertising2Button = {
	text = "advertising 2",
	x = 650,
	y = 200+yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

screensaver1Button = {
	text = "Screensaver 1",
	x = 650,
	y = 200+yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

screensaver2Button = {
	text = "Screensaver 2",
	x = 650,
	y = 200+yspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

screensaver3Button = {
	text = "Screensaver 3",
	x = 650,
	y = 200+yspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

tetrisButton = {
	text = "Tetris",
	x = 650,
	y = 200+yspace*12,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


introButton = {
	text = "Intro",
	x = 650,
	y = 200+yspace*14,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

cutscenesButton = {
	text = "Cutscenes",
	x = 650,
	y = 200+yspace*16,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


alternativecreditsButton = {
	text = "Alternative credits",
	x = 1050,
	y = 200+yspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

levelEditorButton = {
	text = "Level editor",
	x = 1050,
	y = 200+yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

DragCustomLevelButton = {
	text = "Play custom level",
	x = 1050,
	y = 200+yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

ExportSavedGameButton = {
	text = "Export game",
	x = 1050,
	y = 200+yspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

UpdateFFilletsButton = {
	text = "Update FFillets",
	x = 1050,
	y = 200+yspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


end
