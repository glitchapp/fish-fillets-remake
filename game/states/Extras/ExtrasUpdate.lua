
--function extrasupdate(dt) updates the timer and limits the frame rate depending on the value of limitframerate variable. However, it seems that the code is currently commented out.
function extrasupdate(dt)
		--if cdplaying==true then
		--equalizerupdate(dt)
		--end
		timer = timer + dt	-- timer update
		--framerate limit
		if limitframerate==30 then if dt < 1/15 then love.timer.sleep(1/17 - dt) end
	elseif limitframerate==60 then if dt < 1/60 then love.timer.sleep(1/40 - dt) end
	end
end

function updateExtras(dt)

  updategetjoystickaxis()
        extrasupdate(dt)		
        updatefpsmetrics(dt)			
		updateButtonFocus(dt)	--sparkles animations

        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end

        -- If advertising1 is playing, update Talkies and display subtitles
        if advertising1playing == true then
            Talkies.update(dt)
            subtitle21advertising1()
        -- If advertising2 is playing, update Talkies and display subtitles
        elseif advertising2playing == true then
            Talkies.update(dt)
            subtitle21advertising2()
        end


end
