local ddwidth, ddheight = love.window.getDesktopDimensions( display )

function drawExtras()

	-- Draw the extras menu with delta time
	extrasdraw(dt)
	-- If advertising 1 is playing, draw its Talkies dialog
		if advertising1playing==true then Talkies.draw(dt)
	-- If advertising 2 is playing, draw its Talkies dialog
	elseif advertising2playing==true then Talkies.draw(dt)
	end
	-- Draw FPS metrics
	drawfpsmetrics()


end

--[[This is a Lua function called "extrasdraw(dt)" that draws the GUI (graphical user interface) elements for the "Extras" section of a video game. Here's a breakdown of what the code does:

    The first line sets the font to "poorfishsmall", which is likely a small font size for text on the GUI elements.
    The next few lines check if the "extrasReturnButton" should be drawn (based on the "hideplayercontrols" variable) and whether it is currently being hovered over by the mouse. If it is being hovered over and clicked, some audio variables are set to false and a new track is played (if "soundon" and "musicison" are true) before changing the game status to "levelselection".
   
--]]
function extrasdraw(dt)
     love.graphics.setFont(poorfishsmall)
   
		local hovered = isButtonHovered (extrasReturnButton)
		if hideplayercontrols==false then
			drawButton (extrasReturnButton, hovered)
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.audio.stop()
			advertising1playing=false
			advertising2playing=false
			introvideoplaying=false
			PlayingFromLevelEditor=false
		cdplaying=false
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
			love.timer.sleep( 0.3 )
			gamestatus="levelselection"
		end
	
--If "cutscenesunlocked" is true, the "cutscenesButton" is drawn and checked for hovering and clicking. If it is clicked, the game status is set to "cutscenes" and some functions are called to load and play the cutscenes.
       	if cutscenesunlocked==true then
		-- cutscenes
		local hovered = isButtonHovered (cutscenesButton)
		drawButton (cutscenesButton, hovered,cutscenest)
		
				if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Cutscenes", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This menu allows you to hear all the dialogs of the game (you need to unlock the dialogs by completing the correspondent levels first). While playing the dialogs, the tool plays the level's assigned music and render the dialogs over the level´s background.", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
			end
	
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.audio.stop()
			advertising1playing=false
			advertising2playing=false
			introvideoplaying=false
			gamestatus="cutscenes"
			loadcutscenes()
			love.timer.sleep( 0.5 )	
		end
	end

	
-- The "stopbutton" is drawn and checked for hovering and clicking. If it is clicked, various audio variables are set to false and a new track is played before resetting some other variables.	
	if unlockedlevels[21]==true or cutscenesunlocked==true then	-- The stop button will be rendered once level 21 has been solved or the cutscenes has been unlocked
	local hovered = isButtonHovered (stopbutton)
	drawButton (stopbutton, hovered)
	if hovered then
					love.graphics.setFont(font3)
					drawTextContent("Stop advertising audio", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Click here to stop the advertising audios. ", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.audio.stop()
	cdplaying=false
		advertising1playing=false
		advertising2playing=false
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		currenttrackname=""
		currenttrackdesc=""
		rate=0
		channels=0
	love.timer.sleep( 0.3 )
	end
	end
	
	if unlockedlevels[21]==true then	-- The advertising audio will be unlocked once level 21 has been solved
	
	--The "advertising1Button" and "advertising2Button" are similar to the "extrasReturnButton" and are checked for hovering and clicking. If either button is clicked, some audio variables are set to false and a new track is played (if "soundon" and "musicison" are true) before loading and playing some subtitles and voices for the advertising level.
	local hovered = isButtonHovered (advertising1Button)
	drawButton (advertising1Button, hovered,advertising1Button.text)
	if hovered then
					love.graphics.setFont(font3)
					drawTextContent("Advertising 1", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Funny advertising dialog from the level 21 with music from https://airyluvs.com/. Depending on the language selected the actor / voice over would change ", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and love.mouse.isDown(1) or (isjoystickbeingpressed(joystick,button)) then
	love.audio.stop()
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		if musicison==true then
		--equalizerload("/externalassets/dialogs/extras/advertising1.ogg") end
		advertisingmusic1 = love.audio.newSource( "externalassets/dialogs/extras/advertisingmusic1.ogg","stream" )
		advertisingmusic1:setVolume(0.2)
		advertisingmusic1:play()
		advertising1playing=true
		advertising2playing=false
							nLevel=21
							loadsubtitles()
							loadvoices()				-- load voices	
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev21advertising1() end
		love.timer.sleep( 0.3 )
		end
	end
	
	local hovered = isButtonHovered (advertising2Button)
	drawButton (advertising2Button, hovered, advertising2Button.text)
	if hovered then
					love.graphics.setFont(font3)
					drawTextContent("Advertising 2", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Funny advertising dialog from the level 21 with music from https://airyluvs.com/. Depending on the language selected the actor / voice over would change ", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.audio.stop()
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		if musicison==true and language=="en" then
			advertisingmusic2 = love.audio.newSource( "externalassets/dialogs/extras/advertisingmusic2.ogg","stream" )
			advertisingmusic2:setVolume(0.1)
			advertisingmusic2:play()
			nLevel=21
			advertising1playing=false
			advertising2playing=true
							loadsubtitles()
							loadvoices()				-- load voices	
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev21advertising2() end
		end
		love.timer.sleep( 0.3 )
	end
	end
	
	
--[[
The code seems to define different buttons, such as screensaver1Button, screensaver2Button, screensaver3Button, tetrisButton, and introButton, and checks whether they are currently being hovered over by the user's mouse. If the button is hovered over and the left mouse button is clicked, the code executes certain actions specific to that button.

Based on the code, it appears that the code is controlling multiple buttons, each with their own unique function. Here's a summary of each button's purpose:

screensaver1Button, screensaver2Button, and screensaver3Button: These buttons are likely for selecting a screensaver. When a button is clicked (hovered is true and the left mouse button is down), the nLevel variable is set to a specific value (101, 102, or 103), gamestatus is set to "game", screensaver is set to true, and changelevel() is called.


--]]
	if unlockedlevels[3]==true then	-- The screensaver 1 will be unlocked once level 3 has been solved
	local hovered = isButtonHovered (screensaver1Button)
	drawButton (screensaver1Button, hovered,screensaver1Button.text)
	if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Screensaver 1", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Screensaver using assets from the game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
		end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.audio.stop()
			love.timer.sleep( 0.1 )
			nLevel=101 gamestatus="game" screensaver=true
			changelevel()
	end
	end
	
	if unlockedlevels[21]==true then	-- The screensaver 2 will be unlocked once level 21 has been solved
	local hovered = isButtonHovered (screensaver2Button)
	drawButton (screensaver2Button, hovered,screensaver2Button.text)
	if hovered then
	love.graphics.setFont(font3)
					drawTextContent("Screensaver 2", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Screensaver using assets from the game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.audio.stop()
			love.timer.sleep( 0.1 )
			nLevel=102 gamestatus="game" screensaver=true
			changelevel()
		
	end
	end
	
	if unlockedlevels[34]==true then	-- The screensaver 3 will be unlocked once level 34 has been solved
		local hovered = isButtonHovered (screensaver3Button)
	drawButton (screensaver3Button, hovered,screensaver1Button.text)
	if hovered then
	love.graphics.setFont(font3)
					drawTextContent("Screensaver 3", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Screensaver using assets from the game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.audio.stop()
			love.timer.sleep( 0.1 )
			nLevel=103 gamestatus="game" screensaver=true
			changelevel()
		
	end
	end
	
	if unlockedlevels[65]==true then	-- The tetris mini game will be unlocked once the tetris level has been solved
--	tetrisButton: This button is likely for starting a game of Tetris. When clicked, the code checks if cdplaying is true. If it is, it stops the current audio playback and sets cdplaying to false. If cdplaying is false, the tetris game is started (tetris is set to true), the level selection menu is loaded (require ('lib/blocks/main')), gamestatus is set to "game", and nLevel is set to 103.
	local hovered = isButtonHovered (tetrisButton)
	drawButton (tetrisButton, hovered,tetrisButton.text)
		if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Tetris", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This is a mini game that should be unlocked once you complete the tetris level. It is fully playable and features nice backgrounds and sounds from the game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
		end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		if cdplaying==true then
			love.audio.stop()
			cdplaying=false
				love.timer.sleep( 0.1 )
			else
				
			tetristimer=0
			tetris=true
			tetris = require ('lib/blocks/main')			-- load level selection menu
			blocksload()
			gamestatus="tetris"
			
			nLevel=103
			changelevel()
		end
	end
	end
	
	--introButton: This button is likely for starting an intro video. When clicked, the code loads an intro video file (introvideo = love.graphics.newVideo( "externalassets/video/intro.ogg" )), starts playing the video (introvideo:play()), sets introvideoplaying and hideplayercontrols to true, and sets gamestatus to "intro".
	--if skin=="classic" then
		local hovered = isButtonHovered (introButton)
		drawButton (introButton, hovered, introButton.text)
		if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Intro", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Original fish fillets intro video from altar but upscaled to enhance resolution", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			--mus:stop()
			love.audio.stop()
			introvideo = love.graphics.newVideo( "externalassets/video/intro.ogg" )
			introvideo:play()							
			introvideoplaying=true
			hideplayercontrols=true
			gamestatus="intro"
			love.timer.sleep( 0.3 )
		end

	
	--alternative end credits version
	--This code handles the "Credits" button. It checks if the button is being hovered over and if the left mouse button is pressed. If so, it stops any currently playing music, loads a new audio source for the credits music, sets its volume, and changes the game status to "credits" to switch to the credits screen.
	
	
	if unlockedlevels[79]==true then	-- this make sure that last level has been solved and unlock the final credit scene
	hovered = isButtonHovered (alternativecreditsButton)
	drawButton (alternativecreditsButton, hovered,creditstext)
	
	if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Alternative credits", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This is a modified version of the credits animation, it should be unlocked and triggered once you finish the game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			love.timer.sleep( 0.2 )
			music:stop()
				--alternative end credits version
				loadassetscscene()
				loadcutscenes()
				gamestatus="creditsend" 
			music = love.audio.newSource( "/externalassets/music/mixes/TylerMix.ogg","static" )
			love.audio.play( music )
			 --lovebpmload("/externalassets/music/mixes/TylerMix.ogg")
			 music:play()
			music:setVolume(0.4)
		end
	end
	end
		-- Level editor btton
	hovered = isButtonHovered (levelEditorButton)
	drawButton (levelEditorButton, hovered,levelEditorButton.text)
	if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Level editor", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("Built-in level editor which make easy and possible to create, save, export, test and share custom levels for fish fillets remake and fish fillets Vr", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
					
					drawTextContent("This tool is a work in progress and some functionality may not work as expected", ddwidth/1.5, ddheight/6+300, ddwidth/4, 50)
	end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			timer3=0
			music:stop()
			music = love.audio.newSource( "/externalassets/music/EricMatyas/Hypnotic_Puzzle_loop.ogg","static" )
			love.audio.play( music )
			music:setVolume(0.2)
			--loadLevelEditor()
			loveLoadLevelEditor()
			gamestatus="levelEditor" 
			love.timer.sleep( 0.2 )
			
		end
	end
	
		-- Drag custom level
	hovered = isButtonHovered (DragCustomLevelButton)
	drawButton (DragCustomLevelButton, hovered,DragCustomLevelButton.text)
	if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Drag a custom level", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This functions aims at making easy to load custom levels. Simply drag your saved custom level to this window and the custom level will be automatically loaded and started.", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			timer3=0
			music:stop()
			music = love.audio.newSource( "/externalassets/music/EricMatyas/Hypnotic_Puzzle_loop.ogg","static" )
			love.audio.play( music )
			music:setVolume(0.2)
			DragCustomLevel()
			gamestatus="DragCustomLevel" 
			love.timer.sleep( 0.2 )
			
		end
	end
	

		
		-- Export saved game	This function zip all the files of a saved game to transfer the game to another pc or for cross save to Fish fillets Vr
	local hovered = isButtonHovered (ExportSavedGameButton)
	drawButton (ExportSavedGameButton, hovered,ExportSavedGameButton.text)
	if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Save game as a zip file", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This functions aims at making easy to transfer saved games between game instances. To transfer your game simply copy the zip file to the love save folder of another game", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
	end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			zipMyGame()
			Acceptsound:play()
			love.timer.sleep( 0.2 )
			
		end
	end
	
			-- Update Fish fillets
	local hovered = isButtonHovered (UpdateFFilletsButton)
	drawButton (UpdateFFilletsButton, hovered,UpdateFFilletsButton.text)

		if hovered then 
					love.graphics.setFont(font3)
					drawTextContent("Update game", ddwidth/1.5, ddheight/6, ddwidth/4, 50)
					love.graphics.setFont(poorfishsmall)
					drawTextContent("This option aims at making easier to update the game.", ddwidth/1.5, ddheight/6+50, ddwidth/4, 50)
					drawTextContent("This option uses git to pull last update and will only work if the following conditions apply: 1. You have git installed on your system, 2. You downloaded the game from source (so that git can find the necessary git data). ", ddwidth/1.5, ddheight/6+100, ddwidth/4, 50)
					drawTextContent("Since this is a system script that triggers a git action, the result of the action would only be visible on a console provided that you started the game from a console. Alternative you can run the following commands manually from a terminal / console: 'git pull; cd externalassets/; git pull'   ", ddwidth/1.5, ddheight/6+350, ddwidth/4, 50)
			end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			
			os.execute("git pull; cd externalassets/; git pull")
			Acceptsound:play()
			love.timer.sleep( 0.2 )
			
		end
	end
end
