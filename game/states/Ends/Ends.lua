
function endmenuload()

--negativesound2 = love.audio.newSource( "externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static" )
locksymbol = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/interface/locki.webp")))
require("game/interface/sceneplayer")
loadsceneplayer()						-- load the scene player

		--[[fishhousegraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/0fishhouse/fishhousegraph.webp")))
		shipwrecksgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/1shipwrecks/shipwrecksgraph.webp")))
		silversshipgraph = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/2silversship/silversshipgraph.webp")))
		cityinthedeepgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/3cityinthedeep/cityinthedeepgraph.webp")))
		ufograph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/4ufo/ufograph.webp")))
		coralreefgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/5coralreef/coralreefgraph.webp")))
		treasurecavegraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/6treasurecave/treasurecavegraph.webp")))
		dumpgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/7dump/dumpgraph.webp")))
		secretcomputergraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/8secretcomputer/secretcomputergraph.webp")))
		nextgenerationgraph  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/ends/9nextgeneration/nextgenerationgraph.webp")))
	--]]
local xspace = 150
local yspace = 50
returntext="Return" 
fishhousetext="Fish house"
shipwreckstext="Ship wrecks"
silversshiptext="Silver's ship"
cityinthedeeptext="City in the deep"
ufotext="U.F.O."
coralreeftext="Coral reef"
treasurecavetext="Treasure cave"
dumptext="Dump"
secretcomputertext="Secret computer"
nextgenerationtext="Next generation"
gameendtext="Game end"

	if language=="bg" then 
elseif language=="br" then 
elseif language=="ch" then 
elseif language=="cs" then 
elseif language=="de" then returntext="zurück" 
elseif language=="eo" then 
elseif language=="es" then returntext="Volver"
elseif language=="fr" then 
elseif language=="en" then 
elseif language=="it" then 
elseif language=="nl" then 
elseif language=="pl" then 
elseif language=="pr" then 
elseif language=="ru" then 
elseif language=="sv" then 
elseif language=="sk" then
elseif language=="jp" then returntext="帰ります"
elseif language=="chi" then returntext="回传" 


elseif language=="ko" then returntext="반품" 

elseif language=="thai" then returntext="กลับ" 
elseif language=="tamil" then returntext="திரும்ப" 
elseif language=="hindi" then returntext="वापस करना"

end

optionsReturnButton = {
	text = returntext,
	x = 1600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


fishhouseButton = {
	text = fishhousetext,
	x = 100,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
shipwrecksButton = {
	text = shipwreckstext,
	x = 600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


silversshipButton = {
	text = silversshiptext,
	x = 1100,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cityinthedeepButton = {
	text = cityinthedeeptext,
	x = 100,
	y = 400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ufoButton = {
	text = ufotext,
	x = 600,
	y = 400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

coralreefButton = {
	text = coralreeftext,
	x = 1100,
	y = 400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

treasurecaveButton = {
	text = treasurecavetext,
	x = 100,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

dumpButton = {
	text = dumptext,
	x = 600,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

secretcomputerButton = {
	text = secretcomputertext,
	x = 1100,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

nextgenerationButton = {
	text = nextgenerationtext,
	x = 100,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

gameendButton = {
	text = gameendtext,
	x = 600,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}



end
