
function endmenudraw(dt)




     love.graphics.setFont(poorfishsmall)
    
   
    	
	local hovered = isButtonHovered (optionsReturnButton)
	drawButton (optionsReturnButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		--if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
		love.timer.sleep( 0.3 )
		levelload()
		gamestatus="levelselection"
	end
	
	
	local hovered = isButtonHovered (fishhouseButton)
	drawButton (fishhouseButton, hovered)
	--love.graphics.draw (fishhousegraph, fishhouseButton.x,fishhouseButton.y+100,0,0.2,0.2)
	if fishhouseunlockedend==false then	love.graphics.draw (locksymbol, fishhouseButton.x+50,fishhouseButton.y+100,0,0.2,0.2) end
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if fishhouseunlockedend==true then
			--[[if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
			love.timer.sleep( 0.3 )
			nLevel=8
			loadvoices()				-- load voices
			require("game/dialogs/talkies8")
			if language=="en" then require ("game/dialogs/en/l8en") 		loadsubtitles() end				-- load subtitles
				-- trigger subtitles
						timer=0
						stepdone=0
						if talkies==true then Talkies.clearMessages()
							--Obey.lev51end()
						end
			require ('game/ends/fishhouseend')		-- Load Silver's ship end
			fishhouseendload()
			gamestatus="fishhouseend"
		--]]	
		elseif fishhouseunlockedend==false then 
		 TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
		 love.timer.sleep( 0.3 )
		--negativesound2
		end
	end
	
	local hovered = isButtonHovered (shipwrecksButton)
	drawButton (shipwrecksButton, hovered)
	if shipwrecksunlockedend==false then	love.graphics.draw (locksymbol, shipwrecksButton.x+50,shipwrecksButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (fishhousegraph, shipwrecksButton.x,shipwrecksButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if shipwrecksunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
			love.timer.sleep( 0.3 )
			nLevel=19
			loadvoices()				-- load voices
			loadsubtitles()				-- load subtitles
			require("game/dialogs/talkies19")
				if language=="en" then require ("game/dialogs/en/l19en")
			elseif language=="es" then require ("game/dialogs/es/l19es")
			elseif language=="de" then require ("game/dialogs/de/l19de")
			elseif language=="fr" then require ("game/dialogs/fr/l19fr")
			elseif language=="pl" then require ("game/dialogs/pl/l19pl")
			elseif language=="sv" then require ("game/dialogs/sv/l19sv")
			elseif language=="sl" then require ("game/dialogs/sl/l19sl")
			elseif language=="ru" then require ("game/dialogs/ru/l19ru")
			end

								-- trigger subtitles
							timer=0
							stepdone=0
							Talkies.clearMessages()
							Obey.lev19end()
			require ('game/ends/shipwrecksend')		-- Load Silver's ship end
			shipwrecksendload()
			gamestatus="shipwrecksend"
		elseif shipwrecksunlockedend==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
			--negativesound2
		end
	end
	
	local hovered = isButtonHovered (silversshipButton)
	drawButton (silversshipButton, hovered)
	if silversshipunlockedend==false then	love.graphics.draw (locksymbol, silversshipButton.x+50,silversshipButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (silversshipgraph, silversshipButton.x,silversshipButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if silversshipunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=51
				loadvoices()				-- load voices
				loadsubtitles() 				-- load subtitles		
				if skin=="classic" then love.graphics.setBackgroundColor( 1,1,1,1 ) end	-- set background white on classic end
				require("game/dialogs/talkies51")
	
						-- trigger subtitles
							timer=0
							stepdone=0
							Talkies.clearMessages()
							Obey.lev51end()

			require ('game/ends/silversshipend')		-- Load Silver's ship end
			silversshipendload()
			gamestatus="silversshipend"
		elseif fishhouseunlockedend==false then 
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
			--negativesound2
		end
	end
	
		local hovered = isButtonHovered (cityinthedeepButton)
	drawButton (cityinthedeepButton, hovered)
	if cityinthedeepunlockedend==false then	love.graphics.draw (locksymbol, cityinthedeepButton.x+50,cityinthedeepButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (cityinthedeepgraph, cityinthedeepButton.x,cityinthedeepButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if silversshipunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=29
					loadvoices()				-- load voices
				require("game/dialogs/talkies29")
			if language=="en" then require ("game/dialogs/en/l29en") 		loadsubtitles() end				-- load subtitles
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev29end()
							end
			require ('game/ends/cityinthedeepend')		-- Load Silver's ship end
			cityinthedeependload()
			gamestatus="cityinthedeepend"
	
		elseif silversshipunlockedend==false then
		TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end
	
		local hovered = isButtonHovered (ufoButton)
	drawButton (ufoButton, hovered)
	if ufounlockedend==false then	love.graphics.draw (locksymbol, ufoButton.x+50,ufoButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (ufograph, ufoButton.x,ufoButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if ufounlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then
				theyarehere = love.audio.newSource( "externalassets/music/EricMatyas/theyarehereloop.ogg","stream" )
				love.audio.play( theyarehere)
				theyarehere:setVolume(0.3)
				
			
				lovebpmload("externalassets/dialogs/level55/alarmloop.ogg")
				--music:setBPM(127)
				music:setBPM(48)
				music:play()
				music:setVolume(0.2)
					
			
		
			end
			love.timer.sleep( 0.3 )
			nLevel=58
			loadvoices()				-- load voices
			require("game/dialogs/talkies58")
			if language=="en" then require ("game/dialogs/en/l58en") 		loadsubtitles() end				-- load subtitles
		
			-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								--Obey.lev51end()
							end
			--timer=50
			require ('game/ends/ufoend')		-- Load Silver's ship end
			ufoendload()
			gamestatus="ufoend"
			
			if language=="en" then require ("game/dialogs/en/l58en") 		loadsubtitles() end				-- load subtitles
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev58end()
							end
		
		elseif ufounlockedend==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end
	
	local hovered = isButtonHovered (coralreefButton)
	drawButton (coralreefButton, hovered)
	if coralreefunlockedend==false then	love.graphics.draw (locksymbol, coralreefButton.x+50,coralreefButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (coralreefgraph, coralreefButton.x,coralreefButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if coralreefunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=37
				loadvoices()				-- load voices
				require("game/dialogs/talkies37")
			if language=="en" then require ("game/dialogs/en/l37en") 		loadsubtitles() end				-- load subtitles
					-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev37end()
							end
			require ('game/ends/coralreefend')		-- Load Silver's ship end
			coralreefendload()
			gamestatus="coralreefend"
		elseif coralreefunlockedend==false then
			TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
			love.timer.sleep( 0.3 )
		end
	end	
	

			local hovered = isButtonHovered (treasurecaveButton)
	drawButton (treasurecaveButton, hovered)
	if treasurecaveunlockedend==false then	love.graphics.draw (locksymbol, treasurecaveButton.x+50,treasurecaveButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (treasurecavegraph, treasurecaveButton.x,treasurecaveButton.y+100,0,0.2,0.2)
		if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
			if treasurecaveunlockedend==true then
				if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
					--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
					love.timer.sleep( 0.3 )
					nLevel=64
					loadvoices()				-- load voices
					require("game/dialogs/talkies64")
					if language=="en" then require ("game/dialogs/en/l64en") 		loadsubtitles() end				-- load subtitles
						-- trigger subtitles
								timer=0
								stepdone=0
								if talkies==true then Talkies.clearMessages()
									Obey.lev64end()
								end
		
					require ('game/ends/treasurecaveend')		-- Load Silver's ship end
					treasurecaveendload()
					gamestatus="treasurecaveend"
					
			elseif treasurecaveunlockedend==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
			end
		end

		local hovered = isButtonHovered (dumpButton)
	drawButton (dumpButton, hovered)
	if dumpunlockedend==false then	love.graphics.draw (locksymbol, dumpButton.x+50,dumpButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (dumpgraph, dumpButton.x,dumpButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if dumpunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=44
				loadvoices()				-- load voices
				require("game/dialogs/talkies44")
					if language=="en" then require ("game/dialogs/en/l44en") 		loadsubtitles() end				-- load subtitles
						-- trigger subtitles
								timer=0
								stepdone=0
								if talkies==true then Talkies.clearMessages()
									Obey.lev44end()
								end
					require ('game/ends/barrelend')		-- Load dump end
					barrelendload()
					gamestatus="barrelend"
				
		elseif dumpunlockedend==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
	
	
	
			local hovered = isButtonHovered (secretcomputerButton)
	drawButton (secretcomputerButton, hovered)
	if secretcomputerunlockedend==false then	love.graphics.draw (locksymbol, secretcomputerButton.x+50,secretcomputerButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (secretcomputergraph, secretcomputerButton.x,secretcomputerButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if secretcomputerunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=70
				loadvoices()				-- load voices
				require("game/dialogs/talkies70")
				if language=="en" then require ("game/dialogs/en/l70en") 		loadsubtitles() end				-- load subtitles
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev70end()
							end
		
				require ('game/ends/secretcomputerend')		-- Load Secret computer end
				secretcomputerendload()
				gamestatus="secretcomputerend"
		elseif secretcomputerunlockedend==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	end
		
	
	local hovered = isButtonHovered (nextgenerationButton)
	drawButton (nextgenerationButton, hovered)
	if nextgenerationunlockedend==false then	love.graphics.draw (locksymbol, nextgenerationButton.x+50,nextgenerationButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (nextgenerationgraph, nextgenerationButton.x,nextgenerationButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if nextgenerationunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=77
				loadvoices()				-- load voices
				require("game/dialogs/talkies77")
				if language=="en" then require ("game/dialogs/en/l77en") 		loadsubtitles() end				-- load subtitles
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev77end()
							end
					require ('game/ends/linuxend')		-- Load Silver's ship end
					linuxendload()
					gamestatus="linuxend"
				
		elseif nextgenerationunlockedend==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	
	end
	
		local hovered = isButtonHovered (gameendButton)
	drawButton (gameendButton, hovered)
	if gameendunlockedend==false then	love.graphics.draw (locksymbol, gameendButton.x+50,gameendButton.y+100,0,0.2,0.2) end
	--love.graphics.draw (gameendgraph, gameendButton.x,gameendButton.y+100,0,0.2,0.2)
	if hovered and (love.mouse.isDown(1) or joystick:isGamepadDown("a")) then
		if gameendunlockedend==true then
			if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
				--if musicison==true then lovebpmload("/externalassets/music/EricMatyas/Monkey_Island_Band.ogg") end
				love.timer.sleep( 0.3 )
				nLevel=79
				loadvoices()				-- load voices
				require("game/dialogs/talkies79")
				if language=="en" then require ("game/dialogs/en/l79en") 		loadsubtitles() end				-- load subtitles
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev79end()
							end
					require ('game/ends/gameend')		-- Load Silver's ship end
					gameendload()
					gamestatus="gameend"
				
		elseif gameendunlockedend==false then
				TEsound.play("externalassets/sounds/GUI_Sound_Effects_by_Lokif/negative_2.ogg","static")
				love.timer.sleep( 0.3 )
		end
	
	end
end
