function loadStates()

	require ('game/states/mainStatesUpdate')
	require ('game/states/mainStatesDraw')

	require ('game/states/SplashScreen/SplashScreen')
	require ('game/states/FirstStart/FirstStartUpdate')
	require ('game/states/FirstStart/FirstStartDraw')
	require ('game/states/Tetris/TetrisUpdate')
	require ('game/states/Tetris/TetrisDraw')
	require ('game/states/Benchmark/Benchmark')
	require ('game/states/Benchmark/BenchmarkUpdate')
	require ('game/states/Benchmark/BenchmarkDraw')
	require ('game/states/Game/GameUpdate')
	require ('game/states/Game/GameDraw')
	require ('game/states/Options/Options')
	require ('game/states/Options/OptionsUpdate')
	require ('game/states/Options/OptionsDraw')
	require ('game/states/GamePlusOptions/GamePlusOptionsUpdate')
	--require ('game/states/gameShaders')
	require ('game/states/GameShaders/GameShaders')
	require ('game/states/GameShaders/GameShadersUpdate')
	require ('game/states/GameShaders/GameShadersDraw')
	require ('game/states/BriefcaseMessage/BriefcaseMessage')
	require ('game/states/BriefcaseMessage/BriefcaseMessageUpdate')
	require ('game/states/BriefcaseMessage/BriefcaseMessageDraw')
	require ('game/states/Credits/Credits')
	require ('game/states/Credits/CreditsUpdate')
	require ('game/states/Credits/CreditsDraw')
	require ('game/states/CreditsEnd/CreditsUpdate')
	require ('game/states/CreditsEnd/CreditsEndUpdate')
	require ('game/states/LevelCompleted/LevelCompleted')
	require ('game/states/LevelCompleted/LevelCompletedUpdate')
	require ('game/states/LevelCompleted/LevelCompletedDraw')
	
	require ('game/states/LevelSelection/LevelSelection')
	require ('game/states/LevelSelection/LevelSelectionUpdate')
	require ('game/states/LevelSelection/LevelSelectionDraw')
	--require ('game/states/levelselection2')
	
	require ('game/states/GameOver/GameOver')
	loadgameover()
	require ('game/states/GameOver/GameOverUpdate')
	require ('game/states/GameOver/GameOverDraw')
	require ('game/states/Music/Music')
	require ('game/states/Music/MusicUpdate')
	require ('game/states/Music/MusicDraw')
	require ('game/states/Extras/Extras')
	require ('game/states/Extras/ExtrasUpdate')
	require ('game/states/Extras/ExtrasDraw')
	require ('game/states/ExtrasIntro/ExtrasIntro')
	require ('game/states/ExtrasIntro/ExtrasIntroUpdate')
	require ('game/states/ExtrasIntro/ExtrasIntroDraw')
	require ('game/states/DragCustomLevel/DragCustomLevel')
	require ('game/states/DragCustomLevel/DragCustomLevelUpdate')
	require ('game/states/DragCustomLevel/DragCustomLevelDraw')
	require ('game/states/Ends/Ends')
	require ('game/states/Ends/EndsUpdate')
	require ('game/states/Ends/EndsDraw')
	--require ('game/states/leveleditorMain')
	require ('game/states/LevelEditor/LevelEditor')
	require ('game/states/LevelEditor/LevelEditorUpdate')
	require ('game/states/LevelEditor/LevelEditorDraw')
	--require("game/states/SplashScreen/SplashScreen")		--Splash Screen
	--loadSplashScreen()
	--require("lib/splashes/main")		--Splash Screen
	
	
	
	
end

function updateStates(dt)

		if gamestatus == "levelEditor" then loveUpdateLevelEditor(dt)
	elseif gamestatus=="game" then updateGameStateShaders(dt) 
	elseif gamestatus=="gameplusoptions" then 
	updategetjoystickaxis()
	updateRadialMenu()	
	if screenshottaken==true then			--if a screenshot has been taken
		updatetakescreenshot()
	end

	updateButtonHeld(dt)
	elseif gamestatus=="options" then updateOptions(dt)
	elseif gamestatus=="splashScreen" then updateSplashScreen(dt)
	elseif gamestatus=="firststart" then updateFirstStart(dt)
	elseif gamestatus=="briefcasemessage" then updateBriefcaseMessage(dt)
	elseif gamestatus == "credits" then updateCredits(dt)
	elseif gamestatus == "creditsend" then updateCreditsEnd(dt)
	elseif gamestatus == "music" then updateMusic(dt)
	elseif gamestatus == "tetris" then updateTetris(dt)
	elseif gamestatus=="levelselection" or gamestatus=="levelselection2" then updateLevelSelection(dt)
	elseif gamestatus=="endmenu" or gamestatus=="fishhouseend" or gamestatus=="shipwrecksend" or gamestatus=="silversshipend" or gamestatus=="cityinthedeepend" or gamestatus=="ufoend" or gamestatus=="coralreefend" or gamestatus=="treasurecaveend" or gamestatus=="barrelend" or gamestatus=="secretcomputerend" or gamestatus=="linuxend" or gamestatus=="gameend" then updateEnds(dt)
	elseif gamestatus=="extras" then updateExtras(dt)
	elseif gamestatus=="benchmark" then benchmarkupdate(dt)
	elseif gamestatus=="levelcompleted" then updateLevelCompleted(dt)
	end

end






--||This code is responsible for rendering different screens depending on the current game status.
--[[ If the game status is "levelselection", it will draw the level selection menu with a 3D background and an optional loading icon.
 If the game status is "levelselection2", it will draw the second level selection menu with the same background. 
 If the game status is "levelcompleted", it will draw the level completed screen,
  and if the game status is "gameover", it will draw the game over screen.
   Finally, if the game status is "music", it will draw the music menu, and if the CD is playing, it will draw the equalizer.
   Additionally, the code draws FPS metrics and battery info if enabled. The code assumes that other required functions and variables are defined elsewhere in the code.
--]]

function drawstates()
		if gamestatus=="tetris" then drawTetris()
	elseif gamestatus=="splashScreen" then drawSplashScreen()
	elseif gamestatus=="firststart" then drawFirststart()
	elseif gamestatus=="game" then
		drawGamestate()
		pb:drawMap ()	-- draw exit areas and other elements
	elseif gamestatus=="gameplusoptions" then
		drawGamestate()
		if nLevel==120 then drawReturnToLevelEditor() end	-- button for the playing custom level to return to level editor
	elseif gamestatus=="levelselection" then drawLevelselection()
	elseif gamestatus=="levelselection2" then drawLevelselection2()
	elseif gamestatus=="levelcompleted" then drawLevelcompleted(dt)
	elseif gamestatus=="gameover" then drawGameover()
	elseif gamestatus=="music" then drawMusic()
	elseif gamestatus == "options" then optionsdraw(dt)	
	elseif gamestatus == "credits" then creditsdraw(dt)	--If the game status is "credits", the credits menu is drawn. 
	elseif gamestatus == "creditsend" then	drawcscene() creditsdraw(dt) -- This is like credits but with animations behind the credits and it could be used if the players beats the whole game (end credits)
	elseif gamestatus == "briefcasemessage" then briefcasedraw(dt) --If the game status is "briefcasemessage", the briefcase message is drawn, Talkies dialog is displayed, and the "Press any mouse key to skip" message is shown after 1 second.
	
	--extras menu
	elseif gamestatus=="extras" then drawExtras()
		elseif gamestatus=="intro" then drawIntro()
		elseif gamestatus == "cutscenes" then drawcutscenes(dt)	--If the game status is "cutscenes", the cutscenes are drawn. 
	
	elseif gamestatus == "benchmark" then benchmarkdraw(dt) --If the game status is "benchmark", the benchmark is drawn. 
	elseif gamestatus == "leditor" then leditordraw(dt) --If the game status is "leditor", the level editor is drawn. 
	elseif gamestatus == "assets" then assetsmenudraw() --If the game status is "assets", the assets menu is drawn. 
	elseif gamestatus == "instructions" then instructionsdraw() -- If the game status is "instructions", the instructions are drawn. 
	elseif gamestatus == "levelEditor" then  loveDrawLevelEditor(dt)
	elseif gamestatus == "DragCustomLevel" then  drawDragCustomLevel()
	
	--end scenes
	--[[This code is a conditional statement that checks the value of the variable gamestatus.
	Depending on its value, different functions will be called to draw the end menu for each level of the game.
	The levels include fishhouseend, shipwrecksend, silversshipend, cityinthedeepend, ufoend, coralreefend,
	treasurecaveend, barrelend, secretcomputerend, linuxend, and gameend. 
	For the ufoend level, there is an additional check to see if a moonshine shader effect is enabled. If it is, the scene will be drawn with the effect applied.
	--]]    
	elseif gamestatus == "endmenu" then endmenudraw(dt)
	elseif gamestatus == "fishhouseend" then fishhouseenddraw(dt)
	elseif gamestatus == "shipwrecksend" then shipwrecksenddraw(dt)
	elseif gamestatus == "silversshipend" then silversshipenddraw(dt)
	elseif gamestatus == "cityinthedeepend" then cityinthedeependdraw(dt)
	elseif gamestatus == "ufoend" then
		-- Apply moonshine shaders if enabled
		if moonshineeffect == 1 or moonshineeffect == 3 or moonshineeffect == 4 then
			love.graphics.setShader()
			effect(function()
				ufoenddraw(dt)
			end)
			drawscenetext()
		elseif moonshineeffect == 2 then
			love.graphics.setShader()
			effect(function()
				ufoenddraw(dt)
				drawscenetext()
			end)
		else
			ufoenddraw(dt)
			drawscenetext()
		end
	-- Draw end menu for different levels
	
	elseif gamestatus == "coralreefend" then coralreefenddraw(dt)
	elseif gamestatus == "treasurecaveend" then treasurecaveenddraw(dt)
	elseif gamestatus == "barrelend" then barrelenddraw(dt)
	elseif gamestatus == "secretcomputerend" then secretcomputerenddraw(dt)
	elseif gamestatus == "linuxend" then linuxenddraw(dt)
	elseif gamestatus == "gameend" then gameenddraw(dt)
	end
end
