
function optionsload()
		if res=="1080p" then xspace = 200 yspace = 50
	elseif res=="1440p" then xspace = 200 yspace = 50
	elseif res=="4k" then xspace = 350 yspace = 100
	else  xspace = 250 yspace = 75
	end

optionsloadlanguages()	-- load languages
optionsloadaudio()		-- load sounds
optionsloadcontrols()	-- load controls
optionsloadgraphics()	-- load graphics
optionsloadgraphicshub()	-- load graphics
optionsmenupressed=false	--The variable "optionsmenupressed" is set to false.

	scolor1={1,1,1,0.95} scolor2={1,1,1,0.7}  scolor3={1,1,1,1}
	if chromaab==nil then chromaab=false end		-- avoid chromaab to be nil
	
optionsReturnButton = {
	text = returntext,
	x = xspace*6,
	y = yspace,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

savesettingsButton = {
	text = "Yes",
	x = xspace*2,
	y = yspace*7,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

donotsavesettingsButton = {
	text = "No",
	x = xspace*4,
	y = yspace*7,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
--	font = love.graphics.newFont( 12*3, hinting, dpiscale ),
}

-- Tabs

languagetabButton = {
	text = "Language",
	x = xspace*2,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


graphicstabButton = {
	text = "Graphics",
	x = xspace*3,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

audiotabButton = {
	text = "Audio",
	x = xspace*4,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

controlstabButton = {
	text = "Controls",
	x = xspace*4.8,
	y = yspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
--[[

Finally, if the resolution is 1080p or 1440p, the font size of the UI elements is reduced to "poorfishsmall".
--]]
if res=="1080p" or res=="1440p" then	-- reduce font size below 4k
	optionsReturnButton.font=poorfishsmall 
	savesettingsButton.font=poorfishsmall 
	donotsavesettingsButton.font=poorfishsmall 
	languagetabButton.font=poorfishsmall 
	graphicstabButton.font=poorfishsmall 
	audiotabButton.font=poorfishsmall 
	controlstabButton.font=poorfishsmall 
end

end
