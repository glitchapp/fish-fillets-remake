-- This is a copy the credits update code above mixed with benchmark / cutscene animations that could be used if the player beats the game.
function updateCreditsEnd(dt)
        
   creditsupdate(dt)
        creditsendcutsceneupdate(dt)			-- This is new in this version of the credits and will update animations behind the credits text
        updatefpsmetrics(dt)			
	updateButtonFocus(dt)	--sparkles animations
	
        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end
end


-- This function would eventually be used in an alternative credits scene.
function creditsendcutsceneupdate(dt)
		timer=timer +dt
		smallfishframe=smallfishframe+1*dt
		if smallfishframe>1.45 then smallfishframe=0.001 end
		bigfishframe=bigfishframe+1*dt
		if bigfishframe>1.45 then bigfishframe=0.001 end
		
			if timer >60 and nLevel==1 and endtrackname=="taylormix" then
				nLevel=3
				loadassetscscene()
		elseif timer >120 and nLevel==3 and endtrackname=="taylormix" then
				nLevel=23
				loadassetscscene()
		end
		
		if timer>160 and endtrackname=="taylormix" then 
				love.audio.stop()
				mus= love.audio.newSource( "/externalassets/music/mixes/endmixes/ARobotsWayToHeavenRemix.ogg","stream" )
				love.audio.play( mus )
				endtrackname="arobotswaytoheaven"
				nLevel=30
				loadassetscscene()
				timer=0
		end
		if timer>100 and endtrackname=="arobotswaytoheaven" then 
				love.audio.stop()
				mus= love.audio.newSource( "/externalassets/music/mixes/endmixes/CleytonRX-UnderwaterRemix.ogg","stream" )
				love.audio.play( mus )
				endtrackname="cleytonunderwatermix"
				nLevel=36
				loadassetscscene()
				timer=0
		end
		if timer>140 and endtrackname=="cleytonunderwatermix" then 
				love.audio.stop()
				mus= love.audio.newSource( "/externalassets/music/ViktorKraus/ARobotswaytoHeaven.ogg","stream" )
				love.audio.play( mus )
				endtrackname="arobotswaytoheavenoriginal"
				nLevel=45
				loadassetscscene()
				timer=0
		end
		if timer>170 and endtrackname=="arobotswaytoheavenoriginal" then 
				love.audio.stop()
				mus= love.audio.newSource( "/externalassets/music/mixes/remix2.ogg","stream" )
				love.audio.play( mus )
				endtrackname="remix2"
				nLevel=20
				loadassetscscene()
				timer=0
		end
		if timer>140 and endtrackname=="remix2" then 
				love.audio.stop()
				mus= love.audio.newSource( "/externalassets/music/mixes/remix3.5.ogg","stream" )
				love.audio.play( mus )
				endtrackname="remix3.5"
				nLevel=46
				loadassetscscene()
				timer=0
		end
		
		-- This function exits the credits end scene when pressing the mouse button
		if love.mouse.isDown(0) or love.mouse.isDown(1) or love.mouse.isDown(2) then -- This line checks if the left, right or middle mouse button is pressed.
			love.timer.sleep( 0.3 )
			shader1=false shader2=false inputtime=0	-- This line sets shader1 and shader2 variables to false and inputtime variable to 0.
			music:stop()
			lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")	-- This line loads the background music.
			music:play()	--This line plays the background music.
			nLevel=1		-- This line sets the nLevel variable to 1.
			loadlevelvariables()	--This line loads the variables required for the level.
			levelload()		--This line loads the level.
			
			gamestatus="levelselection" -- This line sets the game status to "levelselection".
			
		end
end
