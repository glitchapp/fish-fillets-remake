function loadsceneplayer()
hideplayercontrols=false
playerspeed=2

poorfish = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 84) -- Thanks @hicchicc for the font
poorfishmiddle = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 64) -- Thanks @hicchicc for the font
poorfishsmall = love.graphics.newFont("/externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32) -- Thanks @hicchicc for the font

require("game/states/Cutscenes/ScenePlayer/Buttons/Controls")
require("game/states/Cutscenes/ScenePlayer/IntroScene/DrawIntroScene")

	if playerplaying==true then playbuttontext="Play"
elseif playerplaying==false then playbuttontext="play"
end

	--chain moonshine effects
			effect = moonshine(1920,1080,moonshine.effects.filmgrain)
					.chain(moonshine.effects.colorgradesimple)
					.chain(moonshine.effects.vignette)
					.chain(moonshine.effects.scanlines)
					.chain(moonshine.effects.crt)
					.chain(moonshine.effects.dmg)
					.chain(moonshine.effects.godsray)
					.chain(moonshine.effects.desaturate)
					.chain(moonshine.effects.pixelate)
					.chain(moonshine.effects.posterize)
					.chain(moonshine.effects.boxblur)
					.chain(moonshine.effects.fastgaussianblur)
					.chain(moonshine.effects.chromasep)
					.chain(moonshine.effects.glow)



end

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	local gx, gy = emulatedmousexscale*48, emulatedmouseyscale*46
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h 
		or ((gx+1) >= x and (gx-1) <= x+w		--Checks whether the mouse is within the bounds of the button.
		and (gy+1) >= y and (gy-1) <= y+h) 
		then	-- Checks whether the y-coordinate of the mouse is within the y-bounds of the button.
		button.w, button.h = w, h
		return true
	end
	return false
end



local function drawButton (button, hovered)
	
	love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)
end




