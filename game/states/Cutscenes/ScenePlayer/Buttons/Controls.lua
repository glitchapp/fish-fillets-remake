introReturnButton = {
	text = returntext,
	x = 1600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

rewindButton = {
	text = "<<",
	x = 600,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

rewindcutsceneButton = {
	text = "<<",
	x = 600,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}



playButton = {
	text = playbuttontext,
	x = 1200,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

playintroButton = {
	text = playbuttontext,
	x = 800,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

forwardButton = {
	text = ">>",
	x = 1000,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

forwardcutsceneButton = {
	text = ">>",
	x = 1000,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


speed2halfButton = {
	text = "/4",
	x = 1200,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

speedhalfButton = {
	text = "/2",
	x = 1200,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

speed1Button = {
	text = "x1",
	x = 1300,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}
speed2Button = {
	text = "x2",
	x = 1400,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}
speed4Button = {
	text = "x4",
	x = 1400,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

effectsoffButton = {
	text = "off",
	x = 1600,
	y = 925, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

effect1Button = {
	text = "1",
	x = 1550,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

effect2Button = {
	text = "2",
	x = 1650,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

effect3Button = {
	text = "3",
	x = 1550,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

effect4Button = {
	text = "4",
	x = 1650,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hideplayercontrolsButton = {
	text = "Hide controls",
	x = 1650,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

showplayercontrolsButton = {
	text = "",
	x = 1650,
	y = 950, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

introReturnButton = {
	text = returntext,
	x = 1600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
