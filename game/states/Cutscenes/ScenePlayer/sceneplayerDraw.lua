

function drawsceneplayer()
pb:drawMouse ()

local hovered = isButtonHovered (rewindButton)
	drawButton (rewindButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		timer=timer-5
		love.timer.sleep( 0.3 )
	end
	if not (gamestatus=="ufoend") then
	local hovered = isButtonHovered (playButton)
	drawButton (playButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		if playerplaying==true then
			playerplaying=false
			love.timer.sleep( 0.3 )
		elseif playerplaying==false then
		 playerplaying=true 
		 love.timer.sleep( 0.3 )
		 end
	end
	end
	local hovered = isButtonHovered (forwardButton)
	drawButton (forwardButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		timer=timer+5
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (speedhalfButton)
	drawButton (speedhalfButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		timer=0
		playerspeed=0.5
	end
	
	local hovered = isButtonHovered (speed2halfButton)
	drawButton (speed2halfButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		timer=0
		playerspeed=0.25
	end
		local hovered = isButtonHovered (speed1Button)
	drawButton (speed1Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		timer=0
		playerspeed=1
	end
	
			local hovered = isButtonHovered (speed2Button)
	drawButton (speed2Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		timer=0
		playerspeed=2
	end
	
		local hovered = isButtonHovered (speed4Button)
	drawButton (speed4Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		timer=0
		playerspeed=4
	end
	
		local hovered = isButtonHovered (effectsoffButton)
	drawButton (effectsoffButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		
		effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
		moonshineeffect=0
	end
	
	
	
		local hovered = isButtonHovered (effect1Button)
	drawButton (effect1Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
			
			--set 1 of moonshine effects		"Scanlines + Vignette"
					effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("crt","scanlines","filmgrain")
						effect.vignette.radius=1.5
						effect.vignette.opacity=2
						effect.dmg.palette=3
				moonshineeffect=1
		 end

	
		local hovered = isButtonHovered (effect2Button)
	drawButton (effect2Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
		     effect.enable("godsray")
              --effect.godsray.samples=3
              moonshineeffect=2
	end
	
			local hovered = isButtonHovered (effect3Button)
	drawButton (effect3Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
		    effect.enable("posterize")
               effect.vignette.radius=1.2
			   effect.vignette.opacity=3
			   effect.dmg.palette=4
			   effect.filmgrain.size=2.5
			  effect.filmgrain.opacity=2
			  
		  moonshineeffect=3
	end
	
			local hovered = isButtonHovered (effect4Button)
	drawButton (effect4Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
		  	effect.enable("pixelate","dmg")
		  	
		  moonshineeffect=4
	end

	love.graphics.print("Effect",1550,975,0,1)
		if moonshineeffect==0 then love.graphics.print("off",1650,975,0,1)
	elseif moonshineeffect==1 then love.graphics.print("Old tv",1650,975,0,1)
	elseif moonshineeffect==2 then love.graphics.print("Gods ray",1650,975,0,1)
	elseif moonshineeffect==3 then love.graphics.print("Posterize",1650,975,0,1)
	elseif moonshineeffect==4 then love.graphics.print("retro fluor",1650,975,0,1)
	end

	love.graphics.print("Speed controls",1250,975,0,1)
end
