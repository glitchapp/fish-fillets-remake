
				
				require("game/states/Cutscenes/CutscenesUpdate")
				require("game/states/Cutscenes/CutscenesDraw")
				
				require("game/states/Cutscenes/Buttons/Area")
				require("game/states/Cutscenes/Buttons/AreaDraw")
				require("game/states/Cutscenes/Buttons/languages")
				require("game/states/Cutscenes/Buttons/languagesDraw")
				require("game/states/Cutscenes/Buttons/Level")
				require("game/states/Cutscenes/Buttons/LevelDraw")
				require("game/states/Cutscenes/Buttons/PlayerControls")
				require("game/states/Cutscenes/Buttons/PlayerControlsDraw")
				require("game/states/Cutscenes/Buttons/VoiceActor")
				
				require("game/states/Cutscenes/Scenes/FishScene")
				require("game/states/Cutscenes/Scenes/LinuxScene")
				require("game/states/Cutscenes/Scenes/MainScene")
				require("game/states/Cutscenes/Scenes/StatueScene")
				
				require("game/states/Cutscenes/Scenes/DrawScript")
				
				CSub={}

function loadassetscutscenes()
				TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
				love.audio.stop( )
				Talkies.clearMessages()
				loadvoices()
				loadsubtitles()
				loadlevelmusic()
				timer=0
				stepdone=0
				smallfishframe=0
				bigfishframe=0
				CSub={}
				
				if nLevel==1 then CutSceneCurrentLine=0
				else CutSceneCurrentLine=1
				end
				--CutSceneTextRotationAngle = 0.02
				--CutSceneTextZoomFactor = 6.02
				--CutScene3DRotationAngle = 0.001
				
				loadsubtitles()
				assignSubtitlesToVariable()
				
				loadassetscscene()
					if nLevel==1 then Obey.lev1()  
				elseif nLevel==2 then Obey.lev2()
				elseif nLevel==3 then Obey.lev3()
				elseif nLevel==4 then Obey.lev4()
				elseif nLevel==5 then Obey.lev5()
				elseif nLevel==6 then Obey.lev6()
				elseif nLevel==7 then Obey.lev7()
				elseif nLevel==8 then Obey.lev8()
				end

				
end


function loadassetscscene()
	

	loadlevelsounds()
	if skin=="remake" then loadlevelassets() shader2=false
			if nLevel==1 then cutfrg = level1frg cutbck = level1bck cutbck2 = level1bck2 cutbck3 = level1frg2
		elseif nLevel==2 then cutfrg = level2frg cutbck = level2bck2 cutbck2 = smask cutbck3 = smask
		elseif nLevel==3 then cutfrg = level3frg cutbck = level3bck cutbck2 = level3bck2 cutbck3 = level3bck3
		elseif nLevel==4 then cutfrg = level4frg1 cutbck = level4bck1 cutbck2 = level4bck2 cutbck3 = smask
		elseif nLevel==5 then cutfrg = level5frg cutbck = level5bck cutbck2 = level5bck2 cutbck3 = level5bck3
		elseif nLevel==6 then cutfrg = level6frg cutbck = level6bck cutbck2 = level6bck2 cutbck3 = smask
		elseif nLevel==7 then cutfrg = level7frg cutbck = level7bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==8 then cutfrg = level8frg cutbck = level8bck cutbck2 = level8bck2 cutbck3 = level8bck3
		elseif nLevel==9 then cutfrg = level9frg cutbck = level9bck cutbck2 = level9bck2 cutbck3 = smask
		elseif nLevel==10 then cutfrg = level10frg cutbck = level18bck cutbck2 = level18bck2 cutbck3 = level18bck3
		elseif nLevel==11 then cutfrg = foreground11 cutbck = UnderwaterBGBlank cutbck2 = level11bck2 cutbck3 = level11frg2
		elseif nLevel==12 then cutfrg = level12frg cutbck = smask cutbck2 = smask cutbck3 = smask
		elseif nLevel==13 then cutfrg = level13frg cutbck = level17bck cutbck2 = level17bck cutbck3 = level17bck3
		elseif nLevel==14 then cutfrg = level14frg2 cutbck = level14frg cutbck2 =  level14bck cutbck3 = smask
		elseif nLevel==15 then cutfrg = level15bck cutbck = level15frg2 cutbck = level15bck2 cutbck2 = level15bck3
		elseif nLevel==16 then cutfrg = level16frg cutbck = level16bck cutbck2 = level16bck2 cutbck3 = smask
		elseif nLevel==17 then cutfrg = level17frg cutbck = level17bck cutbck2 = level17bck cutbck3 = level17bck3
		elseif nLevel==18 then cutfrg = level18frg cutbck = level18bck cutbck2 = level18bck2 cutbck3 = level18bck3
		elseif nLevel==19 then cutfrg = level19frg cutbck = level19bck cutbck2 = level19bck2 cutbck3 = level19bck3
		elseif nLevel==20 then cutfrg = level20frg cutbck = level20bck cutbck2 = level20bck2 cutbck3 = level20bck3
		elseif nLevel==21 then cutfrg = level21frg cutbck = underwater_fantasy1 cutbck2 = smask cutbck3 = smask statueframe=0 loadultrastatue()
		elseif nLevel==22 then cutfrg = level22frg cutbck = smask cutbck2 = level22bck2 cutbck3 = smask
		elseif nLevel==23 then cutfrg = level23bck3 cutbck = level23bck2  cutbck2 =  level23bck cutbck3 = smask
		elseif nLevel==24 then cutfrg = level24frg cutbck = level24bck cutbck2 = smask cutbck3 = smask statueframe=0 loadultrastatue()
		elseif nLevel==25 then cutfrg = level25frg cutbck = level25bck cutbck2 = level25bck2 cutbck3 = smask
		elseif nLevel==26 then cutfrg = level26frg cutbck = level26bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==27 then cutfrg = level27frg cutbck = level27bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==28 then cutfrg = level28frg cutbck = level28bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==29 then cutfrg = level29frg cutbck = level29bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==30 then cutfrg = level30frg cutbck = level30bck cutbck2 = level30bck2 cutbck3 = smask
		elseif nLevel==36 then cutfrg = level36frg cutbck = level36bck cutbck2 = level36bck2 cutbck3 = level36bck3
		elseif nLevel==45 then cutfrg = level45frg cutbck = level45bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==46 then cutfrg = level46frg cutbck = level46bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==60 then cutfrg = level60frg cutbck = level60bck cutbck2 = level60bck2 cutbck3 = smask
		else
			cutfrg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/level3frg.webp")))
			cutbck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/level3bck1.webp")))
			cutbck2 = smask
			cutbck3 = smask
		end
	elseif skin=="classic" then loadlevelassetsclassic() shader2=false
			if nLevel==1 then cutfrg = level1frgcl cutbck = level1bckcl cutbck2 = level1bck2cl cutbck3 = level1bck3cl
		elseif nLevel==2 then cutfrg = level2frgcl cutbck = level2bckcl cutbck2 = level2bck2cl cutbck3 = smask
		elseif nLevel==3 then cutfrg = level3frg cutbck = level3bck cutbck2 = smask cutbck3 = smask
		elseif nLevel==4 then cutfrg = level4frg1 cutbck = level4bck1 cutbck2 = level4bck2 cutbck3 = smask
		elseif nLevel==5 then cutfrg = level5frg cutbck = level5bck cutbck2 = level5bck2 cutbck3 = level5bck3
		elseif nLevel==6 then cutfrg = level6frg cutbck = level6bck cutbck2 = level6bck2 cutbck3 = level6bck3
		elseif nLevel==7 then cutfrg = level7frgcl cutbck = level7bckcl cutbck2 = smask cutbck3 = smask
		elseif nLevel==8 then cutfrg = level8frgcl cutbck = level8bckcl cutbck2 = level8bck2cl cutbck3 = level8bck3cl
		elseif nLevel==9 then cutfrg = level9frgcl cutbck = level9bckcl cutbck2 = smask cutbck3 = smask
		
		elseif nLevel==21 then cutfrg = level21frgcl cutbck = level21bckcl cutbck2 = smask cutbck3 = smask statueframe=0 loadultrastatue()
		elseif nLevel==24 then cutfrg = level24frgcl cutbck = level24bckcl cutbck2 = smask cutbck3 = smask statueframe=0 loadultrastatue()
		
		elseif nLevel==77 then cutfrg = level77frgcl cutbck = level77bckcl cutbck2 = smask cutbck3 = smask statueframe=0 loadultralinuxusers() linuxakframe=0
		else
			cutfrg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/classic/upscaled/level3/level3frg.webp")))
			cutbck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/classic/upscaled/level3/level3bck.webp")))
			cutbck2 = smask
			cutbck3 = smask
		end
	end
end
