-- Voice actor selection

AaroncButton = {
	text = "Aaron Relf",
	x = 1200,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

ishVAcButton = {
	text = "ishVA",
	x = 1600,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


JohncButton = {
	text = "John Combs",
	x = 1200,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

snoozycButton = {
	text = "Snoozy",
	x = 1600,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

labyrinthuscButton = {
	text = "Labyrinthus",
	x = 1200,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

vnaiiscButton = {
	text = "Vn_aiis",
	x = 1600,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


bastiancButton = {
	text = "Bastian Villaroel",
	x = 1200,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}


zeroSkycButton = {
	text = "ZeroSky",
	x = 1600,
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}
