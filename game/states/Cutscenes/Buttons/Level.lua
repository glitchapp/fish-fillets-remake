
cutscenelevel1Button = {
	text = "1",
	x = 300,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel2Button = {
	text = "2",
	x = 350,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel3Button = {
	text = "3",
	x = 400,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel4Button = {
	text = "4",
	x = 450,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel5Button = {
	text = "5",
	x = 500,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel6Button = {
	text = "6",
	x = 550,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel7Button = {
	text = "7",
	x = 600,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel8Button = {
	text = "8",
	x = 650,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel9Button = {
	text = "9",
	x = 700,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel10Button = {
	text = "10",
	x = 750,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cutscenelevel11Button = {
	text = "11",
	x = 800,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
