
fishhouseareaButton = {
	text = "1",
	x = 300,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

shipwrecksareaButton = {
	text = "2",
	x = 350,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cityareaButton = {
	text = "3",
	x = 400,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

coralareaButton = {
	text = "4",
	x = 450,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

dumpareaButton = {
	text = "5",
	x = 500,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

silversshipareaButton = {
	text = "6",
	x = 550,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ufoareaButton = {
	text = "7",
	x = 600,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

treasureareaButton = {
	text = "8",
	x = 650,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

computerareaButton = {
	text = "9",
	x = 700,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ngareaButton = {
	text = "10",
	x = 750,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
