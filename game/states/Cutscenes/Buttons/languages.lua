-- English
encButton = {
	text = "en",
	x = 1200,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- French
frcButton = {
	text = "fr",
	x = 1300,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--Spanish
escButton = {
	text = "es",
	x = 1400,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--acents

-- Britísh
enbrcButton = {
	text = "br",
	x = 1600,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
-- American
enuscButton = {
	text = "us",
	x = 1700,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Spanish (Spain)
esescButton = {
	text = "es",
	x = 1600,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Latin american Spanish
eslacButton = {
	text = "la",
	x = 1700,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- French (France)

frfrcButton = {
	text = "fr",
	x = 1600,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- French (Canada)

frcacButton = {
	text = "ca",
	x = 1700,
	y = 800,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


function rectangleoverlanguagecutscenes()
love.graphics.setColor(1,1,0,0.5)
		if language=="bg" then love.graphics.rectangle("fill",bgButton.x-5,bgButton.y-5,100, 100)
	elseif language=="br" then love.graphics.rectangle("fill",brButton.x-5,brcButton.y-5,100, 100)
	elseif language=="ch" then love.graphics.rectangle("fill",chButton.x-5,chButton.y-5,100, 100)

	elseif language=="de" then love.graphics.rectangle("fill",deButton.x-5,deButton.y-5,100, 100)
			if accent=="ch" then love.graphics.rectangle("fill",chButton.x-5,chButton.y-5,100, 100)
		elseif accent=="de" then love.graphics.rectangle("fill",dedeButton.x-5,dedeButton.y-5,150, 30) 
				-- lunarwingz
				local hovered = isButtonHovered (lunarwingzcButton)
				drawButton (lunarwingzcButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						language="de"
				end
			end
	
	elseif language=="en" then love.graphics.rectangle("fill",encButton.x-5,encButton.y-5,100, 100)
	    -- Voice actors
		if accent=="br" then love.graphics.rectangle("fill",enbrcButton.x-5,enbrcButton.y-5,100, 100)
			-- Aaron
			local hovered = isButtonHovered (AaroncButton)
			drawButton (AaroncButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="us"
			end
		elseif accent=="us" then love.graphics.rectangle("fill",enuscButton.x-5,enuscButton.y-5,100, 100)
			-- John
			local hovered = isButtonHovered (JohncButton)
			drawButton (JohncButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="br"
			end
		end

	elseif language=="eo" then love.graphics.rectangle("fill",eoButton.x-5,eoButton.y-5,100, 100)
	elseif language=="es" then love.graphics.rectangle("fill",escButton.x-5,escButton.y-5,100, 100)
		if accent=="es" then love.graphics.rectangle("fill",esescButton.x-5,esescButton.y-5,100, 100)
		elseif accent=="la" then love.graphics.rectangle("fill",eslacButton.x-5,eslacButton.y-5,100, 100)
		-- Bastián
			local hovered = isButtonHovered (bastiancButton)
			drawButton (bastiancButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="la"
			end
		end
	elseif language=="fr" then love.graphics.rectangle("fill",frcButton.x-5,frcButton.y-5,100, 100)
			if accent=="fr" then
				-- Labyrinthus
				local hovered = isButtonHovered (labyrinthuscButton)
				drawButton (labyrinthuscButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent="fr"
				end
				
			
			elseif accent=="ca" then love.graphics.rectangle("fill",frcacButton.x-5,frcacButton.y-5,100, 100)
			end
	elseif language=="it" then love.graphics.rectangle("fill",itButton.x-5,itButton.y-5,100, 100)
	elseif language=="nl" then love.graphics.rectangle("fill",nlButton.x-5,nlButton.y-5,100, 100)
			if accent=="nl" then love.graphics.rectangle("fill",nlnlButton.x-5,nlnlButton.y-5,100, 100)
		elseif accent=="fl" then love.graphics.rectangle("fill",flButton.x-5,flButton.y-5,100, 100)
		end
	
    elseif language=="pr" then love.graphics.rectangle("fill",prButton.x-5,prButton.y-5,100, 100)
    	if accent=="pr" then love.graphics.rectangle("fill",prprButton.x-5,enbrButton.y-5,100, 100)
		elseif accent=="br" then love.graphics.rectangle("fill",brButton.x-5,enusButton.y-5,100, 100)
		end
	elseif language=="ru" then love.graphics.rectangle("fill",ruButton.x-5,ruButton.y-5,100, 100)
	elseif language=="sv" then love.graphics.rectangle("fill",seButton.x-5,seButton.y-5,100, 100)
	elseif language=="sk" then love.graphics.rectangle("fill",skButton.x-5,skButton.y-5,100, 100)
    end
    
    if language2=="en" then
		if accent2=="br" then 
				-- isVA
			local hovered = isButtonHovered (ishVAcButton)
			drawButton (ishVAcButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="us"
			end
		elseif accent2=="us" then
				-- Snoozy
			local hovered = isButtonHovered (snoozycButton)
			drawButton (snoozycButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="br"
			end
		end
	elseif language2=="pl" then love.graphics.rectangle("fill",plButton.x-5,plButton.y-5,100, 100)
		-- Nelakori
			local hovered = isButtonHovered (nelakoriButton)
			drawButton (nelakoriButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="pl"
			end
	elseif language2=="es" then love.graphics.setColor(1,1,0,0.5) love.graphics.rectangle("fill",escButton.x-5,escButton.y-5,100, 100)
			if accent2=="es" then love.graphics.rectangle("fill",esescButton.x-5,esescButton.y-5,100, 100)
		elseif accent2=="la" then love.graphics.rectangle("fill",eslacButton.x-5,eslacButton.y-5,100, 100)
		-- zeroSky
			local hovered = isButtonHovered (zeroSkycButton)
			drawButton (zeroSkycButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="la"
			end
		end
	elseif language2=="nl" then love.graphics.rectangle("fill",escButton.x-5,esButton.y-5,100, 100)
			if accent2=="nl" then love.graphics.rectangle("fill",nlnlButton.x-5,nlnlButton.y-5,100, 100)
					-- Rosie Krijgsman
				local hovered = isButtonHovered (rosieButton)
				drawButton (rosieButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent2="nl"
				end
			end
	elseif language2=="fr" then love.graphics.setColor(1,1,0,0.5) love.graphics.rectangle("fill",frcButton.x-5,frcButton.y-5,100, 100)
			if accent2=="fr" then love.graphics.rectangle("fill",frfrcButton.x-5,frfrcButton.y-5,100, 100)
				-- vn_aiis
				local hovered = isButtonHovered (vnaiiscButton)
				drawButton (vnaiiscButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent2="fr"
				end
			end
		elseif accent2=="fl" then love.graphics.rectangle("fill",flButton.x-5,flButton.y-5,100, 100)
		

	elseif language2=="cs" then love.graphics.rectangle("fill",csButton.x-5,csButton.y-5,100, 100)
	end
    
end
