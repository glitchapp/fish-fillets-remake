function CutSceneAreaDraw()
	
	local hovered = isButtonHovered (fishhouseareaButton)
	drawButton (fishhouseareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=1
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (shipwrecksareaButton)
	drawButton (shipwrecksareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=2
		love.timer.sleep( 0.3 )
	end	
	
	local hovered = isButtonHovered (cityareaButton)
	drawButton (cityareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=3
		love.timer.sleep( 0.3 )
	end	
	
	local hovered = isButtonHovered (coralareaButton)
	drawButton (coralareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=4
		love.timer.sleep( 0.3 )
	end	
	
	local hovered = isButtonHovered (dumpareaButton)
	drawButton (dumpareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=5
		love.timer.sleep( 0.3 )
	end	
	
		local hovered = isButtonHovered (silversshipareaButton)
	drawButton (silversshipareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=6
		love.timer.sleep( 0.3 )
	end	
	
	local hovered = isButtonHovered (ufoareaButton)
	drawButton (ufoareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=7
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (treasureareaButton)
	drawButton (treasureareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=8
		love.timer.sleep( 0.3 )
	end	
	
		local hovered = isButtonHovered (computerareaButton)
	drawButton (computerareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=9
		love.timer.sleep( 0.3 )
	end	
	
	local hovered = isButtonHovered (ngareaButton)
	drawButton (ngareaButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		cutscenearea=10
		love.timer.sleep( 0.3 )
	end	
end
