
function CutScenesLevelDraw()
	if unlockedlevels[1]==true then
			local hovered = isButtonHovered (cutscenelevel1Button)
		drawButton (cutscenelevel1Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then	nLevel=1 
			elseif cutscenearea==2 then nLevel=9
			elseif cutscenearea==3 then nLevel=20
			elseif cutscenearea==4 then nLevel=30
			elseif cutscenearea==5 then nLevel=38
			elseif cutscenearea==6 then nLevel=45
			elseif cutscenearea==7 then nLevel=52
			elseif cutscenearea==8 then nLevel=59
			elseif cutscenearea==9 then nLevel=65
			elseif cutscenearea==10 then nLevel=71
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	if unlockedlevels[2]==true then
			local hovered = isButtonHovered (cutscenelevel2Button)
		drawButton (cutscenelevel2Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=2
			elseif cutscenearea==2 then nLevel=10
			elseif cutscenearea==3 then nLevel=21
			elseif cutscenearea==4 then nLevel=31
			elseif cutscenearea==5 then nLevel=39
			elseif cutscenearea==6 then nLevel=46
			elseif cutscenearea==7 then nLevel=53
			elseif cutscenearea==8 then nLevel=60
			elseif cutscenearea==9 then nLevel=66
			elseif cutscenearea==10 then nLevel=72
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
	if unlockedlevels[3]==true then
	local hovered = isButtonHovered (cutscenelevel3Button)
		drawButton (cutscenelevel3Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=3
			elseif cutscenearea==2 then nLevel=11
			elseif cutscenearea==3 then nLevel=22
			elseif cutscenearea==4 then nLevel=32
			elseif cutscenearea==5 then nLevel=40
			elseif cutscenearea==6 then nLevel=47
			elseif cutscenearea==7 then nLevel=54
			elseif cutscenearea==8 then nLevel=61
			elseif cutscenearea==9 then nLevel=67
			elseif cutscenearea==10 then nLevel=73
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
	if unlockedlevels[4]==true then
	local hovered = isButtonHovered (cutscenelevel4Button)
		drawButton (cutscenelevel4Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=4
			elseif cutscenearea==2 then nLevel=12
			elseif cutscenearea==3 then nLevel=23
			elseif cutscenearea==4 then nLevel=33
			elseif cutscenearea==5 then nLevel=41
			elseif cutscenearea==6 then nLevel=48
			elseif cutscenearea==7 then nLevel=55
			elseif cutscenearea==8 then nLevel=62
			elseif cutscenearea==9 then nLevel=68
			elseif cutscenearea==10 then nLevel=74
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
		if unlockedlevels[5]==true then
	local hovered = isButtonHovered (cutscenelevel5Button)
		drawButton (cutscenelevel5Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=5
			elseif cutscenearea==2 then nLevel=13
			elseif cutscenearea==3 then nLevel=24
			elseif cutscenearea==4 then nLevel=34
			elseif cutscenearea==5 then nLevel=42
			elseif cutscenearea==6 then nLevel=49
			elseif cutscenearea==7 then nLevel=56
			elseif cutscenearea==8 then nLevel=63
			elseif cutscenearea==9 then nLevel=69
			elseif cutscenearea==10 then nLevel=75
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
		if unlockedlevels[6]==true then
	local hovered = isButtonHovered (cutscenelevel6Button)
		drawButton (cutscenelevel6Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=6
			elseif cutscenearea==2 then nLevel=14
			elseif cutscenearea==3 then nLevel=25
			elseif cutscenearea==4 then nLevel=35
			elseif cutscenearea==5 then nLevel=43
			elseif cutscenearea==6 then nLevel=50
			elseif cutscenearea==7 then nLevel=57
			elseif cutscenearea==8 then nLevel=64
			elseif cutscenearea==9 then nLevel=70
			elseif cutscenearea==10 then nLevel=76
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
		if unlockedlevels[7]==true then
	local hovered = isButtonHovered (cutscenelevel7Button)
		drawButton (cutscenelevel7Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=7
			elseif cutscenearea==2 then nLevel=15
			elseif cutscenearea==3 then nLevel=26
			elseif cutscenearea==4 then nLevel=36
			elseif cutscenearea==6 then nLevel=51
			elseif cutscenearea==7 then nLevel=58
			--elseif cutscenearea==8 then nLevel=65
			--elseif cutscenearea==9 then nLevel=71
			elseif cutscenearea==10 then nLevel=77
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
		if unlockedlevels[8]==true then
	local hovered = isButtonHovered (cutscenelevel8Button)
		drawButton (cutscenelevel8Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then nLevel=8
			elseif cutscenearea==2 then nLevel=16
			elseif cutscenearea==3 then nLevel=27
			elseif cutscenearea==4 then nLevel=37
			elseif cutscenearea==10 then nLevel=78
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
		if unlockedlevels[9]==true and (cutscenearea==2 or cutscenearea==3) then
	local hovered = isButtonHovered (cutscenelevel9Button)
		drawButton (cutscenelevel9Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then 
			elseif cutscenearea==2 then nLevel=17
			elseif cutscenearea==3 then nLevel=28
			elseif cutscenearea==10 then nLevel=79
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
			if unlockedlevels[10]==true and (cutscenearea==2 or cutscenearea==3) then
	local hovered = isButtonHovered (cutscenelevel10Button)
		drawButton (cutscenelevel10Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then 
			elseif cutscenearea==2 then nLevel=18
			elseif cutscenearea==3 then nLevel=29
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
			if unlockedlevels[11]==true and cutscenearea==2 then
	local hovered = isButtonHovered (cutscenelevel11Button)
		drawButton (cutscenelevel11Button, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				if cutscenearea==1 then 
			elseif cutscenearea==2 then nLevel=19
			elseif cutscenearea==3 then
			end
			loadassetscutscenes()
			cutsceneplaying=true
			love.timer.sleep( 0.3 )
		end
	end
	
							if cutscenearea==1 then love.graphics.rectangle("line",fishhouseareaButton.x-5,fishhouseareaButton.y-5,50, 100)
						elseif cutscenearea==2 then love.graphics.rectangle("line",shipwrecksareaButton.x-5,shipwrecksareaButton.y-5,50, 100)
						elseif cutscenearea==3 then love.graphics.rectangle("line",cityareaButton.x-5,shipwrecksareaButton.y-5,50, 100)
						elseif cutscenearea==4 then love.graphics.rectangle("line",coralareaButton.x-5,coralareaButton.y-5,50, 100)
						elseif cutscenearea==5 then love.graphics.rectangle("line",dumpareaButton.x-5,dumpareaButton.y-5,50, 100)
						elseif cutscenearea==6 then love.graphics.rectangle("line",silversshipareaButton.x-5,silversshipareaButton.y-5,50, 100)
						elseif cutscenearea==7 then love.graphics.rectangle("line",ufoareaButton.x-5,ufoareaButton.y-5,50, 100)
						end
						
		love.graphics.print("Area",100,800,0,1)
	love.graphics.print("level",100,900,0,1)
	rectangleoverlanguagecutscenes()
						
		if nLevel==1 then 	love.graphics.rectangle("line",cutscenelevel1Button.x-5,cutscenelevel1Button.y-5,50, 100)
	elseif nLevel==2 then 	love.graphics.rectangle("line",cutscenelevel2Button.x-5,cutscenelevel2Button.y-5,50, 100)
	end
	
end
