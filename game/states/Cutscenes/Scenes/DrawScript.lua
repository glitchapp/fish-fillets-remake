
-- Current line index to display
CutSceneCurrentLine = 0
--CutSceneTextRotationAngle = 0.02
--CutSceneTextZoomFactor = 1.02
--CutScene3DRotationAngle = 0.001

function CutScenesDrawScript()
    -- Set the rotation and zoom parameters
	--CutSceneTextRotationAngle = 0.02
    --CutSceneTextZoomFactor = 1.02

    -- Calculate the position based on line index
    local x = 100
    local y = 100 + (CutSceneCurrentLine - 1) * 20
	local depth = CutSceneCurrentLine

    -- Apply rotation and zoom
    love.graphics.push()
    love.graphics.translate(x, y)
    --love.graphics.rotate(CutSceneTextRotationAngle)
    --love.graphics.rotate(CutScene3DRotationAngle * depth)
    --love.graphics.scale( CutSceneTextZoomFactor,  CutSceneTextZoomFactorzoom)

    -- Draw the line
    local line = CSub[CutSceneCurrentLine]
    if line then
        love.graphics.print(line, 0, 0)
    end
    

    -- Restore the transformation
    love.graphics.pop()

    -- Increment the line index for the next frame
    --CutSceneCurrentLine = CutSceneCurrentLine + 1
end

function CutScenesUpdateScript()

	--CutSceneTextRotationAngle = CutSceneTextRotationAngle + 0.00005
    --CutScene3DRotationAngle = CutScene3DRotationAngle + 0.00001  -- Adjusted 3D rotation increment
    --CutSceneTextZoomFactor = CutSceneTextZoomFactor - 0.0005

end
