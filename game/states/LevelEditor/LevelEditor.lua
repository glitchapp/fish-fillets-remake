function loveLoadLevelEditor()
	loadLevelEditor()
end

function loadLevelEditor()

MouseLine =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/Mouse.webp")))
--[[	
tileTextures = {
    [1] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level1Tile.webp"))),
    [2] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level3Tile.webp"))),
    [3] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level11Tile.webp"))),
    [4] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level52Tile.webp"))),
    [5] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile1.webp"))),
    [6] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile2.webp"))),
    [7] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile3.webp"))),
    [8] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile4.webp"))),
    [9] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile5.webp"))),
    [10] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile6.webp"))),
    [11] = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/speccy/Tile7.webp"))),
}
--]]


require ('game/states/LevelEditor/levelEditor/AddAgents')

require ('game/states/LevelEditor/levelEditor/AddExitArea')

require ('game/states/LevelEditor/levelEditor/AddObjects') 	-- this file show the path where the custom level is saved
Initialize_Objects()

require ('game/states/LevelEditor/levelEditor/AddTiles')
loadBackgroundTiles()

require ('game/states/LevelEditor/levelEditor/AddGeometry/AddLine')
require ('game/states/LevelEditor/levelEditor/AddGeometry/AddCircle')
require ('game/states/LevelEditor/levelEditor/AddGeometry/AddRectangle')

require ('game/states/LevelEditor/levelEditor/Buttons/loadButtons') 	-- this file show the path where the custom level is saved
require ('game/states/LevelEditor/levelEditor/Buttons/hideButtons') 	-- this file show the path where the custom level is saved
require ('game/states/LevelEditor/levelEditor/Buttons/drawButtons') 	-- this file show the path where the custom level is saved
--require ('game/states/LevelEditor/levelEditor/leveleditorButtons') 	-- this file show the path where the custom level is saved
loadLevelEditorButtons()

require ('game/states/LevelEditor/levelEditor/leveleditorTabs')
loadLevelEditorTabs()

require ('game/states/LevelEditor/levelEditor/leveleditorExportFolder') 	-- this file show the path where the custom level is saved
loadExportdata()

require ('game/states/LevelEditor/levelEditor/levelEditor_Load_levels') 	-- this file load custom levels

require ('game/states/LevelEditor/levelEditor/levelEditor_Play_level') 	-- this file load custom levels

require ('game/states/LevelEditor/levelEditor/Export2Graphics')

require ('game/states/LevelEditor/levelEditor/AddBackground')
loadBackgroundButtons()



require ('game/states/LevelEditor/levelEditor/levelEditorMouseInput')

require ('game/states/LevelEditor/levelEditor/Help')

require ('game/states/LevelEditor/levelEditor/About')
LoadAboutLevelEditor()

-- Define your grid and palette
--gridSize = 20
grid = {}  -- 2D array representing the grid
currentTile = 1  -- The currently selected tile from the palette
gridWidth = 50
gridHeight = 30
gridSizeWidthInput=20
gridSizeHeigthInput=20
gridVisible=true
	levelEditorTab="Maze"

    -- Define a global 'level' variable
    level = {}

    -- Initialize level properties
    level.map = {}  -- Initialize the map as an empty table
    level.agents = {}  -- Initialize agents as an empty table
    level.blocks = {}  -- Initialize objects as an empty table
    level.areas = {}  -- Initialize exit area
    editorTileData = {}
    selectedTiles = {}
    
     selectedTile = 0
	aestheticTileData = {}
	
    
    agent1Added = false
	agent2Added = false
	PlayingFromLevelEditor=false
	
	typingGridSize=false	-- Variable to check if typing grid size mode is activated
	
	gridWidthInput = ""  -- Variable to store the user input for grid size
	gridHeigthInput = ""  -- Variable to store the user input for grid size
	gridVisible=true
	newSize=0	-- Initialize variable to type grid size
	
	local isDrawingObject = false

levelEditorHelp = [[
Shortcuts:

1 draw walls
2 draw border
3 draw empty cell (delete walls)
4 draw agent 1
5 draw agent 2
6 draw exit
7 draw object
e export the maze.

Use the mouse to draw the maze.
]]

drawingMode = "wall"  -- Initialize with "wall" drawing mode


    --if checkIfCustomLevelExists("customlevel") then
		-- Initialize the grid with empty cells
		initializeGridLevelEditor()
	--else 
		-- Initialize the grid with loaded level
		--initializeGridLevelEditor(level)
	--end

--checkIfCustomLevelExists("customlevel")

end


--[[
function initializeGridLevelEditor()

    -- Initialize the grid with empty cells
    for x = 1, gridSize do
        grid[x] = {}
        for y = 1, gridSize do
            grid[x][y] = 0  -- 0 represents an empty cell
        end
    end
    
	-- Initialize a two-dimensional table to store selected tiles for each cell
	selectedTiles = {}
	for x = 1, gridSize do
		selectedTiles[x] = {}
		for y = 1, gridSize do
			selectedTiles[x][y] = 0 -- Default to no tile selected (change this if needed)
		end
	end


end--]]

function initializeGridLevelEditor(customLevelData)
    -- Check if custom level data is provided
    if customLevelData and customLevelData.map then
        -- Use the loaded custom level's grid size
        gridWidth = #customLevelData.map[1]
        gridHeight = #customLevelData.map

        -- Initialize the grid with values from the loaded custom level
        grid = {}
        for x = 1, gridWidth do
            grid[x] = {}
            for y = 1, gridHeight do
                grid[x][y] = customLevelData.map[y][x]  -- Swap x and y when copying data
            end
        end

        -- Initialize a two-dimensional table to store selected tiles for each cell
        selectedTiles = {}
        for x = 1, gridWidth do
            selectedTiles[x] = {}
            for y = 1, gridHeight do
                selectedTiles[x][y] = 0 -- Default to no tile selected (change this if needed)
            end
        end
    else
        -- Initialize the grid with empty cells
        grid = {}
        for x = 1, gridWidth do
            grid[x] = {}
            for y = 1, gridHeight do
                grid[x][y] = 0  -- 0 represents an empty cell
            end
        end

        -- Initialize a two-dimensional table to store selected tiles for each cell
        selectedTiles = {}
        for x = 1, gridWidth do
            selectedTiles[x] = {}
            for y = 1, gridHeight do
                selectedTiles[x][y] = 0 -- Default to no tile selected (change this if needed)
            end
        end
    end
    
  -- Update cellSize based on screen size and grid size
    local screenWidth, screenHeight = love.graphics.getDimensions()
    cellSize = math.floor(math.min(screenWidth / gridWidth, screenHeight / gridHeight))
end

-- Function to zoom in (increase cell size)
function zoomIn()
    cellSize = cellSize + 2  -- You can adjust the zoom factor (2) as needed
end

-- Function to zoom out (decrease cell size)
function zoomOut()
    cellSize = math.max(2, cellSize - 2)  -- Ensure a minimum cell size of 2 pixels
end

-- Function to convert screen coordinates to grid coordinates
function screenToGrid(x, y)
    local gridOffsetX = 200
    local gridOffsetY = 200
    local gridX = math.floor((x - gridOffsetX) / cellSize) + 1
    local gridY = math.floor((y - gridOffsetY) / cellSize) + 1
    return gridX, gridY
end

function updateLevelEditor(dt)
	
	timer=timer+dt
	timer2=timer2+dt
	timer3=timer3+dt
	
	if timer3>5 then
			showExportPath=false
	end
	
	love.mousepressed(x, y, button, istouch, presses)
	
	if screenshottaken then 
		updatetakescreenshot()
	end
	
	--if showCustomLevels then
        --updateMenu(dt)
    --end
	
end


function love.textinput(text)
    if not (gridSizeInput==nil) then 
		gridSizeInput = gridSizeInput .. text  -- Append typed characters to the input string
	end
end


-- Function to set the grid width
function setGridWidth(newWidth)
    gridWidth = newWidth
    grid = {}  -- Clear the existing grid
    currentTile = 1

    -- Initialize the grid with empty cells
    for x = 1, gridSize do
        grid[x] = {}
        for y = 1, gridSize do
            grid[x][y] = 0  -- 0 represents an empty cell
        end
    end
end

-- Function to set the grid height
function setGridHeight(newHeight)
    -- Ensure that the grid has been initialized with width
    if gridHeight == 0 then
        print("Error: Grid width must be set before setting the height.")
        return
    end

    -- Update the height of the existing grid
    for x = 1, gridHeight do
        local currentHeight = #grid[x]
        if currentHeight < newHeight then
            for y = currentHeight + 1, newHeight do
                grid[x][y] = 0  -- Add empty cells to fill the height
            end
        elseif currentHeight > newHeight then
            for y = newHeight + 1, currentHeight do
                grid[x][y] = nil  -- Remove cells to reduce the height
            end
        end
    end
   end

function drawGrid()
    if gridVisible==true then
    
    local gridOffsetX = 200
    local gridOffsetY = 200
	
    -- Define the cell size
    --local cellSize = 32

    -- Draw the vertical grid lines
    for x = 1, gridWidth + 1 do
        local cellX = (x - 1) * cellSize + gridOffsetX
        love.graphics.line(cellX, gridOffsetY, cellX, (gridHeight * cellSize) + gridOffsetY)
    end

    -- Draw the horizontal grid lines
    for y = 1, gridHeight + 1 do
        local cellY = (y - 1) * cellSize + gridOffsetY
        love.graphics.line(gridOffsetX, cellY, (gridWidth * cellSize) + gridOffsetX, cellY)
    end
    
    end
end







