
function addExitAreaAtMouse(x, y)
    --local cellX = math.floor(x / 32) + 1
    --local cellY = math.floor(y / 32) + 1
    
local gridOffsetX = 200
local gridOffsetY = 200
    
     -- Subtract gridOffsetX and gridOffsetY from x and y to account for the offset
        local cellX = math.floor((x - gridOffsetX) / 32) + 1
        local cellY = math.floor((y - gridOffsetY) / 32) + 1

    if cellX >= 1 and cellX <= gridWidth and cellY >= 1 and cellY <= gridHeight then
        addExitArea(cellX, cellY)
    end
end


-- Helper function to add an exit area and update the grid
function addExitArea(cellX, cellY)

	if cellX >= 1 and cellX <= gridWidth and cellY >= 1 and cellY <= gridHeight then
        grid[cellX][cellY] = 6  -- Set grid cell to indicate an exit area
    end

    local exitArea = {
        name = "exit",
        x = cellX,
        y = cellY,
        w = 1,  -- Adjust the width and height as needed
        h = 1,
    }
    table.insert(level.areas, exitArea)
end



