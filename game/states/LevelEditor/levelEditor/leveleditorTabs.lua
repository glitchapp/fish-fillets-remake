function loadLevelEditorTabs()
local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
currentTab="Tilemap editor"
	if res=="1080p" then
		desktopHeight=desktopHeight-600		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-600
	
	elseif res=="720p" then
		desktopHeight=desktopHeight-600		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-1200
	
	end
	
	MazeTabButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-400, 
	y = 150,
    sx = 1,
    image = levelEditorIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	TilesTabButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-325, 
	y = 150,
    sx = 1,
    image = tilesIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	BackgroundTabButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-250, 
	y = 150,
    sx = 1,
    image = bckIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	AboutTabButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-175, 
	y = 150,
    sx = 1,
    image = AboutIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	PlayLevelButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-400, 
	y = 225,
    sx = 1,
    image = PlayIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	LoadLevelButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-325, 
	y = 225,
    sx = 1,
    image = LoadIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	ExportLevelButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-250, 
	y = 225,
    sx = 1,
    image = ExportIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	ExportDirButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-175, 
	y = 225,
    sx = 1,
    image = FolderIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	TakeScreenshotButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-400, 
	y = 300,
    sx = 1,
    image = ScreenshotIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	clearLevelButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-325, 
	y = 300,
    sx = 1,
    image = clearIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}

	TypeGridWidthButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-250, 
	y = 300,
    sx = 1,
    image = gridWidthIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	TypeGridHeightButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-175, 
	y = 300,
    sx = 1,
    image = gridHeightIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	ToggleGridButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-400, 
	y = 400,
    sx = 1,
    image = ToggleGridIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	dropLevelButton = {
	text = "",
	textPosition ="top",
	x = desktopWidth-325, 
	y = 400,
    sx = 1,
    image = DropCustomLevelIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}


end


function drawLevelEditorTabs()
	love.graphics.setFont( poorfishsmall )
	
	local cellSize = 70
	local gridWidth = 4
	local gridHeight = 4
	
	 -- Use the x and y values of the MazeTabButton as the starting point
    local startX = MazeTabButton.x
    local startY = MazeTabButton.y
    
    
    
	love.graphics.setColor(0.5,0.5,0.5,5-timer3)
    -- Draw the vertical grid lines
    for x = 1, gridWidth + 1 do
        local cellX = startX + (x - 1) * cellSize
        love.graphics.line(cellX, startY, cellX, startY + gridHeight * cellSize)
    end

    -- Draw the horizontal grid lines
    for y = 1, gridHeight + 1 do
        local cellY = startY + (y - 1) * cellSize
        love.graphics.line(startX, cellY, startX + gridWidth * cellSize, cellY)
    end


		
		love.graphics.setColor(0.8,0.5,0.8,5-timer3)
		love.graphics.rectangle('fill',200,25,300,50)
		love.graphics.setColor(0.9,0.6,0.9,5-timer3)
		--love.graphics.rectangle('fill',AboutTabButton.x,AboutTabButton.y-50,300,50)
		love.graphics.setColor(1,1,1,5-timer3)
		love.graphics.print("Level editor", 200,25,0,1)
		love.graphics.print("" .. currentTab,PlayLevelButton.x, 100,0,1)
		
		
		love.graphics.setColor(0.5,0.5,0.5,5-timer3)
		
		--[[
		if levelEditorTab=="Maze" then love.graphics.rectangle('fill',MazeTabButton.x,MazeTabButton.y,300,50)
	elseif levelEditorTab=="Tiles" then love.graphics.rectangle('fill',TilesTabButton.x,TilesTabButton.y,300,50)
	elseif levelEditorTab=="Backgrounds" then love.graphics.rectangle('fill',BackgroundTabButton.x,BackgroundTabButton.y,300,50)
	end
	--]]
		
	if levelEditorTab=="Backgrounds" then
		drawLevelEditorBackgroundButtons()
	elseif levelEditorTab=="Tiles" then
		drawTileButtons()
	end
		
			-- Tabs
		local hovered = isButtonHovered (AboutTabButton)
			drawButton (AboutTabButton, hovered, MazeTabButton.text)
				if hovered and love.mouse.isDown(1) then 
					aboutInfo=true
				elseif hovered then
					love.graphics.print("About info", startX,startY+300,0,1)
				end
			
			
		local hovered = isButtonHovered (MazeTabButton)
			drawButton (MazeTabButton, hovered, MazeTabButton.text)
				if hovered and love.mouse.isDown(1) then 
					currentTab="Tilemap editor"
					levelEditorTab="Maze"
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Level editor", startX,startY+300,0,1)
				end
			
		local hovered = isButtonHovered (TilesTabButton)
			drawButton (TilesTabButton, hovered, TilesTabButton.text)
				if hovered and love.mouse.isDown(1) then
					currentTab="Tiles editor"
					levelEditorTab="Tiles"	
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Tiles selection tab", startX,startY+300,0,1)
				end
	
		local hovered = isButtonHovered (BackgroundTabButton)
			drawButton (BackgroundTabButton, hovered, BackgroundTabButton.text)
				if hovered and love.mouse.isDown(1) then 
					currentTab="Backgrounds"
					levelEditorTab="Backgrounds"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Background selection tab", startX,startY+300,0,1)
				end
				
						
			local hovered = isButtonHovered (LoadLevelButton)
			drawButton (LoadLevelButton, hovered)
				
				if hovered and love.mouse.isDown(1) then 
					-- Load the list of custom levels
					--listCustomLevels()
					--showCustomLevels=true
					checkIfCustomLevelExists("customlevel")
					initializeGridLevelEditor(level)
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Load custom level", startX,startY+300,0,1)
				end
				
		
				
			local hovered = isButtonHovered (PlayLevelButton)
			drawButton (PlayLevelButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					PlayCustomLevel()
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Play exported custom level", startX,startY+300,0,1)
				end
			
			local hovered = isButtonHovered (ExportLevelButton)
			drawButton (ExportLevelButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					exportLevel()
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Export level", startX,startY+300,0,1)
				end
		
			local hovered = isButtonHovered (ExportDirButton)
			drawButton (ExportDirButton, hovered, ExportDirButton.text)
				if hovered and love.mouse.isDown(1) then 
					love.system.openURL(export_dir)
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Open save directory", startX,startY+300,0,1)
				end
				
								
			local hovered = isButtonHovered (TakeScreenshotButton)
			drawButton (TakeScreenshotButton, hovered, TakeScreenshotButton.text)
				if hovered and love.mouse.isDown(1) then 
					myscreenshot = love.graphics.captureScreenshot(os.time() .. ".png")
					photosound:play()
					timer3=0
					screenshottaken=true
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Take a screenshot", startX,startY+300,0,1)
				end
				
							
			
				local hovered = isButtonHovered (clearLevelButton)
				drawButton (clearLevelButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					initializeGridLevelEditor()
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Clear grid", startX,startY+300,0,1)
				end
				
				local hovered = isButtonHovered (TypeGridWidthButton)
				drawButton (TypeGridWidthButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					typingGridWidth=true
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Change grid width", startX,startY+300,0,1)
				end
			
				local hovered = isButtonHovered (TypeGridHeightButton)
				drawButton (TypeGridHeightButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					typingGridHeight=true
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Change grid height", startX,startY+300,0,1)
				end
				
				local hovered = isButtonHovered (ToggleGridButton)
				drawButton (ToggleGridButton, hovered)
				if hovered and love.mouse.isDown(1) then 
						if gridVisible==true then gridVisible=false
					elseif gridVisible==false then gridVisible=true
					end
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Toggle grid on and off", startX,startY+300,0,1)
				end
				
				--[[local hovered = isButtonHovered (dropLevelButton)
				drawButton (dropLevelButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					dropFileWindow=true
					
				elseif hovered then
					love.graphics.print("drop a custom level to load it", startX,startY+300,0,1)
				end
				
				if dropFileWindow==true then
					love.graphics.print("Drop your custom level to the window's game to load it", 800, 360, 0, 2)
					love.graphics.print("Press play custom level after dropping your custom level", 800, 460, 0, 2)
				end
				--]]
		
				if typingGridWidth==true then
					love.graphics.print("Type your wanted grid width and press enter",TypeGridHeightButton.x,TypeGridHeightButton.y-100,0, 0.8, 1)
				end
				
				if typingGridHeigth==true then
					love.graphics.print("Type your wanted grid height and press enter",TypeGridHeightButton.x,TypeGridHeightButton.y-100,0, 0.8, 1)
				end
			
	
end

function love.filedropped(file)
	if dropFileWindow==true then
		file:open("r")
		DropedCustomLevel = file:read()
	end
	
	
end
