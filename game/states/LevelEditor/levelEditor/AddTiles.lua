function loadBackgroundTiles()
--[[
if Tile1Texture==nil then

	Tile1Texture =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level1Tile.webp")))
	Tile2Texture =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level3Tile.webp")))
	Tile3Texture =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level11Tile.webp")))
	Tile4Texture =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/textures/level52Tile.webp")))
end
--]]
local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

	if res=="1080p" then
		desktopHeight=desktopHeight-400		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-600
	end

Tile1Button = {
    text = "Tile 1",
    textPosition ="top",
	x = desktopWidth-800, 
	y = 400,
    sx = 1,
    image = tileTextures[1],
    --image = love.graphics.newImage("assets/icons/fficon.png"),
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
}


Tile2Button = {
    text = "Tile 2",
    textPosition ="top",
  	x = desktopWidth-600, 
	y = 400,
    sx = 1,
    image = tileTextures[2],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile3Button = {
    text = "Tile 3",
    textPosition ="top",
  	x = desktopWidth-400, 
	y = 400,
    sx = 1,
    image = tileTextures[3],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}
Tile4Button = {
    text = "Tile 4",
    textPosition ="top",
  	x = desktopWidth-200, 
	y = 400,
    sx = 1,
    image = tileTextures[4],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile5Button = {
    text = "Tile 5",
    textPosition ="top",
  	x = desktopWidth-800, 
	y = 500,
    sx = 1,
    image = tileTextures[5],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile6Button = {
    text = "Tile 6",
    textPosition ="top",
  	x = desktopWidth-600, 
	y = 500,
    sx = 1,
    image = tileTextures[6],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile7Button = {
    text = "Tile 7",
    textPosition ="top",
  	x = desktopWidth-400, 
	y = 500,
    sx = 1,
    image = tileTextures[7],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile8Button = {
    text = "Tile 8",
    textPosition ="top",
  	x = desktopWidth-200, 
	y = 500,
    sx = 1,
    image = tileTextures[8],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

Tile9Button = {
    text = "Tile 9",
    textPosition ="top",
  	x = desktopWidth-800, 
	y = 600,
    sx = 1,
    image = tileTextures[9],
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

DeselectTileButton = {
		text = "Deselect tile",
		x = desktopWidth-600, 
		y = 800,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

end


function drawTileButtons()

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

				-- Define an array of buttons and their corresponding tile values
tileButtons = {
    {button = Tile1Button, tileValue = 1},
    {button = Tile2Button, tileValue = 2},
    {button = Tile3Button, tileValue = 3},
    {button = Tile4Button, tileValue = 4},
    {button = Tile5Button, tileValue = 5},
    {button = Tile6Button, tileValue = 6},
    {button = Tile7Button, tileValue = 4},
    {button = Tile8Button, tileValue = 7},
    {button = Tile9Button, tileValue = 8},
    {button = DeselectTileButton, tileValue = 0}
}

-- Loop through the tileButtons array
for _, buttonInfo in ipairs(tileButtons) do
    local hovered = isButtonHovered(buttonInfo.button)
    drawButton(buttonInfo.button, hovered)
    
    if hovered and love.mouse.isDown(1) then
        selectedTile = buttonInfo.tileValue
        TileSet = true
        love.timer.sleep(0.3)
    end
end
end

