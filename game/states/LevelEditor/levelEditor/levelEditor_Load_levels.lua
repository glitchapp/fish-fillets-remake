function checkIfCustomLevelExists(filename)
    --local filename = "customlevel.lua"
    local fileExtension = ".lua"
    local fullFilename = filename .. fileExtension

    -- Check the root folder
    local fileInfoRoot = love.filesystem.getInfo(fullFilename)
    
    -- Check the parent folder of the root
    local fileInfoParent = love.filesystem.getInfo("../" .. fullFilename)
    
    -- Check the save directory
    local fileInfoSaveDir = love.filesystem.getInfo(love.filesystem.getSaveDirectory() .. fullFilename)

    print("Checking for file:", fullFilename)
    print("Root folder:", fileInfoRoot)
    print("Parent folder:", fileInfoParent)
    print("Save directory:", fileInfoSaveDir)
    
    if fileInfoRoot or fileInfoParent or fileInfoSaveDir then
        -- The file exists in one of the locations
        print(fullFilename .. " exists, loading level....")
        --local moduleName = filename -- Remove the file extension
        
        package.loaded[filename] = nil
        level, loadError = require(filename)
        
        
        --debug
        --print(level.map)
                -- Initialize the grid with values from the loaded custom level
        --[[
        for x = 1, 30 do
            grid[x] = {}
            for y = 1, 30 do
                grid[x][y] = level.map[x][y]
            end
        end
        --]]
        
        --initializeGridLevelEditor(level)
        return true
    else
        -- The file does not exist in any of the locations
        print(fullFilename .. " does not exist.")
        return false
    end
end




function loadCustomLevel(filename)
       local level = {}
	steps=0
   
   -- Ensure the module is not cached
    package.loaded["customlevel"] = nil

    -- Load the module
		if level==nil then
			print("level was nil")
			level, loadError  = require ("customlevel")
		else
			print("level was not nil")
			level, loadError = filename
		end
		
		-- Initialize the grid based on the custom level data
		initializeGridLevelEditor(level)

    if not level then
        print("Failed to load custom level module: " .. loadError)
        return nil
    end
    


    return level
end

function listCustomLevels()
    local dirItems = love.filesystem.getDirectoryItems(love.filesystem.getSaveDirectory())  -- Use the default save directory
    
    for _, item in ipairs(dirItems) do
        if item:match("%.lua$") then
            table.insert(customLevels, item)
        end
    end
end

function selectCustomLevelWithMouse()
  for i, levelFile in ipairs(customLevels) do
        local x = 100  -- Adjust the x-position based on your layout
        local y = 100 + i * 30  -- Adjust the y-position based on your layout
        local width = 100  -- Adjust the button width
        local height = 30  -- Adjust the button height

        local hovered = isButtonHovered(x, y, width, height)
        drawButton(x, y, width, height, hovered, levelFile)

        if hovered and love.mouse.isDown(1) then
            local selectedLevel = loadCustomLevel(levelFile)

            if selectedLevel then
                -- Start the game with the selected custom level
                startGame(selectedLevel)
            end
        end
    end
    return nil
end




function drawCustomLevels()
        -- Draw the custom level selection menu here
    
    for i, levelFile in ipairs(customLevels) do
        local x = 100  -- Adjust the x-position based on your layout
        local y = 100 + i * 30  -- Adjust the y-position based on your layout
        local width = 100  -- Adjust the button width
        local height = 30  -- Adjust the button height

        local hovered = isCustomLevelButtonHovered(x, y, width, height)
        drawCustomLevelButton(x, y, width, height, hovered, levelFile)

        if hovered and love.mouse.isDown(1) then
            local selectedLevel = loadCustomLevel(levelFile)

            if selectedLevel then
                -- Start the game with the selected custom level
                startGame(selectedLevel)
            end
        end
    end
end

-- Implement a function to check if the mouse is hovering over a button
function isCustomLevelButtonHovered(x, y, width, height)
    local mouseX, mouseY = love.mouse.getPosition()
    return mouseX >= x and mouseX <= x + width and mouseY >= y and mouseY <= y + height
end

-- Implement a function to draw a button
function drawCustomLevelButton(x, y, width, height, hovered, text)
    love.graphics.setColor(0.5, 0.5, 0.5)  -- Gray color
    if hovered then
        love.graphics.setColor(1, 1, 1)  -- White color when hovered
    end
    love.graphics.rectangle("fill", x, y, width, height)
    love.graphics.setColor(0, 0, 0)  -- Black text color
    love.graphics.print(text, x + 10, y + 5)  -- Adjust text position
    love.graphics.setColor(1, 1, 1)  -- Reset color to white
end
