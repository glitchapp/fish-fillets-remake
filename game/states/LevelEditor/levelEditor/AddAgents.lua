function addAgentAtMouse(x, y)
	local cellX = math.floor((x - gridOffsetX) / cellSize) + 1
    local cellY = math.floor((y - gridOffsetY) / cellSize) + 1

    if cellX >= 1 and cellX <= gridWidth and cellY >= 1 and cellY <= gridHeight then
        if drawingMode == "agent" then
            addAgent(cellX, cellY)
        elseif drawingMode == "agent2" then
            addAgent2(cellX, cellY)
        end
    end
end

-- Helper function to add an agent at a given cell
function addAgent(cellX, cellY)
    for _, agent in ipairs(level.agents) do
        if agent.name == "fish-4x2" then
            -- Agent of type "fish-4x2" already exists, replace it
            agent.x = cellX
            agent.y = cellY
            return  -- Exit the function, as we don't need to insert a new agent
        end
    end

    -- If no agent of this type exists, create a new one
    local agent = {
        name = "fish-4x2",
        fish = true,
        heavy = false,
        x = cellX,
        y = cellY,
        form = {
            {1, 1, 1},
            {1, 1, 1},
        },
    }

    table.insert(level.agents, agent)
end

function addAgent2(cellX, cellY)
    for _, agent in ipairs(level.agents) do
        if agent.name == "fish-3x1" then
            -- Agent of type "fish-3x1" already exists, replace it
            agent.x = cellX
            agent.y = cellY
            return  -- Exit the function, as we don't need to insert a new agent
        end
    end

    -- If no agent of this type exists, create a new one
    local agent = {
        name = "fish-3x1",
        fish = true,
        heavy = false,
        x = cellX,
        y = cellY,
        form = {
            {1, 1, 1},
        },
    }

    table.insert(level.agents, agent)
end
