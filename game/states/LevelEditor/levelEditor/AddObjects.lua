
	--local currentObject = {}  -- Keep track of the current object being drawn
		
--currentObject = {}
isDrawingObject = false
isPlacingObject = false
objectForm = nil

function Initialize_Objects(heavy)
    currentObject = {
        name = "object_name_here",
        heavy = heavy or false, -- Default to false if not specified
        x = 0,
        y = 0,
        form = {
            {1},
        },
    }
end


function moveObject(object, cellX, cellY)
    
    -- Check if the target cell is empty
    if grid[cellX][cellY] == 0 then
        -- Clear the old cell
        grid[object.x][object.y] = 0
        
        -- Update the object's position
        object.x = cellX
        object.y = cellY
        
        -- Mark the new cell as occupied by the object
        grid[cellX][cellY] = 1
    end
end



function love.mousepressed(x, y, button, istouch, presses)
    if button == 1 then
    if drawingMode == "object" then
        if not isDrawingObject and not isPlacingObject then
            -- Start drawing the object
            currentObject = createObjectForm(x, y)
            isDrawingObject = true
            print("Start drawing object")
        elseif isDrawingObject and not isPlacingObject then
            -- Finalize the object drawing and move to placement step
            isDrawingObject = false
            isPlacingObject = true
            print("Finalize object drawing, start placement")
        elseif isPlacingObject then
            -- Place the object at the clicked grid cell
            addObjectFromForm(currentObject)
            currentObject = nil
            isPlacingObject = false
            print("Place object on the grid")
        end
    end


    elseif button == 2 then
        if isDrawingObject then
            -- Cancel object drawing
            currentObject = nil
            isDrawingObject = false
            print("Cancel object drawing")
        elseif isPlacingObject then
            -- Cancel object placement
            currentObject = nil
            isPlacingObject = false
            print("Cancel object placement")
        end
    end
end

function createObjectForm(cellX, cellY)
    return {
        name = "object_name_here",
        heavy = heavy or false,
        x = cellX,
        y = cellY,
        form = {
            {1},
        },
    }
end

function updateObjectForm(form, cellX, cellY)

      if not form then
        return  -- Handle the case where form is nil
    end

    if not form.x then
        form.x = cellX  -- Initialize x if it's nil
    end
    if not form.y then
        form.y = cellY  -- Initialize y if it's nil
    end
    if not form.form then
        form.form = {{1}}  -- Initialize form if it's nil
    end
    
      -- Adjust the object's position based on cell size and zoom
    form.x = cellX * cellSize
    form.y = cellY * cellSize

    local deltaX = cellX - form.x
    local deltaY = cellY - form.y

    -- Adjust the object's size based on the movement
    if deltaX > 0 then
        for i = 1, deltaX do
            table.insert(form.form, {})
        end
    elseif deltaX < 0 then
        for i = 1, -deltaX do
            table.remove(form.form, 1)
        end
    end

    if deltaY > 0 then
        for _, row in ipairs(form.form) do
            for i = 1, deltaY do
                table.insert(row, 1)
            end
        end
    elseif deltaY < 0 then
        for _, row in ipairs(form.form) do
            for i = 1, -deltaY do
                table.remove(row, 1)
            end
        end
    end

    -- Update the form's position
    form.x = cellX
    form.y = cellY
end





function addObjectFromForm(form)
	local cellX = math.floor(form.x / cellSize) + 1  -- Calculate cellX based on adjusted x position
    local cellY = math.floor(form.y / cellSize) + 1  -- Calculate cellY based on adjusted y position

    
    -- Create the actual object using the form and add it to your level
	local block = {
        name = form.name,
        heavy = form.heavy,
        x = startX,
        y = startY,
        form = form.form,
    }

    -- Ensure that the cells are empty before adding the object
    local canAddObject = true
    for i = 1, #form.form do
        for j = 1, #form.form[i] do
            local cellX = startX + j - 1
            local cellY = startY + i - 1
            if grid[cellX][cellY] ~= 0 then
                canAddObject = false
                break
            end
        end
        if not canAddObject then
            break
        end
    end

    if canAddObject then
        -- Add the object to your level.objects
        table.insert(level.blocks, block)

        -- Update the grid to mark the cells as occupied by the object
		for i = 1, #block.form do
            for j = 1, #block.form[i] do
                local cellX = block.x + j - 1
                local cellY = block.y + i - 1
                grid[cellX][cellY] = 1
            end
        end
    end
end



function addObjectAtMouse(x, y)

     if isDrawingObject then
        local cellX = math.floor(x / 32) + 1
        local cellY = math.floor(y / 32) + 1

        if cellX >= 1 and cellX <= gridSize and cellY >= 1 and cellY <= gridSize then
            addObject(cellX, cellY)
            -- Display debug information on the Love2D screen
            --local debugText = "Creating object at (" .. cellX .. ", " .. cellY .. ")"
            --love.graphics.print(debugText, 900,500,0, 0.8, 1)
            print("Creating object at (" .. cellX .. ", " .. cellY .. ")") -- Debug print
        end
    end
end




function addObject(object)
    if object then
        -- Add the object to your level.objects
        table.insert(level.objects, object)

        -- Update the grid to mark the cells as occupied by the object
        for i = 1, #object.form do
            for j = 1, #object.form[i] do
                local cellX = object.x + j - 1
                local cellY = object.y + i - 1
                grid[cellX][cellY] = 1
            end
        end
    end
end

function createObject(cellX, cellY)
    return {
        name = "object_name_here",
        heavy = heavy or false,
        x = cellX,
        y = cellY,
        form = {
            {1},
        },
    }
end



