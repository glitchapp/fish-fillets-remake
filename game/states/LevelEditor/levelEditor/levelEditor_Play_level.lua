function PlayCustomLevel()
    local level = {}
	steps=0
    -- Construct the full path to the custom level file
    --local filename = "customlevel.lua"
    --local fullPath = love.filesystem.getSaveDirectory() .. "/" .. filename
	
    -- Ensure the module is not cached
    package.loaded["customlevel"] = nil

    -- Load the module
    if dropFileWindow==true and not (DropedCustomLevel==nil) then	-- if custom level is droped to the window load it
		level, loadError  = require (DropedCustomLevel)
		dropFileWindow=false
	else															-- otherwise load the customlevel.lua from the save or root directory
		level, loadError  = require ("customlevel")
	end

    if not level then
        print("Failed to load custom level module: " .. loadError)
        return nil
    end
    
    PlayingFromLevelEditor=true
    music:stop()
    lovebpmload("externalassets/music/poinl/ambient2_Nautilus.ogg")
	music:play()
	assignfrgbck()				-- Assign layers
	love.graphics.clear()
	dx,dy=0,0		-- prevent game from crashing due variable not initialized
	pb:load (level)
	palette=2
	shader1=false shader2=false
    nLevel=120
    gamestatus="game"
    

    return level
end

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	
	if res=="1080p" then
		desktopHeight=desktopHeight-400		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-600
	elseif res=="720p" then
		desktopHeight=desktopHeight-600		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-1200
	
	end

levelEditorReturnButton = {
	text = "Level editor",
	x = desktopWidth-500, 
	y = 200,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

levelcompletedfanfareplayed=false

function drawReturnToLevelEditor()
	
	-- nLevel120 is a number assigned for custom levels in the level editor (provisional)
	if nLevel==120 then
		-- Level editor btton
		hovered = isButtonHovered (levelEditorReturnButton)
		drawButton (levelEditorReturnButton, hovered,levelEditorButton.text)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
				inputtime=0
				timer3=0
				music:stop()
				music = love.audio.newSource( "/externalassets/music/EricMatyas/Hypnotic_Puzzle_loop.ogg","static" )
				love.audio.play( music )
				music:setVolume(0.2)
				
				initializeGridLevelEditor()
				 -- Ensure the module is not cached
				package.loaded["customlevel"] = nil
				 -- Load the module
				level, loadError  = require ("customlevel")
				
				love.graphics.clear()
				pb:load (level)
				
				if not level then
					print("Failed to load custom level module: " .. loadError)
					return nil
				end
				
				
				gamestatus="levelEditor" 
				love.timer.sleep( 0.2 )
				
			end
		end
	end
end
