
function exportLevelAsImage()
   export_dir = love.filesystem.getSaveDirectory()					-- assign directory path to save_dir

    -- Create a canvas to render the level
    local canvas = love.graphics.newCanvas(gridSize * 32, gridSize * 32)

    -- Set the canvas as the rendering target
    love.graphics.setCanvas(canvas)

    -- Clear the canvas
    love.graphics.clear()

    -- Render the level grid, agents, objects, etc. on the canvas
    renderLevel() -- Call your renderLevel function here to draw elements

    -- Reset the rendering target to the screen
    love.graphics.setCanvas()

    -- Specify the filename
    local filename = "level_screenshot.png"

    -- Attempt to capture the screenshot
    local screenshot = love.graphics.newScreenshot()
    
    if screenshot then
        -- Save the screenshot to an image file (PNG format in this example)
        screenshot:encode(filename)
        print("Level screenshot saved as " .. filename)
        Acceptsound:play()
        levelexported = true
    else
        print("Failed to capture screenshot")
    end
end



function renderLevel()
    love.graphics.setColor(1, 1, 1)  -- Set the drawing color to white

    -- Render the level grid
    for y = 1, gridSize do
        for x = 1, gridSize do
            local cellValue = grid[x][y]
            
            if cellValue == 1 then
                love.graphics.rectangle("fill", (x - 1) * 32, (y - 1) * 32, 32, 32)  -- Render walls
            elseif cellValue == 3 then
                love.graphics.rectangle("line", (x - 1) * 32, (y - 1) * 32, 32, 32)  -- Render borders
            end
        end
    end

    -- Render agents
    for _, agent in ipairs(level.agents) do
        love.graphics.setColor(0, 0, 1)  -- Set color to blue for agents
        for y, row in ipairs(agent.form) do
            for x, cellValue in ipairs(row) do
                if cellValue == 1 then
                    local drawX = (agent.x + x - 2) * 32
                    local drawY = (agent.y + y - 2) * 32
                    love.graphics.rectangle("fill", drawX, drawY, 32, 32)
                end
            end
        end
    end

    -- Render objects
    for _, object in ipairs(level.objects) do
        love.graphics.setColor(1, 0, 0)  -- Set color to red for objects
        for y, row in ipairs(object.form) do
            for x, cellValue in ipairs(row) do
                if cellValue == 1 then
                    local drawX = (object.x + x - 2) * 32
                    local drawY = (object.y + y - 2) * 32
                    love.graphics.rectangle("fill", drawX, drawY, 32, 32)
                end
            end
        end
    end

    love.graphics.setColor(1, 1, 1)  -- Reset the drawing color to white
end

function fadesExportGraphicsInformation()
				
	if levelexported then
		if timer3>0 and timer3<5 then
			love.graphics.setColor(0.5,0.5,0.5,1-(timer3-4))
			love.graphics.rectangle( "fill", 475*scalefactor, 425*scalefactor,1000*scalefactor,300*scalefactor)
			love.graphics.setColor(1,1,1,1-(timer3-4))
			love.graphics.print ("Graphic saved on directory:",500*scalefactor, 450*scalefactor,0,1)
			love.graphics.print ("press open directory to open it:",500*scalefactor, 550*scalefactor,0,1)
			
			love.graphics.print (export_dir,500*scalefactor, 500*scalefactor,0,1)
			love.graphics.setFont( poorfish )
			if timer3>4.5 then
				levelexported=false
			end
		end	
	end	
	
		
		
	end
