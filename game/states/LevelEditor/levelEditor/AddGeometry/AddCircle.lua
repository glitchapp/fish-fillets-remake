-- Function to add a circle to the grid
function addCircleToGrid(centerX, centerY, radius)
	local centerX = 10 
	local centerY = 10 
	local radius = 5

    -- Implementing a simple circle drawing algorithm (Midpoint Circle Algorithm)
    local x = radius
    local y = 0
    local radiusError = 1 - x

    while x >= y do
        -- Fill the grid cells based on circle symmetry
        for i = -x, x do
            grid[centerX + i][centerY + y] = 1
            grid[centerX + i][centerY - y] = 1
        end

        for i = -y, y do
            grid[centerX + i][centerY + x] = 1
            grid[centerX + i][centerY - x] = 1
        end

        y = y + 1

        if radiusError < 0 then
            radiusError = radiusError + 2 * y + 1
        else
            x = x - 1
            radiusError = radiusError + 2 * (y - x + 1)
        end
    end
end
