-- Function to add a line to the grid
function addLineToGrid(gridStartX, gridStartY, endX, endY)
    -- Calculate the slope of the line
    local deltaX = math.abs(endX - gridStartX)
    local deltaY = math.abs(endY - gridStartY)

    local signX = gridStartX < endX and 1 or -1
    local signY = gridStartY < endY and 1 or -1

    local errorAccumulation = deltaX - deltaY

    local currentX, currentY = gridStartX, gridStartY
    
    -- Ensure that currentX and currentY are at least 1
    if currentX == 0 then currentX = 1 end
    if currentY == 0 then currentY = 1 end
    
    while currentX ~= endX and currentY ~= endY do
        -- Set the grid cell to 1 to mark the line
        grid[currentX][currentY] = 1

        local doubleError = errorAccumulation * 2

        if doubleError > -deltaY then
            errorAccumulation = errorAccumulation - deltaY
            currentX = currentX + signX
        end

        if doubleError < deltaX then
            errorAccumulation = errorAccumulation + deltaX
            currentY = currentY + signY
        end
    end
end
