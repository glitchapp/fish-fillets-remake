-- Function to add a rectangle to the grid
function addRectangleToGrid(startX, startY, endX, endY)
if x==0 then x=1 end -- prevent game from crashing due to false grid coordinates
if y==0 then y=1 end
    for x = math.min(startX, endX), math.max(startX, endX) do
        for y = math.min(startY, endY), math.max(startY, endY) do
            grid[x][y] = 1
        end
    end
end
