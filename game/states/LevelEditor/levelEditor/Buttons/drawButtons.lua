
function updateLevelEditorButtonPositions()
	
	-- Update the position of each button in the levelEditorButtons table
    for _, button in ipairs(levelEditorButtons) do
        button.x = levelEditorContainer.x
        button.y = levelEditorContainer.y
    end

end

local cellSize = 70
local gridWidth = 2
local gridHeight = 8

function drawLevelEditorButtons()

		local hovered = isButtonHovered (exitLevelEditorButton)
		--if hideplayercontrols==false then
			drawButton (exitLevelEditorButton, hovered)
		--end
		
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		
			love.audio.stop()
			advertising1playing=false
			advertising2playing=false
			introvideoplaying=false
			PlayingFromLevelEditor=true
		cdplaying=false
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
			love.timer.sleep( 0.3 )
			gamestatus="levelselection"
		end

    -- Use the x and y values of the DrawWallsButton as the starting point
    local startX = DrawWallsButton.x
    local startY = DrawWallsButton.y
	love.graphics.setColor(0.5,0.5,0.5,5-timer3)
    -- Draw the vertical grid lines
    for x = 1, gridWidth + 1 do
        local cellX = startX + (x - 1) * cellSize
        love.graphics.line(cellX, startY, cellX, startY + gridHeight * cellSize)
    end

    -- Draw the horizontal grid lines
    for y = 1, gridHeight + 1 do
        local cellY = startY + (y - 1) * cellSize
        love.graphics.line(startX, cellY, startX + gridWidth * cellSize, cellY)
    end



		local hovered = isButtonHovered (DrawWallsButton)
			drawButton (DrawWallsButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "wall"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Draw walls:", 25, 150, 0, 1)
				end
				

				
			local hovered = isButtonHovered (DrawBordersButton)
			drawButton (DrawBordersButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "border"
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("Draw borders", 25, 150, 0, 1)
				end
				
			local hovered = isButtonHovered (AddLineButton)
			drawButton (AddLineButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "line"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Draw lines", 25, 150, 0, 1)
				end
				
					local hovered = isButtonHovered (AddRectangleButton)
				drawButton (AddRectangleButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "rectangle"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Draw rectangles", 25, 150, 0, 1)
				end
				
				
				local hovered = isButtonHovered (AddCircleButton)
				drawButton (AddCircleButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "circle"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Draw circles", 25, 150, 0, 1)
				end
				
				
				
			local hovered = isButtonHovered (DrawEmptySpacesButton)
			drawButton (DrawEmptySpacesButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "emptyCell"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("erase cells", 25, 150, 0, 1)
				end
				
			local hovered = isButtonHovered (DrawAgent1Button)
			drawButton (DrawAgent1Button, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "agent"
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("add agent 1", 25, 150, 0, 1)
				end
				
			local hovered = isButtonHovered (DrawAgent2Button)
			drawButton (DrawAgent2Button, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "agent2"
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("add agent 2", 25, 150, 0, 1)
				end
				--[[
			local hovered = isButtonHovered(DrawObjectButton)
			drawButton(DrawObjectButton, hovered)
			if hovered and love.mouse.isDown(1) then
				drawingMode = "object"
				isDrawingObject = true
				isMovingObject = false -- ensure that isMovingObject is set to false
				Initialize_Objects(false)
				love.timer.sleep(0.3)
			elseif hovered then
				love.graphics.print("This option is still work in progress and may not work as expected:", 500 * scalefactor, 450 * scalefactor, 0, 1)
			end
			--]]

				
			--if isDrawingHeavy then
				local hovered = isButtonHovered (DrawHeavyObjectButton)
				drawButton (DrawHeavyObjectButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					Initialize_Objects(true)	-- false initialize heavy object
					drawingMode = "object"
					isDrawingObject = true
					isMovingObject = false -- ensure that isMovingObject is set to false
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("draw heavy object", 25, 150, 0, 1)
					love.graphics.print("This option is still work in progress and may not work as expected:", 500 * scalefactor, 450 * scalefactor, 0, 1)
				end
			
			--elseif not (isDrawingHeavy) then drawButton (TogleLightButton, hovered)
				local hovered = isButtonHovered (DrawLightObjectButton)
				drawButton (DrawLightObjectButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					isDrawingObject = true
					isMovingObject = false -- ensure that isMovingObject is set to false
					Initialize_Objects(false)	-- false initialize light object
					love.timer.sleep( 0.3 )	
				elseif hovered then
					love.graphics.print("draw light object", 25, 150, 0, 1)
					love.graphics.print("This option is still work in progress and may not work as expected:", 500 * scalefactor, 450 * scalefactor, 0, 1)
				end
			--end

				
			
			local hovered = isButtonHovered (DrawExitButton)
			drawButton (DrawExitButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					drawingMode = "exit"
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("draw exit area", 25, 150, 0, 1)
				end
			
			local hovered = isButtonHovered (ZoomInButton)
			drawButton (ZoomInButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					zoomIn()
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Zoom in", 25, 150, 0, 1)
				end
			
	
			local hovered = isButtonHovered (ZoomOutButton)
			drawButton (ZoomOutButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					zoomOut()
					love.timer.sleep( 0.3 )
				elseif hovered then
					love.graphics.print("Zoom out", 25, 150, 0, 1)
				end
			
	
	
	
				
		
			--[[
				local hovered = isButtonHovered (ToggleGridButton)
			drawButton (ToggleGridButton, hovered, ToggleGridButton.text)
				if hovered and love.mouse.isDown(1) then 
						if gridVisible==true then gridVisible=false
					elseif gridVisible==false then gridVisible=true
					end
					love.timer.sleep( 0.3 )	
				end
				--]]
				
			if drawingMode=="object" then
				local hovered = isButtonHovered (ShowHelpButton)
				drawButton (ShowHelpButton, hovered, ShowHelpButton.text)
					if hovered and love.mouse.isDown(1) then 
						objectHelpShowed=false
						love.timer.sleep( 0.3 )	
					end
				
			elseif drawingMode~="object" then
							--[[
			local hovered = isButtonHovered (ExportasGraphicButton)
			drawButton (ExportasGraphicButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					exportLevelAsImage()
					love.timer.sleep( 0.3 )	
				end
			--]]
			end
			
		


end
