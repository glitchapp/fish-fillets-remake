function loadLevelEditorButtons()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	newWidth=0
	newHeight=0
		
	levelEditorContainer = {
    x = 0,  -- Initial x-coordinate of the container
    y = 0   -- Initial y-coordinate of the container
	}
	
	 levelEditorButtons = {}  -- Initialize the levelEditorButtons table

    -- Add each button to the levelEditorButtons table
    table.insert(levelEditorButtons, DrawWallsButton)
    table.insert(levelEditorButtons, DrawBordersButton)
    table.insert(levelEditorButtons, AddLineButton)
    table.insert(levelEditorButtons, AddRectangleButton)
    table.insert(levelEditorButtons, AddCircleButton)
    table.insert(levelEditorButtons, DrawEmptySpacesButton)
    table.insert(levelEditorButtons, DrawAgent1Button)
    table.insert(levelEditorButtons, DrawAgent2Button)
    table.insert(levelEditorButtons, DrawExitButton)
    table.insert(levelEditorButtons, DrawHeavyObjectButton)
    table.insert(levelEditorButtons, DrawLightObjectButton)
    
	
	if res=="1080p" then
		desktopHeight=desktopHeight-400		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-600
	elseif res=="720p" then
		desktopHeight=desktopHeight-600		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-1200
	
	end
		
		exitLevelEditorButton = {
		text = "Exit",
		x = 100, y = 1000*scalefactor, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
		
	DrawWallsButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 200,
    sx = 1,
    image = ShellIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	

	DrawEmptySpacesButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 200,
    sx = 1,
    image = Eraser,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}

	
	
	AddLineButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 275,
    sx = 1,
    image = AddLineIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	AddRectangleButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 275,
    sx = 1,
    image = AddRectangleIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	AddCircleButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 350,
    sx = 1,
    image = AddCircleIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
			
		
	DrawBordersButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 425,
    sx = 1,
    image = borderIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
			
	DrawExitButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 425,
    sx = 1,
    image = ExitEditor,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}

	

	isDrawingHeavy=true	-- default true

	DrawHeavyObjectButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 500,
    sx = 1,
    image = heavyIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	DrawLightObjectButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 500,
    sx = 1,
    image = lightIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	DrawAgent1Button = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 575,
    sx = 1,
    image = nesfish4x2mini,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	DrawAgent2Button = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 575,
    sx = 1,
    image = nesfish3x1mini,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}



	ZoomInButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x,
	y = levelEditorContainer.y + 625,
    sx = 1,
    image = zoomInIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	ZoomOutButton = {
	text = "",
	textPosition ="top",
	x = levelEditorContainer.x + 75,
	y = levelEditorContainer.y + 625,
    sx = 1,
    image = zoomOutIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
	ShowHelpButton = {
		text = "Help",
		x = 1800, 
		y = desktopHeight-150,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	

	
end
