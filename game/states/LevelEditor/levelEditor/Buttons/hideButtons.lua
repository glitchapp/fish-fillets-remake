function hideLevelEditorButtons(button, hovered,text)
	if gamestatus=="levelEditor" then
			if button.text=="Walls" then shortcutText="1"
		elseif button.text=="Borders" then shortcutText="2"
		elseif button.text=="Empty" then shortcutText="3"
		elseif button.text=="Agent 1" then shortcutText="4"
		elseif button.text=="Agent 2" then shortcutText="5"
		elseif button.text=="Exit area" then shortcutText="6"
		elseif button.text=="Object" then shortcutText="7"
		elseif button.text=="Heavy" then shortcutText="8"
		elseif button.text=="Light" then shortcutText="8"
		elseif button.text=="Export level" then shortcutText="e"
		else shortcutText=""
		end
	end	
	
	if gamestatus=="levelEditor" and not (timer3>5) then									--If the button is being hovered over, 
		--shortcuts
			
		love.graphics.setColor(1,1,1,5-timer3)
		
		
		if not (shortcutText=="") then
			love.graphics.draw(KeyIcon, button.x,button.y+50, button.r,0.4)
		end
		love.graphics.setColor(0,0,0,5-timer3)
		love.graphics.print(shortcutText,button.x+10,button.y+60, button.r,button.sx*2)
		
		love.graphics.setColor(1,1,1,5-timer3)
	elseif gamestatus=="levelEditor" and timer3>2.8 then
			
			love.graphics.setColor(1,1,1,(-timer3)+5)
			if drawingMode=="object" then
				objectHelpShowed=true
			end
		
	end
	
		local mx, my = love.mouse.getPosition()
		
		if res=="1440p" or res=="4k" then
			--print("mx:" .. mx .. " my: " ..my .. " timer3: " .. timer3)
			-- if mouse hover the left side of the screen
			
			if mx <200 or (joystick and emulatedmousexscale>200) then
				timer3=0
			end
			-- if mouse hover the right side of the screen
			if mx >2000 or (joystick and emulatedmousexscale>2000) then
				timer3=0
			end
			
			--[[
			-- if mouse hover the top of the screen
			if my <130 or (joystick and emulatedmouseyscale<130) then
				timer3=0
			end
			--]]
			
			-- if mouse hover the bottom of the screen
			if my >1000 or (joystick and emulatedmouseyscale>1000) then
				timer3=0
			end
			
		elseif res=="1080p" then
		
		-- if mouse hover the left side of the screen
			if mx <150 or (joystick and emulatedmousexscale>2000) then
				timer3=0
			end
		-- if mouse hover the right side of the screen
			if mx >1600 or (joystick and emulatedmousexscale>1600) then
				timer3=0
			end
			-- if mouse hover the bottom of the screen
			if my >700 or (joystick and emulatedmouseyscale>600) then
				timer3=0
			end
		elseif res=="720p" then
		
		-- if mouse hover the left side of the screen
			if mx <100 or (joystick and emulatedmousexscale>2000) then
				timer3=0
			end
		-- if mouse hover the right side of the screen
			if mx >800 or (joystick and emulatedmousexscale>1600) then
				timer3=0
			end
			-- if mouse hover the bottom of the screen
			if my >400 or (joystick and emulatedmouseyscale>600) then
				timer3=0
			end
		end
	
end
