function loadBackgroundButtons()

if level1thumb==nil then

	level1thumb =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/thumbnails/level-1.webp")))
	level2thumb =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/thumbnails/level-2.webp")))
end

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

	if res=="1080p" then
		desktopHeight=desktopHeight-400		-- This prevents the buttons to be hidden outside the window
		desktopWidth=desktopWidth-600
	end

background1Button = {
    text = "Fish house",
    textPosition ="top",
	x = desktopWidth-800, 
	y = 400,
    sx = 1,
    image = level1thumb,
    --image = love.graphics.newImage("assets/icons/fficon.png"),
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
}

background2Button = {
    text = "Briefcase message",
    textPosition ="top",
  	x = desktopWidth-400, 
	y = 400,
    sx = 1,
    image = level2thumb,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = false,
}

ClearBackgroundButton = {
		text = "Clear Background",
		x = desktopWidth-800, 
		y = 800,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

end


function drawLevelEditorBackgroundButtons()

local desktopWidth, desktopHeight = love.window.getDesktopDimensions()

    local hovered = isButtonHovered (background1Button)
			drawButton (background1Button, hovered)
				if hovered and love.mouse.isDown(1) then 
					nLevel=1
					loadlevelvariables()
					inputtime=0
					loadlevelassets()			-- load assets
					--loadlevelmusic()			-- load music
					--assignfrgbck()
					currentbck=level1bck   currentbck2=level1bck2 currentbck3=false currentbck4=whale
					currentfrg=false
					expx=(32*gridSize)/3840 expy=(32*gridSize)/2120
					loadwhale()					-- load whale animation

					BackgroundSet=true
					
					love.timer.sleep( 0.3 )	
				end
				
	local hovered = isButtonHovered (background2Button)
			drawButton (background2Button, hovered)
				if hovered and love.mouse.isDown(1) then 
					nLevel=2
					loadlevelvariables()
					inputtime=0
					loadlevelassets()			-- load assets
					assignfrgbck()
					currentfrg=false
					expx=(32*gridSize)/3840 expy=(32*gridSize)/2120
					--loadlevelmusic()			-- load music
						
					
					BackgroundSet=true
					love.timer.sleep( 0.3 )	
				end
				
		local hovered = isButtonHovered (ClearBackgroundButton)
			drawButton (ClearBackgroundButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					BackgroundSet=false
				end
end

