function loadExportdata()
	
	export_dir = love.filesystem.getSaveDirectory()					-- assign directory path to save_dir
	
end


function fadesExportInformation()
		--show screenshot information for 3 seconds
		--if timer3>5 then
		-- hide the mouse pointer
			--if showExportPath==true then
				--mousestate = not love.mouse.isVisible()
				--love.mouse.setVisible(mousestate)
			--end
			--showExportPath=false
		--end
		
	if levelexported then
		if timer3>0 and timer3<5 then
			love.graphics.setColor(0.5,0.5,0.5,1-(timer3-4))
			love.graphics.rectangle( "fill", 475*scalefactor, 425*scalefactor,1000*scalefactor,300*scalefactor)
			love.graphics.setColor(1,1,1,1-(timer3-4))
			love.graphics.print ("Custom level saved on directory:",500*scalefactor, 450*scalefactor,0,1)
			love.graphics.print ("press open directory to open it:",500*scalefactor, 550*scalefactor,0,1)
			
			love.graphics.print (export_dir,500*scalefactor, 500*scalefactor,0,1)
			love.graphics.setFont( poorfish )
			if timer3>4.5 then
				levelexported=false
				music:setVolume(0.2)
			end
		end	
	end	
	
	if notEnoughAgents then
		if timer3>-1 and timer3<5 then
			love.graphics.setColor(0.5,0.5,0.5,1-(timer3-4))
			love.graphics.rectangle( "fill", 475*scalefactor, 425*scalefactor,1000*scalefactor,300*scalefactor)
			love.graphics.setColor(1,1,1,1-(timer3-4))
			love.graphics.print ("You need to place both agents in the grid before exporting.",500*scalefactor, 450*scalefactor,0,1)
			
			love.graphics.setFont( poorfish )
			if timer3>4.5 then
				notEnoughAgents=false
			end
		end	
	end
	
		
		
	end
	
function exportLevel()
   -- Check if both agents are set up
    if #level.agents < 2 then
        print("You need to set up both agents in the level editor before exporting.")
        timer3=0
        notEnoughAgents=true
        return
    end


    -- Generate Lua code to represent the current level
    local levelCode = "local level = {}\n\n"
    
    -- Add level name
    levelCode = levelCode .. "level.name = 'level-1'\n\n"

    levelCode = levelCode .. "level.map = {\n"
    for y = 1, gridHeight do
        levelCode = levelCode .. "    {"
        for x = 1, gridWidth do
            levelCode = levelCode .. grid[x][y]
            if x < gridWidth then
                levelCode = levelCode .. ","
            end
        end
        levelCode = levelCode .. "}"
        if y < gridHeight then
            levelCode = levelCode .. ","
        end
        levelCode = levelCode .. "\n"
    end
    levelCode = levelCode .. "}\n"

    -- Add agent, object, exit area, and custom tile data
    levelCode = levelCode .. "level.customTiles = {\n"
    for x, tileRow in pairs(editorTileData) do
        levelCode = levelCode .. "    {"
        for y, tileData in pairs(tileRow) do
            levelCode = levelCode .. "{" .. (tileData.tile or 0) .. "," .. (tileData.tileType or 0) .. "}"
            if y < gridHeight then
                levelCode = levelCode .. ","
            end
        end
        levelCode = levelCode .. "}"
        if x < gridWidth then
            levelCode = levelCode .. ","
        end
        levelCode = levelCode .. "\n"
    end
    levelCode = levelCode .. "}\n"

     -- Add agent and block (formerly object) data
    levelCode = levelCode .. "level.agents = {\n"
    for _, agent in ipairs(level.agents) do
        levelCode = levelCode .. "    {name = '" .. agent.name .. "', fish = " .. tostring(agent.fish) ..
                    ", heavy = " .. tostring(agent.heavy) .. ", x = " .. agent.x .. ", y = " .. agent.y ..
                    ", form = {\n"
        for i, row in ipairs(agent.form) do
            levelCode = levelCode .. "        {" .. table.concat(row, ",") .. "}"
            if i < #agent.form then
                levelCode = levelCode .. ","
            end
            levelCode = levelCode .. "\n"
        end
        levelCode = levelCode .. "    }},\n"
    end
    levelCode = levelCode .. "}\n"

     -- Add block data
    levelCode = levelCode .. "level.blocks = {\n"
    for _, block in ipairs(level.blocks) do
        levelCode = levelCode .. "    {name = '" .. block.name .. "', heavy = " .. tostring(block.heavy) ..
                    ", x = " .. block.x .. ", y = " .. block.y .. ", form = {\n"
        for i, row in ipairs(block.form) do
            levelCode = levelCode .. "        {" .. table.concat(row, ",") .. "}"
            if i < #block.form then
                levelCode = levelCode .. ","
            end
            levelCode = levelCode .. "\n"
        end
        levelCode = levelCode .. "    }},\n"
    end
    levelCode = levelCode .. "}\n"

	-- this is the table currently used to designate the exit area, it will be deprecated in favor of writing the exit area directly in the grid with a value
	-- Add exit area data
    levelCode = levelCode .. "level.areas = {\n"
    levelCode = levelCode .. "    {\n"
    levelCode = levelCode .. "        name = 'exit',\n"
    levelCode = levelCode .. "        x = 0,\n"
    levelCode = levelCode .. "        y = 0,\n"
    levelCode = levelCode .. "        w = 1,\n"
    levelCode = levelCode .. "        h = 1,\n"
    levelCode = levelCode .. "    },\n"
    levelCode = levelCode .. "}\n"

    -- Add the code to prepare the map
    levelCode = levelCode .. "prepareMap(level)\n\n"

    -- Add the return statement
    levelCode = levelCode .. "return level\n"

    -- Save the generated code to a file
    love.filesystem.write("customlevel.lua", levelCode)

    print("Level code saved to customlevel.lua")
    music:setVolume(0.05)
    SaveSound:play()
    levelexported = true
end
