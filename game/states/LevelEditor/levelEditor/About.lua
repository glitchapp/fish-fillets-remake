function LoadAboutLevelEditor()

	AboutCloseButton = {
		text = "Close Window",
		x = 1400, 
		y = 360,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

		AboutHyperlinkButton = {
		text = "Repository",
		x = 800, 
		y = 900,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

end

function drawAbout()
    if aboutInfo == true then
        love.graphics.setColor(0.2, 0.2, 0.2, 0.8)
        love.graphics.rectangle("fill", 800, 350, 800, 50)
        love.graphics.rectangle("fill", 800, 400, 800, 600)
        love.graphics.setColor(1, 1, 1)
        love.graphics.rectangle("line", 800, 350, 800, 50)
        love.graphics.rectangle("line", 800, 400, 800, 600)
        love.graphics.print("About",  800, 360, 0, 1)

        local maxWidth = 800
        local lineHeight = love.graphics.getFont():getHeight()

        local paragraph = "Sokoban level editor <br>" ..
                          "Copyright (C) 2023 Glitchapp <br>" ..
                          ". <br>" ..
                          "This program is free software: you can redistribute it and/or modify " ..
                          "it under the terms of the GNU General Public License as published by " ..
                          "the Free Software Foundation, either version 2 of the License, or " ..
                          "(at your option) any later version.<br>" ..
                          ". <br>" ..
                          "This program is distributed in the hope that it will be useful, " ..
                          "but WITHOUT ANY WARRANTY; without even the implied warranty of " ..
                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " ..
                          "GNU General Public License for more details." ..
                          ". <br>"

        -- Replace "<br>" with newline characters "\n"
        paragraph = string.gsub(paragraph, "<br>", "\n")

        -- Split the paragraph into lines respecting the maximum width
        local lines = {}
        local currentLine = ""
        for line in paragraph:gmatch("[^\n]+") do
            local words = {}
            for word in line:gmatch("%S+") do
                table.insert(words, word)
            end

            local testLine = currentLine
            for _, word in ipairs(words) do
                local testLineWidth = love.graphics.getFont():getWidth(testLine .. word .. " ")
                if testLineWidth <= maxWidth then
                    testLine = testLine .. word .. " "
                else
                    table.insert(lines, testLine)
                    testLine = word .. " "
                end
            end

            if testLine ~= "" then
                table.insert(lines, testLine)
            end
            currentLine = ""
        end

        local x, y = 800, 410
        for _, line in ipairs(lines) do
            love.graphics.print(line, x, y)
            y = y + lineHeight
        end
    end
    
    if aboutInfo==true then
     	local hovered = isButtonHovered (AboutCloseButton)
				drawButton (AboutCloseButton, hovered)
					if hovered and love.mouse.isDown(1) then 
						aboutInfo=false
						love.timer.sleep( 0.3 )	
					end
    
    	local hovered = isButtonHovered (AboutHyperlinkButton)
				drawButton (AboutHyperlinkButton, hovered)
					if hovered and love.mouse.isDown(1) then 
							love.system.openURL("https://codeberg.org/glitchapp/Grid-Level-Editor")
						love.timer.sleep( 0.3 )	
					end
    end
end
