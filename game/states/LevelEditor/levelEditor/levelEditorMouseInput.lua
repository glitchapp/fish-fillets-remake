-- Define variables to track mouse state
local isMousePressed = false
local pixelStartX, pixelStartY = 0, 0 -- Inizialite with default values
local gridStartX, gridStartY = 0, 0 -- Track grid cell coordinates (in cells)
local endX, endY = 0, 0

function love.mousepressed(x, y, button, istouch, presses)
    if love.mouse.isDown(1) then
        -- Start tracking mouse coordinates and state
        isMousePressed = true
        pixelStartX, pixelStartY = x, y
        
        -- Calculate the grid cell position based on the initial coordinates
        --local adjustedX = x - gridOffsetX
        --local adjustedY = y - gridOffsetY

        --gridStartX = math.floor(adjustedX / cellSize) + 1
        --gridStartY = math.floor(adjustedY / cellSize) + 1
        
        if nLevel==120 then
			gamestatus="levelEditor"	-- return to level editor by simply clicking the mouse anywhere
        end
    end
end

function love.mousemoved(x, y, dx, dy, istouch)
    local gridOffsetX = 200
    local gridOffsetY = 200

    if gamestatus == "levelEditor" then
        if love.mouse.isDown(1) then
            -- Adjust x and y coordinates for the zoomed grid
            x = x - gridOffsetX
            y = y - gridOffsetY

            -- Calculate the cell position based on the adjusted coordinates
            local cellX = math.floor(x / cellSize) + 1
            local cellY = math.floor(y / cellSize) + 1

            if cellX >= 1 and cellX <= gridWidth and cellY >= 1 and cellY <= gridHeight then
                -- Update the aesthetic grid based on the selected tile
                if TileSet then
                    aestheticTileData[cellX] = aestheticTileData[cellX] or {}
                    aestheticTileData[cellX][cellY] = selectedTile
                end

                if drawingMode == "wall" then
                    grid[cellX][cellY] = 1
                elseif drawingMode == "agent" then
                    addAgent(cellX, cellY)
                elseif drawingMode == "agent2" then
                    addAgent2(cellX, cellY)
                elseif drawingMode == "exit" then
                    addExitArea(cellX, cellY)
                elseif drawingMode == "border" then
                    grid[cellX][cellY] = 3
                elseif drawingMode == "emptyCell" then
                    grid[cellX][cellY] = 0
                end

				-- Update pixelStart
				--pixelStartX, pixelStartY = x, y
                -- Update gridStartX and gridStartY for grid cell coordinates
                --gridStartX, gridStartY = cellX, cellY
                      -- Update endX and endY
                endX, endY = cellX, cellY

               -- Update endX and endY
                endX, endY = cellX, cellY
            end
        end
    end
end

function love.mousereleased(x, y, button, istouch, presses)
    -- Stop tracking mouse coordinates and state on mouse release
    isMousePressed = false
    
    if pixelStartX == nil then pixelStartX = 0 end
    if pixelStartY == nil then pixelStartY = 0 end
    
    -- Calculate the grid cell positions based on the starting pixel coordinates
    gridStartX = math.floor(pixelStartX / cellSize) + 1
    gridStartY = math.floor(pixelStartY / cellSize) + 1
    endX = math.floor(x / cellSize) + 1
    endY = math.floor(y / cellSize) + 1

    -- Update the grid based on the drawn shape
    if drawingMode == "line" then
        print("Drawing line from (" .. gridStartX .. ", " .. gridStartY .. ") to (" .. endX .. ", " .. endY .. ")")
        addLineToGrid(gridStartX, gridStartY, endX, endY)
    elseif drawingMode == "rectangle" then
        print("Drawing rectangle from (" .. gridStartX .. ", " .. gridStartY .. ") to (" .. endX .. ", " .. endY .. ")")
        addRectangleToGrid(gridStartX, gridStartY, endX, endY)
    elseif drawingMode == "circle" then
        print("Drawing circle at (" .. gridStartX .. ", " .. gridStartY .. ") with radius " .. endX)
        addCircleToGrid(gridStartX, gridStartY, endX, endY)
    end
end
