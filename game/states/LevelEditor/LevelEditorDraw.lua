
function loveDrawLevelEditor(dt)

local gridOffsetX = 200
local gridOffsetY = 200

	if BackgroundSet==true then
		drawforshader2()
	end

if drawingMode=="object" then
	love.graphics.setFont( poorfishsmall )
	love.graphics.setColor(1,1,1,1)
	love.graphics.print ("Info for debuging, this feature is a work in progres and may not work as expected:", game.screen_width-800,100,0, 0.8, 1)
	love.graphics.print ("Drawing Mode:" .. drawingMode, game.screen_width-800,150,0, 0.8, 1)
	love.graphics.print ("isDrawingObject:" .. tostring(isDrawingObject), game.screen_width-800,200,0, 0.8, 1)
	love.graphics.print ("isMovingObject:" .. tostring(isMovingObject), game.screen_width-800,250,0, 0.8, 1)
	love.graphics.print ("isPlacingObject:" .. tostring(isPlacingObject), game.screen_width-800,300,0, 0.8, 1)
	love.graphics.print ("Current object:" .. tostring(currentObject), game.screen_width-800,350,0, 0.8, 1)
	
	love.graphics.setFont( poorfish )
	
end


	if gridVisible==true then
			
		if BackgroundSet then
			love.graphics.setColor(1,1,1,0.5)
		else
			love.graphics.setColor(0.5, 0.5, 0.5)  -- Set grid color (gray)
		end
			drawGrid()	-- Draw the grid lines
		--love.graphics.setColor(1, 1, 1)  -- Reset color to white
	end

    -- Draw the grid
    for x = 1, gridWidth do
        for y = 1, gridHeight do
            local cellType = grid[x][y]
            
            -- Adjust the x and y coordinates by the cell size and grid offsets
            local cellX = (x - 1) * cellSize + gridOffsetX
            local cellY = (y - 1) * cellSize + gridOffsetY
            

            -- Draw cells based on their type
        if cellType == 1 then  -- walls
            love.graphics.setColor(1, 1, 1)  -- white
            
				if not (selectedTile == 0) then
					love.graphics.draw(tileTextures[selectedTile], cellX, cellY, 0, 0.3)
				else
					 love.graphics.rectangle("fill", cellX, cellY, cellSize, cellSize)
				end
			
        elseif cellType == 3 then  -- borders
            love.graphics.setColor(0, 0, 128 / 255)
           
				if not (selectedTile == 0) then
					love.graphics.draw(tileTextures[selectedTile], cellX, cellY, 0, 0.3)
				else
					 love.graphics.rectangle("fill", cellX, cellY, cellSize, cellSize)
				end
			
            --love.graphics.setColor(1, 1, 1)
        elseif cellType == 6 then  -- exit areas
            love.graphics.setColor(1, 236 / 255, 139 / 255)  -- harmonious yellow
			love.graphics.rectangle("fill", cellX, cellY, cellSize, cellSize)
            
        end
            
            
	--[[gridElementColor=
	{
		{66, 66, 66},						--Walls: Dark Gray					1
		{0, 0, 128},						--Borders: Navy Blue				2
		{ 65, 105, 225},					--Agent 1: Royal Blue				3
		{255, 69, 0},						--Agent 2: Orange Red				4
		{152 / 255, 251 / 255, 152 / 255},	--Light Objects: Light Yellow		5
		{139, 69, 19},						--Heavy Objects: Dark Brown			6
		{128, 0, 128}						--Exit Area: Purple					7
		
		{152 / 255, 251 / 255, 152 / 255}  -- Light Green (Harmonious with the palette) light objects

	}--]]


           -- Draw agents with different colors
for _, agent in ipairs(level.agents) do
    if agent.name == "fish-4x2" then
        love.graphics.setColor(65 / 255, 105 / 255, 225 / 255)  --Agent 1: Royal Blue
    elseif agent.name == "fish-3x1" then
        love.graphics.setColor(255 / 255, 69 / 255, 0)  -- Set color for agent 2 (green)
    end
    
    for i = 1, #agent.form do
        for j = 1, #agent.form[i] do
            if agent.form[i][j] == 1 then
                local cellX = (agent.x + j - 1) * cellSize + gridOffsetX -- Calculate the X position
                local cellY = (agent.y + i - 1) * cellSize + gridOffsetY -- Calculate the Y position
                love.graphics.rectangle("fill", cellX, cellY, cellSize, cellSize)
            end
        end
    end
end

 
 
 if drawingMode == "object" then
 
    for _, object in ipairs(level.blocks) do
		
		 -- Adjust the x and y coordinates by the cell size and grid offsets
            local cellX = (object.x - 1) * cellSize + gridOffsetX
            local cellY = (object.y - 1) * cellSize + gridOffsetY
        -- Set the appropriate color for objects
        local objectColor = {139 / 255, 69 / 255, 19 / 255}  -- Default color (white) for objects

        -- Set the object color based on type (light or heavy)
        if object.heavy then
            objectColor = {139 / 255, 69 / 255, 19 / 255}  -- Dark Brown for heavy objects
        else
            objectColor = {152 / 255, 251 / 255, 152 / 255}  -- Light Yellow for light objects
        end

        love.graphics.setColor(unpack(objectColor))

        for i = 1, #object.form do
            for j = 1, #object.form[i] do
                if object.form[i][j] == 1 then
                    local cellX = (object.x + j - 1) * 32  -- Calculate the X position
                    local cellY = (object.y + i - 1) * 32  -- Calculate the Y position
                   love.graphics.rectangle("fill", cellX, cellY, cellSize, cellSize)

                    -- Highlight the borders for heavy objects
                    if object.heavy then
                        love.graphics.setColor(0, 0, 128 / 255)  -- Navy Blue for borders
                         love.graphics.rectangle("line", cellX, cellY, cellSize, cellSize)  -- Draw border
                        love.graphics.setColor(unpack(objectColor))  -- Reset color
                    end
                end
            end
        end
    end
end



    love.graphics.setColor(1, 1, 1)  -- Reset color to white
        end
    end

    --drawLevelEditorHelp()
	drawObjectHelp()
	
    love.graphics.setFont( poorfishsmall )
    if not (drawingMode=="object") then
			if not (drawingMode=="emptyCell") then
				love.graphics.print ("Drawing:" .. drawingMode, DrawWallsButton.x,DrawWallsButton.y-100,0, 0.8, 1)
			elseif drawingMode=="emptyCell" then
				love.graphics.print ("Erasing:" , DrawWallsButton.x,DrawWallsButton.y-100,0, 0.8, 1)
			end
	elseif drawingMode=="object" then
				if isDrawingObject then
					love.graphics.print ("Drawing object:", DrawWallsButton.x,DrawWallsButton.y-100,0, 0.8, 1)
			elseif isMovingObject then
					love.graphics.print ("Moving object:", DrawWallsButton.x,DrawWallsButton.y-100,0, 0.8, 1)
			end
	end
    
    drawLevelEditorButtons()
    drawLevelEditorTabs()
    drawAbout()
    
	
    fadesExportInformation() -- render the path and button to access the saved custom level
    
    if screenshottaken then 
		fadescreenshotinformation()
	end

end
