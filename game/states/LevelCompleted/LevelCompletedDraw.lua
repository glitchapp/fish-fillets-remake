
function drawLevelcompleted(dt)
				
			drawdefaultlevelcompletiontext()
			
			if nLevel==1 then 
				love.graphics.print("Music player unlocked!:" ,400,800,0, 0.8, 1) pixelsphereunlocked=true 
				love.graphics.print("Aquaria track unlocked!:" ,400,900,0, 0.8, 1) aquaria_song=true
				love.graphics.print("rybky4 song unlocked!:" ,400,1000,0, 0.8, 1) rybky4_song=true
				musicplayerunlocked=true
				classictracksunlocked=true

			end
			
			if nLevel==2 then 	love.graphics.print("Underwater ambient Pad unlocked!:" ,400,900,0, 0.8, 1)
								love.graphics.print("rybky15 song unlocked!:" ,400,1000,0, 0.8, 1)
				isaiah658unlocked=true
				underwater2_song=true
				--classic songs
				rybky15_song=true
			end
			
			if nLevel==3 then 	love.graphics.print("Mysterious ambient song unlocked!:" ,400,900,0, 0.8, 1)
								love.graphics.print("rybky03 song unlocked!:" ,400,1000,0, 0.8, 1)
				unlockedlevels[9]=true
				song21_song=true
				--classic songs
				rybky03_song=true
				
			end
			
			if nLevel==4 then 	love.graphics.print("Crystal cave song unlocked!:" ,400,900,0, 0.8, 1)
								love.graphics.print("rybky01 song unlocked!:" ,400,1000,0, 0.8, 1)
				unlockedlevels[20]=true
				song18_song=true
				--classic songs
				rybky01_song=true
			end
			
			if nLevel==5 then love.graphics.print("Crystal cave song unlocked!:" ,400,900,0, 0.8, 1)
				unlockedlevels[65]=true
				song18_song=true
			end
			
			if nLevel==6 then love.graphics.print("Ambient Relaxing Loop unlocked!:" ,400,900,0, 0.8, 1) isaiah658unlocked=true ambient_song=true
				unlockedlevels[30]=true
			end
			
			if nLevel==7 then 	love.graphics.print("Dreaming of reefs track unlocked!:" ,400,900,0, 0.8, 1) unlockedlevels[52]=true ericmatyasunlocked=true dream_song=true
								love.graphics.print("rybky07 song unlocked!:" ,400,1000,0, 0.8, 1) 	rybky07_song=true
			end
			
			if nLevel==8 then love.graphics.print("Deep sea unlocked!:" ,400,900,0, 0.8, 1)
				fishhouseunlockedend=true
				umplixunlocked=true
				deepsea_song=true
				
				--classic songs
				rybky10_song=true
			end
			
			if nLevel==9 then love.graphics.print("Sinking feeling unlocked!:" ,400,900,0, 0.8, 1) 	sinkingfeeling_song=true end
			
			if nLevel==10 then 	love.graphics.print("Enchanted tiki unlocked!:" ,400,900,0, 0.8, 1) enchanted_song=true
								love.graphics.print("rybky09 song unlocked!:" ,400,1000,0, 0.8, 1) rybky09_song=true
			end
			
			if nLevel==11 then love.graphics.print("Ambient nautilus unlocked!:" ,400,900,0, 0.8, 1) nautilus_song=true end
			
			if nLevel==12 then love.graphics.print("rybky11 song unlocked!:" ,400,1000,0, 0.8, 1) rybky11_song=true end
			if nLevel==13 then love.graphics.print("rybky06 song unlocked!:" ,400,1000,0, 0.8, 1)
				rybky06_song=true
			end
			if nLevel==14 then 	love.graphics.print("rybky05 song unlocked!:" ,400,900,0, 0.8, 1) rybky05_song=true
								love.graphics.print("Ambient II song unlocked!:" ,400,1000,0, 0.8, 1) ambientII_song=true				
			end
			
			--unlock ends once last level of the area is completed
			
			if nLevel==20 then 	love.graphics.print("rybky07 song unlocked!:" ,400,900,0, 0.8, 1) rybky07_song=true
								love.graphics.print("Sirens in darkness unlocked!:" ,400,1000,0, 0.8, 1) sirens_song=true			
			end
				
				
				if nLevel==19 then
					shipwrecksunlockedend=true
					shipwrecksendload()
					gamestatus="shipwrecksend"
			
			elseif nLevel==29 then
				cityinthedeepunlockedend=true
				cityinthedeependload()
				gamestatus="cityinthedeepend"
			
			elseif nLevel==37 then 
				coralreefunlockedend=true
				coralreefendload()
				gamestatus="coralreefend"
				
			elseif nLevel==44 then
				dumpunlockedend=true
				barrelendload()
				gamestatus="barrelend"
				
			elseif nLevel==51 then
				silversshipunlockedend=true
				silversshipendload()
				gamestatus="silversshipend"
				
			elseif nLevel==58 then
				ufounlockedend=true
				ufoendload()
				gamestatus="ufoend"
				
			elseif nLevel==64 then
				treasurecaveunlockedend=true
				treasurecaveendload()
				gamestatus="treasurecaveend"
				
			elseif nLevel==70 then
				secretcomputerunlockedend=true
				secretcomputerendload()
				gamestatus="secretcomputerend"
				
			elseif nLevel==77 then
				nextgenerationunlockedend=true
				linuxendload()
				gamestatus="linuxend"
				
			elseif nLevel==79 then
				nextgenerationunlockedend=true
				cutscenesunlocked=true
				gameendload()
				gamestatus="gameend"
			end
			
			
			
			-- and not (nLevel==29) and not (nLevel==37) and not (nLevel==44) and not (nLevel==51) and not (nLevel==58) and not (nLevel==64) and not (nLevel==70) and not (nLevel==79) then
			
			if nLevel==38 then
				unlockedlevels[45]=true
			end
			if nLevel==39 then
				unlockedlevels[71]=true
			end
			
			if nLevel==40 then
				unlockedlevels[59]=true
			end
			
			
end

--[[
This function is responsible for drawing the default text when a level is completed. It first disables some visual effects and enables a box blur effect. Then, it uses a shader to draw all the content and some special content for the shader.

If the level completion sentence has not been said, it sets a compressor audio effect, sets the volume of the audio, and displays the completion message, along with the time taken and steps taken to complete the level.

Finally, it draws a "Continue" button using the isButtonHovered and drawButton functions. If the button is hovered over and clicked, it stops the current audio, sets a reverb effect, loads a new audio file, and sets various visual effects before changing the game status to "levelselection".
--]]
function drawdefaultlevelcompletiontext()

			--video effect
			-- Disable all effects except for the box blur
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
			
			-- Set the shader to use the current effects and draw all content						
			love.graphics.setShader()
			effect(function()
	
				drawallcontent()
				drawforshader2()
			end)

		-- If the level completed sentence has not been said yet, play a sound effect
		if levelcompletedsentencesaid==false then
			
			-- Set the music effect to "myEffect" and set the volume to 0.3
			if not (mus==nil) then
				mus:setEffect('myEffect')
				mus:setVolume(0.3)
			else music:setVolume(0.1)
			end
		end	
		    -- Draw the level completion text, the time taken to complete the level, and the number of steps taken
			love.graphics.print("Level completed",600,300,0, 0.8, 1)
			love.graphics.print("Time:" .. timeneeded,600,400,0, 0.8, 1)
			love.graphics.print("Steps:" .. steps,600,500,0, 0.8, 1)
			
		
			--rate steps and time
			if nLevel==1 then
				
				rate = (ratesteps / steps) * 100
				love.graphics.print("rate: " .. rate .. "%",600,600,0, 0.8, 1)

					if rate>100 then love.graphics.print("Congratulations! Your rate is: " .. rate .. " which is higher than 100%",600,650,0, 0.8, 1)
				elseif rate==100 then love.graphics.print("Congratulations! Your rate is exactly: " .. rate .. "%",600,650,0, 0.8, 1)
				elseif rate<100 then love.graphics.print("Try to complete with less than: " .. ratesteps .. " steps to achieve 100%",600,650,0, 0.8, 1)
				end
			
			end
						-- continue

				hovered = isButtonHovered (continueButton)
				drawButton (continueButton, hovered,"Continue")
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					if love.mouse.isDown(1) then
						--cursor = love.mouse.newCursor("/assets/interface/ichtys_cursor100.png", 0, 0 )	--restore cursor
						--love.mouse.setCursor(cursor)
						--mousestate = not love.mouse.isVisible()	-- show mouse pointer
						--love.mouse.setVisible(mousestate)
						shader2=false
						levelcompletedsentencesaid=false
						if PlayingFromLevelEditor==false then
							mus:stop()
							setreverbeffect()
							mybpm=112
							lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
							effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
							effect.enable("scanlines","crt","glow","filmgrain")
							love.timer.sleep( 0.2 )
							mus:setVolume(1)
							gamestatus="levelselection" 
						elseif PlayingFromLevelEditor==true then
								if loadLevelEditor()==nil then 
									require ('game/states/levelEditor/leveleditor')
									loadLevelEditor()
								end
							PlayingFromLevelEditor=false
							gamestatus="levelEditor" 
						end
					end
				end
				
				drawLevelCompletedButtonFocus()
	
end


function drawLevelCompletedButtonFocus()
	--if joystick then 	
		buttonfocusanimations()		-- focus color and size animations
		--level menu
			if bfocus=="continuelevelcompleted" 	then love.graphics.rectangle ('line', continueButton.x, continueButton.y, fwidth, fheight)		--fwidth and fheight are the size variables used for the button focus animations
		end
		love.graphics.setColor(1,1,1,1)
	--end
end


function drawLevelcompleted()
	drawlevelcompleted()
	-- Draw FPS metrics
	drawfpsmetrics()
	
	if  PlayingFromLevelEditor==true then
		--draw confetti
		for _, confetto in ipairs(confettiParticles) do
			-- Draw confetti particles as polygons
			love.graphics.setColor(confetto.color)
			love.graphics.rectangle("fill", confetto.x, confetto.y, confetto.size, confetto.size)
		end
   end
end

-- Function to convert HSV color to RGB
function hsvToRgb(h, s, v)
    local r, g, b

    local i = math.floor(h * 6)
    local f = h * 6 - i
    local p = v * (1 - s)
    local q = v * (1 - f * s)
    local t = v * (1 - (1 - f) * s)

    if i % 6 == 0 then
        r, g, b = v, t, p
    elseif i % 6 == 1 then
        r, g, b = q, v, p
    elseif i % 6 == 2 then
        r, g, b = p, v, t
    elseif i % 6 == 3 then
        r, g, b = p, q, v
    elseif i % 6 == 4 then
        r, g, b = t, p, v
    else
        r, g, b = v, p, q
    end

    return r, g, b
end
