--The code appears to be for a game or interactive application, with the functions loadlevelcompleted() and loadlevelcompletedsentences() being called after a level is completed.
function loadlevelcompleted()
	continueButton = {
		text = "Continue",
		x = 500,
		y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	--buttons

confettiParticles = {}
	
	 math.randomseed(os.time())  -- Seed the random number generator

    for i = 1, 100 do
        local confetto = {
            x = love.math.random(0, love.graphics.getWidth()),  -- Random x-coordinate
            y = love.math.random(0, love.graphics.getHeight()), -- Random y-coordinate
            size = love.math.random(5, 20),  -- Random size for the confetto
            hue = love.math.random(),  -- Random hue value (0-1)
            color = {1, 1, 1}  -- Initial color (white)
        }
        table.insert(confettiParticles, confetto)
    end

end

function LoadAndPlayFanfares()
	--fanfares
	--applause = love.audio.newSource( "/externalassets/sounds/levelEditor/applause.ogg","static" )
	--end_level = love.audio.newSource( "/externalassets/sounds/levelEditor/end_level.ogg","static" )
	--victory sting = love.audio.newSource( "/externalassets/sounds/levelEditor/victory sting.ogg","static" )
	--Well_Done_CCBY3 = love.audio.newSource( "/externalassets/sounds/levelEditor/Well_Done_CCBY3.ogg","static" )
	--winneris = love.audio.newSource( "/externalassets/sounds/levelEditor/winneris.ogg","static" )
	
	math.randomseed(os.time())
	aleatorylevelcompletedfanfare = math.random(0,4)
	--print(aleatorylevelcompletedfanfare)
	
				if aleatorylevelcompletedfanfare==0 then	levelcompletedfanfare = love.audio.newSource( "/externalassets/sounds/levelEditor/applause.ogg","static" )
			elseif aleatorylevelcompletedfanfare==1 then	levelcompletedfanfare = love.audio.newSource( "/externalassets/sounds/levelEditor/end_level.ogg","static" )
			elseif aleatorylevelcompletedfanfare==2 then	levelcompletedfanfare = love.audio.newSource( "/externalassets/sounds/levelEditor/victory sting.ogg","static" )
			elseif aleatorylevelcompletedfanfare==3 then	levelcompletedfanfare = love.audio.newSource( "/externalassets/sounds/levelEditor/Well_Done_CCBY3.ogg","static" )
			elseif aleatorylevelcompletedfanfare==4 then	levelcompletedfanfare = love.audio.newSource( "/externalassets/sounds/levelEditor/winneris.ogg","static" )
			end
			
			levelcompletedfanfare:setEffect('myEffect')
			levelcompletedfanfare:play()
			levelcompletedfanfareplayed=true
	
			music:setVolume(0.3)
end

--[[
loadlevelcompletedsentences() creates a levelcompletedsentence object using love.audio.newSource(), which loads an audio file for later playback.
The function also uses a random number generator to select one of several possible audio files to load, based on the values of language and accent (which are presumably set elsewhere in the code). The selected audio file is then assigned to the levelcompletedsentence object. However, the purpose of this object is not entirely clear from this code snippet - it may be played automatically after a level is completed, or triggered by some user action.
--]]
function loadlevelcompletedsentences()
levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/border/empty.ogg","stream" )
	if levelcompletedsentencesaid==false then
				math.randomseed(os.time())
				aleatorylevelcompletedsentence = math.random(0,8)
	if language=="en" then
		if accent=="br" then
				if aleatorylevelcompletedsentence==0 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/shout/1yes.ogg","stream" )					--	"Yeeessss!"
			elseif aleatorylevelcompletedsentence==1 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/shout/2wow.ogg","stream" )					--	"Wow!"
			elseif aleatorylevelcompletedsentence==2 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/shout/3perfect.ogg","stream" )				--	"Perfect!"
			elseif aleatorylevelcompletedsentence==3 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/shout/4done.ogg","stream" )				--	"Done!"
			elseif aleatorylevelcompletedsentence==4 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en/shout/5thatwasreallygood.ogg","stream" )	--	"That was really good!"
			end
		elseif accent=="us" then
				if aleatorylevelcompletedsentence==0 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout/1yes.ogg","stream" )				--	"Yeeessss!"
			elseif aleatorylevelcompletedsentence==1 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout/2wow.ogg","stream" )				--	"Wow!"
			elseif aleatorylevelcompletedsentence==2 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout/3perfect.ogg","stream" )			--	"Perfect!"
			elseif aleatorylevelcompletedsentence==3 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout/4done.ogg","stream" )				--	"Done!"
			elseif aleatorylevelcompletedsentence==4 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout/5thatwasreallygood.ogg","stream" ) --	"That was really good!"
			
			elseif aleatorylevelcompletedsentence==5 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout2/1yes.ogg","stream" ) 			-- "Yeeessss!"
			elseif aleatorylevelcompletedsentence==6 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout2/2good.ogg","stream" ) 			-- "Good!"
			elseif aleatorylevelcompletedsentence==7 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout2/3great.ogg","stream" ) 			-- "Great!"
			elseif aleatorylevelcompletedsentence==8 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/shout2/4yippee.ogg","stream" ) 			-- "Yippee!"
			end
		end
	end
	
	if language=="es" then
		if accent=="es" then
				
		elseif accent=="la" then
				if aleatorylevelcompletedsentence==0 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/shout/1yes.ogg","stream" )					--	"Yeeessss!"
			elseif aleatorylevelcompletedsentence==1 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/shout/2wow.ogg","stream" )					--	"Wow!"
			elseif aleatorylevelcompletedsentence==2 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/shout/3perfect.ogg","stream" )				--	"Perfect!"
			elseif aleatorylevelcompletedsentence==3 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/shout/4done.ogg","stream" )				--	"Done!"
			elseif aleatorylevelcompletedsentence==4 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/shout/5thatwasreallygood.ogg","stream" )	--	"That was really good!"
			end
		end
	end
			if language=="fr" then	
					if aleatorylevelcompletedsentence==0 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/fr/shout/1yes.ogg","stream" )					--	"Yeeessss!"
				elseif aleatorylevelcompletedsentence==1 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/fr/shout/2wow.ogg","stream" )					--	"Wow!"
				elseif aleatorylevelcompletedsentence==2 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/fr/shout/3perfect.ogg","stream" )				--	"Perfect!"
				elseif aleatorylevelcompletedsentence==3 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/fr/shout/4done.ogg","stream" )				--	"Done!"
				elseif aleatorylevelcompletedsentence==4 then	levelcompletedsentence = love.audio.newSource( "/externalassets/dialogs/share/fr/shout/5thatwasreallygood.ogg","stream" )	--	"That was really good!"
				end
			end
		end
			
			levelcompletedsentence:setEffect('myEffect')
			levelcompletedsentence:play()
			levelcompletedsentencesaid=true
	end

