
function updateLevelCompleted(dt)

	if  PlayingFromLevelEditor==true then
	--confetti effect update
     for _, confetto in ipairs(confettiParticles) do
        -- Simulate confetti movement (falling)
        confetto.y = confetto.y + 50 * dt

        -- Reset confetti when it goes off-screen
        if confetto.y > love.graphics.getHeight() then
            confetto.y = -confetto.size
            confetto.x = love.math.random(0, love.graphics.getWidth())
        end

        -- Update confetti color based on hue
        confetto.hue = confetto.hue + 0.001 * dt  -- Adjust the rate of color change
        if confetto.hue > 1 then
            confetto.hue = 0
        end

        -- Convert hue to RGB color
        local r, g, b = hsvToRgb(confetto.hue, 1, 1)
        confetto.color = {r, g, b}
    end
    timer=timer+dt
    end
end

