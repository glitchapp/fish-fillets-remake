
--The function briefcasedraw(dt) is responsible for drawing the briefcase video and the briefcase image on the screen.
function briefcasedraw(dt)
	
	--If the variable briefcasevideoplaying is true, it will draw the briefcase video at a certain position based on the skin selected. The position and scale of the video are adjusted based on the skin used.
	if briefcasevideoplaying==true then
			--[[if skinupscaled==false then	love.graphics.draw(briefcasevideo, 200, 50,0,3.2)
		elseif skinupscaled==true then	love.graphics.draw(briefcasevideo, 200, 50,0,0.8)
		end--]]
		if res=="1080p" then
				if skin=="classic" then love.graphics.draw(briefcasevideo, 200, 50,0,0.8)
			elseif skin=="remake"  then love.graphics.draw(briefcasevideo, 150, 75,0,1.1,0.8)
			end
		elseif res=="1440p" then
				if skin=="classic" then love.graphics.draw(briefcasevideo, 250, 50,0,1.4,1.2)
			elseif skin=="remake"  then love.graphics.draw(briefcasevideo, 250, 75,0,1.4,1.2)
			end
		end
	end
	
	--Then, the function draws the briefcase image on the screen at the position (0, 0).
		if res=="1080p" then love.graphics.draw (briefcaseopen2, 0,0,0,1,1)
	elseif res=="1440p" then love.graphics.draw (briefcaseopen2, -50,0,0,1.4,1.4)
	end
	love.graphics.setFont(poorfish)
	
	
		Talkies.draw() -- Draw text box using Talkies library
		-- Show "Press any mouse key to skip" message after 1 second
		if timer2 > 0 and timer2 < 0.5 and inputtime == 0 then
			love.graphics.print("Press any key to skip", 800, 900, 0, 0.8, 1)
		end
		-- Show "press 'R' to return" message in 720p resolution
		if res == "720p" then
			love.graphics.print("press 'R' to return", 100, 100)
		end
	
	--[[
	local hovered = isButtonHovered (briefcaseForwardButton)
	drawButton (briefcaseForwardButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  briefcasespeed=232
	else
	briefcasespeed=32
	end
	--]]
	
	--[[local hovered = isButtonHovered (briefcaseReturnButton)
	drawButton (briefcaseReturnButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  	love.audio.stop() music:stop()
				mousestate = not love.mouse.isVisible()	-- show mouse pointer
				love.mouse.setVisible(mousestate)
		  if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			
				if musicison==true then love.audio.stop() music:stop() 
						if musictype=="new" then 
							UnderwaterAmbientPadisaiah658 = love.audio.newSource( "externalassets/music/isaiah658/Underwater-Ambient-Pad-isaiah658loop.ogg","stream" )
							UnderwaterAmbientPadisaiah658:setVolume(0.3)
							love.audio.play( UnderwaterAmbientPadisaiah658 ) 
					elseif musictype=="classic" then 
							kufrik = love.audio.newSource( "externalassets/music/classic/kufrik.ogg","stream" )
							kufrik:setVolume(0.3)
							love.audio.play( kufrik )
					end
											
							diskexplosion = love.audio.newSource( "/externalassets/sounds/level2/diskexplosion.ogg","stream" )
							diskexplosion:setEffect('myEffect')
							diskexplosion:play()
	
							if talkies==true then Talkies.clearMessages() Obey.lev2thedisk() end
						elseif musicison==false then love.audio.stop() music:stop()
						end
					
						if language=="chi" then
							Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32)
							smallfisht="小鱼" bigfisht="大鱼" 
						end
					
					if skin=="remake" then 
						shader2=true
						assignfrgbck()
				elseif skin=="classic" then
						shader2=true
						assignfrgbckclassic()
				end
				timer=0
				stepdone=0
				gamestatus="game"
		end
	--]]
end



--[[The function skipbriefcasevideo(dt) is used to skip the briefcase video and start the game.
The code stops the audio and music that might be playing, shows or hides the mouse cursor, plays a sound effect if sound is on, sets the language and assigns the shader depending on the skin chosen.
It also initializes some variables to their default values, sets the timer and step count to zero and sets the game status to "game".
If autoload is false, the function sets some variables and loads the first level. Here is a breakdown of the code:
--]]
function skipbriefcasevideo(dt)
			
		--This stops any audio or music that might be playing.
	  	love.audio.stop() music:stop()
	  	--This checks if the variable autoload is true.
	  	if autoload==true then
				--This toggles the visibility of the mouse cursor.
				mousestate = not love.mouse.isVisible()	-- show mouse pointer
				love.mouse.setVisible(mousestate)
		  --This plays a sound effect if sound is on.
		  if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		
			--This code stops the music that might be playing and sets the music according to the musictype variable.
			--If musicison is true, it plays the music and sets the volume to 0.3. It also plays a sound effect called "diskexplosion" and sets an effect called "myEffect" to it.
				if musicison==true then love.audio.stop() music:stop() 
						if musictype=="new" then 
							UnderwaterAmbientPadisaiah658 = love.audio.newSource( "externalassets/music/isaiah658/Underwater-Ambient-Pad-isaiah658loop.ogg","stream" )
							UnderwaterAmbientPadisaiah658:setVolume(0.3)
							love.audio.play( UnderwaterAmbientPadisaiah658 ) 
					elseif musictype=="classic" then 
							kufrik = love.audio.newSource( "externalassets/music/classic/kufrik.ogg","stream" )
							kufrik:setVolume(0.3)
							love.audio.play( kufrik )
					end
											
							diskexplosion = love.audio.newSource( "/externalassets/sounds/level2/diskexplosion.ogg","stream" )
							diskexplosion:setEffect('myEffect')
							diskexplosion:play()
	
							
				elseif musicison==false then love.audio.stop() music:stop()
				end
					
					--This sets the font and language to Chinese and assigns the text to the smallfisht and bigfisht variables.
						if language=="chi" then
							Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32)
							smallfisht="小鱼" bigfisht="大鱼" 
					elseif language=="jp" then
							Talkies.font = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 32)
					elseif language=="gr" then
							Talkies.font = love.graphics.newFont("externalassets/fonts/pecita/Pecita.otf", 32)
					elseif language=="thai" then
							Talkies.font = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 32)
					end
				
				--This assigns the shader depending on the skin chosen.	
					if skin=="remake" then 
						shader2=true
						assignfrgbck()
				elseif skin=="classic" then
						shader2=true
						assignfrgbckclassic()
				end
				--This initializes some variables to their default values and sets the game status to "game".
				goodmorningsaidonce=true
				timer=0
				stepdone=0
				gamestatus="game"
				
		
		--This checks if the variable autoload is false.
		elseif autoload==false then
			--This sets some variables to their default values.
			shader1=false shader2=false autoload=true inputtime=0 briefcaseclosed=true
			lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
			music:play()
			nLevel=1
			loadlevelvariables()
			levelload()
			mousestate = not love.mouse.isVisible()	-- show mouse pointer
			love.mouse.setVisible(mousestate)
			gamestatus="levelselection" 

		end
end
