-- If the gamestatus is "briefcasemessage", update briefcase and Talkies, and handle screenshake and framerate limit
function updateBriefcaseMessage(dt)
	updategetjoystickaxis()	--get gamepad axis positions
	briefcaseupdate(dt)
	updateButtonFocus(dt)
	Talkies.update(dt)
	if screenshake==true then
		if timer2>0.5 then screenshake=false end
	end
	-- Framerate limit
	if limitframerate==30 then if dt < 1/15 then love.timer.sleep(1/17 - dt) end
	elseif limitframerate==60 then if dt < 1/30 then love.timer.sleep(1/32 - dt) end
	end
	timer = timer + dt
	timer2=timer2 +dt
	subtitle2FDTOmessage()	
end

--The briefcaseupdate(dt) function updates the position of a briefcase sprite by subtracting the product of the briefcasespeed and dt from its current position. It also checks if the mouse buttons are pressed, and if so, calls the skipbriefcasevideo() function.
function briefcaseupdate(dt)
	briefcaseposition = briefcaseposition - (briefcasespeed * dt)  -- x will increase by 32 for every second right is held down
	if love.mouse.isDown(0) or love.mouse.isDown(1) or love.mouse.isDown(2) then 
		skipbriefcasevideo()
	end
end
