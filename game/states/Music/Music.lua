
function musicload()

trackspositionx=100
trackspositiony=200
bcspace=40
author="none"
currenttrackname=""
currenttrackdesc=""

require ("game/states/Music/Buttons/MusicArtist")
require ("game/states/Music/Buttons/MusicTracks")
require ("game/states/Music/Buttons/TrackTitles")

-- Load music equalizer
musicequalizer = require ('game/musicequalizer')
musicequalizerload()

--controls

locksymbol = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("assets/interface/locki.webp")))

	require ("lib/equalizer/equalizer")
	require ("lib/equalizer/luafft")
	require ("lib/equalizer/complex")
	--equalizerload()
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger


cdspeed = 32
cdplaying=false
cdangle = 0


cdasset=love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/music/player/cdasset.webp")))
covercd=love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/music/player/covercd.webp")))
--covercd=love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/music/player/bckpoeticheart1.jpeg")))




--[[The first button is a "Return" button, with the text "Return" displayed on it. It is located at coordinates (1000, 100) on the screen and has a rotation value of 0 degrees. Its size is not explicitly defined, but its sx (scale x) value is set to 1. It has not been hovered over by the mouse, so its "hovered" attribute is set to false. Its default color is white, with RGB values of (1, 1, 1), and its color when hovered over is yellowish-green, with RGB values of (1, 1, 0). It uses a font called "poorfishmiddle".

Based on the information provided in the descriptions, here is what I believe the purpose of each button to be:

    ReturnButton: This button likely serves as a way for the user to go back to a previous screen or menu.
    stopbutton: This button likely serves as a way for the user to stop a process or action.
    synthwave421kb: This button likely represents a track or audio option called "Calm Relax 1 (Synthwave_421k)" that the user can select to listen to.
    synthwave4kb: This button likely represents a track or audio option called "Calm Ambient 1 (Synthwave 4k)" that the user can select to listen to.
    aquariab: This button likely represents a track or audio option called "Aquaria" that the user can select to listen to.
--]]
musicReturnButton = {
	text = "Return",
	x = 1600,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

	AboutMusicPlayerButton = {
	text = "",
	textPosition ="top",
	x = 1600,
	y = 200, 
    sx = 1,
    image = AboutIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
		AboutMusicHyperlinkButton = {
		text = "Standalone app's Repository",
		x = 800, 
		y = 900,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}

		AboutMusicCloseButton = {
		text = "Close Window",
		x = 1400, 
		y = 360,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishsmall,
	}
	
musicPlayButton = {
	text = "Play",
	x = 1200,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicstopbutton = {
	text = "stop",
	x = 1200,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicbackwardbutton = {
	text = "<<",
	x = 1100,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicforwardbutton = {
	text = ">>",
	x = 1350,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



end
