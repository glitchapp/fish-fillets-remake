
--The musicupdate(dt) function is used to update the state of the music player.
--If cdplaying is true, it will rotate the CD by decreasing the cdangle value based on cdspeed multiplied by the dt parameter which represents the time elapsed since the last update. If cdangle becomes greater than 360, it is reset to 0.
function musicupdate(dt)
		--[[
		if cdplaying==true then
			--timer = timer + dt
			creditsupdate(dt)
			creditsendcutsceneupdate(dt)			-- This is new in this version of the credits and will update animations behind the credits text
			cdangle = cdangle - (cdspeed * dt)  -- rotate cd
		
			--equalizerupdate(dt)
			if cdangle>360 then cdangle=0 end
		end
		--]]
		volume = love.audio.getVolume(music)
end

  -- If the game status is "music", update music, equalizer and FPS metrics
  --If the game is in the benchmark screen or the music screen, the benchmark or music is updated and the FPS is limited.
  function updateMusic(dt)
    updategetjoystickaxis()
        updateRadialMenu()
        musicupdate(dt)		
        --updateButtonFocus(dt)	--sparkles animations
        if cdplaying == true and gamestatus=="music" then 
            equalizerupdate(dt) 
        end
        updatefpsmetrics(dt)			

        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end
  end
