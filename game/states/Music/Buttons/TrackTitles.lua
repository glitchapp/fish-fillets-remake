pixelspheret=[[Alex Smith]]
pixelspheret2=[[pixelsphere.org]]
pixelspheret3=[[https://cynicmusic.com]]

pixelenchanted=[[enchanted_tiki_86]]
pixelenchanted2=[[https://opengameart.org/content/calm-ambient-3-Calm Ambient 3]]
pixelaquaria=[[Aquaria]]
pixelaquaria2=[[https://opengameart.org/content/aquaria]]
pixelsirens=[[Sirens in darkness]]
pixelsirens2=[[https://opengameart.org/content/sirens-in-darkness]]
pixelsong18=[[Crystal cave song]]
pixelsong182=[[https://opengameart.org/content/crystal-cave-song18]]
pixelsong21=[[Mysterious ambient song]]
pixelsong212=[[https://opengameart.org/content/mysterious-ambience-song21]]
pixelvapor=[[Calm Piano 1 (Vaporware)]]
pixelvapor2=[[https://opengameart.org/content/calm-piano-1-vaporware]]
pixelcalmrelax=[[Calm Relax 1 (Synthwave_421k)]]
pixelcalmrelax2=[[https://opengameart.org/content/calm-relax-1-synthwave-421k]]
pixelcalmambientI=[[Calm Ambient 1 (Synthwave 4k]]
pixelcalmambientI2=[[https://opengameart.org/content/calm-relax-1-Calm Relax 1 (Synthwave]]
pixelcalmambientII=[[Calm Ambient 2 (Synthwave 15k) ]]
pixelcalmambientII2=[[https://opengameart.org/content/calm-ambient-2-synthwave-15k]]
pixelcalmambientIII=[[Calm Ambient 3 (Lifewave 2k)]]
pixelcalmambientIII2=[[https://opengameart.org/content/calm-ambient-3-lifewave-2k]]
pixelanotheraugust=[[Another August ]]
pixelanotheraugust2=[[https://opengameart.org/content/another-august#comment-form]]
pixelsevenandeight=[[Icy Realm]]
pixelsevenandeight2=[[https://opengameart.org/content/icy-realm-seven-and-eight#comment-form]]
pixelnovembersnow=[[November Snow]]
pixelnovembersnow2=[[https://opengameart.org/content/november-snow#comment-form]]
pixelHappyLullaby=[[Happy Lullaby]]
pixelHappyLullaby2=[[https://opengameart.org/content/happy-lullaby-song17]]
pixelTheHex=[[The Hex]]
pixelTheHex2=[[https://opengameart.org/content/the-hex#comment-form]]

isaiah658t=[[isaiah658]]
isaiah658t2=[[https://opengameart.org/users/isaiah658]]
isaiahambient=[[Ambient Relaxing Loop]]
isaiahambient2=[[https://opengameart.org/content/ambient-relaxing-loop]]
isaiahunderwater=[[Underwater ambient pad]]
isaiahunderwater2=[[https://opengameart.org/content/underwater-ambient-pad]]

umplixt=[[Umplix]]
umplixt2=[[https://opengameart.org/users/umplix]]
umplixdeepsea=[[Deep sea]]
umplixdeepsea2=[[https://opengameart.org/content/deep-sea]]
umplixsinkingfeeling=[[Sinking feeling]]
umplixsinkingfeeling2=[[https://opengameart.org/content/sinking-feeling]]

ericmatyast=[[Eric Matyas]]
ericmatyast2=[[http://soundimage.org]]
ericmatyast3=[[https://opengameart.org/users/eric-matyas]]
ericmatyashyp=[[Hypnotic Puzzle (Looping)]]
ericmatyashyp2=[[http://soundimage.org/looping-music/]]
ericmatyasdream=[[Dreaming of Reefs]]
ericmatyasdream2=[[http://soundimage.org/looping-music/]]

ericmatyastislandofmysteries=[[Island of mysteries]]
ericmatyastislandofmysteries2=[[https://opengameart.org/content/island-of-mysteries-looping]]

ericmatyastMonkeyIslandBand=[[Monkey island band]]
ericmatyastMonkeyIslandBand2=[[https://opengameart.org/content/monkey-island-band]]

ericmatyastPuzzleGame=[[Puzzle game]]
ericmatyastPuzzleGame2=[[https://opengameart.org/content/puzzle-game-2]]

ericmatyastTheyarehere=[[They are here]]
ericmatyastTheyarehere2=[[https://opengameart.org/content/theyre-here]]

cleytonkauffmant=[[Cleyton Kauffman]]
cleytonkauffmant2=[[https://soundcloud.com/cleytonkauffman]]
cleytonkauffmant3=[[https://opengameart.org/users/cleytonkauffman]]
cleytonkauffmanunderwaterI=[[Underwater theme I]]
cleytonkauffmanunderwaterI2=[[https://opengameart.org/content/underwater-theme]]
cleytonkauffmanunderwaterII=[[Underwater theme II]]
cleytonkauffmanunderwaterII2=[[https://opengameart.org/content/underwater-theme-ii]]

marcelofgt=[[Marcelo Fernandez]]
marcelofgt2=[[soundcloud.com/marcelofernandezmusic]]
marcelofgt3=[[www.marcelofernandezmusic.com]]
marcelofgairy=[[Airy ambience]]
marcelofgairy2=[[https://opengameart.org/content/airy-ambience]]

springyspringot=[[SpringySpringo]]
springyspringot2=[[https://opengameart.org/users/springyspringo]]
springywater=[[Water ambience]]
springywater2=[[https://opengameart.org/content/water-ambience]]

tokygeishat=[[TokyoGeisha]]
tokygeishat2=[[https://opengameart.org/users/tokyogeisha]]
tokygeishaambience=[[Dream Ambience]]
tokygeishaambience2=[[https://opengameart.org/content/dream-ambience]]
tokygeishaambienceII=[[Dream Ambience II]]
tokygeishaambienceII2=[[https://opengameart.org/content/dream-2-ambience]]
tokygeishacreep=[[Creepy]]
tokygeishacreep2=[[https://opengameart.org/content/creepy]]

poinlt=[[poinl]]
poinlt2=[[https://opengameart.org/users/poinl]]
poinlnautilus=[[Nautilus]]
poinlnautilus2=[[https://opengameart.org/content/nautilus]]

hectavext=[[hectavex]]
hectavext2=[[https://opengameart.org/users/hectavex]]
hectavext3=[[http://glassocean.net]]
hectavexrupture=[[Rupture]]
hectavexrupture2=[[https://opengameart.org/content/rupture]]
hectavexrupturechorus=[[Rupture]]
hectavexrupturechorus2=[[https://opengameart.org/content/rupture (Light chorus remix by Glitchapp)]]
hectavexova=[[Ova]]
hectavexova2=[[https://opengameart.org/content/ova]]
hectavexvanish=[[Vanish]]
hectavexvanish2=[[https://opengameart.org/content/vanish]]

tedkerrt=[[Tedd Kerr]]
tedkerrt2=[[https://opengameart.org/users/wolfgang]]
tedkerrt3=[[]]
tedkerrcrashedship=[[Sci-Fi Ambient - Crashed Ship]]
tedkerrcrashedship2=[[https://opengameart.org/content/sci-fi-ambient-crashed-ship]]
tedkerrscifi=[[Space / Scifi Ambient]]
tedkerrscifi2=[[https://opengameart.org/content/space-scifi-ambient]]

isaot=[[SOUND AIRYLUVS]]
isaot2=[[]]
isaot3=[[https://airyluvs.com/]]
isaoupbeat=[[Upbeat Refreshing Commercial]]
isaoupbeat2=[[https://opengameart.org/content/upbeat-refreshing-commercial]]
isaoepicdance=[[Epic Melodic Electronic Dance Music]]
isaoepicdance2=[[https://opengameart.org/content/epic-melodic-electronic-dance-music]]

viktorkraus=[[Viktor Kraus]]
viktorkraus2=[[https://opengameart.org/users/viktor-kraus]]
viktorkraus3=[[]]
arobotswaytoheaven=[[A robot's way to heaven]]
arobotswaytoheaven2=[[https://opengameart.org/content/a-robots-way-to-heaven]]

glitchappt=[[Glitchapp]]
glitchappt2=[[https://glitchapp.codeberg.page/]]
glitchappt3=[[]]
hexhappylullaby=[[
Tracks by the Cynic Project
cynicmusic.com / pixelsphere.org / Alex Smith & Steven Ruud:

- Happy lullaby by The Cynic Project
  cynicmusic.com / pixelsphere.org 
  https://opengameart.org/content/happy-lullaby-song17



- The hex by Steven Ruud (https://opengameart.org/content/the-hex)
]]
hexhappylullaby2=[[Remix by glitchapp]]
glitchappremix1=[[
Tracks by the Cynic Project 
cynicmusic.com / pixelsphere.org / Alex Smith:

- Calm Piano 1 (Vaporware) by cynicmusic
	https://opengameart.org/content/calm-piano-1-vaporware
- Enchanted_tiki by cynicmusic
	https://opengameart.org/content/enchanted-tiki-86

Dream ambience by Tokyo Geisha
	https://opengameart.org/content/dream-ambience

Ambient Loop by isaiah658 
	https://opengameart.org/content/underwater-ambient-pad
	
Rupture by Hectavex:
https://opengameart.org/content/rupture
]]
glitchappremix1_2=[[Remix1 by glitchapp]]

glitchappremix2=[[
Tracks by the Cynic Project 
cynicmusic.com / pixelsphere.org / Alex Smith & Steven Ruud:

- Calm Piano 1 (Vaporware) by cynicmusic
	https://opengameart.org/content/calm-piano-1-vaporware
- Enchanted_tiki by cynicmusic
	https://opengameart.org/content/enchanted-tiki-86


- The hex by Steven Ruud
	https://opengameart.org/content/the-hex

Dream ambience by Tokyo Geisha
	https://opengameart.org/content/dream-ambience

Ambient Loop by isaiah658 
	https://opengameart.org/content/underwater-ambient-pad

A robots way to heaven by Viktor Kraus 
	https://opengameart.org/content/a-robots-way-to-heaven
]]
glitchappremix2_2=[[Remix by glitchapp]]
glitchappremix35=[[
- Epic Melodic Electronic Dance Music by ISAo
	https://opengameart.org/content/epic-melodic-electronic-dance-music
	https://www.patreon.com/airyluvs 
	https://airyluvs.com/
	https://www.youtube.com/c/ISAoGameMusic




- The hex by Steven Ruud
	https://opengameart.org/content/the-hex
	https://www.pixelsphere.org
]]
glitchappremix35_2=[[Remix by glitchapp]]
glitchapprupturem1=[[
-Rupture by hectavex
	https://opengameart.org/content/rupture - http://glassocean.net.
]]
glitchapprupturem1_2=[[Remix by glitchapp]]


glitchappremix4_1=[[Tracks by Hectavex and poinl]]
glitchappremix4_2=[[

Nautilus by poinl                                    
https://opengameart.org/content/nautilus

Vanish by hectavex
https://opengameart.org/content/vanish

]]

glitchappaquariaremix=[[
Tracks by the Cynic Project 
cynicmusic.com / pixelsphere.org / Alex Smith:
- Aquaria
https://opengameart.org/content/aquaria

- The hex by Steven Ruud
	https://opengameart.org/content/the-hex
	https://www.pixelsphere.org
]]

glitchappaquariaremix2=[[Remix1 by glitchapp]]
glitchappremix1_2=[[Remix1 by glitchapp]]

classictrackst=[[Classic tracks]]
classictrackst2=[[]]
classictrackst3=[[]]

kufriksongt=[[Kufrik]]
kufriksongt2=[[]]

classicmenusongt=[[]]
classicmenusongt2=[[]]

rybky1t=[[]]
rybky1t2=[[]]
rybky2t=[[]]
rybky2t2=[[]]
rybky3t=[[]]
rybky3t2=[[]]
rybky4t=[[]]
rybky4t2=[[]]
rybky5t=[[]]
rybky5t2=[[]]
rybky6t=[[]]
rybky6t2=[[]]
rybky7t=[[]]
rybky7t2=[[]]
rybky8t=[[]]
rybky8t2=[[]]
rybky9t=[[]]
rybky9t2=[[]]
rybky10t=[[]]
rybky10t2=[[]]
rybky11t=[[]]
rybky11t2=[[]]
rybky12t=[[]]
rybky12t2=[[]]
rybky13t=[[]]
rybky13t2=[[]]
rybky14t=[[]]
rybky14t2=[[]]
rybky15t=[[]]
rybky15t2=[[]]
