anotheraugustb = {
	text = "Another August ",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

aquariab = {
	text = "Aquaria",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

synthwave421kb = {
	text = "Calm Relax 1 (Synthwave_421k)",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

synthwave4kb = {
	text = "Calm Ambient 1 (Synthwave 4k)",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



synthwave15kb = {
	text = "Calm Ambient 2 (Synthwave 15k)",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

lifewave2kb = {
	text = "Calm Ambient 3 (Lifewave 2k)",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

vaporwareb = {
	text = "Calm piano 1 (Vaporware)",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


song18b = {
	text = "Crystal cave song",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

enchantedtikib = {
	text = "enchanted_tiki_86",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

happylullabyb = {
	text = "Happy Lullaby",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


icyrealmb = {
	text = "Icy Realm",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


song21b = {
	text = "Mysterious ambience song",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*12,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

sirensindarknessb = {
	text = "Sirens in darkness",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

thehexb = {
	text = "The Hex",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*14,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

novembersnowb = {
	text = "November Snow",
x = trackspositionx*10,
	y = trackspositiony+bcspace*15,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}






kufriksongb = {
	text = "Kufrik",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

classicmenusongb = {
	text = "Menu",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky1b = {
	text = "rybky1",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky2b = {
	text = "rybky2",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky3b = {
	text = "rybky3",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky4b = {
	text = "rybky4",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky5b = {
	text = "rybky5",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky6b = {
	text = "rybky6",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky7b = {
	text = "rybky7",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky8b = {
	text = "rybky8",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky9b = {
	text = "rybky9",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky10b = {
	text = "rybky10",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky11b = {
	text = "rybky11",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*12,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


rybky12b = {
	text = "rybky12",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky13b = {
	text = "rybky13",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*14,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky14b = {
	text = "rybky14",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*15,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rybky15b = {
	text = "rybky15",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*16,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



ambientrelaxingloopb = {
	text = "Ambient_relaxing loop",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

underwaterambientpadb = {
	text = "Underwater ambient pad",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


deepseab = {
	text = "Deep Sea",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

sinkingfeelingb = {
	text = "Sinking feeling",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



hypnoticpuzzleb = {
	text = "Hypnotic puzzle",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

dreamingofreefsb = {
	text = "Dreaming of Reefs",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

islandofmysteriesb = {
	text = "Island of mysteries",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

monkeyislandbandb = {
	text = "Monkey island Band",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

puzzlegame2b = {
	text = "Puzzle game 2",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

theyarehereb = {
	text = "They are here",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



underwaterthemeIb = {
	text = "Underwater theme I",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

underwaterthemeIIb = {
	text = "Underwater theme II",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



airyambienceb = {
	text = "Airy ambience",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



waterambienceb = {
	text = "Water ambience",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



dreamambienceb = {
	text = "Dream ambience",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

dreamambience2b = {
	text = "Dream ambience II",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

creepb = {
	text = "Creepy",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



nautilusb = {
	text = "Ambient 2 (Nautilus)",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}





hectavexrupture = {
	text = "Rupture",
	x = trackspositionx*10,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hectavexrupturechorus = {
	text = "Rupture with chorus",
	x = trackspositionx+bcspace*4,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hectavexova = {
	text = "Ova",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hectavexvanish = {
	text = "Vanish",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



tedkerrcrashedship = {
	text = "Crashed Ship",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}
tedkerrscifib = {
	text = "Space / Scifi Ambient",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


isaoupbeatb = {
	text = "Upbeat Refreshing Commercial",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}
isaoepicb = {
	text = "Epic Melodic Electronic Dance Music",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



arobotswaytoheavenb = {
	text = "A robot's way to heaven",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hexhappylullabyb = {
	text = "hex & happy lullaby",
	x = trackspositionx*10,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

remix1b = {
	text = "Remix 1",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


remix2b = {
	text = "Remix 2",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

remix35b = {
	text = "Remix 35",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

rupturemb = {
	text = "Rupture",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

remix4b = {
	text = "Remix 4",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

tylermixb = {
	text = "Aquaria remix",
	x = trackspositionx*10,
	y = trackspositiony+bcspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}
