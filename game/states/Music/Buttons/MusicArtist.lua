
--authors

classictracksb = {
	text = "Classic tracks",
	x = trackspositionx,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


cleytonkauffmanb = {
	text = "Cleyton Kauffman",
	x = trackspositionx,
	y = trackspositiony+bcspace,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

ericmatyasb = {
	text = "Eric Matyas",
	x = trackspositionx,
	y = trackspositiony+bcspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

hectavexb = {
	text = "hectavex",
	x = trackspositionx,
	y = trackspositiony+bcspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

isaiah658b = {
	text = "isaiah658",
	x = trackspositionx,
	y = trackspositiony+bcspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

isaob = {
	text = "Isao",
	x = trackspositionx,
	y = trackspositiony+bcspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

marcelofgb = {
	text = "marcelofg55",
	x = trackspositionx,
	y = trackspositiony+bcspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

glitchappb = {
	text = "Mixes",
	x = trackspositionx,
	y = trackspositiony+bcspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

pixelsphereb = {
	text = "PIXELSPHERE by Alex Smith",
	x = trackspositionx,
	y = trackspositiony+bcspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


poinlb = {
	text = "Poinl",
	x = trackspositionx,
	y = trackspositiony+bcspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

springyspringob = {
	text = "SpringySpringo",
	x = trackspositionx,
	y = trackspositiony+bcspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

tedkerrb = {
	text = "Ted Kerr",
	x = trackspositionx,
	y = trackspositiony+bcspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

umplixb = {
	text = "umplix",
	x = trackspositionx,
	y = trackspositiony+bcspace*12,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

tokyogeishab = {
	text = "TokyoGeisha",
	x = trackspositionx,
	y = trackspositiony+bcspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

viktorkrausb = {
	text = "Viktor Kraus",
	x = trackspositionx,
	y = trackspositiony+bcspace*14,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}
