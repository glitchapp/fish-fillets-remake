function confirmexitbutton()
-- confirm save
	if exitbuttonpressed==true and exitconfirmpressed==false then
		local hovered = isButtonHovered (confirmexitButton)
		drawButton (confirmexitButton, hovered,confirmexittext)
		love.graphics.print ("Do you really want to close the game?", 200,400)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				exitconfirmpressed=true timer2=0 inputtime=0
				love.timer.sleep( 0.1 )	
			end
	end
	-- reject exit
	local hovered = isButtonHovered (rejectexitButton)
	drawButton (rejectexitButton, hovered,rejectexittext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		exitbuttonpressed=false timer2=0 inputtime=0
		exitconfirmpressed=false
		if joystick then bfocus="exit" end
		love.timer.sleep( 0.5 )	
	end

end
