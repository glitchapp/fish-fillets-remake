--[[The code defines two functions: drawloadmessage() and drawsavemessage().

The drawloadmessage() function is used to draw a message on the screen if the loadbuttonpressed variable is true. The message will display "Settings loaded" at position (500,500) on the screen with a font size of 1. The timer2 variable is used to reset loadbuttonpressed after 4 seconds.
--]]

function drawloadmessage()
	if loadbuttonpressed==true then
	love.graphics.print ("Settings loaded ", 500,500,0,1)
		if timer2>4 then 
			loadbuttonpressed=false 
		end
	end
end
--The drawsavemessage() function is used to draw a message on the screen if both savebuttonpressed and saveconfirmpressed variables are true. The message will display "Settings saved" at position (500,500) on the screen with a font size of 1. The timer2 variable is used to reset both savebuttonpressed and saveconfirmpressed after 4 seconds.
function drawsavemessage()
	if savebuttonpressed==true and saveconfirmpressed==true then
	love.graphics.print ("Settings saved ", 500,500,0,1)
		if timer2>4 then 
			savebuttonpressed=false 
			saveconfirmpressed=false
		end
	end
end
 
function exitmessage()
local playsoundonce=false
	if exitbuttonpressed==true and exitconfirmpressed==true then
	love.graphics.print ("shutting down in ".. (math.floor((timer2/2)-1)), 500,500,0,1)
	
		if timer2>1 and playsoundonce==false then underwatermovement2:play() playsoundonce=true end
		
		if timer2>2 then love.event.quit() end
	end
end
