
function drawlevelmenubck()	-- Function to draw the background for the level menu
	local tileWidth = 24 -- Define the amount of tiles in the x and y direction
	local tileHeight = 16
	
	--[[love.graphics.setCanvas(canvas)
	warpshader2_2:send("time", timer)
	love.graphics.setShader(warpshader2_2)
	--]]
	
	-- If skin is "remake" then draw the background based on the screen resolution
	
	if skin=="remake" then
			if res=="1080p" then love.graphics.draw(menumapremake,0,0,0,0.5)
		elseif res=="1440p" then love.graphics.draw(menumapremake,0,0,0,0.7)
		elseif res=="4k" then love.graphics.draw(menumapremake,0,0,0,1)
		else love.graphics.draw(menumapremake,0,0,0,0.5)
		end
	
	
	--[[love.graphics.setCanvas()
	love.graphics.setShader()
	--]]
		-- Draw the branches on top of the background
		love.graphics.draw(allbranch,0,0,0,0.52*scalefactor)
	end
	-- If skin is "classic"
	if skin=="classic" then
	-- If the skin is not upscaled, then draw the background
		if skinupscaled==false then
			love.graphics.draw(menumap,0,0,0,2.1)
			-- If skin is upscaled and the level 68 is not unlocked
		elseif skinupscaled==true then
		end
		-- Draw the upscaled background and branches of each unlocked zone
			if not (unlockedlevels[68]) then
				love.graphics.draw(menumapupscaled,0,0,0,0.52*scalefactor)
				love.graphics.draw(branch1house,0,0,0,0.52*scalefactor)
				
				if unlockedlevels[4] then love.graphics.draw(branch2ship,0,0,0,0.52*scalefactor) end
				if unlockedlevels[5] then love.graphics.draw(branch3city,0,0,0.52*scalefactor) end
				if unlockedlevels[12] then love.graphics.draw(branch5,0,0,0,0.52*scalefactor) end
				if unlockedlevels[22] then love.graphics.draw(branch4,0,0,0,0.52*scalefactor) end
				if unlockedlevels[7] then love.graphics.draw(branch67,0,0,0,0.52*scalefactor) end
				if unlockedlevels[58] then love.graphics.draw(branch67,0,0,0,0.52*scalefactor) end
			-- If all levels are unlocked, draw the complete background
			else love.graphics.draw(menumapupscaled_complete,0,0,0,0.52*scalefactor)
			end
		
	end

end
