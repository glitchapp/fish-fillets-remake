--[[The second function drawCenteredText draws a given text string centered in a specified rectangle. It takes in rectX and rectY as the top-left corner of the rectangle, rectWidth and rectHeight as the dimensions of the rectangle, and the text string to be drawn. It then calculates the width and height of the text using the current font, and prints the text at the center of the rectangle.--]]
local function drawCenteredText (rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end

--[[The third function drawCenteredSprite draws a given sprite centered on a specified tile. It takes in x and y as the tile coordinates, tileSize as the size of the tile, and sprite as the image to be drawn. It first calculates the dimensions of the sprite, and then scales it to fit within the tile size. Finally, it draws the sprite at the center of the tile coordinates.--]]
local function drawCenteredSprite (x, y, tileSize, sprite)
--	x and y it tiles, [1,1] is top left
	local w, h = sprite:getDimensions()
	local scale = tileSize/math.max (w, h)
	love.graphics.draw (sprite, (x-0.5)*tileSize, (y-0.5)*tileSize, 0, scale, scale, w/2, h/2)
end



--[[This function draws the levels as spheres on the screen, with different sizes depending on the current screen resolution, and displays the level number on top of each sphere. It also checks if the level is unlocked, and if so, it will be displayed with a green color.--]]

function drawlevelspheres(x,y)
	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	local width, height = love.graphics.getDimensions()

	-- to make the spheres adaptive to window size:
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)

	--This section defines the width and height of the screen, and sets the tileSize to the smaller of either the width divided by the amount of tiles, or the height divided by the amount of tiles. It also sets a maximum tileSize of 50.
	local mx, my = love.mouse.getPosition()
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1

	--This section gets the position of the mouse, and calculates which tile the mouse is currently hovering over.
	for i, spherelevel in ipairs (spheres) do
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y

			love.graphics.setColor(0,1,0) 
				if res=="720p"  then drawCenteredSprite ((x*1.04), (y*1.03), tileSize, sphere)
			elseif res=="1080p" then drawCenteredSprite (x, y, tileSize, sphere)
			elseif res=="1440p" then drawCenteredSprite ((x*1.34)-0.1, (y*1.34)-0.1, tileSize, sphere)
			elseif res=="4k" 	then drawCenteredSprite ((x*scalefactor/1.5)-0.2, (y*scalefactor/1.5)-0.2, tileSize*1.5, sphere)
			else drawCenteredSprite (x, y, tileSize, sphere)
			end
			love.graphics.setFont(poorfishsmall)
			love.graphics.setColor(0,0,0)
			drawCenteredText ((x-1)*tileSize*scalefactor, (y-1)*tileSize*scalefactor, tileSize, tileSize, level)
		end
	end
	--[[The section above loops through each sphere, checking if the corresponding level is unlocked. If it is, it sets the color to green, and draws the sphere centered on the screen with the size adjusted depending on the screen resolution. It also displays the level number in the center of the sphere.--]]
	


--[[This code appears to be checking whether the mouse is hovering over a particular sphere in a list of spheres. Here is a summary of the code with comments for each line:--]]

-- Declare a variable to hold the sphere that is currently being hovered over
	local sphereHovered

-- Loop through the list of spheres
	for i, spherelevel in ipairs (spheres) do
	-- Check if the level is unlocked
		if unlockedlevels[i]==true then
		-- Get the level and position of the current sphere
			local level = spherelevel.level
			local x1, y1 = spherelevel.x, spherelevel.y
			
			-- Scale the position of the sphere based on screen resolution
			if res=="1080p" then
				scalefactor=1
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
		elseif res=="1440p" then
				x1=x1*1.34
				y1=y1*1.34
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
				scalefactor=1.34
		elseif res=="4k"	then
				x1=x1*scalefactor-0.2
				y1=y1*scalefactor-0.2
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
		else
			scalefactor=1
		end
			--print(emulatedmousexscale)
			--print(emulatedmouseyscale)
			
			-- Check if the mouse is hovering over the current sphere
			if x1 > x-1 and x1< x+1 and y1 > y-1 and y1< y+1 and love.mouse.isVisible( ) then
			-- If the mouse is hovering over the sphere, set the sphereHovered variable and break out of the loop
				sphereHovered = spherelevel
				break
			end
				if x1 > emulatedmousexscale-1 and x1< emulatedmousexscale+1 and y1 > emulatedmouseyscale-1 and y1< emulatedmouseyscale+1 and axistouchedonce==true then
					
						-- If the gamepad emulated mouse is hovering over the sphere, set the sphereHovered variable and break out of the loop
							sphereHovered = spherelevel
					
				break
			end
		end
	end

-- If a sphere is currently being hovered over and the save button has not been pressed, draw the sphere with a yellow border
	if sphereHovered and savebuttonpressed==false and exitbuttonpressed==false and not (aboutInfo==true) then

		-- Set the color to yellow
		love.graphics.setColor(1,1,0)
		
		-- Get the level and position of the hovered sphere
		local level = sphereHovered.level
		local x1, y1 = sphereHovered.x, sphereHovered.y
		
				
		if isLogoLoaded==false then		-- this conditional cheks if the transition animation is loaded
			if level==8					-- if it is, it loads it when hovering the end of area levels
				or level==19
				or level==29
				or level==37 
				or level==44
				or level==51
				or level==58
				or level==64
				or level==70
				or level==79
			then
			
						if loadingTransitionAnimation==0 then	-- this condition will be checked in game/states/LevelDraw to draw a message
							loadingTransitionAnimation=1		-- indicating that the assets is being loaded
					elseif loadingTransitionAnimation==1 then
							if inputtime>0.01 then
								LoadLogoAnimation()
								loadingTransitionAnimation=2
							end
					end
					
			end
		end
		
		-- Scale the position and font based on screen resolution
		if res=="1440p" then
			x1=x1*1.34-0.1
			y1=y1*1.34-0.1
			--love.graphics.setFont(poorfishmiddle)
			drawCenteredSprite (x1, y1, tileSize, sphere)
	elseif res=="4k" then
			x1=x1*scalefactor-0.5
			y1=y1*scalefactor-0.5
			love.graphics.setFont(poorfish)
			--drawCenteredSprite (x1, y1, tileSize, sphere)
	end
		if vibration==true and axistouchedonce and level~=lastgamepadtouchedlevel then
			  if leftxaxis>0.2 or leftxaxis<-0.2 or  leftyaxis>0.2 or leftyaxis<-0.2 then
					joystickvibration = joystick:setVibration(0.05, 0.05,0.05)
					lastgamepadtouchedlevel=level
			end
		
		end
		
			--[[ This code draws an animated selection circle around a sphere level and displays information about the level.--]]
		
				love.graphics.setColor(0.5,0.5,0)	-- Set the color of the animated selection circle to grey.
				love.graphics.circle("line",(x1-0.5)*tileSize, (y1-0.5)*tileSize,20+subbeat*20)	-- Draw a circle with line style using the specified x, y, and radius.
		
		love.graphics.setColor(1,1,1)	-- Set the color of the following graphics to white.
		drawCenteredText ((x1-1)*tileSize, (y1-1)*tileSize, tileSize, tileSize, level)	-- Draw centered text at the specified x, y position with the specified width and height.
				
displaylevelinformation(level)

	
				
		--[[This code checks if the left mouse button is pressed and if the current level is unlocked. If so, it sets several variables to specific values, indicating that the loading process is starting. It then sets the current level to a variable nLevel. Depending on the value of shader1type, it enables and disables different shader effects using the Love2D graphics library.--]]
		if (love.mouse.isVisible( ) and love.mouse.isDown(1)) or joystick and joystick:isGamepadDown("a") and unlockedlevels[level]==true then	-- This line checks if the left mouse button is currently being pressed (love.mouse.isDown(1)) and if the current game level is unlocked (unlockedlevels[level]==true).
			--These lines reset various variables to their default values, preparing them for the loading process.
			
			Transitionload()

			timer2=0
			timer3=0
			isloading=true
			loadingprocess=true
			loadbuttonpressed=false
			savebuttonpressed=false
			levelcompletedsentencesaid=false
			bfocus=nil
				--This line sets the value of the nLevel variable to be equal to the current level.
				nLevel=level
				loadcurrenttrackname()	-- Load the current music track name (function on loading.lua)
				
				if selectedTile==0 then selectedTile=nil end
					--This line checks if the shader1type variable is set to "crt".
					if shader1type=="crt" then
						--standard crt
						--These lines enable and disable various post-processing effects provided by the effect library, depending on which shader is selected. In this case, it is disabling several effects and enabling scanlines, crt, glow, and filmgrain.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
						effect.enable("scanlines","crt","glow","filmgrain")
				elseif shader1type=="godsgrid" then	--This line checks if the shader1type variable is set to "godsgrid".
						--These lines disable all post-processing effects except for godsray, which is enabled.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("godsray")
				elseif shader1type=="gameboy" then	--This line checks if the shader1type variable is set to "gameboy".
						--gameboy
						--These lines disable all post-processing effects except for scanlines and dmg, which are enabled. It then sets various parameters for those effects to create a Game Boy-like look.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("scanlines","dmg")
						palette=3
						 effect.vignette.radius=1.2
						effect.vignette.opacity=3
						effect.dmg.palette=4
						effect.filmgrain.size=2.5
						effect.filmgrain.opacity=2
						effect.scanlines.width=1
						--effect.scanlines.thickness=0.5
				end			
		end
	end



DrawLevelSelectionButtons()
end

function prepareInstructions()

					effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.crt).chain(moonshine.effects.glow).chain(moonshine.effects.filmgrain)
					effect.scanlines.opacity=0.6
					effect.glow.min_luma = 0.2
					effect.filmgrain.size=1.5
					effect.filmgrain.opacity=2
					shader2=false gamestatus="instructions"
					love.graphics.setColor(1,1,1)
					love.timer.sleep( 0.3 )	

end
