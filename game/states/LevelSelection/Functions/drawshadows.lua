
-- this function is not being used due to performance issues (it heavily impact performance)
function drawshadows(x,y,newBody)
	-- change shadow light from green to yellow when mouse hover spheres
	if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end

  --shadows --0. Fish House levels 1-8 
    if x>10 and x<16 and y>0 and y<12 then
        for i=1, 8, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, i*2*1)
			CircleShadow:new(newBody, 0, 20+i*50*1, 16)
		end
			drawfishhousesphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
				--shadows 1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then
			for i=1, 11, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 110)
				CircleShadow:new(newBody, 300+i*50*1, 110, 16)
			end
			drawshipwreckssphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
			
		end
	
			     --shadows 2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then 
			for i=1, 7, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 260)
				CircleShadow:new(newBody, 300+i*50*1, 260, 16)
			end            
			drawsilvershipsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
	       --shadows 3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then 
			for i=1, 10, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 140)
				CircleShadow:new(newBody, 315-i*50*1, 140, 16)
			end
			drawcitydeepsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
		--shadows 	4. UFO	--level 52-58
	if y>8 and y<10 and x>4 and x<14 then
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(315-i*0.5, 210)
			CircleShadow:new(newBody, 315-i*50*1, 210, 16)
		end 
		drawufosphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
	    --shadows 5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 then 
			for i=1, 8, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawcoralreefsphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 then
			for i=1, 4, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawtreasurecavesphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 7. Dump--level 38-44
      if x>12 and x<14 and y>8 and y<18 then 
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, 200+i*2*1)
			CircleShadow:new(newBody, 0, 200+i*50*1, 16)
		end
		drawdumpsphere(x,y)

       else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
       end
	
		 --shadows 8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then
			for i=1, 6, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 160)
				CircleShadow:new(newBody, 300+i*50*1, 160, 16)
			end
			drawsecretcomputersphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
                
        end

end
