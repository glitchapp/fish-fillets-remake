--This code is responsible for drawing the level selection menu of the game. It first checks if either the load button or the save button have been pressed, and if so, applies a shader effect to the background before drawing the level spheres and displaying any relevant messages. If neither button has been pressed, it simply draws the level spheres and the background without the shader effect.

function leveldraw()

	if loadbuttonpressed or savebuttonpressed or exitbuttonpressed and not (aboutInfo==true) then
			love.graphics.setShader()
			effect(function()
				drawlevelmenubck(x,y)
				drawlevelspheres(x,y)
				
			end)
			drawloadmessage()
			drawsavemessage()
			confirmsavebuttons()
			
	elseif aboutInfo==true then
		love.graphics.setShader()
			effect(function()
				drawlevelmenubck(x,y)
				drawlevelspheres(x,y)
			end)
				drawAboutFFillets()
				if donateInfo==true then
					drawDonateInfo()
				end
	else
		drawlevelmenubck(x,y)
		drawlevelspheres(x,y)
	end
	if exitbuttonpressed then
		confirmexitbutton()
	end
	if exitconfirmpressed then
		exitmessage()
	end
	if loadingTransitionAnimation==1 then	-- This condition draws a message to warn that an assets is loading (check game/states/LevelSelection/Functions/DrawLevelSpheres)
			inputtime=0
			drawLoadingWarning()
	end
end
