
function drawButtonFocus()

		
	buttonfocusanimations()		-- focus color and size animations
		
	--if bfocus=="exit" or bfocus=="extras" or bfocus=="ends" or bfocus=="save" or bfocus=="reset" or bfocus=="music" or bfocus=="credits" or bfocus=="options" then	
	--end
	
	
	--level menu
		if bfocus=="exit" 	then love.graphics.rectangle ('line', exitButton.x, exitButton.y, fwidth, fheight)		--fwidth and fheight are the size variables used for the button focus animations
	elseif bfocus=="extras" then love.graphics.rectangle ('line', extrasButton.x, extrasButton.y, fwidth, fheight)
	elseif bfocus=="ends" 	then love.graphics.rectangle ('line', endButton.x, endButton.y, fwidth, fheight)
	elseif bfocus=="save" 	then love.graphics.rectangle ('line', saveButton.x, saveButton.y, fwidth, fheight)
	elseif bfocus=="reset" 	then love.graphics.rectangle ('line', resetsavegameButton.x, resetsavegameButton.y, fwidth, fheight)
	elseif bfocus=="music" 	then love.graphics.rectangle ('line', musicButton.x, musicButton.y, fwidth, fheight)
	elseif bfocus=="options" then love.graphics.rectangle ('line', optionsButton.x, optionsButton.y, fwidth, fheight)
	elseif bfocus=="credits" then love.graphics.rectangle ('line', creditsButton.x, creditsButton.y, fwidth, fheight)
	elseif bfocus=="rejectexit" then love.graphics.rectangle ('line', rejectexitButton.x, rejectexitButton.y, fwidth, fheight)
	elseif bfocus=="confirmexit" then love.graphics.rectangle ('line', confirmexitButton.x, confirmexitButton.y, fwidth, fheight)
	elseif bfocus=="confirmsave" then love.graphics.rectangle ('line', confirmsaveButton.x, confirmsaveButton.y, fwidth, fheight)
	elseif bfocus=="rejectsave" then love.graphics.rectangle ('line', rejectsaveButton.x, rejectsaveButton.y, fwidth, fheight)
	end
	
	-- After drawing all the button outlines, draw the particle sparkles
    --drawSparkleParticles()
	
end
