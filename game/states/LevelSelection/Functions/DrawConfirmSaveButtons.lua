--[[The confirmsavebuttons() function handles the confirmation process for saving a game.
If the save button has been pressed and the save confirmation button has not been pressed, it will display a message asking the user if they are sure they want to proceed with the save.
It will also display two buttons, one to confirm the save and another to reject it.
If the confirm button is hovered over and clicked, it will set the saveconfirmpressed variable to true and call the savemygame() function to save the game.
If the reject button is clicked, it will reset the savebuttonpressed and saveconfirmpressed variables.
The drawloadmessage() and drawsavemessage() functions are used to display messages to the user when loading or saving the game settings.
--]]
function confirmsavebuttons()
		-- confirm save
if savebuttonpressed==true and saveconfirmpressed==false then
	local hovered = isButtonHovered (confirmsaveButton)
	drawButton (confirmsaveButton, hovered,confirmsavetext)

love.graphics.print ("Saving will replace any previous saved data, are you sure to proceed?", 200,400)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		saveconfirmpressed=true timer2=0 inputtime=0
		if joystick then bfocus="exit" end
		
		savemygame()	-- on game/mainfunctions/savegame.lua
		love.timer.sleep( 0.5 )	
	end
	
	-- reject save
	local hovered = isButtonHovered (rejectsaveButton)
	drawButton (rejectsaveButton, hovered,rejectsavetext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		negativegui:play()
		savebuttonpressed=false timer2=0 inputtime=0
		saveconfirmpressed=false
		if joystick then bfocus="exit" end
		love.timer.sleep( 0.5 )	
	end
end	
end
