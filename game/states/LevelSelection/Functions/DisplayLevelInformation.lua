
function displaylevelinformation(level)
				-- Display level information based on the level number.
if level>1 and level <9 then
-- Display the name of the zone and the name of the fishhouse for the current level.
love.graphics.print (zonetext[1],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..fishhouse0[level],500*scalefactor, 900*scalefactor,0)
elseif level>8 and level <20 then
-- Display the name of the zone and the name of the shipwreck for the current level.
love.graphics.print (zonetext[2],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..shipwrecks1[level-8], 500*scalefactor, 900*scalefactor,0,1)
elseif level>44 and level <52 then
-- Display the name of the zone and the name of the silver ship for the current level.
love.graphics.print (zonetext[3],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..silversship2[level-44], 500*scalefactor, 900*scalefactor,0,1)
elseif level>19 and level <30 then
-- Display the name of the zone and the name of the city in the deep for the current level.
love.graphics.print (zonetext[4],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..cityinthedeep3[level-19], 500*scalefactor, 900*scalefactor,0,1)
elseif level>29 and level <38 then
-- Display the name of the zone and the name of the coral reef for the current level.
love.graphics.print (zonetext[6],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..coralreef5[level-29], 500*scalefactor, 900*scalefactor,0,1)
elseif level>37 and level <45 then
-- Display the name of the zone and the name of the dump for the current level.
love.graphics.print (zonetext[8],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..dump7[level-37], 500*scalefactor, 900*scalefactor,0,1)
elseif level>51 and level <59 	then
-- Display the name of the zone and the name of the ufo for the current level.
love.graphics.print (zonetext[5],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..ufo4[(level-51)], 500*scalefactor, 900*scalefactor,0,1)
elseif level>58 and level <66 	then
-- Display the name of the zone and the name of the treasurecave for the current level.
love.graphics.print (zonetext[7],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..treasurecave6[(level-58)], 500*scalefactor, 900*scalefactor,0,1) 
elseif level>64 and level <71 	then
-- Display the name of the zone and the name of the secretcomputer for the current level.
love.graphics.print (zonetext[9],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..secretcomputer8[level-64], 500*scalefactor, 900*scalefactor,0,1)
elseif level>70 and level <79 	then
-- Display the name of the zone and the name of the nextgeneration levels(secret) for the current level.
love.graphics.print (zonetext[10],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..secret9[level-70], 500*scalefactor, 900*scalefactor,0,1)
end
end		
