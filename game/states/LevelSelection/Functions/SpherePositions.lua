
--[[This code defines a Lua table called "spheres" which holds the positions and levels of different levels in a game. The levels are organized into several groups, with the first group consisting of levels 1-8, and the second group consisting of levels 9-19. For each level, the table includes a "level" property that specifies the level number and an "x" and "y" property that specify the position of the level on a map.--]]
spheres = {
-- 0. Fish house (8 levels)
	{level= 1, x=13.9, y=5.6},
	{level= 2, x=14.2, y=6.9},
	{level= 3, x=13.8, y=8.4},
	{level= 4, x=13, y=9.9},
	{level= 5, x=12.4, y=11},
	{level= 6, x=12.2, y=12.4},
	{level= 7, x=12.7, y=13.6},
	{level= 8, x=13.6, y=14.6},

-- 1. Ship Wrecks (9 - 19) (11 levels)
	{level= 9, x=14.7, y=10},
	{level=10, x=16.4, y=9.8},
	{level=11, x=18.1, y=9.3},
	{level=12, x=19.5, y=8.4},
	{level=13, x=20.6, y=7.1},
	{level=14, x=21,   y=5.5},
	{level=15, x=20.4, y=4},
	{level=16, x=19.1, y=3},
	{level=17, x=17.3, y=3.1},
	{level=18, x=16,  y=4.2},
	{level=19, x=16.8,  y=5.7},

-- 3. City in the deep (20-29)	 10 levels
	{level=20, x=10.8, y=10.3},
	{level=21, x=9.6, y=9.6},
	{level=22, x=8.5, y=8.5},
	{level=23, x=7.6, y=7.2},
	{level=24, x=7.3, y=5.7},
	{level=25, x= 7.8, y=3.9},
	{level=26, x= 9.2, y=2.7},
	{level=27, x= 10.7, y=2.4},
	{level=28, x= 11.6, y=3.7},
	{level=29, x= 11, y=5.3},
	
--5 Coral Reef	(30 - 37)		7 levels

	{level=30, x=14.2, y=12.8},
	{level=31, x=15.8, y=12.8},
	{level=32, x=17.2, y=13.6},
	{level=33, x=18, y=15},
	{level=34, x=17.7, y=16.5},
	{level=35, x=16.5, y=17.5},
	{level=36, x=15.1, y=17.5},
	{level=37, x=14.5, y=16.1},

-- 7. Dump (38-44) 				(7 levels)
		
	{level=38, x=12.6, y=15.9},
	{level=39, x=11.2, y= 17.1},
	{level=40, x=9.6, y= 17.8},
	{level=41, x=7.6, y= 17.8},
	{level=42, x=6.2, y= 16.5},
	{level=43, x=6.9, y= 14.7},
	{level=44, x=8.5, y= 15.2},

-- 2. Silver's ship (45-51) (7 levels)
	
	{level=45, x=19.9, y=10},
	{level=46, x=20.8, y=11.3},
	{level=47, x=22.2, y=11.9},
	{level=48, x=23.8, y=11.1},
	{level=49, x=24.3, y=9.3},
	{level=50, x=23.4, y=7.8},
	{level=51, x=21.7, y=8.9},

-- 4. UFO (52-58)	 (7 levels) (not 6)
	
	{level=52, x=7.4, y= 9.8},
	{level=53, x=6, y= 10.4},
	{level=54, x=4.4, y= 10.4},
	{level=55, x= 3.4, y= 9.4},
	{level=56, x= 3.7, y= 7.5},
	{level=57, x= 5.2, y= 7.2},
	{level=58, x= 5.5, y= 8.5},
-- 6. Treasure cave	 (59-64) (6 levels)

	
	{level=59, x=11.1, y=15.1},
	{level=60, x=10, y=13.7},
	{level=61, x=8.8, y=12.6},
	{level=62, x=7.3, y=12.1},
	{level=63, x=5.9, y=12.5},
	{level=64, x=4.6, y=13.6},


-- 8. Secret computer (65-70) (6 levels)

	{level=65, x=17.5, y=12.2},
	{level=66, x=18.9, y=12.7},
	{level=67, x=20, y=13.8},
	{level=68, x=21.1, y=14.7},
	{level=69, x=22.6, y=14.4},
	{level=70, x=23.7, y=13.7},

-- 9 Secret.			 (71-79) (9 levels)
	{level=71, x= 9, y=20},
	{level=72, x= 8, y=20},
	{level=73, x= 7, y=20},
	{level=74, x= 6, y=20},
	{level=75, x= 5, y=20},
	{level=76, x= 4, y=20},
	{level=77, x= 3, y=20},
	{level=78, x= 2, y=20},
	{level=79, x= 1, y=20},

}
