--[[This code defines a function called drawfishhousesphere which draws a selection circle around fish houses based on the music beat.--]]
	function drawfishhousesphere(x,y)	--Defines the function with two parameters x and y,     The function name implies it will draw a circle around the levels buttons.
				if y>1 and y<10 and x==13 then	--Checks if y is greater than 1 and less than 10 and if x is equal to 13.
												--This condition ensures that only the fish house area is targeted.
					love.graphics.setColor(0.5,0.5,0)	--Sets the color for the circle to dark gray.
					love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)	-- Draws a circle with a radius of 20 + subbeat * 20 at position (-25+x50, -25+y50). The subbeat variable is used to sync the circle to the music.
					love.graphics.setColor(1,1,1)										-- Sets the color back to white.
					love.graphics.print (zonetext[1],500, 850,0,1) 						-- Prints the text stored in zonetext[1] at position (500, 850). This line seems to display the name of the zone.
					love.graphics.print ((y-1) .. '.' ..fishhouse0[y-1],500, 900,0,1) 	-- Prints the fish house number and level at position (500, 900).
					--love.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
				end
	end
