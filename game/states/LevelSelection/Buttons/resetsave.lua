	resetsavegameButton = {
		text = resetsavegamet,
		x = 1600,
		y = 600, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}


function DrawResetSaveButton()
		-- reset saved game
	local hovered = isButtonHovered (resetsavegameButton)
	drawButton (resetsavegameButton, hovered,resetsavegamet)
	if hovered then love.graphics.print ("This option will delete all progress, achievements and settings", 200,900) end

	if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
	resetcolors()
		savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		loadachievements()
		savebuttonpressed=true saveconfirmpressed=false timer2=0
		love.timer.sleep( 0.5 )	
	end
end
