	loadButton = {
		text = loadtext,
		x = 1600,
		y = 400, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}


function DrawLoadButton()

	local hovered = isButtonHovered (loadButton)
	drawButton (loadButton, hovered,savetext)
	if hovered then love.graphics.print ("This option loads current progress, achievements and settings", 200,900) end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		loadmygame()
		LoadSound:play()
		love.timer.sleep( 0.5 )	
	end
end
