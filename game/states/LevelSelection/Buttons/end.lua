
	endButton = {
	text = endtext,
	--x = 1500, y = 300,
	x = 1600, y = 300,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1}, alpha=1,
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}


function DrawEndButton()
--This code checks if the endmenuunlocked flag is set to true. If it is true, it checks if the user is hovering over the endButton and if the left mouse button is clicked. If both conditions are true, it stops the current music, loads the "end menu" module and sets the game status to "endmenu".
if endmenuunlocked==true then	
	-- Ends

	hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  music:stop()
			   gui079:play()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
		end
	end
end
end
