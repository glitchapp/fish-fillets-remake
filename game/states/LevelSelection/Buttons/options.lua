	optionsButton = {
		text = optionstext,
		--x = 1500, y = 10, 
		--x = 1500*scalefactor, y = 1000*scalefactor, 
		x = 1600, y = desktopHeight / 1.2,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}


function DrawOptionsButton()

	-- options
	--This code is related to the "options" button in a game. It checks if the mouse is hovering over the button and if the left mouse button is clicked. If the button is clicked, it sets the palette to 1 if it is not already set to a valid value, loads the appropriate assets based on the selected skin, and sets the game status to "options".
	hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			
	if level1bck==nil then
		level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
		level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
		level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
	end

		  --prevent palette not to be assigned
		    if palette==1 or palette==2 or palette==3 or palette==4 or palette==5 or palette==6 or palette==7 then
		    else palette=1
		    end
		    if currentfrg==false then 
					if skin=="remake" then
						loadlevelassets()			-- load assets
						--loadlevelmusic()			-- load music
						assignfrgbck()				-- Assign layers
				elseif skin=="classic" then	
						loadlevelassetsclassic()	-- load classic assets
						--loadlevelmusicclassic()		-- load music
						assignfrgbckclassic()		-- Assign layers
				end
			end
		--loadlevelassets()
		gui076:play()
		optionsloadlanguages()
			bfocus="langarrowup"
			bfocustab="languages"
			gamestatus="options" 
		end
	end
	
end
