
function DrawLevelSelectionButtons()

------------------------------------------------
-- buttons
------------------------------------------------
	if not (PlayingFromLevelEditor==true) then
		DrawExitButton()
	elseif PlayingFromLevelEditor==true then	-- prevent game from crashing due to unresolved error with DrawExitButton() when called after level editor
		DrawExitMenuButton()
	end

	DrawLoadButton()

	DrawSaveButton()
		
	DrawResetSaveButton()
	
	DrawOptionsButton()

	drawCreditsButton()
	
	DrawMusicButton()

	DrawExtrasButton()

	DrawEndButton()

	DrawAboutButton()
				
	DrawHowToPlayButton()
			

end

