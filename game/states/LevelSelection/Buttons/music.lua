	musicButton = {
		text = musictext,
		--x = 1500, y = 100, 
		x = 100, y = 100, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}


function DrawMusicButton()

	--Music player
	--This code creates a music player button that is only visible if the musicplayerunlocked variable is true. When the button is hovered and clicked, it stops the current music and sets the gamestatus to "music", allowing the player to access the music player menu.
if musicplayerunlocked==true then
	hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			cdplaying = false
			music:stop()
			currenttrackname="none"
			--loadassetscscene()
			--loadcutscenes()
			gui078:play()
			love.timer.sleep( 0.5 )
			gamestatus="music"
		end
	end
end

end
