
	
	creditsButton = {
		text = creditstext,
		--x = 100, y = 1000*scalefactor, 
		x = 100, y = desktopHeight / 1.2,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

function drawCreditsButton()

--credits
	--This code handles the "Credits" button. It checks if the button is being hovered over and if the left mouse button is pressed. If so, it stops any currently playing music, loads a new audio source for the credits music, sets its volume, and changes the game status to "credits" to switch to the credits screen.
	hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			--gui073:play()
			inputtime=0
			love.timer.sleep( 0.2 )
			music:stop()
				--alternative end credits version
				--loadassetscscene()
				--loadcutscenes()
				--gamestatus="creditsend" 
				--music = love.audio.newSource( "/externalassets/music/mixes/TylerMix.ogg","static" )
				
				--Standard Credits version
			music = love.audio.newSource( "/externalassets/music/pixelsphere/003_Vaporwareloop.ogg","static" )
				love.audio.play( music )
				music:setVolume(0.4)
				gamestatus="credits" 
		end
	end
end
