extrasButton = {
	text = extrastext,
	--x = 1500, y = 200, 
	x = 1600, y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1}, alpha=1,
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}


function DrawExtrasButton()
--[[This is a block of code that displays an "Extras" button if a certain condition is met (i.e., the "extrasunlocked" variable is true). When the button is hovered over and clicked with the left mouse button, it stops the current music and sets the game status to "extras", presumably to access some sort of extra content in the game.--]]
	-- extras
if extrasunlocked==true then
	hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			 music:stop()
			 gui078:play()
			gamestatus="extras" 
			love.timer.sleep( 0.3 )
		end
	end
end	
end
