
	saveButton = {
		text = savetext,
		x = 1600,
		y = 500, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}


function DrawSaveButton()
--[[This code displays two buttons: one to save progress and settings, and one to reset the saved game. If the mouse is hovering over a button and left-clicked, the corresponding action is triggered. If the save button is clicked, progress and settings are saved, and a video effect is enabled for half a second. If the reset button is clicked, all progress, achievements, and settings are deleted, and the video effect is enabled. Then, the achievements are loaded, and the save button is pressed.--]]
	-- save
	local hovered = isButtonHovered (saveButton)
	drawButton (saveButton, hovered,savetext)
	if hovered then love.graphics.print ("This option saves current progress, achievements and settings", 200,900) end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		
		love.timer.sleep( 0.5 )	
	end
end
