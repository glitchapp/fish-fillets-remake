
exitMenuButton = {
		text = "Exit",
		--x = 1500, y = 700,
		x = 1600, y = 100,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

function DrawExitMenuButton()

	-- quit
	--This code checks if the exit button is being hovered over and then draws the button with the corresponding text. If the button is being hovered over and the left mouse button is pressed, the program waits for 0.5 seconds and then quits the game.
	local hovered = isButtonHovered (exitMenuButton)
		 drawButton (exitMenuButton, hovered,exittext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		exitbuttonpressed=true exitconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
			
			love.timer.sleep( 0.5 )	
		
	end

end
