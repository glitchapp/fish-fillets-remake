
function CalculateScaleFactor()
		if res=="1080p" then scalefactor=1
	elseif res=="1440p" then scalefactor=1.34
	elseif res=="4k" then scalefactor=2
	else scalefactor=1
	end
end

function levelload()
		reloadadjustedcolors()
		CalculateScaleFactor()
	
	-- initialize bfocus only if a gamepad is detected
	if joystick then bfocus="exit" else bfocus="none" end

	thumb=thumb1

	font1 = love.graphics.newFont(12) -- standard font
	font3 = love.graphics.newFont(12*3) -- three times bigger

	--levelmap= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/levelselection.webp")))
	sphere= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/sphere.webp")))

		if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
	elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
	elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends" loadtext="Load" savetext="Save"
	elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" poorfishmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" poorfishmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" poorfishmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
	end

confirmsavetext="yes"
confirmexittext="yes"
rejectsavetext="no"
rejectexittext="no"
cutscenest="cutscenes"
resetsavegamet="Reset"
exitbuttonpressed=false
exitconfirmpressed=false
gamepadselectanimation=false
screensaver=false
--emulatedmousex=0
--emulatedmousey=0
emulatedmousexscale=0
emulatedmouseyscale=0
PlayingFromLevelEditor=false
	fwidth=150
	fheight=50
	
	-- Define global variables for the pulsating effect and particle sparkles
	pulsateTimer = 0
	sparkleParticles = {}
	
	button = {}
	
	
	if bfocustab=="languages" then drawOptionsFocusLanguages() end
	if language=="jp" or language=="chi" or language=="thai" or language=="tamil" or language=="hindi" then love.graphics.setFont( poorfish )
	--else love.graphics.setFont( button.font )
	end

--[[This code defines variables for various buttons that will be used in a game or application interface.
 Each button has its own properties such as text, position, rotation, scaling, color, and font.
 The buttons include options for cutscenes, music, extras, credits, loading, saving, resetting save games,
 confirming saves, rejecting saves, exiting, and using a level editor.
 The code also requires a text file for the English version of a level menu.
--]]

--buttons

require("game/states/LevelSelection/Buttons/about")
require("game/states/LevelSelection/Buttons/confirmexit")
require("game/states/LevelSelection/Buttons/confirmsave")
require("game/states/LevelSelection/Buttons/credits")
require("game/states/LevelSelection/Buttons/end")
require("game/states/LevelSelection/Buttons/exit")
require("game/states/LevelSelection/Buttons/exitMenu")
require("game/states/LevelSelection/Buttons/extras")
require("game/states/LevelSelection/Buttons/howtoplay")
require("game/states/LevelSelection/Buttons/leditor")
require("game/states/LevelSelection/Buttons/load")
require("game/states/LevelSelection/Buttons/music")
require("game/states/LevelSelection/Buttons/options")
require("game/states/LevelSelection/Buttons/rejectexit")
require("game/states/LevelSelection/Buttons/resetsave")
require("game/states/LevelSelection/Buttons/rejectsave")
require("game/states/LevelSelection/Buttons/save")



require("game/states/LevelSelection/Functions/DisplayLevelInformation")
require("game/states/LevelSelection/Functions/DrawButton")
require("game/states/LevelSelection/Buttons/DrawLevelSelectionButtons")
require("game/states/LevelSelection/Functions/DrawButtonFocus")
require("game/states/LevelSelection/Functions/DrawConfirmExitButton")
require("game/states/LevelSelection/Functions/DrawConfirmSaveButtons")
require("game/states/LevelSelection/Functions/DrawFishHouseSphere")
require("game/states/LevelSelection/Functions/DrawLevelMenuBackground")
require("game/states/LevelSelection/Functions/DrawLevelSpheres")
require("game/states/LevelSelection/Functions/DrawMessages")
require("game/states/LevelSelection/Functions/LevelDraw")
require("game/states/LevelSelection/Functions/SpherePositions")

	require("assets/text/levelmenu/level-en")
	--loadlevelpreviews()
	

	
end



--buttons

--[[This code defines a local function named isButtonHovered that takes a table named button as an argument.
 The button table has properties such as text, font, sx, sy, x, and y which are used to calculate the position and size of the button.
 The function checks whether the mouse pointer is over the button by comparing the mouse's position to the position and size of the button.
 If the mouse is over the button, the function sets the button's width and height properties to the calculated values and returns true. Otherwise, it returns false.
--]]

-- Function to check if a button (regular or image) is hovered
function isButtonHovered (button)
	  local x, y, width, height
  -- Check if the button is an image button
    if button.image then
        x = button.x
        y = button.y
        width = button.image:getWidth()
        height = button.image:getHeight()
        
    else
       -- Calculate dimensions for a regular button with text
        local font = button.font or love.graphics.getFont()
        local textWidth = font:getWidth(button.text)
        local textHeight = font:getHeight()
        local sx, sy = button.sx or 1, button.sy or button.sx or 1
        x = button.x
        y = button.y
        width = textWidth * sx
        height = textHeight * sy
    end

-- Get the current mouse position
    local mouseX, mouseY = love.mouse.getPosition()
     -- Calculate emulated mouse position (if needed)
    local emulatedmousexscale = (emulatedmousex / 64) * 1.34
    local emulatedmouseyscale = (emulatedmousey / 45)
    local gx = emulatedmousexscale * 48
    local gy = emulatedmouseyscale * 46

  -- Check if the mouse cursor is within the button's bounds
    if (mouseX >= x and mouseX <= x + width and mouseY >= y and mouseY <= y + height) or
       ((gx + 1) >= x and (gx - 1) <= x + width and (gy + 1) >= y and (gy - 1) <= y + height) then
          -- Set the button's dimensions to the calculated width and height
        button.w, button.h = width, height
        return true	-- Return true to indicate that the button is being hovered over
    end

       return false  -- Return false to indicate that the button is not being hovered over
end
