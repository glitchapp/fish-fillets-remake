
function updateLevelSelection(dt)

inputtime = inputtime + dt
	timer2 = timer2 + dt
	timer3 = timer3 + dt
	
	updateButtonFocus(dt)
	updategetjoystickaxis()	--get gamepad axis positions
	updateRadialMenu()
	updatefpsmetrics(dt)			-- update FPS metrics
	
	-- Framerate limit
	if limitframerate == 30 then 
		if dt < 1/15 then 
			love.timer.sleep(1/17 - dt) 
		end
	elseif limitframerate == 60 then 
		if dt < 1/30 then 
			love.timer.sleep(1/32 - dt) 
		end
	end
	
	-- Check battery status if enabled
	if showbattery then 
		powerstate, powerpercent, powerseconds = love.system.getPowerInfo( )	
		if powerpercent == nil then 
			powerpercent = "∞" 
			powerseconds = "∞" 
		end
	end
	

	
	--[[
	if showtimedate then	-- if time and date is enabled from hub menu get the needed data
		systemdate = os.date("*t")
		systemsec = getTime.sec
		systemmin = getTime.min
		systemhours = getTime.hour
	end--]]
	
	
	-- Update game elements
	lovebpmupdate(dt)
	inputtime = inputtime + dt
	timer = timer + dt
	timer2 = timer2 + dt
	timer3 = timer3 + dt
	
				-- Check if game is still loading
	if isloading == true then 
		loadingupdate(dt) 
		fadeloadingprocess(dt) 
	end	
	
		-- Load the game once the loading is completed
	if loadingprocess == true then
		changelevel()
		gamestatus="game"
		timer3=0
		stepdone=0
		loadingprocess=false
		--love.audio.play( sharpechosound )
	end
		
		--if inputtime<1.6 then transitionTime=transitionTime+dt TransitionLogoUpdate(dt) end
-- Check if user has not selected a level for a long time
	if inputtime > 60 then
		initializeffishvalues()
		autoload=false 
		nLevel=101
		loadlevelassets()
		assignfrgbck()
		timer=0 
		timer2=0
		stepdone=0
		inputtime=0
		gamestatus="firststart"
		--music:stop()
	end
end

function updateButtonFocus(dt)
    -- Update pulsating effect
    pulsateTimer = pulsateTimer + dt
    if pulsateTimer >= 1 then
        pulsateTimer = 0
    end

    -- Update particle sparkles
    for i, sparkle in ipairs(sparkleParticles) do
        sparkle.x = sparkle.x + sparkle.speed * dt
        sparkle.y = sparkle.y + sparkle.speed * dt

        -- Remove sparkles that go off-screen
        if sparkle.x > love.graphics.getWidth() or sparkle.y > love.graphics.getHeight() then
            table.remove(sparkleParticles, i)
        end
    end
end

-- In your game update function (e.g., love.update), update the alpha when hovered for any button
function updateButtonAlpha(button)
    if isButtonHovered(button) then
        button.alpha = math.min(1, button.alpha + 0.05 ) -- Increase alpha gradually
    else
        button.alpha = math.max(0, button.alpha - 0.05 ) -- Decrease alpha gradually
    end
    
end

function updateButtonZoom(button)
local hoverScale = 1.2 -- Scale factor when hovered

		if isButtonHovered(button) and not (button.image) then
			button.sx = math.min(1.5, button.sx + 0.05) -- Increase button gradually
	elseif isButtonHovered(button) and button.image then
			--button.sx = math.min(1.5, button.sx + 0.05) -- Increase image gradually		
    elseif not (isButtonHovered(button)) and button.image then
			--button.sx = math.max(0.5, button.sx - 0.05) -- Decrease image gradually
    elseif not (isButtonHovered(button)) and not (button.image) then
			button.sx = math.max(1, button.sx - 0.05) -- Decrease button gradually
		
    end
end


function buttonfocusanimations()

-- Implement the pulsating effect on the buttons
    local focusColorShift = math.abs(math.sin(pulsateTimer * math.pi))
    
			if timer2>1 then --love.graphics.setColor(0, 1 - focusColorShift, 0, 1)
							love.graphics.setColor(0,1,0,1) 
								gamepadselectanimation=false
			elseif timer2<1 then love.graphics.setColor(0,timer2,timer2-1,1)
			end	
			
					if gamepadselectanimation==false then
				elseif gamepadselectanimation==true then
						if timer2<3 then fwidth=100+timer2*40 fheight=20+timer2*40
						end
						
				end
end
