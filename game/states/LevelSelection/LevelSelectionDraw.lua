
-- If gamestatus is "levelselection", draw the level selection menu
function drawLevelselection()
	shader2=false
	-- Draw 3d background with lovebpm
	lovebpmdraw()
	-- Draw level selection
	leveldraw()
	-- If the game is loading, draw the loading icon
	if isloading==true then loadingdraw(dt) end
	-- Draw FPS metrics
	drawfpsmetrics()
	-- Draw battery info if enabled
	
	--[[if inputtime<1.8 then
		love.graphics.setShader(transitionShader)
		InvertedTransitionLogoDraw()
		love.graphics.setShader()
	end
	--]]
end




--[[The first function drawTile draws a single tile on the screen. It takes in x and y coordinates and a tileSize value as parameters. It checks if the sum of x and y is even, and sets the tile color accordingly. Then, it sets the color to the chosen value, and draws a rectangle at the appropriate coordinates with the specified tile size.--]]
local function drawTile (x, y, tileSize)
	local color = {0.2, 0.2, 0.2}
	if (x+y)%2 == 0 then
		color = {0.3, 0.3, 0.3}
	end
	love.graphics.setColor (color)
	love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
end






















