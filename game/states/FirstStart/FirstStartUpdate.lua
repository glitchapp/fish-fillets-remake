
function updateFirstStart(dt)
	updateBoids(dt)
timer = timer +dt -- add the time elapsed since the last update to the timer variable
	timer2 = timer2 +dt -- add the time elapsed since the last update to the timer2 variable
	timer3 = timer3 + dt -- add the time elapsed since the last update to the timer3 variable
	updatefpsmetrics(dt) -- update the FPS metrics

	--framerate limit
	if limitframerate==30 then -- if the frame rate limit is set to 30 frames per second
		if dt < 1/15 then love.timer.sleep(1/17 - dt) end -- if the time elapsed since the last update is less than 1/15, sleep for the remaining time to maintain the frame rate limit
	elseif limitframerate==60 then -- if the frame rate limit is set to 60 frames per second
		if dt < 1/60 then love.timer.sleep(1/40 - dt) end -- if the time elapsed since the last update is less than 1/60, sleep for the remaining time to maintain the frame rate limit
	end
	
	smallfishupdate(dt) -- update the small fish animation frames
	bigfishupdate(dt) -- update the big fish animation frames
	ffish1update(dt) -- update the ffish1 animation frames
	fishsupdate(dt) -- update the fishs animation frames
	
	--bore scripts
	inputtime=inputtime +dt -- add the time elapsed since the last update to the inputtime variable
	if timer2>1 then 
		timer2=0	-- reset timer2 to 0 if it has elapsed for more than 1 second
	end 
	if love.mouse.isDown(0) or love.mouse.isDown(1) or love.mouse.isDown(2) then -- if the left, middle or right mouse button is pressed
		 -- set the shader and autoload variables to false, reset the input time and start loading the level music
		shader1=false 
		shader2=false 
		autoload=true 
		screensaver=false
		inputtime=0
		
		nLevel=1 -- set the level number to 1
		loadlevelvariables() -- load the level variables
		levelload() -- load the level
		
		lovebpmload("externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
		music:play()
		gamestatus="levelselection" -- set the game status to level selection mode
		
		love.timer.sleep( 0.5 ) -- sleep for 0.5 seconds
	end
		if inputtime>30 then -- if the input time has elapsed for more than 30 seconds
			shader1=false shader2=false -- set the shader variables to false
			timer=0 timer2=0 -- reset the timer and timer2 variables
			stepdone=0 -- reset the stepdone variable
			inputtime=0 -- reset the inputtime variable
		
		randomvideo=math.random(0,1)          -- generate a random number 0 or 1
		if randomvideo==0 then                -- if random number is 0, play intro
			music:stop()
			introvideo = love.graphics.newVideo( "externalassets/video/intro.ogg" )
			introvideo:play()							
			introvideoplaying=true
			hideplayercontrols=true
			randomvideo=math.random(0,1)
			gamestatus="intro"
						
		elseif randomvideo==1 then             -- if random number is 1, play Briefcase video
			nLevel=2                          -- set level number to 2
			loadvoices()                      -- load voices for the level
			loadlevelassets()                 -- load assets for the level
			require("game/states/BriefcaseMessage/BriefcaseMessage") -- load the briefcase message script
			timer=0                           -- reset timer
			stepdone=0                        -- reset stepdone
			loadsubtitles()                   -- load subtitles for the video
			optionsload()                     -- reload the values for the languages on the menus
			loadtouchtext()                   -- reload the touch interface
			goodmorningsaidonce=false         
			if talkies==true then             -- if Talkies is enabled, clear messages and play briefcase video
				Talkies.clearMessages() 
				Obey.lev2briefcase() 
			end
			randomvideo=math.random(0,1)      -- generate a new random number 0 or 1
			gamestatus="briefcasemessage"     -- set game status to briefcase message
			briefcaseload()                   -- load briefcase message assets
		end
	end
love.timer.sleep( 0.1 )               -- sleep for 0.1 seconds

end
