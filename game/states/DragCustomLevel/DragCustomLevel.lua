function DragCustomLevel()
	require ('game/states/LevelEditor/LevelEditor')
	gridWidth = 50
	gridHeight = 30
	require ('game/states/LevelEditor/levelEditor/levelEditor_Load_levels') 	-- this file load custom le	vels
	require ('game/states/LevelEditor/levelEditor/levelEditor_Play_level') 	-- this file load custom levels
	
	CustomLevelIcon =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/customlevel.webp")))
end
