
function love.filedropped(file)
    local filename = file:getFilename()
    local ext = filename:match("%.%w+$")

    if not (filename == nil) then
        if ext == ".lua" then
            print("File detected")
            file:open("r")
            local fileData = file:read(file:getSize()) -- Read the entire file

            -- Load and execute the Lua code to get the level data
            local chunk, error_message = load(fileData) -- Use 'load' here
            if chunk then
                local level = chunk()
                if type(level) == "table" then
                    PlayingFromLevelEditor = true
                    music:stop()
                    lovebpmload("externalassets/music/poinl/ambient2_Nautilus.ogg")
                    music:play()
                    assignfrgbck()
                    love.graphics.clear()
                    dx, dy = 0, 0
                    steps = 0
                    pb:load(level)
                    palette = 2
                    shader1 = false
                    shader2 = false
                    nLevel = 120
                    gamestatus = "game"
                else
                    print("Error: Lua code did not return a table")
                end
            else
                print("Error loading Lua code: " .. error_message)
            end
        end
    end
end
