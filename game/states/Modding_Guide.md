# Modding Guide for Fish fillets remake

## Introduction

Welcome to the modding guide for Fish fillets remake. This guide will help you understand the structure of the game's code and how to contribute or create your own mods.

## Folder Structure

The game is organized into modular components called "states," each residing in its own folder. Here's a quick overview of the folder structure:

- **game/**
  - **states/**
    - Each state has its own folder (e.g., SplashScreen, FirstStart).
      - [StateName].lua: Contains the initialization logic for the state.
      - [StateName]Update.lua: Handles the state's update logic.
      - [StateName]Draw.lua: Manages the state's drawing logic.

States tree:

 * [gameStateModules.lua](./gameStateModules.lua)
 * [Tetris](./Tetris)
   * [TetrisUpdate.lua](./Tetris/TetrisUpdate.lua)
   * [TetrisDraw.lua](./Tetris/TetrisDraw.lua)
 * [FirstStart](./FirstStart)
   * [FirstStartUpdate.lua](./FirstStart/FirstStartUpdate.lua)
   * [FirstStartDraw.lua](./FirstStart/FirstStartDraw.lua)
 * [SplashScreen](./SplashScreen)
   * [Source](./SplashScreen/Source)
     * [LICENSE.md](./SplashScreen/Source/LICENSE.md)
     * [o-ten-one](./SplashScreen/Source/o-ten-one)
       * [baby.png](./SplashScreen/Source/o-ten-one/baby.png)
       * [handy-andy.otf](./SplashScreen/Source/o-ten-one/handy-andy.otf)
       * [heart.png](./SplashScreen/Source/o-ten-one/heart.png)
       * [init.lua](./SplashScreen/Source/o-ten-one/init.lua)
       * [logo-mask.png](./SplashScreen/Source/o-ten-one/logo-mask.png)
       * [logo.png](./SplashScreen/Source/o-ten-one/logo.png)
       * [timer.lua](./SplashScreen/Source/o-ten-one/timer.lua)
     * [README.md](./SplashScreen/Source/README.md)
     * [main.lua](./SplashScreen/Source/main.lua)
   * [SplashScreenUpdate.lua](./SplashScreen/SplashScreenUpdate.lua)
   * [SplashScreenDraw.lua](./SplashScreen/SplashScreenDraw.lua)
   * [SplashScreen.lua](./SplashScreen/SplashScreen.lua)
 * [Benchmark](./Benchmark)
   * [BenchmarkUpdate.lua](./Benchmark/BenchmarkUpdate.lua)
   * [BenchmarkDraw.lua](./Benchmark/BenchmarkDraw.lua)
   * [Benchmark.lua](./Benchmark/Benchmark.lua)
 * [Game](./Game)
   * [GameUpdate.lua](./Game/GameUpdate.lua)
   * [GameDraw.lua](./Game/GameDraw.lua)
 * [GamePlusOptions](./GamePlusOptions)
   * [GamePlusOptionsUpdate.lua](./GamePlusOptions/GamePlusOptionsUpdate.lua)
 * [CreditsEnd](./CreditsEnd)
   * [CreditsUpdate.lua](./CreditsEnd/CreditsUpdate.lua)
   * [CreditsEndUpdate.lua](./CreditsEnd/CreditsEndUpdate.lua)
 * [tree.md](./tree.md)
 * [LevelEditorMain](./LevelEditorMain)
   * [LevelEditorMain.lua](./LevelEditorMain/LevelEditorMain.lua)
   * [LevelEditorMainUpdate.lua](./LevelEditorMain/LevelEditorMainUpdate.lua)
   * [LevelEditorMainDraw.lua](./LevelEditorMain/LevelEditorMainDraw.lua)
 * [Options](./Options)
   * [Options.lua](./Options/Options.lua)
   * [OptionsDraw.lua](./Options/OptionsDraw.lua)
   * [OptionsUpdate.lua](./Options/OptionsUpdate.lua)
 * [GameShaders](./GameShaders)
   * [GameShadersDraw.lua](./GameShaders/GameShadersDraw.lua)
   * [GameShadersUpdate.lua](./GameShaders/GameShadersUpdate.lua)
   * [GameShaders.lua](./GameShaders/GameShaders.lua)
   * [shaders.lua](./GameShaders/shaders.lua)
 * [BriefcaseMessage](./BriefcaseMessage)
   * [BriefcaseMessage.lua](./BriefcaseMessage/BriefcaseMessage.lua)
   * [BriefcaseMessageUpdate.lua](./BriefcaseMessage/BriefcaseMessageUpdate.lua)
   * [BriefcaseMessageDraw.lua](./BriefcaseMessage/BriefcaseMessageDraw.lua)
 * [Credits](./Credits)
   * [Credits.lua](./Credits/Credits.lua)
   * [CreditsDraw.lua](./Credits/CreditsDraw.lua)
   * [CreditsUpdate.lua](./Credits/CreditsUpdate.lua)
 * [LevelCompleted](./LevelCompleted)
   * [LevelCompleted.lua](./LevelCompleted/LevelCompleted.lua)
   * [LevelCompletedDraw.lua](./LevelCompleted/LevelCompletedDraw.lua)
   * [LevelCompletedUpdate.lua](./LevelCompleted/LevelCompletedUpdate.lua)
 * [LevelSelection](./LevelSelection)
   * [LevelSelection.lua](./LevelSelection/LevelSelection.lua)
   * [LevelSelectionUpdate.lua](./LevelSelection/LevelSelectionUpdate.lua)
   * [LevelSelectionDraw.lua](./LevelSelection/LevelSelectionDraw.lua)
 * [Music](./Music)
   * [Music.lua](./Music/Music.lua)
   * [MusicUpdate.lua](./Music/MusicUpdate.lua)
   * [MusicDraw.lua](./Music/MusicDraw.lua)
 * [Extras](./Extras)
   * [Extras.lua](./Extras/Extras.lua)
   * [ExtrasUpdate.lua](./Extras/ExtrasUpdate.lua)
   * [ExtrasDraw.lua](./Extras/ExtrasDraw.lua)
 * [ExtrasIntro](./ExtrasIntro)
   * [ExtrasIntro.lua](./ExtrasIntro/ExtrasIntro.lua)
   * [ExtrasIntroUpdate.lua](./ExtrasIntro/ExtrasIntroUpdate.lua)
   * [ExtrasIntroDraw.lua](./ExtrasIntro/ExtrasIntroDraw.lua)
 * [DragCustomLevel](./DragCustomLevel)
   * [DragCustomLevel.lua](./DragCustomLevel/DragCustomLevel.lua)
   * [DragCustomLevelUpdate.lua](./DragCustomLevel/DragCustomLevelUpdate.lua)
   * [DragCustomLevelDraw.lua](./DragCustomLevel/DragCustomLevelDraw.lua)
 * [Ends](./Ends)
   * [Ends.lua](./Ends/Ends.lua)
   * [EndsDraw.lua](./Ends/EndsDraw.lua)
   * [EndsUpdate.lua](./Ends/EndsUpdate.lua)
 * [GameOver](./GameOver)
   * [GameOverUpdate.lua](./GameOver/GameOverUpdate.lua)
   * [GameOverDraw.lua](./GameOver/GameOverDraw.lua)
   * [GameOver.lua](./GameOver/GameOver.lua)
 * [AlternativeLevelSelection](./AlternativeLevelSelection)
   * [AlternativeLevelSelectionDraw.lua](./AlternativeLevelSelection/AlternativeLevelSelectionDraw.lua)
 * [mainStates.lua](./mainStates.lua)
 * [LevelEditor](./LevelEditor)
   * [LevelEditorUpdate.lua](./LevelEditor/LevelEditorUpdate.lua)
   * [LevelEditorDraw.lua](./LevelEditor/LevelEditorDraw.lua)
   * [LevelEditor.lua](./LevelEditor/LevelEditor.lua)
   * [levelEditor](./LevelEditor/levelEditor)
   * [Export2Graphics.lua](./LevelEditor/levelEditor/Export2Graphics.lua)
   * [AddBackground.lua](./LevelEditor/levelEditor/AddBackground.lua)
   * [Help.lua](./LevelEditor/levelEditor/Help.lua)
   * [keyinputLevelEditor.lua](./LevelEditor/levelEditor/keyinputLevelEditor.lua)
   * [AddExitArea.lua](./LevelEditor/levelEditor/AddExitArea.lua)
   * [leveleditorExportFolder.lua](./LevelEditor/levelEditor/leveleditorExportFolder.lua)
   * [About.lua](./LevelEditor/levelEditor/About.lua)
   * [AddTiles.lua](./LevelEditor/levelEditor/AddTiles.lua)
   * [AddAgents.lua](./LevelEditor/levelEditor/AddAgents.lua)
   * [AddObjects.lua](./LevelEditor/levelEditor/AddObjects.lua)
   * [leveleditorDraw.lua](./LevelEditor/levelEditor/leveleditorDraw.lua)
   * [AddGeometry](./LevelEditor/levelEditor/AddGeometry)
     * [AddCircle.lua](./LevelEditor/levelEditor/AddGeometry/AddCircle.lua)
     * [AddLine.lua](./LevelEditor/levelEditor/AddGeometry/AddLine.lua)
     * [AddRectangle.lua](./LevelEditor/levelEditor/AddGeometry/AddRectangle.lua)
   * [levelEditorMouseInput.lua](./LevelEditor/levelEditor/levelEditorMouseInput.lua)
   * [leveleditorTabs.lua](./LevelEditor/levelEditor/leveleditorTabs.lua)
   * [levelEditor_Play_level.lua](./LevelEditor/levelEditor/levelEditor_Play_level.lua)
   * [levelEditor_Load_levels.lua](./LevelEditor/levelEditor/levelEditor_Load_levels.lua)
   * [leveleditorButtons.lua](./LevelEditor/levelEditor/leveleditorButtons.lua)
   * [leveleditor.lua](./LevelEditor/levelEditor/leveleditor.lua)

# other game structures:

- game/ interface: This folder contains most of the game's graphical interfaces.
- game /levels: This folder contain all levels and all loading functionality for the level assets
- game / ends: End animations for the game's areas.
- game /dialogs: All subtitles, dubs, syncing mechanism etc... can be found on this folder.



# Independent modules:

## About / Windows manager:

* [https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/About.lua](About.lua) About.lua contains the buttons, text parsing mechanism and a simple windows manager which allows to drag the windows and move it around, close it etc.

- A standalone tool containing the functions has been created so that the module can be implemented in other projects and tweaked / improved.
* [https://codeberg.org/glitchapp/love-windows-manager](Windows manager standalone) 

## Level editor:

* [https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/states/LevelEditor](Level editor) The level editor of the game which allows to create, save and export custom levels.

- A standalone version can be found on the repository ready to beimplemented to other projects or to be tweaked and improved.
* [https://codeberg.org/glitchapp/Grid-Level-Editor](Level editor) 

## Music player:

* [https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/states/Music](Music player) The built it music player allows to browse and play the music soundtracks while rendering information about the authors and graphic bars about the music length, volume etc.

* [https://codeberg.org/glitchapp/Music-Player](Music player standalone) The music player has been made standalone but the project differs slightly from the original in that I added several extra features such as vu Bars and a forked retro demoscene shader collection created by created by Cedric Baudouin (See readme on the project for more information).


## Analog clock:

* [https://codeberg.org/glitchapp/fish-fillets-remake/src/branch/main/game/interface/AnalogClock](Analog clock) A simple analog clock that can be enabled from the graphic /hub menu and it is also used on level 15 (for the analog clock).

- A standalone can be found in the following repository just in case anyone wants to fork, improve or implement it on other projects:

* [https://codeberg.org/glitchapp/Analog-clock](Standalone analog clock) 

## Game controller:
* [https://codeberg.org/glitchapp/love-controller] (Game controller)
Functions aimed at navigating the game's menu with a game controller featuring thumbstick pointer control and radial menu.

# Contributing

If you want to contribute to the game or create your own mods, follow these steps:

1. **Understand the States:**
   - Explore the `states/` folder to understand the existing states and their functionalities.

2. **Create a New State:**
   - To add a new state, create a new folder under `states/`.
   - Inside the folder, create [StateName].lua, [StateName]Update.lua, and [StateName]Draw.lua.
   - Register your new state in the `loadStates` function in `main.lua`.

3. **Modify Existing States:**
   - If you want to tweak existing states, modify the respective [StateName].lua, [StateName]Update.lua, or [StateName]Draw.lua files.

4. **Collaborate:**
   - Join the community forums or repositories to collaborate with other modders and developers.

## Important Notes

- Ensure that your mods do not conflict with the game's core functionalities.
- Document your code and changes for better collaboration and understanding.
- Test your mods thoroughly before sharing them with the community.

Happy modding!
