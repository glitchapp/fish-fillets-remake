

causticsoffButton = {
	text = "",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,

}

causticsonButton = {
	text = "",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


crtoffButton = {
	text = "",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

crtonButton = {
	text = "",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


godsgrideffectButton
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

godsgrideffectButtonOFF
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

gameboyeffectButton
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*7,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

gameboyeffectButtonOFF
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*7,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

bluenoiseeffectButton
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*8,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

bluenoiseeffectButtonOFF
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*8,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

speccyditheringeffectButton
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*9,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

speccyditheringeffectButtonOFF
= {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*9,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawGraphicShaders()
drawGraphicTabs()
	love.graphics.rectangle("line", ShadersTabButton.x,ShadersTabButton.y,150, 30) 
	

	
	love.graphics.print ("Caustic effects: ", xspace*2,causticsonButton.y)
			love.graphics.print ("CRT: ", xspace*2,crtonButton.y)
			love.graphics.print ("Gods ray + grid: ", xspace*2,godsgrideffectButton.y)
			love.graphics.print ("Gameboy: ", xspace*2,gameboyeffectButton.y)
			love.graphics.print ("Blue noise: ", xspace*2,bluenoiseeffectButton.y)
			love.graphics.print ("Speccy dithering: ", xspace*2,speccyditheringeffectButton.y)
			
			
		
			-- Chromatic aberration
			love.graphics.print ("Chromatic aberration: ", xspace*2,ChromaticAberrationOnButton.y)
	
	-- Caustic shaders
	--The final two blocks of code are for buttons labeled "causticsonButton" and "causticsoffButton," which enable or disable a caustic effect shader. The behavior of these buttons is also similar to the other buttons, with "caustics" and "shader2" being set to true or false depending on which button is clicked. A message is displayed on the GUI explaining the effect of enabling or disabling the shader.
	--The "love.timer.sleep( 0.3 )" command is used after each button is clicked to prevent rapid clicking of the button and to provide a short delay before executing the function associated with the button.
	
	if caustics==false then
		local hovered = isButtonHovered (causticsonButton)
		drawButton (causticsonButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="Enables caustic effect shader"
						graphmes2="It could decrease performance and increase battery comsumption "
						graphbuttonhovered=true
		end
	
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		caustics=true shader2=true
		end
	elseif caustics==true then	
		local hovered = isButtonHovered (causticsoffButton)
		drawButton (causticsoffButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="Disables caustic effect shader" graphmes2="" graphbuttonhovered=true end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_off_sound:play()
		love.timer.sleep( 0.3 )
		caustics=false shader2=false
		end
	end

 --[[
This code appears to be a part of a graphics program that enables various shaders and effects when certain buttons are clicked or hovered over.

The code starts by setting the color for the graphics object, then checks if the "crtonButton" is being hovered over. If it is, a message is set to be displayed, and if it is clicked, certain effects are enabled, and various parameters are adjusted.
    
The code then checks for hovering and clicking on the "crtoffButton," which disables the CRT effect and resets some parameters.
    --]]
        -- effects
    	-- Moonshine shaders
    	--The Moonshine shader buttons toggle between different types of Moonshine shaders. Only one button is highlighted at a time.

    
    --CRT
    if not (shader2type=="crt") or (shader1==false) then
		love.graphics.setColor(adjustr,adjustg,adjustb,1)
		local hovered = isButtonHovered (crtonButton)
		drawButton (crtonButton, hovered)
		if hovered then 
			graphmes1="Enable the CRT effect"
			graphmes2="It may decrease performance and increase battery comsumption"
			graphbuttonhovered=true
		end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
			effect.enable("scanlines","crt","glow","filmgrain")
	
			effect.scanlines.opacity=0.1
			effect.glow.min_luma = 1
			effect.filmgrain.size=0.5
			effect.filmgrain.opacity=1
				
		shader1=true
		shader2type="crt" caustics=true
	end
	
	elseif shader2type=="crt" and shader1==true then
	
		local hovered = isButtonHovered (crtoffButton)
		drawButton (crtoffButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print ("Disables the CRT effect", 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			switch_off_sound:play()
			love.timer.sleep( 0.3 )
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
			moonshineeffect=0
			palette=1
			shader1type=false
			shader1=false
			shader2=false
		end
	end
	
		--[[The next section checks for hovering and clicking on the "godsgrideffectButton" and "gameboyeffectButton," enabling various effects and setting specific parameters for each effect.--]]
		--Gods grid
		if shader1==false or not (shader1type=="godsgrid") then
			local hovered = isButtonHovered (godsgrideffectButton)
			drawButton (godsgrideffectButton, hovered)
			if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
			if hovered then 
				graphmes1="Combines 'godsray' shader with grid mode"
			graphmes2="It may decrease performance and increase battery comsumption "
				graphbuttonhovered=true
			end
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				switch_on_sound:play()
				love.timer.sleep( 0.3 )
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
				effect.enable("godsray")
				palette=7
				shader1=true shader2=false shader1type="godsgrid" caustics=false
			end
		elseif shader1==true or (shader1type=="godsgrid") then
			local hovered = isButtonHovered (godsgrideffectButtonOFF)
			drawButton (godsgrideffectButtonOFF, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				switch_off_sound:play()
				love.timer.sleep( 0.3 )
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
				moonshineeffect=0
				palette=1
				shader1type=false
				shader1=false
				shader2=false
			end
		end
		
	-- Gameboy
	if shader1==false or not (shader1type=="gameboy") then
		local hovered = isButtonHovered (gameboyeffectButton)
		drawButton (gameboyeffectButton, hovered)
		--if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then
			graphmes1="Enables scanlines & dmg shaders"
			graphmes2="It may decrease performance and increase battery comsumption "
			graphbuttonhovered=true
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				switch_on_sound:play()
				love.timer.sleep( 0.3 )
				--gameboy
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
				effect.enable("scanlines","dmg")
				--palette=3
				effect.vignette.radius=1.2
				effect.vignette.opacity=3
				effect.dmg.palette=4
				effect.filmgrain.size=2.5
				effect.filmgrain.opacity=2
				effect.scanlines.width=1
				setuppaletteforshaderfilters()
				shader1=true	shader1type="gameboy" caustics=false
		end
	elseif shader1==true or (shader1type=="gameboy") then
		local hovered = isButtonHovered (gameboyeffectButtonOFF)
			drawButton (gameboyeffectButtonOFF, hovered)
			--if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
			if hovered then
				graphmes1="Enables scanlines & dmg shaders"
				graphmes2="It may decrease performance and increase battery comsumption "
				graphbuttonhovered=true
			end
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				switch_off_sound:play()
				love.timer.sleep( 0.3 )
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
				moonshineeffect=0
				palette=1
				shader1type=false
				shader1=false
				shader2=false
			end
	end
	
	
	-- Blue noise dithering effect
	if shader1==false or not (shader1type=="bluenoise") then
		local hovered = isButtonHovered (bluenoiseeffectButton)
		drawButton (bluenoiseeffectButton, hovered)
		--if hovered and gamestatus=="gameplusoptions" then  end
		if hovered then
			graphmes1="Enables a blue noise dithering effect"
			graphmes2="It could impact performance"
			graphbuttonhovered=true
			
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				shader1=false shader2=false
				switch_on_sound:play()
				love.timer.sleep( 0.3 )
				
				-- Import blue noise dithering shader created by chatGPT
				bluenoise = require '/shaders/zblueNoise/bluenoise'
				
				-- Load the blue noise texture
				local blueNoiseTexture = love.graphics.newImage("shaders/zblueNoise/BlueNoise470.png")
				
				-- Set the blue noise dithering shader
				love.graphics.setShader(blueNoiseDitherShader)
	
				ditherIntensity = 0.5
				-- Pass the intensity value and blue noise texture to the shader
				blueNoiseDitherShader:send("intensity", ditherIntensity)
				blueNoiseDitherShader:send("blueNoiseTexture", blueNoiseTexture)
		
				setuppaletteforshaderfilters()
				shader1=true	shader1type="bluenoise" caustics=false
		end
		
	elseif shader1==true or (shader1type=="bluenoise") then
				local hovered = isButtonHovered (bluenoiseeffectButtonOFF)
		drawButton (bluenoiseeffectButtonOFF, hovered)
		--if hovered and gamestatus=="gameplusoptions" then  end
		if hovered then
			graphmes1="Enables a blue noise dithering effect"
			graphmes2="It could impact performance"
			graphbuttonhovered=true
			
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				switch_off_sound:play()
				love.timer.sleep( 0.3 )
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
				moonshineeffect=0
				palette=1
				shader1type=false
				shader1=false
				shader2=false
		end
	end
	
	if shader1==false or not (shader1type=="bluenoise") then
		-- Speccy color quantization + blue noise dithering shader
		local hovered = isButtonHovered (speccyditheringeffectButton)
		drawButton (speccyditheringeffectButton, hovered)
		--if hovered and gamestatus=="gameplusoptions" then  end
		if hovered then
			graphmes1="Enables the speccy dithering shader"
			graphmes2="It could impact performance"
			graphbuttonhovered=true
			
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
				shader1=false shader2=false
				switch_on_sound:play()
				love.timer.sleep( 0.3 )
				
				-- load the speccy color quantization + blue noise dithering shader
				bluenoise = require '/shaders/zblueNoise/bluenZx'
	
				-- Load the blue noise texture
				local blueNoiseTexture = love.graphics.newImage("shaders/zblueNoise/BlueNoise470.png")
	
				-- Load zx palette
				local zxpaletteTexture = love.graphics.newImage("shaders/zblueNoise/zxpaletteTexture.png")
				
				-- Set the blue noise dithering shader
				love.graphics.setShader(blueNoiseDitherShader)
	
				ditherIntensity = 0.5
				-- Pass the intensity value and blue noise texture to the shader
				blueNoiseDitherShader:send("intensity", ditherIntensity)
				blueNoiseDitherShader:send("blueNoiseTexture", blueNoiseTexture)
    
				setuppaletteforshaderfilters()
				shader1=true	shader1type="bluenoise" caustics=false
		end
		
	elseif shader1==true or (shader1type=="bluenoise") then
		local hovered = isButtonHovered (speccyditheringeffectButtonOFF)
		drawButton (speccyditheringeffectButtonOFF, hovered)
		--if hovered and gamestatus=="gameplusoptions" then  end
		if hovered then
			graphmes1="Enables the speccy dithering shader"
			graphmes2="It could impact performance"
			graphbuttonhovered=true
			
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_off_sound:play()
		love.timer.sleep( 0.3 )
				effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
				moonshineeffect=0
				palette=1
				shader1type=false
				shader1=false
				shader2=false
		end
		
	end
end
