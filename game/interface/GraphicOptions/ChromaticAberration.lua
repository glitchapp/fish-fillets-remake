ChromaticAberrationOnButton
= {
	text = "",
	x = xspace*4,
	y = yspace*10,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ChromaticAberrationOffButton
= {
	text = "",
	x = xspace*4,
	y = yspace*10,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawGraphicChromaticAberration()

--[[This section of the code is responsible for handling different graphical options for the game. Here's what it does:

The ChromaticAberrationOnButton and ChromaticAberrationOffButton are buttons to turn the chromatic aberration effect on and off, respectively.

If the mouse is hovering over the ChromaticAberrationOnButton, graphmes1 and graphmes2 are set to describe the effect and its potential impact on performance, and graphbuttonhovered is set to true	

If the ChromaticAberrationOnButton is clicked, chromaab is set to true after a 0.3 second delay, and the effect is turned on.

Similarly, if the mouse is hovering over the ChromaticAberrationOffButton, graphmes1 is set to describe that the effect is being disabled, and graphbuttonhovered is set to true.

If the ChromaticAberrationOffButton is clicked, chromaab is set to false after a 0.3 second delay, and the effect is turned off.
--]]
	-- Chromatic aberration
	if chromaab==false then
			local hovered = isButtonHovered (ChromaticAberrationOnButton)		-- Set Chromatic aberration on
		drawButton (ChromaticAberrationOnButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="This option enables a chromatic aberration effect"  
						graphmes2="This effect could decrease performance"
						graphbuttonhovered=true
		
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		chromaab=true
		end
	elseif chromaab==true then
	
			local hovered = isButtonHovered (ChromaticAberrationOffButton)		-- Set Chromatic aberration off
		drawButton (ChromaticAberrationOffButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="Disables chromatic aberration effect" graphmes2="" graphbuttonhovered=true end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_off_sound:play()
		love.timer.sleep( 0.3 )
		chromaab=false
		end
	end
end
