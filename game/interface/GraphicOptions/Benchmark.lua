--[[
	The first block of code is for a button labeled "benchmarkButton" that, when clicked, will start a benchmark tool.
	The variable "hovered" is assigned the value returned by the function "isButtonHovered," which checks if the mouse cursor is hovering over the button.
	The function "drawButton" is then called to draw the button on the GUI.
	If the button is being hovered over, a message is displayed on the GUI explaining what the button does, and the "graphbuttonhovered" variable is set to true.
	If the button is both being hovered over and clicked with the left mouse button, the function "benchmarkload()" is called to initiate the benchmark tool, and the "gamestatus" variable is set to "benchmark".
	--]]

benchmarkButton
= {
	text = "Start Benchmark",
	x = xspace*2,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

function DrawBenchmark()
drawGraphicTabs()
love.graphics.rectangle("line", PerformanceTabButton.x,PerformanceTabButton.y,150, 30) 
	
	
	
			local hovered = isButtonHovered (benchmarkButton)		-- Start benchmark
	drawButton (benchmarkButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Start a benchmark tool" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		benchmarkload()
		gamestatus="benchmark"
		love.timer.sleep( 0.3 )
	end
end
