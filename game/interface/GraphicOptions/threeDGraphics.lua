
threedonButton = {
	text = "",
	textPosition ="top",
	x = xspace*4,
	y = yspace*3,
    sx = 1,
    image = SwitchOFF,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfish,
    unlocked = true,
	}
--[[
threedonButton = {
	text = ont,
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
--]]
threedoffButton = {
	text = "",
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
	unlocked = true,
}

function DrawthreeDGraphics()

    love.graphics.print ("3D: ", xspace*2,threedonButton.y)
		-- 3d engine
		--The 3D engine buttons turn the 3D engine on or off.


	--3d Graphics
	--The next two buttons are related to 3D graphics. When the threedonButton is hovered over, a message is displayed on the screen indicating that 3D graphics will be enabled. If the left mouse button is clicked, then the threeD variable is set to true and 3D assets are loaded. Similarly, when the threedoffButton is hovered over, a message is displayed indicating that 3D graphics will be disabled, and if the left mouse button is clicked, then the threeD variable is set to false.
	if threeD==false then
		local hovered = isButtonHovered (threedonButton)
		drawButton (threedonButton, hovered)
		if hovered then graphmes1="Enables 3d graphics" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
			if threedassetsloaded==false then 
				require ("game/levels/3dlevel")		--3DreamEngine graphics
				load3dassets()
				threedassetsloaded=true
			end
			threeD=true
		end
	elseif threeD==true then
	
		local hovered = isButtonHovered (threedoffButton)
		drawButton (threedoffButton, hovered)
		if hovered then graphmes1="Disables 3d graphics" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			switch_off_sound:play()
			love.timer.sleep( 0.3 )
			threeD=false
		end
	end
end
