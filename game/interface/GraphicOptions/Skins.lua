	
	-- Game skins
	--[[
This section of code controls the selection of game skins, which change the visual style of the game. The code consists of several if statements, each corresponding to a different skin button. The first block of code corresponds to the "remake" skin button.

The variable "hovered" is set to the result of a function call to "isButtonHovered", which checks if the user's mouse cursor is hovering over the "skinremakeButton" button. The "drawButton" function is then called with "skinremakeButton" and "hovered" as arguments, which draws the button with a different color if the cursor is hovering over it.

If "hovered" is true, the message "Set remake graphics" is displayed and the "graphbuttonhovered" variable is set to true. If both "hovered" is true and the left mouse button is clicked, the game music is stopped and various game variables are set to change the skin to "remake" and load new music.

Similar blocks of code follow for the "retro" and "classic" skin buttons, as well as a separate block for options related to the "classic" skin. The code within the "classic" skin block allows the user to choose between original and upscaled graphics for the classic skin, with different variables being set depending on which option is chosen.
	--]]
	
skinremakeButton = {
	text = skinremaket,
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

skinretroButton = {
	text = skinretrot,
	x = xspace*5,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

skinclassicButton = {
	text = skinclassict,
	x = xspace*6,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

skinclassicnormalButton = {
	text = skinclassicnormalt,
	x = xspace*4,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

skinclassicupscaledButton = {
	text = skinclassicupscaledt,
	x = xspace*5,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawSkins()

love.graphics.print ("skin: ", xspace*2,skinremakeButton.y)
love.graphics.rectangle("line", SkinsTabButton.x,SkinsTabButton.y,150, 30) 

-- Skin
	love.graphics.setColor(1,0,1,1)
		--The skin buttons change the graphical skin of the game. 
		if skin=="remake" then love.graphics.rectangle("fill",skinremakeButton.x-5,skinremakeButton.y-5,200, 50)
    elseif skin=="retro" then love.graphics.rectangle("fill",skinretroButton.x-5,skinretroButton.y-5,200, 50)
    elseif skin=="classic" then love.graphics.rectangle("fill",skinclassicButton.x-5,skinclassicButton.y-5,200, 50)
    end
	
	love.graphics.setColor(1,1,1,1)
	local hovered = isButtonHovered (skinremakeButton)
	drawButton (skinremakeButton, hovered)
	if hovered then graphmes1="Set remake graphics" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		music:stop()
		--if skinlevel1loaded==false then loadlevelassets() skinlevel1loaded=true end
		skin="remake" musictype="new" palette=1 skinhaschanged=true musichaschanged=true
		loadlevelmusic()
		
		--mybpm=112
		--lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
		reloadpreviewassets()
		love.timer.sleep( 0.3 )
	
	end
	
	local hovered = isButtonHovered (skinretroButton)
	drawButton (skinretroButton, hovered)
	if hovered then graphmes1="Set retro graphics" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	skin="retro" palette=2 caustics=false shader1=false shader2=false

	end
	
	local hovered = isButtonHovered (skinclassicButton)
	drawButton (skinclassicButton, hovered)
	
	if hovered then graphmes1="Set classic graphics" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		music:stop()
		skin="classic" musictype="classic" skinhaschanged=true musichaschanged=true
		loadlevelmusicclassic()
		reloadpreviewassets()
		palette=1
		love.timer.sleep( 0.3 )
		--mybpm=112
		--lovebpmload("/externalassets/music/classic/menu.ogg")
	end
	
	
			
	
			-- upscale
		--The upscale buttons toggle between normal and upscaled graphics for the classic skin. Only one of the two buttons is highlighted at a time.
		if skin=="classic" and skinupscaled==false then
			love.graphics.rectangle("fill",skinclassicnormalButton.x-5,skinclassicnormalButton.y-5,200, 50)
    elseif skin=="classic" and skinupscaled==true then
			love.graphics.rectangle("fill",skinclassicupscaledButton.x-5,skinclassicupscaledButton.y-5,200, 50)
    end

	
	-- Upscaled graphics (classic mode only)
	if skin=="classic" then
		love.graphics.print ("Upscale: ", xspace*2,skinclassicnormalButton.y)
		local hovered = isButtonHovered (skinclassicnormalButton)
		drawButton (skinclassicnormalButton, hovered)
		
		if hovered then graphmes1="Original resolution for the classic graphics" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			music:stop()
			skinupscaled=false skinhaschanged=true musichaschanged=true
			loadlevelmusicclassic()
			reloadpreviewassets()
			love.timer.sleep( 0.3 )
		end
		
		local hovered = isButtonHovered (skinclassicupscaledButton)
		drawButton (skinclassicupscaledButton, hovered)
		
		if hovered then graphmes1="Upscaled classic graphics" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			music:stop()
			skinupscaled=true  musictype="classic" skinhaschanged=true musichaschanged=true
			loadlevelmusicclassic()
			reloadpreviewassets()
			love.timer.sleep( 0.3 )
		end
	
	end

end
	
