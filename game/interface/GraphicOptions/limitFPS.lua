--[[
	The fpsnoButton, fps30Button, and fps60Button are buttons to set the maximum framerate of the game.
    If the mouse is hovering over the fpsnoButton, graphmes1 is set to describe that there is no framerate limit, and graphbuttonhovered is set to true.
    If the fpsnoButton is clicked, limitframerate is set to nil, and there is no framerate limit.
    Similarly, if the mouse is hovering over the fps30Button, graphmes1 is set to describe that the framerate is being limited to 30fps, and graphbuttonhovered is set
	--]]
fps30Button
= {
	text = "30",
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

fps60Button
= {
	text = "60",
	x = xspace*5,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

fpsnoButton
= {
	text = "Unlimited",
	x = xspace*6,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

function DrawlimitFPS()

	love.graphics.print ("Graphic profile: ", xspace*2,lowprofileButton.y)
	love.graphics.print ("lmit FPS: ", xspace*2,fps30Button.y)

 -- limit FPS
    --The limit FPS buttons set the frame rate limit to 30, 60, or no limit. Only one button is highlighted at a time.
		love.graphics.setColor(1,0,1,0.7)
		if limitframerate==nil then love.graphics.rectangle("fill",fpsnoButton.x-5,fpsnoButton.y-5,200, 50) 
	elseif limitframerate==30 then love.graphics.rectangle("fill",fps30Button.x-5,fps30Button.y-5,200, 50) 
    elseif limitframerate==60 then love.graphics.rectangle("fill",fps60Button.x-5,fps60Button.y-5,200, 50) 
    end
    love.graphics.setColor(1,1,1,1)

	--limit FPS
			local hovered = isButtonHovered (fpsnoButton)		-- Do not limit framerate
	drawButton (fpsnoButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="unlimited framerate" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		limitframerate=nil
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (fps30Button)		-- limit framerate to 30fps
	drawButton (fps30Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="limit framerate to 30fps" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		limitframerate=30
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (fps60Button)		-- limit framerate to 60fps
	drawButton (fps60Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="limit framerate to 60fps" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		limitframerate=60
		love.timer.sleep( 0.3 )
	end
end
