--[[
    The vsyncOnButton and vsyncOffButton are buttons to turn Vsync on and off, respectively.
    If the mouse is hovering over the vsyncOnButton, graphmes1 is set to describe that Vsync is being enabled, and graphbuttonhovered is set to true.
    If the vsyncOnButton is clicked, vsyncstatus is set to true and the game window is set to synchronize with the monitor's refresh rate.
    Similarly, if the mouse is hovering over the vsyncOffButton, graphmes1 is set to describe that Vsync is being disabled, and graphbuttonhovered is set to true.
    If the vsyncOffButton is clicked, vsyncstatus is set to false and Vsync is turned off.
--]]
vsyncOnButton
= {
	text = "",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

vsyncOffButton
= {
	text = "",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


function DrawVSync()


			love.graphics.print ("Vsync: ", xspace*2,vsyncOnButton.y)

 -- Vsync
    --The Vsync buttons turn Vsync on or off. 
		if vsyncstatus==true then love.graphics.rectangle("fill",vsyncOnButton.x-5,vsyncOnButton.y-5,200, 50) 
    elseif vsyncstatus==false then love.graphics.rectangle("fill",vsyncOffButton.x-5,vsyncOffButton.y-5,200, 50) 
    end
    
	-- Vsync
	if vsyncstatus==false then
			local hovered = isButtonHovered (vsyncOnButton)		-- Set Vsync on
		drawButton (vsyncOnButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="enable Vsync" graphmes2="" graphbuttonhovered=true end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			vsyncstatus=true
			love.window.setVSync(true)
			switch_on_sound:play()
			love.timer.sleep( 0.3 )
		end
	elseif vsyncstatus==true then
	
			local hovered = isButtonHovered (vsyncOffButton)		-- Set Vsync off
		drawButton (vsyncOffButton, hovered)
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="disables Vsync" graphmes2="" graphbuttonhovered=true end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.window.setVSync(false)
			vsyncstatus=false
			switch_off_sound:play()
			love.timer.sleep( 0.3 )
		end
	end
end
