--[[
This section of the code is for handling the RGB (red, green, blue) color adjustments and presets. It first checks if any of the color adjustment buttons are being hovered over by the mouse and draws them accordingly. If a button is being hovered over, it sets the graphmes1 variable to a message indicating the action associated with that button and sets graphbuttonhovered to true.

If the hovered button is also being clicked (left mouse button is down), it waits for 0.3 seconds (using love.timer.sleep(0.3)) to prevent rapid clicking and adjusts the corresponding color value (adjustr, adjustg, or adjustb) by adding or subtracting 0.1, with limits of 0 and 1 to prevent values from going out of range.

There are also several RGB color presets defined as buttons (crimsonRGBButton, seagreenRGBButton, and bluevioletRGBButton). If these buttons are hovered over and clicked, they adjust the RGB values to preset values.
	--]]
rplusButton
= {
	text = "+",
	x = xspace*3,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
rminusButton
= {
	text = "-",
	x = xspace*2.7,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
gplusButton
= {
	text = "+",
	x = xspace*3,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
gminusButton
= {
	text = "-",
	x = xspace*2.7,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
bplusButton
= {
	text = "+",
	x = xspace*3,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

bminusButton
= {
	text = "-",
	x = xspace*2.7,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
-- RGB presets
resetRGBButton
= {
	text = "reset",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

crimsonRGBButton
= {
	text = "crimson",
	x = xspace*5,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

seagreenRGBButton
= {
	text = "sea green",
	x = xspace*6,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

bluevioletRGBButton
= {
	text = "blue violet",
	x = xspace*7,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawGraphicRGBPresets()
drawGraphicTabs()
love.graphics.rectangle("line", ColorsTabButton.x,ColorsTabButton.y,150, 30) 
	-- RGB
			love.graphics.print ("Color presets: ".. adjustr, xspace*2,crimsonRGBButton.y)
			love.graphics.print ("R: ".. adjustr, xspace*2,rplusButton.y)
			love.graphics.print ("G: ".. adjustg, xspace*2,gplusButton.y)
			love.graphics.print ("B: ".. adjustb, xspace*2,bplusButton.y)

	-- RGB
	--R
	local hovered = isButtonHovered (rplusButton)		-- Button to increase Red color
	drawButton (rplusButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Increase red" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=adjustr+0.1
	if adjustr>1 then adjustr=1 end
	end
	
	local hovered = isButtonHovered (rminusButton)		-- Button to reduce red color
	drawButton (rminusButton, hovered)
	--if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Decrease red" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=adjustr-0.1
	if adjustr<0 then adjustr=0 end
	end
	--G
	local hovered = isButtonHovered (gplusButton)		-- Button to increase green color
	drawButton (gplusButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Increase green" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustg=adjustg+0.1
	if adjustg>1 then adjustg=1 end
	end
	
	local hovered = isButtonHovered (gminusButton)		-- Button to reduce green color
	drawButton (gminusButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Decrease green" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustg=adjustg-0.1
	if adjustg<0 then adjustg=0 end
	end
	--R
	local hovered = isButtonHovered (bplusButton)		-- Button to increase blue color
	drawButton (bplusButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Increase blue" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustb=adjustb+0.1
	if adjustb>1 then adjustb=1 end
	end
	
	local hovered = isButtonHovered (bminusButton)		-- Button to reduce blue color
	drawButton (bminusButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Decrease blue" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustb=adjustb-0.1
	if adjustb<0 then adjustb=0 end
	end
	
	-- RGB presets
	
	local hovered = isButtonHovered (resetRGBButton)		-- Reset RGB colors
	drawButton (resetRGBButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Reset RGB values" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=1 adjustg=1 adjustb=1
	end
	
		local hovered = isButtonHovered (crimsonRGBButton)		-- RGB Preset "crimson red"
	drawButton (crimsonRGBButton, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="'Crimson red' color preset" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=0.86 adjustg=0.07 adjustb=0.23
	end
	
		local hovered = isButtonHovered (seagreenRGBButton)		-- RGB Preset "Sea green"
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered then graphmes1="'Sea green' color preset" graphmes2="" graphbuttonhovered=true end
	drawButton (seagreenRGBButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=0.18 adjustg=0.70 adjustb=0.34
	end
	
		local hovered = isButtonHovered (bluevioletRGBButton)		-- RGB Preset "Blue violet"
	drawButton (bluevioletRGBButton, hovered)
	if hovered then graphmes1="'Blue violet' color preset" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	adjustr=0.54 adjustg=0.16 adjustb=0.88
	end

end
