

graphicshubButton
= {
	text = "HUD",
	x = xspace*3,
	y = yspace*2,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

function DrawGraphicHub()
--HUB
	love.graphics.rectangle("line", Hud_Button.x,Hud_Button.y,150, 30)
	local hovered = isButtonHovered (graphicshubButton)
	drawButton (graphicshubButton, hovered)
	
	if hovered then graphmes1="HUD menu" graphmes2="toggles several data over the game" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		graphbuttonhovered=false
		bfocustab="graphicshub"
		bfocus="hub"
		currenttab="graphicshub"
		love.timer.sleep( 0.3 )
	end
	
end
