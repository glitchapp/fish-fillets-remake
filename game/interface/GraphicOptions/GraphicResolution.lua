
-- Resolution
	--[[
	
	This section of the code is responsible for handling the resolution and screen mode settings of the game. There are four buttons for different resolutions: 1k, QHD, 4k, and 8k, as well as two buttons for toggling between full screen and windowed modes.

The code first checks if a button is being hovered over by the mouse using the isButtonHovered function, and if so, it sets a message to be displayed on the screen. If the button is also clicked (love.mouse.isDown(1)), then it sets the resolution or screen mode accordingly using the changeresolution function and love.window.setMode function. A short timer is used after changing the settings to avoid potential issues with rapidly clicking buttons.

The dividx and dividy variables are used to adjust the aspect ratio of the game screen based on the chosen resolution, while the res variable keeps track of the current resolution setting.

--]]

resol720Button = {
	text = "720p",
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

resol1kButton = {
	text = "1080p",
	x = xspace*5,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

resolqhdButton = {
	text = "1440p",
	x = xspace*6,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

resol4kButton = {
	text = "4k",
	x = xspace*7,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

resol8kButton = {
	text = "8k",
	x = xspace*8,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawGraphicResolution()
drawGraphicTabs()
	love.graphics.rectangle("line", ResolutionTabButton.x,ResolutionTabButton.y,150, 30) 
	love.graphics.print ("Resolution: ", xspace*2,resol1kButton.y)
			love.graphics.print ("Fullscreen: ", xspace*2,fullscreenyesButton.y)
			love.graphics.setColor(1,0,1,0.7)
				if res=="720p" then love.graphics.rectangle("fill",resol720Button.x-5,resol720Button.y-5,200, 50)
    		elseif res=="1080p" then love.graphics.rectangle("fill",resol1kButton.x-5,resol1kButton.y-5,200, 50)
    		elseif res=="1440p" then love.graphics.rectangle("fill",resolqhdButton.x-5,resolqhdButton.y-5,200, 50)
			elseif res=="4k" then love.graphics.rectangle("fill",resol4kButton.x-5,resol4kButton.y-5,200, 50)
			elseif res=="8k" then love.graphics.rectangle("fill",resol8kButton.x-5,resol8kButton.y-5,200, 50)
			end
			
				if fullscreen==true then love.graphics.rectangle("fill",fullscreenyesButton.x-5,fullscreenyesButton.y-5,200, 50)
			elseif fullscreen==false then love.graphics.rectangle("fill",fullscreennoButton.x-5,fullscreennoButton.y-5,200, 50)
			end
	
	love.graphics.setColor(1,1,1,1)
   	local hovered = isButtonHovered (resol720Button)
	drawButton (resol720Button, hovered)
	if hovered then graphmes1="720p resolution" graphmes2="" end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			res="720p"
			changeresolution()	
			love.timer.sleep( 0.5 )
		end
	
	local hovered = isButtonHovered (resol1kButton)
	drawButton (resol1kButton, hovered)
	
	if hovered then graphmes1="1080p resolution" graphmes2="" end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			res="1080p"
			changeresolution()
			love.timer.sleep( 0.5 )
		end
	
		local hovered = isButtonHovered (resolqhdButton)
	drawButton (resolqhdButton, hovered)
	if hovered then graphmes1="QHD resolution" graphmes2="" end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			res="1440p"
			changeresolution()
			love.timer.sleep( 0.5 )
		end
	
	local hovered = isButtonHovered (resol4kButton)
	drawButton (resol4kButton, hovered)
	if hovered then graphmes1="4K resolution" graphmes2="" end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			res="4k"
			changeresolution()
			love.timer.sleep( 0.5 )
		end
	
	local hovered = isButtonHovered (resol8kButton)
	drawButton (resol8kButton, hovered)
	if hovered then graphmes1="8K resolution" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			res="8k"
			--loadmultiresolution()
			changeresolution()
			love.timer.sleep( 0.5 )
		end
end
