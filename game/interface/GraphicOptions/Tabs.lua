
SkinsTabButton = {
	text = "Skins",
	textPosition ="top",
	x = xspace*2,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}

ColorsTabButton = {
	text = "Colors",
	textPosition ="top",
	x = xspace*3,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}

RetroSchemeTabButton = {
	text = "Retro scheme",
	textPosition ="top",
	x = xspace*4,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}
	
ShadersTabButton = {
	text = "shaders",
	textPosition ="top",
	x = xspace*5,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}
	
PerformanceTabButton = {
	text = "Performance",
	textPosition ="top",
	x = xspace*6,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}
	
ResolutionTabButton = {
	text = "Resolution",
	textPosition ="top",
	x = xspace*7,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}

Hud_Button = {
	text = "Hud",
	textPosition ="top",
	x = xspace*8,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}
	
Alignements_Button = {
	text = "Alignements",
	textPosition ="top",
	x = xspace*9,
	y = yspace*2,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
	}



function drawGraphicTabs()
	local hovered = isButtonHovered (SkinsTabButton)
	drawButton (SkinsTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="skins"
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (ColorsTabButton)
	drawButton (ColorsTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="colors"
		love.timer.sleep( 0.3 )
	end

	local hovered = isButtonHovered (RetroSchemeTabButton)
	drawButton (RetroSchemeTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="retroscheme"
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (ShadersTabButton)
	drawButton (ShadersTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="shaders"
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (PerformanceTabButton)
	drawButton (PerformanceTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="performance"
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (ResolutionTabButton)
	drawButton (ResolutionTabButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="resolution"
		love.timer.sleep( 0.3 )
	end
	
	
	local hovered = isButtonHovered (Hud_Button)
	drawButton (Hud_Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="graphicshub"
		love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (Alignements_Button)
	drawButton (Alignements_Button, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		bfocus="graphicsAlignements"
		love.timer.sleep( 0.3 )
	end
end
