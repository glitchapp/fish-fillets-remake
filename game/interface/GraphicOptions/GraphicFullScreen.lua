

fullscreenyesButton = {
	text = "Yes",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

fullscreennoButton = {
	text = "No",
	x = xspace*5,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

function DrawGraphicFullScreen()
	-- Full screenmode
	local hovered = isButtonHovered (fullscreenyesButton)
	drawButton (fullscreenyesButton, hovered)
	if hovered then graphmes1="Full screen mode" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.timer.sleep( 0.3 )
				if res=="1080p" then dividx=1 dividy=1 		love.window.setMode(1920, 1080, {resizable=false, borderless=true})
			elseif res=="4k" then dividx=0.468 dividy=0.468 love.window.setMode(3840, 2160, {resizable=false, borderless=true})
			elseif res=="8k" then dividx=0.234 dividy=0.234 love.window.setMode(7680, 4320, {resizable=false, borderless=true})
			end

			love.timer.sleep( 1 )
		end
	
	-- Windowed mode
	local hovered = isButtonHovered (fullscreennoButton)
	drawButton (fullscreennoButton, hovered)
	if hovered then graphmes1="Windowed mode" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.timer.sleep( 0.3 )
					if res=="720p" then dividx=0.71 dividy=0.71 	love.window.setMode(1366, 768, {resizable=true, borderless=false})
				elseif res=="1080p" then dividx=1 dividy=1 	love.window.setMode(1920, 1080, {resizable=true, borderless=false})
			elseif res=="4k" then dividx=0.468 dividy=0.468 love.window.setMode(3840, 2160, {resizable=true, borderless=false})
			elseif res=="8k" then dividx=0.234 dividy=0.234 love.window.setMode(7680, 4320, {resizable=true, borderless=false})
			end
			love.timer.sleep( 1 )
		end
end
