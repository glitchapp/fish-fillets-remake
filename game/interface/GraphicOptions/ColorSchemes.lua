


--Color schemes
cscheme1Button = {
	text = "1",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme2Button = {
	text = "2",
	x = xspace*4+50,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme3Button = {
	text = "3",
	x = xspace*4+100,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme4Button = {
	text = "4",
	x = xspace*4+150,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme5Button = {
	text = "5",
	x = xspace*4+200,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme6Button = {
	text = "6",
	x = xspace*4+250,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

cscheme7Button = {
	text = "7",
	x = xspace*4+300,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


function DrawColorSchemes()
	drawGraphicTabs()
	love.graphics.rectangle("line", RetroSchemeTabButton.x,RetroSchemeTabButton.y,150, 30) 
			love.graphics.setColor(1,0,1,0.7)
			-- color schemes
		if palette==1 then love.graphics.rectangle("fill",cscheme1Button.x-5,cscheme1Button.y-5,40, 50)
    elseif palette==2 then love.graphics.rectangle("fill",cscheme2Button.x-5,cscheme2Button.y-5,40, 50)
    elseif palette==3 then love.graphics.rectangle("fill",cscheme3Button.x-5,cscheme3Button.y-5,40, 50)
    elseif palette==4 then love.graphics.rectangle("fill",cscheme4Button.x-5,cscheme4Button.y-5,40, 50)
    elseif palette==5 then love.graphics.rectangle("fill",cscheme5Button.x-5,cscheme5Button.y-5,40, 50)
    elseif palette==6 then love.graphics.rectangle("fill",cscheme6Button.x-5,cscheme6Button.y-5,40, 50)
    elseif palette==7 then love.graphics.rectangle("fill",cscheme7Button.x-5,cscheme7Button.y-5,40, 50)
    end
	love.graphics.setColor(1,1,1,1)
	
	love.graphics.print ("Retro scheme: ", xspace*2,cscheme1Button.y)
	--[[
	The first set of buttons are related to color schemes, and there are seven buttons defined with different color schemes. When a button is hovered over, a message is displayed on the screen using the graphmes1 and graphmes2 variables. If the left mouse button is clicked on a button, then the palette variable is set to a particular value and the skin variable is set to "classic" or "retro" depending on the button clicked. The skinhaschanged variable is also set to true indicating that the skin has changed.
	--]]
	-- Color schemes
	local hovered = isButtonHovered (cscheme1Button)
	drawButton (cscheme1Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="Set default color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=1 skin="classic" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme2Button)
	drawButton (cscheme2Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="2nd retro color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=2 skin="retro" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme3Button)
	drawButton (cscheme3Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then love.graphics.setFont(poorfish) love.graphics.print ("3rd retro color scheme", 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=3 skin="retro" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme4Button)
	drawButton (cscheme4Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="4th retro color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=4 skin="retro" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme5Button)
	drawButton (cscheme5Button, hovered)
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="6th retro color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=5 skin="retro" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme6Button)
	drawButton (cscheme6Button, hovered)
	--if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered then graphmes1="6th retro color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=6 skin="retro" skinhaschanged=true turnshadersoff()
	end
	
	local hovered = isButtonHovered (cscheme7Button)
	drawButton (cscheme7Button, hovered)
	if hovered then graphmes1="7th retro color scheme" graphmes2="" graphbuttonhovered=true end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
	love.timer.sleep( 0.3 )
	palette=7 skin="retro" skinhaschanged=true turnshadersoff()
	end
end
