

--Graphic profiles
lowprofileButton = {
	text = "Low",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

standardprofileButton = {
	text = "Standard",
	x = xspace*5,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

highprofileButton = {
	text = "High",
	x = xspace*6,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

function DrawGraphicProfiles()
 
 --[[
The second part of the code handles the graphic profiles. Three buttons are defined for low, standard, and high profile graphics. The isButtonHovered function is used to determine whether the mouse cursor is hovering over each button. If the cursor is hovering over a button, drawButton is called to display the button with a highlight effect.

If the cursor is hovering over the low profile button, a message is displayed explaining that this profile turns off Vsync and shaders, and sets the skin to "retro". If the user clicks the button with the left mouse button, Vsync and shaders are turned off, all effects are turned off, the skin is set to "retro", and a 0.5 second delay is added.
  
The same process is repeated for the standard and high profile buttons, with different messages and effects for each profile. For the standard profile, Vsync and caustic effects are turned on, and the skin is set to "remake". For the high profile, all effects are turned on and the skin is also set to "remake".
  --]]
    --Graphic profiles
    --Low profile
    	local hovered = isButtonHovered (lowprofileButton)		-- Low profile graphics
	drawButton (lowprofileButton, hovered)
	if hovered then 
		graphmes1="Sets Vsync and shaders off, set skin to 'retro'"
		graphmes2="Use this profile if you get bad performance"
		graphbuttonhovered=true
		
	else graphbuttonhovered=false
	end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		love.window.setVSync(false)	vsyncstatus=false			-- set vsync off
		shader1=false shader2=false chromaab=false caustics=false	-- set all effects off
		palette=2 skin="retro" skinhaschanged=true				-- set skin to "retro"
		love.timer.sleep( 0.5 )
	end
	 
	--standard profile
    	local hovered = isButtonHovered (standardprofileButton)		-- Standard profile graphics
	drawButton (standardprofileButton, hovered)
	if hovered then 
		graphmes1="Sets Vsync & Caustic on, sets chromatic aberration and CRT off. Set skin to 'remake'"
		graphmes2="Use this profile if you get good performance"
		graphbuttonhovered=true
	end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		vsyncstatus=false											-- set vsync off
		shader1=false shader2=true chromaab=false caustics=true		-- set caustic effects on
		palette=1 skin="remake" skinhaschanged=true					-- set skin to "remake"
		love.timer.sleep( 0.5 )
	end
	
		--High profile
    	local hovered = isButtonHovered (highprofileButton)		-- High profile graphics
	drawButton (highprofileButton, hovered)
	if hovered then 
		graphmes1="Sets Vsync,Caustic, CRT and chromatic effects on & set skin to 'remake'"
		graphmes2="Use this profile if you get very good performance"
		graphbuttonhovered=true
	end
	if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		love.window.setVSync(true)	vsyncstatus=true					-- set vsync on
		shader1=false shader2=true chromaab=false	caustics=true		-- set all effects on
		palette=1 skin="remake" skinhaschanged=true						-- set skin to "remake"
		love.timer.sleep( 0.5 )
	end
end
