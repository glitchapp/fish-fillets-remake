	--[[
This code controls two buttons, hdroffButton and hdronButton, which enable or disable high dynamic range (HDR) mode.

When either button is hovered over by the mouse cursor, the variable graphmes1 is set to a message indicating that the option is not yet implemented, and graphbuttonhovered is set to true.

If the mouse is clicked while hovering over either button, the hdr variable is set to true or false depending on which button was clicked. Then skinhaschanged is set to true, and reloadpreviewassets() function is called to reload the preview assets with the new setting. Finally, there is a delay of 0.3 seconds before continuing.
	--]]

hdronButton = {
	text = "",
	x = xspace*4,
	y = yspace*8,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

hdroffButton = {
	text = "",
	x = xspace*4,
	y = yspace*8,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


function DrawHDR()

		love.graphics.print ("HDR: ", xspace*2,hdronButton.y)

		-- hdr
		--The HDR buttons toggle between HDR on or off.
		if hdr==false then love.graphics.rectangle("fill",hdroffButton.x-5,hdroffButton.y-5,200, 50)
    elseif hdr==true then love.graphics.rectangle("fill",hdronButton.x-5,hdronButton.y-5,200, 50)
    end

		--hdr off
	if hdr==true then
		local hovered = isButtonHovered (hdroffButton)
		drawButton (hdroffButton, hovered)
		
		if hovered then graphmes1="This option is not working yet" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			hdr=false skinhaschanged=true
			reloadpreviewassets()
			switch_on_sound:play()
			love.timer.sleep( 0.3 )
		end
	elseif hdr==false then
	
		--hdr on
		local hovered = isButtonHovered (hdronButton)
		drawButton (hdronButton, hovered)
		
		if hovered then graphmes1="This option is not working yet" graphmes2="" graphbuttonhovered=true end
		if hovered and gamestatus=="gameplusoptions" then love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			hdr=true skinhaschanged=true
			reloadpreviewassets()
			switch_off_sound:play()
			love.timer.sleep( 0.3 )
		end
	end
end
