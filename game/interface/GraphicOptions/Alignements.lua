-- A temporary work around for the misalignements unresolved problems.

AlignObjectsUpButton
= {
	text = "Obj Up",
	x = xspace*2,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

AlignObjectsDownButton
= {
	text = "Obj Down",
	x = xspace*2,
	y = yspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

AlignBcksUpButton
= {
	text = "Bck Up",
	x = xspace*4,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

AlignBcksDownButton
= {
	text = "Bck Down",
	x = xspace*4,
	y = yspace*8,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

function DrawAlignementsButtons()

	love.graphics.print("Objects X" .. dividx,AlignObjectsUpButton.x+200,AlignObjectsUpButton.y,0,1)
	love.graphics.print("Background X" .. expx,AlignBcksUpButton.x+200,AlignBcksUpButton.y,0,1)

	
	local hovered = isButtonHovered (AlignObjectsUpButton)
	drawButton (AlignObjectsUpButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		dividx=dividx+0.005
		dividy=dividy+0.005
		love.timer.sleep( 0.1 )
	end
	
		local hovered = isButtonHovered (AlignObjectsDownButton)
	drawButton (AlignObjectsDownButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		dividx=dividx-0.005
		dividy=dividy-0.005
		love.timer.sleep( 0.1 )
	end
	
		local hovered = isButtonHovered (AlignBcksUpButton)
	drawButton (AlignBcksUpButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		expx=expx+0.005
		expy=expy+0.005
		love.timer.sleep( 0.1 )
	end
	
		local hovered = isButtonHovered (AlignBcksDownButton)
	drawButton (AlignBcksDownButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		expx=expx-0.005
		expy=expy-0.005
		love.timer.sleep( 0.1 )
	end
end
