
function optionsloadlanguages()


langlist={"bg","cs","de","eo","es","fr","en","it","ko","nl","pl","pt","ru","sv","sk","jp","chi","thai","tamil","hindi"}
			--			1			2		3		4			5			6		7			8		9		10			11		12			13		14			15			16		17			18		19		20
langlistlanguagename={"Bulgarian","Czech","German","Esperanto","Spanish","French","English","Italian","Korean","Dutch","Polish","Portuguese","Russian","Swedish","Slovenian","Japanese","Chinese","Thai","Tamil","Hindi"}
returntext="Return" bgt="Bulgarian" brt="Brazil" cht="Switzerland" cst="Czech" det="German" dedet="Germany" eot="Esperanto" est="Spanish" esest="Spain" eslat="Lateinamerica" frt="French" frfrt="France" frcat="Canada" ent="English" entbr="British" entus="American"itt="Italian" kot="Korean" nlt="Dutch" nlnlt="Niederland" flt="Flemish" plt="Polish" prt="Portuguese" prprt="Portugal" rut="Russian" set="Swedish" skt="Slovenian" jpt="Japanese" chit="Chinese" thait="Thai" tamilt="Tamil" hindit="Hindi" ont="on" offt="off" readdiatext="read" closediatext="close" skinremaket="remake" skinretrot="retro" skinclassict="classic" skinclassicnormalt="Normal" skinclassicupscaledt="Upscaled"

	if language=="bg" then langlistnumber=1
elseif language=="br" then 
elseif language=="ch" then 
elseif language=="cs" then langlistnumber=2
elseif language=="de" then langlistnumber=3 returntext="zurück" bgt="Bulgarisch" brt="Portugiesisch (Brasilien)" cht="der Schweiz" cst="Tschechisch" det="Deutsch" dedet="Deutschland" eot="Esperanto" est="Spanisch" frt="Französisch" ent="Englisch" itt="Italienisch" nlt="Niederländisch" plt="Polisch" prt="Portugiesisch" rut="Russisch" set="Schwedisch" skt="Slowenisch" thait="thailändisch" tamilt="Tamil" hindit="Hindi"
elseif language=="eo" then langlistnumber=4
elseif language=="es" then langlistnumber=5 returntext="Volver" bgt="Bulgaro" brt="Portugés (Brasil)" cht="Alemán (Suiza)" cst="Checo" det="Alemán" eot="Esperanto" est="Español" frt="Francés" ent="Inglés" itt="Italiano" nlt="Holandés" plt="Polaco" prt="Portugés" rut="Ruso" set="Sueco" skt="Esloveno" thait="Tailandés" tamilt="Tamil" ont="on" offt="off" readdiatext="leer" closediatext="cerrar"
elseif language=="fr" then langlistnumber=6
elseif language=="en" then langlistnumber=7
elseif language=="it" then langlistnumber=8
elseif language=="ko" then langlistnumber=9 returntext="반품" bgt="불가리아 사람" brt="브라질 사람" cht="스위스" cst="체코 사람" det="독일 사람" eot="에스페란토 말" est="스페인의" frt="프랑스 국민" ent="영어" entbr="영국" entus="미국 사람" itt="이탈리아 사람" nlt="네덜란드 사람" plt="광택" prt="포르투갈 인" rut="러시아인" set="스웨덴어" skt="슬로베니아" jpt="일본어" chit="중국인" thait="태국어" tamilt="타밀 사람" hindit="힌디 어" ont="켜기" offt="끄기"
elseif language=="nl" then langlistnumber=10
elseif language=="pl" then langlistnumber=11
elseif language=="pt" then langlistnumber=12
elseif language=="ru" then langlistnumber=13
elseif language=="sv" then langlistnumber=14
elseif language=="sk" then langlistnumber=15
elseif language=="jp" then langlistnumber=16 returntext="帰ります" bgt="ブルガリア語" brt="ポルトガル語 (ブラジル)" cht="ドイツ語 (スイス)" cst="チェコ" det="ドイツ語" eot="エスペラント" est="スペイン語" frt="フランス語" ent="英語" entbr="英国の" entus="アメ" itt="イタリア語" nlt="オランダ語" plt="ポーランド語" prt="ポルトガル語" rut="ロシア語" set="スウェーデン語" skt="スロベニア" jpt="日本語" chit="中国語" thait="タイ語" tamilt="タミル語" hindit="ヒンディー語" ont="オン" offt="オフ"
elseif language=="chi" then langlistnumber=17 returntext="回传" bgt="Bulgarian" brt="葡萄牙语 （巴西" cht="德语（瑞士" cst="Czech" det="德语" eot="世界语" est="Spanish" frt="法语" ent="英语" entbr="英国的" entus="American"itt="意大利语" nlt="荷兰语" plt="Polish" prt="Portuguese" rut="Russian" set="Swedish" skt="斯洛文尼亚语" jpt="日语" chit="Chinese" thait="泰国" tamilt="泰米尔语" hindit="印地语" ont="开" offt="关" readdiatext="看" closediatext="关闭"
elseif language=="thai" then langlistnumber=18 returntext="กลับ" bgt="บัลแกเรีย" brt="โปรตุเกส (บราซิล)" cht="เยอรมัน (สวิตเซอร์แลนด์)" cst="เช็ก" det="เยอรมัน" eot="ภาษาเอสเปรันโต" est="สเปน" frt="ภาษาฝรั่งเศส" ent="ภาษาอังกฤษ" entbr="อังกฤษ" entus="อเมริกัน" itt="ภาษาอิตาลี" nlt="ดัตช์" plt="ขัด" prt="โปรตุเกส" rut="รัสเซีย" set="ภาษาสวีเดน" skt="สโลวีเนีย" jpt="ญี่ปุ่น" chit="ชาวจีน" thait="แบบไทย" tamilt="ทมิฬ" hindit="ภาษาฮินดี" ont="บน" offt="ปิด"
elseif language=="tamil" then langlistnumber=19 returntext="திரும்ப" bgt="பல்கேரிய" brt="போர்த்துகீசியம் (பிரேசில்)" cht="ஜெர்மன் (சுவிட்சர்லாந்து)" cst="செக்" det="ஜெர்மன்" eot="எஸ்பெராண்டோ" est="ஸ்பானிஷ்" frt="பிரெஞ்ச" ent="ஆங்கிலம்" entbr="பிரிட்டிஷ்" entus="அமெரிக்கன்" itt="இத்தாலிய" nlt="டச்ச" plt="போலிஷ்" prt="போர்த்துகீசியம்" rut="ரஷ்யன்" set="ஸ்வீடிஷ்" skt="ஸ்லோவேனியன்" jpt="ஜப்பானியர்" chit="சீன" thait="தாய்" tamilt="தமிழ்" hindit="ஹிந்தி" ont="ஆன் " offt="ஆஃப்"
elseif language=="hindi" then langlistnumber=20 returntext="वापस करना" bgt="बल्गेरियाई" brt="पुर्तगाली (ब्राजील)" cht="जर्मन (स्विट्जरलैंड)" cst="चेक" det="जर्मन" eot="एस्पेरांत" est="स्पैनिश" frt="फ्रेंच" ent="अंग्रेज़ी" entbr="अंग्रेजों" entus="अमेरिकन" itt="इतालव" nlt="डच" plt="पोलिश" prt="पुर्तगाली" rut="रूसी" set="स्वीडिश" skt="स्लोवेनियाई" jpt="जापानी" chit="चीनी" thait="थाई" tamilt="तामिल" hindit="हिन्द" ont="पर" offt="बंद" 

else returntext="Volver" bgt="Bulgarian" brt="Portuguese (Brazil)" cht="German (Switzerland)" cst="Czech" det="German" eot="Esperanto" est="Spanish" frt="French" ent="English" itt="Italian" nlt="Dutch" plt="Polish" prt="Portuguese" rut="Russian" set="Swedish" skt="Slovenian"

end

-- Focus up arrow
LanguageFocusUpButton = {
	text = "^",
	x = 1000,
	y = 150,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Focus down arrow
LanguageFocusDownButton = {
	text = "v",
	x = 1000,
	y = 450,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Bulgarian
-- line 1
bgButton = {
	text = bgt,
	x = xspace*2,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

daButton = {
	text = "Danish",
	x = xspace*3,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Czesch
csButton = {
	text = cst,
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Dutch
nlButton = {
	text = nlt,
	x = xspace*5,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}



--line 2

-- English
enButton = {
	text = ent,
	x = xspace*2,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
-- Esperanto
eoButton = {
	text = eot,
	x = xspace*3,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Finnish
fiButton = {
	text = "Finnish",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


-- French
frButton = {
	text = frt,
	x = xspace*5,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--line 3


-- German
deButton = {
	text = det,
	x = xspace*2,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Greek
grButton = {
	text = "Greek",
	x = xspace*3,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- icelandic
isButton = {
	text = "Icelandic",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Italian
itButton = {
	text = itt,
	x = xspace*5,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--line 4

-- Norwegian
noButton = {
	text = "Norwegian",
	x = xspace*2,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Polish
plButton = {
	text = plt,
	x = xspace*3,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Portuguese
prButton = {
	text = prt,
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Russian
ruButton = {
	text = rut,
	x = xspace*5,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--line 5


--line 6
-- Slovenian
skButton = {
	text = skt,
	x = xspace*2,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
--Swedish
seButton = {
	text = set,
	x = xspace*3,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
--Spanish
esButton = {
	text = est,
	x = xspace*4,
	y = yspace*7,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--acents

-- Britísh
enbrButton = {
	text = entbr,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
-- American
enusButton = {
	text = entus,
	x = xspace*4,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

brButton = {
	text = brt,
	x = xspace*6,
	y = yspace*12*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- High German
dedeButton = {
	text = dedet,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Swiss
chButton = {
	text = cht,
	x = xspace*4,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Spanish (Spain)
esesButton = {
	text = esest,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Latin american Spanish
eslaButton = {
	text = eslat,
	x = xspace*4,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- French (France)

frfrButton = {
	text = frfrt,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- French (Canada)

frcaButton = {
	text = frcat,
	x = xspace*4,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Dutch

nlnlButton = {
	text = nlnlt,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


-- Dutch (Flemmish)
flButton = {
	text = flt,
	x = xspace*4,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Portuguese (Portugal)
prprButton = {
	text = prprt,
	x = xspace*3,
	y = yspace*13,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


-- Asian languages

-- Chinese
chiButton = {
	text = chit,
	x = xspace*2,
	y = yspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--Hindi
hindiButton = {
	text = hindit,
	x = xspace*3,
	y = yspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--Japanese
jpButton = {
	text = jpt,
	x = xspace*4,
	y = yspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Tamil
tamilButton = {
	text = tamilt,
	x = xspace*5,
	y = yspace*9,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Thai
thaiButton = {
	text = thait,
	x = xspace*2,
	y = yspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

--Korean
koreanButton = {
	text = kot,
	x = xspace*3,
	y = yspace*10,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

-- Voice actor selection

AaronButton = {
	text = "Aaron Relf",
	x = xspace*3,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

JohnButton = {
	text = "John Combs",
	x = xspace*3,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

lunarwingzButton = {
	text = "Lunarwingz",
	x = xspace*3,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

labyrinthusButton = {
	text = "Labyrinthus",
	x = xspace*3,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

vnaiisButton = {
	text = "Vn_aiis",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


bastianButton = {
	text = "Bastian Villaroel",
	x = xspace*3,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

ishVAButton = {
	text = "ishVA",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}


snoozyButton = {
	text = "Snoozy",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

rosieButton = {
	text = "Rosie Krijgsman",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

nelakoriButton = {
	text = "Nelakori",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

zeroSkyButton = {
	text = "ZeroSky",
	x = xspace*4,
	y = yspace*11,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

if res=="1080p" or res=="1440p" then			-- reduce font below 4k
	bgButton.font=poorfishsmall
	daButton.font=poorfishsmall
	csButton.font=poorfishsmall
	nlButton.font=poorfishsmall
	enButton.font=poorfishsmall
	eoButton.font=poorfishsmall
	frButton.font=poorfishsmall
	deButton.font=poorfishsmall
	hindiButton.font=poorfishsmall
	itButton.font=poorfishsmall
	noButton.font=poorfishsmall
	plButton.font=poorfishsmall
	prButton.font=poorfishsmall
	ruButton.font=poorfishsmall
	skButton.font=poorfishsmall
	seButton.font=poorfishsmall
	esButton .font=poorfishsmall
	fiButton .font=poorfishsmall
	isButton .font=poorfishsmall
	enbrButton.font=poorfishsmall
	enusButton.font=poorfishsmall
	brButton.font=poorfishsmall
	dedeButton.font=poorfishsmall
	chButton.font=poorfishsmall
	esesButton.font=poorfishsmall
	eslaButton.font=poorfishsmall
	frfrButton.font=poorfishsmall
	frcaButton.font=poorfishsmall
	grButton.font=poorfishsmall
	nlnlButton.font=poorfishsmall
	flButton.font=poorfishsmall
	prprButton.font=poorfishsmall
	chiButton.font=poorfishsmall
	tamilButton.font=poorfishsmall
	jpButton.font=poorfishsmall
	thaiButton.font=poorfishsmall
	koreanButton.font=poorfishsmall
	AaronButton.font=poorfishsmall
	JohnButton.font=poorfishsmall
	lunarwingzButton.font=poorfishsmall
	labyrinthusButton.font=poorfishsmall
	vnaiisButton.font=poorfishsmall
	bastianButton.font=poorfishsmall
	ishVAButton.font=poorfishsmall
	snoozyButton.font=poorfishsmall
	rosieButton.font=poorfishsmall
	nelakoriButton.font=poorfishsmall
	zeroSkyButton.font=poorfishsmall
end
end

function optionsdrawlanguagesfocus(dt)

		love.graphics.setFont( poorfishsmall)
		if not (langlistlanguagename[langlistnumber-1]==nil) then
			love.graphics.print(langlistlanguagename[langlistnumber-1],1000,200,0,1)
		end
		love.graphics.setFont( poorfish)
		if not (langlistlanguagename[langlistnumber]==nil) then
			love.graphics.print(langlistlanguagename[langlistnumber],1000,300,0,1)
		end
		love.graphics.setFont( poorfishsmall)
		if not (langlistlanguagename[langlistnumber+1]==nil) then
			love.graphics.print(langlistlanguagename[langlistnumber+1],1000,400,0,1)
		end

end


function optionsdrawlanguages(dt)



			love.graphics.print ("Actors: ", xspace*2,yspace*11) 

			if language=="es" then love.graphics.print ("Acento: ", 350,yspace*13)
		elseif language=="pr" then love.graphics.print ("Accent: ", 350,yspace*13)
		elseif language=="de" then love.graphics.print ("Akzent: ", 350,yspace*13)
		elseif language=="nl" then love.graphics.print ("Accent: ", 350,yspace*13)
		elseif language=="ru" then love.graphics.print ("язык: ", 350,yspace*13)
		elseif language=="jp" then love.graphics.print ("言語: ", 350,yspace*13)
		elseif language=="chi" then love.graphics.print ("语: ", 350,yspace*13)
        elseif language=="thai" then love.graphics.print ("ภาษา: ", 350,yspace*13)
    	elseif language=="ko" then love.graphics.print ("언어: ", 350,yspace*13)
        elseif language=="tamil" then love.graphics.print ("மொழி: ", 350,yspace*13)
        elseif language=="hindi" then love.graphics.print ("भाषा: हिन्द: ", 350,yspace*1)
			
			
			love.graphics.print ("Touch controls: ", touchonButton.x-200,touchonButton.y)
			
			
			love.graphics.print ("Audio: ", 800,550)
			love.graphics.print ("Controls: ", vibrationonButton.x-200,vibrationonButton.y-50)
			love.graphics.print ("Vibration: ", vibrationonButton.x-200,vibrationonButton.y)  
		else
			
			love.graphics.print ("Accent: ", 350,yspace*13)
			
				
		end
		
		--rectangleoverlanguage()
    
    love.graphics.print ("Language: ", 350,300)
    	
-- Focus up arrow
local hovered = isButtonHovered (LanguageFocusUpButton)
	drawButton (LanguageFocusUpButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				
		if not (langlistnumber<2) then
			language=langlist[langlistnumber-1]
			language2=langlist[langlistnumber-1]
			love.timer.sleep( 0.3 )
			languagehaschanged=true
			optionsload()
		end
	end
	
	-- Focus down arrow
local hovered = isButtonHovered (LanguageFocusDownButton)
	drawButton (LanguageFocusDownButton, hovered)
	if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) then 
		if not (langlistnumber>19) then
			language=langlist[langlistnumber+1]
			language2=langlist[langlistnumber+1]
			love.timer.sleep( 0.3 )
			languagehaschanged=true
			optionsload()
		end
	end
	
optionsdrawlanguagesfocus(dt)
selectaccent()

printactors()

end

function selectaccent()

	if language=="fr" and not (accent=="fr" or accent=="ca") then accent="fr" accent2="fr"
elseif language=="en" and not (accent=="br" or accent=="us") then accent="us" accent2="us" 
elseif language=="de" and not (accent=="de" or accent=="ch") then accent="de" accent2="de" 
elseif language=="es" and not (accent=="es" or accent=="la") then accent="la" accent2="la"
elseif language=="nl" and not (accent=="nl" or accent=="fr") then accent="nl" accent2="nl"
end

		-- accent highlight button
		love.graphics.setColor(1,1,1,0.3)
		
		
		if language=="de" then
				if accent=="de" then	love.graphics.rectangle("fill",dedeButton.x-5,dedeButton.y-5,150, 30) 
				end
				require("assets/text/levelmenu/level-de")
		end
		
		if language=="en" then
				if accent=="br" then	love.graphics.rectangle("fill",enbrButton.x-5,enbrButton.y-5,200, 50)
			elseif accent=="us" then	 love.graphics.rectangle("fill",enusButton.x-5,enusButton.y-5,200, 50)
			end
			require("assets/text/levelmenu/level-en")
		end
		
		if language=="es" then
				if accent=="es" then love.graphics.rectangle("fill",esesButton.x-5,esesButton.y-5,200, 50)
			elseif accent=="la" then love.graphics.rectangle("fill",eslaButton.x-5,eslaButton.y-5,200, 50)
			end
			require("assets/text/levelmenu/level-es")
		end
		
		if language=="fr" then
				if accent=="fr" then love.graphics.rectangle("fill",frfrButton.x-5,frfrButton.y-5,200, 50)
			elseif accent=="ca" then love.graphics.rectangle("fill",frcaButton.x-5,frcaButton.y-5,200, 50)
			end
			require("assets/text/levelmenu/level-fr")
		end
	
		if language=="nl" then
				if accent=="nl" then love.graphics.rectangle("fill",nlnlButton.x-5,nlnlButton.y-5,200, 50)
			elseif accent=="fl" then
			end
			require("assets/text/levelmenu/level-nl")
		end

		
		if language=="pt" then
				if accent=="pt" then love.graphics.rectangle("fill",prprButton.x-5,enbrButton.y-5,200, 50)
			elseif accent=="br" then
			end
		end
		
		--if language2=="pl" then love.graphics.rectangle("fill",plButton.x-5,plButton.y-5,200, 50) end
		
		love.graphics.setColor(1,1,1,1)

	if language=="de" then
		local hovered = isButtonHovered (chButton)
		drawButton (chButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="de" accent="ch" language2="de" accent2="ch" languagehaschanged=true
			require("assets/text/levelmenu/level-de")
			optionsload()
		end
	
		local hovered = isButtonHovered (dedeButton)
		drawButton (dedeButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="de" accent="de" language2="de" accent2="de" languagehaschanged=true
			require("assets/text/levelmenu/level-de")
			optionsload()
		end
	end
	
	if language=="fr" then
		local hovered = isButtonHovered (frfrButton)
		drawButton (frfrButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="fr" accent="fr" language2="fr" accent2="fr" languagehaschanged=true
			optionsload()
		end
	
	
		local hovered = isButtonHovered (frcaButton)
		drawButton (frcaButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="fr" accent="ca" language2="fr" accent2="ca"
			--require("assets/text/levelmenu/level-fr")
			optionsload()
		end
	end
	
	if language=="en" then
		local hovered = isButtonHovered (enbrButton)
		drawButton (enbrButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="en" accent="br" language2="en" accent2="br" languagehaschanged=true
			require("assets/text/levelmenu/level-en")
			optionsload()
		end
	
	
		local hovered = isButtonHovered (enusButton)
		drawButton (enusButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="en" accent="us" language2="en" accent2="us" languagehaschanged=true
			require("assets/text/levelmenu/level-en")
			optionsload()
		end
	end
	
	if language=="es" then
		local hovered = isButtonHovered (esesButton)
		drawButton (esesButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="es" accent="es" language2="es" accent2="es" languagehaschanged=true
			require("assets/text/levelmenu/level-es")
			optionsload()
		end
	
		local hovered = isButtonHovered (eslaButton)
		drawButton (eslaButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			love.timer.sleep( 0.3 )
			language="es" accent="la" language2="es" accent2="la" languagehaschanged=true
			require("assets/text/levelmenu/level-es")
			optionsload()
		end
	end

		
end

function printactors()

	if language=="de" then
		if accent=="de" then
				-- lunarwingz
				local hovered = isButtonHovered (lunarwingzButton)
				drawButton (lunarwingzButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						language="de"
				end
			end
	
	elseif language=="en" then
	    -- Voice actors
		if accent=="br" then 
			-- Aaron
			local hovered = isButtonHovered (AaronButton)
			drawButton (AaronButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="us"
			end
		elseif accent=="us" then
			-- John
			local hovered = isButtonHovered (JohnButton)
			drawButton (JohnButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="br"
			end
		end

	elseif language=="es" then
		if accent=="es" then 
		elseif accent=="la" then
		-- Bastián
			local hovered = isButtonHovered (bastianButton)
			drawButton (bastianButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent="la"
			end
		end
	elseif language=="fr" then
			if accent=="fr" then
				-- Labyrinthus
				local hovered = isButtonHovered (labyrinthusButton)
				drawButton (labyrinthusButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent="fr"
				end
				
			
		
    end
    end
    
    if language2=="en" then
		if accent2=="br" then 
				-- isVA
			local hovered = isButtonHovered (ishVAButton)
			drawButton (ishVAButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="us"
			end
		elseif accent2=="us" then
				-- Snoozy
			local hovered = isButtonHovered (snoozyButton)
			drawButton (snoozyButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="br"
			end
		end
	elseif language2=="pl" then
		-- Nelakori
			local hovered = isButtonHovered (nelakoriButton)
			drawButton (nelakoriButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="pl"
			end
	elseif language2=="es" then 
			if accent2=="es" then 
		elseif accent2=="la" then
		-- zeroSky
			local hovered = isButtonHovered (zeroSkyButton)
			drawButton (zeroSkyButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				love.timer.sleep( 0.3 )
					accent2="la"
			end
		end
	elseif language2=="nl" then 
			if accent2=="nl" then
					-- Rosie Krijgsman
				local hovered = isButtonHovered (rosieButton)
				drawButton (rosieButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent2="nl"
				end
			end
	elseif language2=="fr" then
			if accent2=="fr" then
				-- vn_aiis
				local hovered = isButtonHovered (vnaiisButton)
				drawButton (vnaiisButton, hovered)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					love.timer.sleep( 0.3 )
						accent2="fr"
				end
			end
	end

end
