	LogoAnimTransitionTime=0
	LogoAnimTransitionDuration = 3 -- Adjust the duration as needed
	inTransitionLogoAnim = true
	FFLogoframe=0
	transitionTime=0
	isLogoLoaded=false
	loadingTransitionAnimation=0
	
	function drawLoadingWarning()
		love.graphics.setColor(0.5,0.5,0.5,0.5)
		love.graphics.rectangle("fill",400*scalefactor, 500*scalefactor,1300,100)
		love.graphics.setFont(poorfish)
		love.graphics.setColor(1,1,1,1)
		love.graphics.print ("Loading transition animation, please wait...", 400*scalefactor, 500*scalefactor,0,1)
	end
	
	function LoadLogoAnimation()
	
	if not loadedAssets["externalassets/video/FFLogo/0-1.webp"] and isLogoLoaded==false then
	isLogoLoaded=true
	--logo Animation
	FFLogoFrame01					=  loadAsset("externalassets/video/FFLogo/0-1.webp", "Image")
	FFLogoFrame02					=  loadAsset("externalassets/video/FFLogo/0-2.webp", "Image")
	FFLogoFrame03					=  loadAsset("externalassets/video/FFLogo/0-3.webp", "Image")
	FFLogoFrame04					=  loadAsset("externalassets/video/FFLogo/0-4.webp", "Image")
	FFLogoFrame05					=  loadAsset("externalassets/video/FFLogo/0-5.webp", "Image")
	FFLogoFrame06					=  loadAsset("externalassets/video/FFLogo/0-6.webp", "Image")
	
	FFLogoFrame1					=  loadAsset("externalassets/video/FFLogo/1.webp", "Image")
	FFLogoFrame2					=  loadAsset("externalassets/video/FFLogo/2.webp", "Image")
	FFLogoFrame3					=  loadAsset("externalassets/video/FFLogo/3.webp", "Image")
	FFLogoFrame4					=  loadAsset("externalassets/video/FFLogo/4.webp", "Image")
	FFLogoFrame5					=  loadAsset("externalassets/video/FFLogo/5.webp", "Image")
	FFLogoFrame6					=  loadAsset("externalassets/video/FFLogo/6.webp", "Image")
	
	--interpolated
	
	FFLogoFrame01i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-1.webp", "Image")
	FFLogoFrame02i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-2.webp", "Image")
	FFLogoFrame03i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-3.webp", "Image")
	FFLogoFrame04i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-4.webp", "Image")
	FFLogoFrame05i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-5.webp", "Image")
	FFLogoFrame06i					=  loadAsset("externalassets/video/FFLogo/interpolated/0-6.webp", "Image")
	
	FFLogoFrame1i					=  loadAsset("externalassets/video/FFLogo/interpolated/1.webp", "Image")
	FFLogoFrame2i					=  loadAsset("externalassets/video/FFLogo/interpolated/2.webp", "Image")
	FFLogoFrame3i					=  loadAsset("externalassets/video/FFLogo/interpolated/3.webp", "Image")
	FFLogoFrame4i					=  loadAsset("externalassets/video/FFLogo/interpolated/4.webp", "Image")
	FFLogoFrame5i					=  loadAsset("externalassets/video/FFLogo/interpolated/5.webp", "Image")
	FFLogoFrame6i					=  loadAsset("externalassets/video/FFLogo/interpolated/6.webp", "Image")
	end
	end
	

function preloadTransitionAnimation()
	if isLogoLoaded==false then		-- this conditional cheks if the transition animation is loaded
			if level==8					-- if it is, it loads it when hovering the end of area levels
				or level==19
				or level==29
				or level==37 
				or level==44
				or level==51
				or level==58
				or level==64
				or level==70
				or level==79
			then
			
						if loadingTransitionAnimation==0 then	-- this condition will be checked in game/states/LevelDraw to draw a message
							loadingTransitionAnimation=1		-- indicating that the assets is being loaded
					elseif loadingTransitionAnimation==1 then
							if inputtime>0.01 then
								LoadLogoAnimation()
								loadingTransitionAnimation=2
							end
					end
					
			end
		end
end
