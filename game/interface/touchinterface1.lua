function loadtouchtext()


panelright = {
	text = "panelright",
	x = 1000,
	y = 400, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
if res=="1080p" then
readdialogs = {
	text = readdiatext,
	x = 1000,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

clossedialogs = {
	text = closediatext,
	x = 1200,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
elseif res=="1440p" then
readdialogs = {
	text = readdiatext,
	x = 1400,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

clossedialogs = {
	text = closediatext,
	x = 1600,
	y = 1300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}
end
switchfish1 = {
	text = "switchfish1",
	x = 100,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

switchfish2 = {
	text = "switchfish2",
	x = 300,
	y = 900, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

moveleft = {
	text = "right",
	x = 1200,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

moveright = {
	text = "left",
	x = 1080,
	y = 800, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

moveup = {
	text = "up",
	x = 1150,
	y = 700, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

movedown = {
	text = "down",
	x = 1150,
	y = 880, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfish,
}

end

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end


local function drawButton (button, hovered)
	
	love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)
end

local function drawicon (button, hovered)
	
	love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w/5, button.h)
	else
		love.graphics.setColor(button.color)
	end
	--love.graphics.draw(icongraph,button.x,button.y,button.r,button.sx/10)
end


function drawtouchinterface()

if touchinterfaceison==true then 
	if panelright==true then				 --panel right show / hide 
		gui21:play() 
		love.timer.sleep( 0.3 )  
		panelright=false 
	elseif  panelright==false and touchinterfaceison==true then
		gui24:play()
		love.timer.sleep( 0.3 )
		panelright=true 
	end
end
--[[
local hovered = isButtonHovered (panelright)
		if touchinterfaceison==true then drawicon (panelright, hovered)
			if panelright==true then
				if hovered and love.mouse.isDown(1) then 
					TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
					love.timer.sleep( 0.3 )
					panelright=false
				elseif panelright==false then
					love.timer.sleep( 0.3 )
					panelright=true
				end	
			end
		end
--]]
local hovered = isButtonHovered (moveleft)
		if touchinterfaceison==true then drawicon (moveleft, hovered)
				if hovered and love.mouse.isDown(1) then 
							pb:mainMoving (1, 0)
					love.timer.sleep( 0.3 )
			end
		end


local hovered = isButtonHovered (moveright)
		if touchinterfaceison==true then drawicon (moveright, hovered)
				if hovered and love.mouse.isDown(1) then 
						pb:mainMoving (-1, 0)
					love.timer.sleep( 0.3 )
			end
		end
		
		
local hovered = isButtonHovered (moveup)
		if touchinterfaceison==true then drawicon (moveup, hovered)
				if hovered and love.mouse.isDown(1) then 
						pb:mainMoving (0, -1)
					love.timer.sleep( 0.3 )
			end
		end
		
local hovered = isButtonHovered (movedown)
		if touchinterfaceison==true then drawicon (movedown, hovered)
				if hovered and love.mouse.isDown(1) then 
						pb:mainMoving (0, 1)
					love.timer.sleep( 0.3 )
			end
		end

local hovered = isButtonHovered (switchfish1)
		if touchinterfaceison==true then drawicon (switchfish1, hovered)
				if hovered and love.mouse.isDown(1) then 
						pb:switchAgent ()
					love.timer.sleep( 0.3 )
			end
		end

local hovered = isButtonHovered (switchfish2)
		if touchinterfaceison==true then drawicon (switchfish2, hovered)
				if hovered and love.mouse.isDown(1) then 
						pb:switchAgent ()
					love.timer.sleep( 0.3 )
			end
		end

--[[
if talkies==true then

local hovered = isButtonHovered (readdialogs)
	drawButton (readdialogs, hovered)
	if hovered and love.mouse.isDown(1) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		Talkies.onAction()
		love.timer.sleep( 0.3 )
	end

local hovered = isButtonHovered (clossedialogs)
	drawButton (clossedialogs, hovered)
	if hovered and love.mouse.isDown(1) then 
		TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		talkies=false

		love.timer.sleep( 0.3 )
	end
end
--]]
if touchinterfaceison==true then 						--Draw Touch controls if enabled

    if talkies==true then										--contrast during dialogs
      if ispaneldown==false then
          love.graphics.draw(fishtouch1,50,900,0,0.8)
          love.graphics.draw(fishtouch2,250,900,0,0.8)
      end
      
      love.graphics.draw(contcontrast,1000,700,0,0.8)				
    elseif talkies==false then
      if ispaneldown==false then
			love.graphics.setColor(1,1,1,0.8)
			--love.graphics.rectangle('fill',50,900,190,90)
			--love.graphics.rectangle('fill',250,900,190,90)
			
            love.graphics.draw(fishtouch1,50,900,0,0.8)					-- Fish 1 selection
            love.graphics.draw(fishtouch2,250,900,0,0.8)				-- Fish 2 selection
      end
      	love.graphics.circle('fill',1095,860,42)
      	love.graphics.circle('fill',1220,860,42)
      	love.graphics.circle('fill',1158,798,42)
      	love.graphics.circle('fill',1158,920,42)
      love.graphics.draw(touchcontrolstrans,1000,700,0,0.8)			-- arrows
    end
end
  if panelright==true and touchinterfaceison==true then
       
    love.graphics.setColor(0.5,0.5,0.5,0.8)
	--love.graphics.rectangle('fill',1140,30,100,600)
       
    love.graphics.draw(confcontrol1,1150,50,0,0.1)				-- Options menu
    love.graphics.draw(levelcontrol1,1150,150,0,0.1)		-- screen options
    
    love.graphics.draw(screenoptionsicon,1150,250,0,0.15)		-- screen options
    
    love.graphics.draw(touchicon,1165,320,0,0.10)		-- touch interface
  
    love.graphics.draw(hidepanelright,1165,400,0,0.10)		-- hide panel right
    
    --love.graphics.draw(netcontrols,1140,480,0,0.16)		-- network controls
    
  elseif panelright==false and touchinterfaceison==true then

    
    love.graphics.setColor(0.5,0.5,0.5,0.8)
	love.graphics.rectangle('fill',1165,400,75,75)
    love.graphics.draw(showpanelright,1165,400,0,0.10)		-- show panel right
  end
    
    if ispaneldown==true and talkies==false and touchinterfaceison==true then
    love.graphics.setColor(0.5,0.5,0.5,0.8)
	love.graphics.rectangle('fill',50,900,600,100)
       
       love.graphics.draw(panelup,500,900,0,0.10)		-- hide bottom panel
      if talkies==false and touchinterfaceison==true then
        love.graphics.draw(opendialogs,200,900,0,0.20)					-- Open dialogs
        love.graphics.setColor(0,0,0)
        love.graphics.print("dialogs",200,950,0,1)
        love.graphics.setColor(1,1,1)
      end
      
      if soundon==true and talkies==false then
        love.graphics.draw(soundonicon,300,900,0,0.10)		-- turn sound off
      elseif soundon==false then
        love.graphics.draw(soundoff,300,900,0,0.10)		-- turn sound off
      end
      
      if musicison==true and talkies==false then
        love.graphics.draw(musiconicon,400,900,0,0.10)		-- turn music off
      elseif musicison==false and talkies==false then
        love.graphics.draw(musicoff,400,900,0,0.10)		-- turn music off
      end
      
      
     elseif ispaneldown==false and talkies==false and touchinterfaceison==true then
        
           love.graphics.setColor(0.5,0.5,0.5,0.8)
		love.graphics.rectangle('fill',500,900,75,75)
        love.graphics.draw(panelup,500,900,0,0.10)		-- show bottom panel
    end
  
    
    if screenoptions==true then
    
			love.graphics.setColor(0.5,0.5,0.5,0.8)
			love.graphics.rectangle('fill',150,220,800,700)
			love.graphics.setColor(1,1,1)
    
    love.graphics.draw(crteffecticon,200,300,0,0.15)				-- CRT effect
    love.graphics.print("CRT effect",200,370,0,1)
    
    love.graphics.draw(shadericon,400,300,0,0.10)				-- Shader effect
    love.graphics.print("Shader",400,370,0,1)
    
    love.graphics.draw(paletteicon,600,300,0,0.10)					-- paletteeffect
    love.graphics.print("Change color scheme",600,370,0,1)
    
    love.graphics.draw(threedicon,200,500,0,0.10)					-- 3d icon
    love.graphics.print("3d on / off",200,570,0,1)
    
    love.graphics.draw(dioramaicon,400,470,0,0.15)				-- Diorama
    love.graphics.print("Diorama on/off",400,570,0,1)
    
    love.graphics.draw(screenresolutionicon,600,500,0,0.10)		-- Screen resolution
    love.graphics.print("Screen resolution",600,570,0,1)
    
    --love.graphics.draw(leditoricon,200,700,0,0.15)				-- level editor
    --love.graphics.print("Background editor",200,770,0,1)
    
    love.graphics.draw(closeicon,400,700,0,0.10)					-- Close
    love.graphics.print("Close screen options",400,770,0,1)
    
    --love.graphics.draw(helpicon,1150,530,0,0.10)					-- help icon
    end


end


