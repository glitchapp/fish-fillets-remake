--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
These are functions related to displaying a loading animation in a game or application. Here is a description of each function:
--]]

--loadingvariables(): Initializes various variables used in the loading process, including the timer, loading speed, loading angle, and images used in the loading animation.
function loadingvariables()
timer3=0
loadingspeed = 16
isloading=false
loadingprocess=false

loadingangle = 0
--Thread:start( loadingthread)
	loading= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/loading.webp")))
    loading2= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/loading2.webp")))
    --loadingsound = love.audio.newSource( "/externalassets/sounds/GUI_Sound_Effects_by_Lokif/load.ogg","static" )
    --sharpechosound = love.audio.newSource( "/externalassets/sounds/GUI_Sound_Effects_by_Lokif/sharp_echo.ogg","static" )

		
			if res=="1440p" then xload=1600 yload=1300		-- loading logo position
			  else 					 xload=1600 yload=800
			end
end

--Updates the loading angle variable based on the elapsed time since the last update.
function loadingupdate(dt)
		loadingangle = loadingangle - (loadingspeed * dt)  -- rotate loading icon
		if loadingangle>360 then loadingangle=0 end
		Transitionupdate(dt)

end

--loadingdraw(dt): Draws the loading animation on the screen. If the loading process is ongoing, it displays a rotating loading icon; if the loading process is complete, it fades out the animation gradually.
function loadingdraw(dt)

love.graphics.setShader()

		if loadingprocess==true then
			love.graphics.draw(loading2,xload,yload,0,0.3,0.3,369,368.5)
			love.graphics.draw(loading,xload,yload,-loadingangle,0.3,0.3,369,368.5)
			
				TransitionDraw()
				
	elseif loadingprocess==false then
			love.graphics.setColor(1,1,1,1-timer3/2)
			love.graphics.draw(loading2,xload,yload,0,0.3,0.3,369,368.5)
			love.graphics.draw(loading,xload,yload,-loadingangle,0.3,0.3,369,368.5)
			
			if timer3>2 then love.graphics.setColor(0.5,0.5,0.5,1-timer3/4) else love.graphics.setColor(0.5,0.5,0.5,0.3) end
		
			-- Audio track rectangle
			love.graphics.rectangle( "fill", 1075*scalefactor, 825*scalefactor,650*scalefactor,150*scalefactor )
			
			fadelevelinformation()
			
			love.graphics.print ("Playing: ",1100*scalefactor, 850*scalefactor,0,1)
			love.graphics.print (currenttrackname,1300*scalefactor, 850*scalefactor,0,1)
			love.graphics.print (author,		  1300*scalefactor, 900*scalefactor,0,1)

			
			love.graphics.setColor(1,1,1,1)
				TransitionDraw()
				
	end

end

--triggerloadingprocess(dt): Initiates the loading process when called. It plays an audio sound, changes the game status to "game," and resets the timer and stepdone variables.
function triggerloadingprocess(dt)

		if timer2>0.01 and timer<0.02 then
			--love.audio.play( loadingsound )
			changelevel()
			gamestatus="game"
			timer=0
			stepdone=0
			
			
			--love.audio.play( sharpechosound )
	end
	
end

--: Gradually fades out the loading animation after the loading process is complete. When the timer reaches a certain point, it sets the isloading variable to false, indicating that the loading process has finished.
function fadeloadingprocess(dt)
	if timer3>5 and timer3<6 then
			isloading=false
	end
end

function fadelevelinformation()


	-- Level name rectangle
	if timer3>3 then love.graphics.setColor(0.5,0.5,0.5,1-timer3/5) else love.graphics.setColor(0.5,0.5,0.5,0.3) end
	love.graphics.rectangle( "fill", 475*scalefactor, 825*scalefactor,425*scalefactor,150*scalefactor)
	
	if timer3>3 then love.graphics.setColor(1,1,1,1-timer3/5) else love.graphics.setColor(1,1,1,1) end
	
-- Display level information based on the level number.
if nLevel>1 and nLevel <9 then
-- Display the name of the zone and the name of the fishhouse for the current nLevel.
love.graphics.print (zonetext[1],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..fishhouse0[nLevel],500*scalefactor, 900*scalefactor,0)
elseif nLevel>8 and nLevel <20 then
-- Display the name of the zone and the name of the shipwreck for the current nLevel.
love.graphics.print (zonetext[2],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..shipwrecks1[nLevel-8], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>44 and nLevel <52 then
-- Display the name of the zone and the name of the silver ship for the current nLevel.
love.graphics.print (zonetext[3],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..silversship2[nLevel-44], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>19 and nLevel <30 then
-- Display the name of the zone and the name of the city in the deep for the current nLevel.
love.graphics.print (zonetext[4],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..cityinthedeep3[nLevel-19], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>29 and nLevel <38 then
-- Display the name of the zone and the name of the coral reef for the current nLevel.
love.graphics.print (zonetext[6],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..coralreef5[nLevel-29], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>37 and nLevel <45 then
-- Display the name of the zone and the name of the dump for the current nLevel.
love.graphics.print (zonetext[8],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..dump7[nLevel-37], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>51 and nLevel <59 	then
-- Display the name of the zone and the name of the ufo for the current nLevel.
love.graphics.print (zonetext[5],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..ufo4[(nLevel-51)], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>58 and nLevel <66 	then
-- Display the name of the zone and the name of the treasurecave for the current nLevel.
love.graphics.print (zonetext[7],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..treasurecave6[(nLevel-58)], 500*scalefactor, 900*scalefactor,0,1) 
elseif nLevel>64 and nLevel <71 	then
-- Display the name of the zone and the name of the secretcomputer for the current nLevel.
love.graphics.print (zonetext[9],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..secretcomputer8[nLevel-64], 500*scalefactor, 900*scalefactor,0,1)
elseif nLevel>70 and nLevel <79 	then
-- Display the name of the zone and the name of the nextgeneration nLevels(secret) for the current nLevel.
love.graphics.print (zonetext[10],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((nLevel) .. '.' ..secret9[nLevel-70], 500*scalefactor, 900*scalefactor,0,1)
end
end		



