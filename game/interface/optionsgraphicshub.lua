


function optionsloadgraphicshub()
		if res=="1080p" then xspace = 200 yspace = 50
	elseif res=="1440p" then xspace = 200 yspace = 50
	elseif res=="4k" then xspace = 350 yspace = 100
	else  xspace = 250 yspace = 75
	end



-- Hud buttons
fpsOnButton
= {
	text = "On",
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

fpsOffButton
= {
	text = "Off",
	x = xspace*5,
	y = yspace*3,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

batteryOnButton
= {
	text = "On",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

batteryOffButton
= {
	text = "Off",
	x = xspace*5,
	y = yspace*4,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

timedataOnButton
= {
	text = "On",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

timedataOffButton
= {
	text = "Off",
	x = xspace*5,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

AnalogClockOnButton
= {
	text = "On",
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

AnalogClockOffButton
= {
	text = "Off",
	x = xspace*5,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}





end

function graphicsdrawinformation(graphmes1,graphmes2,dt)
			love.graphics.setColor(0,0,0.5,0.8)	
			love.graphics.rectangle("fill",300,850,1600,250)
			love.graphics.setColor(adjustr,adjustg,adjustb,1)	
			love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) 
											love.graphics.print (graphmes2, 400,1000)
	
end
--[[
This code defines the optionsdrawgraphics function, which is responsible for drawing the graphics hub screen of a game.
--]]

function optionsdrawgraphicshub(dt)
         drawGraphicTabs()
		--[[The second section of the code draws the various options on the screen.
		 The love.graphics.print() function is used to print the names of each option, followed by the values associated with each option.
		The xspace variable is used to space out the options. The options include the graphic profile, color scheme, 3D, skin, upscale, resolution, fullscreen, HDR, caustic effects, 2nd effect, RGB, Chromatic aberration, Vsync, FPS, limit FPS, and battery.--]]
			
	--[[		-- FPS
	--The FPS buttons turn the FPS display on or off.
		if showfps==true then love.graphics.rectangle("fill",fpsOnButton.x-5,fpsOnButton.y-5,200, 50) 
    elseif showfps==false then love.graphics.rectangle("fill",fpsOffButton.x-5,fpsOffButton.y-5,200, 50) 
    end
			
		    -- Battery
    --determines whether to display the battery level by checking the value of the showbattery variable. If it is true, the battery level is displayed on the batteryOnButton object, otherwise it is displayed on the batteryOffButton object.
  		if showbattery==true then love.graphics.rectangle("fill",batteryOnButton.x-5,batteryOnButton.y-5,200, 50) 
    elseif showbattery==false then love.graphics.rectangle("fill",batteryOffButton.x-5,batteryOffButton.y-5,200, 50) 
    end
		--]]	
			love.graphics.print ("FPS: ", xspace*2,fpsOnButton.y)
			love.graphics.print ("battery: ", xspace*2,batteryOnButton.y)
			love.graphics.print ("Time / Data (digital): ", xspace*2,timedataOnButton.y)
			love.graphics.print ("Analog clock: ", xspace*2,AnalogClockOnButton.y)
			
			
			
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(adjustr,adjustg,adjustb,0.3)	
				else  love.graphics.setColor(adjustr,adjustb,adjustb,0.3)
				end

	--[[This is a piece of code written in Lua that draws a HUB for a game.
	--]]
      
	-- FPS
	--The FPS buttons turn the FPS display on or off.
		if showfps==true then love.graphics.rectangle("fill",fpsOnButton.x-5,fpsOnButton.y-5,200, 50) 
    elseif showfps==false then love.graphics.rectangle("fill",fpsOffButton.x-5,fpsOffButton.y-5,200, 50) 
    end
    
    -- Battery
    --determines whether to display the battery level by checking the value of the showbattery variable. If it is true, the battery level is displayed on the batteryOnButton object, otherwise it is displayed on the batteryOffButton object.
  		if showbattery==true then love.graphics.rectangle("fill",batteryOnButton.x-5,batteryOnButton.y-5,200, 50) 
    elseif showbattery==false then love.graphics.rectangle("fill",batteryOffButton.x-5,batteryOffButton.y-5,200, 50) 
    end
    
      -- Time data
    --determines whether to display time / data.
  		if showtimedate==true then love.graphics.rectangle("fill",timedataOnButton.x-5,timedataOnButton.y-5,200, 50) 
    elseif showtimedate==false then love.graphics.rectangle("fill",timedataOffButton.x-5,timedataOffButton.y-5,200, 50) 
    end
    
    --Analog Clock
    	if showAnalogClock==true then love.graphics.rectangle("fill",AnalogClockOnButton.x-5,AnalogClockOnButton.y-5,200, 50) 
    elseif showAnalogClock==false then love.graphics.rectangle("fill",AnalogClockOffButton.x-5,AnalogClockOffButton.y-5,200, 50) 
    end
  
	
	--[[
The fpsOnButton and fpsOffButton are buttons to show or hide FPS metrics.
If the mouse is hovering over the fpsOnButton, graphmes1 is set to describe that FPS metrics are being enabled, and graphbuttonhovered is set to true.
If the fpsOnButton is clicked, showfps is set to true, and FPS metrics are shown on the game screen.
Similarly, if the mouse is hovering over the fpsOffButton, graphmes1 is set to describe that FPS metrics are being disabled, and graphbuttonhovered is set to true.
If the fpsOffButton is clicked, showfps is set to false, and FPS metrics are hidden.
--]]
	--FPS
		local hovered = isButtonHovered (fpsOnButton)		-- Set FPS metrics on
	drawButton (fpsOnButton, hovered)
	if hovered then graphmes1="enable FPS metrics" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showfps=true
		love.timer.sleep( 0.3 )
	--else graphbuttonhovered=false
	end
	
		local hovered = isButtonHovered (fpsOffButton)		-- Set FPS metrics off
	drawButton (fpsOffButton, hovered)
	if hovered then graphmes1="disables FPS metrics"  graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showfps=false
		love.timer.sleep( 0.3 )
	end
		
	
	--battery
	--[[
	The next block of code is for two buttons, "batteryOnButton" and "batteryOffButton," that control the display of battery status information. The behavior of these buttons is similar to the benchmark button, with "showbattery" being set to true or false depending on whether the "batteryOnButton" or "batteryOffButton" is clicked.
	--]]
			local hovered = isButtonHovered (batteryOnButton)		-- Set battery ide on
	drawButton (batteryOnButton, hovered)
	if hovered then graphmes1="show battery status if available" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showbattery=true
			if powerpercent==nil then powerpercent="∞" powerseconds="∞" end
			if powerstate==nil then powerstate="No battery" end
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (batteryOffButton)		-- Set battery ide off
	drawButton (batteryOffButton, hovered)
	if hovered then graphmes1="disable battery status" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showbattery=false
		love.timer.sleep( 0.3 )
	end
	
		--Time data
			local hovered = isButtonHovered (timedataOnButton)		-- Set battery ide on
	drawButton (timedataOnButton, hovered)
	if hovered then graphmes1="show time / data" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showtimedate=true
			if powerpercent==nil then powerpercent="∞" powerseconds="∞" end
			if powerstate==nil then powerstate="No battery" end
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (timedataOffButton)		-- Set battery ide off
	drawButton (timedataOffButton, hovered)
	if hovered then graphmes1="disable time / data" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showtimedate=false
		love.timer.sleep( 0.3 )
	end
	
				local hovered = isButtonHovered (AnalogClockOnButton)		-- Set battery ide on
	drawButton (AnalogClockOnButton, hovered)
	if hovered then graphmes1="show time / data" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showAnalogClock=true
			
		love.timer.sleep( 0.3 )
	end
	
		local hovered = isButtonHovered (AnalogClockOffButton)		-- Set battery ide off
	drawButton (AnalogClockOffButton, hovered)
	if hovered then graphmes1="disable time / data" graphmes2="" end
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		showAnalogClock=false
		love.timer.sleep( 0.3 )
	end
	

    
end
