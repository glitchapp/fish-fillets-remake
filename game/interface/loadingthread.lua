
function love.load()
loadingspeed = 16
isloading=false
loadingprocess=false
loadingangle = 0

	loading= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/loading.png")))
    loading2= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/loading2.png")))
    --loadingsound = love.audio.newSource( "/externalassets/sounds/GUI_Sound_Effects_by_Lokif/load.ogg","static" )
    --sharpechosound = love.audio.newSource( "/externalassets/sounds/GUI_Sound_Effects_by_Lokif/sharp_echo.ogg","static" )


end

function love.update(dt)
		loadingangle = loadingangle - (loadingspeed * dt)  -- rotate loading icon
		if loadingangle>360 then loadingangle=0 end

end

function love.draw(dt)

		if loadingprocess==true then
			love.graphics.draw(loading2,1600,800,0,0.3,0.3,369,368.5)
			love.graphics.draw(loading,1600,800,-loadingangle,0.3,0.3,369,368.5)
	elseif loadingprocess==false then
			love.graphics.setColor(1,1,1,1-timer2/2)
			love.graphics.draw(loading2,1600,800,0,0.3,0.3,369,368.5)
			love.graphics.draw(loading,1600,800,-loadingangle,0.3,0.3,369,368.5)
			love.graphics.setColor(1,1,1,1)
	end
end

function triggerloadingprocess(dt)

		if timer2>0.01 and timer<1.02 then
			--love.audio.play( loadingsound )
			changelevel()
			gamestatus="game"
			timer=0
			stepdone=0
			loadingprocess=false
			--love.audio.play( sharpechosound )
	end
	
end

function fadeloadingprocess(dt)
	if timer2>2 and timer2<4 then
			isloading=false
	end
end
