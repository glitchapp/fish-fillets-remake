
function Transitionupdate(dt)
	TransitionLogoUpdate(dt)
    if inTransition then
        transitionTime = transitionTime + dt
        --prevent problems with the fade out music transition on levels using lovebpm
        if not (nLevel==15 or nLevel==16 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==34 or nLevel==38 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==62 or nLevel==6 or nLevel==71 or nLevel==72 or nLevel==73 or nLevel==75 or nLevel==76) then
			music:setVolume(-transitionTime+1)
		end
	
        -- Check if the transition is complete
        if transitionTime >= transitionDuration then
            transitionTime = 0
			--prevent problems with the fade out music transition on levels using lovebpm
             if not (nLevel==15 or nLevel==16 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==34 or nLevel==38 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==62 or nLevel==6 or nLevel==71 or nLevel==72 or nLevel==73 or nLevel==75 or nLevel==76) then
				music:stop()
			end
            inTransition = false
            
            -- Load the next scene
            -- loadNextScene()
        end
    end

    if transitionShader then
        transitionShader:send("time", transitionTime / transitionDuration)
    end
end
