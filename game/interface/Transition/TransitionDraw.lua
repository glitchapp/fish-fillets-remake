

function TransitionDraw()

    -- Draw current scene

    if inTransition then
       -- Set the time uniform in the shader
        transitionShader:send("time", transitionTime / transitionDuration)

        -- Set the noise texture in the shader
        --transitionShader:send("noiseTexture", noiseTexture)

        -- Apply the shader
        love.graphics.setShader(transitionShader)
        
        	
	if nLevel==8
	or nLevel==19
	or nLevel==29
	or nLevel==37 
	or nLevel==44
	or nLevel==51
	or nLevel==58
	or nLevel==64
	or nLevel==70
	or nLevel==79
	then transitionDuration=3
		TransitionLogoDraw()
		--InvertedTransitionLogoDraw()
	end

        -- Draw a fullscreen rectangle to cover the screen
        love.graphics.setColor(0,0,0,1)
        love.graphics.rectangle("fill", love.graphics.getWidth()-love.graphics.getWidth()/3.3, 0, love.graphics.getWidth()/3.3, love.graphics.getHeight())
        --love. graphics.rectangle("fill",0,0,4000,4000)
        love.graphics.setColor(1,1,1,1)
			drawlevelmenubck(x,y)
			
				drawlevelspheres2(x,y)

		
        -- Reset the shader
        love.graphics.setShader()
        --TransitionLogoDraw()

    end
end


--[[The third function drawCenteredSprite draws a given sprite centered on a specified tile. It takes in x and y as the tile coordinates, tileSize as the size of the tile, and sprite as the image to be drawn. It first calculates the dimensions of the sprite, and then scales it to fit within the tile size. Finally, it draws the sprite at the center of the tile coordinates.--]]
local function drawCenteredSprite (x, y, tileSize, sprite)
--	x and y it tiles, [1,1] is top left
	local w, h = sprite:getDimensions()
	local scale = tileSize/math.max (w, h)
	love.graphics.draw (sprite, (x-0.5)*tileSize, (y-0.5)*tileSize, 0, scale, scale, w/2, h/2)
end

--[[The second function drawCenteredText draws a given text string centered in a specified rectangle. It takes in rectX and rectY as the top-left corner of the rectangle, rectWidth and rectHeight as the dimensions of the rectangle, and the text string to be drawn. It then calculates the width and height of the text using the current font, and prints the text at the center of the rectangle.--]]
local function drawCenteredText (rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end

	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	

function drawlevelspheres2(x,y)

	-- to make the spheres adaptive to window size:
	local tileSizePrevious=tileSize
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)

	--This section defines the width and height of the screen, and sets the tileSize to the smaller of either the width divided by the amount of tiles, or the height divided by the amount of tiles. It also sets a maximum tileSize of 50.
	local mx, my = love.mouse.getPosition()
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1
					--This section gets the position of the mouse, and calculates which tile the mouse is currently hovering over.
	for i, spherelevel in ipairs (spheres) do
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y

			love.graphics.setColor(0,1,0) 
				if res=="720p"  then drawCenteredSprite ((x*1.04), (y*1.03), tileSize, sphere)
			elseif res=="1080p" then drawCenteredSprite (x, y, tileSize, sphere)
			elseif res=="1440p" then drawCenteredSprite ((x*1.34)-0.1, (y*1.34)-0.1, tileSize, sphere)
			elseif res=="4k" 	then drawCenteredSprite ((x*scalefactor/1.5)-0.2, (y*scalefactor/1.5)-0.2, tileSize*1.5, sphere)
			else drawCenteredSprite (x, y, tileSize, sphere)
			end
			--love.graphics.setFont(poorfishsmall)
			--love.graphics.setColor(0,0,0)
			--drawCenteredText ((x-1)*tileSize*scalefactor, (y-1)*tileSize*scalefactor, tileSize, tileSize, level)
		end
	end
	tileSize=tileSizePrevious
end
