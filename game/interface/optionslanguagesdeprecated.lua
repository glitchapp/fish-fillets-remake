

function rectangleoverlanguage()
love.graphics.setColor(1,1,0,0.5)
		if language=="bg" then love.graphics.rectangle("fill",bgButton.x-5,bgButton.y-5,200, 50)
	elseif language=="br" then love.graphics.rectangle("fill",brButton.x-5,brButton.y-5,200, 50)
	elseif language=="ch" then love.graphics.rectangle("fill",chButton.x-5,chButton.y-5,200, 50)
	elseif language=="da" then love.graphics.rectangle("fill",daButton.x-5,daButton.y-5,200, 50)
	elseif language=="de" then love.graphics.rectangle("fill",deButton.x-5,deButton.y-5,200, 50)
			if accent=="ch" then love.graphics.rectangle("fill",chButton.x-5,chButton.y-5,200, 50)
		elseif accent=="de" then love.graphics.rectangle("fill",dedeButton.x-5,dedeButton.y-5,150, 30) 
				-- lunarwingz
				local hovered = isButtonHovered (lunarwingzButton)
				drawButton (lunarwingzButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					love.timer.sleep( 0.3 )
						language="de"
				end
			end
	
	elseif language=="en" then love.graphics.rectangle("fill",enButton.x-5,enButton.y-5,200, 50)
	    -- Voice actors
		if accent=="br" then love.graphics.rectangle("fill",enbrButton.x-5,enbrButton.y-5,200, 50)
			-- Aaron
			local hovered = isButtonHovered (AaronButton)
			drawButton (AaronButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent="us"
			end
		elseif accent=="us" then love.graphics.rectangle("fill",enusButton.x-5,enusButton.y-5,200, 50)
			-- John
			local hovered = isButtonHovered (JohnButton)
			drawButton (JohnButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent="br"
			end
		end

	elseif language=="eo" then love.graphics.rectangle("fill",eoButton.x-5,eoButton.y-5,200, 50)
	elseif language=="es" then love.graphics.rectangle("fill",esButton.x-5,esButton.y-5,200, 50)
		if accent=="es" then love.graphics.rectangle("fill",esesButton.x-5,esesButton.y-5,200, 50)
		elseif accent=="la" then love.graphics.rectangle("fill",eslaButton.x-5,eslaButton.y-5,200, 50)
		-- Bastián
			local hovered = isButtonHovered (bastianButton)
			drawButton (bastianButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent="la"
			end
		end
	elseif language=="fi" then love.graphics.rectangle("fill",fiButton.x-5,fiButton.y-5,200, 50)
	elseif language=="is" then love.graphics.rectangle("fill",isButton.x-5,isButton.y-5,200, 50)
	elseif language=="fr" then love.graphics.rectangle("fill",frButton.x-5,frButton.y-5,200, 50)
			if accent=="fr" then
				-- Labyrinthus
				local hovered = isButtonHovered (labyrinthusButton)
				drawButton (labyrinthusButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					love.timer.sleep( 0.3 )
						accent="fr"
				end
				
			
			elseif accent=="ca" then love.graphics.rectangle("fill",frcaButton.x-5,frcaButton.y-5,200, 50)
			end
	elseif language=="gr" then love.graphics.rectangle("fill",grButton.x-5,grButton.y-5,200, 50)
	elseif language=="it" then love.graphics.rectangle("fill",itButton.x-5,itButton.y-5,200, 50)
	elseif language=="nl" then love.graphics.rectangle("fill",nlButton.x-5,nlButton.y-5,200, 50)
			if accent=="nl" then love.graphics.rectangle("fill",nlnlButton.x-5,nlnlButton.y-5,200, 50)
		elseif accent=="fl" then love.graphics.rectangle("fill",flButton.x-5,flButton.y-5,200, 50)
		end
	elseif language=="no" then love.graphics.rectangle("fill",noButton.x-5,noButton.y-5,200, 50)
    elseif language=="pt" then love.graphics.rectangle("fill",prButton.x-5,prButton.y-5,200, 50)
    	if accent=="pt" then love.graphics.rectangle("fill",prprButton.x-5,enbrButton.y-5,200, 50)
		elseif accent=="br" then love.graphics.rectangle("fill",brButton.x-5,enusButton.y-5,200, 50)
		end
	
	elseif language=="ru" then love.graphics.rectangle("fill",ruButton.x-5,ruButton.y-5,200, 50)
	elseif language=="sv" then love.graphics.rectangle("fill",seButton.x-5,seButton.y-5,200, 50)
	elseif language=="sk" then love.graphics.rectangle("fill",skButton.x-5,skButton.y-5,200, 50)
    end
    
    if language2=="en" then
		if accent2=="br" then 
				-- isVA
			local hovered = isButtonHovered (ishVAButton)
			drawButton (ishVAButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent2="us"
			end
		elseif accent2=="us" then
				-- Snoozy
			local hovered = isButtonHovered (snoozyButton)
			drawButton (snoozyButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent2="br"
			end
		end
	elseif language2=="pl" then love.graphics.rectangle("fill",plButton.x-5,plButton.y-5,200, 50)
		-- Nelakori
			local hovered = isButtonHovered (nelakoriButton)
			drawButton (nelakoriButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent2="pl"
			end
	elseif language2=="es" then love.graphics.rectangle("fill",esButton.x-5,esButton.y-5,200, 50)
			if accent2=="es" then love.graphics.rectangle("fill",esesButton.x-5,esesButton.y-5,200, 50)
		elseif accent2=="la" then love.graphics.rectangle("fill",eslaButton.x-5,eslaButton.y-5,200, 50)
		-- zeroSky
			local hovered = isButtonHovered (zeroSkyButton)
			drawButton (zeroSkyButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
					accent2="la"
			end
		end
	elseif language2=="nl" then love.graphics.rectangle("fill",esButton.x-5,esButton.y-5,200, 50)
			if accent2=="nl" then love.graphics.rectangle("fill",nlnlButton.x-5,nlnlButton.y-5,200, 50)
					-- Rosie Krijgsman
				local hovered = isButtonHovered (rosieButton)
				drawButton (rosieButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					love.timer.sleep( 0.3 )
						accent2="nl"
				end
			end
	elseif language2=="fr" then love.graphics.rectangle("fill",frButton.x-5,frButton.y-5,200, 50)
			if accent2=="fr" then love.graphics.rectangle("fill",frfrButton.x-5,frfrButton.y-5,200, 50)
				-- vn_aiis
				local hovered = isButtonHovered (vnaiisButton)
				drawButton (vnaiisButton, hovered)
				if hovered and love.mouse.isDown(1) then 
					love.timer.sleep( 0.3 )
						accent2="fr"
				end
			end
		elseif accent2=="fl" then love.graphics.rectangle("fill",flButton.x-5,flButton.y-5,200, 50)
		

	elseif language2=="cs" then love.graphics.rectangle("fill",csButton.x-5,csButton.y-5,200, 50)
	end
    

		
end


function drawdeprecatedbuttonlayout()
local hovered = isButtonHovered (bgButton)
	drawButton (bgButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="bg" languagehaschanged=true
	optionsload()
	end
	
	-- Danish
			local hovered = isButtonHovered (daButton)
			drawButton (daButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
				language="da" language2="da" languagehaschanged=true
				require("assets/text/levelmenu/level-da")
				optionsload()
			end
	
	-- Finnish
			local hovered = isButtonHovered (fiButton)
			drawButton (fiButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
				language="fi" language2="fi" languagehaschanged=true
				require("assets/text/levelmenu/level-fi")
				optionsload()
			end
	
	-- Icelandic
			local hovered = isButtonHovered (isButton)
			drawButton (isButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
				language="is" language2="is" languagehaschanged=true
				require("assets/text/levelmenu/level-is")
				optionsload()
			end
	
	-- Norwegian
			local hovered = isButtonHovered (noButton)
			drawButton (noButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
				language="no" language2="no" languagehaschanged=true
				require("assets/text/levelmenu/level-no")
				optionsload()
			end
	
	if language=="de" then
			local hovered = isButtonHovered (chButton)
			drawButton (chButton, hovered)
			if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.3 )
				language="de" accent="ch" language2="de" accent2="ch" languagehaschanged=true
				require("assets/text/levelmenu/level-de")
				optionsload()
			end
		local hovered = isButtonHovered (dedeButton)
		drawButton (dedeButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			love.timer.sleep( 0.3 )
			language="de" accent="de" language2="de" accent2="de" languagehaschanged=true
			require("assets/text/levelmenu/level-de")
			optionsload()
		end
	end
	
	local hovered = isButtonHovered (csButton)
	drawButton (csButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="en" language2="cs" languagehaschanged=true			-- temporarily the missing audios will be filled with British English audios
	optionsload()
	end
	if language2=="cs" then love.graphics.print ("kat-leroy", xspace*4,yspace*10) end

	local hovered = isButtonHovered (frButton)
	drawButton (frButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="fr" accent="fr" language2="fr" accent2="fr" languagehaschanged=true		
	optionsload()
	end
	
	
	if language=="fr" then
	local hovered = isButtonHovered (frfrButton)
	drawButton (frfrButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="fr" accent="fr" language2="fr" accent2="fr" languagehaschanged=true
	optionsload()
	end
	
	
	local hovered = isButtonHovered (frcaButton)
	drawButton (frcaButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="fr" accent="ca" language2="fr" accent2="ca"
	--require("assets/text/levelmenu/level-fr")
	optionsload()
	end
	end
	
	local hovered = isButtonHovered (enButton)
	drawButton (enButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="en" language2="en" accent="br" accent2="br" languagehaschanged=true
	require("assets/text/levelmenu/level-en")
	optionsload()
	end
	
	if language=="en" then
	local hovered = isButtonHovered (enbrButton)
	drawButton (enbrButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="en" accent="br" language2="en" accent2="br" languagehaschanged=true
	require("assets/text/levelmenu/level-en")
	optionsload()
	end
	
	
	local hovered = isButtonHovered (enusButton)
	drawButton (enusButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="en" accent="us" language2="en" accent2="us" languagehaschanged=true
	require("assets/text/levelmenu/level-en")
	optionsload()
	end
	end
	
	local hovered = isButtonHovered (eoButton)
	drawButton (eoButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="eo" languagehaschanged=true
	optionsload()
	end
	
	local hovered = isButtonHovered (esButton)
	drawButton (esButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="es" accent="la" language2="es" accent2="la" languagehaschanged=true
	require("assets/text/levelmenu/level-es")
	optionsload()
	end

	if language=="es" then
		local hovered = isButtonHovered (esesButton)
		drawButton (esesButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			love.timer.sleep( 0.3 )
			language="es" accent="es" language2="es" accent2="es" languagehaschanged=true
			require("assets/text/levelmenu/level-es")
			optionsload()
		end
	
		local hovered = isButtonHovered (eslaButton)
		drawButton (eslaButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			love.timer.sleep( 0.3 )
			language="es" accent="la" language2="es" accent2="la" languagehaschanged=true
			require("assets/text/levelmenu/level-es")
			optionsload()
		end
	end
	
	local hovered = isButtonHovered (deButton)
	drawButton (deButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="de" accent="de" language2="de" accent2="de" languagehaschanged=true
		require("assets/text/levelmenu/level-de")
	optionsload()
	end
	
	-- Greek
	local hovered = isButtonHovered (grButton)
	drawButton (grButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	require("assets/text/levelmenu/level-gr")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/pecita/Pecita.otf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/pecita/Pecita.otf", 32)
	language="gr" languagehaschanged=true
	optionsload()
	end
	
	local hovered = isButtonHovered (itButton)
	drawButton (itButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="it" languagehaschanged=true
	optionsload()
	end
	
	local hovered = isButtonHovered (nlButton)
	drawButton (nlButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="en" accent="br" language2="nl" accent2="nl" languagehaschanged=true
	optionsload()
	end
	
		if language=="nl" then
		local hovered = isButtonHovered (nlnlButton)
		drawButton (nlnlButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			love.timer.sleep( 0.3 )
			language="nl" accent="nl" language2="nl" accent2="nl" languagehaschanged=true
			require("assets/text/levelmenu/level-nl")
			optionsload()
		end
	
		local hovered = isButtonHovered (flButton)
		drawButton (flButton, hovered)
		if hovered and love.mouse.isDown(1) then 
			love.timer.sleep( 0.3 )
			language="nl" accent="fl" language2="nl" accent2="fl" languagehaschanged=true
			require("assets/text/levelmenu/level-nl")
			optionsload()
		end

	end
	
	local hovered = isButtonHovered (plButton)
	drawButton (plButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	--language="pl" languagehaschanged=true
	language="en" accent="br" language2="pl" languagehaschanged=true			-- temporarily the missing audios will be filled with British English audios
	optionsload()
	end
	
	local hovered = isButtonHovered (prButton)
	drawButton (prButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="pt" accent="pt" languagehaschanged=true	
	optionsload()
	end

	if language=="pt" then
	
	local hovered = isButtonHovered (prprButton)
	drawButton (prprButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="pt" accent="pt" language2="pt" accent2="pt" 
	optionsload()
	end
	
	local hovered = isButtonHovered (brButton)
	drawButton (brButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="pt" accent="br" language2="pt" accent2="br"
	optionsload()
	end
	end
	
	local hovered = isButtonHovered (ruButton)
	drawButton (ruButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="ru" languagehaschanged=true
	require("assets/text/levelmenu/level-ru")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32)
	optionsload()
	end
	
	local hovered = isButtonHovered (seButton)
	drawButton (seButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="sv" language2="sv" languagehaschanged=true
	optionsload()
	end
	
	local hovered = isButtonHovered (skButton)
	drawButton (skButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="sk" 
	optionsload()
	end
	
	local hovered = isButtonHovered (jpButton)
	drawButton (jpButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="jp"
	languagehaschanged=true
	require("assets/text/levelmenu/level-jp")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 24)
	optionsload()
	end
	
	local hovered = isButtonHovered (chiButton)
	drawButton (chiButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="chi"	languagehaschanged=true
	require("assets/text/levelmenu/level-chi")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 24)
	poorfish = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 84)
	require "game/interface/touchinterface1" --require touch interface
	optionsload()
	loadtouchtext()
	end
	
	local hovered = isButtonHovered (thaiButton)
	drawButton (thaiButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="thai"
	languagehaschanged=true
	require("assets/text/levelmenu/level-thai")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 24)
	optionsload()
	end
	
	local hovered = isButtonHovered (koreanButton)
	drawButton (koreanButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="ko"
	languagehaschanged=true
	require("assets/text/levelmenu/level-thai")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/singleday-regular/18938.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/singleday-regular/18938.ttf", 24)
	optionsload()
	end
	
	local hovered = isButtonHovered (tamilButton)
	drawButton (tamilButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="tamil"
	require("assets/text/levelmenu/level-tamil")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 24)
	optionsload()
	end
	
	local hovered = isButtonHovered (hindiButton)
	drawButton (hindiButton, hovered)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
	language="hindi"
	require("assets/text/levelmenu/level-hindi")
	poorfishmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 64)
	poorfish = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 24)
	optionsload()
	end
end
