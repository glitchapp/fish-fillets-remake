HelpButton = {}
HelpButton.x=helpWindowX
HelpButton.y=helpWindowY

-- Define the help button
HelpButton = {
    text = "Help",
    x = HelpButton.x,
	y = HelpButton.y,
    r = 0,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
}



function showhelp()

if gamestatus=="game" then
    if helpison == "yes" then
        -- Draw the draggable window
		love.graphics.setColor(1,1,1,1)
        love.graphics.rectangle('line', helpWindowX, helpWindowY, 400, 30)
        love.graphics.rectangle('line', helpWindowX, helpWindowY, 400, 800)
        love.graphics.setColor(0.7, 0.7, 0.7, 0.8)
        love.graphics.rectangle('fill', helpWindowX, helpWindowY, 400, 30)
        love.graphics.setColor(0.5, 0.5, 0.5, 0.8)
        love.graphics.rectangle('fill', helpWindowX, helpWindowY+30, 400, 770)

        -- Add your help content here
        love.graphics.setColor(1, 1, 1)
        love.graphics.setFont(love.graphics.newFont(20))
        love.graphics.print(help, helpWindowX + 10, helpWindowY + 40, 0, 0.8, 1)
		love.graphics.print('Drag here', helpWindowX+100, helpWindowY,0,1)
		
        -- Draw the draggable button
        local hovered = isButtonHovered(HelpButton)
        drawButton(HelpButton, hovered)

        -- Check for button click
        if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick, button)) then
            helpison = "no"
        end
    end
end
end


-- Update the position of the help button
function updateHelpButtonPosition()
if gamestatus=="game" then
    HelpButton.x = helpWindowX + 10
    HelpButton.y = helpWindowY + 10
end
end



-- Function to draw the help window
function drawHelpWindow()
if gamestatus=="game" then
    if helpison == "yes" then
        -- Draw the draggable window
        love.graphics.setColor(0.5, 0.5, 0.5, 0.8)
        love.graphics.rectangle('fill', helpWindowX, helpWindowY, 400, 700)

        -- help content here
        love.graphics.setColor(1, 1, 1)
        love.graphics.setFont(love.graphics.newFont(30))
        love.graphics.print(help, helpWindowX + 10, helpWindowY + 40, 0, 0.8, 1)
        



        -- Draw the draggable button
        local hovered = isButtonHovered(HelpButton)
        drawButton(HelpButton, hovered)
    end
end
end

-- Function to draw the help button
function drawHelpButton()
if gamestatus=="game" then
    if helpison == "no" then
        local hovered = isButtonHovered(HelpButton)
        drawButton(HelpButton, hovered)
    end
end
end

-- Function to update the positions of the help window and button
function updateHelpPositions()
if gamestatus=="game" then
    if isHelpDragging==true then
        -- Update the help window position based on mouse movement
        helpWindowX = love.mouse.getX() - helpOffsetX
        helpWindowY = love.mouse.getY() - helpOffsetY

        -- Update the position of the help button
        updateHelpButtonPosition()
    end
end
end


