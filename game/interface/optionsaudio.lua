
function optionsloadaudio()



-- Sound


soundonButton = {
	text = "",
	x = xspace*3,
	y = yspace*3,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

soundoffButton = {
	text = "",
	x = xspace*3,
	y = yspace*3,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musiconButton = {
	text = "",
	x = xspace*3,
	y = yspace*4,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicoffButton = {
	text = "",
	x = xspace*3,
	y = yspace*4,
	image = SwitchON,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicnewButton = {
	text = "new",
	x = xspace*3,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

musicclassicButton = {
	text = "classic",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

dialogsonButton = {
	text = "",
	x = xspace*3,
	y = yspace*6,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

dialogsoffButton = {
	text = "",
	x = xspace*3,
	y = yspace*6,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

end

function optionsdrawaudio(dt)


			love.graphics.print ("Sound: ", soundonButton.x-xspace,soundonButton.y)
			love.graphics.print ("Music: ", musiconButton.x-xspace,musiconButton.y)
			love.graphics.print ("Music: ", musicnewButton.x-xspace,musicnewButton.y)
			love.graphics.print ("Dialogs: ", dialogsonButton.x-xspace,dialogsonButton.y)
	love.graphics.setColor(1,1,0,0.5)
		if soundon==true  then love.graphics.rectangle("fill",soundonButton.x-5,soundonButton.y-5,100, 50)
    elseif soundon==false then love.graphics.rectangle("fill",soundoffButton.x-5,soundoffButton.y-5,100, 50)
    end
    
	 	if musicison==true  then love.graphics.rectangle("fill",musiconButton.x-5,musiconButton.y-5,100, 50)
    elseif musicison==false then love.graphics.rectangle("fill",musicoffButton.x-5,musicoffButton.y-5,100, 50)
    end
    
    	if musictype=="new"  then love.graphics.rectangle("fill",musicnewButton.x-5,musicnewButton.y-5,100, 50)
    elseif musictype=="classic" then love.graphics.rectangle("fill",musicclassicButton.x-5,musicclassicButton.y,100, 50)
    end
    
    -- dialogs
		if talkies==true  then love.graphics.rectangle("fill",dialogsonButton.x-5,dialogsonButton.y-5,100, 50)
    elseif talkies==false then love.graphics.rectangle("fill",dialogsoffButton.x-5,dialogsoffButton.y-5,100, 50)
    end
    
    
    if soundon==false then
		local hovered = isButtonHovered (soundonButton)
		drawButton (soundonButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		soundon=true
		end
	elseif soundon==true then
	
		local hovered = isButtonHovered (soundoffButton)
		drawButton (soundoffButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_off_sound:play()
		love.timer.sleep( 0.3 )
		soundon=false
		end
	end
	
	if musicison==false then
		local hovered = isButtonHovered (musiconButton)
		drawButton (musiconButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_on_sound:play()
		
		musicison=true   
			if musictype=="new" then loadlevelmusic()
		elseif musictype=="classic" then loadlevelmusicclassic()
		end
		love.timer.sleep( 0.3 )
		end
	elseif musicison==true then
	
		local hovered = isButtonHovered (musicoffButton)
		drawButton (musicoffButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_off_sound:play()
		if not (mus==nil) then mus:stop() end
		if not (music==nil) then music:stop() end
		love.timer.sleep( 0.3 )
		musicison=false 
		end
	end
	
	local hovered = isButtonHovered (musicnewButton)
	drawButton (musicnewButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		music:stop()
		love.timer.sleep( 0.3 )
		musictype="new"  musichaschanged=true
		loadlevelmusic()
		--mybpm=112
		--lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
	end
	
	local hovered = isButtonHovered (musicclassicButton)
	drawButton (musicclassicButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
		music:stop()
		love.timer.sleep( 0.3 )
		musictype="classic"  musichaschanged=true
		loadlevelmusicclassic()
		--mybpm=112
		--lovebpmload("/externalassets/music/classic/menu.ogg")
	end

	if talkies==false then
		local hovered = isButtonHovered (dialogsonButton)
		drawButton (dialogsonButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		talkies=true
		end
	elseif talkies==true then
	
		local hovered = isButtonHovered (dialogsoffButton)
		drawButton (dialogsoffButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_off_sound:play()
		love.timer.sleep( 0.3 )
		talkies=false
		end
	end
    
    drawaudiofocus()
    
end


function drawaudiofocus()
love.graphics.setColor(1,1,1,0.3)
		
		
								
							
							if bfocus=="sound" then
								if bfocus2=="on" then	love.graphics.rectangle("fill",soundonButton.x-5,soundonButton.y-5,150, 30) 
							elseif bfocus2=="off" then	love.graphics.rectangle("fill",soundoffButton.x-5,soundoffButton.y-5,150, 30) 
							end
						elseif bfocus=="music" then	
								love.timer.sleep( 0.3 )
								if bfocus2=="on" then 	love.graphics.rectangle("fill",musiconButton.x-5,musiconButton.y-5,150, 30) 
							elseif bfocus2=="off" then	love.graphics.rectangle("fill",musicoffButton.x-5,musicoffButton.y-5,150, 30) 
							end
							
						elseif bfocus=="musicScheme" then
								if bfocus2=="new" then	love.graphics.rectangle("fill",musicnewButton.x-5,musicnewButton.y-5,150, 30) 
							elseif bfocus2=="classic" then	love.graphics.rectangle("fill",musicclassicButton.x-5,musicclassicButton.y-5,150, 30) 
							end
							
						elseif bfocus=="dialogs" then
								if bfocus2=="on" then	love.graphics.rectangle("fill",dialogsonButton.x-5,dialogsonButton.y-5,150, 30) 
							elseif bfocus2=="off" then	love.graphics.rectangle("fill",dialogsoffButton.x-5,dialogsoffButton.y-5,150, 30) 
							end
							
						end
		
						
		
		
		love.graphics.setColor(1,1,1,1)

end
