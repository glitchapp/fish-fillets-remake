--The function reloadadjustedcolors() sets the color adjustment for the Love2D graphics context based on the values of adjustr, adjustg, and adjustb. If all three of these values are equal to 1, then the color adjustment is not changed. Otherwise, if any of these values is nil, it is set to 1. Then the function sets the color with love.graphics.setColor() using the adjusted color values.
function reloadadjustedcolors()
					if adjustr==1 and adjustg==1 and adjustb==1 then
				else 
					if adjustr==nil then adjustr=1 end
					if adjustg==nil then adjustg=1 end
					if adjustb==nil then adjustb=1 end
				love.graphics.setColor(adjustr,adjustg,adjustb,1)
				end
				graphmes1=""
end

function turnshadersoff()
 if skin=="retro" then shader1=false shader2=false caustics=false end	-- prevent retro mode from not rendering properly
end

--The function resetcolors() simply sets the adjustr, adjustg, and adjustb values to 1, effectively resetting any color adjustments that were previously made.
function resetcolors()
	adjustr=1 adjustg=1 adjustb=1
end

--[[
This is a Lua function called drawpreview() that seems to be used for drawing a preview of a video game level. It uses the Lua game development framework called Love2D (also known as just "Love") to handle the graphics.

The function sets the color of the graphics being drawn using the love.graphics.setColor() function, passing it four arguments: adjustr, adjustg, adjustb, and 1. These variables control the red, green, and blue components of the color, with the fourth argument being the alpha (transparency) component, which is set to 1 (fully opaque). The graphics drawn after this call will use this color.

The function then checks whether a variable called skin is set to "remake" or "retro", and depending on its value, it draws different preview graphics.

If skin is "remake", the function draws a background image and a foreground image of a level, as well as some "objects" and a 3D preview of the level. The function also checks the value of a variable called palette, and if it's set to 1, it applies a chromatic aberration effect to the graphics being drawn.

If skin is "retro", the function draws a base image of a retro-style level and then checks the value of palette to draw different palette variations of the same base image.

Overall, the drawpreview() function seems to be used for rendering a preview of a level in a video game, using different skins and palettes depending on the game's design.
--]]

function drawpreview()

	 love.graphics.setColor(adjustr,adjustb,adjustb,1)
				-- skin
					if skin=="remake" then
						if palette==1 then
							if adjustr==1 and adjustg==1 and adjustb==1 then 	 
							else love.graphics.setColor(adjustr,adjustb,adjustb,1)
								--love.graphics.print ("test: ", 100,100)
							end
							
							-- Chromatic aberration
							if chromaab==false then
								love.graphics.draw (level1bck2, xspace*7.5,100,0,0.2,0.2)
								love.graphics.draw (level1bck, xspace*7.5,100,0,0.2,0.2)
								love.graphics.draw (level1frg, xspace*7.5,100,0,0.2,0.2)
							elseif chromaab==true then
								love.graphics.setColor(1,0,0,0.3)
									love.graphics.draw (level1bck2, xspace*7.5,100,0,0.2,0.2)
									love.graphics.draw (level1bck, xspace*7.5,100,0,0.2,0.2)
									love.graphics.draw (level1frg, xspace*7.5,100,0,0.2,0.2)
								love.graphics.setColor(0,1,0,0.3)
									love.graphics.draw (level1bck2, xspace*7.5+5,100-5,0,0.2,0.2)
									love.graphics.draw (level1bck, xspace*7.5+5,100-5,0,0.2,0.2)
									love.graphics.draw (level1frg, xspace*7.5+5,100-5,0,0.2,0.2)
								love.graphics.setColor(0,0,1,0.3)
									love.graphics.draw (level1bck2, xspace*7.5+10,100-10,0,0.2,0.2)
									love.graphics.draw (level1bck, xspace*7.5+10,100-10,0,0.2,0.2)
									love.graphics.draw (level1frg, xspace*7.5+10,100-10,0,0.2,0.2)
							end
							--[[if not currentbck2==false then love.graphics.setColor(scolor2) love.graphics.draw (currentbck2, xspace*7.5,100,0,0.2,0.2) end
							if not currentbck==false then love.graphics.setColor(scolor1) love.graphics.draw (currentbck, xspace*7.5,100,0,0.2,0.2) end
							if not currentfrg==false then love.graphics.setColor(scolor3) love.graphics.draw (currentfrg, xspace*7.5,100,0,0.2,0.2) end
							if not currentfrg2==false then love.graphics.draw (currentfrg2, xspace*7.5,100,0,0.2,0.2) end
							--]]
							
								--objects
								-- Chromatic aberration
									if chromaab==false then	love.graphics.draw (previewobjects, xspace*7.5,100,0,1,1)
								elseif chromaab==true then
								love.graphics.setColor(1,0,0,0.3) love.graphics.draw (previewobjects, xspace*7.5,100,0,1,1)
								love.graphics.setColor(0,1,0,0.3) love.graphics.draw (previewobjects, xspace*7.5+5,100-5,0,1,1)
								love.graphics.setColor(0,0,1,0.3) love.graphics.draw (previewobjects, xspace*7.5+10,100-10,0,1,1)
								end
								-- 3d engine
								if threeD==true then 
									love.graphics.draw (preview3d, xspace*7.5,100,0,1,1)
								end
							
						end
				elseif skin=="retro" then love.graphics.draw (previewretrobase, xspace*7.5,100,0,1,1)
						if palette==2 then love.graphics.draw (previewretro, xspace*7.5,100,0,1,1)
					elseif palette==3 then love.graphics.draw (previewretro2, xspace*7.5,100,0,1,1)
					elseif palette==4 then love.graphics.draw (previewretro3, xspace*7.5,100,0,1,1)
					elseif palette==5 then love.graphics.draw (previewretro4, xspace*7.5,100,0,1,1)
					elseif palette==6 then love.graphics.draw (previewretro5, xspace*7.5,100,0,1,1)
					elseif palette==7 then love.graphics.draw (previewretro6, xspace*7.5,100,0,1,1)
					end
					-- 3d engine
						if threeD==true then 
						-- Chromatic aberration
								if chromaab==false then	love.graphics.draw (preview3d, xspace*7.5,100,0,1,1)
							elseif chromaab==true then
								love.graphics.setColor(1,0,0,0.3) love.graphics.draw (preview3d, xspace*7.5,100,0,1,1)
								love.graphics.setColor(0,1,0,0.3) love.graphics.draw (preview3d, xspace*7.5+5,100-5,0,1,1)
								love.graphics.setColor(0,0,1,0.3) love.graphics.draw (preview3d, xspace*7.5+10,100-10,0,1,1)
							end
						end
				elseif skin=="classic" then
						
							-- Chromatic aberration
								if chromaab==false then
									love.graphics.setColor(adjustr,adjustb,adjustb,1) love.graphics.draw (previewclassicbck, xspace*7.5,110,0,1,1)
									love.graphics.setColor(adjustr,adjustb,adjustb,1) love.graphics.draw (previewclassicbck2, xspace*7.5,110,0,1,1)
									love.graphics.setColor(adjustr,adjustb,adjustb,1) love.graphics.draw (previewclassicfrg, xspace*7.5,110,0,1,1)
								elseif chromaab==true then
									love.graphics.setColor(1,0,0,0.3) love.graphics.draw (previewclassicbck, xspace*7.5,110,0,1,1)
									love.graphics.setColor(1,0,0,0.3) love.graphics.draw (previewclassicbck2, xspace*7.5,110,0,1,1)
									love.graphics.setColor(1,0,0,0.3) love.graphics.draw (previewclassicfrg, xspace*7.5,110,0,1,1)
									
									love.graphics.setColor(0,1,0,0.3) love.graphics.draw (previewclassicbck, xspace*7.5+5,110-5,0,1,1)
									love.graphics.setColor(0,1,0,0.3) love.graphics.draw (previewclassicbck2, xspace*7.5+5,110-5,0,1,1)
									love.graphics.setColor(0,1,0,0.3) love.graphics.draw (previewclassicfrg, xspace*7.5+5,110-5,0,1,1)
									
									love.graphics.setColor(0,0,1,0.3) love.graphics.draw (previewclassicbck, xspace*7.5+10,110-10,0,1,1)
									love.graphics.setColor(0,0,1,0.3) love.graphics.draw (previewclassicbck2, xspace*7.5+10,110-10,0,1,1)
									love.graphics.setColor(0,0,1,0.3) love.graphics.draw (previewclassicfrg, xspace*7.5+10,110-10,0,1,1)
								end
					
							--objects
							-- Chromatic aberration
									if chromaab==false then love.graphics.draw (previewobjects, xspace*7.5,100,0,1,1)
								elseif chromaab==true then
								love.graphics.setColor(1,0,0,0.3) love.graphics.draw (previewobjects, xspace*7.5,100,0,1,1)
								love.graphics.setColor(0,1,0,0.3) love.graphics.draw (previewobjects, xspace*7.5+5,100-5,0,1,1)
								love.graphics.setColor(0,0,1,0.3) love.graphics.draw (previewobjects, xspace*7.5+10,100-10,0,1,1)
								end
							-- 3d engine
							if threeD==true then 
							-- Chromatic aberration
									if chromaab==false then	love.graphics.draw (preview3d, xspace*7.5,100,0,1,1)
								elseif chromaab==true then
									love.graphics.setColor(1,0,0,0.3) love.graphics.draw (preview3d, xspace*7.5,100,0,1,1)
									love.graphics.setColor(0,1,0,0.3) love.graphics.draw (preview3d, xspace*7.5+5,100-5,0,1,1)
									love.graphics.setColor(0,0,1,0.3) love.graphics.draw (preview3d, xspace*7.5+10,100-10,0,1,1)
								end	
							end
						
				end
				
				-- dialogs
				if talkies==true  then love.graphics.draw (previewdialogbase, xspace*7.5,50,0,1,1)
						if language=="en" then love.graphics.draw (previewdialogen, xspace*7.5,50,0,1,1)
					elseif language=="fr" then love.graphics.draw (previewdialogfr, xspace*7.5,50,0,1,1)
					end
						if language2=="pl" then love.graphics.draw (previewdialogpl, xspace*7.5,50,0,1,1)
					elseif language2=="es" then love.graphics.draw (previewdialoges, xspace*7.5,50,0,1,1)
					elseif language2=="nl" then love.graphics.draw (previewdialoges, xspace*7.5,50,0,1,1)
					end
				end
				if adjustr==1 and adjustg==1 and adjustb==1 then 
							else
					love.graphics.setColor(1,1,1,1)
				end
end

--[[
This is a Lua function that sets up a color palette for shader filters based on certain conditions.

The function starts with an if statement that is commented out. It checks if the variable "shader1type" is equal to "crt" and if the boolean variable "shader1" is true. If both conditions are true, it sets the variable "palette" to 1. However, this code is currently commented out, so it will not be executed.

The next set of conditions are executed with an "elseif" statement. It checks if the variable "shader1type" is equal to "gameboy" and if the boolean variable "shader1" is true. If both conditions are true, it sets the variable "palette" to 3.

Finally, there is another "elseif" statement that checks if the variable "shader1type" is equal to "godsgrid" and if the boolean variable "shader1" is true. If both conditions are true, it sets the variable "palette" to 7, and sets "shader1" to true, "shader2" to false, and "caustics" to false.

Overall, this function appears to be setting up a specific color palette for different shader types, based on certain conditions being met. However, without seeing the rest of the code, it is difficult to determine exactly how this function is being used.
--]]
function setuppaletteforshaderfilters()
			
			--if shader1type=="crt" and shader1==true then palette=1
			if shader1type=="gameboy" and shader1==true then palette=3
		elseif shader1type=="godsgrid" and shader1==true then palette=7 shader1=true shader2=false caustics=false
		end
end

function optionsloadgraphics()
		if res=="1080p" then xspace = 200 yspace = 50
	elseif res=="1440p" then xspace = 200 yspace = 50
	elseif res=="4k" then xspace = 350 yspace = 100
	else  xspace = 250 yspace = 75
	end


require ('game/interface/GraphicOptions/Tabs')

require ('game/interface/GraphicOptions/Benchmark')
require ('game/interface/GraphicOptions/ChromaticAberration')
require ('game/interface/GraphicOptions/ColorSchemes')
require ('game/interface/GraphicOptions/GraphicFullScreen')
require ('game/interface/GraphicOptions/GraphicHub')
require ('game/interface/AnalogClock/AnalogClock')
loadAnalogClock()
require ('game/interface/GraphicOptions/GraphicProfiles')
require ('game/interface/GraphicOptions/GraphicResolution')
require ('game/interface/GraphicOptions/GraphicRGBPresets')
require ('game/interface/GraphicOptions/GraphicShaders')
require ('game/interface/GraphicOptions/HDR')
require ('game/interface/GraphicOptions/limitFPS')
require ('game/interface/GraphicOptions/Skins')
require ('game/interface/GraphicOptions/threeDGraphics')
require ('game/interface/GraphicOptions/VSync')

require ('game/interface/GraphicOptions/Alignements')




if res=="1080p" or res=="1440p" then	-- reduce font size below 4k
	cscheme1Button.font=poorfishsmall
	cscheme2Button.font=poorfishsmall
	cscheme3Button.font=poorfishsmall
	cscheme4Button.font=poorfishsmall
	cscheme5Button.font=poorfishsmall
	cscheme6Button.font=poorfishsmall
	cscheme7Button.font=poorfishsmall
	threedonButton.font=poorfishsmall
	threedoffButton.font=poorfishsmall
	skinremakeButton.font=poorfishsmall
	skinretroButton.font=poorfishsmall
	skinclassicButton.font=poorfishsmall
	skinclassicnormalButton.font=poorfishsmall
	skinclassicupscaledButton.font=poorfishsmall
	hdronButton.font=poorfishsmall
	hdroffButton.font=poorfishsmall
	resol720Button.font=poorfishsmall
	resol1kButton.font=poorfishsmall
	resolqhdButton.font=poorfishsmall
	resol4kButton.font=poorfishsmall
	resol8kButton.font=poorfishsmall
	fullscreenyesButton.font=poorfishsmall
	fullscreennoButton.font=poorfishsmall
	causticsoffButton.font=poorfishsmall
	causticsonButton.font=poorfishsmall
	crtoffButton.font=poorfishsmall
	crtonButton.font=poorfishsmall
	godsgrideffectButton.font=poorfishsmall
	gameboyeffectButton.font=poorfishsmall
	bluenoiseeffectButton.font=poorfishsmall
	speccyditheringeffectButton.font=poorfishsmall
	rplusButton.font=poorfishsmall
	rminusButton.font=poorfishsmall
	gplusButton.font=poorfishsmall
	gminusButton.font=poorfishsmall
	bplusButton.font=poorfishsmall
	bminusButton.font=poorfishsmall
	resetRGBButton.font=poorfishsmall
	crimsonRGBButton.font=poorfishsmall
	seagreenRGBButton.font=poorfishsmall
	bluevioletRGBButton.font=poorfishsmall
	ChromaticAberrationOnButton.font=poorfishsmall
	ChromaticAberrationOffButton.font=poorfishsmall
end
	-- default
	previewobjects = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewobjects.webp")))
	
	-- retro
	previewretrobase = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretrobase.webp")))
	previewretro = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro.webp")))
	previewretro2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro2.webp")))
	previewretro3 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro3.webp")))
	previewretro4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro4.webp")))
	previewretro5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro5.webp")))
	previewretro6 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewretro6.webp")))
	
	--classic
	previewclassicfrg  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/level1/level1frg.webp")))
	previewclassicbck  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/level1/level1bck.webp")))
	previewclassicbck2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/level1/level1bck2.webp")))
	
	--3d engine
	preview3d = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/preview3d.webp")))
	
	--dialogs
	previewdialogbase = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewdialogbase.webp")))
	previewdialogen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewdialogen.webp")))
	previewdialogpl = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewdialogpl.webp")))
	previewdialogfr = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewdialogfr.webp")))
	previewdialoges = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/preview/previewdialoges.webp")))


	
end
--[[
This is a Lua function that uses the Love2D game engine's graphics library to draw some text on the screen. The function takes in three parameters: graphmes1 and graphmes2 are the messages to be displayed, and dt is the time since the last update (in seconds).

The function starts by setting the color to a dark blue color with some transparency and drawing a filled rectangle on the screen. Then, it sets the color to a custom color (determined by the values of adjustr, adjustg, and adjustb) and uses the poorfish font to draw the two messages on the screen at positions (400, 900) and (400, 1000), respectively. Finally, the function comments out a line of code that would set the font to poorfishsmall, but this line of code is not currently being used.
--]]
function graphicsdrawinformation(graphmes1,graphmes2,dt)
			love.graphics.setColor(0,0,0.5,0.8)	
			love.graphics.rectangle("fill",300,850,1600,250)
			love.graphics.setColor(adjustr,adjustg,adjustb,1)	
			love.graphics.setFont(poorfish) love.graphics.print (graphmes1, 400,900) 
											love.graphics.print (graphmes2, 400,1000)

			--love.graphics.setFont(poorfishsmall)
	
end

--[[
This code defines the optionsdrawgraphics function, which is responsible for drawing the graphics settings screen of a game.
--]]
function optionsdrawgraphics(dt)


 --previews
 --The first section of the code contains previews for the game graphics. If shader1 is false, it will draw a drawpreview() function. If caustics is true, it will set a shader, then draw a preview and then unset the shader. If shader1 is true, it will draw a drawpreview() function wrapped in an effect() function which will set the shader, draw the preview and unset the shader.
    if shader1==false then
		if caustics==true and not (gamestatus=="gameplusoptions") then
				love.graphics.setShader(shader)
					drawpreview()
				love.graphics.setShader()
		elseif caustics==false and not (gamestatus=="gameplusoptions") then
					drawpreview()
		end
	elseif shader1==true and not (gamestatus=="gameplusoptions") then
		effect(function()
			love.graphics.setShader(shader)
				drawpreview()
			love.graphics.setShader()
		end)
	end
	
         
		--[[The second section of the code draws the various options on the screen.
		 The love.graphics.print() function is used to print the names of each option, followed by the values associated with each option.
		The xspace variable is used to space out the options. The options include the graphic profile, color scheme, 3D, skin, upscale, resolution, fullscreen, HDR, caustic effects, 2nd effect, RGB, Chromatic aberration, Vsync, FPS, limit FPS, and battery.--]]

			
			--effects 
			
			--love.graphics.print ("FPS: ", xspace*2,fpsOnButton.y)	--moved to the HUB tab
		
			--love.graphics.print ("battery: ", xspace*2,batteryOnButton.y)		moved to the HUB tab
			
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(adjustr,adjustg,adjustb,0.3)	
				else  love.graphics.setColor(adjustr,adjustb,adjustb,1)
				end
		
	--[[This is a piece of code written in Lua that draws a graphical user interface (GUI) for a game.
	The GUI is comprised of various rectangular buttons that change color when clicked.
	The functionality of each button is as follows:	
	The color scheme buttons change the color scheme of the game. The currently selected color scheme is highlighted.--]]


	--DrawHDR()
	if gamestatus=="gameplusoptions" then
		pb:drawAgents ()
		pb:drawBlocks ()
	end
    
    drawgraphicsfocus()
end

function drawgraphicsfocus()

love.graphics.setColor(1,0,1,0.3)
		
		
							--if bfocus==nil or bfocus=="axis" then bfocus="hub"
							if bfocus=="hub" then									love.graphics.rectangle("fill",graphicshubButton.x-5,graphicshubButton.y-5,150, 30) 
						elseif bfocus=="graphicprofile" then
								if bfocus2=="low" then 								love.graphics.rectangle("fill",lowprofileButton.x-5,lowprofileButton.y-5,150, 30) 
							elseif bfocus2=="standard" then 						love.graphics.rectangle("fill",standardprofileButton.x-5,standardprofileButton.y-5,150, 30) 
							elseif bfocus2=="high" then 							love.graphics.rectangle("fill",highprofileButton.x-5,highprofileButton.y-5,150, 30) 
							end
						elseif bfocus=="colorscheme" then	
								if bfocus2=="scheme1" then 							love.graphics.rectangle("fill",cscheme1Button.x-5,cscheme1Button.y-5,150, 30) 
							elseif bfocus2=="scheme2" then 							love.graphics.rectangle("fill",cscheme2Button.x-5,cscheme2Button.y-5,150, 30) 
							elseif bfocus2=="scheme3" then 							love.graphics.rectangle("fill",cscheme3Button.x-5,cscheme3Button.y-5,150, 30) 
							elseif bfocus2=="scheme4" then 							love.graphics.rectangle("fill",cscheme4Button.x-5,cscheme4Button.y-5,150, 30) 
							elseif bfocus2=="scheme5" then 							love.graphics.rectangle("fill",cscheme5Button.x-5,cscheme5Button.y-5,150, 30) 
							elseif bfocus2=="scheme6" then 							love.graphics.rectangle("fill",cscheme6Button.x-5,cscheme6Button.y-5,150, 30) 
							elseif bfocus2=="scheme7" then 							love.graphics.rectangle("fill",cscheme7Button.x-5,cscheme7Button.y-5,150, 30) 
							end
							
						elseif bfocus=="3d" then
								if bfocus2=="on" then 								love.graphics.rectangle("fill",threedonButton.x-5,threedonButton.y-5,150, 30) 
							elseif bfocus2=="off" then 								love.graphics.rectangle("fill",threedoffButton.x-5,threedoffButton.y-5,150, 30) 
							end
							
						elseif bfocus=="skin" then
								if bfocus2=="remake" then 							love.graphics.rectangle("fill",skinremakeButton.x-5,skinremakeButton.y-5,150, 30) 
							elseif bfocus2=="retro" then 							love.graphics.rectangle("fill",skinretroButton.x-5,skinretroButton.y-5,150, 30) 
							elseif bfocus2=="classic" then 							love.graphics.rectangle("fill",skinclassicButton.x-5,skinclassicButton.y-5,150, 30) 
							end
							
						elseif bfocus=="upscale" then
								if bfocus2=="no" then 								love.graphics.rectangle("fill",skinclassicnormalButton.x-5,skinclassicnormalButton.y-5,150, 30) 
							elseif bfocus2=="yes" then 								love.graphics.rectangle("fill",skinclassicupscaledButton.x-5,skinclassicupscaledButton.y-5,150, 30) 
							end
							--hdron
							--hdroff
							--resol720
						elseif bfocus=="hdr" then
						elseif bfocus=="resolution" then
								if bfocus2=="1080p" then 							love.graphics.rectangle("fill",resol1kButton.x-5,resol1kButton.y-5,150, 30) 
							elseif bfocus2=="1440p" then 							love.graphics.rectangle("fill",resolqhdButton.x-5,resolqhdButton.y-5,150, 30) 
							elseif bfocus2=="4k" then 								love.graphics.rectangle("fill",resol4k.x-5,resol4k.y-5,150, 30) 
							end
						elseif bfocus=="fullscreen" then
								if bfocus2=="yes" then 								love.graphics.rectangle("fill",fullscreenyesButton.x-5,fullscreenyesButton.y-5,150, 30) 
							elseif bfocus2=="no" then 								love.graphics.rectangle("fill",fullscreennoButton.x-5,fullscreennoButton.y-5,150, 30) 
							end
						elseif bfocus=="caustics" then
								if bfocus2=="on" then								love.graphics.rectangle("fill",causticsonButton.x-5,causticsonButton.y-5,150, 30) 
							elseif bfocus2=="off" then								love.graphics.rectangle("fill",causticsoffButton.x-5,causticsoffButton.y-5,150, 30) 
							end
						elseif bfocus=="2ndeffect" then
								if bfocus2=="off" then 								love.graphics.rectangle("fill",crtoffButton.x-5,crtoffButton.y-5,150, 30) 
							elseif bfocus2=="CRT" then 								love.graphics.rectangle("fill",crtonButton.x-5,crtonButton.y-5,150, 30) 
							elseif bfocus2=="GodsrayGrid" then 						love.graphics.rectangle("fill",godsgrideffectButton.x-5,godsgrideffectButton.y-5,150, 30) 
							elseif bfocus2=="Gameboy" then 							love.graphics.rectangle("fill",gameboyeffectButton.x-5,gameboyeffectButton.y-5,150, 30) 
							elseif bfocus2=="BlueNoise" then 						love.graphics.rectangle("fill",bluenoiseeffectButton.x-5,bluenoiseeffectButton.y-5,150, 30) 
							elseif bfocus2=="BlueNoise2" then						love.graphics.rectangle("fill",speccyditheringeffectButton.x-5,speccyditheringeffectButton.y-5,150, 30) 
							end
						
						elseif bfocus=="RGBpresets" then
								if bfocus2=="reset" then 							love.graphics.rectangle("fill",resetRGBButton.x-5,resetRGBButton.y-5,150, 30) 
							elseif bfocus2=="crimson" then 							love.graphics.rectangle("fill",crimsonRGBButton.x-5,crimsonRGBButton.y-5,150, 30) 
							elseif bfocus2=="SeaGreen" then 						love.graphics.rectangle("fill",seagreenRGBButton.x-5,seagreenRGBButton.y-5,150, 30) 
							elseif bfocus2=="BlueViolet" then						love.graphics.rectangle("fill",bluevioletRGBButton.x-5,bluevioletRGBButton.y-5,150, 30) 
							end
						elseif bfocus=="R1" then
								if bfocus2=="+" then 								love.graphics.rectangle("fill",rplusButton.x-5,rplusButton.y-5,150, 30) 
							elseif bfocus2=="-" then 								love.graphics.rectangle("fill",rminusButton.x-5,rminusButton.y-5,150, 30) 
							end
						elseif bfocus=="G1" then
								if bfocus2=="+" then 								love.graphics.rectangle("fill",gplusButton.x-5,gplusButton.y-5,150, 30) 
							elseif bfocus2=="-" then 								love.graphics.rectangle("fill",gminusButton.x-5,gminusButton.y-5,150, 30) 
							end
						elseif bfocus=="B1" then
								if bfocus2=="+" then 								love.graphics.rectangle("fill",bplusButton.x-5,bplusButton.y-5,150, 30) 
							elseif bfocus2=="-" then								love.graphics.rectangle("fill",bminusButton.x-5,bminusButton.y-5,150, 30) 
							end
						elseif bfocus=="ChromaticAberration" then
								if bfocus2=="off" then 								love.graphics.rectangle("fill",ChromaticAberrationOffButton.x-5,ChromaticAberrationOffButton.y-5,150, 30) 
							elseif bfocus2=="on" then								love.graphics.rectangle("fill",ChromaticAberrationOnButton.x-5,ChromaticAberrationOnButton.y-5,150, 30) 
							end
						elseif bfocus=="VSync" then
								if bfocus2=="off" then 								love.graphics.rectangle("fill",vsyncOffButton.x-5,vsyncOffButton.y-5,150, 30) 
							elseif bfocus2=="on" then 								love.graphics.rectangle("fill",vsyncOnButton.x-5,vsyncOnButton.y-5,150, 30) 
							end
						elseif bfocus=="limitFPS" then
								if bfocus2=="30" then								love.graphics.rectangle("fill",fps30Button.x-5,fps30Button.y-5,150, 30) 
							elseif bfocus2=="60" then								love.graphics.rectangle("fill",fps60Button.x-5,fps60Button.y-5,150, 30) 
							elseif bfocus2=="Unlimited" then 						love.graphics.rectangle("fill",fpsnoButton.x-5,fpsnoButton.y-5,150, 30) 
							elseif bfocus2=="StartBenchmark" then					love.graphics.rectangle("fill",benchmarkButton.x-5,benchmarkButton.y-5,150, 30) 
							end
						end
		
		
		love.graphics.setColor(1,1,1,1)

end
