
function optionsloadcontrols()

ControllerEmpty= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/xboxgamepadEmpty.webp")))
ControllerDefault= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/xboxgamepadDefault.webp")))
ControllerAlternative= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/xboxgamepadAlternative.webp")))

	axiscircle = {}
	axiscircle.x = 0
	axiscircle.y = 0
	
	axiscircle2 = {}
	axiscircle2.x = 0
	axiscircle2.y = 0

-- Control options

vibrationonButton = {
	text = "",
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

vibrationoffButton = {
	text = "",
	x = xspace*4,
	y = yspace*3,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


touchonButton = {
	text = "",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	image = SwitchOFF,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

touchoffButton = {
	text = "",
	x = xspace*4,
	y = yspace*4,
	r = 0,
	sx = 1,
	image = SwitchON,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

defaultControllerLayoutButton = {
	text = "Default",
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

alternativeControllerLayoutButton = {
	text = "Alternative",
	x = xspace*5,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


testcontrollerButton = {
	text = "test gamepad",
	x = xspace*4,
	y = yspace*6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

networkcontrolonButton = {
	text = ont,
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

networkcontroloffButton = {
	text = offt,
	x = xspace*5,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

twoplayeronButton = {
	text = ont,
	x = xspace*4,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

twoplayeroffButton = {
	text = offt,
	x = xspace*5,
	y = yspace*5,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}


end

function optionsdrawcontrols(dt)
--print(controllerLayout)
 -- Controls
		love.graphics.print ("Vibration: ", vibrationonButton.x-xspace*2,vibrationonButton.y)
		love.graphics.print ("Touch controls: ", touchonButton.x-xspace*2,touchonButton.y)  
		love.graphics.print ("Controller layout: ", defaultControllerLayoutButton.x-xspace*2,defaultControllerLayoutButton.y)  
		
		love.graphics.setColor(1,1,1,0.8)
		
		if controllerLayout=="default" 		then 
			love.graphics.rectangle("fill",defaultControllerLayoutButton.x-5,defaultControllerLayoutButton.y-5,100, 50)
			if testcontroller==false or testcontroller==nil then
				love.graphics.draw(ControllerDefault,love.graphics.getWidth()/6,love.graphics.getWidth()/5,0,0.4)
			end
	elseif controllerLayout=="alternative" 	then
			love.graphics.rectangle("fill",alternativeControllerLayoutButton.x-5,alternativeControllerLayoutButton.y-5,100, 50)
			if testcontroller==false or testcontroller==nil then
				love.graphics.draw(ControllerAlternative,love.graphics.getWidth()/6,love.graphics.getWidth()/5,0,0.4)
			end
	end
	
	
	
	if  touchinterfaceison==false then
		local hovered = isButtonHovered (touchonButton)
		drawButton (touchonButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		switch_on_sound:play()
		love.timer.sleep( 0.3 )
		touchinterfaceison=true
		end
	elseif  touchinterfaceison==true then
	
		local hovered = isButtonHovered (touchoffButton)
		drawButton (touchoffButton, hovered)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				switch_off_sound:play()
				love.timer.sleep( 0.3 )
				touchinterfaceison=false
			end
	end

	
	if vibration==false then
		local hovered = isButtonHovered (vibrationonButton)
		drawButton (vibrationonButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			switch_on_sound:play()
			love.timer.sleep( 0.3 )
			vibration=true   
		end
	elseif vibration==true then
	
		local hovered = isButtonHovered (vibrationoffButton)
		drawButton (vibrationoffButton, hovered)
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
			switch_off_sound:play()
			love.timer.sleep( 0.3 )
			vibration=false   
		end
	end
	
	local hovered = isButtonHovered (defaultControllerLayoutButton)
	drawButton (defaultControllerLayoutButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
	controllerLayout="default"
	love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (alternativeControllerLayoutButton)
	drawButton (alternativeControllerLayoutButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
	controllerLayout="alternative"
	love.timer.sleep( 0.3 )
	end
	
	local hovered = isButtonHovered (testcontrollerButton)
	drawButton (testcontrollerButton, hovered)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
	testcontroller=true
	love.timer.sleep( 0.3 )
	
	end

	
	if testcontroller==true then
		if joystick then
			--keymapdraw()
			love.graphics.draw(xboxgamepad1,love.graphics.getWidth()/6,love.graphics.getWidth()/5,0,0.7)
			axiscircle.x = joystick:getGamepadAxis("leftx") * 100
			axiscircle.y = joystick:getGamepadAxis("lefty") * 100
			love.graphics.circle("line", axiscircle.x+love.graphics.getWidth()/4+50, (axiscircle.y+love.graphics.getHeight()/2)+100, 50)
				
			axiscircle2.x = joystick:getGamepadAxis("rightx") * 100
			axiscircle2.y = joystick:getGamepadAxis("righty") * 100
			love.graphics.circle("line", (axiscircle2.x+love.graphics.getWidth()/4)+420, (axiscircle2.y+love.graphics.getHeight()/2)+220, 50)
			
				if joystick:isGamepadDown("dpleft") then
					love.graphics.rectangle("fill", (love.graphics.getWidth()/4)+100, (love.graphics.getHeight()/2)+200, 50,50)
			elseif joystick:isGamepadDown("dpright") then
					love.graphics.rectangle("fill", (love.graphics.getWidth()/4)+200, (love.graphics.getHeight()/2)+200, 50,50)
			elseif joystick:isGamepadDown("dpup") then
					love.graphics.rectangle("fill", (love.graphics.getWidth()/4)+150, (love.graphics.getHeight()/2)+150, 50,50)
			elseif joystick:isGamepadDown("dpdown") then
					love.graphics.rectangle("fill", (love.graphics.getWidth()/4)+150, (love.graphics.getHeight()/2)+250, 50,50)
			elseif joystick:isGamepadDown("a") then
				love.graphics.circle("fill", (love.graphics.getWidth()/3)+335, (love.graphics.getHeight()/2)+140, 35)
			elseif joystick:isGamepadDown("x") then
				love.graphics.circle("fill", (love.graphics.getWidth()/3)+265, (love.graphics.getHeight()/2)+80, 35)
			elseif joystick:isGamepadDown("b") then
				love.graphics.circle("fill", (love.graphics.getWidth()/3)+400, (love.graphics.getHeight()/2)+80, 35)
			elseif joystick:isGamepadDown("y") then
				love.graphics.circle("fill", (love.graphics.getWidth()/3)+335, (love.graphics.getHeight()/2)+20, 35)
			
			end
		end
	end
	

	
end
