function loadAnalogClock()


    -- Current time
    time = os.date("*t")

    -- Clock background
    clockRadius = 100
    --clockCenter = { x = love.graphics.getWidth() / 2, y = love.graphics.getHeight() / 2 }
    clockCenter = { x = love.graphics.getWidth()-100, y = 100 }

    -- Hands lengths
    hourHandLength = clockRadius * 0.5
    minuteHandLength = clockRadius * 0.8
    secondHandLength = clockRadius * 0.9
    
    font12 = love.graphics.newFont(12)
   
   
end

function updateAnalogClock(dt)
    -- Update the current time
    time = os.date("*t")

end

function drawAnalogClock()
    -- Draw clock face
    love.graphics.setColor(0,0,0,0.5)
    love.graphics.circle("fill", clockCenter.x, clockCenter.y, clockRadius)
    love.graphics.setColor(1,1,1,1)
    love.graphics.circle("line", clockCenter.x, clockCenter.y, clockRadius)
  

   drawHourNumbersAnalogClock()

	drawMinutesLinesAnalogClock()
    
    -- Display time information
    drawDateAnalagClock()

    
  drawClockHands()
    
    
    love.graphics.setFont(poorfish)
    
    
end

function drawHourNumbersAnalogClock()
    -- Draw hour numbers
    for hour = 1, 12 do
        local angle = math.rad((hour - 3) * 30)
        local x, y

        -- Invert the numbers on the bottom half of the clock
        if hour <= 2 or hour > 8 then
            x = clockCenter.x + (clockRadius - 20) * math.cos(angle)
            y = clockCenter.y + (clockRadius - 20) * math.sin(angle)

            love.graphics.print(tostring(hour), x, y, angle + math.rad(90), 1, 1, font12:getWidth(tostring(hour)) / 2, font12:getHeight() / 2)
        elseif hour > 2 and hour <= 8 then
            x = clockCenter.x + (clockRadius - 20) * math.cos(angle)
            y = clockCenter.y + (clockRadius - 20) * math.sin(angle)
            love.graphics.print(tostring(hour), x, y, angle + math.rad(-90), 1, 1, font12:getWidth(tostring(hour)) / 2, font12:getHeight() / 2)
        end
    end
end

function drawMinutesLinesAnalogClock()
-- Draw minute lines
    for minute = 0, 59 do
        local angle = math.rad((minute / 5 - 3) * 30)
        local lineLength = minute % 5 == 0 and 10 or 5
        local x1 = clockCenter.x + (clockRadius - lineLength) * math.cos(angle)
        local y1 = clockCenter.y + (clockRadius - lineLength) * math.sin(angle)
        local x2 = clockCenter.x + clockRadius * math.cos(angle)
        local y2 = clockCenter.y + clockRadius * math.sin(angle)
        love.graphics.line(x1, y1, x2, y2)
    end

end

function drawClockHands()
    -- Draw clock hands
    love.graphics.setColor(1,1,1,1)
    drawHand(clockCenter.x, clockCenter.y, time.hour % 12 / 12 * 360, hourHandLength, 10)
    drawHand(clockCenter.x, clockCenter.y, time.min / 60 * 360, minuteHandLength, 5)
    drawHand(clockCenter.x, clockCenter.y, time.sec / 60 * 360, secondHandLength, 2)
end




function drawHand(x, y, angle, length, width)
    local radianAngle = math.rad(angle - 90)
    local x2 = x + length * math.cos(radianAngle)
    local y2 = y + length * math.sin(radianAngle)

    if not nLevel==15 then
		love.graphics.setLineWidth(width)
	 else 
		love.graphics.setLineWidth(width/3)
	 end
    love.graphics.line(x, y, x2, y2)
end


function drawDateAnalagClock()
     -- Display date with day of the week and month name in German
    local germanDayOfWeek = {
        ["Monday"] = "Montag",
        ["Tuesday"] = "Dienstag",
        ["Wednesday"] = "Mittwoch",
        ["Thursday"] = "Donnerstag",
        ["Friday"] = "Freitag",
        ["Saturday"] = "Samstag",
        ["Sunday"] = "Sonntag"
    }
    
    local englishMonth = {
        ["January"] = "Jan",
        ["February"] = "Feb",
        ["March"] = "March",
        ["April"] = "Apr",
        ["May"] = "May",
        ["June"] = "Jun",
        ["July"] = "July",
        ["August"] = "Aug",
        ["September"] = "Sep",
        ["October"] = "Oct",
        ["November"] = "Nov",
        ["December"] = "Dec"
    }

    local germanMonth = {
        ["January"] = "Jan",
        ["February"] = "Feb",
        ["March"] = "März",
        ["April"] = "Apr",
        ["May"] = "Mai",
        ["June"] = "Jun",
        ["July"] = "Jul",
        ["August"] = "Aug",
        ["September"] = "Sep",
        ["October"] = "Okt",
        ["November"] = "Nov",
        ["December"] = "Dez"
    }



    
    love.graphics.setColor(0.5,0.5,1,1)
    --love.graphics.print(string.format("%02d:%02d:%02d", time.hour, time.min, time.sec), clockCenter.x - 60, clockCenter.y - 10)
    love.graphics.print(string.format("%02d:%02d", time.hour, time.min), clockCenter.x - 60, clockCenter.y - 20)

    -- Display date
    --local dateString = string.format("%04d-%02d-%02d", time.year, time.month, time.day)
    
    --English
    local WeekString = string.format(os.date("%A"))
    local dateString = string.format("%02d %s, %04d", time.day, englishMonth[os.date("%B")], time.year)
    --local dateString = string.format("%02d %s, %04d", time.day , os.date("%B"),  time.year)
    
    --Deutsch  
    --local WeekString = germanDayOfWeek[os.date("%A")]
    --local dateString = string.format("%02d %s, %04d", time.day, germanMonth[os.date("%B")], time.year)
    
    love.graphics.setColor(0,1,1,1)
    love.graphics.setFont(poorfishsmall)
    love.graphics.print(WeekString, clockCenter.x - 60, clockCenter.y + 20)
    
	love.graphics.setColor(1,1,0,1)
	love.graphics.setFont(font12)
    love.graphics.print(dateString, clockCenter.x - 40, clockCenter.y + 40)
end
