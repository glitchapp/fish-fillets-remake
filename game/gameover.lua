--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This function appears to define the game over screen for a game. It initializes different arrays for game over messages in English, Spanish, and French. It also defines a restartButton table, which contains properties such as text, position, and appearance that will be used to display a restart button on the game over screen. However, the function does not actually display the game over screen or the restart button, so it is likely that other parts of the code are responsible for calling and implementing this function.
--]]
function loadgameover()
	
gameoverdarn_en={"Darn!",
"Shhh, children can be playing this game!",
}
gameoverparrot_en={"Aye, caramba!",
"But there is no parrot here!",
"Shut up, you are dead.",
}
gameoverhereafter_en={"Hellooo? Can you hear me hereafter? Is there life after death?",
"I don’t know. I don’t think so.",
}

gameoverdarn_es={"Oh, mier...!",
"Shht, ¡Niños pueden estar jugando este juego!",
}
gameoverparrot_es={"Aye, caramba!",
"¡Pero no hay ningún perico aquí!",
"Cállate, estás muerto.",
}
gameoverhereafter_es={"¿Holaaa? ¿Puedes oirme por ahí? ¿Hay vida después de la muerte?",
"No lo sé. No lo creo.",
}

gameoverdarn_fr={"Oh, mer.. !",
"Chut, des enfants peuvent jouer à ce jeu !",
}

gameoverparrot_fr={"Aye, caramba!",
"Mais il n'y a pas de perroquet ici !",
"La ferme, tu es mort.",
}
gameoverhereafter_fr={"Saluuuut ? Pouvez-vous m'entendre de l'au-delà ? Y a-t-il une vie après la mort ?",
"Je ne sais pas. Je ne crois pas.",
}

	restartButton = {
		text = "Restart",
		x = 500,
		y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	--buttons

end

--[[
This is a Lua script that defines a function called loadgameoversentences(). When called, this function loads a game over sentence that will be played as an audio sound.

The function first creates a gameoversentence variable that loads an audio file called "empty.ogg" from a specific path using the love.audio.newSource() function. This audio file will be played if none of the game over conditions are met.

After that, the function checks whether the variable fish1status is equal to "dead" and whether the variable gameoversentencesaid is false. If these conditions are true, the function chooses a random number between 0 and 7 (inclusive) using math.random() and assigns it to the aleatorygameoversentence variable.

Then, if the language2 variable is "en" and the accent2 variable is "br", the function selects an audio file for the game over sentence based on the value of aleatorygameoversentence. If aleatorygameoversentence is 0, for example, it loads an audio file called "10somuchforhim.ogg" from a specific path. If aleatorygameoversentence is 1, it loads an audio file called "102ohno.ogg", and so on.

If accent2 is "us", the function loads the same audio files, but from a different path.

If aleatorygameoversentence is 6, the function also sets the mousestate variable to the opposite of its current value (i.e., true if it's currently false, or vice versa), hides the mouse pointer using love.mouse.setVisible(), and calls the changelevel() function if timer2 is greater than 6.

Finally, if aleatorygameoversentence is 7, the function loads three audio files ("2aycaramba.ogg", "16butthereis.ogg", and "17shutup.ogg") and assigns them to three variables (aycarambago, buthereisnoparrot, and shutupyouaredead).

Based on the code, it seems that the purpose is to load game over sentences in different languages and accents for a game developed using the Lua-based LÖVE game engine.

If the selected sentence requires a repeat of the level, the code also hides the mouse cursor, changes the level, and plays the selected audio source.
--]]

function updategameover()
	if aleatorygameoversentence==6 and timer2>6 then
		--repeat level
		mousestate = not love.mouse.isVisible()	-- hide mouse pointer
		--love.mouse.setVisible(mousestate)
		--changelevel()
		mousestate = not love.mouse.isVisible()	-- hide mouse pointer
		love.mouse.setVisible(mousestate)
		changelevel()
		fish1status="idle"
		fish2status="idle"
		gamestatus="game" 
	end

end

function loadgameoversentences()
gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/border/empty.ogg","stream" )
	-- if big fish is dead
	if fish1status=="dead" and gameoversentencesaid==false then
		if language2=="en" then
				if accent2=="br" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,7)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/10somuchforhim.ogg","stream" )		--	"So much for him."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/102ohno.ogg","stream" )				--	"Oh, nooo."
					elseif aleatorygameoversentence==2 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/11andnowiamalone.ogg","stream" )	--	"And now I am alone."
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/12thatmusthavehurt.ogg","stream" ) 	--	"That must have hurt."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/13hedidnt.ogg","stream" )			--	"He didn’t deserve such a terrible end…"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/14soletsstart.ogg","stream" )		--	"So, let’s start again…"
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/25iamnotgoing.ogg","stream" )		-- "I am not going to solve it alone. Don’t trouble yourself with restart, I’ll do it myself."
															
					elseif aleatorygameoversentence==7 then	aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
															buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
															shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end -- end aleatorygameoversentence
			elseif accent2=="us" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,7) 
							--aleatorygameoversentence = 6 print(aleatorygameoversentence)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/10somuchforhim.ogg","stream" )		--	"So much for him."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/102ohno.ogg","stream" )				--	"Oh, nooo."
					elseif aleatorygameoversentence==2 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/11andnowiamalone.ogg","stream" )	--	"And now I am alone."
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/12thatmusthavehurt.ogg","stream" ) 	--	"That must have hurt."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/13hedidnt.ogg","stream" )			--	"He didn’t deserve such a terrible end…"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/14soletsstart.ogg","stream" )		--	"So, let’s start again…"
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/25iamnotgoing.ogg","stream" )		--  "I am not going to solve it alone. Don’t trouble yourself with restart, I’ll do it myself."
														--repeat level
														timer2=0
					elseif aleatorygameoversentence==7 then	aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
															buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
															shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
			end	-- end accent2
		end	-- end language2
	if language2=="pl" then
					math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,7) 
							--aleatorygameoversentence = 6 print(aleatorygameoversentence)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/10somuchforhim.ogg","stream" )			--	"So much for him."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/102ohno.ogg","stream" )				--	"Oh, nooo."
					elseif aleatorygameoversentence==2 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/11andnowiamalone.ogg","stream" )		--	"And now I am alone."
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/12thatmusthavehurt.ogg","stream" ) 	--	"That must have hurt."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/13hedidnt.ogg","stream" )				--	"He didn’t deserve such a terrible end…"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/14soletsstart.ogg","stream" )			--	"So, let’s start again…"
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/pl/black/25iamnotgoing.ogg","stream" )			--  "I am not going to solve it alone. Don’t trouble yourself with restart, I’ll do it myself."
														--repeat level
														timer2=0
					elseif aleatorygameoversentence==7 then	aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
															buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/pl/black/16butthereis.ogg","stream" )			--  "But there is no parrot here!"
															shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
					
	end -- end fish1status dead
	
	
	
	-- if small fish is dead
	
	if fish2status=="dead" and gameoversentencesaid==false then
		if language=="en" then
				if accent=="br" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
																shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shhh.ogg","stream" )		-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/7didsheknow.ogg","stream" )	--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg.ogg","stream" )			-- "Luckily, it’s possible to restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==12 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==13 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==14 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==15 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
					
			elseif accent=="us" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
																--	shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/7didsheknow.ogg","stream" )		--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	--end aleatorygameoversentence
			end	--end accent
	
		elseif language=="es" then
		
				if accent=="es" then
			elseif accent=="la" then
						math.randomseed(os.time())
						aleatorygameoversentence = math.random(0,26)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/1ohshoot.ogg","stream" )			-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then	--darn = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )							--	"Darn."
					--										shhchildren = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/2shhh.ogg","stream" )					-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/4dang.ogg","stream" )				--	"Dang!"
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/scheiße.ogg","stream" )				--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/6hmmandnow.ogg","stream" )			--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/7didsheknow.ogg","stream" )			--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/8terrible.ogg","stream" )			--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/9whatcanbe.ogg","stream" )			--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/20fortunately.ogg","stream" )	-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/21restartthelevel.ogg","stream" ) -- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/23canyouhearmehereafter.ogg","stream" ) -- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) 	-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/26andsheisgone.ogg","stream" )	-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	-- end aleatorygameoversentence
			end -- end accent
			
			elseif language=="fr" then
							math.randomseed(os.time())
							aleatorygameoversentence = math.random(0,15)
						if aleatorygameoversentence==0 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==1 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/1ohshoot.ogg","stream" )		-- "Oh, shoot!"
					elseif aleatorygameoversentence==2 then		--	  darngo = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
																--	shhchildren = love.audio.newSource( "/externalassets/dialogs/share/fr/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
					elseif aleatorygameoversentence==3 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==4 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/4dang.ogg","stream" )			--	"Dang!"
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Darn."
					elseif aleatorygameoversentence==5 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )			--	"Scheisse!
					elseif aleatorygameoversentence==6 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/6hmmandnow.ogg","stream" )		--	"Hmmm... and now I am alone."
					elseif aleatorygameoversentence==7 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/7didsheknow.ogg","stream" )		--  "Did she know she has such a sexy skeleton?"
					elseif aleatorygameoversentence==8 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/8terrible.ogg","stream" )		--	"Terrible end of the little fish…"	
					elseif aleatorygameoversentence==9 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/9whatcanbe.ogg","stream" )		--	"What can be the constant of decay of the fish skeleton?"
					elseif aleatorygameoversentence==10 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/20fortunately.ogg","stream" )		-- "Fortunately, we can restart the level."
					elseif aleatorygameoversentence==11 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/21restartthelevel.ogg","stream" )	-- "Restart the level! The afterlife is too tedious."
					elseif aleatorygameoversentence==12 then	--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/23hello.ogg","stream" )				-- "Hellooo? Can you hear me hereafter? Is there life after death?"
																--gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/24idontknow.ogg","stream" )			-- "I don’t know. I don’t think so."
					elseif aleatorygameoversentence==13 then	gameoversentence = love.audio.newSource( "/externalassets/dialogs/share/fr/black/26andsheisgone.ogg","stream" )		-- "And she is gone. I’m sure you liked her as much as I did. Certainly, you have nothing against restarting the level."
					elseif aleatorygameoversentence==14 then	--buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/fr/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
																--shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/fr/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
					end	--end aleatorygameoversentence
			
			end	--end language
	end	-- end fish2status dead
	
		
--[[
This section of the code handles different scenarios for when the fish in the game die. It first checks which fish has died (big or small), and then selects a pre-recorded voice clip to play based on the language and accent chosen in the game settings.

For example, if the big fish dies, and the game is set to English (US accent), it will play two voice clips: one saying "But there is no parrot here!" and the other saying "Shut up, you are dead."

Similarly, if the small fish dies and the game is set to Spanish (Latin American accent), it will play a voice clip saying "Darn" and another saying "Shhh, children can be playing this game!".

If a particular game over scenario has already played once (gameoversentencesaid=true), it will not play again.
--]]
			if talkies==true then
					-- if big fish dies
					if fish1status=="dead" and aleatorygameoversentence==14 and gameoversentencesaid==false then
						stepdone=0
						aycarambago = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )				--	"Aye, caramba!"
							if language=="en" then
									if accent=="br" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
								elseif accent=="us" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
								end	-- end accent
							elseif language=="es" then
									if accent=="es" then
								elseif accnet=="la" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/16butthereisnoparrot.ogg","stream" )	--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/shutupyouaredead.ogg","stream" )	--  "Shut up, you are dead."
								end	-- end accent
							elseif language=="fr" then
										buthereisnoparrot = love.audio.newSource( "/externalassets/dialogs/share/fr/black/16butthereis.ogg","stream" )		--  "But there is no parrot here!"
										shutupyouaredead = love.audio.newSource( "/externalassets/dialogs/share/fr/black/17shutup.ogg","stream" )			--  "Shut up, you are dead."
									Talkies.clearMessages()
									Obey.gameoverparrot()
							end	--end language
					-- if small fish dies
				elseif fish2status=="dead" and aleatorygameoversentence==2 and gameoversentencesaid==false then print("second")
							stepdone=0
							if language=="en" then
								if accent=="br" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/en/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								elseif accent=="us" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								end	-- end accent
							elseif language=="es" then
								if accent=="es" then
								elseif accent=="la" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								end	-- end accent
							elseif language=="fr" then
									darngo = love.audio.newSource( "/externalassets/dialogs/share/fr/black/3darn.ogg","stream" )				--	"Darn."
									shhchildren = love.audio.newSource( "/externalassets/dialogs/share/fr/black/2shh.ogg","stream" )			-- "Shhh, children can be playing this game!"
								Talkies.clearMessages()
								Obey.gameoverdarn()
							end	--end language
				-- if small fish dies
				elseif fish2status=="dead" and aleatorygameoversentence==12 and gameoversentencesaid==false then
						stepdone=0
							if language=="en" then
								if accent=="br" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) -- "I don’t know. I don’t think so."
								elseif accent=="us" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) 			-- "I don’t know. I don’t think so."
								end	--end accent
							elseif language=="es" then
								if accent=="es" then
								elseif accent=="la" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/es-la/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/en-us/black/24idontknow.ogg","stream" ) -- "I don’t know. I don’t think so."
								end --end accent
							elseif language=="fr" then
									canyouhearmehere = love.audio.newSource( "/externalassets/dialogs/share/fr/black/23hello.ogg","stream" )			-- "Hellooo? Can you hear me hereafter? Is there life after death?"
									idontknowidontthinkso = love.audio.newSource( "/externalassets/dialogs/share/fr/black/24idontknow.ogg","stream" ) 			-- "I don’t know. I don’t think so."
								Talkies.clearMessages()
								Obey.gameoverhereafter()
							end	 --end language
				else
				
					gameoversentence:setEffect('myEffect')
					gameoversentence:play()
				end --end of fishstatus
			
			end --end of talkies==true
				
			gameoversentencesaid=true		
end



function drawButtonFocusGameOver()
		--if joystick then 
			buttonfocusanimations()		-- focus color and size animations
			if bfocus=="restartgameover" 	then love.graphics.rectangle ('line', restartButton.x, restartButton.y, fwidth, fheight)		--fwidth 
			end
		--end
end

--The third function, drawgameover, does not take any input parameters. It calls the drawdefaultgameovertext() function to draw the default game over text. However, the code for the drawdefaultgameovertext() function is not provided in the code snippet.
function drawgameover()
			drawdefaultgameovertext()
			drawButtonFocusGameOver() 
			love.graphics.setColor(1,1,1,1)
end

--[[
This function, drawdefaultgameovertext(), is responsible for drawing the "Game Over" screen when the game ends.

It first disables several visual effects and enables a box blur effect. Then, it sets a shader and draws all the game content using this shader. After that, it sets the color of some variables and sets an audio effect for the music based on the current level of the game.

Finally, it draws the "Game Over" text in the center of the screen and a "Restart" button below it. It checks whether the mouse is hovering over the button and if the left mouse button is clicked, it stops the music, sets the reverb effect, changes the BPM of the music, changes the game status to "levelselection" and resets the status of the fish in the game.
--]]
function drawdefaultgameovertext()
			--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
			
			love.graphics.setShader()
			effect(function()
	
				drawallcontent()
				drawforshader2()
			end)
			scolor1={1,0.5,0.5}
			scolor2={1,0.5,0.5}
			scolor3={1,0,0}
			scolor4={1,0,0}
			--audio effect
			--setcompressoreffect()
			if not (nLevel==15 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==32 or nLevel==34 or nLevel==38  or nLevel==44 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==63 or nLevel==66 or nLevel==67 or nLevel==68 or nLevel==69 or nLevel==70) then 
				mus:setEffect('myEffect')
				mus:setVolume(0.3)
			elseif not (nLevel==68) then music:setVolume(0.3)
			end
			
			
			love.graphics.print("Game over",600,300,0, 0.8, 1)
			--love.graphics.print("Time:" .. timeneeded,600,400,0, 0.8, 1)
			--love.graphics.print("Steps:" .. steps,600,500,0, 0.8, 1)
			
						-- continue

				hovered = isButtonHovered (restartButton)
				drawButton (restartButton, hovered,"Restart")
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
					if love.mouse.isDown(1) then 
						if not (nLevel==15 or nLevel==20 or nLevel==26 or nLevel==30 or nLevel==32 or nLevel==34 or nLevel==44 or nLevel==38 or nLevel==52 or nLevel==54 or nLevel==56 or nLevel==60 or nLevel==61 or nLevel==63 or nLevel==66 or nLevel==67 or nLevel==68 or nLevel==69 or nLevel==70) then 
							mus:stop()
							mus:setVolume(1)
						elseif not (nLevel==68) then music:stop() music:setVolume(1)
						end
						shader2=false
						gameoversentencesaid=false
						--[[
						setreverbeffect()
						mybpm=112
						lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
						effect.enable("scanlines","crt","glow","filmgrain")
						love.timer.sleep( 0.2 )
						--mus:setVolume(1)
						gamestatus="levelselection" 
						--]]
						mousestate = not love.mouse.isVisible()	-- hide mouse pointer
						love.mouse.setVisible(mousestate)
						changelevel()
						fish1status="idle"
						fish2status="idle"
						gamestatus="game" 
					end
				end
end

--[[
This function is a part of a larger program that involves dialogue between two characters, a big fish and a small fish, upon a game over event. The function starts by loading two images (webp files) which will be used as avatars for the two characters.

Next, it sets the font for the text displayed in the dialogue box based on the chosen language (English, Russian, Slovenian, or Chinese). It also sets the text to be displayed for the "small fish" and "big fish" variables depending on the language.

Then, the function uses the Talkies library to display two separate dialogues, one for the big fish and one for the small fish. The dialogues consist of a single message for each character, which is stored in an array (gameoverdarn_en) and chosen based on the language selected. The Talkies library also allows for avatar images and a sound effect to be associated with each character's dialogue box.

Overall, this function adds a bit of personality to the game over event by giving the two characters a chance to react to it and speak to each other.
--]]
function Obey.gameoverdarn()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	gameoverdarn_en[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverdarn_en[2],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

--The function Obey.gameoverparrot() displays a dialogue using the Talkies library. It sets three avatar images (avatar, avatar2, parrotavatar) and selects a font based on the language variable. If language is "chi", it sets the variables smallfisht and bigfisht to "小鱼" and "大鱼", respectively; otherwise, it sets them to "small fish" and "Big fish". The function then displays three lines of dialogue, spoken by "Parrot", "Big fish", and "small fish", respectively, using the Talkies.say() function. Each line of dialogue includes an image and a talk sound.
function Obey.gameoverparrot()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  parrotavatar = love.graphics.newImage("/externalassets/objects/level45/parrotavatar.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Parrot",
    { 
    	gameoverparrot_en[1],
    },
    {
      image=parrotavatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

Talkies.say( "Big fish",
    { 
    	gameoverparrot_en[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverparrot_en[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

--The function Obey.gameoverhereafter() is similar to Obey.gameoverparrot(), but it displays only two lines of dialogue, spoken by "Big fish" and "small fish", respectively. It sets two avatar images (avatar, avatar2) and selects a font based on the language variable. If language is "chi", it sets the variables smallfisht and bigfisht to "小鱼" and "大鱼", respectively; otherwise, it sets them to "small fish" and "Big fish". The function then displays two lines of dialogue, spoken by "Big fish" and "small fish", respectively, using the Talkies.say() function. Each line of dialogue includes an image and a talk sound.
function Obey.gameoverhereafter()
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.webp")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.webp")
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	gameoverhereafter_en[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	gameoverhereafter_en[2],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

