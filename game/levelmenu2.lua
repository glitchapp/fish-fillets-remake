
function levelload2()
	
	thumb=thumb1

	font1 = love.graphics.newFont(12) -- standard font
	font3 = love.graphics.newFont(12*3) -- three times bigger

	levelmap= love.graphics.newImage("/assets/levels/levelselection.webp")
	sphere= love.graphics.newImage("/assets/levels/sphere.webp")

	if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
	elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
	elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends" returnareatext="Return Area 1"
	elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" poorfishmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" poorfishmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" poorfishmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
	end

--buttons

	exitButton = {
		text = exittext,
		x = 1000,
		y = 10, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	musicButton = {
		text = musictext,
		x = 100,
		y = 10, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	creditsButton = {
		text = creditstext,
		x = 100,
		y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	optionsButton = {
		text = optionstext,
		x = 		900,
		y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	leditorButton = {
	text = leditortext,
	x = 1500,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	extrasButton = {
	text = extrastext,
	x = 1500,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	endButton = {
	text = endtext,
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}


	returnArea1Button = {
	text = returnareatext,
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	require("assets/text/levelmenu/level-en")
end



--buttons


local function drawTile (x, y, tileSize)
	local color = {0.2, 0.2, 0.2}
	if (x+y)%2 == 0 then
		color = {0.3, 0.3, 0.3}
	end
	love.graphics.setColor (color)
	love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
end

local function drawCenteredText (rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end


local function drawCenteredSprite (x, y, tileSize, sprite)
--	x and y it tiles, [1,1] is top left
	local w, h = sprite:getDimensions()
	local scale = tileSize/math.max (w, h)
	love.graphics.draw (sprite, (x-0.5)*tileSize, (y-0.5)*tileSize, 0, scale, scale, w/2, h/2)
end


local spheres = {
-- 0. test (8 levels)
	{level= 80, x=13, y=2},
	{level= 81, x=13, y=3},
	{level= 82, x=13, y=4},
	{level= 83, x=13, y=5},
	{level= 84, x=13, y=6},
	{level= 85, x=13, y=7},
	{level= 86, x=13, y=8},
	{level= 87, x=13, y=9},

}


-- function draw selection circle synced to the music
	
function drawfishhousesphere(x,y)
				if y>1 and y<10 and x==13 then
					love.graphics.setColor(0.5,0.5,0)
					love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
					love.graphics.setColor(1,1,1)
					love.graphics.print (zonetext[12],500, 850,0,1) 
					love.graphics.print ((y-1) .. '.' ..test10[y-1],500, 900,0,1) 
					--love.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
				end
end


function leveldraw2()
	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	local width, height = love.graphics.getDimensions()
	
	-- to make constant tilesize:
	--tileSize = 50

	-- to make the spheres adaptive to window size:
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)



	local mx, my = love.mouse.getPosition()

	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1



	for i, spherelevel in ipairs (spheres) do
	
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y

			love.graphics.setColor(0,1,0)
			drawCenteredSprite (x, y, tileSize, sphere)
			love.graphics.setFont(poorfishsmall)
			love.graphics.setColor(0,0,0)
			drawCenteredText ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize, level)
		end
	end


	--shadows
	local newBody = Body:new(newLightWorld)
	newBody:SetPosition(x*tileSize, y*tileSize)

	for i, spherelevel in ipairs (spheres) do
		local level = spherelevel.level
		local x1, y1 = spherelevel.x*tileSize, spherelevel.y*tileSize
		CircleShadow:new(newBody, x1*tileSize, y1*tileSize, 16) 
	end
	


	local sphereHovered
		
	for i, spherelevel in ipairs (spheres) do
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x1, y1 = spherelevel.x, spherelevel.y
			if x1 == x and y1 == y then
				sphereHovered = spherelevel
				break
			end
		end
	end

	if sphereHovered then
		local level = sphereHovered.level
		local x1, y1 = sphereHovered.x, sphereHovered.y
		
		love.graphics.setColor(1,1,0)
		drawCenteredSprite (x1, y1, tileSize, sphere)

		love.graphics.setColor(1,1,1)
		drawCenteredText ((x1-1)*tileSize, (y1-1)*tileSize, tileSize, tileSize, level)
			
		if love.mouse.isDown(1) and unlockedlevels[level]==true then
			nLevel=level
			changelevel() 
			gamestatus="game"
			timer=0
			stepdone=0
		end
	end

		drawonlytitles(x,y)
		--drawshadows(x,y,newBody)

	
------------------------------------------------
-- buttons
------------------------------------------------




	-- Return Area 1

	local hovered = isButtonHovered (returnArea1Button)
	drawButton (returnArea1Button, hovered,returnareatext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
				music:stop()
				levelmenu = require ('game/levelmenu')			-- load level selection menu
				levelload()
				gamestatus="levelselection"
		end
	end



	-- quit

	local hovered = isButtonHovered (exitButton)
	drawButton (exitButton, hovered,exittext)

	if hovered and love.mouse.isDown(1) then 
		love.timer.sleep( 0.5 )
		love.event.quit()
	end


	-- options

	hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then 
			gamestatus="options" 
		end
	end


	--credits

	hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			love.timer.sleep( 0.2 )
			music:stop()
			music = love.audio.newSource( "/externalassets/music/pixelsphere/003_Vaporwareloop.ogg","static" )
				love.audio.play( music )
				music:setVolume(0.4)
			gamestatus="credits" 
		end
	end
	
	--Music player
if musicplayerunlocked==true then
	hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			music:stop()
			love.timer.sleep( 0.5 )
			gamestatus="music"
		end
	end
end

	--Level editor
if leveleditorunlocked==true then
	hovered = isButtonHovered (leditorButton)
	drawButton (leditorButton, hovered,leditortext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			music:stop()
			gamestatus="leditor"
		end
	end
end	
	-- extras
if extrasunlocked==true then
	hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			 music:stop()
			gamestatus="extras" 
		end
	end
end	

if endmenuunlocked==true then	
	-- Ends

	hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			  music:stop()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
		end
	end
end

end

function drawonlytitles(x,y)
-- change shadow light from green to yellow when mouse hover spheres
	--[[if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end
	--]]
		love.graphics.setFont(poorfishmiddle)
	  --Fish House levels 1-8 
		if x>10 and x<16 and y>0 and y<12 and unlockedlevels[y-1]==true then drawfishhousesphere(x,y) end

end

function drawshadows(x,y,newBody)


	-- change shadow light from green to yellow when mouse hover spheres
	if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end

  --shadows --0. Fish House levels 1-8 
    if x>10 and x<16 and y>0 and y<12 then
        for i=1, 8, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, i*2*1)
			CircleShadow:new(newBody, 0, 20+i*50*1, 16)
		end
			drawfishhousesphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
				--shadows 1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then
			for i=1, 11, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 110)
				CircleShadow:new(newBody, 300+i*50*1, 110, 16)
			end
			drawshipwreckssphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
			
		end
	
			     --shadows 2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then 
			for i=1, 7, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 260)
				CircleShadow:new(newBody, 300+i*50*1, 260, 16)
			end            
			drawsilvershipsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
	       --shadows 3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then 
			for i=1, 10, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 140)
				CircleShadow:new(newBody, 315-i*50*1, 140, 16)
			end
			drawcitydeepsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
		--shadows 	4. UFO	--level 52-58
	if y>8 and y<10 and x>4 and x<14 then
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(315-i*0.5, 210)
			CircleShadow:new(newBody, 315-i*50*1, 210, 16)
		end 
		drawufosphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
	    --shadows 5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 then 
			for i=1, 8, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawcoralreefsphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 then
			for i=1, 4, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawtreasurecavesphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 7. Dump--level 38-44
      if x>12 and x<14 and y>8 and y<18 then 
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, 200+i*2*1)
			CircleShadow:new(newBody, 0, 200+i*50*1, 16)
		end
		drawdumpsphere(x,y)

       else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
       end
	
		 --shadows 8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then
			for i=1, 6, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 160)
				CircleShadow:new(newBody, 300+i*50*1, 160, 16)
			end
			drawsecretcomputersphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
                
        end

end
