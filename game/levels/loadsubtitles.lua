function loadsubtitles()

	if language=="en" then borescript=borescript_en
elseif language=="es" then borescript=borescript_es
elseif language=="fr" then borescript=borescript_fr
end

--level 1
	
if nLevel==1 then

	require("game/dialogs/talkies1")

	--talkies=true
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l1en")	
			elseif language=="bg" then require ("game/dialogs/bg/l1bg")
			elseif language=="br" then require ("game/dialogs/br/l1br")	
			elseif language=="ch" then require ("game/dialogs/ch/l1ch")	
			elseif language2=="cs" then require ("game/dialogs/cs/l1cs")	
			elseif language=="da" then require ("game/dialogs/da/l1da")
			elseif language=="de" then require ("game/dialogs/de/l1de")
			elseif language=="eo" then require ("game/dialogs/eo/l1eo")
			elseif language=="es" then require ("game/dialogs/es/l1es")
			elseif language=="fi" then require ("game/dialogs/fi/l1fi")
			elseif language=="gr" then require ("game/dialogs/gr/l1gr")
			elseif language=="is" then require ("game/dialogs/is/l1is")
			elseif language=="it" then require ("game/dialogs/it/l1it")
			elseif language=="fr" then require ("game/dialogs/fr/l1fr")
			elseif language=="no" then require ("game/dialogs/no/l1no")
			
			elseif language=="jp" then require ("game/dialogs/jp/l1jp")
			elseif language2=="nl" then require ("game/dialogs/nl/l1nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l1pl")
			elseif language=="pt" then require ("game/dialogs/pt/l1pt")
			elseif language=="ru" then require ("game/dialogs/ru/l1ru")
			elseif language=="sv" then require ("game/dialogs/sv/l1sv")
			elseif language=="sk" then require ("game/dialogs/sl/l1sl")
			elseif language=="thai" then require ("game/dialogs/th/l1th")
			elseif language=="chi" then require ("game/dialogs/chi/l1chi")
			end
			
			-- trigger subtitles
							--timer=0
							--stepdone=0
							if pipefalled==true then
								if talkies==true then Talkies.clearMessages()
									Obey.lev1()
								end
							end
			
		

			end

elseif nLevel==2 then

	require("game/dialogs/talkies2")
	
--talkies=true
		
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l2en")	
				--if language=="bg" then require ("game/dialogs/es/l2bg")
			--elseif language=="br" then require ("game/dialogs/es/l2br")	
			--elseif language=="ch" then require ("game/dialogs/es/l2ch")	
			elseif language2=="cs" then require ("game/dialogs/cs/l2cs")	
			elseif language=="da" then require ("game/dialogs/da/l2da")
			elseif language=="de" then require ("game/dialogs/de/l2de")
			--elseif language=="eo" then require ("game/dialogs/es/l2eo")
			elseif language=="es" then require ("game/dialogs/es/l2es")
			elseif language=="fi" then require ("game/dialogs/fi/l2fi")
			elseif language=="gr" then require ("game/dialogs/gr/l2gr")
			elseif language=="is" then require ("game/dialogs/is/l2is")
			elseif language=="it" then require ("game/dialogs/it/l2it")
			elseif language=="fr" then require ("game/dialogs/fr/l2fr")
			elseif language=="no" then require ("game/dialogs/no/l2no")
			
			--elseif language=="it" then require ("game/dialogs/es/l2it")
			elseif language=="jp" then require ("game/dialogs/jp/l2jp")
			elseif language2=="nl" then require ("game/dialogs/nl/l2nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l2pl")
			elseif language=="pt" then require ("game/dialogs/pt/l2pt")
			elseif language=="ru" then require ("game/dialogs/ru/l2ru")
			elseif language=="sv" then require ("game/dialogs/sv/l2sv")
			--elseif language=="sk" then require ("game/dialogs/es/l2sk")
			elseif language=="thai" then require ("game/dialogs/th/l2th")
			elseif language=="chi" then require ("game/dialogs/chi/l2chi")
			end
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev2() 
							end
	end
	
	
elseif nLevel==3 then


	require("game/dialogs/talkies3")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l3en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l3ch")	
			elseif language=="chi" then require ("game/dialogs/chi/l3chi")
			elseif language=="da" then require ("game/dialogs/da/l3da")
			elseif language=="fi" then require ("game/dialogs/fi/l3fi")
			elseif language=="gr" then require ("game/dialogs/gr/l3gr")
			elseif language=="is" then require ("game/dialogs/is/l3is")
			elseif language=="it" then require ("game/dialogs/it/l3it")
			elseif language=="jp" then require ("game/dialogs/jp/l3jp")
			elseif language=="no" then require ("game/dialogs/no/l3no")
			elseif language=="pt" then require ("game/dialogs/pt/l3pt")
			elseif language=="th" then require ("game/dialogs/th/l3th")
			elseif language2=="cs" then require ("game/dialogs/cs/l3cs")	
			elseif language=="de" then require ("game/dialogs/de/l3de")
			--elseif language=="eo" then require ("game/dialogs/es/l3eo")
			elseif language=="es" then require ("game/dialogs/es/l3es")
			elseif language=="fr" then require ("game/dialogs/fr/l3fr")
			
			elseif language2=="nl" then require ("game/dialogs/nl/l3nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l3pl")
			--elseif language=="pr" then require ("game/dialogs/es/l3pr")
			elseif language=="ru" then require ("game/dialogs/ru/l3ru")
			elseif language=="sv" then require ("game/dialogs/sv/l3sv")
			elseif language=="thai" then require ("game/dialogs/th/l3th")
			--elseif language=="sk" then require ("game/dialogs/es/l3sk")
			end
					-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev3()
							end
	end
	
elseif nLevel==4 then


	require("game/dialogs/talkies4")
		
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l4en")	
				--if language=="bg" then require ("game/dialogs/es/l4bg")
			--elseif language=="br" then require ("game/dialogs/es/l4br")	
			--elseif language=="ch" then require ("game/dialogs/es/l4ch")
			elseif language=="chi" then require ("game/dialogs/chi/l4chi")
			elseif language=="da" then require ("game/dialogs/da/l4da")
			elseif language=="fi" then require ("game/dialogs/fi/l4fi")
			elseif language=="gr" then require ("game/dialogs/gr/l4gr")
			elseif language=="is" then require ("game/dialogs/is/l4is")
			elseif language=="it" then require ("game/dialogs/it/l4it")
			elseif language=="jp" then require ("game/dialogs/jp/l4jp")
			elseif language=="no" then require ("game/dialogs/no/l4no")
			elseif language=="pt" then require ("game/dialogs/pt/l4pt")
			elseif language=="th" then require ("game/dialogs/th/l4th")
			elseif language2=="cs" then require ("game/dialogs/cs/l4cs")	
			elseif language=="de" then require ("game/dialogs/de/l4de")
			--elseif language=="eo" then require ("game/dialogs/es/l4eo")
			elseif language=="es" then require ("game/dialogs/es/l4es")
			elseif language=="fr" then require ("game/dialogs/fr/l4fr")
			
			elseif language2=="nl" then require ("game/dialogs/nl/l4nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l4pl")
			--elseif language=="pr" then require ("game/dialogs/es/l4pr")
			elseif language=="ru" then require ("game/dialogs/ru/l4ru")
			elseif language=="sv" then require ("game/dialogs/sv/l4sv")
			 elseif language=="thai" then require ("game/dialogs/th/l4th")
			--elseif language=="sk" then require ("game/dialogs/es/l4sk")
			end
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev4()
							end
		end
elseif nLevel==5 then

require("game/dialogs/talkies5")
			
				if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l5en")	
				--if language=="bg" then require ("game/dialogs/es/l5bg")
			--elseif language=="br" then require ("game/dialogs/es/l5br")	
			--elseif language=="ch" then require ("game/dialogs/es/l5ch")	
			elseif language2=="cs" then require ("game/dialogs/cs/l5cs")
			elseif language=="chi" then require ("game/dialogs/chi/l5chi")
			elseif language=="da" then require ("game/dialogs/da/l5da")
			elseif language=="fi" then require ("game/dialogs/fi/l5fi")
			elseif language=="gr" then require ("game/dialogs/gr/l5gr")
			elseif language=="is" then require ("game/dialogs/is/l5is")
			elseif language=="it" then require ("game/dialogs/it/l5it")
			elseif language=="jp" then require ("game/dialogs/jp/l5jp")
			elseif language=="no" then require ("game/dialogs/no/l5no")
			elseif language=="pt" then require ("game/dialogs/pt/l5pt")
			elseif language=="th" then require ("game/dialogs/th/l5th")
			elseif language=="de" then require ("game/dialogs/de/l5de")
			--elseif language=="eo" then require ("game/dialogs/es/l5eo")
			elseif language=="es" then require ("game/dialogs/es/l5es")
			elseif language=="fr" then require ("game/dialogs/fr/l5fr")
			
			elseif language2=="nl" then require ("game/dialogs/nl/l5nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l5pl")
			--elseif language=="pr" then require ("game/dialogs/es/l5pr")
			elseif language=="ru" then require ("game/dialogs/ru/l5ru")
			elseif language=="sv" then require ("game/dialogs/sv/l5sv")
			elseif language=="thai" then require ("game/dialogs/th/l5th")
			--elseif language=="sk" then require ("game/dialogs/es/l5sk")
			end
						
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev5()
							end
			end


elseif nLevel==6 then

	require("game/dialogs/talkies6")
					
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l6en")	
				--if language=="bg" then require ("game/dialogs/es/l6bg")
			--elseif language=="br" then require ("game/dialogs/es/l6br")	
			--elseif language=="ch" then require ("game/dialogs/es/l6ch")	
			elseif language2=="cs" then require ("game/dialogs/cs/l6cs")
			elseif language=="chi" then require ("game/dialogs/chi/l6chi")
			elseif language=="da" then require ("game/dialogs/da/l6da")
			elseif language=="fi" then require ("game/dialogs/fi/l6fi")
			elseif language=="gr" then require ("game/dialogs/gr/l6gr")
			elseif language=="is" then require ("game/dialogs/is/l6is")
			elseif language=="it" then require ("game/dialogs/it/l6it")
			elseif language=="jp" then require ("game/dialogs/jp/l6jp")
			elseif language=="no" then require ("game/dialogs/no/l6no")
			elseif language=="pt" then require ("game/dialogs/pt/l6pt")
			elseif language=="th" then require ("game/dialogs/th/l6th")
			elseif language=="de" then require ("game/dialogs/de/l6de")
			--elseif language=="eo" then require ("game/dialogs/es/l6eo")
			elseif language=="es" then require ("game/dialogs/es/l6es")
			elseif language=="fr" then require ("game/dialogs/fr/l6fr")

			elseif language2=="nl" then require ("game/dialogs/nl/l6nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l6pl")
			--elseif language=="pr" then require ("game/dialogs/es/l6pr")
			elseif language=="ru" then require ("game/dialogs/ru/l6ru")
			elseif language=="sv" then require ("game/dialogs/sv/l6sv")
			 elseif language=="thai" then require ("game/dialogs/th/l6th")
			--elseif language=="sk" then require ("game/dialogs/es/l6sk")
			end
		
			-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev6()
							end
			end

elseif nLevel==7 then

	require("game/dialogs/talkies7")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l7en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")	
			elseif language2=="cs" then require ("game/dialogs/cs/l7cs")
			elseif language=="chi" then require ("game/dialogs/chi/l7chi")
			elseif language=="da" then require ("game/dialogs/da/l7da")
			elseif language=="fi" then require ("game/dialogs/fi/l7fi")
			elseif language=="gr" then require ("game/dialogs/gr/l7gr")
			elseif language=="is" then require ("game/dialogs/is/l7is")
			elseif language=="it" then require ("game/dialogs/it/l7it")
			elseif language=="jp" then require ("game/dialogs/jp/l7jp")
			elseif language=="no" then require ("game/dialogs/no/l7no")
			elseif language=="pt" then require ("game/dialogs/pt/l7pt")
			elseif language=="th" then require ("game/dialogs/th/l7th")
			elseif language=="de" then require ("game/dialogs/de/l7de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l7es")
			elseif language=="fr" then require ("game/dialogs/fr/l7fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l7nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l7pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l7sv")
			 elseif language=="thai" then require ("game/dialogs/th/l7th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
			
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev7()
							end
							
					matresspushed=false
				
			end


elseif nLevel==8 then

	require("game/dialogs/talkies8")
			
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l8en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l8chi")
			elseif language=="da" then require ("game/dialogs/da/l8da")
			elseif language=="fi" then require ("game/dialogs/fi/l8fi")
			elseif language=="gr" then require ("game/dialogs/gr/l8gr")
			elseif language=="is" then require ("game/dialogs/is/l8is")
			elseif language=="it" then require ("game/dialogs/it/l8it")
			elseif language=="jp" then require ("game/dialogs/jp/l8jp")
			elseif language=="no" then require ("game/dialogs/no/l8no")
			elseif language=="pt" then require ("game/dialogs/pt/l8pt")
			elseif language=="th" then require ("game/dialogs/th/l8th")
			elseif language2=="cs" then require ("game/dialogs/cs/l8cs")
			
			elseif language=="de" then require ("game/dialogs/de/l8de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l8es")
			elseif language=="fr" then require ("game/dialogs/fr/l8fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l8nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l8pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l8sv")
			 elseif language=="thai" then require ("game/dialogs/th/l8th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev8()
							end
			
		
				
			end


elseif nLevel==9 then

	require("game/dialogs/talkies9")
			
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l9en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l9chi")
			elseif language=="da" then require ("game/dialogs/da/l9da")
			elseif language=="fi" then require ("game/dialogs/fi/l9fi")
			elseif language=="gr" then require ("game/dialogs/gr/l9gr")
			elseif language=="is" then require ("game/dialogs/is/l9is")
			elseif language=="it" then require ("game/dialogs/it/l9it")
			elseif language=="jp" then require ("game/dialogs/jp/l9jp")
			elseif language=="no" then require ("game/dialogs/no/l9no")
			elseif language=="pt" then require ("game/dialogs/pt/l9pt")
			elseif language=="th" then require ("game/dialogs/th/l9th")
			elseif language2=="cs" then require ("game/dialogs/cs/l9cs")	
			elseif language=="de" then require ("game/dialogs/de/l9de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l9es")
			elseif language=="fr" then require ("game/dialogs/fr/l9fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l9nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l9pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l9sv")
			 elseif language=="thai" then require ("game/dialogs/th/l9th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev9()
							end

		
		end
		


elseif nLevel==10 then

	require("game/dialogs/talkies10")
			
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l10en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l10chi")
			elseif language=="da" then require ("game/dialogs/da/l10da")
			elseif language=="fi" then require ("game/dialogs/fi/l10fi")
			elseif language=="gr" then require ("game/dialogs/gr/l10gr")
			elseif language=="is" then require ("game/dialogs/is/l10is")
			elseif language=="it" then require ("game/dialogs/it/l10it")
			elseif language=="jp" then require ("game/dialogs/jp/l10jp")
			elseif language=="no" then require ("game/dialogs/no/l10no")
			elseif language=="pt" then require ("game/dialogs/pt/l10pt")
			elseif language=="th" then require ("game/dialogs/th/l10th")
			elseif language2=="cs" then require ("game/dialogs/cs/l10cs")	
			elseif language=="de" then require ("game/dialogs/de/l10de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l10es")
			elseif language=="fr" then require ("game/dialogs/fr/l10fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l10nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l10pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l10sv")
			 elseif language=="thai" then require ("game/dialogs/th/l10th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
			Obey.lev10()
			
	
		end


elseif nLevel==11 then

require("game/dialogs/talkies11")
			
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l11en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l11chi")
			elseif language=="da" then require ("game/dialogs/da/l11da")
			elseif language=="fi" then require ("game/dialogs/fi/l11fi")
			elseif language=="gr" then require ("game/dialogs/gr/l11gr")
			elseif language=="is" then require ("game/dialogs/is/l11is")
			elseif language=="it" then require ("game/dialogs/it/l11it")
			elseif language=="jp" then require ("game/dialogs/jp/l11jp")
			elseif language=="no" then require ("game/dialogs/no/l11no")
			elseif language=="pt" then require ("game/dialogs/pt/l11pt")
			elseif language=="th" then require ("game/dialogs/th/l11th")
			elseif language2=="cs" then require ("game/dialogs/cs/l11cs")	
			elseif language=="de" then require ("game/dialogs/de/l11de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l11es")
			elseif language=="fr" then require ("game/dialogs/fr/l11fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l11nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l11pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l11sv")
			 elseif language=="thai" then require ("game/dialogs/th/l11th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
			Obey.lev11()
			

			
		end


elseif nLevel==12 then

	require("game/dialogs/talkies12")
				
				if talkies==true then
				Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l12en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l12chi")
			elseif language=="da" then require ("game/dialogs/da/l12da")
			elseif language=="fi" then require ("game/dialogs/fi/l12fi")
			elseif language=="gr" then require ("game/dialogs/gr/l12gr")
			elseif language=="is" then require ("game/dialogs/is/l12is")
			elseif language=="it" then require ("game/dialogs/it/l12it")
			elseif language=="jp" then require ("game/dialogs/jp/l12jp")
			elseif language=="no" then require ("game/dialogs/no/l12no")
			elseif language=="pt" then require ("game/dialogs/pt/l12pt")
			elseif language=="th" then require ("game/dialogs/th/l12th")
			elseif language2=="cs" then require ("game/dialogs/cs/l12cs")	
			elseif language=="de" then require ("game/dialogs/de/l12de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l12es")
			elseif language=="fr" then require ("game/dialogs/fr/l12fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l12nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l12pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l12sv")
			 elseif language=="thai" then require ("game/dialogs/th/l12th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
			Obey.lev12()
				
		end
	
		
		elseif nLevel==13 then
		
			require("game/dialogs/talkies13")
				
				if talkies==true then
				Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l13en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l13chi")
			elseif language=="da" then require ("game/dialogs/da/l13da")
			elseif language=="fi" then require ("game/dialogs/fi/l13fi")
			elseif language=="gr" then require ("game/dialogs/gr/l13gr")
			elseif language=="is" then require ("game/dialogs/is/l13is")
			elseif language=="it" then require ("game/dialogs/it/l13it")
			elseif language=="jp" then require ("game/dialogs/jp/l13jp")
			elseif language=="no" then require ("game/dialogs/no/l13no")
			elseif language=="pt" then require ("game/dialogs/pt/l13pt")
			elseif language=="th" then require ("game/dialogs/th/l13th")
			elseif language2=="cs" then require ("game/dialogs/cs/l13cs")	
			elseif language=="de" then require ("game/dialogs/de/l13de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l13es")
			elseif language=="fr" then require ("game/dialogs/fr/l13fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l13nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l13pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l13sv")
			 elseif language=="thai" then require ("game/dialogs/th/l13th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
			Obey.lev13()
		end
		
		elseif nLevel==14 then
		
			require("game/dialogs/talkies14")
			
				if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l14en")	
				--if language=="bg" then require ("game/dialogs/es/l15bg")
			--elseif language=="br" then require ("game/dialogs/es/l15br")	
			--elseif language=="ch" then require ("game/dialogs/es/l15ch")
			elseif language=="chi" then require ("game/dialogs/chi/l14chi")
			elseif language=="da" then require ("game/dialogs/da/l14da")
			elseif language=="fi" then require ("game/dialogs/fi/l14fi")
			elseif language=="gr" then require ("game/dialogs/gr/l14gr")
			elseif language=="is" then require ("game/dialogs/is/l14is")
			elseif language=="it" then require ("game/dialogs/it/l14it")
			elseif language=="jp" then require ("game/dialogs/jp/l14jp")
			elseif language=="no" then require ("game/dialogs/no/l14no")
			elseif language=="pt" then require ("game/dialogs/pt/l14pt")
			elseif language=="th" then require ("game/dialogs/th/l14th")
			elseif language2=="cs" then require ("game/dialogs/cs/l14cs")	
			elseif language=="de" then require ("game/dialogs/de/l14de")
			--elseif language=="eo" then require ("game/dialogs/es/l15eo")
			elseif language=="es" then require ("game/dialogs/es/l14es")
			elseif language=="fr" then require ("game/dialogs/fr/l14fr")
				
			--elseif language=="it" then require ("game/dialogs/es/l15it")
			elseif language2=="nl" then require ("game/dialogs/nl/l14nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l14pl")
			--elseif language=="pr" then require ("game/dialogs/es/l15pr")
			elseif language=="ru" then require ("game/dialogs/ru/l14ru")
			elseif language=="sv" then require ("game/dialogs/sv/l14sv")
			 elseif language=="thai" then require ("game/dialogs/th/l14th")
			--elseif language=="sk" then require ("game/dialogs/es/l15sk")
			end
			
			
			
			
				
				Obey.lev14() end
		
		elseif nLevel==15 then
		
		require("game/dialogs/talkies15")
			
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l15en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l7ch")
			elseif language=="chi" then require ("game/dialogs/chi/l15chi")
			elseif language=="da" then require ("game/dialogs/da/l15da")
			elseif language=="fi" then require ("game/dialogs/fi/l15fi")
			elseif language=="gr" then require ("game/dialogs/gr/l15gr")
			elseif language=="is" then require ("game/dialogs/is/l15is")
			elseif language=="it" then require ("game/dialogs/it/l15it")
			elseif language=="jp" then require ("game/dialogs/jp/l15jp")
			elseif language=="no" then require ("game/dialogs/no/l15no")
			elseif language=="pt" then require ("game/dialogs/pt/l15pt")
			elseif language=="th" then require ("game/dialogs/th/l15th")
			elseif language2=="cs" then require ("game/dialogs/cs/l15cs")	
			elseif language=="de" then require ("game/dialogs/de/l15de")
			--elseif language=="eo" then require ("game/dialogs/es/l7eo")
			elseif language=="es" then require ("game/dialogs/es/l15es")
			elseif language=="fr" then require ("game/dialogs/fr/l15fr")
				
			--elseif language=="it" then require ("game/dialogs/es/l7it")
			elseif language2=="nl" then require ("game/dialogs/nl/l15nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l15pl")
			--elseif language=="pr" then require ("game/dialogs/es/l7pr")
			elseif language=="ru" then require ("game/dialogs/ru/l7ru")
			elseif language=="sv" then require ("game/dialogs/sv/l15sv")
			 elseif language=="thai" then require ("game/dialogs/th/l15th")
			--elseif language=="sk" then require ("game/dialogs/es/l7sk")
			end
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev15()
							end
			end
		
		elseif nLevel==16 then
		
		 	require("game/dialogs/talkies16")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l16en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l3chi")
			elseif language=="da" then require ("game/dialogs/da/l16da")
			elseif language=="fi" then require ("game/dialogs/fi/l16fi")
			elseif language=="gr" then require ("game/dialogs/gr/l16gr")
			elseif language=="is" then require ("game/dialogs/is/l16is")
			elseif language=="it" then require ("game/dialogs/it/l16it")
			elseif language=="jp" then require ("game/dialogs/jp/l16jp")
			elseif language=="no" then require ("game/dialogs/no/l16no")
			elseif language=="pt" then require ("game/dialogs/pt/l16pt")
			elseif language=="th" then require ("game/dialogs/th/l16th")
			elseif language2=="cs" then require ("game/dialogs/cs/l16cs")	
			elseif language=="de" then require ("game/dialogs/de/l16de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l16es")
			elseif language=="fr" then require ("game/dialogs/fr/l16fr")
				
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l16nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l16pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l16ru")
			elseif language=="sv" then require ("game/dialogs/sv/l16sv")
			 elseif language=="thai" then require ("game/dialogs/th/l16th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev16()
							end
			
			
		end
		
		elseif nLevel==17 then
		
		
			      	require("game/dialogs/talkies17")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l17en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l17chi")
			elseif language=="da" then require ("game/dialogs/da/l17da")
			elseif language=="fi" then require ("game/dialogs/fi/l17fi")
			elseif language=="gr" then require ("game/dialogs/gr/l17gr")
			elseif language=="is" then require ("game/dialogs/is/l17is")
			elseif language=="it" then require ("game/dialogs/it/l17it")
			elseif language=="jp" then require ("game/dialogs/jp/l17jp")
			elseif language=="no" then require ("game/dialogs/no/l17no")
			elseif language=="pt" then require ("game/dialogs/pt/l17pt")
			elseif language=="th" then require ("game/dialogs/th/l17th")
			elseif language2=="cs" then require ("game/dialogs/cs/l17cs")	
			elseif language=="de" then require ("game/dialogs/de/l17de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l17es")
			elseif language=="fr" then require ("game/dialogs/fr/l17fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l17nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l17pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l16ru")
			elseif language=="sv" then require ("game/dialogs/sv/l17sv")
			 elseif language=="thai" then require ("game/dialogs/th/l17th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			Obey.lev17()
		end
			
		elseif nLevel==18 then
		
			  	require("game/dialogs/talkies18")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l18en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l18chi")
			elseif language=="da" then require ("game/dialogs/da/l18da")
			elseif language=="fi" then require ("game/dialogs/fi/l18fi")
			elseif language=="gr" then require ("game/dialogs/gr/l18gr")
			elseif language=="is" then require ("game/dialogs/is/l18is")
			elseif language=="it" then require ("game/dialogs/it/l18it")
			elseif language=="jp" then require ("game/dialogs/jp/l18jp")
			elseif language=="no" then require ("game/dialogs/no/l18no")
			elseif language=="pt" then require ("game/dialogs/pt/l18pt")
			elseif language=="th" then require ("game/dialogs/th/l18th")
			elseif language2=="cs" then require ("game/dialogs/cs/l18cs")
			elseif language=="de" then require ("game/dialogs/de/l18de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l18es")
			elseif language=="fr" then require ("game/dialogs/fr/l18fr")
				
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l18nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l18pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l16ru")
			elseif language=="sv" then require ("game/dialogs/sv/l18sv")
			 elseif language=="thai" then require ("game/dialogs/th/l18th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			Obey.lev18()
		end
		
		elseif nLevel==19 then
		
				require("game/dialogs/talkies19")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l19en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l19chi")
			elseif language=="da" then require ("game/dialogs/da/l19da")
			elseif language=="fi" then require ("game/dialogs/fi/l19fi")
			elseif language=="gr" then require ("game/dialogs/gr/l19gr")
			elseif language=="is" then require ("game/dialogs/is/l19is")
			elseif language=="it" then require ("game/dialogs/it/l19it")
			elseif language=="jp" then require ("game/dialogs/jp/l19jp")
			elseif language=="no" then require ("game/dialogs/no/l19no")
			elseif language=="pt" then require ("game/dialogs/pt/l19pt")
			elseif language=="th" then require ("game/dialogs/th/l19th")
			elseif language2=="cs" then require ("game/dialogs/cs/l19cs")
			elseif language=="de" then require ("game/dialogs/de/l19de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l19es")
			elseif language=="fr" then require ("game/dialogs/fr/l19fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l19nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l19pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l16ru")
			elseif language=="sv" then require ("game/dialogs/sv/l19sv")
			 elseif language=="thai" then require ("game/dialogs/th/l19th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			Obey.lev19()
		end
		
		elseif nLevel==20 then
		
			require("game/dialogs/talkies20")
		
		  	if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l20en")	
				--if language=="bg" then require ("game/dialogs/es/l20bg")
			--elseif language=="br" then require ("game/dialogs/es/l20br")	
			--elseif language=="ch" then require ("game/dialogs/es/l20ch")
			elseif language=="chi" then require ("game/dialogs/chi/l20chi")
			elseif language=="da" then require ("game/dialogs/da/l20da")
			elseif language=="fi" then require ("game/dialogs/fi/l20fi")
			elseif language=="gr" then require ("game/dialogs/gr/l20gr")
			elseif language=="is" then require ("game/dialogs/is/l20is")
			elseif language=="it" then require ("game/dialogs/it/l20it")
			elseif language=="jp" then require ("game/dialogs/jp/l20jp")
			elseif language=="no" then require ("game/dialogs/no/l20no")
			elseif language=="pt" then require ("game/dialogs/pt/l20pt")
			elseif language=="th" then require ("game/dialogs/th/l20th")
			elseif language2=="cs" then require ("game/dialogs/cs/l20cs")	
			elseif language=="de" then require ("game/dialogs/de/l20de")
			--elseif language=="eo" then require ("game/dialogs/es/l20eo")
			elseif language=="es" then require ("game/dialogs/es/l20es")
			elseif language=="fr" then require ("game/dialogs/fr/l20fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l20it")
			elseif language2=="nl" then require ("game/dialogs/nl/l20nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l20pl")
			--elseif language=="pr" then require ("game/dialogs/es/l20pr")
			elseif language=="ru" then require ("game/dialogs/ru/l20ru")
			elseif language=="sv" then require ("game/dialogs/sv/l20sv")
			 elseif language=="thai" then require ("game/dialogs/th/l20th")
			--elseif language=="sk" then require ("game/dialogs/es/l20sk")
			end
			music:setVolume(0.6)
				love.audio.setEffect('myEffect', {type = 'reverb'})
			
		
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev20()
							end
			end
		
		elseif nLevel==21 then
		
			   	require("game/dialogs/talkies21")
						if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l21en")	
				--if language=="bg" then require ("game/dialogs/es/l21bg")
			--elseif language=="br" then require ("game/dialogs/es/l21br")	
			--elseif language=="ch" then require ("game/dialogs/es/l21ch")
			elseif language=="chi" then require ("game/dialogs/chi/l21chi")
			elseif language=="da" then require ("game/dialogs/da/l21da")
			elseif language=="fi" then require ("game/dialogs/fi/l21fi")
			elseif language=="gr" then require ("game/dialogs/gr/l21gr")
			elseif language=="is" then require ("game/dialogs/is/l21is")
			elseif language=="it" then require ("game/dialogs/it/l21it")
			elseif language=="jp" then require ("game/dialogs/jp/l21jp")
			elseif language=="no" then require ("game/dialogs/no/l21no")
			elseif language=="pt" then require ("game/dialogs/pt/l21pt")
			elseif language=="th" then require ("game/dialogs/th/l21th")
			elseif language2=="cs" then require ("game/dialogs/cs/l21cs")	
			elseif language=="de" then require ("game/dialogs/de/l21de")
			--elseif language=="eo" then require ("game/dialogs/es/l21eo")
			elseif language=="es" then require ("game/dialogs/es/l21es")
			elseif language=="fr" then require ("game/dialogs/fr/l21fr")

			elseif language2=="nl" then require ("game/dialogs/nl/l21nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l21pl")
			--elseif language=="pr" then require ("game/dialogs/es/l21pr")
			elseif language=="ru" then require ("game/dialogs/ru/l21ru")
			elseif language=="sv" then require ("game/dialogs/sv/l21sv")
			 elseif language=="thai" then require ("game/dialogs/th/l21th")
			--elseif language=="sk" then require ("game/dialogs/es/l21sk")
			end
			
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev21()
							end
		end
		
		elseif nLevel==22 then
		
			require("game/dialogs/talkies22")
			
			if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l22en")	
				--if language=="bg" then require ("game/dialogs/es/l22bg")
			--elseif language=="br" then require ("game/dialogs/es/l22br")	
			--elseif language=="ch" then require ("game/dialogs/es/l22ch")
			elseif language=="chi" then require ("game/dialogs/chi/l22chi")
			elseif language=="da" then require ("game/dialogs/da/l22da")
			elseif language=="fi" then require ("game/dialogs/fi/l22fi")
			elseif language=="gr" then require ("game/dialogs/gr/l22gr")
			elseif language=="is" then require ("game/dialogs/is/l22is")
			elseif language=="it" then require ("game/dialogs/it/l22it")
			elseif language=="jp" then require ("game/dialogs/jp/l22jp")
			elseif language=="no" then require ("game/dialogs/no/l22no")
			elseif language=="pt" then require ("game/dialogs/pt/l22pt")
			elseif language=="th" then require ("game/dialogs/th/l22th")
			elseif language2=="cs" then require ("game/dialogs/cs/l22cs")	
			elseif language=="de" then require ("game/dialogs/de/l22de")
			--elseif language=="eo" then require ("game/dialogs/es/l22eo")
			elseif language=="es" then require ("game/dialogs/es/l22es")
			elseif language=="fr" then require ("game/dialogs/fr/l22fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l22it")
			elseif language2=="nl" then require ("game/dialogs/nl/l22nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l22pl")
			--elseif language=="pr" then require ("game/dialogs/es/l22pr")
			elseif language=="ru" then require ("game/dialogs/ru/l22ru")
			elseif language=="sv" then require ("game/dialogs/sv/l22sv") 
			elseif language=="thai" then require ("game/dialogs/th/l22th")
			--elseif language=="sk" then require ("game/dialogs/es/l22sk")
			end
		Obey.lev22()
		
		end
		elseif nLevel==23 then
		
		  require("game/dialogs/talkies23")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l23en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l23chi")
			elseif language=="da" then require ("game/dialogs/da/l23da")
			elseif language=="fi" then require ("game/dialogs/fi/l23fi")
			elseif language=="gr" then require ("game/dialogs/gr/l23gr")
			elseif language=="is" then require ("game/dialogs/is/l23is")
			elseif language=="it" then require ("game/dialogs/it/l23it")
			elseif language=="jp" then require ("game/dialogs/jp/l23jp")
			elseif language=="no" then require ("game/dialogs/no/l23no")
			elseif language=="pt" then require ("game/dialogs/pt/l23pt")
			elseif language=="th" then require ("game/dialogs/th/l23th")
			elseif language2=="cs" then require ("game/dialogs/cs/l23cs")	
			elseif language=="de" then require ("game/dialogs/de/l23de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l23es")
			elseif language=="fr" then require ("game/dialogs/fr/l23fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l23nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l23pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l23ru")
			elseif language=="sv" then require ("game/dialogs/sv/l23sv") 
			elseif language=="thai" then require ("game/dialogs/th/l23th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev23()
							end
		
		end
		
		
		
		elseif nLevel==24 then
		
			require("game/dialogs/talkies24")
			
			if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l24en")	
				--if language=="bg" then require ("game/dialogs/es/l24bg")
			--elseif language=="br" then require ("game/dialogs/es/l24br")	
			--elseif language=="ch" then require ("game/dialogs/es/l24ch")
			elseif language=="chi" then require ("game/dialogs/chi/l24chi")
			elseif language=="da" then require ("game/dialogs/da/l24da")
			elseif language=="fi" then require ("game/dialogs/fi/l24fi")
			elseif language=="gr" then require ("game/dialogs/gr/l24gr")
			elseif language=="is" then require ("game/dialogs/is/l24is")
			elseif language=="it" then require ("game/dialogs/it/l24it")
			elseif language=="jp" then require ("game/dialogs/jp/l24jp")
			elseif language=="no" then require ("game/dialogs/no/l24no")
			elseif language=="pt" then require ("game/dialogs/pt/l24pt")
			elseif language=="th" then require ("game/dialogs/th/l24th")
			elseif language2=="cs" then require ("game/dialogs/cs/l24cs")	
			elseif language=="de" then require ("game/dialogs/de/l24de")
			--elseif language=="eo" then require ("game/dialogs/es/l24eo")
			elseif language=="es" then require ("game/dialogs/es/l24es")
			elseif language=="fr" then require ("game/dialogs/fr/l24fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l24it")
			elseif language2=="nl" then require ("game/dialogs/nl/l24nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l24pl")
			--elseif language=="pr" then require ("game/dialogs/es/l24pr")
			elseif language=="ru" then require ("game/dialogs/ru/l24ru")
			elseif language=="sv" then require ("game/dialogs/sv/l24sv")
			 elseif language=="thai" then require ("game/dialogs/th/l24th")
			--elseif language=="sk" then require ("game/dialogs/es/l24sk")
			end
		
		
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev24()
							end
			end
		
		elseif nLevel==25 then
		
		   require("game/dialogs/talkies25")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l25en")
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l25chi")
			elseif language=="da" then require ("game/dialogs/da/l25da")
			elseif language=="fi" then require ("game/dialogs/fi/l25fi")
			elseif language=="gr" then require ("game/dialogs/gr/l25gr")
			elseif language=="is" then require ("game/dialogs/is/l25is")
			elseif language=="it" then require ("game/dialogs/it/l25it")
			elseif language=="jp" then require ("game/dialogs/jp/l25jp")
			elseif language=="no" then require ("game/dialogs/no/l25no")
			elseif language=="pt" then require ("game/dialogs/pt/l25pt")
			elseif language=="th" then require ("game/dialogs/th/l25th")
			elseif language2=="cs" then require ("game/dialogs/cs/l25cs")
			elseif language=="de" then require ("game/dialogs/de/l25de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l25es")
			elseif language=="fr" then require ("game/dialogs/fr/l25fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l25nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l25pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l25ru")
			elseif language=="sv" then require ("game/dialogs/sv/l25sv")
			 elseif language=="thai" then require ("game/dialogs/th/l25th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			
					-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev25()
							end
		end
		
		elseif nLevel==26 then
		
			  require("game/dialogs/talkies26")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l26en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l26chi")
			elseif language=="da" then require ("game/dialogs/da/l26da")
			elseif language=="fi" then require ("game/dialogs/fi/l26fi")
			elseif language=="gr" then require ("game/dialogs/gr/l26gr")
			elseif language=="is" then require ("game/dialogs/is/l26is")
			elseif language=="it" then require ("game/dialogs/it/l26it")
			elseif language=="jp" then require ("game/dialogs/jp/l26jp")
			elseif language=="no" then require ("game/dialogs/no/l26no")
			elseif language=="pt" then require ("game/dialogs/pt/l26pt")
			elseif language=="th" then require ("game/dialogs/th/l26th")
			elseif language2=="cs" then require ("game/dialogs/cs/l26cs")	
			elseif language=="de" then require ("game/dialogs/de/l26de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l26es")
			elseif language=="fr" then require ("game/dialogs/fr/l26fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l26nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l26pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l26ru")
			elseif language=="sv" then require ("game/dialogs/sv/l26sv")
			 elseif language=="thai" then require ("game/dialogs/th/l26th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev26()
							end
			
		end
		elseif nLevel==27 then
		
		
			  require("game/dialogs/talkies27")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l27en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l27chi")
			elseif language=="da" then require ("game/dialogs/da/l27da")
			elseif language=="fi" then require ("game/dialogs/fi/l27fi")
			elseif language=="gr" then require ("game/dialogs/gr/l27gr")
			elseif language=="is" then require ("game/dialogs/is/l27is")
			elseif language=="it" then require ("game/dialogs/it/l27it")
			elseif language=="jp" then require ("game/dialogs/jp/l27jp")
			elseif language=="no" then require ("game/dialogs/no/l27no")
			elseif language=="pt" then require ("game/dialogs/pt/l27pt")
			elseif language=="th" then require ("game/dialogs/th/l27th")
			elseif language2=="cs" then require ("game/dialogs/cs/l27cs")
			elseif language=="de" then require ("game/dialogs/de/l27de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l27es")
			elseif language=="fr" then require ("game/dialogs/fr/l27fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l27nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l27pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l27ru")
			elseif language=="sv" then require ("game/dialogs/sv/l27sv")
			 elseif language=="thai" then require ("game/dialogs/th/l27th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev27()
							end

		end
		
		elseif nLevel==28 then
		
		
				require("game/dialogs/talkies28")
			
			if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l28en")	
				--if language=="bg" then require ("game/dialogs/es/l28bg")
			--elseif language=="br" then require ("game/dialogs/es/l28br")	
			--elseif language=="ch" then require ("game/dialogs/es/l28ch")
			elseif language=="chi" then require ("game/dialogs/chi/l28chi")
			elseif language=="da" then require ("game/dialogs/da/l28da")
			elseif language=="fi" then require ("game/dialogs/fi/l28fi")
			elseif language=="gr" then require ("game/dialogs/gr/l28gr")
			elseif language=="is" then require ("game/dialogs/is/l28is")
			elseif language=="it" then require ("game/dialogs/it/l28it")
			elseif language=="jp" then require ("game/dialogs/jp/l28jp")
			elseif language=="no" then require ("game/dialogs/no/l28no")
			elseif language=="pt" then require ("game/dialogs/pt/l28pt")
			elseif language=="th" then require ("game/dialogs/th/l28th")
			elseif language2=="cs" then require ("game/dialogs/cs/l28cs")	
			elseif language=="de" then require ("game/dialogs/de/l28de")
			--elseif language=="eo" then require ("game/dialogs/es/l28eo")
			elseif language=="es" then require ("game/dialogs/es/l28es")
			elseif language=="fr" then require ("game/dialogs/fr/l28fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l28it")
			elseif language2=="nl" then require ("game/dialogs/nl/l28nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l28pl")
			--elseif language=="pr" then require ("game/dialogs/es/l28pr")
			elseif language=="ru" then require ("game/dialogs/ru/l28ru")
			elseif language=="sv" then require ("game/dialogs/sv/l28sv")
			 elseif language=="thai" then require ("game/dialogs/th/l28th")
			--elseif language=="sk" then require ("game/dialogs/es/l28sk")
			end
		
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev28()
							end
					
		end
		elseif nLevel==29 then
		
		require("game/dialogs/talkies29")
			
			if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l29en")	
				--if language=="bg" then require ("game/dialogs/es/l16bg")
			--elseif language=="br" then require ("game/dialogs/es/l16br")	
			--elseif language=="ch" then require ("game/dialogs/es/l16ch")
			elseif language=="chi" then require ("game/dialogs/chi/l29chi")
			elseif language=="da" then require ("game/dialogs/da/l29da")
			elseif language=="fi" then require ("game/dialogs/fi/l29fi")
			elseif language=="gr" then require ("game/dialogs/gr/l29gr")
			elseif language=="is" then require ("game/dialogs/is/l29is")
			elseif language=="it" then require ("game/dialogs/it/l29it")
			elseif language=="jp" then require ("game/dialogs/jp/l29jp")
			elseif language=="no" then require ("game/dialogs/no/l29no")
			elseif language=="pt" then require ("game/dialogs/pt/l29pt")
			elseif language=="th" then require ("game/dialogs/th/l29th")
			elseif language2=="cs" then require ("game/dialogs/cs/l29cs")	
			elseif language=="de" then require ("game/dialogs/de/l29de")
			--elseif language=="eo" then require ("game/dialogs/es/l16eo")
			elseif language=="es" then require ("game/dialogs/es/l29es")
			elseif language=="fr" then require ("game/dialogs/fr/l29fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l16it")
			elseif language2=="nl" then require ("game/dialogs/nl/l29nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l29pl")
			--elseif language=="pr" then require ("game/dialogs/es/l16pr")
			elseif language=="ru" then require ("game/dialogs/ru/l29ru")
			elseif language=="sv" then require ("game/dialogs/sv/l29sv")
			 elseif language=="thai" then require ("game/dialogs/th/l29th")
			--elseif language=="sk" then require ("game/dialogs/es/l16sk")
			end
			
			
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev29()
							end
		
		end
		
		elseif nLevel==30 then
		
			
			require("game/dialogs/talkies30")
		--talkies=true
		if talkies==true then
		Talkies.clearMessages() 
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")   then require ("game/dialogs/en/l30en")	
				--if language=="bg" then require ("game/dialogs/bg/l1bg")
			--elseif language=="br" then require ("game/dialogs/es/l1br")	
			--elseif language=="ch" then require ("game/dialogs/es/l30ch")
			elseif language=="chi" then require ("game/dialogs/chi/l30chi")
			elseif language=="da" then require ("game/dialogs/da/l30da")
			elseif language=="fi" then require ("game/dialogs/fi/l30fi")
			elseif language=="gr" then require ("game/dialogs/gr/l30gr")
			elseif language=="is" then require ("game/dialogs/is/l30is")
			elseif language=="it" then require ("game/dialogs/it/l30it")
			elseif language=="jp" then require ("game/dialogs/jp/l30jp")
			elseif language=="no" then require ("game/dialogs/no/l30no")
			elseif language=="pt" then require ("game/dialogs/pt/l30pt")
			elseif language=="th" then require ("game/dialogs/th/l30th")
			elseif language=="cs" then require ("game/dialogs/es/l30cs")	
			elseif language=="de" then require ("game/dialogs/de/l30de")
			--elseif language=="eo" then require ("game/dialogs/es/l30eo")
			elseif language=="es" then require ("game/dialogs/es/l30es")
			elseif language=="fr" then require ("game/dialogs/fr/l30fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l30it")
			elseif language2=="nl" then require ("game/dialogs/nl/l30nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l30pl")
			--elseif language=="pr" then require ("game/dialogs/es/l30pr")
			elseif language=="ru" then require ("game/dialogs/ru/l30ru")
			elseif language=="sv" then require ("game/dialogs/sv/l30sv")
			 elseif language=="thai" then require ("game/dialogs/th/l30th")
			--elseif language=="sk" then require ("game/dialogs/es/l30sk")
			end
			
							music:setVolume(0.6)
				
		Obey.lev30() end
		elseif nLevel==31 then
		
			
			
				require("game/dialogs/talkies31")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l31chi")
			elseif language=="da" then require ("game/dialogs/da/l31da")
			elseif language=="fi" then require ("game/dialogs/fi/l31fi")
			elseif language=="gr" then require ("game/dialogs/gr/l31gr")
			elseif language=="is" then require ("game/dialogs/is/l31is")
			elseif language=="it" then require ("game/dialogs/it/l31it")
			elseif language=="jp" then require ("game/dialogs/jp/l31jp")
			elseif language=="no" then require ("game/dialogs/no/l31no")
			elseif language=="pt" then require ("game/dialogs/pt/l31pt")
			elseif language=="th" then require ("game/dialogs/th/l31th")
			elseif language=="de" then require ("game/dialogs/de/l31de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l31es")
			elseif language=="fr" then require ("game/dialogs/fr/l31fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l31en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l31ru")
			elseif language=="sv" then require ("game/dialogs/sv/l31sv") 
			elseif language=="thai" then require ("game/dialogs/th/31lth")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev31() end
		
		elseif nLevel==32 then
		
		
			require("game/dialogs/talkies32")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l32bg")
			--elseif language=="br" then require ("game/dialogs/es/l32br")	
			--elseif language=="ch" then require ("game/dialogs/es/l32ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l32cs")
				if language=="chi" then require ("game/dialogs/chi/l32chi")
			elseif language=="da" then require ("game/dialogs/da/l32da")
			elseif language=="fi" then require ("game/dialogs/fi/l32fi")
			elseif language=="gr" then require ("game/dialogs/gr/l32gr")
			elseif language=="is" then require ("game/dialogs/is/l32is")
			elseif language=="it" then require ("game/dialogs/it/l32it")
			elseif language=="jp" then require ("game/dialogs/jp/l32jp")
			elseif language=="no" then require ("game/dialogs/no/l32no")
			elseif language=="pt" then require ("game/dialogs/pt/l32pt")
			elseif language=="th" then require ("game/dialogs/th/l32th")	
			elseif language=="de" then require ("game/dialogs/de/l32de")
			--elseif language=="eo" then require ("game/dialogs/es/l32eo")
			elseif language=="es" then require ("game/dialogs/es/l32es")
			elseif language=="fr" then require ("game/dialogs/fr/l32fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l32en")	
			--elseif language=="it" then require ("game/dialogs/es/l32it")
			elseif language2=="nl" then require ("game/dialogs/nl/l32nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l32pl")
			--elseif language=="pr" then require ("game/dialogs/es/l32pr")
			elseif language=="ru" then require ("game/dialogs/ru/l32ru")
			elseif language=="sv" then require ("game/dialogs/sv/l32sv")
			 elseif language=="thai" then require ("game/dialogs/th/l32th")
			--elseif language=="sk" then require ("game/dialogs/es/l32sk")
			end
		Obey.lev32() end
		
		elseif nLevel==33 then
		
		
			require("game/dialogs/talkies33")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")
				if language=="chi" then require ("game/dialogs/chi/l33chi")
			elseif language=="da" then require ("game/dialogs/da/l33da")
			elseif language=="fi" then require ("game/dialogs/fi/l33fi")
			elseif language=="gr" then require ("game/dialogs/gr/l33gr")
			elseif language=="is" then require ("game/dialogs/is/l33is")
			elseif language=="it" then require ("game/dialogs/it/l33it")
			elseif language=="jp" then require ("game/dialogs/jp/l33jp")
			elseif language=="no" then require ("game/dialogs/no/l33no")
			elseif language=="pt" then require ("game/dialogs/pt/l33pt")
			elseif language=="th" then require ("game/dialogs/th/l33th")
			elseif language=="de" then require ("game/dialogs/de/l33de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l33es")
			elseif language=="fr" then require ("game/dialogs/fr/l33fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l33en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l33ru")
			elseif language=="sv" then require ("game/dialogs/sv/l33sv") 
			elseif language=="thai" then require ("game/dialogs/th/l33th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev33() end
		
		elseif nLevel==34 then
		
		
			require("game/dialogs/talkies34")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")
				if language=="chi" then require ("game/dialogs/chi/l34chi")
			elseif language=="da" then require ("game/dialogs/da/l34da")
			elseif language=="fi" then require ("game/dialogs/fi/l34fi")
			elseif language=="gr" then require ("game/dialogs/gr/l34gr")
			elseif language=="is" then require ("game/dialogs/is/l34is")
			elseif language=="it" then require ("game/dialogs/it/l34it")
			elseif language=="jp" then require ("game/dialogs/jp/l34jp")
			elseif language=="no" then require ("game/dialogs/no/l34no")
			elseif language=="pt" then require ("game/dialogs/pt/l34pt")
			elseif language=="th" then require ("game/dialogs/th/l34th")
			elseif language=="de" then require ("game/dialogs/de/l34de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l34es")
			elseif language=="fr" then require ("game/dialogs/fr/l34fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l34en")
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			--elseif language2=="nl" then require ("game/dialogs/nl/l34nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l34pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l34ru")
			elseif language=="sv" then require ("game/dialogs/sv/l34sv") 
			elseif language=="thai" then require ("game/dialogs/th/l34th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev34() end
		
		elseif nLevel==35 then
		
			require("game/dialogs/talkies35")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l35chi")
			elseif language=="da" then require ("game/dialogs/da/l35da")
			elseif language=="fi" then require ("game/dialogs/fi/l35fi")
			elseif language=="gr" then require ("game/dialogs/gr/l35gr")
			elseif language=="is" then require ("game/dialogs/is/l35is")
			elseif language=="it" then require ("game/dialogs/it/l35it")
			elseif language=="jp" then require ("game/dialogs/jp/l35jp")
			elseif language=="no" then require ("game/dialogs/no/l35no")
			elseif language=="pt" then require ("game/dialogs/pt/l35pt")
			elseif language=="th" then require ("game/dialogs/th/l35th")
			elseif language=="de" then require ("game/dialogs/de/l35de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l35es")
			elseif language=="fr" then require ("game/dialogs/fr/l35fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l35en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l35ru")
			elseif language=="sv" then require ("game/dialogs/sv/l35sv") 
			elseif language=="thai" then require ("game/dialogs/th/l35th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev35() end
		
		
		elseif nLevel==36 then
		
		
		shader2=true
			
			require("game/dialogs/talkies36")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l36chi")
			elseif language=="da" then require ("game/dialogs/da/l36da")
			elseif language=="fi" then require ("game/dialogs/fi/l36fi")
			elseif language=="gr" then require ("game/dialogs/gr/l36gr")
			elseif language=="is" then require ("game/dialogs/is/l36is")
			elseif language=="it" then require ("game/dialogs/it/l36it")
			elseif language=="jp" then require ("game/dialogs/jp/l36jp")
			elseif language=="no" then require ("game/dialogs/no/l36no")
			elseif language=="pt" then require ("game/dialogs/pt/l36pt")
			elseif language=="th" then require ("game/dialogs/th/l36th")
			elseif language=="de" then require ("game/dialogs/de/l36de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l36es")
			elseif language=="fr" then require ("game/dialogs/fr/l36fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l36en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l36ru")
			elseif language=="sv" then require ("game/dialogs/sv/l36sv") 
			elseif language=="thai" then require ("game/dialogs/th/l36th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev36() end
		
		elseif nLevel==37 then
		
		
			require("game/dialogs/talkies37")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l37chi")
			elseif language=="da" then require ("game/dialogs/da/l37da")
			elseif language=="fi" then require ("game/dialogs/fi/l37fi")
			elseif language=="gr" then require ("game/dialogs/gr/l37gr")
			elseif language=="is" then require ("game/dialogs/is/l37is")
			elseif language=="it" then require ("game/dialogs/it/l37it")
			elseif language=="jp" then require ("game/dialogs/jp/l37jp")
			elseif language=="no" then require ("game/dialogs/no/l37no")
			elseif language=="pt" then require ("game/dialogs/pt/l37pt")
			elseif language=="th" then require ("game/dialogs/th/l37th")
			elseif language=="de" then require ("game/dialogs/de/l37de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l37es")
			elseif language=="fr" then require ("game/dialogs/fr/l37fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l37en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l37ru")
			elseif language=="sv" then require ("game/dialogs/sv/l37sv") 
			elseif language=="thai" then require ("game/dialogs/th/l37th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev37() end
		
		elseif nLevel==38 then
		
		
			require("game/dialogs/talkies38")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l38chi")
			elseif language=="da" then require ("game/dialogs/da/l38da")
			elseif language=="fi" then require ("game/dialogs/fi/l38fi")
			elseif language=="gr" then require ("game/dialogs/gr/l38gr")
			elseif language=="is" then require ("game/dialogs/is/l38is")
			elseif language=="it" then require ("game/dialogs/it/l38it")
			elseif language=="jp" then require ("game/dialogs/jp/l38jp")
			elseif language=="no" then require ("game/dialogs/no/l38no")
			elseif language=="pt" then require ("game/dialogs/pt/l38pt")
			elseif language=="th" then require ("game/dialogs/th/l38th")
			elseif language=="de" then require ("game/dialogs/de/l38de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l38es")
			elseif language=="fr" then require ("game/dialogs/fr/l38fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l38en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l38ru")
			elseif language=="sv" then require ("game/dialogs/sv/l38sv") 
			elseif language=="thai" then require ("game/dialogs/th/l38th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev38() end
		
		elseif nLevel==39 then
		
		
			require("game/dialogs/talkies39")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l39chi")
			elseif language=="da" then require ("game/dialogs/da/l39da")
			elseif language=="fi" then require ("game/dialogs/fi/l39fi")
			elseif language=="gr" then require ("game/dialogs/gr/l39gr")
			elseif language=="is" then require ("game/dialogs/is/l39is")
			elseif language=="it" then require ("game/dialogs/it/l39it")
			elseif language=="jp" then require ("game/dialogs/jp/l39jp")
			elseif language=="no" then require ("game/dialogs/no/l39no")
			elseif language=="pt" then require ("game/dialogs/pt/l39pt")
			elseif language=="th" then require ("game/dialogs/th/l39th")
			elseif language=="de" then require ("game/dialogs/de/l39de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l39es")
			elseif language=="fr" then require ("game/dialogs/fr/l39fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l39en")	
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l31pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l39ru")
			elseif language=="sv" then require ("game/dialogs/sv/l39sv") 
			elseif language=="thai" then require ("game/dialogs/th/l39th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev39() end
		
		elseif nLevel==40 then
		
		
			
				require("game/dialogs/talkies40")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")
				if language=="chi" then require ("game/dialogs/chi/l40chi")
			elseif language=="da" then require ("game/dialogs/da/l40da")
			elseif language=="fi" then require ("game/dialogs/fi/l40fi")
			elseif language=="gr" then require ("game/dialogs/gr/l40gr")
			elseif language=="is" then require ("game/dialogs/is/l40is")
			elseif language=="it" then require ("game/dialogs/it/l40it")
			elseif language=="jp" then require ("game/dialogs/jp/l40jp")
			elseif language=="no" then require ("game/dialogs/no/l40no")
			elseif language=="pt" then require ("game/dialogs/pt/l40pt")
			elseif language=="th" then require ("game/dialogs/th/l40th")
			elseif language=="de" then require ("game/dialogs/de/l40de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l40es")
			elseif language=="fr" then require ("game/dialogs/fr/l40fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l40en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l40ru")
			elseif language=="sv" then require ("game/dialogs/sv/l40sv") 
			elseif language=="thai" then require ("game/dialogs/th/l40th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev40() end
		
		elseif nLevel==43 then
		
			require("game/dialogs/talkies43")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")
				if language=="chi" then require ("game/dialogs/chi/l43chi")
			elseif language=="da" then require ("game/dialogs/da/l43da")
			elseif language=="fi" then require ("game/dialogs/fi/l43fi")
			elseif language=="gr" then require ("game/dialogs/gr/l43gr")
			elseif language=="is" then require ("game/dialogs/is/l43is")
			elseif language=="it" then require ("game/dialogs/it/l1it")
			elseif language=="jp" then require ("game/dialogs/jp/l43jp")
			elseif language=="no" then require ("game/dialogs/no/l43no")
			elseif language=="pt" then require ("game/dialogs/pt/l43pt")
			elseif language=="th" then require ("game/dialogs/th/l43th")
			elseif language=="de" then require ("game/dialogs/de/l43de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l43es")
			elseif language=="fr" then require ("game/dialogs/fr/l43fr")
			elseif language=="en" and not (language2=="pl") and not (language2=="nl") then require ("game/dialogs/en/l43en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l43ru")
			elseif language=="sv" then require ("game/dialogs/sv/l43sv")
			 elseif language=="thai" then require ("game/dialogs/th/l43th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev43()
							end
				end
			
		elseif nLevel==41 then
		
		
			require("game/dialogs/talkies41")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")
				if language=="chi" then require ("game/dialogs/chi/l41chi")
			elseif language=="da" then require ("game/dialogs/da/l41da")
			elseif language=="fi" then require ("game/dialogs/fi/l41fi")
			elseif language=="gr" then require ("game/dialogs/gr/l41gr")
			elseif language=="is" then require ("game/dialogs/is/l41is")
			elseif language=="it" then require ("game/dialogs/it/l41it")
			elseif language=="jp" then require ("game/dialogs/jp/l41jp")
			elseif language=="no" then require ("game/dialogs/no/l41no")
			elseif language=="pt" then require ("game/dialogs/pt/l41pt")
			elseif language=="th" then require ("game/dialogs/th/l41th")
			elseif language=="de" then require ("game/dialogs/de/l41de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l41es")
			elseif language=="fr" then require ("game/dialogs/fr/l41fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l41en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l41ru")
			elseif language=="sv" then require ("game/dialogs/sv/l41sv") 
			elseif language=="thai" then require ("game/dialogs/th/l41th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev41() end
		
		elseif nLevel==42 then
		
		
			require("game/dialogs/talkies42")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")	
				if language=="chi" then require ("game/dialogs/chi/l42chi")
			elseif language=="da" then require ("game/dialogs/da/l42da")
			elseif language=="fi" then require ("game/dialogs/fi/l42fi")
			elseif language=="gr" then require ("game/dialogs/gr/l42gr")
			elseif language=="is" then require ("game/dialogs/is/l42is")
			elseif language=="it" then require ("game/dialogs/it/l42it")
			elseif language=="jp" then require ("game/dialogs/jp/l42jp")
			elseif language=="no" then require ("game/dialogs/no/l42no")
			elseif language=="pt" then require ("game/dialogs/pt/l42pt")
			elseif language=="th" then require ("game/dialogs/th/l42th")
			elseif language=="de" then require ("game/dialogs/de/l42de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l42es")
			elseif language=="fr" then require ("game/dialogs/fr/l42fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l42en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l42ru")
			elseif language=="sv" then require ("game/dialogs/sv/l42sv")
			 elseif language=="thai" then require ("game/dialogs/th/l42th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev42() end
		
		elseif nLevel==44 then
		
		
			require("game/dialogs/talkies44")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l33bg")
			--elseif language=="br" then require ("game/dialogs/es/l33br")	
			--elseif language=="ch" then require ("game/dialogs/es/l33ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l33cs")	
				if language=="chi" then require ("game/dialogs/chi/l44chi")
			elseif language=="da" then require ("game/dialogs/da/l44da")
			elseif language=="fi" then require ("game/dialogs/fi/l44fi")
			elseif language=="gr" then require ("game/dialogs/gr/l44gr")
			elseif language=="is" then require ("game/dialogs/is/l44is")
			elseif language=="it" then require ("game/dialogs/it/l44it")
			elseif language=="jp" then require ("game/dialogs/jp/l44jp")
			elseif language=="no" then require ("game/dialogs/no/l44no")
			elseif language=="pt" then require ("game/dialogs/pt/l44pt")
			elseif language=="th" then require ("game/dialogs/th/l44th")
			elseif language=="de" then require ("game/dialogs/de/l44de")
			--elseif language=="eo" then require ("game/dialogs/es/l33eo")
			elseif language=="es" then require ("game/dialogs/es/l44es")
			elseif language=="fr" then require ("game/dialogs/fr/l44fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l44en")	
			--elseif language=="it" then require ("game/dialogs/es/l33it")
			elseif language2=="nl" then require ("game/dialogs/nl/l33nl")
			--elseif language2=="pl" then require ("game/dialogs/pl/l33pl")
			--elseif language=="pr" then require ("game/dialogs/es/l33pr")
			elseif language=="ru" then require ("game/dialogs/ru/l44ru")
			elseif language=="sv" then require ("game/dialogs/sv/l44sv")
			 elseif language=="thai" then require ("game/dialogs/th/l44th")
			--elseif language=="sk" then require ("game/dialogs/es/l33sk")
			end
		Obey.lev44() end
		
		elseif nLevel==45 then
		
			require("game/dialogs/talkies45")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l45en")	
				--if language=="bg" then require ("game/dialogs/es/l45bg")
			--elseif language=="br" then require ("game/dialogs/es/l45br")	
			--elseif language=="ch" then require ("game/dialogs/es/l45ch")	
			elseif language=="chi" then require ("game/dialogs/chi/l45chi")
			elseif language=="da" then require ("game/dialogs/da/l45da")
			elseif language=="fi" then require ("game/dialogs/fi/l45fi")
			elseif language=="gr" then require ("game/dialogs/gr/l45gr")
			elseif language=="is" then require ("game/dialogs/is/l45is")
			elseif language=="it" then require ("game/dialogs/it/l45it")
			elseif language=="jp" then require ("game/dialogs/jp/l45jp")
			elseif language=="no" then require ("game/dialogs/no/l45no")
			elseif language=="pt" then require ("game/dialogs/pt/l45pt")
			elseif language=="th" then require ("game/dialogs/th/l45th")
			elseif language2=="cs" then require ("game/dialogs/cs/l45cs")	
			elseif language=="de" then require ("game/dialogs/de/l45de")
			--elseif language=="eo" then require ("game/dialogs/es/l45eo")
			elseif language=="es" then require ("game/dialogs/es/l45es")
			elseif language=="fr" then require ("game/dialogs/fr/l45fr")
			--elseif language=="it" then require ("game/dialogs/es/l45it")
			elseif language2=="nl" then require ("game/dialogs/nl/l45nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l45pl")
			--elseif language=="pr" then require ("game/dialogs/es/l45pr")
			elseif language=="ru" then require ("game/dialogs/ru/l45ru")
			elseif language=="sv" then require ("game/dialogs/sv/l45sv") 
			elseif language=="thai" then require ("game/dialogs/th/l45th")
			--elseif language=="sk" then require ("game/dialogs/es/l45sk")
			end
			Obey.lev45() end
		
		elseif nLevel==46 then
		
		
			require("game/dialogs/talkies46")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")    then require ("game/dialogs/en/l46en")	
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l46chi")
			elseif language=="da" then require ("game/dialogs/da/l46da")
			elseif language=="fi" then require ("game/dialogs/fi/l46fi")
			elseif language=="gr" then require ("game/dialogs/gr/l46gr")
			elseif language=="is" then require ("game/dialogs/is/l46is")
			elseif language=="it" then require ("game/dialogs/it/l46it")
			elseif language=="jp" then require ("game/dialogs/jp/l46jp")
			elseif language=="no" then require ("game/dialogs/no/l46no")
			elseif language=="pt" then require ("game/dialogs/pt/l46pt")
			elseif language=="th" then require ("game/dialogs/th/l46th")	
			elseif language2=="cs" then require ("game/dialogs/cs/l46cs")	
			elseif language=="de" then require ("game/dialogs/de/l46de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l46es")
			elseif language=="fr" then require ("game/dialogs/fr/l46fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l46nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l46pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l46ru")
			elseif language=="sv" then require ("game/dialogs/sv/l46sv")
			 elseif language=="thai" then require ("game/dialogs/th/l46th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
			
				
			
		Obey.lev46() end
			
		
		elseif nLevel==47 then
		
			require("game/dialogs/talkies47")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")    then require ("game/dialogs/en/l47en")	
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l47chi")
			elseif language=="da" then require ("game/dialogs/da/l47da")
			elseif language=="fi" then require ("game/dialogs/fi/l47fi")
			elseif language=="gr" then require ("game/dialogs/gr/l47gr")
			elseif language=="is" then require ("game/dialogs/is/l47is")
			elseif language=="it" then require ("game/dialogs/it/l47it")
			elseif language=="jp" then require ("game/dialogs/jp/l47jp")
			elseif language=="no" then require ("game/dialogs/no/l47no")
			elseif language=="pt" then require ("game/dialogs/pt/l47pt")
			elseif language=="th" then require ("game/dialogs/th/l47th")
			elseif language2=="cs" then require ("game/dialogs/cs/l47cs")	
			elseif language=="de" then require ("game/dialogs/de/l47de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l47es")
			elseif language=="fr" then require ("game/dialogs/fr/l47fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l47pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l47ru")
			elseif language=="sv" then require ("game/dialogs/sv/l47sv")
			 elseif language=="thai" then require ("game/dialogs/th/l47th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end

			
		Obey.lev47() end
		
			
		elseif nLevel==48 then
		
			
			
					require("game/dialogs/talkies48")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")    then require ("game/dialogs/en/l48en")	
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l48chi")
			elseif language=="da" then require ("game/dialogs/da/l48da")
			elseif language=="fi" then require ("game/dialogs/fi/l48fi")
			elseif language=="gr" then require ("game/dialogs/gr/l48gr")
			elseif language=="is" then require ("game/dialogs/is/l48is")
			elseif language=="it" then require ("game/dialogs/it/l48it")
			elseif language=="jp" then require ("game/dialogs/jp/l48jp")
			elseif language=="no" then require ("game/dialogs/no/l48no")
			elseif language=="pt" then require ("game/dialogs/pt/l48pt")
			elseif language=="th" then require ("game/dialogs/th/l48th")	
			elseif language2=="cs" then require ("game/dialogs/cs/l48cs")	
			elseif language=="de" then require ("game/dialogs/de/l48de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l48es")
			elseif language=="fr" then require ("game/dialogs/fr/l48fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l48pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l48ru")
			elseif language=="sv" then require ("game/dialogs/sv/l48sv")
			 elseif language=="thai" then require ("game/dialogs/th/l48th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
				 
			
		Obey.lev48() end
		
		elseif nLevel==49 then
		
							require("game/dialogs/talkies49")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")    then require ("game/dialogs/en/l49en")	
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l49chi")
			elseif language=="da" then require ("game/dialogs/da/l49da")
			elseif language=="fi" then require ("game/dialogs/fi/l49fi")
			elseif language=="gr" then require ("game/dialogs/gr/l49gr")
			elseif language=="is" then require ("game/dialogs/is/l49is")
			elseif language=="it" then require ("game/dialogs/it/l49it")
			elseif language=="jp" then require ("game/dialogs/jp/l49jp")
			elseif language=="no" then require ("game/dialogs/no/l49no")
			elseif language=="pt" then require ("game/dialogs/pt/l49pt")
			elseif language=="th" then require ("game/dialogs/th/l49th")
			elseif language2=="cs" then require ("game/dialogs/cs/l49cs")	
			elseif language=="de" then require ("game/dialogs/de/l49de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l49es")
			elseif language=="fr" then require ("game/dialogs/fr/l49fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l49nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l49pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l49ru")
			elseif language=="sv" then require ("game/dialogs/sv/l49sv")
			 elseif language=="thai" then require ("game/dialogs/th/l49th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev49()
							end
		
		
				
		
		end
			
			elseif nLevel==50 then
		
				 
		 require("game/dialogs/talkies50")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")    then require ("game/dialogs/en/l50en")	
				--if language=="bg" then require ("game/dialogs/es/l50bg")
			--elseif language=="br" then require ("game/dialogs/es/l50br")	
			--elseif language=="ch" then require ("game/dialogs/es/l50ch")
			elseif language=="chi" then require ("game/dialogs/chi/l50chi")
			elseif language=="da" then require ("game/dialogs/da/l50da")
			elseif language=="fi" then require ("game/dialogs/fi/l50fi")
			elseif language=="gr" then require ("game/dialogs/gr/l50gr")
			elseif language=="is" then require ("game/dialogs/is/l50is")
			elseif language=="it" then require ("game/dialogs/it/l50it")
			elseif language=="jp" then require ("game/dialogs/jp/l50jp")
			elseif language=="no" then require ("game/dialogs/no/l50no")
			elseif language=="pt" then require ("game/dialogs/pt/l50pt")
			elseif language=="th" then require ("game/dialogs/th/l50th")
			elseif language2=="cs" then require ("game/dialogs/cs/l50cs")	
			elseif language=="de" then require ("game/dialogs/de/l50de")
			--elseif language=="eo" then require ("game/dialogs/es/l50eo")
			elseif language=="es" then require ("game/dialogs/es/l50es")
			elseif language=="fr" then require ("game/dialogs/fr/l50fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l50it")
			elseif language2=="nl" then require ("game/dialogs/nl/l50nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l50pl")
			--elseif language=="pr" then require ("game/dialogs/es/l50pr")
			elseif language=="ru" then require ("game/dialogs/ru/l50ru")
			elseif language=="sv" then require ("game/dialogs/sv/l50sv") 
			elseif language=="thai" then require ("game/dialogs/th/l50h")
			--elseif language=="sk" then require ("game/dialogs/es/l50sk")
			end
			
			--thalassophobia_loop :setVolume(0.6)
			--love.audio.setEffect('myEffect', {type = 'reverb'})
			
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev50()
							end
		
		end
		
				elseif nLevel==51 then
		
				 
		 require("game/dialogs/talkies51")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en" and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l51en")	
				--if language=="bg" then require ("game/dialogs/es/l51bg")
			--elseif language=="br" then require ("game/dialogs/es/l51br")	
			--elseif language=="ch" then require ("game/dialogs/es/l51ch")
			elseif language=="chi" then require ("game/dialogs/chi/l51chi")
			elseif language=="da" then require ("game/dialogs/da/l51da")
			elseif language=="fi" then require ("game/dialogs/fi/l51fi")
			elseif language=="gr" then require ("game/dialogs/gr/l51gr")
			elseif language=="is" then require ("game/dialogs/is/l51is")
			elseif language=="it" then require ("game/dialogs/it/l51it")
			elseif language=="jp" then require ("game/dialogs/jp/l51jp")
			elseif language=="no" then require ("game/dialogs/no/l51no")
			elseif language=="pt" then require ("game/dialogs/pt/l51pt")
			elseif language=="th" then require ("game/dialogs/th/l51th")
			elseif language2=="cs" then require ("game/dialogs/cs/l51cs")	
			elseif language=="de" then require ("game/dialogs/de/l51de")
			--elseif language=="eo" then require ("game/dialogs/es/l51eo")
			elseif language=="es" then require ("game/dialogs/es/l51es")
			elseif language=="fr" then require ("game/dialogs/fr/l51fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l51it")
			elseif language2=="nl" then require ("game/dialogs/nl/l51nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l51pl")
			--elseif language=="pr" then require ("game/dialogs/es/l51pr")
			elseif language=="ru" then require ("game/dialogs/ru/l51ru")
			elseif language=="sv" then require ("game/dialogs/sv/l51sv")
			 elseif language=="thai" then require ("game/dialogs/th/l51th")
			--elseif language=="sk" then require ("game/dialogs/es/l51sk")
			end
			
			--thalassophobia_loop :setVolume(0.6)
			--love.audio.setEffect('myEffect', {type = 'reverb'})
			
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
									Obey.lev51()
							end
		
		end
		
		elseif nLevel==52 then
		
		
				require("game/dialogs/talkies52")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"   and not (language2=="pl") and not (language2=="cs") and not (language2=="nl")  then require ("game/dialogs/en/l52en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l52chi")
			elseif language=="da" then require ("game/dialogs/da/l52da")
			elseif language=="fi" then require ("game/dialogs/fi/l52fi")
			elseif language=="gr" then require ("game/dialogs/gr/l52gr")
			elseif language=="is" then require ("game/dialogs/is/l52is")
			elseif language=="it" then require ("game/dialogs/it/l52it")
			elseif language=="jp" then require ("game/dialogs/jp/l52jp")
			elseif language=="no" then require ("game/dialogs/no/l52no")
			elseif language=="pt" then require ("game/dialogs/pt/l52pt")
			elseif language=="th" then require ("game/dialogs/th/l52th")
			elseif language2=="cs" then require ("game/dialogs/cs/l52cs")	
			elseif language=="de" then require ("game/dialogs/de/l52de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l52es")
			elseif language=="fr" then require ("game/dialogs/fr/l52fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l52nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l52pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l52ru")
			elseif language=="sv" then require ("game/dialogs/sv/l52sv")
			 elseif language=="thai" then require ("game/dialogs/th/l52th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
			-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev52()
							end
		
		
		end
		
		elseif nLevel==53 then
		
		
					require("game/dialogs/talkies53")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l53en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l53chi")
			elseif language=="da" then require ("game/dialogs/da/l53da")
			elseif language=="fi" then require ("game/dialogs/fi/l53fi")
			elseif language=="gr" then require ("game/dialogs/gr/l53gr")
			elseif language=="is" then require ("game/dialogs/is/l53is")
			elseif language=="it" then require ("game/dialogs/it/l53it")
			elseif language=="jp" then require ("game/dialogs/jp/l53jp")
			elseif language=="no" then require ("game/dialogs/no/l53no")
			elseif language=="pt" then require ("game/dialogs/pt/l53pt")
			elseif language=="th" then require ("game/dialogs/th/l53th")
			elseif language2=="cs" then require ("game/dialogs/cs/l53cs")
			elseif language=="de" then require ("game/dialogs/de/l53de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l53es")
			elseif language=="fr" then require ("game/dialogs/fr/l53fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l53nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l53pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l53ru")
			elseif language=="sv" then require ("game/dialogs/sv/l53sv")
			 elseif language=="thai" then require ("game/dialogs/th/l53th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev53()
							end
	
		 end
		
		elseif nLevel==54 then
		
		
					require("game/dialogs/talkies54")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l54en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l54chi")
			elseif language=="da" then require ("game/dialogs/da/l54da")
			elseif language=="fi" then require ("game/dialogs/fi/l54fi")
			elseif language=="gr" then require ("game/dialogs/gr/l54gr")
			elseif language=="is" then require ("game/dialogs/is/l54is")
			elseif language=="it" then require ("game/dialogs/it/l54it")
			elseif language=="jp" then require ("game/dialogs/jp/l54jp")
			elseif language=="no" then require ("game/dialogs/no/l54no")
			elseif language=="pt" then require ("game/dialogs/pt/l54pt")
			elseif language=="th" then require ("game/dialogs/th/l54th")
			elseif language2=="cs" then require ("game/dialogs/cs/l54cs")
			elseif language=="de" then require ("game/dialogs/de/l54de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l54es")
			elseif language=="fr" then require ("game/dialogs/fr/l54fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l54nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l54pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l54ru")
			elseif language=="sv" then require ("game/dialogs/sv/l54sv")
			elseif language=="thai" then require ("game/dialogs/th/l54th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
						sub54done=false			-- this variable avoids the script to be repeated when engine is on
				-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev54()
							end
							
		end
		
		elseif nLevel==55 then

					require("game/dialogs/talkies55")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l55en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l55chi")
			elseif language=="da" then require ("game/dialogs/da/l55da")
			elseif language=="fi" then require ("game/dialogs/fi/l55fi")
			elseif language=="gr" then require ("game/dialogs/gr/l55gr")
			elseif language=="is" then require ("game/dialogs/is/l55is")
			elseif language=="it" then require ("game/dialogs/it/l55it")
			elseif language=="jp" then require ("game/dialogs/jp/l55jp")
			elseif language=="no" then require ("game/dialogs/no/l55no")
			elseif language=="pt" then require ("game/dialogs/pt/l55pt")
			elseif language=="th" then require ("game/dialogs/th/l55th")
			elseif language2=="cs" then require ("game/dialogs/cs/l55cs")
			elseif language=="de" then require ("game/dialogs/de/l55de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l55es")
			elseif language=="fr" then require ("game/dialogs/fr/l55fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l55nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l55pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l55ru")
			elseif language=="sv" then require ("game/dialogs/sv/l55sv")
			 elseif language=="thai" then require ("game/dialogs/th/l55th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev55() end
		
			elseif nLevel==56 then
		
		
					require("game/dialogs/talkies56")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l56en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l56chi")
			elseif language=="da" then require ("game/dialogs/da/l56da")
			elseif language=="fi" then require ("game/dialogs/fi/l56fi")
			elseif language=="gr" then require ("game/dialogs/gr/l56gr")
			elseif language=="is" then require ("game/dialogs/is/l56is")
			elseif language=="it" then require ("game/dialogs/it/l56it")
			elseif language=="jp" then require ("game/dialogs/jp/l56jp")
			elseif language=="no" then require ("game/dialogs/no/l56no")
			elseif language=="pt" then require ("game/dialogs/pt/l56pt")
			elseif language=="th" then require ("game/dialogs/th/l56th")
			elseif language2=="cs" then require ("game/dialogs/cs/l56cs")	
			elseif language=="de" then require ("game/dialogs/de/l56de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l56es")
			elseif language=="fr" then require ("game/dialogs/fr/l56fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l56nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l56pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l56ru")
			elseif language=="sv" then require ("game/dialogs/sv/l56sv")
			 elseif language=="thai" then require ("game/dialogs/th/l56th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev56() end
		
		elseif nLevel==57 then
		
		
					require("game/dialogs/talkies57")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l57en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l57chi")
			elseif language=="da" then require ("game/dialogs/da/l57da")
			elseif language=="fi" then require ("game/dialogs/fi/l57fi")
			elseif language=="gr" then require ("game/dialogs/gr/l57gr")
			elseif language=="is" then require ("game/dialogs/is/l57is")
			elseif language=="it" then require ("game/dialogs/it/l57it")
			elseif language=="jp" then require ("game/dialogs/jp/l57jp")
			elseif language=="no" then require ("game/dialogs/no/l57no")
			elseif language=="pt" then require ("game/dialogs/pt/l57pt")
			elseif language=="th" then require ("game/dialogs/th/l57th")
			elseif language2=="cs" then require ("game/dialogs/cs/l57cs")	
			elseif language=="de" then require ("game/dialogs/de/l57de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l57es")
			elseif language=="fr" then require ("game/dialogs/fr/l57fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l57nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l57pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l57ru")
			elseif language=="sv" then require ("game/dialogs/sv/l57sv")
			 elseif language=="thai" then require ("game/dialogs/th/l57th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev57()
							end
		
		end
		
	
		
			elseif nLevel==58 then
			
			
				require("game/dialogs/talkies58")
		if talkies==true then
		Talkies.clearMessages()
				if language=="en"  and not (language2=="pl") and not (language2=="cs") and not (language2=="nl") then require ("game/dialogs/en/l58en")
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")
			elseif language=="chi" then require ("game/dialogs/chi/l58chi")
			elseif language=="da" then require ("game/dialogs/da/l58da")
			elseif language=="fi" then require ("game/dialogs/fi/l58fi")
			elseif language=="gr" then require ("game/dialogs/gr/l58gr")
			elseif language=="is" then require ("game/dialogs/is/l58is")
			elseif language=="it" then require ("game/dialogs/it/l58it")
			elseif language=="jp" then require ("game/dialogs/jp/l58jp")
			elseif language=="no" then require ("game/dialogs/no/l58no")
			elseif language=="pt" then require ("game/dialogs/pt/l58pt")
			elseif language=="th" then require ("game/dialogs/th/l58th")
			elseif language2=="cs" then require ("game/dialogs/cs/l58cs")
			elseif language=="de" then require ("game/dialogs/de/l58de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l58es")
			elseif language=="fr" then require ("game/dialogs/fr/l58fr")
			
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l58nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l58pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l58ru")
			elseif language=="sv" then require ("game/dialogs/sv/l58sv") 
			elseif language=="thai" then require ("game/dialogs/th/l58th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages()
								Obey.lev58()
							end
			
		end
		
		elseif nLevel==59 then
		
		
				require("game/dialogs/talkies59")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l59chi")
			elseif language=="da" then require ("game/dialogs/da/l59da")
			elseif language=="fi" then require ("game/dialogs/fi/l59fi")
			elseif language=="gr" then require ("game/dialogs/gr/l59gr")
			elseif language=="is" then require ("game/dialogs/is/l59is")
			elseif language=="it" then require ("game/dialogs/it/l59it")
			elseif language=="jp" then require ("game/dialogs/jp/l59jp")
			elseif language=="no" then require ("game/dialogs/no/l59no")
			elseif language=="pt" then require ("game/dialogs/pt/l59pt")
			elseif language=="th" then require ("game/dialogs/th/l59th")
			elseif language=="de" then require ("game/dialogs/de/l59de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l59es")
			elseif language=="fr" then require ("game/dialogs/fr/l59fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l59en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l59nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l59pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l59ru")
			elseif language=="sv" then require ("game/dialogs/sv/l59sv")
			 elseif language=="thai" then require ("game/dialogs/th/l59th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev59() end
		
		elseif nLevel==60 then
		
		
			require("game/dialogs/talkies60")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l60chi")
			elseif language=="da" then require ("game/dialogs/da/l60da")
			elseif language=="fi" then require ("game/dialogs/fi/l60fi")
			elseif language=="gr" then require ("game/dialogs/gr/l60gr")
			elseif language=="is" then require ("game/dialogs/is/l60is")
			elseif language=="it" then require ("game/dialogs/it/l60it")
			elseif language=="jp" then require ("game/dialogs/jp/l60jp")
			elseif language=="no" then require ("game/dialogs/no/l60no")
			elseif language=="pt" then require ("game/dialogs/pt/l60pt")
			elseif language=="th" then require ("game/dialogs/th/l60th")
			elseif language=="de" then require ("game/dialogs/de/l60de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l60es")
			elseif language=="fr" then require ("game/dialogs/fr/l60fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l60en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l60pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l60ru")
			elseif language=="sv" then require ("game/dialogs/sv/l60sv") 
			elseif language=="thai" then require ("game/dialogs/th/l60th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev60() end
		
		elseif nLevel==61 then
		
			
			require("game/dialogs/talkies61")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l61chi")
			elseif language=="da" then require ("game/dialogs/da/l61da")
			elseif language=="fi" then require ("game/dialogs/fi/l61fi")
			elseif language=="gr" then require ("game/dialogs/gr/l61gr")
			elseif language=="is" then require ("game/dialogs/is/l61is")
			elseif language=="it" then require ("game/dialogs/it/l61it")
			elseif language=="jp" then require ("game/dialogs/jp/l61jp")
			elseif language=="no" then require ("game/dialogs/no/l61no")
			elseif language=="pt" then require ("game/dialogs/pt/l61pt")
			elseif language=="th" then require ("game/dialogs/th/l61th")
			elseif language=="de" then require ("game/dialogs/de/l61de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l61es")
			elseif language=="fr" then require ("game/dialogs/fr/l61fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l61en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l61pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l61ru")
			elseif language=="sv" then require ("game/dialogs/sv/l61sv")
			 elseif language=="thai" then require ("game/dialogs/th/l61th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev61() end
		
		elseif nLevel==62 then
		
		
			
				require("game/dialogs/talkies62")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l62chi")
			elseif language=="da" then require ("game/dialogs/da/l62da")
			elseif language=="fi" then require ("game/dialogs/fi/l62fi")
			elseif language=="gr" then require ("game/dialogs/gr/l62gr")
			elseif language=="is" then require ("game/dialogs/is/l62is")
			elseif language=="it" then require ("game/dialogs/it/l62it")
			elseif language=="jp" then require ("game/dialogs/jp/l62jp")
			elseif language=="no" then require ("game/dialogs/no/l62no")
			elseif language=="pt" then require ("game/dialogs/pt/l62pt")
			elseif language=="th" then require ("game/dialogs/th/l62th")	
			elseif language=="de" then require ("game/dialogs/de/l62de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l62es")
			elseif language=="fr" then require ("game/dialogs/fr/l62fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l62en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l62pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l62ru")
			elseif language=="sv" then require ("game/dialogs/sv/l62sv")
			 elseif language=="thai" then require ("game/dialogs/th/l62th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev62() end
		
		elseif nLevel==63 then
		
			
				require("game/dialogs/talkies63")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l63chi")
			elseif language=="da" then require ("game/dialogs/da/l63da")
			elseif language=="fi" then require ("game/dialogs/fi/l63fi")
			elseif language=="gr" then require ("game/dialogs/gr/l63gr")
			elseif language=="is" then require ("game/dialogs/is/l63is")
			elseif language=="it" then require ("game/dialogs/it/l63it")
			elseif language=="jp" then require ("game/dialogs/jp/l63jp")
			elseif language=="no" then require ("game/dialogs/no/l63no")
			elseif language=="pt" then require ("game/dialogs/pt/l63pt")
			elseif language=="th" then require ("game/dialogs/th/l63th")
			elseif language=="de" then require ("game/dialogs/de/l63de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l63es")
			elseif language=="fr" then require ("game/dialogs/fr/l63fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l63en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l63pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l63ru")
			elseif language=="sv" then require ("game/dialogs/sv/l63sv")
			 elseif language=="thai" then require ("game/dialogs/th/l63th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev63() end
		
		elseif nLevel==64 then
		
		
			require("game/dialogs/talkies64")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l64chi")
			elseif language=="da" then require ("game/dialogs/da/l64da")
			elseif language=="fi" then require ("game/dialogs/fi/l64fi")
			elseif language=="gr" then require ("game/dialogs/gr/l64gr")
			elseif language=="is" then require ("game/dialogs/is/l64is")
			elseif language=="it" then require ("game/dialogs/it/l64it")
			elseif language=="jp" then require ("game/dialogs/jp/l64jp")
			elseif language=="no" then require ("game/dialogs/no/l64no")
			elseif language=="pt" then require ("game/dialogs/pt/l64pt")
			elseif language=="th" then require ("game/dialogs/th/l64th")
			elseif language=="de" then require ("game/dialogs/de/l64de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l64es")
			elseif language=="fr" then require ("game/dialogs/fr/l64fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l64en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l64pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l64ru")
			elseif language=="sv" then require ("game/dialogs/sv/l64sv")
			 elseif language=="thai" then require ("game/dialogs/th/l64th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev64() end
		
		elseif nLevel==65 then
		
		
			
			require("game/dialogs/talkies65")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l65chi")
			elseif language=="da" then require ("game/dialogs/da/l65da")
			elseif language=="fi" then require ("game/dialogs/fi/l65fi")
			elseif language=="gr" then require ("game/dialogs/gr/l65gr")
			elseif language=="is" then require ("game/dialogs/is/l65is")
			elseif language=="it" then require ("game/dialogs/it/l65it")
			elseif language=="jp" then require ("game/dialogs/jp/l65jp")
			elseif language=="no" then require ("game/dialogs/no/l65no")
			elseif language=="pt" then require ("game/dialogs/pt/l65pt")
			elseif language=="th" then require ("game/dialogs/th/l65th")
			elseif language=="de" then require ("game/dialogs/de/l65de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l65es")
			elseif language=="fr" then require ("game/dialogs/fr/l65fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l65en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l65pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l65ru")
			elseif language=="sv" then require ("game/dialogs/sv/l65sv")
			 elseif language=="thai" then require ("game/dialogs/th/l65th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev65() end
			
			elseif nLevel==66 then
			
				
					require("game/dialogs/talkies66")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l66chi")
			elseif language=="da" then require ("game/dialogs/da/l66da")
			elseif language=="fi" then require ("game/dialogs/fi/l66fi")
			elseif language=="gr" then require ("game/dialogs/gr/l66gr")
			elseif language=="is" then require ("game/dialogs/is/l66is")
			elseif language=="it" then require ("game/dialogs/it/l66it")
			elseif language=="jp" then require ("game/dialogs/jp/l66jp")
			elseif language=="no" then require ("game/dialogs/no/l66no")
			elseif language=="pt" then require ("game/dialogs/pt/l66pt")
			elseif language=="th" then require ("game/dialogs/th/l66th")
			elseif language=="de" then require ("game/dialogs/de/l66de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l66es")
			elseif language=="fr" then require ("game/dialogs/fr/l66fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l66en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l66nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l66pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l66ru")
			elseif language=="sv" then require ("game/dialogs/sv/l66sv")
			 elseif language=="thai" then require ("game/dialogs/th/l66th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev66() end
		
		
			
			elseif nLevel==67 then
			
			
						require("game/dialogs/talkies67")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l67chi")
			elseif language=="da" then require ("game/dialogs/da/l67da")
			elseif language=="fi" then require ("game/dialogs/fi/l67fi")
			elseif language=="gr" then require ("game/dialogs/gr/l67gr")
			elseif language=="is" then require ("game/dialogs/is/l67is")
			elseif language=="it" then require ("game/dialogs/it/l67it")
			elseif language=="jp" then require ("game/dialogs/jp/l67jp")
			elseif language=="no" then require ("game/dialogs/no/l67no")
			elseif language=="pt" then require ("game/dialogs/pt/l67pt")
			elseif language=="th" then require ("game/dialogs/th/l67th")
			elseif language=="de" then require ("game/dialogs/de/l67de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l67es")
			elseif language=="fr" then require ("game/dialogs/fr/l67fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l67en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l67pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l67ru")
			elseif language=="sv" then require ("game/dialogs/sv/l67sv")
			 elseif language=="thai" then require ("game/dialogs/th/l67h")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev67() end
			
			elseif nLevel==68 then 
				require("game/dialogs/talkies68")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l68chi")
			elseif language=="da" then require ("game/dialogs/da/l68da")
			elseif language=="fi" then require ("game/dialogs/fi/l68fi")
			elseif language=="gr" then require ("game/dialogs/gr/l68gr")
			elseif language=="is" then require ("game/dialogs/is/l68is")
			elseif language=="it" then require ("game/dialogs/it/l68it")
			elseif language=="jp" then require ("game/dialogs/jp/l68jp")
			elseif language=="no" then require ("game/dialogs/no/l68no")
			elseif language=="pt" then require ("game/dialogs/pt/l68pt")
			elseif language=="th" then require ("game/dialogs/th/l68th")
			elseif language=="de" then require ("game/dialogs/de/l68de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l68es")
			elseif language=="fr" then require ("game/dialogs/fr/l68fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l68en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l68pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l68ru")
			elseif language=="sv" then require ("game/dialogs/sv/l68sv")
			 elseif language=="thai" then require ("game/dialogs/th/l68th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev68() end
			
			
			elseif nLevel==69 then
			
				require("game/dialogs/talkies69")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l69chi")
			elseif language=="da" then require ("game/dialogs/da/l69da")
			elseif language=="fi" then require ("game/dialogs/fi/l69fi")
			elseif language=="gr" then require ("game/dialogs/gr/l69gr")
			elseif language=="is" then require ("game/dialogs/is/l69is")
			elseif language=="it" then require ("game/dialogs/it/l69it")
			elseif language=="jp" then require ("game/dialogs/jp/l69jp")
			elseif language=="no" then require ("game/dialogs/no/l69no")
			elseif language=="pt" then require ("game/dialogs/pt/l69pt")
			elseif language=="th" then require ("game/dialogs/th/l69th")
			elseif language=="de" then require ("game/dialogs/de/l69de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l69es")
			elseif language=="fr" then require ("game/dialogs/fr/l69fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l69en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l69pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l69ru")
			elseif language=="sv" then require ("game/dialogs/sv/l69sv")
			 elseif language=="thai" then require ("game/dialogs/th/l69th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev69() end
		
		elseif nLevel==70 then
		
		
			
				require("game/dialogs/talkies70")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")
				if language=="chi" then require ("game/dialogs/chi/l70chi")
			elseif language=="da" then require ("game/dialogs/da/l70da")
			elseif language=="fi" then require ("game/dialogs/fi/l70fi")
			elseif language=="gr" then require ("game/dialogs/gr/l70gr")
			elseif language=="is" then require ("game/dialogs/is/l70is")
			elseif language=="it" then require ("game/dialogs/it/l70it")
			elseif language=="jp" then require ("game/dialogs/jp/l70jp")
			elseif language=="no" then require ("game/dialogs/no/l70no")
			elseif language=="pt" then require ("game/dialogs/pt/l70pt")
			elseif language=="th" then require ("game/dialogs/th/l70th")
			elseif language=="de" then require ("game/dialogs/de/l70de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l70es")
			elseif language=="fr" then require ("game/dialogs/fr/l70fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l70en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l70pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l70ru")
			elseif language=="sv" then require ("game/dialogs/sv/l70sv")
			 elseif language=="thai" then require ("game/dialogs/th/l70th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev70() end
		
		elseif nLevel==71 then 
		
		
				require("game/dialogs/talkies71")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l71chi")
			elseif language=="da" then require ("game/dialogs/da/l71da")
			elseif language=="fi" then require ("game/dialogs/fi/l71fi")
			elseif language=="gr" then require ("game/dialogs/gr/l71gr")
			elseif language=="is" then require ("game/dialogs/is/l71is")
			elseif language=="it" then require ("game/dialogs/it/l71it")
			elseif language=="jp" then require ("game/dialogs/jp/l71jp")
			elseif language=="no" then require ("game/dialogs/no/l71no")
			elseif language=="pt" then require ("game/dialogs/pt/l71pt")
			elseif language=="th" then require ("game/dialogs/th/l71th")
			elseif language=="de" then require ("game/dialogs/de/l71de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l71es")
			elseif language=="fr" then require ("game/dialogs/fr/l71fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l71en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l71pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l71ru")
			elseif language=="sv" then require ("game/dialogs/sv/l71sv")
			 elseif language=="thai" then require ("game/dialogs/th/l71th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev71() end
		
		elseif nLevel==72 then
		
		
				require("game/dialogs/talkies72")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l72chi")
			elseif language=="da" then require ("game/dialogs/da/l72da")
			elseif language=="fi" then require ("game/dialogs/fi/l72fi")
			elseif language=="gr" then require ("game/dialogs/gr/l72gr")
			elseif language=="is" then require ("game/dialogs/is/l72is")
			elseif language=="it" then require ("game/dialogs/it/l72it")
			elseif language=="jp" then require ("game/dialogs/jp/l72jp")
			elseif language=="no" then require ("game/dialogs/no/l72no")
			elseif language=="pt" then require ("game/dialogs/pt/l72pt")
			elseif language=="th" then require ("game/dialogs/th/l72th")
			elseif language=="de" then require ("game/dialogs/de/l72de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l72es")
			elseif language=="fr" then require ("game/dialogs/fr/l72fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l72en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l72pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l72ru")
			elseif language=="sv" then require ("game/dialogs/sv/l72sv")
			 elseif language=="thai" then require ("game/dialogs/th/l72th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev72() end
		
		elseif nLevel==73 then
		
		
				require("game/dialogs/talkies73")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l73chi")
			elseif language=="da" then require ("game/dialogs/da/l73da")
			elseif language=="fi" then require ("game/dialogs/fi/l73fi")
			elseif language=="gr" then require ("game/dialogs/gr/l73gr")
			elseif language=="is" then require ("game/dialogs/is/l73is")
			elseif language=="it" then require ("game/dialogs/it/l73it")
			elseif language=="jp" then require ("game/dialogs/jp/l73jp")
			elseif language=="no" then require ("game/dialogs/no/l73no")
			elseif language=="pt" then require ("game/dialogs/pt/l73pt")
			elseif language=="th" then require ("game/dialogs/th/l73th")
			elseif language=="de" then require ("game/dialogs/de/l73de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l73es")
			elseif language=="fr" then require ("game/dialogs/fr/l73fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l73en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l73pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l73ru")
			elseif language=="sv" then require ("game/dialogs/sv/l73sv")
			 elseif language=="thai" then require ("game/dialogs/th/l73th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev73() end
		
		elseif nLevel==74 then 
		
		
			
				require("game/dialogs/talkies74")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l74chi")
			elseif language=="da" then require ("game/dialogs/da/l74da")
			elseif language=="fi" then require ("game/dialogs/fi/l74fi")
			elseif language=="gr" then require ("game/dialogs/gr/l74gr")
			elseif language=="is" then require ("game/dialogs/is/l74is")
			elseif language=="it" then require ("game/dialogs/it/l74it")
			elseif language=="jp" then require ("game/dialogs/jp/l74jp")
			elseif language=="no" then require ("game/dialogs/no/l74no")
			elseif language=="pt" then require ("game/dialogs/pt/l74pt")
			elseif language=="th" then require ("game/dialogs/th/l74th")
			elseif language=="de" then require ("game/dialogs/de/l74de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l74es")
			elseif language=="fr" then require ("game/dialogs/fr/l74fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l74en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l74pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l74ru")
			elseif language=="sv" then require ("game/dialogs/sv/l74sv")
			 elseif language=="thai" then require ("game/dialogs/th/l74th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev74() end
		
		elseif nLevel==75 then 
		
		
				require("game/dialogs/talkies75")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l75chi")
			elseif language=="da" then require ("game/dialogs/da/l75da")
			elseif language=="fi" then require ("game/dialogs/fi/l75fi")
			elseif language=="gr" then require ("game/dialogs/gr/l75gr")
			elseif language=="is" then require ("game/dialogs/is/l75is")
			elseif language=="it" then require ("game/dialogs/it/l75it")
			elseif language=="jp" then require ("game/dialogs/jp/l75jp")
			elseif language=="no" then require ("game/dialogs/no/l75no")
			elseif language=="pt" then require ("game/dialogs/pt/l75pt")
			elseif language=="th" then require ("game/dialogs/th/l75th")
			elseif language=="de" then require ("game/dialogs/de/l75de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l75es")
			elseif language=="fr" then require ("game/dialogs/fr/l75fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l75en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l75pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l75ru")
			elseif language=="sv" then require ("game/dialogs/sv/l75sv")
			 elseif language=="thai" then require ("game/dialogs/th/l75th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev75() end
		
		elseif nLevel==76 then 
		
		
				require("game/dialogs/talkies76")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l76chi")
			elseif language=="da" then require ("game/dialogs/da/l76da")
			elseif language=="fi" then require ("game/dialogs/fi/l76fi")
			elseif language=="gr" then require ("game/dialogs/gr/l76gr")
			elseif language=="is" then require ("game/dialogs/is/l76is")
			elseif language=="it" then require ("game/dialogs/it/l76it")
			elseif language=="jp" then require ("game/dialogs/jp/l76jp")
			elseif language=="no" then require ("game/dialogs/no/l76no")
			elseif language=="pt" then require ("game/dialogs/pt/l76pt")
			elseif language=="th" then require ("game/dialogs/th/l76th")
			elseif language=="de" then require ("game/dialogs/de/l76de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l76es")
			elseif language=="fr" then require ("game/dialogs/fr/l76fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l76en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l76pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l76ru")
			elseif language=="sv" then require ("game/dialogs/sv/l76sv")
			 elseif language=="thai" then require ("game/dialogs/th/l76th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev76() end
		
		elseif nLevel==77 then 
		
					require("game/dialogs/talkies77")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l77chi")
			elseif language=="da" then require ("game/dialogs/da/l77da")
			elseif language=="fi" then require ("game/dialogs/fi/l77fi")
			elseif language=="gr" then require ("game/dialogs/gr/l77gr")
			elseif language=="is" then require ("game/dialogs/is/l77is")
			elseif language=="it" then require ("game/dialogs/it/l77it")
			elseif language=="jp" then require ("game/dialogs/jp/l77jp")
			elseif language=="no" then require ("game/dialogs/no/l77no")
			elseif language=="pt" then require ("game/dialogs/pt/l77pt")
			elseif language=="th" then require ("game/dialogs/th/l77th")
			elseif language=="de" then require ("game/dialogs/de/l77de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l77es")
			elseif language=="fr" then require ("game/dialogs/fr/l77fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l77en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l77pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l77ru")
			elseif language=="sv" then require ("game/dialogs/sv/l77sv")
			 elseif language=="thai" then require ("game/dialogs/th/l77th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev77() end
		
		elseif nLevel==78 then 
				require("game/dialogs/talkies78")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l78chi")
			elseif language=="da" then require ("game/dialogs/da/l78da")
			elseif language=="fi" then require ("game/dialogs/fi/l78fi")
			elseif language=="gr" then require ("game/dialogs/gr/l78gr")
			elseif language=="is" then require ("game/dialogs/is/l78is")
			elseif language=="it" then require ("game/dialogs/it/l78it")
			elseif language=="jp" then require ("game/dialogs/jp/l78jp")
			elseif language=="no" then require ("game/dialogs/no/l78no")
			elseif language=="pt" then require ("game/dialogs/pt/l78pt")
			elseif language=="th" then require ("game/dialogs/th/l78th")
			elseif language=="de" then require ("game/dialogs/de/l78de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l78es")
			elseif language=="fr" then require ("game/dialogs/fr/l78fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l78en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l78pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l78ru")
			elseif language=="sv" then require ("game/dialogs/sv/l78sv")
			 elseif language=="thai" then require ("game/dialogs/th/l78th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev78() end
		
		elseif nLevel==79 then
		
		
			
				require("game/dialogs/talkies79")
		if talkies==true then
		Talkies.clearMessages()
				--if language=="bg" then require ("game/dialogs/es/l31bg")
			--elseif language=="br" then require ("game/dialogs/es/l31br")	
			--elseif language=="ch" then require ("game/dialogs/es/l31ch")	
			--elseif language=="cs" then require ("game/dialogs/es/l31cs")	
				if language=="chi" then require ("game/dialogs/chi/l79chi")
			elseif language=="da" then require ("game/dialogs/da/l79da")
			elseif language=="fi" then require ("game/dialogs/fi/l79fi")
			elseif language=="gr" then require ("game/dialogs/gr/l79gr")
			elseif language=="is" then require ("game/dialogs/is/l79is")
			elseif language=="it" then require ("game/dialogs/it/l79it")
			elseif language=="jp" then require ("game/dialogs/jp/l79jp")
			elseif language=="no" then require ("game/dialogs/no/l79no")
			elseif language=="pt" then require ("game/dialogs/pt/l79pt")
			elseif language=="th" then require ("game/dialogs/th/l79th")
			elseif language=="de" then require ("game/dialogs/de/l79de")
			--elseif language=="eo" then require ("game/dialogs/es/l31eo")
			elseif language=="es" then require ("game/dialogs/es/l79es")
			elseif language=="fr" then require ("game/dialogs/fr/l79fr")
			elseif language=="en" and not (language2=="pl") then require ("game/dialogs/en/l79en")
			--elseif language=="it" then require ("game/dialogs/es/l31it")
			elseif language2=="nl" then require ("game/dialogs/nl/l31nl")
			elseif language2=="pl" then require ("game/dialogs/pl/l79pl")
			--elseif language=="pr" then require ("game/dialogs/es/l31pr")
			elseif language=="ru" then require ("game/dialogs/ru/l79ru")
			elseif language=="sv" then require ("game/dialogs/sv/l79sv")
			 elseif language=="thai" then require ("game/dialogs/th/l79th")
			--elseif language=="sk" then require ("game/dialogs/es/l31sk")
			end
			
		Obey.lev79() end
			
		
end
end

function assignSubtitlesToVariable()

			if nLevel==1 then CSub=l1
		elseif nLevel==2 then CSub=l2
		elseif nLevel==3 then CSub=l3
		elseif nLevel==4 then CSub=l4
		elseif nLevel==5 then CSub=l5
		elseif nLevel==6 then CSub=l6
		elseif nLevel==7 then CSub=l7
		elseif nLevel==8 then CSub=l8
		elseif nLevel==9 then CSub=l9
		elseif nLevel==10 then CSub=l10
		elseif nLevel==11 then CSub=l11
		elseif nLevel==12 then CSub=l12
		elseif nLevel==13 then CSub=l13
		elseif nLevel==14 then CSub=l14
		elseif nLevel==15 then CSub=l15
		elseif nLevel==16 then CSub=l16
		elseif nLevel==17 then CSub=l17
		elseif nLevel==18 then CSub=l18
		elseif nLevel==19 then CSub=l19
		elseif nLevel==20 then CSub=l20
		elseif nLevel==21 then CSub=l21
		elseif nLevel==22 then CSub=l22
		elseif nLevel==23 then CSub=l23
		elseif nLevel==24 then CSub=l24
		elseif nLevel==25 then CSub=l25
		elseif nLevel==26 then CSub=l26
		elseif nLevel==27 then CSub=l27
		elseif nLevel==28 then CSub=l28
		elseif nLevel==29 then CSub=l29
		elseif nLevel==30 then CSub=l30
		elseif nLevel==31 then CSub=l31
		elseif nLevel==32 then CSub=l32
		elseif nLevel==33 then CSub=l33
		elseif nLevel==34 then CSub=l34
		elseif nLevel==35 then CSub=l35
		elseif nLevel==36 then CSub=l36
		elseif nLevel==37 then CSub=l37
		elseif nLevel==38 then CSub=l38
		elseif nLevel==39 then CSub=l39
		
		elseif nLevel==40 then CSub=l40
		elseif nLevel==41 then CSub=l41
		elseif nLevel==42 then CSub=l42
		elseif nLevel==43 then CSub=l43
		elseif nLevel==44 then CSub=l44
		elseif nLevel==45 then CSub=l45
		
		elseif nLevel==46 then CSub=l46
		elseif nLevel==47 then CSub=l47
		elseif nLevel==48 then CSub=l48
		elseif nLevel==49 then CSub=l49
		elseif nLevel==40 then CSub=l50
		elseif nLevel==51 then CSub=l51
		elseif nLevel==52 then CSub=l52
		elseif nLevel==53 then CSub=l53
		elseif nLevel==54 then CSub=l54
		elseif nLevel==55 then CSub=l55
		elseif nLevel==56 then CSub=l56
		elseif nLevel==57 then CSub=l57
		elseif nLevel==58 then CSub=l58
		
		elseif nLevel==59 then CSub=l59
		elseif nLevel==60 then CSub=l60
		elseif nLevel==61 then CSub=l61
		elseif nLevel==62 then CSub=l62
		elseif nLevel==63 then CSub=l63
		elseif nLevel==64 then CSub=l64
		elseif nLevel==65 then CSub=l65
		elseif nLevel==66 then CSub=l66
		elseif nLevel==67 then CSub=l67
		elseif nLevel==68 then CSub=l68
		elseif nLevel==69 then CSub=l69
		elseif nLevel==70 then CSub=l70
		elseif nLevel==71 then CSub=l71
		elseif nLevel==72 then CSub=l72
		elseif nLevel==73 then CSub=l73
		elseif nLevel==74 then CSub=l74
		elseif nLevel==75 then CSub=l75
		elseif nLevel==76 then CSub=l76
		elseif nLevel==77 then CSub=l77
		elseif nLevel==78 then CSub=l78
		elseif nLevel==79 then CSub=l79
		end
end
