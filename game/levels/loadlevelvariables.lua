function loadlevelvariables()

	local ddwidth, ddheight = love.window.getDesktopDimensions( display )
	-- initialize lip sync variables
    lipstimer=0	
	lipsiter=1
	lipsline=1
	lipsitercompleted=false
	if nLevel==1 then 
	whalepositionx=-500 
	whalepositiony=0 
	
	whalescalex=0.6
	whalescaley=0.6
	
	ratesteps = 75
	
	
	
	boids.list = {} N_BOIDS = 200 VMAX = 40 W_WIDTH = ddwidth/1.6 initDemo()		-- change boids parameters
				
	
elseif nLevel==2 then boids.list = {} N_BOIDS = 800 VMAX = 60 W_WIDTH = ddwidth initDemo()		-- change boids parameters
elseif nLevel==3 then boids.list = {} N_BOIDS = 800 VMAX = 20 W_WIDTH = ddwidth initDemo()		-- change boids parameters
elseif nLevel==4 then strangecreatureframe=0 snailframe=0
elseif nLevel==5 then crabframe=0 snailframe=0
elseif nLevel==6 then broommoving=false broomframe=0
elseif nLevel==7 then crabframe=0 snailframe=0 
						boids.list = {} N_BOIDS = 100 initDemo()		-- change boids parameters
elseif nLevel==8 then
elseif nLevel==9 then periframe=0	mirrorpositionx=0
elseif nLevel==10 or nLevel==18 then 
	boatwindowframe=0
	boatglassframe=0
	glastimer=0
	glassmoving=false
	glassstandmoving=false
elseif nLevel==11 then
	elkframe=0
	deadparrotframe=0
	snailframe=0
elseif nLevel==12 then
	medusaframe=0
	rybickaframe=0
elseif nLevel==13 or nLevel==17 then
		vikingframe=0
		snailframe=0
		SkullDialogSaidOnce=nil
elseif nLevel==14 then eyeframe=0
elseif nLevel==15 then 
	dalekohledframe=0 
	mikroskop=0
	snailframe=0
	mikroskopframe=0
	budikframe=0
	bubblesy=0
	--boids.list = {} N_BOIDS = 300 VMAX = 20 W_WIDTH = ddwidth/1.6 W_HEIGHT = ddheight/1.6 W_LIMIT = 0 initDemo()		-- change boids parameters
elseif nLevel==16 then snailframe=0


elseif nLevel==19 then neptunframe=0 poseidonframe=0
elseif nLevel==20 or nLevel==28 then timer=0 faceframe=0 crossframe=0 skullpushed=false SkullDialogSaidOnce=nil
boids.list = {} N_BOIDS = 800 VMAX = 30 initDemo()
elseif nLevel==21 then statueframe=0 crabframe=0
boids.list = {} N_BOIDS = 500 VMAX = 15 initDemo()
elseif nLevel==22 then faceframe=0
elseif nLevel==23 then
elseif nLevel==24 then
elseif nLevel==25 then boids.list = {} VMAX = 30 initDemo()
elseif nLevel==26 then
elseif nLevel==27 then dooropened=false statueframe=0 crabframe=0 crab2frame=0 shrimpframe=0 ballframe=0 antickaframe=0

elseif nLevel==29 then
		crabframe=0
		snailframe=0
		faceframe=0
		plugisinplace=false
elseif nLevel==30 then 

boids.list = {} N_BOIDS = 300 VMAX = 60 W_WIDTH = ddwidth/1.6  initDemo()		-- change boids parameters
elseif nLevel==31 then

 boids.list = {} N_BOIDS = 500 VMAX = 60 W_WIDTH = ddwidth/1.6  initDemo()		-- change boids parameters
elseif nLevel==32 then
		sasankaframe=0
		seahorseframe=0
		snailframe=0
		
		boids.list = {} N_BOIDS = 800 VMAX = 20 W_WIDTH = ddwidth/1.6 initDemo()		-- change boids parameters
elseif nLevel==33 then		
		timer=0
		crabframe=0
		seahorseframe=0
		boids.list = {} N_BOIDS = 100 VMAX = 20 W_WIDTH = ddwidth initDemo()
elseif nLevel==34 then				
			whalescalex=1
			whalescaley=1
			whalepositionx=400
			whalepositiony=300
			sasankaframe=0
			balalframe=0
			sepieframe=0
			snekframe=0
			crabframe=0
			boids.list = {} N_BOIDS = 400 VMAX = 20 W_WIDTH = ddwidth initDemo()
elseif nLevel==35 then
			klavirframe=0
			sasankaframe=0
			sepieframe=0
			crabframe=0
			rejnokframe=0
			boids.list = {} N_BOIDS = 400 VMAX = 20 W_WIDTH = ddwidth initDemo()
elseif nLevel==36 then			
			whalescalex=1
			whalescaley=1
			whalepositionx=400
			whalepositiony=300
			boids.list = {} N_BOIDS = 200 VMAX = 20 W_WIDTH = ddwidth initDemo()
elseif nLevel==37 then
			turtlestatus="idle"
			zframe=0
elseif nLevel==38 then timer3=0
elseif nLevel==40 then washmachineframe=0			
boids.list = {} N_BOIDS = 800 VMAX = 40 initDemo()		-- change boids parameters
elseif nLevel==41 then boids.list = {} N_BOIDS = 800 VMAX = 40 initDemo()		-- change boids parameters
elseif nLevel==42 then pinkmonsterframe=0
boids.list = {} N_BOIDS = 800 VMAX = 40 initDemo()		-- change boids parameters
elseif nLevel==43 then 
			mnohonozkaframe=0
			budikframe=0
			medusaframe=0
			meduzaframe=0
			strangecreatureframe=0
			uhorframe=0
			balonekframe=0
			boids.list = {} N_BOIDS = 800 VMAX = 40 initDemo()		-- change boids parameters
			
			
elseif nLevel==44 then
	--initialize mutant animations
			barelframe=0
			bagetframe=0
			doubleframe=0
			hadframe=0
			hlubinnaframe=0
			kachnaframe=0
			killerframe=0
			mutantkrabframe=0
			kukajdaframe=0
			pinkmonsterframe=0
			mutantsharkframe=0
			bagetframe=0
			nohaframe=0
			mutanteyeframe=0
			boids.list = {} N_BOIDS = 800 VMAX = 40 initDemo()		-- change boids parameters
elseif nLevel==45 then chobotniceframe=0 papouchaframe=0 SkullDialogSaidOnce=nil
elseif nLevel==46 then snehulakframe=0
elseif nLevel==47 then deloframe=0
elseif nLevel==48 then
elseif nLevel==49 then chobotniceframe=0 papouchaframe=0
elseif nLevel==50 then krystalcframe=0 drahokamframe=0 lebzaframe=0 okoframe=0
elseif nLevel==51 then
elseif nLevel==52 then pinkmonsterframe=0 shader2=true
elseif nLevel==53 then
elseif nLevel==54 then alienenginekeyframe=0 shader2=true
elseif nLevel==55 then pipepushed=false
elseif nLevel==56 then
			shader2=true
			lightswitchon=true
			lightswitchpushedafterfalling=false
			lightonvoiceplayed=false
			aproachrobodog=false
elseif nLevel==57 then 
		shader2=true
		thinjarframe=0 
		porganismframe=0
		qorganismframe=0
		okaframe=0
		malaframe=0
		mutantframe=0
		sklenaframe=0
		horni_tvorframe=0
		lahvacframe=0 lahvaccrashframe=0 lahvaccrashedframe=0 lahvacstate="complete"
		
		--events
		aproach3eyes=false

		
elseif nLevel==58 then
			shader2=true
			timer=0
			pohonframe=0
			syncwashingmachine=0
			washmachinedrumframe=0
			transitionmixdone=false
			transitionmixdone2=false
			laundrystarted=false
			ufoframe=0
			plutonium4frame=0
			hadiceframe=0
			podstavecframe=0
			
			
elseif nLevel==59 then
			totemframe=0
			seahorseframe=0
			bigskull_pushed=false
			SkullDialogSaidOnce=nil
elseif nLevel==60 then krystalframe=0
elseif nLevel==61 then krystalframe=0 korunaframe=0
elseif nLevel==62 then
			timer=0
			korunaframe=0
			krystalframe=0

elseif nLevel==63 then dasfishframe=0
elseif nLevel==64 then stepdone=0 krystalframe=0 krystalMovement=0 totalHolyGrails=25
elseif nLevel==65 then
elseif nLevel==66 then
elseif nLevel==67 then
elseif nLevel==68 then
elseif nLevel==69 then
elseif nLevel==70 then stepdone=0
elseif nLevel==71 then
elseif nLevel==72 then nahoreframe=0 velrybframe=0 semaforframe=0 konikframe=0 seahorseframe=0 timer3=0
elseif nLevel==73 then
elseif nLevel==74 then
elseif nLevel==75 then
elseif nLevel==76 then
elseif nLevel==77 then linuxakframe=0
elseif nLevel==78 then
elseif nLevel==79 then
elseif nLevel==101 then
elseif nLevel==103 then shader1=false		-- Tetris level
end





end



