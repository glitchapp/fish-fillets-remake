function setreverbeffect()
	--love.audio.setEffect('myEffect', {type = 'reverb'})
end

function setflangereffect()
	love.audio.setEffect('myEffect', {
							type = 'flanger',
							phase = .5,
							rate = .25,
							})
end

function setcompressoreffect()
love.audio.setEffect('myEffect', {type = 'compressor'})
end

function loadvoices()

love.audio.setEffect('myEffect', {type = 'reverb'})
--love.audio.setEffect('myEffect', {
--type = 'chorus',
--})

	--love.audio.stop()
if nLevel==1 then
	
	icantgetsaidonce=false
	wowsaidonce=false
	--pipefalled=false
	
	
	if language=="en" or language=="chi" then
			if accent=="br" or language=="chi"  then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/en/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/en/3weshouldgoandhave.ogg","stream" )
				youcancontrolus = love.audio.newSource( "/externalassets/dialogs/level1/en/7youcancontrolus.ogg","stream" )
				ifwecangetthere= love.audio.newSource( "/externalassets/dialogs/level1/en/9ifwecangetthere.ogg","stream" )
				andwhenyourightclick= love.audio.newSource( "/externalassets/dialogs/level1/en/11andwhenyourightclick.ogg","stream" )
				andbecareful= love.audio.newSource( "/externalassets/dialogs/level1/en/13andbecareful.ogg","stream" )
				okay= love.audio.newSource( "/externalassets/dialogs/level1/en/15okay.ogg","stream" )
				
				noproblem = love.audio.newSource( "/externalassets/dialogs/level1/en/noproblem.ogg","stream" )
				
				
			elseif accent=="us" then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/en-us/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/en-us/3weshouldgoandhave.ogg","stream" )
				youcancontrolus = love.audio.newSource( "/externalassets/dialogs/level1/en-us/7youcancontrolus.ogg","stream" )
				ifwecangetthere= love.audio.newSource( "/externalassets/dialogs/level1/en-us/9ifwecangetthere.ogg","stream" )
				andwhenyourightclick= love.audio.newSource( "/externalassets/dialogs/level1/en-us/11andwhenyourightclick.ogg","stream" )
				andbecareful= love.audio.newSource( "/externalassets/dialogs/level1/en-us/13andbecareful.ogg","stream" )
				okay= love.audio.newSource( "/externalassets/dialogs/level1/en-us/15okay.ogg","stream" )
				
					noproblem = love.audio.newSource( "/externalassets/dialogs/level1/en-us/noproblem.ogg","stream" )
					
			end
	
				--whatwasthat:play()				
				
		elseif language=="es" then 
			if accent=="es" then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/es/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/es/3weshouldgoandhave.ogg","stream" )
			elseif accent=="la" then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/es-la/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/es-la/3weshouldgoandhave.ogg","stream" )
				youcancontrolus = love.audio.newSource( "/externalassets/dialogs/level1/es-la/7youcancontrolus.ogg","stream" )
				ifwecangetthere= love.audio.newSource( "/externalassets/dialogs/level1/es-la/9ifwecangetthere.ogg","stream" )
				andwhenyourightclick= love.audio.newSource( "/externalassets/dialogs/level1/es-la/11andwhenyourightclick.ogg","stream" )
				andbecareful= love.audio.newSource( "/externalassets/dialogs/level1/es-la/13andbecareful.ogg","stream" )
				okay= love.audio.newSource( "/externalassets/dialogs/level1/es-la/15okay.ogg","stream" )
				
					noproblem = love.audio.newSource( "/externalassets/dialogs/level1/es-la/noproblem.ogg","stream" )
					
				
			end
			
			
				
		elseif language=="de" then
				dialogsde1m = love.audio.newSource( "/externalassets/dialogs/level1/de/level1dem.ogg","stream" )
				dialogsde1m:setEffect('myEffect')
				dialogsde1m:play()

				
		elseif language=="it" then
				dialogsit1m = love.audio.newSource( "/externalassets/dialogs/level1/it/level1itm.ogg","stream" )
				dialogsit1m:setEffect('myEffect')
				dialogsit1m:play()
		
		elseif language=="fr" then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/fr/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/fr/3weshouldgoandhave.ogg","stream" )
				youcancontrolus = love.audio.newSource( "/externalassets/dialogs/level1/fr/7youcancontrolus.ogg","stream" )
				ifwecangetthere= love.audio.newSource( "/externalassets/dialogs/level1/fr/9ifwecangetthere.ogg","stream" )
				andwhenyourightclick= love.audio.newSource( "/externalassets/dialogs/level1/fr/11andwhenyourightclick.ogg","stream" )
				andbecareful= love.audio.newSource( "/externalassets/dialogs/level1/fr/13andbecareful.ogg","stream" )
				okay= love.audio.newSource( "/externalassets/dialogs/level1/fr/15okay.ogg","stream" )
				
				-- French
				noproblem = love.audio.newSource( "/externalassets/dialogs/level1/fr/noproblem.ogg","stream" )
		
		
		elseif language=="ru" then
				dialogsru1m = love.audio.newSource( "/externalassets/dialogs/level1/ru/level1rum.ogg","stream" )
				dialogsru1m:setEffect('myEffect')
				dialogsru1m:play()

		elseif language=="nl" then
				ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level1/nl/2ihavenoidea.ogg","stream" )
				weshouldgoandhave = love.audio.newSource( "/externalassets/dialogs/level1/nl/3weshouldgoandhave.ogg","stream" )
				youcancontrolus = love.audio.newSource( "/externalassets/dialogs/level1/nl/7youcancontrolus.ogg","stream" )
				ifwecangetthere= love.audio.newSource( "/externalassets/dialogs/level1/nl/9ifwecangetthere.ogg","stream" )
				andwhenyourightclick= love.audio.newSource( "/externalassets/dialogs/level1/nl/11andwhenyourightclick.ogg","stream" )
				andbecareful= love.audio.newSource( "/externalassets/dialogs/level1/nl/13andbecareful.ogg","stream" )
				okay= love.audio.newSource( "/externalassets/dialogs/level1/nl/15okay.ogg","stream" )
				
					noproblem = love.audio.newSource( "/externalassets/dialogs/level1/nl/noproblem.ogg","stream" )
		end
		
		
				
		if language2=="en" then
		
				if accent2=="br" or language=="chi" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/en/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/en/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/en/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/en/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/en/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/en/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/en/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/en/14ohstopit.ogg","stream" )
				
				--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/en/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/en/icantmovethatcylinder.ogg","stream" )
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/en/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/en/thanksnowicango.ogg","stream" )
				
			elseif accent2=="us" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/en-us/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/en-us/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/en-us/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/en-us/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/en-us/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/en-us/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/en-us/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/en-us/14ohstopit.ogg","stream" )
				
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/en-us/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/en-us/icantmovethatcylinder.ogg","stream" )
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/en-us/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/en-us/thanksnowicango.ogg","stream" )
			end
		elseif language2=="es" then
			if accent2=="es" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/es/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/es/4waitimgoingwithyou.ogg","stream" )
				
				
				
			elseif accent2=="la" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/es-la/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/es-la/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/es-la/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/es-la/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/es-la/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/es-la/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/es-la/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/es-la/14ohstopit.ogg","stream" )
				
				--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/es-la/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/es-la/icantmovethatcylinder.ogg","stream" )
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/es-la/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/es-la/thanksnowicango.ogg","stream" )
			end
		elseif language2=="fr" then
			
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/fr/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/fr/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/fr/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/fr/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/fr/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/fr/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/fr/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/fr/14ohstopit.ogg","stream" )
				
				--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/fr/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/fr/icantmovethatcylinder.ogg","stream" )
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/fr/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/fr/thanksnowicango.ogg","stream" )
			
						
		elseif language2=="pl" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/pl/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/pl/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/pl/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/pl/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/pl/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/pl/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/pl/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/pl/14ohstopit.ogg","stream" )
				
				--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/pl/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/pl/icantmovethatcylinder.ogg","stream" )
				
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/pl/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/pl/thanksnowicango.ogg","stream" )
		
		
		elseif language2=="cs" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/cs/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/cs/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/cs/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/cs/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/cs/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/cs/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/cs/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/cs/14ohstopit.ogg","stream" )
				
				--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/cs/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/cs/icantmovethatcylinder.ogg","stream" )
				
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/cs/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/cs/thanksnowicango.ogg","stream" )
				
				
		elseif language2=="de" then
				dialogsde1f = love.audio.newSource( "/externalassets/dialogs/level1/de/level1def.ogg","stream" )
				dialogsde1f:setEffect('myEffect')
				dialogsde1f:play()
		elseif language2=="nl" then
				whatwasthat = love.audio.newSource( "/externalassets/dialogs/level1/nl/1whatwasthat.ogg","stream" )
				waitimgoingwithyou = love.audio.newSource( "/externalassets/dialogs/level1/nl/4waitimgoingwithyou.ogg","stream" )
				whyisntanything = love.audio.newSource( "/externalassets/dialogs/level1/nl/5whyisntanything.ogg","stream" )
				heiplayer = love.audio.newSource( "/externalassets/dialogs/level1/nl/6heiplayer.ogg","stream" )
				oryoucancontrol= love.audio.newSource( "/externalassets/dialogs/level1/nl/8oryoucancontrol.ogg","stream" )
				wellfollow= love.audio.newSource( "/externalassets/dialogs/level1/nl/10wellfollow.ogg","stream" )
				yourgoalshouldbe= love.audio.newSource( "/externalassets/dialogs/level1/nl/12yourgoalshouldbe.ogg","stream" )
				ohstopit= love.audio.newSource( "/externalassets/dialogs/level1/nl/14ohstopit.ogg","stream" )
				
						--pipe touched fish 2
				icantgetthrough = love.audio.newSource( "/externalassets/dialogs/level1/nl/icantgetthrough.ogg","stream" )
				icantmovethatcylinder = love.audio.newSource( "/externalassets/dialogs/level1/nl/icantmovethatcylinder.ogg","stream" )
				--pipe pushed from fish 1
				wowyoumovedit = love.audio.newSource( "/externalassets/dialogs/level1/nl/wowyoumovedit.ogg","stream" )
				thanksnowicango = love.audio.newSource( "/externalassets/dialogs/level1/nl/thanksnowicango.ogg","stream" )
		elseif language2=="it" then
				dialogsit1f = love.audio.newSource( "/externalassets/dialogs/level1/it/level1itf.ogg","stream" )
				dialogsit1f:setEffect('myEffect')
				dialogsit1f:play()
		elseif language2=="ru" then
				dialogsru1f = love.audio.newSource( "/externalassets/dialogs/level1/ru/level1ruf.ogg","stream" )
				dialogsru1f:setEffect('myEffect')
				dialogsru1f:play()
		end
		
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
				
				whatwasthat:setEffect('myEffect')						--reverb
				ihavenoidea:setEffect('myEffect')
				weshouldgoandhave:setEffect('myEffect')
				waitimgoingwithyou:setEffect('myEffect')
				whyisntanything:setEffect('myEffect')
				heiplayer:setEffect('myEffect')
				youcancontrolus:setEffect('myEffect')
				oryoucancontrol:setEffect('myEffect')
				ifwecangetthere:setEffect('myEffect')
				wellfollow:setEffect('myEffect')
				yourgoalshouldbe:setEffect('myEffect')
				andwhenyourightclick:setEffect('myEffect')
				andbecareful:setEffect('myEffect')
				ohstopit:setEffect('myEffect')
				okay:setEffect('myEffect')	
				
				icantgetthrough:setEffect('myEffect')
				icantmovethatcylinder:setEffect('myEffect')
				
				noproblem:setEffect('myEffect')
				
				wowyoumovedit:setEffect('myEffect')
				thanksnowicango:setEffect('myEffect')
	
elseif nLevel==2 then									-- level 2

	

if language=="en" or language=="chi" and not language2=="pl" then

			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/en/diskmessage/25thisdisk.ogg","stream" )


		if accent=="br" or language=="chi" then
			--self destructive message
					
				--part 1
				
				youknow = love.audio.newSource( "/externalassets/dialogs/level2/en/2youknow.ogg","stream" )
				letspushitdown = love.audio.newSource( "/externalassets/dialogs/level2/en/3letspushitdown.ogg","stream" )
				wellletsgettowork = love.audio.newSource( "/externalassets/dialogs/level2/en/4wellletsgettowork.ogg","stream" )
						
				--part 2

				sonowwecan = love.audio.newSource( "/externalassets/dialogs/level2/en/6sonowwecan.ogg","stream" )
				thisbytheway = love.audio.newSource( "/externalassets/dialogs/level2/en/8thisbytheway.ogg","stream" )
				goodidea = love.audio.newSource( "/externalassets/dialogs/level2/en/10goodidea.ogg","stream" )
				before = love.audio.newSource( "/externalassets/dialogs/level2/en/12before.ogg","stream" )
				illvolunteer = love.audio.newSource( "/externalassets/dialogs/level2/en/14illvolunteer.ogg","stream" )

				
				wecanalsoswim = love.audio.newSource( "/externalassets/dialogs/level2/en/wecanalsoswim.ogg","stream" )
				thatsaboutit = love.audio.newSource( "/externalassets/dialogs/level2/en/thatsaboutit.ogg","stream" )

				
			elseif accent=="us" then
				youknow = love.audio.newSource( "/externalassets/dialogs/level2/en-us/2youknow.ogg","stream" )
				letspushitdown = love.audio.newSource( "/externalassets/dialogs/level2/en-us/3letspushitdown.ogg","stream" )
				wellletsgettowork = love.audio.newSource( "/externalassets/dialogs/level2/en-us/4wellletsgettowork.ogg","stream" )
	
				--part 2
								
				sonowwecan = love.audio.newSource( "/externalassets/dialogs/level2/en-us/6sonowwecan.ogg","stream" )
				thisbytheway = love.audio.newSource( "/externalassets/dialogs/level2/en-us/8thisbytheway.ogg","stream" )
				goodidea = love.audio.newSource( "/externalassets/dialogs/level2/en-us/10goodidea.ogg","stream" )
				before = love.audio.newSource( "/externalassets/dialogs/level2/en-us/12before.ogg","stream" )
				illvolunteer = love.audio.newSource( "/externalassets/dialogs/level2/en-us/14illvolunteer.ogg","stream" )

				wecanalsoswim = love.audio.newSource( "/externalassets/dialogs/level2/en-us/wecanalsoswim.ogg","stream" )
				thatsaboutit = love.audio.newSource( "/externalassets/dialogs/level2/en-us/thatsaboutit.ogg","stream" )
	

			end
			
			
	
		
			elseif language=="de" then
			--self destructive message
			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/de/diskmessage/25thisdisk.ogg","stream" )

		elseif language=="fr" then
		--self destructive message
			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/fr/diskmessage/25thisdisk.ogg","stream" )
			
				
					--part 1
				
				youknow = love.audio.newSource( "/externalassets/dialogs/level2/fr/2youknow.ogg","stream" )
				letspushitdown = love.audio.newSource( "/externalassets/dialogs/level2/fr/3letspushitdown.ogg","stream" )
				wellletsgettowork = love.audio.newSource( "/externalassets/dialogs/level2/fr/4wellletsgettowork.ogg","stream" )

				
				--part 2

				sonowwecan = love.audio.newSource( "/externalassets/dialogs/level2/fr/6sonowwecan.ogg","stream" )
				thisbytheway = love.audio.newSource( "/externalassets/dialogs/level2/fr/8thisbytheway.ogg","stream" )
				goodidea = love.audio.newSource( "/externalassets/dialogs/level2/fr/10goodidea.ogg","stream" )
				before = love.audio.newSource( "/externalassets/dialogs/level2/fr/12before.ogg","stream" )
				illvolunteer = love.audio.newSource( "/externalassets/dialogs/level2/fr/14illvolunteer.ogg","stream" )
		
				wecanalsoswim = love.audio.newSource( "/externalassets/dialogs/level2/fr/wecanalsoswim.ogg","stream" )
				thatsaboutit = love.audio.newSource( "/externalassets/dialogs/level2/fr/thatsaboutit.ogg","stream" )


		
			elseif language=="es" then
		
			-- self destructive message
		
			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/es-la/diskmessage/25thisdisk.ogg","stream" )
			if accent=="es" then
				
		
			elseif accent=="la" then
				--part 1
				
				youknow = love.audio.newSource( "/externalassets/dialogs/level2/es-la/2youknow.ogg","stream" )
				letspushitdown = love.audio.newSource( "/externalassets/dialogs/level2/es-la/3letspushitdown.ogg","stream" )
				wellletsgettowork = love.audio.newSource( "/externalassets/dialogs/level2/es-la/4wellletsgettowork.ogg","stream" )

				
				--part 2

				sonowwecan = love.audio.newSource( "/externalassets/dialogs/level2/es-la/6sonowwecan.ogg","stream" )
				thisbytheway = love.audio.newSource( "/externalassets/dialogs/level2/es-la/8thisbytheway.ogg","stream" )
				goodidea = love.audio.newSource( "/externalassets/dialogs/level2/es-la/10goodidea.ogg","stream" )
				before = love.audio.newSource( "/externalassets/dialogs/level2/es-la/12before.ogg","stream" )
				illvolunteer = love.audio.newSource( "/externalassets/dialogs/level2/es-la/14illvolunteer.ogg","stream" )
				
				wecanalsoswim = love.audio.newSource( "/externalassets/dialogs/level2/es-la/wecanalsoswim.ogg","stream" )
				thatsaboutit = love.audio.newSource( "/externalassets/dialogs/level2/es-la/thatsaboutit.ogg","stream" )
		
			end


				
	
				
		elseif language=="sv" then
		
		-- self destructive message
		
			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/sv/diskmessage/25thisdisk.ogg","stream" )
		
		elseif language=="nl" then
				
				--part 1
				
				youknow = love.audio.newSource( "/externalassets/dialogs/level2/nl/2youknow.ogg","stream" )
				letspushitdown = love.audio.newSource( "/externalassets/dialogs/level2/nl/3letspushitdown.ogg","stream" )
				wellletsgettowork = love.audio.newSource( "/externalassets/dialogs/level2/nl/4wellletsgettowork.ogg","stream" )
						
				--part 2

				sonowwecan = love.audio.newSource( "/externalassets/dialogs/level2/nl/6sonowwecan.ogg","stream" )
				thisbytheway = love.audio.newSource( "/externalassets/dialogs/level2/nl/8thisbytheway.ogg","stream" )
				goodidea = love.audio.newSource( "/externalassets/dialogs/level2/nl/10goodidea.ogg","stream" )
				before = love.audio.newSource( "/externalassets/dialogs/level2/nl/12before.ogg","stream" )
				illvolunteer = love.audio.newSource( "/externalassets/dialogs/level2/nl/14illvolunteer.ogg","stream" )

				
				wecanalsoswim = love.audio.newSource( "/externalassets/dialogs/level2/nl/wecanalsoswim.ogg","stream" )
				thatsaboutit = love.audio.newSource( "/externalassets/dialogs/level2/nl/thatsaboutit.ogg","stream" )

				

		end
	
	
	if language2=="en" then 
		if accent2=="br" or language=="chi" then
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/en/1ohnonotagain.ogg","stream" )				
				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/en/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/en/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/en/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/en/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/en/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/en/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/en/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/en/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/en/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/en/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/en/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/en/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/en/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/en/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/en/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/en/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/en/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/en/27insummary.ogg","stream" )

			
		elseif accent2=="us" then
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/en-us/1ohnonotagain.ogg","stream" )

				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/en-us/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/en-us/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/en-us/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/en-us/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/en-us/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/en-us/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/en-us/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/en-us/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/en-us/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/en-us/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/en-us/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/en-us/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/en-us/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/en-us/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/en-us/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/en-us/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/en-us/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/en-us/27insummary.ogg","stream" )

			end
		
	elseif language2=="es"	then
			if accent2=="es" then
				
			elseif accent2=="la" then
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/es-la/1ohnonotagain.ogg","stream" )
				
				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/es-la/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/es-la/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/es-la/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/es-la/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/es-la/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/es-la/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/es-la/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/es-la/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/es-la/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/es-la/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/es-la/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/es-la/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/es-la/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/es-la/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/es-la/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/es-la/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/es-la/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/es-la/27insummary.ogg","stream" )

				
			end
			
	elseif language2=="pl" then
		
			-- self destructive message
		
			goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/1goodmorning.ogg","stream" )
			thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/2thisisanaffair.ogg","stream" )
			ourablest = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/3ourablest.ogg","stream" )
			agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/4agentsoffdto.ogg","stream" )
			managed = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/5managed.ogg","stream" )
			whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/6whichhascrashed.ogg","stream" )
			yourmission = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/7yourmission.ogg","stream" )
			willbe = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/8willbe.ogg","stream" )
			youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/9youshouldalso.ogg","stream" )
			wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/10wearemostly.ogg","stream" )
			thiscase = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/11thiscase.ogg","stream" )
			findout = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/12findout.ogg","stream" )
			oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/13oneofthelost.ogg","stream" )
			therearestill = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/14therearestill.ogg","stream" )
			mostofall = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/15mostofall.ogg","stream" )
			oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/16oneofyour.ogg","stream" )
			itcontains = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/17itcontains.ogg","stream" )
			youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/18youshouldalso.ogg","stream" )
			itescaped = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/19itescaped.ogg","stream" )
			itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/20itisnotarmed.ogg","stream" )
			wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/21wehavebeen.ogg","stream" )
			checkitout = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/22checkitout.ogg","stream" )
			andasusual = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/23andasusual.ogg","stream" )
			altar = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/24altar.ogg","stream" )
			thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/pl/diskmessage/25thisdisk.ogg","stream" )
		
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/pl/1ohnonotagain.ogg","stream" )
				
				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/pl/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/pl/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/pl/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/pl/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/pl/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/pl/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/pl/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/pl/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/pl/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/pl/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/pl/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/pl/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/pl/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/pl/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/pl/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/pl/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/pl/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/pl/27insummary.ogg","stream" )
		
	elseif language2=="cs" then
		
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/cs/1ohnonotagain.ogg","stream" )
				
				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/cs/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/cs/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/cs/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/cs/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/cs/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/cs/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/cs/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/cs/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/cs/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/cs/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/cs/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/cs/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/cs/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/cs/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/cs/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/cs/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/cs/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/cs/27insummary.ogg","stream" )
				
					
		elseif language2=="nl" then
		
					--part 1
		
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/nl/1ohnonotagain.ogg","stream" )				
		
					--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/nl/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/nl/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/nl/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/nl/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/nl/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/nl/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/nl/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/nl/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/nl/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/nl/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/nl/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/nl/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/nl/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/nl/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/nl/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/nl/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/nl/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/nl/27insummary.ogg","stream" )
		
				-- disk message
			
				goodmorning = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/1goodmorning.ogg","stream" )
				thisisanaffair = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/2thisisanaffair.ogg","stream" )
				ourablest = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/3ourablest.ogg","stream" )
				agentsoffdto = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/4agentsoffdto.ogg","stream" )
				managed = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/5managed.ogg","stream" )
				whichhascrashed = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/6whichhascrashed.ogg","stream" )
				yourmission = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/7yourmission.ogg","stream" )
				willbe = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/8willbe.ogg","stream" )
				youshouldalsotry = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/9youshouldalso.ogg","stream" )
				wearemostly = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/10wearemostly.ogg","stream" )
				thiscase = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/11thiscase.ogg","stream" )
				findout = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/12findout.ogg","stream" )
				oneofthelost = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/13oneofthelost.ogg","stream" )
				therearestill = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/14therearestill.ogg","stream" )
				mostofall = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/15mostofall.ogg","stream" )
				oneofyour = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/16oneofyour.ogg","stream" )
				itcontains = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/17itcontains.ogg","stream" )
				youshouldalso = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/18youshouldalso.ogg","stream" )
				itescaped = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/19itescaped.ogg","stream" )
				itisnotarmed = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/20itisnotarmed.ogg","stream" )
				wehavebeen = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/21wehavebeen.ogg","stream" )
				checkitout = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/22checkitout.ogg","stream" )
				andasusual = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/23andasusual.ogg","stream" )
				altar = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/24altar.ogg","stream" )
				thisdisk = love.audio.newSource( "/externalassets/dialogs/level2/nl/diskmessage/25thisdisk.ogg","stream" )
			
		elseif language2=="fr" then
	
				ohnonotagain = love.audio.newSource( "/externalassets/dialogs/level2/fr/1ohnonotagain.ogg","stream" )
				
				--part 2
				
				how = love.audio.newSource( "/externalassets/dialogs/level2/fr/5how.ogg","stream" )
				ourfirsttask = love.audio.newSource( "/externalassets/dialogs/level2/fr/7ourfirsttask.ogg","stream" )
				comeon = love.audio.newSource( "/externalassets/dialogs/level2/fr/9comeon.ogg","stream" )
				fornow = love.audio.newSource( "/externalassets/dialogs/level2/fr/11fornow.ogg","stream" )
				first = love.audio.newSource( "/externalassets/dialogs/level2/fr/13first.ogg","stream" )
				firstofall = love.audio.newSource( "/externalassets/dialogs/level2/fr/15firstofall.ogg","stream" )
				wealsocant = love.audio.newSource( "/externalassets/dialogs/level2/fr/16wealsocant.ogg","stream" )
				anotherthing = love.audio.newSource( "/externalassets/dialogs/level2/fr/17anotherthing.ogg","stream" )
				someobjects = love.audio.newSource( "/externalassets/dialogs/level2/fr/18someobjects.ogg","stream" )
				wealsocantdrop = love.audio.newSource( "/externalassets/dialogs/level2/fr/19wealsocantdrop.ogg","stream" )
				andunlike = love.audio.newSource( "/externalassets/dialogs/level2/fr/20andunlike.ogg","stream" )
				wecanalways = love.audio.newSource( "/externalassets/dialogs/level2/fr/21wecanalways.ogg","stream" )
				wecanpush = love.audio.newSource( "/externalassets/dialogs/level2/fr/22wecanpush.ogg","stream" )
				andeven = love.audio.newSource( "/externalassets/dialogs/level2/fr/23andeven.ogg","stream" )
				icaneven = love.audio.newSource( "/externalassets/dialogs/level2/fr/24icaneven.ogg","stream" )
				wecan = love.audio.newSource( "/externalassets/dialogs/level2/fr/25wecan.ogg","stream" )
				andwe = love.audio.newSource( "/externalassets/dialogs/level2/fr/26andwe.ogg","stream" )
				insummary = love.audio.newSource( "/externalassets/dialogs/level2/fr/27insummary.ogg","stream" )
		
		end		
	
				loadvoiceexceptions()				--exceptions to prevent game from crashing due to missing dubs


				--fish 1
				sonowwecan:setEffect('myEffect')
				thisbytheway:setEffect('myEffect')
				goodidea:setEffect('myEffect')
				before:setEffect('myEffect')
				illvolunteer:setEffect('myEffect')
				thatsaboutit:setEffect('myEffect')

				--fish 2
				--part 1
				ohnonotagain:setEffect('myEffect')
				
				--part 2
				how:setEffect('myEffect')
				ourfirsttask:setEffect('myEffect')
				comeon:setEffect('myEffect')
				fornow:setEffect('myEffect')
				first:setEffect('myEffect')
				
				firstofall:setEffect('myEffect')
				wealsocant:setEffect('myEffect')
				anotherthing:setEffect('myEffect')
				someobjects:setEffect('myEffect')
				wealsocantdrop:setEffect('myEffect')
				andunlike:setEffect('myEffect')
				wecanalways:setEffect('myEffect')
				wecanpush:setEffect('myEffect')
				andeven:setEffect('myEffect')
				icaneven:setEffect('myEffect')
				wecan:setEffect('myEffect')
				wecanalsoswim:setEffect('myEffect')
				andwe:setEffect('myEffect')
				insummary:setEffect('myEffect')
				thatsaboutit:setEffect('myEffect')
	
	elseif nLevel==3 then
		
		if language=="en" then
				if accent=="br" then
		
					imustnotswimdown = love.audio.newSource( "/externalassets/dialogs/level3/en/4imustnotswimdown.ogg","stream" )
					andicannotgo = love.audio.newSource( "/externalassets/dialogs/level3/en/5andicannot.ogg","stream" )
					heiarrange = love.audio.newSource( "/externalassets/dialogs/level3/en/6heiarrange.ogg","stream" )
					butbecareful = love.audio.newSource( "/externalassets/dialogs/level3/en/7butbecareful.ogg","stream" )
					
					thisistricky = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/1thisistricky.ogg","stream" )
					ifwepush = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/2ifwepush.ogg","stream" )
					thatsagood = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/4thatsagood.ogg","stream" )
					youmusnt = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/6youmusnt.ogg","stream" )
					
					ifyousolve = love.audio.newSource( "/externalassets/dialogs/level3/en/ifyousolve/1ifyousolve.ogg","stream" )
					remember = love.audio.newSource( "/externalassets/dialogs/level3/en/ifyousolve/3remember.ogg","stream" )
					
					
				elseif accent=="us" then
					imustnotswimdown = love.audio.newSource( "/externalassets/dialogs/level3/en-us/4imustnotswimdown.ogg","stream" )
					andicannotgo = love.audio.newSource( "/externalassets/dialogs/level3/en-us/5andicannot.ogg","stream" )
					heiarrange = love.audio.newSource( "/externalassets/dialogs/level3/en-us/6heiarrange.ogg","stream" )
					butbecareful = love.audio.newSource( "/externalassets/dialogs/level3/en-us/7butbecareful.ogg","stream" )
		
					thisistricky = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/1thisistricky.ogg","stream" )
					ifwepush = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/2ifwepush.ogg","stream" )
					thatsagood = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/4thatsagood.ogg","stream" )
					youmusnt = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/6youmusnt.ogg","stream" )
					
					ifyousolve = love.audio.newSource( "/externalassets/dialogs/level3/en-us/ifyousolve/1ifyousolve.ogg","stream" )
					remember = love.audio.newSource( "/externalassets/dialogs/level3/en-us/ifyousolve/3remember.ogg","stream" )
				end
			
		elseif language=="es" then
				if accent=="es" then
	
				elseif accent=="la" then
					
					imustnotswimdown = love.audio.newSource( "/externalassets/dialogs/level3/es-la/4imustnotswimdown.ogg","stream" )
					andicannotgo = love.audio.newSource( "/externalassets/dialogs/level3/es-la/5andicannot.ogg","stream" )
					heiarrange = love.audio.newSource( "/externalassets/dialogs/level3/es-la/6heiarrange.ogg","stream" )
					butbecareful = love.audio.newSource( "/externalassets/dialogs/level3/es-la/7butbecareful.ogg","stream" )
			
					thisistricky = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/1thisistricky.ogg","stream" )
					ifwepush = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/2ifwepush.ogg","stream" )
					thatsagood = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/4thatsagood.ogg","stream" )
					youmusnt = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/6youmusnt.ogg","stream" )
			
					ifyousolve = love.audio.newSource( "/externalassets/dialogs/level3/es-la/ifyousolve/1ifyousolve.ogg","stream" )
					remember = love.audio.newSource( "/externalassets/dialogs/level3/es-la/ifyousolve/3remember.ogg","stream" )
				end
			
			
		elseif language=="fr" then

					imustnotswimdown = love.audio.newSource( "/externalassets/dialogs/level3/fr/4imustnotswimdown.ogg","stream" )
					andicannotgo = love.audio.newSource( "/externalassets/dialogs/level3/fr/5andicannot.ogg","stream" )
					heiarrange = love.audio.newSource( "/externalassets/dialogs/level3/fr/6heiarrange.ogg","stream" )
					butbecareful = love.audio.newSource( "/externalassets/dialogs/level3/fr/7butbecareful.ogg","stream" )
		
					thisistricky = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/1thisistricky.ogg","stream" )
					ifwepush = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/2ifwepush.ogg","stream" )
					thatsagood = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/4thatsagood.ogg","stream" )
					youmusnt = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/6youmusnt.ogg","stream" )
			
					ifyousolve = love.audio.newSource( "/externalassets/dialogs/level3/fr/ifyousolve/1ifyousolve.ogg","stream" )
					remember = love.audio.newSource( "/externalassets/dialogs/level3/fr/ifyousolve/3remember.ogg","stream" )
	
	
		elseif language=="nl" then
					
					imustnotswimdown = love.audio.newSource( "/externalassets/dialogs/level3/nl/4imustnotswimdown.ogg","stream" )
					andicannotgo = love.audio.newSource( "/externalassets/dialogs/level3/nl/5andicannot.ogg","stream" )
					heiarrange = love.audio.newSource( "/externalassets/dialogs/level3/nl/6heiarrange.ogg","stream" )
					butbecareful = love.audio.newSource( "/externalassets/dialogs/level3/nl/7butbecareful.ogg","stream" )
		
					thisistricky = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/1thisistricky.ogg","stream" )
					ifwepush = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/2ifwepush.ogg","stream" )
					thatsagood = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/4thatsagood.ogg","stream" )
					youmusnt = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/6youmusnt.ogg","stream" )
			
					ifyousolve = love.audio.newSource( "/externalassets/dialogs/level3/nl/ifyousolve/1ifyousolve.ogg","stream" )
					remember = love.audio.newSource( "/externalassets/dialogs/level3/nl/ifyousolve/3remember.ogg","stream" )
	
		
		end
	
	if language2=="en" then
	
				if accent2=="br" then
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/en/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/en/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/en/3wewillgiveyousomehints.ogg","stream" )
					
					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/en/thisistricky/7ifyoucanpush.ogg","stream" )
					
					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/en/ifyousolve/2butonlyif.ogg","stream" )
					
				elseif accent2=="us" then
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/en-us/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/en-us/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/en-us/3wewillgiveyousomehints.ogg","stream" )

					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/en-us/thisistricky/7ifyoucanpush.ogg","stream" )
				
					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/en-us/ifyousolve/2butonlyif.ogg","stream" )
				
				end
	elseif language2=="es" then
			if accent2=="es" then
			
				elseif accent2=="la" then
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/es-la/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/es-la/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/es-la/3wewillgiveyousomehints.ogg","stream" )
					
					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/es-la/thisistricky/7ifyoucanpush.ogg","stream" )
				
					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/es-la/ifyousolve/2butonlyif.ogg","stream" )
				
				end
	elseif language2=="pl" then
			
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/pl/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/pl/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/pl/3wewillgiveyousomehints.ogg","stream" )

					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/pl/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/pl/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/pl/thisistricky/7ifyoucanpush.ogg","stream" )

					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/pl/ifyousolve/2butonlyif.ogg","stream" )
	
	elseif language2=="cs" then
			
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/cs/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/cs/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/cs/3wewillgiveyousomehints.ogg","stream" )

					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/cs/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/cs/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/cs/thisistricky/7ifyoucanpush.ogg","stream" )

					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/cs/ifyousolve/2butonlyif.ogg","stream" )

	elseif language2=="nl" then
					
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/nl/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/nl/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/nl/3wewillgiveyousomehints.ogg","stream" )
					
					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistricky/7ifyoucanpush.ogg","stream" )
	
					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/nl/ifyousolve/2butonlyif.ogg","stream" )
	
	elseif language2=="fr" then
					youknowtherules = love.audio.newSource( "/externalassets/dialogs/level3/fr/1youknowtherules.ogg","stream" )
					nowyoucanpractice = love.audio.newSource( "/externalassets/dialogs/level3/fr/2nowyoucanpractice.ogg","stream" )
					wewillgiveyou = love.audio.newSource( "/externalassets/dialogs/level3/fr/3wewillgiveyousomehints.ogg","stream" )
					
					couldntwe = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/3couldntwe.ogg","stream" )
					nowicannot = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/5nowicannot.ogg","stream" )
					ifyoucanpush = love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistricky/7ifyoucanpush.ogg","stream" )

					butonlyif = love.audio.newSource( "/externalassets/dialogs/level3/fr/ifyousolve/2butonlyif.ogg","stream" )

	end						
	
					loadvoiceexceptions()				--exceptions to prevent game from crashing due to missing dubs
					
					--fish 1
					imustnotswimdown:setEffect('myEffect')
					andicannotgo:setEffect('myEffect')
					heiarrange:setEffect('myEffect')
					butbecareful:setEffect('myEffect')
					
					thisistricky:setEffect('myEffect')
					ifwepush:setEffect('myEffect')
					thatsagood:setEffect('myEffect')
					youmusnt:setEffect('myEffect')
					
					
					--fish 2
					youknowtherules:setEffect('myEffect')
					nowyoucanpractice:setEffect('myEffect')
					wewillgiveyou:setEffect('myEffect')
					
					--axe
					couldntwe:setEffect('myEffect')
					nowicannot:setEffect('myEffect')
					ifyoucanpush:setEffect('myEffect')
					
					--saving positions
					ifyousolve:setEffect('myEffect')
					butonlyif:setEffect('myEffect')
					remember:setEffect('myEffect')
					
	elseif nLevel==4 then
	
		if language=="en" then
				if accent=="br" then
				
					these = love.audio.newSource( "/externalassets/dialogs/level4/en/1these.ogg","stream" )
					thesesunken = love.audio.newSource( "/externalassets/dialogs/level4/en/2thesesunken.ogg","stream" )
					ohmy = love.audio.newSource( "/externalassets/dialogs/level4/en/3ohmy.ogg","stream" )
					throwout = love.audio.newSource( "/externalassets/dialogs/level4/en/17throwout.ogg","stream" )
					thethree = love.audio.newSource( "/externalassets/dialogs/level4/en/18thethree.ogg","stream" )
					thegingerbread = love.audio.newSource( "/externalassets/dialogs/level4/en/19thegingerbread.ogg","stream" )
					goldilocks = love.audio.newSource( "/externalassets/dialogs/level4/en/20goldilocks.ogg","stream" )
					sharkerella = love.audio.newSource( "/externalassets/dialogs/level4/en/21sharkerella.ogg","stream" )
					slimmy = love.audio.newSource( "/externalassets/dialogs/level4/en/22slimmy.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level4/en/24icant.ogg","stream" )
					icantfit = love.audio.newSource( "/externalassets/dialogs/level4/en/25icantfit.ogg","stream" )
					letsthink = love.audio.newSource( "/externalassets/dialogs/level4/en/30letsthink.ogg","stream" )

					
				elseif accent=="us" then
					these = love.audio.newSource( "/externalassets/dialogs/level4/en-us/1these.ogg","stream" )
					thesesunken = love.audio.newSource( "/externalassets/dialogs/level4/en-us/2thesesunken.ogg","stream" )
					ohmy = love.audio.newSource( "/externalassets/dialogs/level4/en-us/3ohmy.ogg","stream" )
					throwout = love.audio.newSource( "/externalassets/dialogs/level4/en-us/17throwout.ogg","stream" )
					thethree = love.audio.newSource( "/externalassets/dialogs/level4/en-us/18thethree.ogg","stream" )
					thegingerbread = love.audio.newSource( "/externalassets/dialogs/level4/en-us/19thegingerbread.ogg","stream" )
					goldilocks = love.audio.newSource( "/externalassets/dialogs/level4/en-us/20goldilocks.ogg","stream" )
					sharkerella = love.audio.newSource( "/externalassets/dialogs/level4/en-us/21sharkerella.ogg","stream" )
					slimmy = love.audio.newSource( "/externalassets/dialogs/level4/en-us/22slimmy.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level4/en-us/24icant.ogg","stream" )
					icantfit = love.audio.newSource( "/externalassets/dialogs/level4/en-us/25icantfit.ogg","stream" )
					letsthink = love.audio.newSource( "/externalassets/dialogs/level4/en-us/30letsthink.ogg","stream" )

				end
			
		elseif language=="fr" then
					these = love.audio.newSource( "/externalassets/dialogs/level4/fr/1these.ogg","stream" )
					thesesunken = love.audio.newSource( "/externalassets/dialogs/level4/fr/2thesesunken.ogg","stream" )
					ohmy = love.audio.newSource( "/externalassets/dialogs/level4/fr/3ohmy.ogg","stream" )
					throwout = love.audio.newSource( "/externalassets/dialogs/level4/fr/17throwout.ogg","stream" )
					thethree = love.audio.newSource( "/externalassets/dialogs/level4/fr/18thethree.ogg","stream" )
					thegingerbread = love.audio.newSource( "/externalassets/dialogs/level4/fr/19thegingerbread.ogg","stream" )
					goldilocks = love.audio.newSource( "/externalassets/dialogs/level4/fr/20goldilocks.ogg","stream" )
					sharkerella = love.audio.newSource( "/externalassets/dialogs/level4/fr/21sharkerella.ogg","stream" )
					slimmy = love.audio.newSource( "/externalassets/dialogs/level4/fr/22slimmy.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level4/fr/24icant.ogg","stream" )
					icantfit = love.audio.newSource( "/externalassets/dialogs/level4/fr/25icantfit.ogg","stream" )
					letsthink = love.audio.newSource( "/externalassets/dialogs/level4/fr/30letsthink.ogg","stream" )

			
		elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
					
					these = love.audio.newSource( "/externalassets/dialogs/level4/es-la/1these.ogg","stream" )
					thesesunken = love.audio.newSource( "/externalassets/dialogs/level4/es-la/2thesesunken.ogg","stream" )
					ohmy = love.audio.newSource( "/externalassets/dialogs/level4/es-la/3ohmy.ogg","stream" )
					throwout = love.audio.newSource( "/externalassets/dialogs/level4/es-la/17throwout.ogg","stream" )
					thethree = love.audio.newSource( "/externalassets/dialogs/level4/es-la/18thethree.ogg","stream" )
					thegingerbread = love.audio.newSource( "/externalassets/dialogs/level4/es-la/19thegingerbread.ogg","stream" )
					goldilocks = love.audio.newSource( "/externalassets/dialogs/level4/es-la/20goldilocks.ogg","stream" )
					sharkerella = love.audio.newSource( "/externalassets/dialogs/level4/es-la/21sharkerella.ogg","stream" )
					slimmy = love.audio.newSource( "/externalassets/dialogs/level4/es-la/22slimmy.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level4/es-la/24icant.ogg","stream" )
					icantfit = love.audio.newSource( "/externalassets/dialogs/level4/es-la/25icantfit.ogg","stream" )
					letsthink = love.audio.newSource( "/externalassets/dialogs/level4/es-la/30letsthink.ogg","stream" )

					
				end
		elseif language=="nl" then
					these = love.audio.newSource( "/externalassets/dialogs/level4/nl/1these.ogg","stream" )
					thesesunken = love.audio.newSource( "/externalassets/dialogs/level4/nl/2thesesunken.ogg","stream" )
					ohmy = love.audio.newSource( "/externalassets/dialogs/level4/nl/3ohmy.ogg","stream" )
					throwout = love.audio.newSource( "/externalassets/dialogs/level4/nl/17throwout.ogg","stream" )
					thethree = love.audio.newSource( "/externalassets/dialogs/level4/nl/18thethree.ogg","stream" )
					thegingerbread = love.audio.newSource( "/externalassets/dialogs/level4/nl/19thegingerbread.ogg","stream" )
					goldilocks = love.audio.newSource( "/externalassets/dialogs/level4/nl/20goldilocks.ogg","stream" )
					sharkerella = love.audio.newSource( "/externalassets/dialogs/level4/nl/21sharkerella.ogg","stream" )
					slimmy = love.audio.newSource( "/externalassets/dialogs/level4/nl/22slimmy.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level4/nl/24icant.ogg","stream" )
					icantfit = love.audio.newSource( "/externalassets/dialogs/level4/nl/25icantfit.ogg","stream" )
					letsthink = love.audio.newSource( "/externalassets/dialogs/level4/nl/30letsthink.ogg","stream" )

		end
		
		if language2=="en" then
					if accent2=="br" then
							look1 = love.audio.newSource( "/externalassets/dialogs/level4/en/4look.ogg","stream" )
							look2 = love.audio.newSource( "/externalassets/dialogs/level4/en/5look2.ogg","stream" )
							look3 = love.audio.newSource( "/externalassets/dialogs/level4/en/6look3.ogg","stream" )
							see = love.audio.newSource( "/externalassets/dialogs/level4/en/7see.ogg","stream" )
							youbuy = love.audio.newSource( "/externalassets/dialogs/level4/en/8youbuy.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/en/9why.ogg","stream" )
							esp = love.audio.newSource( "/externalassets/dialogs/level4/en/10esp.ogg","stream" )
							life = love.audio.newSource( "/externalassets/dialogs/level4/en/11life.ogg","stream" )
							memories = love.audio.newSource( "/externalassets/dialogs/level4/en/12memories.ogg","stream" )
							holistic = love.audio.newSource( "/externalassets/dialogs/level4/en/13holistic.ogg","stream" )
							teach = love.audio.newSource( "/externalassets/dialogs/level4/en/14teach.ogg","stream" )
							unwilling = love.audio.newSource( "/externalassets/dialogs/level4/en/15unwilling.ogg","stream" )
							outreaching = love.audio.newSource( "/externalassets/dialogs/level4/en/16outreaching.ogg","stream" )
							never = love.audio.newSource( "/externalassets/dialogs/level4/en/23never.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/en/26why.ogg","stream" )
							ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/en/27ifyoudid.ogg","stream" )
							itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/en/28itoldyou.ogg","stream" )
							there = love.audio.newSource( "/externalassets/dialogs/level4/en/29there.ogg","stream" )
				
					
					elseif accent2=="us" then
							look1 = love.audio.newSource( "/externalassets/dialogs/level4/en-us/4look.ogg","stream" )
							look2 = love.audio.newSource( "/externalassets/dialogs/level4/en-us/5look2.ogg","stream" )
							look3 = love.audio.newSource( "/externalassets/dialogs/level4/en-us/6look3.ogg","stream" )
							see = love.audio.newSource( "/externalassets/dialogs/level4/en-us/7see.ogg","stream" )
							youbuy = love.audio.newSource( "/externalassets/dialogs/level4/en-us/8youbuy.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/en-us/9why.ogg","stream" )
							esp = love.audio.newSource( "/externalassets/dialogs/level4/en-us/10esp.ogg","stream" )
							life = love.audio.newSource( "/externalassets/dialogs/level4/en-us/11life.ogg","stream" )
							memories = love.audio.newSource( "/externalassets/dialogs/level4/en-us/12memories.ogg","stream" )
							holistic = love.audio.newSource( "/externalassets/dialogs/level4/en-us/13holistic.ogg","stream" )
							teach = love.audio.newSource( "/externalassets/dialogs/level4/en-us/14teach.ogg","stream" )
							unwilling = love.audio.newSource( "/externalassets/dialogs/level4/en-us/15unwilling.ogg","stream" )
							outreaching = love.audio.newSource( "/externalassets/dialogs/level4/en-us/16outreaching.ogg","stream" )
							never = love.audio.newSource( "/externalassets/dialogs/level4/en-us/23never.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/en-us/26why.ogg","stream" )
							ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/en-us/27ifyoudid.ogg","stream" )
							itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/en-us/28itoldyou.ogg","stream" )
							there = love.audio.newSource( "/externalassets/dialogs/level4/en-us/29there.ogg","stream" )
	
					
					
					end
		elseif language2=="es" then
						if accent2=="es" then
					elseif accent2=="la" then
						look1 = love.audio.newSource( "/externalassets/dialogs/level4/es-la/4look.ogg","stream" )
						look2 = love.audio.newSource( "/externalassets/dialogs/level4/es-la/5look2.ogg","stream" )
						look3 = love.audio.newSource( "/externalassets/dialogs/level4/es-la/6look3.ogg","stream" )
						see = love.audio.newSource( "/externalassets/dialogs/level4/es-la/7see.ogg","stream" )
						youbuy = love.audio.newSource( "/externalassets/dialogs/level4/es-la/8youbuy.ogg","stream" )
						why = love.audio.newSource( "/externalassets/dialogs/level4/es-la/9why.ogg","stream" )
						esp = love.audio.newSource( "/externalassets/dialogs/level4/es-la/10esp.ogg","stream" )
						life = love.audio.newSource( "/externalassets/dialogs/level4/es-la/11life.ogg","stream" )
						memories = love.audio.newSource( "/externalassets/dialogs/level4/es-la/12memories.ogg","stream" )
						holistic = love.audio.newSource( "/externalassets/dialogs/level4/es-la/13holistic.ogg","stream" )
						teach = love.audio.newSource( "/externalassets/dialogs/level4/es-la/14teach.ogg","stream" )
						unwilling = love.audio.newSource( "/externalassets/dialogs/level4/es-la/15unwilling.ogg","stream" )
						outreaching = love.audio.newSource( "/externalassets/dialogs/level4/es-la/16outreaching.ogg","stream" )
						never = love.audio.newSource( "/externalassets/dialogs/level4/es-la/23never.ogg","stream" )
						why = love.audio.newSource( "/externalassets/dialogs/level4/es-la/26why.ogg","stream" )
						ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/es-la/27ifyoudid.ogg","stream" )
						itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/es-la/28itoldyou.ogg","stream" )
						there = love.audio.newSource( "/externalassets/dialogs/level4/es-la/29there.ogg","stream" )
					end
		
			
		elseif language2=="pl" then
					look1 = love.audio.newSource( "/externalassets/dialogs/level4/pl/4look.ogg","stream" )
					look2 = love.audio.newSource( "/externalassets/dialogs/level4/pl/5look2.ogg","stream" )
					look3 = love.audio.newSource( "/externalassets/dialogs/level4/pl/6look3.ogg","stream" )
					see = love.audio.newSource( "/externalassets/dialogs/level4/pl/7see.ogg","stream" )
					youbuy = love.audio.newSource( "/externalassets/dialogs/level4/pl/8youbuy.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level4/pl/9why.ogg","stream" )
					esp = love.audio.newSource( "/externalassets/dialogs/level4/pl/10esp.ogg","stream" )
					life = love.audio.newSource( "/externalassets/dialogs/level4/pl/11life.ogg","stream" )
					memories = love.audio.newSource( "/externalassets/dialogs/level4/pl/12memories.ogg","stream" )
					holistic = love.audio.newSource( "/externalassets/dialogs/level4/pl/13holistic.ogg","stream" )
					teach = love.audio.newSource( "/externalassets/dialogs/level4/pl/14teach.ogg","stream" )
					unwilling = love.audio.newSource( "/externalassets/dialogs/level4/pl/15unwilling.ogg","stream" )
					outreaching = love.audio.newSource( "/externalassets/dialogs/level4/pl/16outreaching.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level4/pl/23never.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level4/pl/26why.ogg","stream" )
					ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/pl/27ifyoudid.ogg","stream" )
					itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/pl/28itoldyou.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level4/pl/29there.ogg","stream" )
		
		elseif language2=="cs" then
					look1 = love.audio.newSource( "/externalassets/dialogs/level4/cs/4look.ogg","stream" )
					look2 = love.audio.newSource( "/externalassets/dialogs/level4/cs/5look2.ogg","stream" )
					look3 = love.audio.newSource( "/externalassets/dialogs/level4/cs/6look3.ogg","stream" )
					see = love.audio.newSource( "/externalassets/dialogs/level4/cs/7see.ogg","stream" )
					youbuy = love.audio.newSource( "/externalassets/dialogs/level4/cs/8youbuy.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level4/cs/9why.ogg","stream" )
					esp = love.audio.newSource( "/externalassets/dialogs/level4/cs/10esp.ogg","stream" )
					life = love.audio.newSource( "/externalassets/dialogs/level4/cs/11life.ogg","stream" )
					memories = love.audio.newSource( "/externalassets/dialogs/level4/cs/12memories.ogg","stream" )
					holistic = love.audio.newSource( "/externalassets/dialogs/level4/cs/13holistic.ogg","stream" )
					teach = love.audio.newSource( "/externalassets/dialogs/level4/cs/14teach.ogg","stream" )
					unwilling = love.audio.newSource( "/externalassets/dialogs/level4/cs/15unwilling.ogg","stream" )
					outreaching = love.audio.newSource( "/externalassets/dialogs/level4/cs/16outreaching.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level4/cs/23never.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level4/cs/26why.ogg","stream" )
					ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/cs/27ifyoudid.ogg","stream" )
					itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/cs/28itoldyou.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level4/cs/29there.ogg","stream" )
										
		elseif language2=="nl" then
							look1 = love.audio.newSource( "/externalassets/dialogs/level4/nl/4look.ogg","stream" )
							look2 = love.audio.newSource( "/externalassets/dialogs/level4/nl/5look2.ogg","stream" )
							look3 = love.audio.newSource( "/externalassets/dialogs/level4/nl/6look3.ogg","stream" )
							see = love.audio.newSource( "/externalassets/dialogs/level4/nl/7see.ogg","stream" )
							youbuy = love.audio.newSource( "/externalassets/dialogs/level4/nl/8youbuy.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/nl/9why.ogg","stream" )
							esp = love.audio.newSource( "/externalassets/dialogs/level4/nl/10esp.ogg","stream" )
							life = love.audio.newSource( "/externalassets/dialogs/level4/nl/11life.ogg","stream" )
							memories = love.audio.newSource( "/externalassets/dialogs/level4/nl/12memories.ogg","stream" )
							holistic = love.audio.newSource( "/externalassets/dialogs/level4/nl/13holistic.ogg","stream" )
							teach = love.audio.newSource( "/externalassets/dialogs/level4/nl/14teach.ogg","stream" )
							unwilling = love.audio.newSource( "/externalassets/dialogs/level4/nl/15unwilling.ogg","stream" )
							outreaching = love.audio.newSource( "/externalassets/dialogs/level4/nl/16outreaching.ogg","stream" )
							never = love.audio.newSource( "/externalassets/dialogs/level4/nl/23never.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/nl/26why.ogg","stream" )
							ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/nl/27ifyoudid.ogg","stream" )
							itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/nl/28itoldyou.ogg","stream" )
							there = love.audio.newSource( "/externalassets/dialogs/level4/nl/29there.ogg","stream" )
							
		elseif language2=="fr" then
							look1 = love.audio.newSource( "/externalassets/dialogs/level4/fr/4look.ogg","stream" )
							look2 = love.audio.newSource( "/externalassets/dialogs/level4/fr/5look2.ogg","stream" )
							look3 = love.audio.newSource( "/externalassets/dialogs/level4/fr/6look3.ogg","stream" )
							see = love.audio.newSource( "/externalassets/dialogs/level4/fr/7see.ogg","stream" )
							youbuy = love.audio.newSource( "/externalassets/dialogs/level4/fr/8youbuy.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/fr/9why.ogg","stream" )
							esp = love.audio.newSource( "/externalassets/dialogs/level4/fr/10esp.ogg","stream" )
							life = love.audio.newSource( "/externalassets/dialogs/level4/fr/11life.ogg","stream" )
							memories = love.audio.newSource( "/externalassets/dialogs/level4/fr/12memories.ogg","stream" )
							holistic = love.audio.newSource( "/externalassets/dialogs/level4/fr/13holistic.ogg","stream" )
							teach = love.audio.newSource( "/externalassets/dialogs/level4/fr/14teach.ogg","stream" )
							unwilling = love.audio.newSource( "/externalassets/dialogs/level4/fr/15unwilling.ogg","stream" )
							outreaching = love.audio.newSource( "/externalassets/dialogs/level4/fr/16outreaching.ogg","stream" )
							never = love.audio.newSource( "/externalassets/dialogs/level4/fr/23never.ogg","stream" )
							why = love.audio.newSource( "/externalassets/dialogs/level4/fr/26why.ogg","stream" )
							ifyoudid = love.audio.newSource( "/externalassets/dialogs/level4/fr/27ifyoudid.ogg","stream" )
							itoldyou = love.audio.newSource( "/externalassets/dialogs/level4/fr/28itoldyou.ogg","stream" )
							there = love.audio.newSource( "/externalassets/dialogs/level4/fr/29there.ogg","stream" )
			end
			
			loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
			
					--fish 2
					look1:setEffect('myEffect')
					look2:setEffect('myEffect')
					look3:setEffect('myEffect')
					see:setEffect('myEffect')
					youbuy:setEffect('myEffect')
					why:setEffect('myEffect')
					esp:setEffect('myEffect')
					life:setEffect('myEffect')
					memories:setEffect('myEffect')
					holistic:setEffect('myEffect')
					teach:setEffect('myEffect')
					unwilling:setEffect('myEffect')
					outreaching:setEffect('myEffect')
					never:setEffect('myEffect')
					why:setEffect('myEffect')
					ifyoudid:setEffect('myEffect')
					itoldyou:setEffect('myEffect')
					there:setEffect('myEffect')
		
					--fish 1
					these:setEffect('myEffect')
					thesesunken:setEffect('myEffect')
					ohmy:setEffect('myEffect')
					throwout:setEffect('myEffect')
					thethree:setEffect('myEffect')
					thegingerbread:setEffect('myEffect')
					goldilocks:setEffect('myEffect')
					sharkerella:setEffect('myEffect')
					slimmy:setEffect('myEffect')
					icant:setEffect('myEffect')
					icantfit:setEffect('myEffect')
		

	elseif nLevel==5 then
	
	if language=="en" then
				if accent=="br" then
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/en/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/en/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/en/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/en/5thats.ogg","stream" )
				
				elseif accent=="us" then
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/5thats.ogg","stream" )
			
				end
			
			elseif language=="fr" then
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/fr/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/fr/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/fr/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/fr/5thats.ogg","stream" )
		
			
		
			elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/8where.ogg","stream" )
				end
			
			elseif language=="nl" then
			
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/nl/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/nl/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/nl/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/nl/5thats.ogg","stream" )
			end
			
			
			if language2=="en" then
				if accent2=="br" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/en/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/en/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/en/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/en/8where.ogg","stream" )
				
				elseif accent2=="us" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/en-us/8where.ogg","stream" )
			
				end
			elseif language2=="pl" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/pl/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/pl/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/pl/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/pl/8where.ogg","stream" )
	
			
			elseif language2=="es" then
				if accent2=="es" then
				elseif accent2=="la" then
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/es-la/5thats.ogg","stream" )
					
		
				end
			elseif language2=="nl" then
					--english voices: roles inverted, this is a temporary solution till the problem is solved
					something  = love.audio.newSource( "/externalassets/dialogs/level5/en/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/en/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/en/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/en/8where.ogg","stream" )
			
					itsome  = love.audio.newSource( "/externalassets/dialogs/level5/nl/2itssome.ogg","stream" )
					weprobably  = love.audio.newSource( "/externalassets/dialogs/level5/nl/3weprobably.ogg","stream" )
					ican  = love.audio.newSource( "/externalassets/dialogs/level5/nl/4ican.ogg","stream" )
					thats  = love.audio.newSource( "/externalassets/dialogs/level5/nl/5thats.ogg","stream" )
			elseif language2=="cs" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/cs/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/cs/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/cs/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/cs/8where.ogg","stream" )
			
			elseif language2=="fr" then
					something  = love.audio.newSource( "/externalassets/dialogs/level5/fr/1something.ogg","stream" )
					imsorry  = love.audio.newSource( "/externalassets/dialogs/level5/fr/6imsorry.ogg","stream" )
					sorry  = love.audio.newSource( "/externalassets/dialogs/level5/fr/7sorry.ogg","stream" )
					where  = love.audio.newSource( "/externalassets/dialogs/level5/fr/8where.ogg","stream" )

			end
			
			loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
			
					--fish 2	
					something:setEffect('myEffect')
					imsorry:setEffect('myEffect')
					sorry:setEffect('myEffect')
					where:setEffect('myEffect')
			
					--fish 1
					itsome:setEffect('myEffect')
					weprobably:setEffect('myEffect')
					ican:setEffect('myEffect')
					thats:setEffect('myEffect')
	
	elseif nLevel==6 then
			
			if language=="en" then
				if accent=="br" then
					atleast  = love.audio.newSource( "/externalassets/dialogs/level6/en/4atleast.ogg","stream" )
					atleast2  = love.audio.newSource( "/externalassets/dialogs/level6/en/5atleast2.ogg","stream" )
					illhave  = love.audio.newSource( "/externalassets/dialogs/level6/en/6illhave.ogg","stream" )
					itsagood  = love.audio.newSource( "/externalassets/dialogs/level6/en/7itsagood.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level6/en/8couldyou.ogg","stream" )
					couldyou2  = love.audio.newSource( "/externalassets/dialogs/level6/en/9couldyou2.ogg","stream" )
		
				elseif accent=="us" then
					atleast  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/4atleast.ogg","stream" )
					atleast2  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/5atleast2.ogg","stream" )
					illhave  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/6illhave.ogg","stream" )
					itsagood  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/7itsagood.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/8couldyou.ogg","stream" )
					couldyou2  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/9couldyou2.ogg","stream" )

				end
				
			elseif language=="es" then
				if accent=="es" then
				
				elseif accent=="la" then
					atleast  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/4atleast.ogg","stream" )
					atleast2  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/5atleast2.ogg","stream" )
					illhave  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/6illhave.ogg","stream" )
					itsagood  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/7itsagood.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/8couldyou.ogg","stream" )
					couldyou2  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/9couldyou2.ogg","stream" )
				
				end
		
			
			elseif language=="fr" then
					atleast  = love.audio.newSource( "/externalassets/dialogs/level6/fr/4atleast.ogg","stream" )
					atleast2  = love.audio.newSource( "/externalassets/dialogs/level6/fr/5atleast2.ogg","stream" )
					illhave  = love.audio.newSource( "/externalassets/dialogs/level6/fr/6illhave.ogg","stream" )
					itsagood  = love.audio.newSource( "/externalassets/dialogs/level6/fr/7itsagood.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level6/fr/8couldyou.ogg","stream" )
					couldyou2  = love.audio.newSource( "/externalassets/dialogs/level6/fr/9couldyou2.ogg","stream" )
		
		elseif language=="nl" then
					atleast  = love.audio.newSource( "/externalassets/dialogs/level6/nl/4atleast.ogg","stream" )
					atleast2  = love.audio.newSource( "/externalassets/dialogs/level6/nl/5atleast2.ogg","stream" )
					illhave  = love.audio.newSource( "/externalassets/dialogs/level6/nl/6illhave.ogg","stream" )
					itsagood  = love.audio.newSource( "/externalassets/dialogs/level6/nl/7itsagood.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level6/nl/8couldyou.ogg","stream" )
					couldyou2  = love.audio.newSource( "/externalassets/dialogs/level6/nl/9couldyou2.ogg","stream" )
			end
	
		if language2=="en" then
				if accent2=="br" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/en/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/en/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/en/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/en/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/en/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/en/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/en/13forget.ogg","stream" )
	
					
				elseif accent2=="us" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/en-us/13forget.ogg","stream" )
		
				end
				
		elseif language2=="es" then
				if accent2=="es" then
				elseif accent2=="la" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/es-la/13forget.ogg","stream" )
		
				end
		elseif language2=="pl" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/pl/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/pl/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/pl/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/pl/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/pl/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/pl/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/pl/13forget.ogg","stream" )
					
		elseif language2=="cs" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/cs/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/cs/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/cs/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/cs/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/cs/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/cs/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/cs/13forget.ogg","stream" )
					
		elseif language2=="nl" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/nl/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/nl/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/nl/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/nl/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/nl/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/nl/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/nl/13forget.ogg","stream" )
		elseif language2=="fr" then
					itsbeen  = love.audio.newSource( "/externalassets/dialogs/level6/fr/1itsbeen.ogg","stream" )
					nobody  = love.audio.newSource( "/externalassets/dialogs/level6/fr/2nobody.ogg","stream" )
					iknew  = love.audio.newSource( "/externalassets/dialogs/level6/fr/3iknew.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level6/fr/10ithink.ogg","stream" )
					itshould  = love.audio.newSource( "/externalassets/dialogs/level6/fr/11itshould.ogg","stream" )
					justsweep  = love.audio.newSource( "/externalassets/dialogs/level6/fr/12justsweep.ogg","stream" )
					forget  = love.audio.newSource( "/externalassets/dialogs/level6/fr/13forget.ogg","stream" )
		end
					
					loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
					
					--fish 1
								
					atleast:setEffect('myEffect')
					atleast2:setEffect('myEffect')
					illhave:setEffect('myEffect')
					itsagood:setEffect('myEffect')
					couldyou:setEffect('myEffect')
					couldyou2:setEffect('myEffect')
					--fish 2
					itsbeen:setEffect('myEffect')
					nobody:setEffect('myEffect')
					iknew:setEffect('myEffect')
					ithink:setEffect('myEffect')
					itshould:setEffect('myEffect')
					justsweep:setEffect('myEffect')
					forget:setEffect('myEffect')
					
	elseif nLevel==7 then
		
		
		if language=="en" then
			if accent=="br" then
				icant = love.audio.newSource( "/externalassets/dialogs/level7/en/2icant.ogg","stream" )
				thisisnot = love.audio.newSource( "/externalassets/dialogs/level7/en/3thisisnot.ogg","stream" )
				iwont = love.audio.newSource( "/externalassets/dialogs/level7/en/4iwont.ogg","stream" )
				maybewehavetoswitch = love.audio.newSource( "/externalassets/dialogs/level7/en/6maybewehavetoswitch.ogg","stream" )
				finallyitsthere = love.audio.newSource( "/externalassets/dialogs/level7/en/10finallyitsthere.ogg","stream" )
				weshouldsearchthecoral = love.audio.newSource( "/externalassets/dialogs/level7/en/12weshouldsearchthecoral.ogg","stream" )
				dontweneedamicroscope = love.audio.newSource( "/externalassets/dialogs/level7/en/14dontweneedamicroscope.ogg","stream" )
				coralturtles = love.audio.newSource( "/externalassets/dialogs/level7/en/16coralturtles.ogg","stream" )

			elseif accent=="us" then
				icant = love.audio.newSource( "/externalassets/dialogs/level7/en-us/2icant.ogg","stream" )
				thisisnot = love.audio.newSource( "/externalassets/dialogs/level7/en-us/3thisisnot.ogg","stream" )
				iwont = love.audio.newSource( "/externalassets/dialogs/level7/en-us/4iwont.ogg","stream" )
				maybewehavetoswitch = love.audio.newSource( "/externalassets/dialogs/level7/en-us/6maybewehavetoswitch.ogg","stream" )
				finallyitsthere = love.audio.newSource( "/externalassets/dialogs/level7/en-us/10finallyitsthere.ogg","stream" )
				weshouldsearchthecoral = love.audio.newSource( "/externalassets/dialogs/level7/en-us/12weshouldsearchthecoral.ogg","stream" )
				dontweneedamicroscope = love.audio.newSource( "/externalassets/dialogs/level7/en-us/14dontweneedamicroscope.ogg","stream" )
				coralturtles = love.audio.newSource( "/externalassets/dialogs/level7/en-us/16coralturtles.ogg","stream" )

			end
			
		elseif language=="es" then
			if accent=="es" then
			elseif accent=="la" then
				icant = love.audio.newSource( "/externalassets/dialogs/level7/es-la/2icant.ogg","stream" )
				thisisnot = love.audio.newSource( "/externalassets/dialogs/level7/es-la/3thisisnot.ogg","stream" )
				iwont = love.audio.newSource( "/externalassets/dialogs/level7/es-la/4iwont.ogg","stream" )
				maybewehavetoswitch = love.audio.newSource( "/externalassets/dialogs/level7/es-la/6maybewehavetoswitch.ogg","stream" )
				finallyitsthere = love.audio.newSource( "/externalassets/dialogs/level7/es-la/10finallyitsthere.ogg","stream" )
				weshouldsearchthecoral = love.audio.newSource( "/externalassets/dialogs/level7/es-la/12weshouldsearchthecoral.ogg","stream" )
				dontweneedamicroscope = love.audio.newSource( "/externalassets/dialogs/level7/es-la/14dontweneedamicroscope.ogg","stream" )
				coralturtles = love.audio.newSource( "/externalassets/dialogs/level7/es-la/16coralturtles.ogg","stream" )
			end
				
		elseif language=="fr" then
				icant = love.audio.newSource( "/externalassets/dialogs/level7/fr/2icant.ogg","stream" )
				thisisnot = love.audio.newSource( "/externalassets/dialogs/level7/fr/3thisisnot.ogg","stream" )
				iwont = love.audio.newSource( "/externalassets/dialogs/level7/fr/4iwont.ogg","stream" )
				maybewehavetoswitch = love.audio.newSource( "/externalassets/dialogs/level7/fr/6maybewehavetoswitch.ogg","stream" )
				finallyitsthere = love.audio.newSource( "/externalassets/dialogs/level7/fr/10finallyitsthere.ogg","stream" )
				weshouldsearchthecoral = love.audio.newSource( "/externalassets/dialogs/level7/fr/12weshouldsearchthecoral.ogg","stream" )
				dontweneedamicroscope = love.audio.newSource( "/externalassets/dialogs/level7/fr/14dontweneedamicroscope.ogg","stream" )
				coralturtles = love.audio.newSource( "/externalassets/dialogs/level7/fr/16coralturtles.ogg","stream" )
	
		elseif language=="nl" then
				icant = love.audio.newSource( "/externalassets/dialogs/level7/nl/2icant.ogg","stream" )
				thisisnot = love.audio.newSource( "/externalassets/dialogs/level7/nl/3thisisnot.ogg","stream" )
				iwont = love.audio.newSource( "/externalassets/dialogs/level7/nl/4iwont.ogg","stream" )
				maybewehavetoswitch = love.audio.newSource( "/externalassets/dialogs/level7/nl/6maybewehavetoswitch.ogg","stream" )
				finallyitsthere = love.audio.newSource( "/externalassets/dialogs/level7/nl/10finallyitsthere.ogg","stream" )
				weshouldsearchthecoral = love.audio.newSource( "/externalassets/dialogs/level7/nl/12weshouldsearchthecoral.ogg","stream" )
				dontweneedamicroscope = love.audio.newSource( "/externalassets/dialogs/level7/nl/14dontweneedamicroscope.ogg","stream" )
				coralturtles = love.audio.newSource( "/externalassets/dialogs/level7/nl/16coralturtles.ogg","stream" )

		end
		
		if language2=="en" then
				if accent2=="br" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/en/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/en/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/en/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/en/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/en/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/en/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/en/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/en/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/en/17andmoreover.ogg","stream" )
				elseif accent2=="us" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/en-us/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/en-us/17andmoreover.ogg","stream" )
				end
		elseif language2=="pl" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/pl/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/pl/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/pl/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/pl/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/pl/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/pl/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/pl/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/pl/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/pl/17andmoreover.ogg","stream" )
		elseif language2=="cs" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/cs/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/cs/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/cs/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/cs/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/cs/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/cs/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/cs/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/cs/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/cs/17andmoreover.ogg","stream" )
		elseif language2=="nl" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/nl/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/nl/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/nl/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/nl/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/nl/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/nl/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/nl/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/nl/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/nl/17andmoreover.ogg","stream" )
		elseif language2=="fr" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/fr/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/fr/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/fr/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/fr/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/fr/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/fr/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/fr/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/fr/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/fr/17andmoreover.ogg","stream" )
					
		elseif language2=="es" then
					if accent2=="es" then
				elseif accent2=="la" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level7/es-la/1mygod.ogg","stream" )
					iprobablyshould  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/5iprobablyshould.ogg","stream" )
					amatress  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/7amatress.ogg","stream" )
					nowwecandropthesnail  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/8nowwecandropthesnail.ogg","stream" )
					itwouldbedifficulttopick  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/9itwouldbedifficulttopick.ogg","stream" )
					thepoorsnail  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/11thepoorsnail.ogg","stream" )
					theerearegoingtobe  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/13therearegoingtobe.ogg","stream" )
					yestheyaresmall  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/15yestheyaresmall.ogg","stream" )
					andmoreover  = love.audio.newSource( "/externalassets/dialogs/level7/es-la/17andmoreover.ogg","stream" )
			
				end
		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
				--fish 2
				mygod:setEffect('myEffect')
				iprobablyshould:setEffect('myEffect')
				amatress:setEffect('myEffect')
				nowwecandropthesnail:setEffect('myEffect')
				itwouldbedifficulttopick:setEffect('myEffect')
				thepoorsnail:setEffect('myEffect')
				theerearegoingtobe:setEffect('myEffect')
				yestheyaresmall:setEffect('myEffect')
				andmoreover:setEffect('myEffect')
				
				--fish 1	
				icant:setEffect('myEffect')
				thisisnot:setEffect('myEffect')
				iwont:setEffect('myEffect')
				maybewehavetoswitch:setEffect('myEffect')
				finallyitsthere:setEffect('myEffect')
				weshouldsearchthecoral:setEffect('myEffect')
				dontweneedamicroscope:setEffect('myEffect')
				coralturtles:setEffect('myEffect')
				
	elseif nLevel==8 then
		
		if language=="en" then
					if accent=="br" then
						thetoilet = love.audio.newSource( "/externalassets/dialogs/level8/en/1thetoilet.ogg","stream" )
						youdont = love.audio.newSource( "/externalassets/dialogs/level8/en/3youdont.ogg","stream" )
						youve = love.audio.newSource( "/externalassets/dialogs/level8/en/5youve.ogg","stream" )
						theflushing = love.audio.newSource( "/externalassets/dialogs/level8/en/7theflushing.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level8/en/10what.ogg","stream" )
	
					elseif accent=="us" then
						thetoilet = love.audio.newSource( "/externalassets/dialogs/level8/en-us/1thetoilet.ogg","stream" )
						youdont = love.audio.newSource( "/externalassets/dialogs/level8/en-us/3youdont.ogg","stream" )
						youve = love.audio.newSource( "/externalassets/dialogs/level8/en-us/5youve.ogg","stream" )
						theflushing = love.audio.newSource( "/externalassets/dialogs/level8/en-us/7theflushing.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level8/en-us/10what.ogg","stream" )
			
					end
		
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
						thetoilet = love.audio.newSource( "/externalassets/dialogs/level8/es-la/1thetoilet.ogg","stream" )
						youdont = love.audio.newSource( "/externalassets/dialogs/level8/es-la/3youdont.ogg","stream" )
						youve = love.audio.newSource( "/externalassets/dialogs/level8/es-la/5youve.ogg","stream" )
						theflushing = love.audio.newSource( "/externalassets/dialogs/level8/es-la/7theflushing.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level8/es-la/10what.ogg","stream" )
	
				end

		elseif language=="fr" then
						thetoilet = love.audio.newSource( "/externalassets/dialogs/level8/fr/1thetoilet.ogg","stream" )
						youdont = love.audio.newSource( "/externalassets/dialogs/level8/fr/3youdont.ogg","stream" )
						youve = love.audio.newSource( "/externalassets/dialogs/level8/fr/5youve.ogg","stream" )
						theflushing = love.audio.newSource( "/externalassets/dialogs/level8/fr/7theflushing.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level8/fr/10what.ogg","stream" )
		
		elseif language=="nl" then
						thetoilet = love.audio.newSource( "/externalassets/dialogs/level8/nl/1thetoilet.ogg","stream" )
						youdont = love.audio.newSource( "/externalassets/dialogs/level8/nl/3youdont.ogg","stream" )
						youve = love.audio.newSource( "/externalassets/dialogs/level8/nl/5youve.ogg","stream" )
						theflushing = love.audio.newSource( "/externalassets/dialogs/level8/nl/7theflushing.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level8/nl/10what.ogg","stream" )
		
		end

	if language2=="en" then
				if accent2=="br" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/en/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/en/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/en/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/en/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/en/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/en/11dontyou.ogg","stream" )
				elseif accent2=="us" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/en-us/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/en-us/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/en-us/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/en-us/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/en-us/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/en-us/11dontyou.ogg","stream" )
				end
		elseif language2=="pl" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/pl/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/pl/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/pl/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/pl/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/pl/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/pl/11dontyou.ogg","stream" )
		elseif language2=="cs" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/cs/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/cs/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/cs/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/cs/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/cs/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/cs/11dontyou.ogg","stream" )
		elseif language2=="nl" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/nl/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/nl/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/nl/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/nl/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/nl/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/nl/11dontyou.ogg","stream" )
		
		elseif language2=="fr" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/fr/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/fr/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/fr/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/fr/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/fr/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/fr/11dontyou.ogg","stream" )				
		
		elseif language2=="es" then
		
					if accent2=="es" then
					
					elseif accent2=="la" then
						what = love.audio.newSource( "/externalassets/dialogs/level8/es-la/2what.ogg","stream" )
						david = love.audio.newSource( "/externalassets/dialogs/level8/es-la/4david.ogg","stream" )
						didntitell = love.audio.newSource( "/externalassets/dialogs/level8/es-la/6didntitell.ogg","stream" )
						luckily = love.audio.newSource( "/externalassets/dialogs/level8/es-la/8luckily.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level8/es-la/9idont.ogg","stream" )
						dontyou = love.audio.newSource( "/externalassets/dialogs/level8/es-la/11dontyou.ogg","stream" )
				
					end
		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
						--fish 1
										
						thetoilet:setEffect('myEffect')
						youdont:setEffect('myEffect')
						youve:setEffect('myEffect')
						theflushing:setEffect('myEffect')
						whatdo:setEffect('myEffect')
						
						--fish 2
						what:setEffect('myEffect')
						david:setEffect('myEffect')
						didntitell:setEffect('myEffect')
						idont:setEffect('myEffect')
						luckily:setEffect('myEffect')
						dontyou:setEffect('myEffect')
	elseif nLevel==9 then
			
		if language=="en" then
					if accent=="br" then
						heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/en/6heyisanybody.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level9/en/7hello.ogg","stream" )
						isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/en/8isanybodyhere.ogg","stream" )
						idontknow = love.audio.newSource( "/externalassets/dialogs/level9/en/13idontknow.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level9/en/14careful.ogg","stream" )
						dontsay = love.audio.newSource( "/externalassets/dialogs/level9/en/15dontsay.ogg","stream" )
				
					elseif accent=="us" then
						heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/en-us/6heyisanybody.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level9/en-us/7hello.ogg","stream" )
						isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/en-us/8isanybodyhere.ogg","stream" )
						idontknow = love.audio.newSource( "/externalassets/dialogs/level9/en-us/13idontknow.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level9/en-us/14careful.ogg","stream" )
						dontsay = love.audio.newSource( "/externalassets/dialogs/level9/en-us/15dontsay.ogg","stream" )
					end
		elseif language=="de" then
						heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/de/6heyisanybody.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level9/de/7hello.ogg","stream" )
						isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/de/8isanybodyhere.ogg","stream" )
						idontknow = love.audio.newSource( "/externalassets/dialogs/level9/de/13idontknow.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level9/de/14careful.ogg","stream" )
						dontsay = love.audio.newSource( "/externalassets/dialogs/level9/de/15dontsay.ogg","stream" )		
		elseif language=="fr" then
						heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/fr/6heyisanybody.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level9/fr/7hello.ogg","stream" )
						isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/fr/8isanybodyhere.ogg","stream" )
						idontknow = love.audio.newSource( "/externalassets/dialogs/level9/fr/13idontknow.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level9/fr/14careful.ogg","stream" )
						dontsay = love.audio.newSource( "/externalassets/dialogs/level9/fr/15dontsay.ogg","stream" )		
		elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
							heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/es-la/6heyisanybody.ogg","stream" )
							hello = love.audio.newSource( "/externalassets/dialogs/level9/es-la/7hello.ogg","stream" )
							isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/es-la/8isanybodyhere.ogg","stream" )
							idontknow = love.audio.newSource( "/externalassets/dialogs/level9/es-la/13idontknow.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level9/es-la/14careful.ogg","stream" )
							dontsay = love.audio.newSource( "/externalassets/dialogs/level9/es-la/15dontsay.ogg","stream" )
				end
		
		elseif language=="nl" then
						heyisanybody = love.audio.newSource( "/externalassets/dialogs/level9/nl/6heyisanybody.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level9/nl/7hello.ogg","stream" )
						isanybodyhere = love.audio.newSource( "/externalassets/dialogs/level9/nl/8isanybodyhere.ogg","stream" )
						idontknow = love.audio.newSource( "/externalassets/dialogs/level9/nl/13idontknow.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level9/nl/14careful.ogg","stream" )
						dontsay = love.audio.newSource( "/externalassets/dialogs/level9/nl/15dontsay.ogg","stream" )	
			end
			
			if language2=="en" then
					if accent2=="br" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/en/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/en/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/en/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/en/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/en/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/en/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/en/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/en/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/en/12whoseeyes.ogg","stream" )
						
					elseif accent2=="us" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/en-us/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/en-us/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/en-us/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/en-us/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/en-us/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/en-us/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/en-us/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/en-us/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/en-us/12whoseeyes.ogg","stream" )
					end
	
					
			elseif language2=="es" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/es-la/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/es-la/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/es-la/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/es-la/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/es-la/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/es-la/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/es-la/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/es-la/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/es-la/12whoseeyes.ogg","stream" )
					
		
		
		
			elseif language2=="nl" then			
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/nl/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/nl/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/nl/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/nl/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/nl/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/nl/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/nl/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/nl/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/nl/12whoseeyes.ogg","stream" )
		
			elseif language2=="fr" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/fr/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/fr/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/fr/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/fr/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/fr/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/fr/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/fr/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/fr/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/fr/12whoseeyes.ogg","stream" )
		
		
			elseif language2=="pl" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/pl/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself = love.audio.newSource( "/externalassets/dialogs/level9/pl/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/pl/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/pl/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/pl/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/pl/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/pl/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/pl/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/pl/12whoseeyes.ogg","stream" )
			elseif language2=="cs" then
						stopmakingfaces = love.audio.newSource( "/externalassets/dialogs/level9/cs/1stopmakingfaces.ogg","stream" )
						stoplookingatyourself  = love.audio.newSource( "/externalassets/dialogs/level9/cs/2stoplookingatyourself.ogg","stream" )
						thatsgreat = love.audio.newSource( "/externalassets/dialogs/level9/cs/3thatsgreat.ogg","stream" )
						asusual = love.audio.newSource( "/externalassets/dialogs/level9/cs/4asusual.ogg","stream" )
						wait = love.audio.newSource( "/externalassets/dialogs/level9/cs/5wait.ogg","stream" )
						stopshouting = love.audio.newSource( "/externalassets/dialogs/level9/cs/9stopshouting.ogg","stream" )
						stopyourscreaming = love.audio.newSource( "/externalassets/dialogs/level9/cs/10stopyourscreaming.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level9/cs/11hereiam.ogg","stream" )
						whoseeyes = love.audio.newSource( "/externalassets/dialogs/level9/cs/12whoseeyes.ogg","stream" )
				end
				
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
				
						--fish 1
				
						heyisanybody:setEffect('myEffect')
						hello:setEffect('myEffect')
						isanybodyhere:setEffect('myEffect')	
						idontknow:setEffect('myEffect')
						careful:setEffect('myEffect')
						dontsay:setEffect('myEffect')

						--fish 2
						stopmakingfaces:setEffect('myEffect')
						stoplookingatyourself:setEffect('myEffect')
						thatsgreat:setEffect('myEffect')
						asusual:setEffect('myEffect')
						wait:setEffect('myEffect')
						stopshouting:setEffect('myEffect')
						stopyourscreaming:setEffect('myEffect')
						hereiam:setEffect('myEffect')
						whoseeyes:setEffect('myEffect')
	
	elseif nLevel==10 then
			
		if language=="en" then
					if accent=="br" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/en/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/en/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/en/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/en/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/en/12careful.ogg","stream" )
		
					elseif accent=="us" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/en-us/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/en-us/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/en-us/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/en-us/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/en-us/12careful.ogg","stream" )
					
					end
		elseif language=="de" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/de/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/de/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/de/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/de/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/de/12careful.ogg","stream" )
			
				elseif language=="es" then
					if accent=="es" then
					elseif accent=="la" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/es-la/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/es-la/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/es-la/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/es-la/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/es-la/12careful.ogg","stream" )
					end
				elseif language=="fr" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/fr/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/fr/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/fr/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/fr/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/fr/12careful.ogg","stream" )
					
			elseif language=="nl" then
							ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level10/nl/2ihaveafeeling.ogg","stream" )
							idontthink = love.audio.newSource( "/externalassets/dialogs/level10/nl/8idontthink.ogg","stream" )
							stopthinking = love.audio.newSource( "/externalassets/dialogs/level10/nl/9stopthinking.ogg","stream" )
							youwant = love.audio.newSource( "/externalassets/dialogs/level10/nl/10youwant.ogg","stream" )
							careful = love.audio.newSource( "/externalassets/dialogs/level10/nl/12careful.ogg","stream" )
					
			end
			
			if language2=="en" then
			
					if accent2=="br" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/en/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/en/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/en/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/en/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/en/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/en/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/en/11icantdo.ogg","stream" )
			
						
					elseif accent2=="us" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/en-us/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/en-us/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/en-us/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/en-us/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/en-us/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/en-us/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/en-us/11icantdo.ogg","stream" )
		
					end
			elseif language2=="es" then
						
					if accent2=="es" then
					elseif accent2=="la" then
						
						
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/es-la/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/es-la/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/es-la/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/es-la/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/es-la/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/es-la/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/es-la/11icantdo.ogg","stream" )
			
						
					end
				
			elseif language2=="pl" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/pl/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/pl/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/pl/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/pl/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/pl/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/pl/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/pl/11icantdo.ogg","stream" )
			elseif language2=="cs" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/cs/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/cs/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/cs/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/cs/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/cs/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/cs/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/cs/11icantdo.ogg","stream" )
			elseif language2=="nl" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/nl/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/nl/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/nl/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/nl/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/nl/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/nl/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/nl/11icantdo.ogg","stream" )
						
			elseif language2=="fr" then
						lookthepartyboat = love.audio.newSource( "/externalassets/dialogs/level10/fr/1looktheparty.ogg","stream" )
						youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level10/fr/3youandyourfeelings.ogg","stream" )
						maybeyouareright = love.audio.newSource( "/externalassets/dialogs/level10/fr/4maybeyouareright.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level10/fr/5doyouthink.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level10/fr/6ihope.ogg","stream" )
						whatiftheystart = love.audio.newSource( "/externalassets/dialogs/level10/fr/7whatiftheystart.ogg","stream" )
						icantdo = love.audio.newSource( "/externalassets/dialogs/level10/fr/11icantdo.ogg","stream" )
				
				end
				
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
				
						--fish 2
						lookthepartyboat:setEffect('myEffect')
						youandyourfeelings:setEffect('myEffect')
						maybeyouareright:setEffect('myEffect')
						doyouthink:setEffect('myEffect')
						ihope:setEffect('myEffect')
						whatiftheystart:setEffect('myEffect')
						icantdo:setEffect('myEffect')
						
						--fish 1
							ihaveafeeling:setEffect('myEffect')
							idontthink:setEffect('myEffect')
							stopthinking:setEffect('myEffect')
							youwant:setEffect('myEffect')
							careful:setEffect('myEffect')
			
		
		elseif nLevel==11 then
			
			if language=="en" then
					if accent=="br" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/en/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/en/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/en/6yesitoo.ogg","stream" )

					elseif accent=="us" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/en-us/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/en-us/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/en-us/6yesitoo.ogg","stream" )
			
					end
			elseif language=="de" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/de/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/de/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/de/6yesitoo.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/es-la/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/es-la/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/es-la/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/es-la/7idlike.ogg","stream" )
					end
					
			elseif language=="fr" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/fr/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/fr/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/fr/6yesitoo.ogg","stream" )
			
			elseif language=="nl" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/nl/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/nl/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/nl/6yesitoo.ogg","stream" )
			
			
			end
						
		if language2=="en" then
			if accent2=="br" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/en/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/en/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/en/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/en/7idlike.ogg","stream" )
		
						
					elseif accent2=="us" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/en-us/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/en-us/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/en-us/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/en-us/7idlike.ogg","stream" )
			
					end
		elseif language2=="es" then
					if accent=="es" then
					elseif accent=="la" then
						thehostility = love.audio.newSource( "/externalassets/dialogs/level11/es-la/2thehostility.ogg","stream" )
						andelk = love.audio.newSource( "/externalassets/dialogs/level11/es-la/4andelk.ogg","stream" )
						yesitoo = love.audio.newSource( "/externalassets/dialogs/level11/es-la/6yesitoo.ogg","stream" )
			
					end
					if accent2=="es" then
					elseif accent2=="la" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/es-la/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/es-la/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/es-la/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/es-la/7idlike.ogg","stream" )
			
					end
		elseif language2=="pl" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/pl/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/pl/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/pl/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/pl/7idlike.ogg","stream" )
		elseif language2=="cs" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/cs/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/cs/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/cs/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/cs/7idlike.ogg","stream" )
						
		elseif language2=="nl" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/nl/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/nl/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/nl/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/nl/7idlike.ogg","stream" )

		elseif language2=="fr" then
						thewar = love.audio.newSource( "/externalassets/dialogs/level11/fr/1thewar.ogg","stream" )
						theywill = love.audio.newSource( "/externalassets/dialogs/level11/fr/3theywillfight.ogg","stream" )
						nowthis = love.audio.newSource( "/externalassets/dialogs/level11/fr/5nowthis.ogg","stream" )
						idlike = love.audio.newSource( "/externalassets/dialogs/level11/fr/7idlike.ogg","stream" )
		
		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
						--fish 2
						thewar:setEffect('myEffect')
						theywill:setEffect('myEffect')
						nowthis:setEffect('myEffect')
						idlike:setEffect('myEffect')
						
						--fish 1
						thehostility:setEffect('myEffect')
						andelk:setEffect('myEffect')
						yesitoo:setEffect('myEffect')
		elseif nLevel==12 then
		
			if language=="en" then
					if accent=="br" then
					
						could = love.audio.newSource( "/externalassets/dialogs/level12/en/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/en/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/en/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/en/5itsuitsyou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/en/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/en/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/en/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/en/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/en/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/en/22icansee.ogg","stream" )
	
					elseif accent=="us" then
						could = love.audio.newSource( "/externalassets/dialogs/level12/en-us/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/en-us/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/en-us/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/en-us/5itsuityou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/en-us/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/en-us/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/en-us/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/en-us/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/en-us/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/en-us/22icansee.ogg","stream" )
					end
				elseif language=="de" then
						could = love.audio.newSource( "/externalassets/dialogs/level12/de/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/de/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/de/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/de/5itsuitsyou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/de/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/de/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/de/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/de/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/de/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/de/23icansee.ogg","stream" )
				elseif language=="es" then
					if accent=="es" then
					elseif accent=="la" then
						could = love.audio.newSource( "/externalassets/dialogs/level12/es-la/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/es-la/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/es-la/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/es-la/5itsuitsyou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/es-la/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/es-la/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/es-la/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/es-la/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/es-la/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/es-la/23icansee.ogg","stream" )
					end
					
				elseif language=="fr" then
						could = love.audio.newSource( "/externalassets/dialogs/level12/fr/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/fr/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/fr/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/fr/5itsuitsyou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/fr/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/fr/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/fr/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/fr/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/fr/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/fr/23icansee.ogg","stream" )
				
				elseif language=="nl" then
						could = love.audio.newSource( "/externalassets/dialogs/level12/nl/1could.ogg","stream" )
						thetitle = love.audio.newSource( "/externalassets/dialogs/level12/nl/3thetitle.ogg","stream" )
						whatisthat = love.audio.newSource( "/externalassets/dialogs/level12/nl/4whatisthat.ogg","stream" )
						itsuitsyou = love.audio.newSource( "/externalassets/dialogs/level12/nl/5itsuitsyou.ogg","stream" )
						averynice = love.audio.newSource( "/externalassets/dialogs/level12/nl/7averynice.ogg","stream" )
						ifyou = love.audio.newSource( "/externalassets/dialogs/level12/nl/11ifyou.ogg","stream" )
						andabeautiful = love.audio.newSource( "/externalassets/dialogs/level12/nl/13andabeautiful.ogg","stream" )
						itseems = love.audio.newSource( "/externalassets/dialogs/level12/nl/17itseems.ogg","stream" )
						stoptalking = love.audio.newSource( "/externalassets/dialogs/level12/nl/19stoptalking.ogg","stream" )
						icansee = love.audio.newSource( "/externalassets/dialogs/level12/nl/23icansee.ogg","stream" )
								
				end
				
				if language2=="en" then
				
					if accent2=="br" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/en/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/en/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/en/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/en/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/en/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/en/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/en/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/en/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/en/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/en/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/en/20nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/en/21whatdo.ogg","stream" )

					elseif accent2=="us" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/en-us/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/en-us/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/en-us/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/en-us/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/en-us/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/en-us/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/en-us/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/en-us/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/en-us/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/en-us/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/en-us/20nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/en-us/21whatdo.ogg","stream" )

					end
				elseif language2=="es" then
				
						if accent2=="es" then
					elseif accent2=="la" then
						--[[whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/es-la/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/es-la/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/es-la/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/es-la/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/es-la/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/es-la/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/es-la/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/es-la/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/es-la/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/es-la/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/es-la/20nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/es-la/21whatdo.ogg","stream" )
					--]]
					end
				elseif language2=="pl" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/pl/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/pl/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/pl/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/pl/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/pl/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/pl/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/pl/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/pl/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/pl/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/pl/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/pl/21nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/pl/22whatdo.ogg","stream" )
				elseif language2=="cs" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/cs/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/cs/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/cs/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/cs/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/cs/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/cs/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/cs/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/cs/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/cs/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/cs/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/cs/21nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/cs/22whatdo.ogg","stream" )
				elseif language2=="nl" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/nl/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/nl/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/nl/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/nl/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/nl/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/nl/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/nl/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/nl/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/nl/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/nl/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/nl/20nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/nl/22whatdo.ogg","stream" )

				elseif language2=="fr" then
						whatmakesyou = love.audio.newSource( "/externalassets/dialogs/level12/fr/2what.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level12/fr/6thanks.ogg","stream" )
						yourstetson = love.audio.newSource( "/externalassets/dialogs/level12/fr/8yourstetson.ogg","stream" )
						myears = love.audio.newSource( "/externalassets/dialogs/level12/fr/9myears.ogg","stream" )
						thatssome = love.audio.newSource( "/externalassets/dialogs/level12/fr/10thatssome.ogg","stream" )
						thissure = love.audio.newSource( "/externalassets/dialogs/level12/fr/12thissure.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level12/fr/14ithink.ogg","stream" )
						itlooks = love.audio.newSource( "/externalassets/dialogs/level12/fr/15itlooks.ogg","stream" )
						theair = love.audio.newSource( "/externalassets/dialogs/level12/fr/16theair.ogg","stream" )
						idont = love.audio.newSource( "/externalassets/dialogs/level12/fr/18idont.ogg","stream" )
						nojust = love.audio.newSource( "/externalassets/dialogs/level12/fr/21nojust.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level12/fr/23whatdo.ogg","stream" )


				end
				
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
				
				
						-- fish 1
						could:setEffect('myEffect')
						thetitle:setEffect('myEffect')
						whatisthat:setEffect('myEffect')
						itsuitsyou:setEffect('myEffect')
						averynice:setEffect('myEffect')
						ifyou:setEffect('myEffect')
						andabeautiful:setEffect('myEffect')
						itseems:setEffect('myEffect')
						stoptalking:setEffect('myEffect')
						icansee:setEffect('myEffect')
						
						-- fish 2
						whatmakesyou:setEffect('myEffect')
						thanks:setEffect('myEffect')
						yourstetson:setEffect('myEffect')
						myears:setEffect('myEffect')
						thatssome:setEffect('myEffect')
						thissure:setEffect('myEffect')
						ithink:setEffect('myEffect')
						itlooks:setEffect('myEffect')
						theair:setEffect('myEffect')
						idont:setEffect('myEffect')
						nojust:setEffect('myEffect')
						whatdo:setEffect('myEffect')
		elseif nLevel==13 then
		
		
			if language=="en" then
			
					--vikings
			
					themusic = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/1themusic.ogg","stream" )
					theystopped = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/2theystopped.ogg","stream" )
					themusicstopped = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/3themusicstopped.ogg","stream" )
					ohno = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/4ohno.ogg","stream" )
					notagain = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/5notagain.ogg","stream" )
					whathappened = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/6whathappened.ogg","stream" )
					again = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/7again.ogg","stream" )
					ohno2 = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/8ohno.ogg","stream" )
					thebloody = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/9thebloody.ogg","stream" )
					whatare = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/10whatare.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/11howcould.ogg","stream" )
					solo = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/12solo.ogg","stream" )
					nevermind = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/13nevermind.ogg","stream" )
					nevermind2 = love.audio.newSource( "/externalassets/dialogs/level13/en/vikings/14nevermind2.ogg","stream" )
					
			
					if accent=="br" then
						sure = love.audio.newSource( "/externalassets/dialogs/level13/en/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/en/4maybe.ogg","stream" )
	
						
					elseif accent=="us" then
						sure = love.audio.newSource( "/externalassets/dialogs/level13/en-us/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/en-us/4maybe.ogg","stream" )

					end
			elseif language=="de" then
						sure = love.audio.newSource( "/externalassets/dialogs/level13/de/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/de/4maybe.ogg","stream" )
			elseif language=="es" then
						if accent=="es" then
						elseif accent=="la" then
							sure = love.audio.newSource( "/externalassets/dialogs/level13/es-la/2sure.ogg","stream" )
							maybe = love.audio.newSource( "/externalassets/dialogs/level13/es-la/4maybe.ogg","stream" )
						end
			elseif language=="fr" then
						sure = love.audio.newSource( "/externalassets/dialogs/level13/fr/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/fr/4maybe.ogg","stream" )
						
			elseif language=="nl" then
						sure = love.audio.newSource( "/externalassets/dialogs/level13/nl/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/nl/4maybe.ogg","stream" )
					
					--vikings
					
					themusic = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/1themusic.ogg","stream" )
					theystopped = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/2theystopped.ogg","stream" )
					themusicstopped = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/3themusicstopped.ogg","stream" )
					ohno = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/4ohno.ogg","stream" )
					notagain = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/5notagain.ogg","stream" )
					whathappened = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/6whathappened.ogg","stream" )
					again = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/7again.ogg","stream" )
					ohno2 = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/8ohno.ogg","stream" )
					thebloody = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/9thebloody.ogg","stream" )
					whatare = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/10whatare.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/11howcould.ogg","stream" )
					solo = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/12solo.ogg","stream" )
					nevermind = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/13nevermind.ogg","stream" )
					nevermind2 = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/14nevermind2.ogg","stream" )
					
						sure = love.audio.newSource( "/externalassets/dialogs/level13/fr/2sure.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level13/fr/4maybe.ogg","stream" )
					
				elseif language=="sv" then
				
				--vikings
					
					themusic = love.audio.newSource( "/externalassets/dialogs/level13/fr/vikings/1themusic.ogg","stream" )
					theystopped = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/2theystopped.ogg","stream" )
					themusicstopped = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/3themusicstopped.ogg","stream" )
					ohno = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/4ohno.ogg","stream" )
					notagain = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/5notagain.ogg","stream" )
					whathappened = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/6whathappened.ogg","stream" )
					again = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/7again.ogg","stream" )
					ohno2 = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/8ohno.ogg","stream" )
					thebloody = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/9thebloody.ogg","stream" )
					whatare = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/10whatare.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/11howcould.ogg","stream" )
					solo = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/12solo.ogg","stream" )
					nevermind = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/13nevermind.ogg","stream" )
					nevermind2 = love.audio.newSource( "/externalassets/dialogs/level13/sv/vikings/14nevermind2.ogg","stream" )
				
				end
				
				
					

				
				if language2=="en" then
				
					if accent2=="br" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/en/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/en/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/en/5thatsawful.ogg","stream" )

						
					elseif accent2=="us" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/en-us/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/en-us/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/en-us/5thatsawful.ogg","stream" )

					end
					
				elseif language2=="pl" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/pl/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/pl/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/pl/5thatsawful.ogg","stream" )
				elseif language2=="cs" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/cs/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/cs/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/cs/5thatsawful.ogg","stream" )
						
				elseif language2=="nl" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/nl/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/nl/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/nl/5thatsawful.ogg","stream" )
				
				elseif language2=="fr" then
						thismust = love.audio.newSource( "/externalassets/dialogs/level13/fr/1thismust.ogg","stream" )
						shemust = love.audio.newSource( "/externalassets/dialogs/level13/fr/3shemust.ogg","stream" )
						thatsawful = love.audio.newSource( "/externalassets/dialogs/level13/fr/5thatsawful.ogg","stream" )

				end
				
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
								
					themusic:setEffect('myEffect')
					theystopped:setEffect('myEffect')
					themusicstopped:setEffect('myEffect')
					ohno:setEffect('myEffect')
					notagain:setEffect('myEffect')
					whathappened:setEffect('myEffect')
					again:setEffect('myEffect')
					ohno:setEffect('myEffect')
					thebloody:setEffect('myEffect')
					whatare:setEffect('myEffect')
					howcould:setEffect('myEffect')
					solo:setEffect('myEffect')
					nevermind:setEffect('myEffect')
					nevermind2:setEffect('myEffect')
								
						sure:setEffect('myEffect')
						maybe:setEffect('myEffect')
								
						thismust:setEffect('myEffect')
						shemust:setEffect('myEffect')
						thatsawful:setEffect('myEffect')
		
		elseif nLevel==14 then
		
			if language=="en" then
					if accent=="br" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/en/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/en/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/en/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/en/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/en/8bethankful.ogg","stream" )
	
				elseif accent=="us" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/en-us/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/en-us/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/en-us/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/en-us/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/en-us/8bethankful.ogg","stream" )
		
				end
			elseif language=="de" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/de/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/de/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/de/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/de/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/de/8bethankful.ogg","stream" )
			elseif language=="fr" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/fr/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/fr/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/fr/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/fr/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/fr/8bethankful.ogg","stream" )
			elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/es-la/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/es-la/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/es-la/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/es-la/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/es-la/8bethankful.ogg","stream" )
				end
			elseif language=="nl" then
						thisis1 = love.audio.newSource( "/externalassets/dialogs/level14/nl/2thisis1.ogg","stream" )
						thisis2 = love.audio.newSource( "/externalassets/dialogs/level14/nl/3thisis2.ogg","stream" )
						thisis3 = love.audio.newSource( "/externalassets/dialogs/level14/nl/4thisis3.ogg","stream" )
						canyousee = love.audio.newSource( "/externalassets/dialogs/level14/nl/5canyousee.ogg","stream" )
						bethankful = love.audio.newSource( "/externalassets/dialogs/level14/nl/8bethankful.ogg","stream" )
			end
			if language2=="en" then
					if accent2=="br" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/en/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/en/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/en/7seats.ogg","stream" )
					
						
					elseif accent2=="us" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/en-us/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/en-us/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/en-us/7seats.ogg","stream" )
					
					end
		
			
			elseif language2=="pl" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/pl/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/pl/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/pl/7seats.ogg","stream" )
			
			elseif language2=="cs" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/cs/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/cs/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/cs/7seats.ogg","stream" )
			
			elseif language2=="nl" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/nl/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/nl/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/nl/7seats.ogg","stream" )
			
			elseif language2=="fr" then
						whatkind = love.audio.newSource( "/externalassets/dialogs/level14/fr/1whatkind.ogg","stream" )
						thisisnot = love.audio.newSource( "/externalassets/dialogs/level14/fr/6thisisnot.ogg","stream" )
						seats = love.audio.newSource( "/externalassets/dialogs/level14/fr/7seats.ogg","stream" )
			end
			loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
			
		
						--fish 1
						thisis1:setEffect('myEffect')
						thisis2:setEffect('myEffect')
						thisis3:setEffect('myEffect')
						canyousee:setEffect('myEffect')
						bethankful:setEffect('myEffect')
						--fish 2
						whatkind:setEffect('myEffect')
						thisisnot:setEffect('myEffect')
						seats:setEffect('myEffect')
		
		elseif nLevel==15 then
		
				
			if language=="en" then
			
					
					--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/13hello2.ogg","stream" )
			
					chiefinspector = love.audio.newSource( "/externalassets/dialogs/level15/en/chiefinspector.ogg","stream" )
					
					answermachine = love.audio.newSource( "/externalassets/dialogs/level15/en/answermachine.ogg","stream" )

					
				if accent=="br" then
					shouldnt = love.audio.newSource( "/externalassets/dialogs/level15/en/3shouldnt.ogg","stream" )
					doyou = love.audio.newSource( "/externalassets/dialogs/level15/en/4doyou.ogg","stream" )
					shouldi = love.audio.newSource( "/externalassets/dialogs/level15/en/5shouldi.ogg","stream" )
					nowisquite = love.audio.newSource( "/externalassets/dialogs/level15/en/8nowisquite.ogg","stream" )
		
				elseif accent=="us" then
					shouldnt = love.audio.newSource( "/externalassets/dialogs/level15/en-us/3shouldnt.ogg","stream" )
					doyou = love.audio.newSource( "/externalassets/dialogs/level15/en-us/4doyou.ogg","stream" )
					shouldi = love.audio.newSource( "/externalassets/dialogs/level15/en-us/5shouldi.ogg","stream" )
					nowisquite = love.audio.newSource( "/externalassets/dialogs/level15/en-us/8nowisquite.ogg","stream" )

				end
			elseif language=="es" then
				chiefinspector = love.audio.newSource( "/externalassets/dialogs/level15/es-la/chiefinspector.ogg","stream" )
				
				
				--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/en/snails/13hello2.ogg","stream" )
					
					answermachine = love.audio.newSource( "/externalassets/dialogs/level15/en/answermachine.ogg","stream" )
				if accent=="es" then
				elseif accent=="la" then
		
					shouldnt = love.audio.newSource( "/externalassets/dialogs/level15/es-la/3shouldnt.ogg","stream" )
					doyou = love.audio.newSource( "/externalassets/dialogs/level15/es-la/4doyou.ogg","stream" )
					shouldi = love.audio.newSource( "/externalassets/dialogs/level15/es-la/5shouldi.ogg","stream" )
					nowisquite = love.audio.newSource( "/externalassets/dialogs/level15/es-la/8nowisquite.ogg","stream" )
				end

			elseif language=="de" then
					shouldnt = love.audio.newSource( "/externalassets/dialogs/level15/de/3shouldnt.ogg","stream" )
					doyou = love.audio.newSource( "/externalassets/dialogs/level15/de/4doyou.ogg","stream" )
					shouldi = love.audio.newSource( "/externalassets/dialogs/level15/de/5shouldi.ogg","stream" )
					nowisquite = love.audio.newSource( "/externalassets/dialogs/level15/de/8nowisquite.ogg","stream" )
					
					--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/de/snails/13hello2.ogg","stream" )
					
		
					chiefinspector = love.audio.newSource( "/externalassets/dialogs/level15/de/chiefinspector.ogg","stream" )
					
					answermachine = love.audio.newSource( "/externalassets/dialogs/level15/de/answermachine.ogg","stream" )
					
		elseif language=="nl" or language2=="nl" then
		
					--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/nl/snails/13hello2.ogg","stream" )
					
					chiefinspector = love.audio.newSource( "/externalassets/dialogs/level15/nl/chiefinspector.ogg","stream" )
		
		elseif language=="fr" then
					shouldnt = love.audio.newSource( "/externalassets/dialogs/level15/fr/3shouldnt.ogg","stream" )
					doyou = love.audio.newSource( "/externalassets/dialogs/level15/fr/4doyou.ogg","stream" )
					shouldi = love.audio.newSource( "/externalassets/dialogs/level15/fr/5shouldi.ogg","stream" )
					nowisquite = love.audio.newSource( "/externalassets/dialogs/level15/fr/8nowisquite.ogg","stream" )
		
		
					--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/fr/snails/13hello2.ogg","stream" )

					chiefinspector = love.audio.newSource( "/externalassets/dialogs/level15/fr/chiefinspector.ogg","stream" )
					
					
					
			elseif language=="pl" then
					--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/pl/snails/13hello2.ogg","stream" )
					
			elseif language=="sv" then
						--snails
					thatisgoing = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/1thatsisgoing.ogg","stream" )
					thatisgoing2 = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/2thatsisgoing2.ogg","stream" )
					couldyou = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/3couldyou.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/4ihope.ogg","stream" )
					really = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/5really.ogg","stream" )
					ireallydont = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/6ireallydont.ogg","stream" )
					justtellme = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/7justtellme.ogg","stream" )
					hello = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/8hello.ogg","stream" )
					isanybody = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/9isanybody.ogg","stream" )
					areyou = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/10areyou.ogg","stream" )
					thesnail = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/11thesnail.ogg","stream" )
					icanthearyou = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/12icanthearyou.ogg","stream" )
					hello2 = love.audio.newSource( "/externalassets/dialogs/level15/sv/snails/13hello2.ogg","stream" )

			end

			
			if language2=="en" then
				if accent2=="br" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/en/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/en/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/en/7wecant.ogg","stream" )

					
				elseif accent2=="us" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/en-us/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/en-us/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/en-us/7wecant.ogg","stream" )
	
				end
		
			elseif language2=="es" then
					if accent2=="es" then
				elseif accent2=="la" then
					--sothis = love.audio.newSource( "/externalassets/dialogs/level15/es-la/1sothis.ogg","stream" )
					--anice = love.audio.newSource( "/externalassets/dialogs/level15/es-la/2anice.ogg","stream" )
					--wecant = love.audio.newSource( "/externalassets/dialogs/level15/es-la/7wecant.ogg","stream" )
	
				end
		
			elseif language2=="pl" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/pl/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/pl/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/pl/7wecant.ogg","stream" )
			
			elseif language2=="cs" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/cs/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/cs/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/cs/7wecant.ogg","stream" )
			
			elseif language2=="nl" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/nl/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/nl/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/nl/7wecant.ogg","stream" )
					answermachine = love.audio.newSource( "/externalassets/dialogs/level15/nl/answermachine.ogg","stream" )
			
			elseif language2=="fr" then
					sothis = love.audio.newSource( "/externalassets/dialogs/level15/fr/1sothis.ogg","stream" )
					anice = love.audio.newSource( "/externalassets/dialogs/level15/fr/2anice.ogg","stream" )
					wecant = love.audio.newSource( "/externalassets/dialogs/level15/fr/7wecant.ogg","stream" )
					answermachine = love.audio.newSource( "/externalassets/dialogs/level15/fr/answermachine.ogg","stream" )
			
			end
			
			loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
			
					--Snails
					thatisgoing:setEffect('myEffect')
					thatisgoing2:setEffect('myEffect')
					couldyou:setEffect('myEffect')
					ihope:setEffect('myEffect')
					really:setEffect('myEffect')
					ireallydont:setEffect('myEffect')
					justtellme:setEffect('myEffect')
					hello:setEffect('myEffect')
					isanybody:setEffect('myEffect')
					areyou:setEffect('myEffect')
					thesnail:setEffect('myEffect')
					icanthearyou:setEffect('myEffect')
					hello2:setEffect('myEffect')
			
					-- Inspector Clouseau & answermachine from the Family roundfish
					chiefinspector:setEffect('myEffect')
					answermachine:setEffect('myEffect')
			
					--fish 1
								
					shouldnt:setEffect('myEffect')
					doyou:setEffect('myEffect')
					shouldi:setEffect('myEffect')
					nowisquite:setEffect('myEffect')
					
					--fish 2
					sothis:setEffect('myEffect')
					anice:setEffect('myEffect')
					wecant:setEffect('myEffect')
		
		elseif nLevel==16 then
		
			if language=="en" then
				if accent=="br" then
					atleast = love.audio.newSource( "/externalassets/dialogs/level16/en/2atleast.ogg","stream" )
					ialways = love.audio.newSource( "/externalassets/dialogs/level16/en/4ialways.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level16/en/7maybe.ogg","stream" )
					sowhy = love.audio.newSource( "/externalassets/dialogs/level16/en/9sowhy.ogg","stream" )
					maybethereis = love.audio.newSource( "/externalassets/dialogs/level16/en/10maybethereis.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level16/en/13idontknow.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level16/en/14ithink.ogg","stream" )
					notoplug = love.audio.newSource( "/externalassets/dialogs/level16/en/16notoplug.ogg","stream" )
		
				elseif accent=="us" then
					atleast = love.audio.newSource( "/externalassets/dialogs/level16/en-us/2atleast.ogg","stream" )
					ialways = love.audio.newSource( "/externalassets/dialogs/level16/en-us/4ialways.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level16/en-us/7maybe.ogg","stream" )
					sowhy = love.audio.newSource( "/externalassets/dialogs/level16/en-us/9sowhy.ogg","stream" )
					maybethereis = love.audio.newSource( "/externalassets/dialogs/level16/en-us/10maybethereis.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level16/en-us/13idontknow.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level16/en-us/14ithink.ogg","stream" )
					notoplug = love.audio.newSource( "/externalassets/dialogs/level16/en-us/16notoplug.ogg","stream" )
				end
				
		elseif language=="fr" then
				atleast = love.audio.newSource( "/externalassets/dialogs/level16/fr/2atleast.ogg","stream" )
				ialways = love.audio.newSource( "/externalassets/dialogs/level16/fr/4ialways.ogg","stream" )
				maybe = love.audio.newSource( "/externalassets/dialogs/level16/fr/7maybe.ogg","stream" )
				sowhy = love.audio.newSource( "/externalassets/dialogs/level16/fr/9sowhy.ogg","stream" )
				maybethereis = love.audio.newSource( "/externalassets/dialogs/level16/fr/10maybethereis.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/level16/fr/13idontknow.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level16/fr/14ithink.ogg","stream" )
				notoplug = love.audio.newSource( "/externalassets/dialogs/level16/fr/16notoplug.ogg","stream" )
		
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
					atleast = love.audio.newSource( "/externalassets/dialogs/level16/es-la/2atleast.ogg","stream" )
					ialways = love.audio.newSource( "/externalassets/dialogs/level16/es-la/4ialways.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level16/es-la/7maybe.ogg","stream" )
					sowhy = love.audio.newSource( "/externalassets/dialogs/level16/es-la/9sowhy.ogg","stream" )
					maybethereis = love.audio.newSource( "/externalassets/dialogs/level16/es-la/10maybethereis.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level16/es-la/13idontknow.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level16/es-la/14ithink.ogg","stream" )
					notoplug = love.audio.newSource( "/externalassets/dialogs/level16/en/16notoplug.ogg","stream" )
			end
		
		elseif language=="nl" then
				atleast = love.audio.newSource( "/externalassets/dialogs/level16/nl/2atleast.ogg","stream" )
				ialways = love.audio.newSource( "/externalassets/dialogs/level16/nl/4ialways.ogg","stream" )
				maybe = love.audio.newSource( "/externalassets/dialogs/level16/nl/7maybe.ogg","stream" )
				sowhy = love.audio.newSource( "/externalassets/dialogs/level16/nl/9sowhy.ogg","stream" )
				maybethereis = love.audio.newSource( "/externalassets/dialogs/level16/nl/10maybethereis.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/level16/nl/13idontknow.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level16/nl/14ithink.ogg","stream" )
				notoplug = love.audio.newSource( "/externalassets/dialogs/level16/nl/16notoplug.ogg","stream" )
		
		end
			
		if language2=="en" then
			if accent2=="br" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/en/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/en/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/en/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/en/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/en/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/en/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/en/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/en/15toclimbout.ogg","stream" )

			
			elseif accent2=="us" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/en-us/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/en-us/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/en-us/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/en-us/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/en-us/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/en-us/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/en-us/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/en-us/15toclimbout.ogg","stream" )
	
			end

				
		elseif language2=="pl" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/pl/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/pl/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/pl/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/pl/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/pl/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/pl/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/pl/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/pl/15toclimbout.ogg","stream" )
			
		elseif language2=="cs" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/cs/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/cs/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/cs/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/cs/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/cs/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/cs/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/cs/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/cs/15toclimbout.ogg","stream" )
						
		elseif language2=="nl" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/nl/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/nl/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/nl/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/nl/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/nl/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/nl/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/nl/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/nl/15toclimbout.ogg","stream" )
		
		elseif language2=="fr" then
				thisissome = love.audio.newSource( "/externalassets/dialogs/level16/fr/1thisissome.ogg","stream" )
				thatisbut = love.audio.newSource( "/externalassets/dialogs/level16/fr/3thatisbut.ogg","stream" )
				itseems = love.audio.newSource( "/externalassets/dialogs/level16/fr/5itseems.ogg","stream" )
				iwonder = love.audio.newSource( "/externalassets/dialogs/level16/fr/6iwonder.ogg","stream" )
				anamphibious = love.audio.newSource( "/externalassets/dialogs/level16/fr/8anamphibious.ogg","stream" )
				itispossible = love.audio.newSource( "/externalassets/dialogs/level16/fr/11itispossible.ogg","stream" )
				doyouthink = love.audio.newSource( "/externalassets/dialogs/level16/fr/12doyouthink.ogg","stream" )
				toclimbout = love.audio.newSource( "/externalassets/dialogs/level16/fr/15toclimbout.ogg","stream" )		

		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
				--fish 1
								
				atleast:setEffect('myEffect')
				ialways:setEffect('myEffect')
				maybe:setEffect('myEffect')
				sowhy:setEffect('myEffect')
				maybethereis:setEffect('myEffect')
				idontknow:setEffect('myEffect')
				ithink:setEffect('myEffect')
				notoplug:setEffect('myEffect')
				
				--fish 2
				thisissome:setEffect('myEffect')
				thatisbut:setEffect('myEffect')
				itseems:setEffect('myEffect')
				iwonder:setEffect('myEffect')
				anamphibious:setEffect('myEffect')
				itispossible:setEffect('myEffect')
				doyouthink:setEffect('myEffect')
				toclimbout:setEffect('myEffect')
		
		elseif nLevel==17 then
		
			if language=="en" then
				--vikings
					shutup = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/1shutup.ogg","stream" )
					willyou = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/2willyou.ogg","stream" )
					soyouarenot = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/3soyouarenot.ogg","stream" )
					argh = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/4argh.ogg","stream" )
					hehehe = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/5hehehe.ogg","stream" )
					isitover = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/6isitover.ogg","stream" )
					arewesafe = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/7arewesafe.ogg","stream" )
					isthefight = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/8isthefight.ogg","stream" )
					arewestill = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/10arewestill.ogg","stream" )
					areweinvalhalla = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/11areweinvalhalla.ogg","stream" )
					whenare = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/12whenare.ogg","stream" )
					illletyou = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/13illletyou.ogg","stream" )
					canigo = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/14canigo.ogg","stream" )
					isntthis = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/15isntthis.ogg","stream" )
					patience = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/16patience.ogg","stream" )
					areyousure = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/17areyousure.ogg","stream" )
					awarrior = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/18awarrior.ogg","stream" )
					imincharge = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/19imincharge.ogg","stream" )
					whydid = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/20whydid.ogg","stream" )
					laughing = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/21laughing.ogg","stream" )
					realvikings = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/22realvikings.ogg","stream" )
					braids = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/23braids.ogg","stream" )
					whatkind = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/24whatkind.ogg","stream" )
					vikingfashion = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/25vikingfashion.ogg","stream" )
					isimply = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/26isimply.ogg","stream" )
					youshould = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/27youshould.ogg","stream" )
					butimcool = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/28butimcool.ogg","stream" )
					evenerik = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/29evenerik.ogg","stream" )
					nonsense = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/30nonsense.ogg","stream" )
					buthealso = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/31buthealso.ogg","stream" )
					hedidnot = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/32hedidnot.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/33doyouthink.ogg","stream" )
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/34definitely.ogg","stream" )
					illthink = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/35illthink.ogg","stream" )
					hmmm = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/36hmm.ogg","stream" )
					breads = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/37breads.ogg","stream" )
					todays = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/38todays.ogg","stream" )
					awarriorwith = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/39awarriorwith.ogg","stream" )
					youyoungsters = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/40youyoungsters.ogg","stream" )
					wellistill = love.audio.newSource( "/externalassets/dialogs/level17/en/vikings/41wellistill.ogg","stream" )
			
				if accent=="br" then
				
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/en/2definitely.ogg","stream" )
					ortothefather = love.audio.newSource( "/externalassets/dialogs/level17/en/3ortothefather.ogg","stream" )
					ormaybeto = love.audio.newSource( "/externalassets/dialogs/level17/en/4ormaybeto.ogg","stream" )
					itcould = love.audio.newSource( "/externalassets/dialogs/level17/en/5itcould.ogg","stream" )
					orpossibly = love.audio.newSource( "/externalassets/dialogs/level17/en/6orpossibly.ogg","stream" )
					itseven = love.audio.newSource( "/externalassets/dialogs/level17/en/7itseven.ogg","stream" )
					foradog = love.audio.newSource( "/externalassets/dialogs/level17/en/11foradog.ogg","stream" )
					hedoesntlook = love.audio.newSource( "/externalassets/dialogs/level17/en/12hedoesntlook.ogg","stream" )

				elseif accent=="us" then
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/en-us/2definitely.ogg","stream" )
					ortothefather = love.audio.newSource( "/externalassets/dialogs/level17/en-us/3ortothefather.ogg","stream" )
					ormaybeto = love.audio.newSource( "/externalassets/dialogs/level17/en-us/4ormaybeto.ogg","stream" )
					itcould = love.audio.newSource( "/externalassets/dialogs/level17/en-us/5itcould.ogg","stream" )
					orpossibly = love.audio.newSource( "/externalassets/dialogs/level17/en-us/6orpossibly.ogg","stream" )
					itseven = love.audio.newSource( "/externalassets/dialogs/level17/en-us/7itseven.ogg","stream" )
					foradog = love.audio.newSource( "/externalassets/dialogs/level17/en-us/11foradog.ogg","stream" )
					hedoesntlook = love.audio.newSource( "/externalassets/dialogs/level17/en-us/12hedoesntlook.ogg","stream" )

				end
				elseif language=="fr" then
							--vikings
					shutup = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/1shutup.ogg","stream" )
					willyou = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/2willyou.ogg","stream" )
					soyouarenot = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/3soyouarenot.ogg","stream" )
					argh = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/4argh.ogg","stream" )
					hehehe = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/5hehehe.ogg","stream" )
					isitover = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/6isitover.ogg","stream" )
					arewesafe = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/7arewesafe.ogg","stream" )
					isthefight = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/8isthefight.ogg","stream" )
					arewestill = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/10arewestill.ogg","stream" )
					areweinvalhalla = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/11areweinvalhalla.ogg","stream" )
					whenare = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/12whenare.ogg","stream" )
					illletyou = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/13illletyou.ogg","stream" )
					canigo = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/14canigo.ogg","stream" )
					isntthis = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/15isntthis.ogg","stream" )
					patience = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/16patience.ogg","stream" )
					areyousure = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/17areyousure.ogg","stream" )
					awarrior = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/18awarrior.ogg","stream" )
					imincharge = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/19imincharge.ogg","stream" )
					whydid = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/20whydid.ogg","stream" )
					laughing = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/21laughing.ogg","stream" )
					realvikings = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/22realvikings.ogg","stream" )
					braids = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/23braids.ogg","stream" )
					whatkind = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/24whatkind.ogg","stream" )
					vikingfashion = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/25vikingfashion.ogg","stream" )
					isimply = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/26isimply.ogg","stream" )
					youshould = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/27youshould.ogg","stream" )
					butimcool = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/28butimcool.ogg","stream" )
					evenerik = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/29evenerik.ogg","stream" )
					nonsense = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/30nonsense.ogg","stream" )
					buthealso = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/31buthealso.ogg","stream" )
					hedidnot = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/32hedidnot.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/33doyouthink.ogg","stream" )
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/34definitely.ogg","stream" )
					illthink = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/35illthink.ogg","stream" )
					hmmm = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/36hmm.ogg","stream" )
					breads = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/37breads.ogg","stream" )
					todays = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/38todays.ogg","stream" )
					awarriorwith = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/39awarriorwith.ogg","stream" )
					youyoungsters = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/40youyoungsters.ogg","stream" )
					wellistill = love.audio.newSource( "/externalassets/dialogs/level17/fr/vikings/41wellistill.ogg","stream" )
					

					
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/fr/2definitely.ogg","stream" )
					ortothefather = love.audio.newSource( "/externalassets/dialogs/level17/fr/3ortothefather.ogg","stream" )
					ormaybeto = love.audio.newSource( "/externalassets/dialogs/level17/fr/4ormaybeto.ogg","stream" )
					itcould = love.audio.newSource( "/externalassets/dialogs/level17/fr/5itcould.ogg","stream" )
					orpossibly = love.audio.newSource( "/externalassets/dialogs/level17/fr/6orpossibly.ogg","stream" )
					itseven = love.audio.newSource( "/externalassets/dialogs/level17/fr/7itseven.ogg","stream" )
					foradog = love.audio.newSource( "/externalassets/dialogs/level17/fr/11foradog.ogg","stream" )
					hedoesntlook = love.audio.newSource( "/externalassets/dialogs/level17/fr/12hedoesntlook.ogg","stream" )
			
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/es-la/2definitely.ogg","stream" )
					ortothefather = love.audio.newSource( "/externalassets/dialogs/level17/es-la/3ortothefather.ogg","stream" )
					ormaybeto = love.audio.newSource( "/externalassets/dialogs/level17/es-la/4ormaybeto.ogg","stream" )
					itcould = love.audio.newSource( "/externalassets/dialogs/level17/es-la/5itcould.ogg","stream" )
					orpossibly = love.audio.newSource( "/externalassets/dialogs/level17/es-la/6orpossibly.ogg","stream" )
					itseven = love.audio.newSource( "/externalassets/dialogs/level17/es-la/7itseven.ogg","stream" )
					foradog = love.audio.newSource( "/externalassets/dialogs/level17/es-la/11foradog.ogg","stream" )
					hedoesntlook = love.audio.newSource( "/externalassets/dialogs/level17/es-la/12hedoesntlook.ogg","stream" )
				end
			
			elseif language=="nl" then
						
					definitely = love.audio.newSource( "/externalassets/dialogs/level17/nl/2definitely.ogg","stream" )
					ortothefather = love.audio.newSource( "/externalassets/dialogs/level17/nl/3ortothefather.ogg","stream" )
					ormaybeto = love.audio.newSource( "/externalassets/dialogs/level17/nl/4ormaybeto.ogg","stream" )
					itcould = love.audio.newSource( "/externalassets/dialogs/level17/nl/5itcould.ogg","stream" )
					orpossibly = love.audio.newSource( "/externalassets/dialogs/level17/nl/6orpossibly.ogg","stream" )
					itseven = love.audio.newSource( "/externalassets/dialogs/level17/nl/7itseven.ogg","stream" )
					foradog = love.audio.newSource( "/externalassets/dialogs/level17/nl/11foradog.ogg","stream" )
					hedoesntlook = love.audio.newSource( "/externalassets/dialogs/level17/nl/12hedoesntlook.ogg","stream" )
			
			end
					
				
			if language2=="en" then
				if accent2=="br" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/en/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/en/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/en/10doesnthe.ogg","stream" )

					
				elseif accent2=="us" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/en-us/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/en-us/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/en-us/10doesnthe.ogg","stream" )

				end
				
		
					
			elseif language2=="pl" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/pl/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/pl/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/pl/10doesnthe.ogg","stream" )
			
			elseif language2=="cs" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/cs/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/cs/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/cs/10doesnthe.ogg","stream" )
			
			elseif language2=="nl" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/nl/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/nl/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/nl/10doesnthe.ogg","stream" )
		
			elseif language2=="fr" then
					thisship = love.audio.newSource( "/externalassets/dialogs/level17/fr/1thisship.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level17/fr/9lookatthat.ogg","stream" )
					doesnthe = love.audio.newSource( "/externalassets/dialogs/level17/fr/10doesnthe.ogg","stream" )
					
			end
			
				loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
					
					--vikings effects
					shutup:setEffect('myEffect')
					willyou:setEffect('myEffect')
					soyouarenot:setEffect('myEffect')
					argh:setEffect('myEffect')
					hehehe:setEffect('myEffect')
					isitover:setEffect('myEffect')
					arewesafe:setEffect('myEffect')
					isthefight:setEffect('myEffect')
					arewestill:setEffect('myEffect')
					areweinvalhalla:setEffect('myEffect')
					whenare:setEffect('myEffect')
					illletyou:setEffect('myEffect')
					canigo:setEffect('myEffect')
					isntthis:setEffect('myEffect')
					patience:setEffect('myEffect')
					areyousure:setEffect('myEffect')
					awarrior:setEffect('myEffect')
					imincharge:setEffect('myEffect')
					whydid:setEffect('myEffect')
					laughing:setEffect('myEffect')
					realvikings:setEffect('myEffect')
					braids:setEffect('myEffect')
					whatkind:setEffect('myEffect')
					vikingfashion:setEffect('myEffect')
					isimply:setEffect('myEffect')
					youshould:setEffect('myEffect')
					butimcool:setEffect('myEffect')
					evenerik:setEffect('myEffect')
					nonsense:setEffect('myEffect')
					buthealso:setEffect('myEffect')
					hedidnot:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					definitely:setEffect('myEffect')
					illthink:setEffect('myEffect')
					hmmm:setEffect('myEffect')
					breads:setEffect('myEffect')
					todays:setEffect('myEffect')
					awarriorwith:setEffect('myEffect')
					youyoungsters:setEffect('myEffect')
					wellistill:setEffect('myEffect')
					
					--fish 1 effects
					definitely:setEffect('myEffect')
					ortothefather:setEffect('myEffect')
					ormaybeto:setEffect('myEffect')
					itcould:setEffect('myEffect')
					orpossibly:setEffect('myEffect')
					itseven:setEffect('myEffect')
					foradog:setEffect('myEffect')
					hedoesntlook:setEffect('myEffect')
			
			
						
					thisship:setEffect('myEffect')
					lookatthat:setEffect('myEffect')
					doesnthe:setEffect('myEffect')
					
					
		
		elseif nLevel==18 then
		
			if language=="en" then
				if accent=="br" then
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level18/en/2canyouseeit.ogg","stream" )
					itellyou = love.audio.newSource( "/externalassets/dialogs/level18/en/2-2itellyou.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level18/en/7ithink.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level18/en/8yes.ogg","stream" )
					ithinktheycannot = love.audio.newSource( "/externalassets/dialogs/level18/en/9ithinktheycannot.ogg","stream" )
					ithinktheyare = love.audio.newSource( "/externalassets/dialogs/level18/en/10ithinktheyare.ogg","stream" )
					itwontbethat = love.audio.newSource( "/externalassets/dialogs/level18/en/11itwontbethat.ogg","stream" )
					itseemsitsgoing = love.audio.newSource( "/externalassets/dialogs/level18/en/12itseemsitsgoing.ogg","stream" )
	
				elseif accent=="us" then
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level18/en-us/2canyouseeit.ogg","stream" )
					itellyou = love.audio.newSource( "/externalassets/dialogs/level18/en-us/2-2itellyou.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level18/en-us/7ithink.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level18/en-us/8yes.ogg","stream" )
					ithinktheycannot = love.audio.newSource( "/externalassets/dialogs/level18/en-us/9ithinktheycannot.ogg","stream" )
					ithinktheyare = love.audio.newSource( "/externalassets/dialogs/level18/en-us/10ithinktheyare.ogg","stream" )
					itwontbethat = love.audio.newSource( "/externalassets/dialogs/level18/en-us/11itwontbethat.ogg","stream" )
					itseemsitsgoing = love.audio.newSource( "/externalassets/dialogs/level18/en-us/12itseemsitsgoing.ogg","stream" )
				end
			elseif language=="fr" then
				canyouseeit = love.audio.newSource( "/externalassets/dialogs/level18/fr/2canyouseeit.ogg","stream" )
				itellyou = love.audio.newSource( "/externalassets/dialogs/level18/fr/2-2itellyou.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level18/fr/7ithink.ogg","stream" )
				yes = love.audio.newSource( "/externalassets/dialogs/level18/fr/8yes.ogg","stream" )
				ithinktheycannot = love.audio.newSource( "/externalassets/dialogs/level18/fr/9ithinktheycannot.ogg","stream" )
				ithinktheyare = love.audio.newSource( "/externalassets/dialogs/level18/fr/10ithinktheyare.ogg","stream" )
				itwontbethat = love.audio.newSource( "/externalassets/dialogs/level18/fr/11itwontbethat.ogg","stream" )
				itseemsitsgoing = love.audio.newSource( "/externalassets/dialogs/level18/fr/12itseemsitsgoing.ogg","stream" )
			
			elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level18/es-la/2canyouseeit.ogg","stream" )
					itellyou = love.audio.newSource( "/externalassets/dialogs/level18/es-la/2-2itellyou.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level18/es-la/7ithink.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level18/es-la/8yes.ogg","stream" )
					ithinktheycannot = love.audio.newSource( "/externalassets/dialogs/level18/es-la/9ithinktheycannot.ogg","stream" )
					ithinktheyare = love.audio.newSource( "/externalassets/dialogs/level18/es-la/10ithinktheyare.ogg","stream" )
					itwontbethat = love.audio.newSource( "/externalassets/dialogs/level18/es-la/11itwontbethat.ogg","stream" )
					itseemsitsgoing = love.audio.newSource( "/externalassets/dialogs/level18/es-la/12itseemsitsgoing.ogg","stream" )
				end
			
			elseif language=="nl" then
				canyouseeit = love.audio.newSource( "/externalassets/dialogs/level18/nl/2canyouseeit.ogg","stream" )
				itellyou = love.audio.newSource( "/externalassets/dialogs/level18/nl/2-2itellyou.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level18/nl/7ithink.ogg","stream" )
				yes = love.audio.newSource( "/externalassets/dialogs/level18/nl/8yes.ogg","stream" )
				ithinktheycannot = love.audio.newSource( "/externalassets/dialogs/level18/nl/9ithinktheycannot.ogg","stream" )
				ithinktheyare = love.audio.newSource( "/externalassets/dialogs/level18/nl/10ithinktheyare.ogg","stream" )
				itwontbethat = love.audio.newSource( "/externalassets/dialogs/level18/nl/11itwontbethat.ogg","stream" )
				itseemsitsgoing = love.audio.newSource( "/externalassets/dialogs/level18/nl/12itseemsitsgoing.ogg","stream" )
			end
			
			if language2=="en" then
			
				if accent2=="br" then
					thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/en/1thatghostly.ogg","stream" )
					whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/en/3whydoyou.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level18/en/4itlooks.ogg","stream" )
					thatsall = love.audio.newSource( "/externalassets/dialogs/level18/en/5thatsall.ogg","stream" )
					theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/en/6theyhavehad.ogg","stream" )

				elseif accent2=="us" then
					thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/en-us/1thatghostly.ogg","stream" )
					whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/en-us/3whydoyou.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level18/en-us/4itlooks.ogg","stream" )
					thatsall = love.audio.newSource( "/externalassets/dialogs/level18/en-us/5thatsall.ogg","stream" )
					theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/en-us/6theyhavehad.ogg","stream" )
				end
							
		elseif language2=="pl" then
				thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/pl/1thatghostly.ogg","stream" )
				whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/pl/3whydoyou.ogg","stream" )
				itlooks = love.audio.newSource( "/externalassets/dialogs/level18/pl/4itlooks.ogg","stream" )
				thatsall = love.audio.newSource( "/externalassets/dialogs/level18/pl/5thatsall.ogg","stream" )
				theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/pl/6theyhavehad.ogg","stream" )
		
		elseif language2=="cs" then
				thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/cs/1thatghostly.ogg","stream" )
				whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/cs/3whydoyou.ogg","stream" )
				itlooks = love.audio.newSource( "/externalassets/dialogs/level18/cs/4itlooks.ogg","stream" )
				thatsall = love.audio.newSource( "/externalassets/dialogs/level18/cs/5thatsall.ogg","stream" )
				theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/cs/6theyhavehad.ogg","stream" )		
		
		elseif language2=="nl" then
				thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/nl/1thatghostly.ogg","stream" )
				whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/nl/3whydoyou.ogg","stream" )
				itlooks = love.audio.newSource( "/externalassets/dialogs/level18/nl/4itlooks.ogg","stream" )
				thatsall = love.audio.newSource( "/externalassets/dialogs/level18/nl/5thatsall.ogg","stream" )
				theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/nl/6theyhavehad.ogg","stream" )
				
		elseif language2=="fr" then
				thatghostly = love.audio.newSource( "/externalassets/dialogs/level18/fr/1thatghostly.ogg","stream" )
				whydoyou = love.audio.newSource( "/externalassets/dialogs/level18/fr/3whydoyou.ogg","stream" )
				itlooks = love.audio.newSource( "/externalassets/dialogs/level18/fr/4itlooks.ogg","stream" )
				thatsall = love.audio.newSource( "/externalassets/dialogs/level18/fr/5thatsall.ogg","stream" )
				theyhavehad = love.audio.newSource( "/externalassets/dialogs/level18/fr/6theyhavehad.ogg","stream" )
		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
				--fish 1
				canyouseeit:setEffect('myEffect')
				itellyou:setEffect('myEffect')
				ithink:setEffect('myEffect')
				yes:setEffect('myEffect')
				ithinktheycannot:setEffect('myEffect')
				ithinktheyare:setEffect('myEffect')
				itwontbethat:setEffect('myEffect')
				itseemsitsgoing:setEffect('myEffect')
				
				--fish 2
				thatghostly:setEffect('myEffect')
				whydoyou:setEffect('myEffect')
				itlooks:setEffect('myEffect')
				thatsall:setEffect('myEffect')
				theyhavehad:setEffect('myEffect')
		
		elseif nLevel==19 then
					
			loadBorderDialogs()	-- load border dialogs
		--gods
		
			a1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/15a1.ogg","stream" )
			b1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/16b1.ogg","stream" )
			c1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/17c1.ogg","stream" )
			d1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/18d1.ogg","stream" )
			e1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/19e1.ogg","stream" )
			f1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/20f1.ogg","stream" )
			g1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/21g1.ogg","stream" )
			h1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/22h1.ogg","stream" )
			i1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/23i1.ogg","stream" )
			g1 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/24g1.ogg","stream" )
		
		if language=="en" then
			--gods
			miss = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/sank3.ogg","stream" )
			

			istart = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/14ivetriedthat.ogg","stream" )
			
			--end
			occupation = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/1occupation.ogg","stream" )
			exceptofthe = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/2exceptofthe.ogg","stream" )
			heisresponsible = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/3heisresponsible.ogg","stream" )
			movingthecontinents = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/4movingthecontinents.ogg","stream" )
			andmeteorite = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/5andmeteorite.ogg","stream" )
			wemanagedto = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/6wemanagedto.ogg","stream" )
			wehavefound = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/7wehavefound.ogg","stream" )
			youcanfindtherecords = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/8youcanfindtherecords.ogg","stream" )
		
			if accent=="br" then
			
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/en/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/en/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/en/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/en/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/en/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/en/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/en/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/en/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/en/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/en/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/en/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/en/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/en/26itshardtosay.ogg","stream" )
			
			
			elseif accent=="us" then
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/en-us/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/en-us/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/en-us/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/en-us/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/en-us/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/en-us/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/en-us/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/en-us/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/en-us/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/en-us/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/en-us/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/en-us/26itshardtosay.ogg","stream" )
						
			end
		elseif language=="fr" then
				--end
					occupation = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/1occupation.ogg","stream" )
					exceptofthe = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/2exceptofthe.ogg","stream" )
					heisresponsible = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/3heisresponsible.ogg","stream" )
					movingthecontinents = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/4movingthecontinents.ogg","stream" )
					andmeteorite = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/5andmeteorite.ogg","stream" )
					wemanagedto = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/6wemanagedto.ogg","stream" )
					wehavefound = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/7wehavefound.ogg","stream" )
					youcanfindtherecords = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/8youcanfindtherecords.ogg","stream" )
		
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/fr/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/fr/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/fr/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/fr/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/fr/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/fr/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/fr/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/fr/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/fr/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/fr/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/fr/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/fr/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/fr/26itshardtosay.ogg","stream" )
				
				math.randomseed(os.time())
				aleatorygod = math.random(0,1)
				
			if aleatorygod==0 then
				--gods sechelinge2000 part 1
		
			miss = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/sank3.ogg","stream" )
			
			--gods Avos part 2

			istart = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/14ivetriedthat.ogg","stream" )
			
			elseif aleatorygod==1 then
				
				--gods Avos part 1
		
			miss = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/avos/sank3.ogg","stream" )
			
				--gods sechelinge2000 part 2

			istart = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/gods/sechelinge2000/14ivetriedthat.ogg","stream" )
			
			end
				
		elseif language=="sv" then
		
			--end
			occupation = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/1occupation.ogg","stream" )
			exceptofthe = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/2exceptofthe.ogg","stream" )
			heisresponsible = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/3heisresponsible.ogg","stream" )
			movingthecontinents = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/4movingthecontinents.ogg","stream" )
			andmeteorite = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/5andmeteorite.ogg","stream" )
			wemanagedto = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/6wemanagedto.ogg","stream" )
			wehavefound = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/7wehavefound.ogg","stream" )
			youcanfindtherecords = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/8youcanfindtherecords.ogg","stream" )
		
			--gods
			
			miss = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/miss4.ogg","stream" )
			--miss5 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/.ogg","stream" )
			hit = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/sank3.ogg","stream" )
			--iwonyouseaslug = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/.ogg","stream" )

			istart = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/12itcantbeamiss.ogg","stream" )
			--youalreadysaid = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/.ogg","stream" )
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/sv/gods/14ivetriedthat.ogg","stream" )
		
				--fish 1
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/en/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/en/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/en/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/en/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/en/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/en/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/en/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/en/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/en/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/en/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/en/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/en/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/en/26itshardtosay.ogg","stream" )
		
		elseif language=="es" then
		
			--end
			occupation = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/1occupation.ogg","stream" )
			exceptofthe = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/2exceptofthe.ogg","stream" )
			heisresponsible = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/3heisresponsible.ogg","stream" )
			movingthecontinents = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/4movingthecontinents.ogg","stream" )
			andmeteorite = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/5andmeteorite.ogg","stream" )
			wemanagedto = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/6wemanagedto.ogg","stream" )
			wehavefound = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/7wehavefound.ogg","stream" )
			youcanfindtherecords = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/8youcanfindtherecords.ogg","stream" )
		
			--gods
		
			miss = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/sank3.ogg","stream" )
			

			istart = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/es-la/gods/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/en/gods/14ivetriedthat.ogg","stream" )
			
				
		
				if accent=="es" then
			elseif accent=="la" then
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/es-la/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/es-la/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/es-la/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/es-la/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/es-la/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/es-la/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/es-la/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/es-la/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/es-la/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/es-la/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/es-la/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/es-la/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/es-la/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/es-la/26itshardtosay.ogg","stream" )
				
				end
		elseif language=="pl" then		
		
		--[[--gods
		
			miss = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank3.ogg","stream" )
			

			istart = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/14ivetriedthat.ogg","stream" )
		--]]
			--end
			occupation = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/1occupation.ogg","stream" )
			exceptofthe = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/2exceptofthe.ogg","stream" )
			heisresponsible = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/3heisresponsible.ogg","stream" )
			movingthecontinents = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/4movingthecontinents.ogg","stream" )
			andmeteorite = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/5andmeteorite.ogg","stream" )
			wemanagedto = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/6wemanagedto.ogg","stream" )
			wehavefound = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/7wehavefound.ogg","stream" )
			youcanfindtherecords = love.audio.newSource( "/externalassets/ends/1shipwrecks/audios/en/8youcanfindtherecords.ogg","stream" )
		
				--fish 1
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/en/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/en/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/en/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/en/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/en/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/en/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/en/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/en/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/en/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/en/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/en/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/en/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/en/26itshardtosay.ogg","stream" )
				
		elseif language=="de" then
			miss = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/sank3.ogg","stream" )
			

			istart = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/5itwasamistake.ogg","stream" )
		
			heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/7heheiwon.ogg","stream" )
			youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/10youalreadytried.ogg","stream" )
			youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/11youalreadytriedthat.ogg","stream" )
			itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/12itcantbeamiss.ogg","stream" )
			
			youcheat = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/13youcheat.ogg","stream" )
			ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/de/gods/14ivetriedthat.ogg","stream" )
		
		
				--fish 1
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/en/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/en/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/en/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/en/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/en/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/en/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/en/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/en/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/en/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/en/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/en/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/en/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/en/26itshardtosay.ogg","stream" )
		
		elseif language=="nl" then
	
	
				ihaveasuspicion = love.audio.newSource( "/externalassets/dialogs/level19/nl/1ihaveasuspicion.ogg","stream" )
				ialwaysknewthat = love.audio.newSource( "/externalassets/dialogs/level19/nl/2ialwaysknewthat.ogg","stream" )
				godismad = love.audio.newSource( "/externalassets/dialogs/level19/nl/3godismad.ogg","stream" )
				wecantleave = love.audio.newSource( "/externalassets/dialogs/level19/nl/8wecantleave.ogg","stream" )
				puttheminto = love.audio.newSource( "/externalassets/dialogs/level19/nl/10putheminto.ogg","stream" )
				ofcourse = love.audio.newSource( "/externalassets/dialogs/level19/nl/12ofcourse.ogg","stream" )
				wellwhichone = love.audio.newSource( "/externalassets/dialogs/level19/nl/14wellwhichone.ogg","stream" )
				thatyellowone = love.audio.newSource( "/externalassets/dialogs/level19/nl/16thatyellow.ogg","stream" )
				weshallleave = love.audio.newSource( "/externalassets/dialogs/level19/nl/18weshallleave.ogg","stream" )
				lookatthatclub = love.audio.newSource( "/externalassets/dialogs/level19/nl/19lookatthatclub.ogg","stream" )
				motherwasright = love.audio.newSource( "/externalassets/dialogs/level19/nl/20motherwasright.ogg","stream" )
				bytheway = love.audio.newSource( "/externalassets/dialogs/level19/nl/22bytheway.ogg","stream" )
				whoknows = love.audio.newSource( "/externalassets/dialogs/level19/nl/24whoknows.ogg","stream" )
				itshardtosay = love.audio.newSource( "/externalassets/dialogs/level19/nl/26itshardtosay.ogg","stream" )
		
		end
		if language2=="en" then
			if accent2=="br" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/en/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/en/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/en/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/en/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/en/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/en/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/en/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/en/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/en/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/en/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/en/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/en/27thistime.ogg","stream" )
				
			elseif accent2=="us" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/en-us/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/en-us/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/en-us/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/en-us/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/en-us/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/en-us/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/en-us/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/en-us/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/en-us/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/en-us/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/en-us/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/en-us/27thistime.ogg","stream" )
		
			end
		
			
		elseif language2=="pl" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/pl/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/pl/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/pl/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/pl/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/pl/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/pl/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/pl/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/pl/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/pl/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/pl/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/pl/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/pl/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/pl/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/pl/27thistime.ogg","stream" )
		
		elseif language2=="cs" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/cs/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/cs/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/cs/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/cs/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/cs/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/cs/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/cs/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/cs/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/cs/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/cs/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/cs/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/cs/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/cs/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/cs/27thistime.ogg","stream" )	
				--gods
		
			miss = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss.ogg","stream" )
			miss2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss2.ogg","stream" )
			miss3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss3.ogg","stream" )
			miss4 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/miss4.ogg","stream" )
			
			hit = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit.ogg","stream" )
			hit2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit2.ogg","stream" )
			hit3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit3.ogg","stream" )
			hit4 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/hit4.ogg","stream" )
			sank = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank.ogg","stream" )
			sank2 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank2.ogg","stream" )
			sank3 = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/sank3.ogg","stream" )
			

			istart = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/1istart.ogg","stream" )
			well = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/2well.ogg","stream" )
			shallwe = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/3shallwe.ogg","stream" )
			idontcheat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/4idontcheat.ogg","stream" )
			itwasamistake = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/5itwasamistake.ogg","stream" )
		
			--heheiwon = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/7heheiwon.ogg","stream" )
			--youalreadytried = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/10youalreadytried.ogg","stream" )
			--youalreadytriedthat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/11youalreadytriedthat.ogg","stream" )
			--itcantbe = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/12itcantbeamiss.ogg","stream" )
			
			--youcheat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/13youcheat.ogg","stream" )
			--ivetriedthat = love.audio.newSource( "/externalassets/dialogs/level19/pl/gods/14ivetriedthat.ogg","stream" )
				
		elseif language2=="nl" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/nl/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/nl/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/nl/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/nl/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/nl/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/nl/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/nl/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/nl/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/nl/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/nl/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/nl/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/nl/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/nl/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/nl/27thistime.ogg","stream" )
		elseif language2=="es" then
		
				if accent2=="es" then
			elseif accent2=="la" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/en/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/en/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/en/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/en/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/en/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/en/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/en/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/en/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/en/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/en/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/en/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/en/27thistime.ogg","stream" )
			end
		elseif language2=="fr" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/fr/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/fr/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/fr/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/fr/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/fr/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/fr/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/fr/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/fr/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/fr/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/fr/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/fr/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/fr/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/fr/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/fr/27thistime.ogg","stream" )
		elseif language2=="de" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/en/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/en/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/en/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/en/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/en/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/en/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/en/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/en/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/en/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/en/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/en/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/en/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/en/27thistime.ogg","stream" )
		elseif language2=="sv" then
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level19/en-us/4ithinkthat.ogg","stream" )
				imafraid = love.audio.newSource( "/externalassets/dialogs/level19/en-us/5imafraid.ogg","stream" )
				yesitsschocking = love.audio.newSource( "/externalassets/dialogs/level19/en-us/6yesitsschocking.ogg","stream" )
				whatarewe = love.audio.newSource( "/externalassets/dialogs/level19/en-us/7whatarewe.ogg","stream" )
				whatmustwedo = love.audio.newSource( "/externalassets/dialogs/level19/en-us/9whatmustwedo.ogg","stream" )
				youareright = love.audio.newSource( "/externalassets/dialogs/level19/en-us/11youareright.ogg","stream" )
				hardlywith = love.audio.newSource( "/externalassets/dialogs/level19/en-us/13hardlywith.ogg","stream" )
				theyellowone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/15theyellowone.ogg","stream" )
				theblueone = love.audio.newSource( "/externalassets/dialogs/level19/en-us/15theblueone.ogg","stream" )
				wewillleavethis = love.audio.newSource( "/externalassets/dialogs/level19/en-us/17wewillleavethis.ogg","stream" )
				thatstrue = love.audio.newSource( "/externalassets/dialogs/level19/en-us/21thatstrue.ogg","stream" )
				isthere = love.audio.newSource( "/externalassets/dialogs/level19/en-us/23isthere.ogg","stream" )
				ithought = love.audio.newSource( "/externalassets/dialogs/level19/en-us/25ithought.ogg","stream" )
				thistime = love.audio.newSource( "/externalassets/dialogs/level19/en-us/27thistime.ogg","stream" )
		end
		
		loadvoiceexceptions()			--exceptions to prevent game from crashing due to missing dubs
		
				--end
				occupation:setEffect('myEffect')
				exceptofthe:setEffect('myEffect')
				heisresponsible:setEffect('myEffect')
				movingthecontinents:setEffect('myEffect')
				andmeteorite:setEffect('myEffect')
				wemanagedto:setEffect('myEffect')
				wehavefound:setEffect('myEffect')
				youcanfindtherecords:setEffect('myEffect')
					
				--fish 1
				ihaveasuspicion:setEffect('myEffect')
				ialwaysknewthat:setEffect('myEffect')
				godismad:setEffect('myEffect')
				wecantleave:setEffect('myEffect')
				puttheminto:setEffect('myEffect')
				ofcourse:setEffect('myEffect')
				wellwhichone:setEffect('myEffect')
				thatyellowone:setEffect('myEffect')
				weshallleave:setEffect('myEffect')
				lookatthatclub:setEffect('myEffect')
				motherwasright:setEffect('myEffect')
				bytheway:setEffect('myEffect')
				whoknows:setEffect('myEffect')
				itshardtosay:setEffect('myEffect')
				
				--fish 2
				ithinkthat:setEffect('myEffect')
				imafraid:setEffect('myEffect')
				yesitsschocking:setEffect('myEffect')
				whatarewe:setEffect('myEffect')
				whatmustwedo:setEffect('myEffect')
				youareright:setEffect('myEffect')
				hardlywith:setEffect('myEffect')
				theyellowone:setEffect('myEffect')
				wewillleavethis:setEffect('myEffect')
				thatstrue:setEffect('myEffect')
				isthere:setEffect('myEffect')
				ithought:setEffect('myEffect')
				thistime:setEffect('myEffect')
				
				--gods
				
				istart:setEffect('myEffect')
				well:setEffect('myEffect')
				shallwe:setEffect('myEffect')
				idontcheat:setEffect('myEffect')
				itwasamistake:setEffect('myEffect')
			
				heheiwon:setEffect('myEffect')
				youalreadytried:setEffect('myEffect')
				youalreadytriedthat:setEffect('myEffect')
				--youalreadysaid:setEffect('myEffect')
				itcantbe:setEffect('myEffect')
				youcheat:setEffect('myEffect')
				ivetriedthat:setEffect('myEffect')
			
				a1:setEffect('myEffect')
				b1:setEffect('myEffect')
				c1:setEffect('myEffect')
				d1:setEffect('myEffect')
				e1:setEffect('myEffect')
				f1:setEffect('myEffect')
				g1:setEffect('myEffect')
				h1:setEffect('myEffect')
				i1:setEffect('myEffect')
				g1:setEffect('myEffect')
			
			
		
	
		
		
		
	
		
		elseif nLevel>19 and nLevel<40 then loadvoices20()
		elseif nLevel>39 and nLevel<60 then loadvoices40()
		elseif nLevel>59 and nLevel<80 then loadvoices60()
		end
end

function loadBorderDialogs()
				
		if language=="en" then		
			if accent=="br" then
					
			--border dialogs
			IThink = love.audio.newSource( "/externalassets/dialogs/level19/en/border/1ithink.ogg","stream" )
			Wedidnt  = love.audio.newSource( "/externalassets/dialogs/level19/en/border/2wedidnt.ogg","stream" )
			Theagencyrely  = love.audio.newSource( "/externalassets/dialogs/level19/en/border/3theagency.ogg","stream" )
			Certainlyweare  = love.audio.newSource( "/externalassets/dialogs/level19/en/border/4certainly.ogg","stream" )
			
			elseif accent=="us" then
							
			--border dialogs
			IThink = love.audio.newSource	( "/externalassets/dialogs/share/en-us/border/1ithink.ogg","stream" )
			Wedidnt  = love.audio.newSource	( "/externalassets/dialogs/share/en-us/border/2wedidnt.ogg","stream" )
			Theagencyrely  = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/3theagency.ogg","stream" )
			Certainlyweare  = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/4certainly.ogg","stream" )
			
			end
		elseif language=="fr" then
								
						--border dialogs
			IThink = love.audio.newSource( "/externalassets/dialogs/share/fr/border/1ithink.ogg","stream" )
			Wedidnt  = love.audio.newSource( "/externalassets/dialogs/share/fr/border/2wedidnt.ogg","stream" )
			Theagencyrely  = love.audio.newSource( "/externalassets/dialogs/share/fr/border/3theagency.ogg","stream" )
			Certainlyweare  = love.audio.newSource( "/externalassets/dialogs/share/fr/border/4certainly.ogg","stream" )
					
		elseif language=="es" then
			if accent=="la" then
							
					--border dialogs
				IThink = love.audio.newSource( "/externalassets/dialogs/share/es-la/border/1ithink.ogg","stream" )
				Wedidnt  = love.audio.newSource( "/externalassets/dialogs/share/es-la/border/2wedidnt.ogg","stream" )
				Theagencyrely  = love.audio.newSource( "/externalassets/dialogs/share/es-la/border/3theagency.ogg","stream" )
				Certainlyweare  = love.audio.newSource( "/externalassets/dialogs/share/es-la/border/4certainly.ogg","stream" )
				
			end
		end
		
		if language2=="en" then
			if accent2=="br" then				
			elseif accent2=="us" then
				
				--border dialogs
			IThink2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/2Ithinkwe.ogg","stream" )
			Wedidnt2  = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/4wedidntfulfill.ogg","stream" )
			Theagencyrely2  = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/5theagency.ogg","stream" )
			Certainlyweare2  = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/8certainly.ogg","stream" )
				
			end
		
		end

end
