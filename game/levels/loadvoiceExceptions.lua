--exceptions to prevent game from crashing due to missing dubs

function ifFirstDubIsmissing()
					--retrieve current language and accent
					templanguage=language 
					tempaccent=accent
					
					--temporarily assign english
					language="en" accent="us"
					
					loadvoices()
					
					--restore language and accent
					language=templanguage
					accent=tempaccent
					--language2=templanguage2
					--accent2=tempaccent2
end

function ifSecondDubIsmissing()
				
					--retrieve current language2 and accent2
					templanguage2=language2
					tempaccent2=accent2
					
					--temporarily assign english
					language2="en" accent2="us"
					
					loadvoices()
					
					--restore language and accent
					--language=templanguage
					--accent=tempaccent
					language2=templanguage2
					accent2=tempaccent2

end

function loadvoiceexceptions()

		if nLevel==1 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	
	elseif nLevel==2 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end

	elseif nLevel==3 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==4 then
				if these==nil then ifFirstDubIsmissing() end
				if look1==nil then ifSecondDubIsmissing() end
	elseif nLevel==5 then
				if itsome==nil then ifFirstDubIsmissing() end
				if something==nil then ifSecondDubIsmissing() end
	elseif nLevel==6 then
				if atleast==nil then ifFirstDubIsmissing() end
				if itsbeen==nil then ifSecondDubIsmissing() end
	elseif nLevel==7 then
				if thisisnot==nil then ifFirstDubIsmissing() end
				if mygod==nil then ifSecondDubIsmissing() end
	elseif nLevel==8 then
				if thetoilet==nil then ifFirstDubIsmissing() end
				if david==nil then ifSecondDubIsmissing() end
	elseif nLevel==9 then
				if heyisanybody==nil then ifFirstDubIsmissing() end
				if stopmakingfaces==nil then ifSecondDubIsmissing() end
				
				--10
	elseif nLevel==10 then
				if ihaveafeeling==nil then ifFirstDubIsmissing() end
				if lookthepartyboat==nil then ifSecondDubIsmissing() end
	elseif nLevel==11 then
				if thehostility==nil then ifFirstDubIsmissing() end
				if thewar==nil then ifSecondDubIsmissing() end
	elseif nLevel==12 then
				if could==nil 	 then ifFirstDubIsmissing() end
				if whatmakesyou==nil then ifSecondDubIsmissing() end
	elseif nLevel==13 then
				if sure==nil then ifFirstDubIsmissing() end
				if thismust==nil then ifSecondDubIsmissing() end
	elseif nLevel==14 then
				if thisis1==nil then ifFirstDubIsmissing() end
				if whatkind==nil then ifSecondDubIsmissing() end
				
	elseif nLevel==15 then
				if shouldnt==nil then ifFirstDubIsmissing() end
				if sothis==nil then ifSecondDubIsmissing() end
	elseif nLevel==16 then
				if atleast==nil then ifFirstDubIsmissing() end
				if thisissome==nil then ifSecondDubIsmissing() end
	elseif nLevel==17 then
				if definitely==nil then ifFirstDubIsmissing() end
				if thisship==nil then ifSecondDubIsmissing() end
	elseif nLevel==18 then
				if canyouseeit==nil then ifFirstDubIsmissing() end
				if thatghostly==nil then ifSecondDubIsmissing() end
	elseif nLevel==19 then
				if ihaveasuspicion==nil then ifFirstDubIsmissing() end
				if ithinkthat==nil then ifSecondDubIsmissing() end
	--[[			
				--20
	elseif nLevel==20 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==21 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==22 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==23 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==24 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==25 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==26 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==27 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==28 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==29 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
				
				--30
		elseif nLevel==30 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==31 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==32 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==33 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==34 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==35 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==36 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==37 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==38 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==39 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
				
								--40
		elseif nLevel==40 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==41 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==42 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==43 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==44 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==45 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==46 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==47 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==48 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==49 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
				
								--50
		elseif nLevel==50 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==51 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==52 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==53 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==54 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==55 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==56 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==57 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==58 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==59 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
				
								--60
		elseif nLevel==60 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==61 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==62 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==63 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==64 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==65 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==66 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==67 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==68 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==69 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
				
								--70
		elseif nLevel==70 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==71 then
				if ihavenoidea==nil then ifFirstDubIsmissing() end
				if whatwasthat==nil then ifSecondDubIsmissing() end
	elseif nLevel==72 then
				if youknow==nil 	 then ifFirstDubIsmissing() end
				if ohnonotagain==nil then ifSecondDubIsmissing() end
	elseif nLevel==73 then
				if imustnotswimdown==nil then ifFirstDubIsmissing() end
				if youknowtherules==nil then ifSecondDubIsmissing() end
	elseif nLevel==74 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==75 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==76 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==77 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==78 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	elseif nLevel==79 then
				if ==nil then ifFirstDubIsmissing() end
				if ==nil then ifSecondDubIsmissing() end
	--]]
	end
end

