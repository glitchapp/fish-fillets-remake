function loadvoices60()
	
	if nLevel==60 then
				
			if language=="en" then
					if accent=="br" then
						pleasenote = love.audio.newSource( "/externalassets/dialogs/level60/en/1pleasenote.ogg","stream" )
						andallofthem = love.audio.newSource( "/externalassets/dialogs/level60/en/2andallofthem.ogg","stream" )
						itwould = love.audio.newSource( "/externalassets/dialogs/level60/en/5itwould.ogg","stream" )
						thisis = love.audio.newSource( "/externalassets/dialogs/level60/en/6thisis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level60/en/9youareright.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level60/en/10ithink.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level60/en/13maybe.ogg","stream" )
				elseif accent=="us" then
						pleasenote = love.audio.newSource( "/externalassets/dialogs/level60/en-us/1pleasenote.ogg","stream" )
						andallofthem = love.audio.newSource( "/externalassets/dialogs/level60/en-us/2andallofthem.ogg","stream" )
						itwould = love.audio.newSource( "/externalassets/dialogs/level60/en-us/5itwould.ogg","stream" )
						thisis = love.audio.newSource( "/externalassets/dialogs/level60/en-us/6thisis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level60/en-us/9youareright.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level60/en-us/10ithink.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level60/en-us/13maybe.ogg","stream" )
				end
			elseif language=="fr" then
						pleasenote = love.audio.newSource( "/externalassets/dialogs/level60/fr/1pleasenote.ogg","stream" )
						andallofthem = love.audio.newSource( "/externalassets/dialogs/level60/fr/2andallofthem.ogg","stream" )
						itwould = love.audio.newSource( "/externalassets/dialogs/level60/fr/5itwould.ogg","stream" )
						thisis = love.audio.newSource( "/externalassets/dialogs/level60/fr/6thisis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level60/fr/9youareright.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level60/fr/10ithink.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level60/fr/13maybe.ogg","stream" )
			elseif language2=="es" then
					if accent2=="es" then
				elseif accent2=="la" then
						pleasenote = love.audio.newSource( "/externalassets/dialogs/level60/es-la/1pleasenote.ogg","stream" )
						andallofthem = love.audio.newSource( "/externalassets/dialogs/level60/es-la/2andallofthem.ogg","stream" )
						itwould = love.audio.newSource( "/externalassets/dialogs/level60/es-la/5itwould.ogg","stream" )
						thisis = love.audio.newSource( "/externalassets/dialogs/level60/es-la/6thisis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level60/es-la/9youareright.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level60/es-la/10ithink.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level60/es-la/13maybe.ogg","stream" )
				end	
		
			end
			
			if language2=="en" then
					if accent2=="br" then
						pleasenote2 = love.audio.newSource( "/externalassets/dialogs/level60/en-us/3pleasenote.ogg","stream" )
						itsbeautiful = love.audio.newSource( "/externalassets/dialogs/level60/en-us/4itsbeautiful.ogg","stream" )
						dontspeak = love.audio.newSource( "/externalassets/dialogs/level60/en-us/7dontspeak.ogg","stream" )
						dontyouthink = love.audio.newSource( "/externalassets/dialogs/level60/en-us/8dontyouthink.ogg","stream" )
						yeahyouareright = love.audio.newSource( "/externalassets/dialogs/level60/en-us/11yeahyouareright.ogg","stream" )
						look = love.audio.newSource( "/externalassets/dialogs/level60/en-us/12look.ogg","stream" )
				elseif accent2=="us" then
						pleasenote2 = love.audio.newSource( "/externalassets/dialogs/level60/en-us/3pleasenote.ogg","stream" )
						itsbeautiful = love.audio.newSource( "/externalassets/dialogs/level60/en-us/4itsbeautiful.ogg","stream" )
						dontspeak = love.audio.newSource( "/externalassets/dialogs/level60/en-us/7dontspeak.ogg","stream" )
						dontyouthink = love.audio.newSource( "/externalassets/dialogs/level60/en-us/8dontyouthink.ogg","stream" )
						yeahyouareright = love.audio.newSource( "/externalassets/dialogs/level60/en-us/11yeahyouareright.ogg","stream" )
						look = love.audio.newSource( "/externalassets/dialogs/level60/en-us/12look.ogg","stream" )
				end
		
			elseif language2=="cs" then
						pleasenote2 = love.audio.newSource( "/externalassets/dialogs/level60/en-us/3pleasenote.ogg","stream" )
						itsbeautiful = love.audio.newSource( "/externalassets/dialogs/level60/en-us/4itsbeautiful.ogg","stream" )
						dontspeak = love.audio.newSource( "/externalassets/dialogs/level60/en-us/7dontspeak.ogg","stream" )
						dontyouthink = love.audio.newSource( "/externalassets/dialogs/level60/en-us/8dontyouthink.ogg","stream" )
						yeahyouareright = love.audio.newSource( "/externalassets/dialogs/level60/en-us/11yeahyouareright.ogg","stream" )
						look = love.audio.newSource( "/externalassets/dialogs/level60/en-us/12look.ogg","stream" )
			
			elseif language2=="pl" then
						pleasenote = love.audio.newSource( "/externalassets/dialogs/level60/pl/1pleasenote.ogg","stream" )
						andallofthem = love.audio.newSource( "/externalassets/dialogs/level60/pl/2andallofthem.ogg","stream" )
						itwould = love.audio.newSource( "/externalassets/dialogs/level60/pl/5itwould.ogg","stream" )
						thisis = love.audio.newSource( "/externalassets/dialogs/level60/pl/6thisis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level60/pl/9youareright.ogg","stream" )
						ithink = love.audio.newSource( "/externalassets/dialogs/level60/pl/10ithink.ogg","stream" )
						maybe = love.audio.newSource( "/externalassets/dialogs/level60/pl/13maybe.ogg","stream" )
			end
			
						pleasenote:setEffect('myEffect')
						andallofthem:setEffect('myEffect')
						itwould:setEffect('myEffect')
						thisis:setEffect('myEffect')
						youareright:setEffect('myEffect')
						ithink:setEffect('myEffect')
						maybe:setEffect('myEffect')
			
						pleasenote2:setEffect('myEffect')
						itsbeautiful:setEffect('myEffect')
						dontspeak:setEffect('myEffect')
						dontyouthink:setEffect('myEffect')
						yeahyouareright:setEffect('myEffect')
						look:setEffect('myEffect')
		

		elseif nLevel==61 then
		
		if language=="en" then
			if accent=="br" then
				
				wefinally = love.audio.newSource( "/externalassets/dialogs/level61/en/1wefinally.ogg","stream" )
				captainssilver = love.audio.newSource( "/externalassets/dialogs/level61/en/3captainssilver.ogg","stream" )
				theholygrail = love.audio.newSource( "/externalassets/dialogs/level61/en/4theholygrail.ogg","stream" )
				itsneatly = love.audio.newSource( "/externalassets/dialogs/level61/en/5itsneatly.ogg","stream" )
				butihave = love.audio.newSource( "/externalassets/dialogs/level61/en/8butihave.ogg","stream" )
				wellallright = love.audio.newSource( "/externalassets/dialogs/level61/en/10wellallright.ogg","stream" )
				itwouldbe = love.audio.newSource( "/externalassets/dialogs/level61/en/14itwouldbe.ogg","stream" )
				trytopush = love.audio.newSource( "/externalassets/dialogs/level61/en/15trytopush.ogg","stream" )
				wewillbeglad = love.audio.newSource( "/externalassets/dialogs/level61/en/19wewillbeglad.ogg","stream" )
				dontforget = love.audio.newSource( "/externalassets/dialogs/level61/en/20dontforget.ogg","stream" )
				thetreasurewould = love.audio.newSource( "/externalassets/dialogs/level61/en/21thetrasurewould.ogg","stream" )
				allthisglinting = love.audio.newSource( "/externalassets/dialogs/level61/en/22allthisglinting.ogg","stream" )
			elseif accent=="us" then
				wefinally = love.audio.newSource( "/externalassets/dialogs/level61/en-us/1wefinally.ogg","stream" )
				captainssilver = love.audio.newSource( "/externalassets/dialogs/level61/en-us/3captainssilver.ogg","stream" )
				theholygrail = love.audio.newSource( "/externalassets/dialogs/level61/en-us/4theholygrail.ogg","stream" )
				itsneatly = love.audio.newSource( "/externalassets/dialogs/level61/en-us/5itsneatly.ogg","stream" )
				butihave = love.audio.newSource( "/externalassets/dialogs/level61/en-us/8butihave.ogg","stream" )
				wellallright = love.audio.newSource( "/externalassets/dialogs/level61/en-us/10wellallright.ogg","stream" )
				itwouldbe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/14itwouldbe.ogg","stream" )
				trytopush = love.audio.newSource( "/externalassets/dialogs/level61/en-us/15trytopush.ogg","stream" )
				wewillbeglad = love.audio.newSource( "/externalassets/dialogs/level61/en-us/19wewillbeglad.ogg","stream" )
				dontforget = love.audio.newSource( "/externalassets/dialogs/level61/en-us/20dontforget.ogg","stream" )
				thetreasurewould = love.audio.newSource( "/externalassets/dialogs/level61/en-us/21thetrasurewould.ogg","stream" )
				allthisglinting = love.audio.newSource( "/externalassets/dialogs/level61/en-us/22allthisglinting.ogg","stream" )
			end
		elseif language=="fr" then
				wefinally = love.audio.newSource( "/externalassets/dialogs/level61/fr/1wefinally.ogg","stream" )
				captainssilver = love.audio.newSource( "/externalassets/dialogs/level61/fr/3captainssilver.ogg","stream" )
				theholygrail = love.audio.newSource( "/externalassets/dialogs/level61/fr/4theholygrail.ogg","stream" )
				itsneatly = love.audio.newSource( "/externalassets/dialogs/level61/fr/5itsneatly.ogg","stream" )
				butihave = love.audio.newSource( "/externalassets/dialogs/level61/fr/8butihave.ogg","stream" )
				wellallright = love.audio.newSource( "/externalassets/dialogs/level61/fr/10wellallright.ogg","stream" )
				itwouldbe = love.audio.newSource( "/externalassets/dialogs/level61/fr/14itwouldbe.ogg","stream" )
				trytopush = love.audio.newSource( "/externalassets/dialogs/level61/fr/15trytopush.ogg","stream" )
				wewillbeglad = love.audio.newSource( "/externalassets/dialogs/level61/fr/19wewillbeglad.ogg","stream" )
				dontforget = love.audio.newSource( "/externalassets/dialogs/level61/fr/20dontforget.ogg","stream" )
				thetreasurewould = love.audio.newSource( "/externalassets/dialogs/level61/fr/21thetrasurewould.ogg","stream" )
				allthisglinting = love.audio.newSource( "/externalassets/dialogs/level61/fr/22allthisglinting.ogg","stream" )
		
		elseif language=="pl" then	-- till there's a polish voice actor we load english subs to prevent the game from crashing
				wefinally = love.audio.newSource( "/externalassets/dialogs/level61/en-us/1wefinally.ogg","stream" )
				captainssilver = love.audio.newSource( "/externalassets/dialogs/level61/en-us/3captainssilver.ogg","stream" )
				theholygrail = love.audio.newSource( "/externalassets/dialogs/level61/en-us/4theholygrail.ogg","stream" )
				itsneatly = love.audio.newSource( "/externalassets/dialogs/level61/en-us/5itsneatly.ogg","stream" )
				butihave = love.audio.newSource( "/externalassets/dialogs/level61/en-us/8butihave.ogg","stream" )
				wellallright = love.audio.newSource( "/externalassets/dialogs/level61/en-us/10wellallright.ogg","stream" )
				itwouldbe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/14itwouldbe.ogg","stream" )
				trytopush = love.audio.newSource( "/externalassets/dialogs/level61/en-us/15trytopush.ogg","stream" )
				wewillbeglad = love.audio.newSource( "/externalassets/dialogs/level61/en-us/19wewillbeglad.ogg","stream" )
				dontforget = love.audio.newSource( "/externalassets/dialogs/level61/en-us/20dontforget.ogg","stream" )
				thetreasurewould = love.audio.newSource( "/externalassets/dialogs/level61/en-us/21thetrasurewould.ogg","stream" )
				allthisglinting = love.audio.newSource( "/externalassets/dialogs/level61/en-us/22allthisglinting.ogg","stream" )
			
		end
		if language2=="en" then
			if accent2=="br" then
				what = love.audio.newSource( "/externalassets/dialogs/level61/en-us/2what.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level61/en-us/6ithink.ogg","stream" )
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level61/en-us/7ithinkthat.ogg","stream" )
				butihavecasted = love.audio.newSource( "/externalassets/dialogs/level61/en-us/9butihavecasted.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level61/en-us/11thatsbetter.ogg","stream" )
				couldntwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/12couldntwe.ogg","stream" )
				whatifwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/13whatifwe.ogg","stream" )
				wontwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/16wontwe.ogg","stream" )
				whatabout = love.audio.newSource( "/externalassets/dialogs/level61/en-us/17whatabout.ogg","stream" )
				arentyousorry = love.audio.newSource( "/externalassets/dialogs/level61/en-us/18arentyousorry.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level61/en-us/23everything.ogg","stream" )
			elseif accent2=="us" then
				what = love.audio.newSource( "/externalassets/dialogs/level61/en-us/2what.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level61/en-us/6ithink.ogg","stream" )
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level61/en-us/7ithinkthat.ogg","stream" )
				butihavecasted = love.audio.newSource( "/externalassets/dialogs/level61/en-us/9butihavecasted.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level61/en-us/11thatsbetter.ogg","stream" )
				couldntwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/12couldntwe.ogg","stream" )
				whatifwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/13whatifwe.ogg","stream" )
				wontwe = love.audio.newSource( "/externalassets/dialogs/level61/en-us/16wontwe.ogg","stream" )
				whatabout = love.audio.newSource( "/externalassets/dialogs/level61/en-us/17whatabout.ogg","stream" )
				arentyousorry = love.audio.newSource( "/externalassets/dialogs/level61/en-us/18arentyousorry.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level61/en-us/23everything.ogg","stream" )
				
			end
		elseif language2=="cs" then
				what = love.audio.newSource( "/externalassets/dialogs/level61/cs/2what.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level61/cs/6ithink.ogg","stream" )
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level61/cs/7ithinkthat.ogg","stream" )
				butihavecasted = love.audio.newSource( "/externalassets/dialogs/level61/cs/9butihavecasted.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level61/cs/11thatsbetter.ogg","stream" )
				couldntwe = love.audio.newSource( "/externalassets/dialogs/level61/cs/12couldntwe.ogg","stream" )
				whatifwe = love.audio.newSource( "/externalassets/dialogs/level61/cs/13whatifwe.ogg","stream" )
				wontwe = love.audio.newSource( "/externalassets/dialogs/level61/cs/16wontwe.ogg","stream" )
				whatabout = love.audio.newSource( "/externalassets/dialogs/level61/cs/17whatabout.ogg","stream" )
				arentyousorry = love.audio.newSource( "/externalassets/dialogs/level61/cs/18arentyousorry.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level61/cs/23everything.ogg","stream" )
		
		elseif language2=="pl" then
				what = love.audio.newSource( "/externalassets/dialogs/level61/pl/2what.ogg","stream" )
				ithink = love.audio.newSource( "/externalassets/dialogs/level61/pl/6ithink.ogg","stream" )
				ithinkthat = love.audio.newSource( "/externalassets/dialogs/level61/pl/7ithinkthat.ogg","stream" )
				butihavecasted = love.audio.newSource( "/externalassets/dialogs/level61/pl/9butihavecasted.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level61/pl/11thatsbetter.ogg","stream" )
				couldntwe = love.audio.newSource( "/externalassets/dialogs/level61/pl/12couldntwe.ogg","stream" )
				whatifwe = love.audio.newSource( "/externalassets/dialogs/level61/pl/13whatifwe.ogg","stream" )
				wontwe = love.audio.newSource( "/externalassets/dialogs/level61/pl/16wontwe.ogg","stream" )
				whatabout = love.audio.newSource( "/externalassets/dialogs/level61/pl/17whatabout.ogg","stream" )
				arentyousorry = love.audio.newSource( "/externalassets/dialogs/level61/pl/18arentyousorry.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level61/pl/23everything.ogg","stream" )
		end
				-- fish 1
				wefinally:setEffect('myEffect')
				captainssilver:setEffect('myEffect')
				theholygrail:setEffect('myEffect')
				itsneatly:setEffect('myEffect')
				butihave:setEffect('myEffect')
				wellallright:setEffect('myEffect')
				itwouldbe:setEffect('myEffect')
				trytopush:setEffect('myEffect')
				wewillbeglad:setEffect('myEffect')
				dontforget:setEffect('myEffect')
				thetreasurewould:setEffect('myEffect')
				allthisglinting:setEffect('myEffect')
				
				-- fish 2
				
				what:setEffect('myEffect')
				ithink:setEffect('myEffect')
				ithinkthat:setEffect('myEffect')
				butihavecasted:setEffect('myEffect')
				thatsbetter:setEffect('myEffect')
				couldntwe:setEffect('myEffect')
				whatifwe:setEffect('myEffect')
				wontwe:setEffect('myEffect')
				whatabout:setEffect('myEffect')
				arentyousorry:setEffect('myEffect')
				everything:setEffect('myEffect')
		elseif nLevel==62 then
			if language=="en" then
				if accent=="br" then
				
					youhavetolook  = love.audio.newSource( "/externalassets/dialogs/level62/en/2youhavetolook.ogg","stream" )
					itseemsthat  = love.audio.newSource( "/externalassets/dialogs/level62/en/3itseemsthat.ogg","stream" )
					justleavemealone  = love.audio.newSource( "/externalassets/dialogs/level62/en/5justleavemealone.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level62/en/7couldyou.ogg","stream" )
					youmean  = love.audio.newSource( "/externalassets/dialogs/level62/en/9youmean.ogg","stream" )
					andwhy  = love.audio.newSource( "/externalassets/dialogs/level62/en/12andwhy.ogg","stream" )
								
				elseif accent=="us" then
					youhavetolook  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/2youhavetolook.ogg","stream" )
					itseemsthat  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/3itseemsthat.ogg","stream" )
					justleavemealone  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/5justleavemealone.ogg","stream" )
					couldyou  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/7couldyou.ogg","stream" )
					youmean  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/9youmean.ogg","stream" )
					andwhy  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/12andwhy.ogg","stream" )

				end
			end
			if language2=="en" then
				if accent2=="br" then
					fromthe  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/1fromthe.ogg","stream" )
					yourfatbody  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/4yourfatbody.ogg","stream" )
					cutthecrap  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/6cutthecrap.ogg","stream" )
					ifeellike  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/8ifeellike.ogg","stream" )
					noitshould  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/10noitshould.ogg","stream" )
					youknowwhat  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/11youknowwhat.ogg","stream" )
					igrew  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/13igrew.ogg","stream" )
					icantbear  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/14icantbear.ogg","stream" )
				elseif accent2=="us" then
					fromthe  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/1fromthe.ogg","stream" )
					yourfatbody  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/4yourfatbody.ogg","stream" )
					cutthecrap  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/6cutthecrap.ogg","stream" )
					ifeellike  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/8ifeellike.ogg","stream" )
					noitshould  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/10noitshould.ogg","stream" )
					youknowwhat  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/11youknowwhat.ogg","stream" )
					igrew  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/13igrew.ogg","stream" )
					icantbear  = love.audio.newSource( "/externalassets/dialogs/level62/en-us/14icantbear.ogg","stream" )
				end
			elseif language2=="cs" then
					fromthe  = love.audio.newSource( "/externalassets/dialogs/level62/cs/1fromthe.ogg","stream" )
					yourfatbody  = love.audio.newSource( "/externalassets/dialogs/level62/cs/4yourfatbody.ogg","stream" )
					cutthecrap  = love.audio.newSource( "/externalassets/dialogs/level62/cs/6cutthecrap.ogg","stream" )
					ifeellike  = love.audio.newSource( "/externalassets/dialogs/level62/cs/8ifeellike.ogg","stream" )
					noitshould  = love.audio.newSource( "/externalassets/dialogs/level62/cs/10noitshould.ogg","stream" )
					youknowwhat  = love.audio.newSource( "/externalassets/dialogs/level62/cs/11youknowwhat.ogg","stream" )
					igrew  = love.audio.newSource( "/externalassets/dialogs/level62/cs/13igrew.ogg","stream" )
					icantbear  = love.audio.newSource( "/externalassets/dialogs/level62/cs/14icantbear.ogg","stream" )
			elseif language2=="pl" then
					fromthe  = love.audio.newSource( "/externalassets/dialogs/level62/pl/1fromthe.ogg","stream" )
					yourfatbody  = love.audio.newSource( "/externalassets/dialogs/level62/pl/4yourfatbody.ogg","stream" )
					cutthecrap  = love.audio.newSource( "/externalassets/dialogs/level62/pl/6cutthecrap.ogg","stream" )
					ifeellike  = love.audio.newSource( "/externalassets/dialogs/level62/pl/8ifeellike.ogg","stream" )
					noitshould  = love.audio.newSource( "/externalassets/dialogs/level62/pl/10noitshould.ogg","stream" )
					youknowwhat  = love.audio.newSource( "/externalassets/dialogs/level62/pl/11youknowwhat.ogg","stream" )
					igrew  = love.audio.newSource( "/externalassets/dialogs/level62/pl/13igrew.ogg","stream" )
					icantbear  = love.audio.newSource( "/externalassets/dialogs/level62/pl/14icantbear.ogg","stream" )
			end
				--fish 1
				youhavetolook:setEffect('myEffect')
				itseemsthat:setEffect('myEffect')
				justleavemealone:setEffect('myEffect')
				couldyou:setEffect('myEffect')
				youmean:setEffect('myEffect')
				andwhy:setEffect('myEffect')
				
				--fish 2
					fromthe:setEffect('myEffect')
					yourfatbody:setEffect('myEffect')
					cutthecrap:setEffect('myEffect')
					ifeellike:setEffect('myEffect')
					noitshould:setEffect('myEffect')
					youknowwhat:setEffect('myEffect')
					igrew:setEffect('myEffect')
					icantbear:setEffect('myEffect')
				
		elseif nLevel==63 then
			if language=="en" then
				if accent=="br" then
					wellyes  = love.audio.newSource( "/externalassets/dialogs/level63/en/2wellyes.ogg","stream" )
					thatmaybetrue  = love.audio.newSource( "/externalassets/dialogs/level63/en/4thatmaybetrue.ogg","stream" )
					itstoonarrow  = love.audio.newSource( "/externalassets/dialogs/level63/en/5itstoonarrow.ogg","stream" )
					fish  = love.audio.newSource( "/externalassets/dialogs/level63/en/7fish.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/en/8ithink.ogg","stream" )
					imnotsosure  = love.audio.newSource( "/externalassets/dialogs/level63/en/10imnotsosure.ogg","stream" )
					itscalledflashy  = love.audio.newSource( "/externalassets/dialogs/level63/en/15itscalledflashy.ogg","stream" )
					itscalledglossy  = love.audio.newSource( "/externalassets/dialogs/level63/en/16itscalledglossy.ogg","stream" )
					itscalledpolitician  = love.audio.newSource( "/externalassets/dialogs/level63/en/17itscalledpolitician.ogg","stream" )
					thisisveryphlegmatic  = love.audio.newSource( "/externalassets/dialogs/level63/en/19thisisveryphlegmatic.ogg","stream" )
					itsasculpture  = love.audio.newSource( "/externalassets/dialogs/level63/en/21itsasculpture.ogg","stream" )
					itsaplainstalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/en/23itsaplainstalagmite.ogg","stream" )
					ithinkyouare  = love.audio.newSource( "/externalassets/dialogs/level63/en/25ithinkyouare.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/en/26ithink.ogg","stream" )
				elseif accent=="us" then
					wellyes  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/2wellyes.ogg","stream" )
					thatmaybetrue  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/4thatmaybetrue.ogg","stream" )
					itstoonarrow  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/5itstoonarrow.ogg","stream" )
					fish  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/7fish.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/8ithink.ogg","stream" )
					imnotsosure  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/10imnotsosure.ogg","stream" )
					itscalledflashy  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/15itscalledflashy.ogg","stream" )
					itscalledglossy  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/16itscalledglossy.ogg","stream" )
					itscalledpolitician  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/17itscalledpolitician.ogg","stream" )
					thisisveryphlegmatic  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/19thisisveryphlegmatic.ogg","stream" )
					itsasculpture  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/21itsasculpture.ogg","stream" )
					itsaplainstalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/23itsaplainstalagmite.ogg","stream" )
					ithinkyouare  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/25ithinkyouare.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/26ithink.ogg","stream" )
				end
			elseif language=="fr" then
					wellyes  = love.audio.newSource( "/externalassets/dialogs/level63/fr/2wellyes.ogg","stream" )
					thatmaybetrue  = love.audio.newSource( "/externalassets/dialogs/level63/fr/4thatmaybetrue.ogg","stream" )
					itstoonarrow  = love.audio.newSource( "/externalassets/dialogs/level63/fr/5itstoonarrow.ogg","stream" )
					fish  = love.audio.newSource( "/externalassets/dialogs/level63/fr/7fish.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/fr/8ithink.ogg","stream" )
					imnotsosure  = love.audio.newSource( "/externalassets/dialogs/level63/fr/10imnotsosure.ogg","stream" )
					itscalledflashy  = love.audio.newSource( "/externalassets/dialogs/level63/fr/15itscalledflashy.ogg","stream" )
					itscalledglossy  = love.audio.newSource( "/externalassets/dialogs/level63/fr/16itscalledglossy.ogg","stream" )
					itscalledpolitician  = love.audio.newSource( "/externalassets/dialogs/level63/fr/17itscalledpolitician.ogg","stream" )
					thisisveryphlegmatic  = love.audio.newSource( "/externalassets/dialogs/level63/fr/19thisisveryphlegmatic.ogg","stream" )
					itsasculpture  = love.audio.newSource( "/externalassets/dialogs/level63/fr/21itsasculpture.ogg","stream" )
					itsaplainstalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/fr/23itsaplainstalagmite.ogg","stream" )
					ithinkyouare  = love.audio.newSource( "/externalassets/dialogs/level63/fr/25ithinkyouare.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/fr/26ithink.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					wellyes  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/2wellyes.ogg","stream" )
					thatmaybetrue  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/4thatmaybetrue.ogg","stream" )
					itstoonarrow  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/5itstoonarrow.ogg","stream" )
					fish  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/7fish.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/8ithink.ogg","stream" )
					imnotsosure  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/10imnotsosure.ogg","stream" )
					itscalledflashy  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/15itscalledflashy.ogg","stream" )
					itscalledglossy  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/16itscalledglossy.ogg","stream" )
					itscalledpolitician  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/17itscalledpolitician.ogg","stream" )
					thisisveryphlegmatic  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/19thisisveryphlegmatic.ogg","stream" )
					itsasculpture  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/21itsasculpture.ogg","stream" )
					itsaplainstalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/23itsaplainstalagmite.ogg","stream" )
					ithinkyouare  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/25ithinkyouare.ogg","stream" )
					ithink  = love.audio.newSource( "/externalassets/dialogs/level63/es-la/26ithink.ogg","stream" )
				end
			end
			if language2=="en" then
				if accent2=="br" then
					thatbat  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/1thatbat.ogg","stream" )
					thatredcreature  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/3thatredcreature.ogg","stream" )
					thatwhitefish  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/6thatwhitefish.ogg","stream" )
					dontbedepressive  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/9dontbedepressive.ogg","stream" )
					seeyouare  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/11seeyouare.ogg","stream" )
					whatkind  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/12whatkind.ogg","stream" )
					thisis  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/13thisis.ogg","stream" )
					thisisan  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/14thisisan.ogg","stream" )
					thisbat  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/18thisbat.ogg","stream" )
					thisisa  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/20thisisa.ogg","stream" )
					itsastalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/22itsastalagmite.ogg","stream" )
					itsjustan  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/24itsjustan.ogg","stream" )
				elseif accent2=="us" then
					thatbat  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/1thatbat.ogg","stream" )
					thatredcreature  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/3thatredcreature.ogg","stream" )
					thatwhitefish  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/6thatwhitefish.ogg","stream" )
					dontbedepressive  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/9dontbedepressive.ogg","stream" )
					seeyouare  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/11seeyouare.ogg","stream" )
					whatkind  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/12whatkind.ogg","stream" )
					thisis  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/13thisis.ogg","stream" )
					thisisan  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/14thisisan.ogg","stream" )
					thisbat  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/18thisbat.ogg","stream" )
					thisisa  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/20thisisa.ogg","stream" )
					itsastalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/22itsastalagmite.ogg","stream" )
					itsjustan  = love.audio.newSource( "/externalassets/dialogs/level63/en-us/24itsjustan.ogg","stream" )
				end
			elseif language2=="cs" then					
					thatbat  = love.audio.newSource( "/externalassets/dialogs/level63/cs/1thatbat.ogg","stream" )
					thatredcreature  = love.audio.newSource( "/externalassets/dialogs/level63/cs/3thatredcreature.ogg","stream" )
					thatwhitefish  = love.audio.newSource( "/externalassets/dialogs/level63/cs/6thatwhitefish.ogg","stream" )
					dontbedepressive  = love.audio.newSource( "/externalassets/dialogs/level63/cs/9dontbedepressive.ogg","stream" )
					seeyouare  = love.audio.newSource( "/externalassets/dialogs/level63/cs/11seeyouare.ogg","stream" )
					whatkind  = love.audio.newSource( "/externalassets/dialogs/level63/cs/12whatkind.ogg","stream" )
					thisis  = love.audio.newSource( "/externalassets/dialogs/level63/cs/13thisis.ogg","stream" )
					thisisan  = love.audio.newSource( "/externalassets/dialogs/level63/cs/14thisisan.ogg","stream" )
					thisbat  = love.audio.newSource( "/externalassets/dialogs/level63/cs/18thisbat.ogg","stream" )
					thisisa  = love.audio.newSource( "/externalassets/dialogs/level63/cs/20thisisa.ogg","stream" )
					itsastalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/cs/22itsastalagmite.ogg","stream" )
					itsjustan  = love.audio.newSource( "/externalassets/dialogs/level63/cs/24itsjustan.ogg","stream" )
			
			elseif language2=="pl" then					
					thatbat  = love.audio.newSource( "/externalassets/dialogs/level63/pl/1thatbat.ogg","stream" )
					thatredcreature  = love.audio.newSource( "/externalassets/dialogs/level63/pl/3thatredcreature.ogg","stream" )
					thatwhitefish  = love.audio.newSource( "/externalassets/dialogs/level63/pl/6thatwhitefish.ogg","stream" )
					dontbedepressive  = love.audio.newSource( "/externalassets/dialogs/level63/pl/9dontbedepressive.ogg","stream" )
					seeyouare  = love.audio.newSource( "/externalassets/dialogs/level63/pl/11seeyouare.ogg","stream" )
					whatkind  = love.audio.newSource( "/externalassets/dialogs/level63/pl/12whatkind.ogg","stream" )
					thisis  = love.audio.newSource( "/externalassets/dialogs/level63/pl/13thisis.ogg","stream" )
					thisisan  = love.audio.newSource( "/externalassets/dialogs/level63/pl/14thisisan.ogg","stream" )
					thisbat  = love.audio.newSource( "/externalassets/dialogs/level63/pl/18thisbat.ogg","stream" )
					thisisa  = love.audio.newSource( "/externalassets/dialogs/level63/pl/20thisisa.ogg","stream" )
					itsastalagmite  = love.audio.newSource( "/externalassets/dialogs/level63/pl/22itsastalagmite.ogg","stream" )
					itsjustan  = love.audio.newSource( "/externalassets/dialogs/level63/pl/24itsjustan.ogg","stream" )
			end
				
				-- fish 1
					thatbat:setEffect('myEffect')
					thatredcreature:setEffect('myEffect')
					thatwhitefish:setEffect('myEffect')
					dontbedepressive:setEffect('myEffect')
					seeyouare:setEffect('myEffect')
					whatkind:setEffect('myEffect')
					thisis:setEffect('myEffect')
					thisisan:setEffect('myEffect')
					thisbat:setEffect('myEffect')
					thisisa:setEffect('myEffect')
					itsastalagmite:setEffect('myEffect')
					itsjustan:setEffect('myEffect')
				
				-- fish 2
				wellyes:setEffect('myEffect')
				thatmaybetrue:setEffect('myEffect')
				itstoonarrow:setEffect('myEffect')
				fish:setEffect('myEffect')
				ithink:setEffect('myEffect')
				imnotsosure:setEffect('myEffect')
				itscalledflashy:setEffect('myEffect')
				itscalledglossy:setEffect('myEffect')
				itscalledpolitician:setEffect('myEffect')
				thisisveryphlegmatic:setEffect('myEffect')
				itsasculpture:setEffect('myEffect')
				itsaplainstalagmite:setEffect('myEffect')
				ithinkyouare:setEffect('myEffect')
				ithink:setEffect('myEffect')
		
		
		elseif nLevel==64 then
		
		loadBorderDialogs()
			if language=="en" then
				--end
				
				werecommend  = love.audio.newSource( "/externalassets/ends/6treasurecave/audios/1werecommend.ogg","stream" )
				duetothelack  = love.audio.newSource( "/externalassets/ends/6treasurecave/audios/2duetothelack.ogg","stream" )
				bombing  = love.audio.newSource( "/externalassets/ends/6treasurecave/audios/3bombing.ogg","stream" )
				someofthem  = love.audio.newSource( "/externalassets/ends/6treasurecave/audios/4someofthem.ogg","stream" )
				itisalsopossible  = love.audio.newSource( "/externalassets/ends/6treasurecave/audios/5itisalsopossible.ogg","stream" )
			
					if accent=="br" then
						howcanyou = love.audio.newSource( "/externalassets/dialogs/level64/en/2howcanyou.ogg","stream" )
						thehalomeans  = love.audio.newSource( "/externalassets/dialogs/level64/en/6thehalomeans.ogg","stream" )
						allthatglitters  = love.audio.newSource( "/externalassets/dialogs/level64/en/7allthatglitters.ogg","stream" )
						hmm  = love.audio.newSource( "/externalassets/dialogs/level64/en/8hmm.ogg","stream" )
						ifeel  = love.audio.newSource( "/externalassets/dialogs/level64/en/9ifeel.ogg","stream" )
						idrather  = love.audio.newSource( "/externalassets/dialogs/level64/en/12idrather.ogg","stream" )
						notopush  = love.audio.newSource( "/externalassets/dialogs/level64/en/15notopush.ogg","stream" )
						almostall  = love.audio.newSource( "/externalassets/dialogs/level64/en/18almostall.ogg","stream" )
						onemoregrail  = love.audio.newSource( "/externalassets/dialogs/level64/en/19onemoregrail.ogg","stream" )
						
				elseif accent=="us" then
						howcanyou  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/2howcanyou.ogg","stream" )
						thehalomeans  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/6thehalomeans.ogg","stream" )
						allthatglitters  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/7allthatglitters.ogg","stream" )
						hmm  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/8hmm.ogg","stream" )
						ifeel  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/9ifeel.ogg","stream" )
						idrather  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/12idrather.ogg","stream" )
						notopush  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/15notopush.ogg","stream" )
						almostall  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/18almostall.ogg","stream" )
						onemoregrail  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/19onemoregrail.ogg","stream" )
				end
			elseif language=="fr" then
						howcanyou  = love.audio.newSource( "/externalassets/dialogs/level64/fr/2howcanyou.ogg","stream" )
						thehalomeans  = love.audio.newSource( "/externalassets/dialogs/level64/fr/6thehalomeans.ogg","stream" )
						allthatglitters  = love.audio.newSource( "/externalassets/dialogs/level64/fr/7allthatglitters.ogg","stream" )
						hmm  = love.audio.newSource( "/externalassets/dialogs/level64/fr/8hmm.ogg","stream" )
						ifeel  = love.audio.newSource( "/externalassets/dialogs/level64/fr/9ifeel.ogg","stream" )
						idrather  = love.audio.newSource( "/externalassets/dialogs/level64/fr/12idrather.ogg","stream" )
						notopush  = love.audio.newSource( "/externalassets/dialogs/level64/fr/15notopush.ogg","stream" )
						almostall  = love.audio.newSource( "/externalassets/dialogs/level64/fr/18almostall.ogg","stream" )
						onemoregrail  = love.audio.newSource( "/externalassets/dialogs/level64/fr/19onemoregrail.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
						howcanyou = love.audio.newSource( "/externalassets/dialogs/level64/es-la/2howcanyou.ogg","stream" )
						thehalomeans  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/6thehalomeans.ogg","stream" )
						allthatglitters  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/7allthatglitters.ogg","stream" )
						hmm  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/8hmm.ogg","stream" )
						ifeel  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/9ifeel.ogg","stream" )
						idrather  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/12idrather.ogg","stream" )
						notopush  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/15notopush.ogg","stream" )
						almostall  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/18almostall.ogg","stream" )
						onemoregrail  = love.audio.newSource( "/externalassets/dialogs/level64/es-la/19onemoregrail.ogg","stream" )			
				end
			end
			
			if language2=="en" then
				if accent2=="br" then
					thatthing  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/1thatthing.ogg","stream" )
					cantyousee  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/3cantyousee.ogg","stream" )
					cantyouseehow  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/4cantyouseehow.ogg","stream" )
					cantyouseehow2  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/5cantyouseehow.ogg","stream" )
					youandyour  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/10youandyour.ogg","stream" )
					letsgo  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/11letsgo.ogg","stream" )
					hmm  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/13hmm.ogg","stream" )
					ourgoal  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/14ourgoal.ogg","stream" )
					theholy  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/16theholy.ogg","stream" )
					ourgoal2  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/17ourgoal.ogg","stream" )
				elseif accent2=="us" then
					thatthing  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/1thatthing.ogg","stream" )
					cantyousee  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/3cantyousee.ogg","stream" )
					cantyouseehow  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/4cantyouseehow.ogg","stream" )
					cantyouseehow2  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/5cantyouseehow.ogg","stream" )
					youandyour  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/10youandyour.ogg","stream" )
					letsgo  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/11letsgo.ogg","stream" )
					hmm  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/13hmm.ogg","stream" )
					ourgoal  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/14ourgoal.ogg","stream" )
					theholy  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/16theholy.ogg","stream" )
					ourgoal2  = love.audio.newSource( "/externalassets/dialogs/level64/en-us/17ourgoal.ogg","stream" )
				end
			elseif language2=="cs" then
					thatthing  = love.audio.newSource( "/externalassets/dialogs/level64/cs/1thatthing.ogg","stream" )
					cantyousee  = love.audio.newSource( "/externalassets/dialogs/level64/cs/3cantyousee.ogg","stream" )
					cantyouseehow  = love.audio.newSource( "/externalassets/dialogs/level64/cs/4cantyouseehow.ogg","stream" )
					cantyouseehow2  = love.audio.newSource( "/externalassets/dialogs/level64/cs/5cantyouseehow.ogg","stream" )
					youandyour  = love.audio.newSource( "/externalassets/dialogs/level64/cs/10youandyour.ogg","stream" )
					letsgo  = love.audio.newSource( "/externalassets/dialogs/level64/cs/11letsgo.ogg","stream" )
					hmm  = love.audio.newSource( "/externalassets/dialogs/level64/cs/13hmm.ogg","stream" )
					ourgoal  = love.audio.newSource( "/externalassets/dialogs/level64/cs/14ourgoal.ogg","stream" )
					theholy  = love.audio.newSource( "/externalassets/dialogs/level64/cs/16theholy.ogg","stream" )
					ourgoal2  = love.audio.newSource( "/externalassets/dialogs/level64/cs/17ourgoal.ogg","stream" )
			
			elseif language2=="pl" then
					thatthing  = love.audio.newSource( "/externalassets/dialogs/level64/pl/1thatthing.ogg","stream" )
					cantyousee  = love.audio.newSource( "/externalassets/dialogs/level64/pl/3cantyousee.ogg","stream" )
					cantyouseehow  = love.audio.newSource( "/externalassets/dialogs/level64/pl/4cantyouseehow.ogg","stream" )
					cantyouseehow2  = love.audio.newSource( "/externalassets/dialogs/level64/pl/5cantyouseehow.ogg","stream" )
					youandyour  = love.audio.newSource( "/externalassets/dialogs/level64/pl/10youandyour.ogg","stream" )
					letsgo  = love.audio.newSource( "/externalassets/dialogs/level64/pl/11letsgo.ogg","stream" )
					hmm  = love.audio.newSource( "/externalassets/dialogs/level64/pl/13hmm.ogg","stream" )
					ourgoal  = love.audio.newSource( "/externalassets/dialogs/level64/pl/14ourgoal.ogg","stream" )
					theholy  = love.audio.newSource( "/externalassets/dialogs/level64/pl/16theholy.ogg","stream" )
					ourgoal2  = love.audio.newSource( "/externalassets/dialogs/level64/pl/17ourgoal.ogg","stream" )
			end
					-- end
					
						werecommend:setEffect('myEffect')
						duetothelack:setEffect('myEffect')
						bombing:setEffect('myEffect')
						someofthem:setEffect('myEffect')
						itisalsopossible:setEffect('myEffect')
			
					--fish 1
						howcanyou:setEffect('myEffect')
						thehalomeans:setEffect('myEffect')
						allthatglitters:setEffect('myEffect')
						hmm:setEffect('myEffect')
						ifeel:setEffect('myEffect')
						idrather:setEffect('myEffect')
						notopush:setEffect('myEffect')
						almostall:setEffect('myEffect')
						onemoregrail:setEffect('myEffect')
					--fish 2
					thatthing:setEffect('myEffect')
					cantyousee:setEffect('myEffect')
					cantyouseehow:setEffect('myEffect')
					cantyouseehow2:setEffect('myEffect')
					youandyour:setEffect('myEffect')
					letsgo:setEffect('myEffect')
					hmm:setEffect('myEffect')
					ourgoal:setEffect('myEffect')
					theholy:setEffect('myEffect')
					ourgoal2:setEffect('myEffect')
					
		elseif nLevel==65 then
			if language=="en" then
				if accent=="br" then
				
					onlydeeprespect = love.audio.newSource( "/externalassets/dialogs/level65/en/2onlydeeprespect.ogg","stream" )
					doyouknow = love.audio.newSource( "/externalassets/dialogs/level65/en/4doyouknow.ogg","stream" )
					ithinkwe = love.audio.newSource( "/externalassets/dialogs/level65/en/6ithinkwe.ogg","stream" )
					atleast = love.audio.newSource( "/externalassets/dialogs/level65/en/8atleast.ogg","stream" )
					ithinkalittle = love.audio.newSource( "/externalassets/dialogs/level65/en/10ithinkalittle.ogg","stream" )
					wewillhave = love.audio.newSource( "/externalassets/dialogs/level65/en/12wewillhave.ogg","stream" )
				elseif accent=="us" then
					onlydeeprespect = love.audio.newSource( "/externalassets/dialogs/level65/en-us/2onlydeeprespect.ogg","stream" )
					doyouknow = love.audio.newSource( "/externalassets/dialogs/level65/en-us/4doyouknow.ogg","stream" )
					ithinkwe = love.audio.newSource( "/externalassets/dialogs/level65/en-us/6ithinkwe.ogg","stream" )
					atleast = love.audio.newSource( "/externalassets/dialogs/level65/en-us/8atleast.ogg","stream" )
					ithinkalittle = love.audio.newSource( "/externalassets/dialogs/level65/en-us/10ithinkalittle.ogg","stream" )
					wewillhave = love.audio.newSource( "/externalassets/dialogs/level65/en-us/12wewillhave.ogg","stream" )
				end
	
			elseif language=="fr" then
				onlydeeprespect = love.audio.newSource( "/externalassets/dialogs/level65/fr/2onlydeeprespect.ogg","stream" )
				doyouknow = love.audio.newSource( "/externalassets/dialogs/level65/fr/4doyouknow.ogg","stream" )
				ithinkwe = love.audio.newSource( "/externalassets/dialogs/level65/fr/6ithinkwe.ogg","stream" )
				atleast = love.audio.newSource( "/externalassets/dialogs/level65/fr/8atleast.ogg","stream" )
				ithinkalittle = love.audio.newSource( "/externalassets/dialogs/level65/fr/10ithinkalittle.ogg","stream" )
				wewillhave = love.audio.newSource( "/externalassets/dialogs/level65/fr/12wewillhave.ogg","stream" )
			
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					onlydeeprespect = love.audio.newSource( "/externalassets/dialogs/level65/es-la/2onlydeeprespect.ogg","stream" )
					doyouknow = love.audio.newSource( "/externalassets/dialogs/level65/es-la/4doyouknow.ogg","stream" )
					ithinkwe = love.audio.newSource( "/externalassets/dialogs/level65/es-la/6ithinkwe.ogg","stream" )
					atleast = love.audio.newSource( "/externalassets/dialogs/level65/es-la/8atleast.ogg","stream" )
					ithinkalittle = love.audio.newSource( "/externalassets/dialogs/level65/es-la/10ithinkalittle.ogg","stream" )
					wewillhave = love.audio.newSource( "/externalassets/dialogs/level65/es-la/12wewillhave.ogg","stream" )
				end
			end
			
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					sothisis = love.audio.newSource( "/externalassets/dialogs/level65/en-us/1sothisis.ogg","stream" )
					buteverybody = love.audio.newSource( "/externalassets/dialogs/level65/en-us/3buteverybody.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level65/en-us/5well.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level65/en-us/7whatdoyoumean.ogg","stream" )
					thetruthis = love.audio.newSource( "/externalassets/dialogs/level65/en-us/9thetruthis.ogg","stream" )
					okaytrytoprogram = love.audio.newSource( "/externalassets/dialogs/level65/en-us/11okaytrytoprogram.ogg","stream" )
					becareful = love.audio.newSource( "/externalassets/dialogs/level65/en-us/12becareful.ogg","stream" )
					hmm = love.audio.newSource( "/externalassets/dialogs/level65/en-us/14hmm.ogg","stream" )
				end
			elseif language2=="cs" then
					sothisis = love.audio.newSource( "/externalassets/dialogs/level65/cs/1sothisis.ogg","stream" )
					buteverybody = love.audio.newSource( "/externalassets/dialogs/level65/cs/3buteverybody.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level65/cs/5well.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level65/cs/7whatdoyoumean.ogg","stream" )
					thetruthis = love.audio.newSource( "/externalassets/dialogs/level65/cs/9thetruthis.ogg","stream" )
					okaytrytoprogram = love.audio.newSource( "/externalassets/dialogs/level65/cs/11okaytrytoprogram.ogg","stream" )
					becareful = love.audio.newSource( "/externalassets/dialogs/level65/cs/12becareful.ogg","stream" )
					hmm = love.audio.newSource( "/externalassets/dialogs/level65/cs/14hmm.ogg","stream" )
			
			elseif language2=="pl" then
					sothisis = love.audio.newSource( "/externalassets/dialogs/level65/pl/1sothisis.ogg","stream" )
					buteverybody = love.audio.newSource( "/externalassets/dialogs/level65/pl/3buteverybody.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level65/pl/5well.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level65/pl/7whatdoyoumean.ogg","stream" )
					thetruthis = love.audio.newSource( "/externalassets/dialogs/level65/pl/9thetruthis.ogg","stream" )
					okaytrytoprogram = love.audio.newSource( "/externalassets/dialogs/level65/pl/11okaytrytoprogram.ogg","stream" )
					becareful = love.audio.newSource( "/externalassets/dialogs/level65/pl/12becareful.ogg","stream" )
					hmm = love.audio.newSource( "/externalassets/dialogs/level65/pl/14hmm.ogg","stream" )
			end
				--fish 1
				onlydeeprespect:setEffect('myEffect')
				doyouknow:setEffect('myEffect')
				ithinkwe:setEffect('myEffect')
				atleast:setEffect('myEffect')
				ithinkalittle:setEffect('myEffect')
				wewillhave:setEffect('myEffect')
				
				--fish 2
					sothisis:setEffect('myEffect')
					buteverybody:setEffect('myEffect')
					well:setEffect('myEffect')
					whatdoyoumean:setEffect('myEffect')
					thetruthis:setEffect('myEffect')
					okaytrytoprogram:setEffect('myEffect')
					becareful:setEffect('myEffect')
					hmm:setEffect('myEffect')
		
		elseif nLevel==66 then
				isloading=false
				if language=="en" then
					if accent=="br" then
						yeah = love.audio.newSource( "/externalassets/dialogs/level66/en/2yeah.ogg","stream" )
						thisisanopportunity = love.audio.newSource( "/externalassets/dialogs/level66/en/4thisisanopportunity.ogg","stream" )
						wellijust = love.audio.newSource( "/externalassets/dialogs/level66/en/6wellijust.ogg","stream" )
						doyouknow = love.audio.newSource( "/externalassets/dialogs/level66/en/7doyouknow.ogg","stream" )
						andimagine = love.audio.newSource( "/externalassets/dialogs/level66/en/8andimagine.ogg","stream" )
						andthatmustbe = love.audio.newSource( "/externalassets/dialogs/level66/en/11andthatmustbe.ogg","stream" )
						thislineof = love.audio.newSource( "/externalassets/dialogs/level66/en/13thislineof.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level66/en/15iwonder.ogg","stream" )
					elseif accent=="us" then
						yeah = love.audio.newSource( "/externalassets/dialogs/level66/en-us/2yeah.ogg","stream" )
						thisisanopportunity = love.audio.newSource( "/externalassets/dialogs/level66/en-us/4thisisanopportunity.ogg","stream" )
						wellijust = love.audio.newSource( "/externalassets/dialogs/level66/en-us/6wellijust.ogg","stream" )
						doyouknow = love.audio.newSource( "/externalassets/dialogs/level66/en-us/7doyouknow.ogg","stream" )
						andimagine = love.audio.newSource( "/externalassets/dialogs/level66/en-us/8andimagine.ogg","stream" )
						andthatmustbe = love.audio.newSource( "/externalassets/dialogs/level66/en-us/11andthatmustbe.ogg","stream" )
						thislineof = love.audio.newSource( "/externalassets/dialogs/level66/en-us/13thislineof.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level66/en-us/15iwonder.ogg","stream" )
					end

				elseif language=="fr" then
					yeah = love.audio.newSource( "/externalassets/dialogs/level66/fr/2yeah.ogg","stream" )
					thisisanopportunity = love.audio.newSource( "/externalassets/dialogs/level66/fr/4thisisanopportunity.ogg","stream" )
					wellijust = love.audio.newSource( "/externalassets/dialogs/level66/fr/6wellijust.ogg","stream" )
					doyouknow = love.audio.newSource( "/externalassets/dialogs/level66/fr/7doyouknow.ogg","stream" )
					andimagine = love.audio.newSource( "/externalassets/dialogs/level66/fr/8andimagine.ogg","stream" )
					andthatmustbe = love.audio.newSource( "/externalassets/dialogs/level66/fr/11andthatmustbe.ogg","stream" )
					thislineof = love.audio.newSource( "/externalassets/dialogs/level66/fr/13thislineof.ogg","stream" )
					iwonder = love.audio.newSource( "/externalassets/dialogs/level66/fr/15iwonder.ogg","stream" )
			
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
						yeah = love.audio.newSource( "/externalassets/dialogs/level66/es-la/2yeah.ogg","stream" )
						thisisanopportunity = love.audio.newSource( "/externalassets/dialogs/level66/es-la/4thisisanopportunity.ogg","stream" )
						wellijust = love.audio.newSource( "/externalassets/dialogs/level66/es-la/6wellijust.ogg","stream" )
						doyouknow = love.audio.newSource( "/externalassets/dialogs/level66/es-la/7doyouknow.ogg","stream" )
						andimagine = love.audio.newSource( "/externalassets/dialogs/level66/es-la/8andimagine.ogg","stream" )
						andthatmustbe = love.audio.newSource( "/externalassets/dialogs/level66/es-la/11andthatmustbe.ogg","stream" )
						thislineof = love.audio.newSource( "/externalassets/dialogs/level66/es-la/13thislineof.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level66/es-la/15iwonder.ogg","stream" )
				end
			end
				
				if language2=="en" then
						if accent2=="br" then
					elseif accent2=="us" then
						sothisisalevel = love.audio.newSource( "/externalassets/dialogs/level66/en-us/1sothisisalevel.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level66/en-us/3canyouseethat.ogg","stream" )
						whatareyouthinking = love.audio.newSource( "/externalassets/dialogs/level66/en-us/5whatareyouthinking.ogg","stream" )
						butstillthesegameshad = love.audio.newSource( "/externalassets/dialogs/level66/en-us/9butstillthesegameshad.ogg","stream" )
						canyouseethatknight = love.audio.newSource( "/externalassets/dialogs/level66/en-us/10canyouseethatknight.ogg","stream" )
						ireallydidntknow = love.audio.newSource( "/externalassets/dialogs/level66/en-us/12ireallydidntknow.ogg","stream" )
						ohsurethey = love.audio.newSource( "/externalassets/dialogs/level66/en-us/14ohsurethey.ogg","stream" )
						thesinglebiggest = love.audio.newSource( "/externalassets/dialogs/level66/en-us/16thesinglebiggest.ogg","stream" )
					end
				elseif language2=="cs" then
						sothisisalevel = love.audio.newSource( "/externalassets/dialogs/level66/cs/1sothisisalevel.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level66/cs/3canyouseethat.ogg","stream" )
						whatareyouthinking = love.audio.newSource( "/externalassets/dialogs/level66/cs/5whatareyouthinking.ogg","stream" )
						butstillthesegameshad = love.audio.newSource( "/externalassets/dialogs/level66/cs/9butstillthesegameshad.ogg","stream" )
						canyouseethatknight = love.audio.newSource( "/externalassets/dialogs/level66/cs/10canyouseethatknight.ogg","stream" )
						ireallydidntknow = love.audio.newSource( "/externalassets/dialogs/level66/cs/12ireallydidntknow.ogg","stream" )
						ohsurethey = love.audio.newSource( "/externalassets/dialogs/level66/cs/14ohsurethey.ogg","stream" )
						thesinglebiggest = love.audio.newSource( "/externalassets/dialogs/level66/cs/16thesinglebiggest.ogg","stream" )
				
				elseif language2=="pl" then
						sothisisalevel = love.audio.newSource( "/externalassets/dialogs/level66/pl/1sothisisalevel.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level66/pl/3canyouseethat.ogg","stream" )
						whatareyouthinking = love.audio.newSource( "/externalassets/dialogs/level66/pl/5whatareyouthinking.ogg","stream" )
						butstillthesegameshad = love.audio.newSource( "/externalassets/dialogs/level66/pl/9butstillthesegameshad.ogg","stream" )
						canyouseethatknight = love.audio.newSource( "/externalassets/dialogs/level66/pl/10canyouseethatknight.ogg","stream" )
						ireallydidntknow = love.audio.newSource( "/externalassets/dialogs/level66/pl/12ireallydidntknow.ogg","stream" )
						ohsurethey = love.audio.newSource( "/externalassets/dialogs/level66/pl/14ohsurethey.ogg","stream" )
						thesinglebiggest = love.audio.newSource( "/externalassets/dialogs/level66/pl/16thesinglebiggest.ogg","stream" )
				end			
					--fish 1
					yeah:setEffect('myEffect')
					thisisanopportunity:setEffect('myEffect')
					wellijust:setEffect('myEffect')
					doyouknow:setEffect('myEffect')
					andimagine:setEffect('myEffect')
					andthatmustbe:setEffect('myEffect')
					thislineof:setEffect('myEffect')
					iwonder:setEffect('myEffect')
		
					--fish 2
					
						sothisisalevel:setEffect('myEffect')
						canyouseethat:setEffect('myEffect')
						whatareyouthinking:setEffect('myEffect')
						butstillthesegameshad:setEffect('myEffect')
						canyouseethatknight:setEffect('myEffect')
						ireallydidntknow:setEffect('myEffect')
						ohsurethey:setEffect('myEffect')
						thesinglebiggest:setEffect('myEffect')
		
		elseif nLevel==67 then
				if language=="en" then
					if accent=="br" then
						holdon = love.audio.newSource( "/externalassets/dialogs/level67/en/3holdon.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level67/en/5doyouthink.ogg","stream" )
						ifindit = love.audio.newSource( "/externalassets/dialogs/level67/en/8ifindit.ogg","stream" )
						whenms = love.audio.newSource( "/externalassets/dialogs/level67/en/9whenms.ogg","stream" )
						themakers = love.audio.newSource( "/externalassets/dialogs/level67/en/14themakers.ogg","stream" )
					elseif accent=="us" then
						holdon = love.audio.newSource( "/externalassets/dialogs/level67/en-us/3holdon.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level67/en-us/5doyouthink.ogg","stream" )
						ifindit = love.audio.newSource( "/externalassets/dialogs/level67/en-us/8ifindit.ogg","stream" )
						whenms = love.audio.newSource( "/externalassets/dialogs/level67/en-us/9whenms.ogg","stream" )
						themakers = love.audio.newSource( "/externalassets/dialogs/level67/en-us/14themakers.ogg","stream" )
					end
				elseif language=="fr" then
					holdon = love.audio.newSource( "/externalassets/dialogs/level67/fr/3holdon.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level67/fr/5doyouthink.ogg","stream" )
					ifindit = love.audio.newSource( "/externalassets/dialogs/level67/fr/8ifindit.ogg","stream" )
					whenms = love.audio.newSource( "/externalassets/dialogs/level67/fr/9whenms.ogg","stream" )
					themakers = love.audio.newSource( "/externalassets/dialogs/level67/fr/14themakers.ogg","stream" )
				elseif language=="es" then
						if accent=="es" then
					elseif accent=="la" then
						holdon = love.audio.newSource( "/externalassets/dialogs/level67/es-la/3holdon.ogg","stream" )
						doyouthink = love.audio.newSource( "/externalassets/dialogs/level67/es-la/5doyouthink.ogg","stream" )
						ifindit = love.audio.newSource( "/externalassets/dialogs/level67/es-la/8ifindit.ogg","stream" )
						whenms = love.audio.newSource( "/externalassets/dialogs/level67/es-la/9whenms.ogg","stream" )
						themakers = love.audio.newSource( "/externalassets/dialogs/level67/es-la/14themakers.ogg","stream" )
					end
			
			
				end
				
				if language2=="en" then
						if accent2=="br" then
					elseif accent2=="us" then
						iamimprisoned = love.audio.newSource( "/externalassets/dialogs/level67/en-us/1iamimprisoned.ogg","stream" )
						howdidthesteel = love.audio.newSource( "/externalassets/dialogs/level67/en-us/2howdidthesteel.ogg","stream" )
						thesearenotpeasants = love.audio.newSource( "/externalassets/dialogs/level67/en-us/4thesearenotpeasants.ogg","stream" )
						iconsiderthis = love.audio.newSource( "/externalassets/dialogs/level67/en-us/6iconsiderthis.ogg","stream" )
						wheredidweget = love.audio.newSource( "/externalassets/dialogs/level67/en-us/7wheredidweget.ogg","stream" )
						whenwedontknow = love.audio.newSource( "/externalassets/dialogs/level67/en-us/10whenwedontknow.ogg","stream" )
						butitsnotso = love.audio.newSource( "/externalassets/dialogs/level67/en-us/12butitsnotso.ogg","stream" )
						andtheywould = love.audio.newSource( "/externalassets/dialogs/level67/en-us/14andtheywould.ogg","stream" )
					end
				elseif language2=="cs" then
						iamimprisoned = love.audio.newSource( "/externalassets/dialogs/level67/cs/1iamimprisoned.ogg","stream" )
						howdidthesteel = love.audio.newSource( "/externalassets/dialogs/level67/cs/2howdidthesteel.ogg","stream" )
						thesearenotpeasants = love.audio.newSource( "/externalassets/dialogs/level67/cs/4thesearenotpeasants.ogg","stream" )
						iconsiderthis = love.audio.newSource( "/externalassets/dialogs/level67/cs/6iconsiderthis.ogg","stream" )
						wheredidweget = love.audio.newSource( "/externalassets/dialogs/level67/cs/7wheredidweget.ogg","stream" )
						whenwedontknow = love.audio.newSource( "/externalassets/dialogs/level67/cs/10whenwedontknow.ogg","stream" )
						butitsnotso = love.audio.newSource( "/externalassets/dialogs/level67/cs/12butitsnotso.ogg","stream" )
						andtheywould = love.audio.newSource( "/externalassets/dialogs/level67/cs/14andtheywould.ogg","stream" )
				elseif language2=="pl" then
						iamimprisoned = love.audio.newSource( "/externalassets/dialogs/level67/pl/1iamimprisoned.ogg","stream" )
						howdidthesteel = love.audio.newSource( "/externalassets/dialogs/level67/pl/2howdidthesteel.ogg","stream" )
						thesearenotpeasants = love.audio.newSource( "/externalassets/dialogs/level67/pl/4thesearenotpeasants.ogg","stream" )
						iconsiderthis = love.audio.newSource( "/externalassets/dialogs/level67/pl/6iconsiderthis.ogg","stream" )
						wheredidweget = love.audio.newSource( "/externalassets/dialogs/level67/pl/7wheredidweget.ogg","stream" )
						whenwedontknow = love.audio.newSource( "/externalassets/dialogs/level67/pl/10whenwedontknow.ogg","stream" )
						butitsnotso = love.audio.newSource( "/externalassets/dialogs/level67/pl/12butitsnotso.ogg","stream" )
						andtheywould = love.audio.newSource( "/externalassets/dialogs/level67/pl/14andtheywould.ogg","stream" )
				end
					--fish 1
					holdon:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					ifindit:setEffect('myEffect')
					whenms:setEffect('myEffect')
					themakers:setEffect('myEffect')
					
					--fish 2
						iamimprisoned:setEffect('myEffect')
						howdidthesteel:setEffect('myEffect')
						thesearenotpeasants:setEffect('myEffect')
						iconsiderthis:setEffect('myEffect')
						wheredidweget:setEffect('myEffect')
						whenwedontknow:setEffect('myEffect')
						butitsnotso:setEffect('myEffect')
						andtheywould:setEffect('myEffect')
		
			elseif nLevel==68 then
				if language=="en" then
					if accent=="br" then
						thisishow = love.audio.newSource( "/externalassets/dialogs/level68/en/1thisishow.ogg","stream" )
						didyoucount = love.audio.newSource( "/externalassets/dialogs/level68/en/7didyoucount.ogg","stream" )
						stop = love.audio.newSource( "/externalassets/dialogs/level68/en/10stop.ogg","stream" )
						sure = love.audio.newSource( "/externalassets/dialogs/level68/en/12sure.ogg","stream" )
						ohno = love.audio.newSource( "/externalassets/dialogs/level68/en/14ohno.ogg","stream" )
						noanimation = love.audio.newSource( "/externalassets/dialogs/level68/en/16noanimation.ogg","stream" )
						nomusic = love.audio.newSource( "/externalassets/dialogs/level68/en/18nomusic.ogg","stream" )
						butfortunately = love.audio.newSource( "/externalassets/dialogs/level68/en/20butfortunately.ogg","stream" )
						waita = love.audio.newSource( "/externalassets/dialogs/level68/en/23waita.ogg","stream" )
						wecant = love.audio.newSource( "/externalassets/dialogs/level68/en/25wecant.ogg","stream" )
						sowellgo = love.audio.newSource( "/externalassets/dialogs/level68/en/27sowellgo.ogg","stream" )
						wearenot = love.audio.newSource( "/externalassets/dialogs/level68/en/29wearenot.ogg","stream" )
						goodness = love.audio.newSource( "/externalassets/dialogs/level68/en/30goodness.ogg","stream" )

					elseif accent=="us" then
						thisishow = love.audio.newSource( "/externalassets/dialogs/level68/en-us/1thisishow.ogg","stream" )
						didyoucount = love.audio.newSource( "/externalassets/dialogs/level68/en-us/7didyoucount.ogg","stream" )
						stop = love.audio.newSource( "/externalassets/dialogs/level68/en-us/10stop.ogg","stream" )
						sure = love.audio.newSource( "/externalassets/dialogs/level68/en-us/12sure.ogg","stream" )
						ohno = love.audio.newSource( "/externalassets/dialogs/level68/en-us/14ohno.ogg","stream" )
						noanimation = love.audio.newSource( "/externalassets/dialogs/level68/en-us/16noanimation.ogg","stream" )
						nomusic = love.audio.newSource( "/externalassets/dialogs/level68/en-us/18nomusic.ogg","stream" )
						butfortunately = love.audio.newSource( "/externalassets/dialogs/level68/en-us/20butfortunately.ogg","stream" )
						waita = love.audio.newSource( "/externalassets/dialogs/level68/en-us/23waita.ogg","stream" )
						wecant = love.audio.newSource( "/externalassets/dialogs/level68/en-us/25wecant.ogg","stream" )
						sowellgo = love.audio.newSource( "/externalassets/dialogs/level68/en-us/27sowellgo.ogg","stream" )
						wearenot = love.audio.newSource( "/externalassets/dialogs/level68/en-us/29wearenot.ogg","stream" )
						goodness = love.audio.newSource( "/externalassets/dialogs/level68/en-us/30goodness.ogg","stream" )

					end
				elseif language=="fr" then
						thisishow = love.audio.newSource( "/externalassets/dialogs/level68/fr/1thisishow.ogg","stream" )
						didyoucount = love.audio.newSource( "/externalassets/dialogs/level68/fr/7didyoucount.ogg","stream" )
						stop = love.audio.newSource( "/externalassets/dialogs/level68/fr/10stop.ogg","stream" )
						sure = love.audio.newSource( "/externalassets/dialogs/level68/fr/12sure.ogg","stream" )
						ohno = love.audio.newSource( "/externalassets/dialogs/level68/fr/14ohno.ogg","stream" )
						noanimation = love.audio.newSource( "/externalassets/dialogs/level68/fr/16noanimation.ogg","stream" )
						nomusic = love.audio.newSource( "/externalassets/dialogs/level68/fr/18nomusic.ogg","stream" )
						butfortunately = love.audio.newSource( "/externalassets/dialogs/level68/fr/20butfortunately.ogg","stream" )
						waita = love.audio.newSource( "/externalassets/dialogs/level68/fr/23waita.ogg","stream" )
						wecant = love.audio.newSource( "/externalassets/dialogs/level68/fr/25wecant.ogg","stream" )
						sowellgo = love.audio.newSource( "/externalassets/dialogs/level68/fr/27sowellgo.ogg","stream" )
						wearenot = love.audio.newSource( "/externalassets/dialogs/level68/fr/29wearenot.ogg","stream" )
						goodness = love.audio.newSource( "/externalassets/dialogs/level68/fr/30goodness.ogg","stream" )

				elseif language=="es" then
						if accent=="es" then
					elseif accent=="la" then
						thisishow = love.audio.newSource( "/externalassets/dialogs/level68/es-la/1thisishow.ogg","stream" )
						didyoucount = love.audio.newSource( "/externalassets/dialogs/level68/es-la/7didyoucount.ogg","stream" )
						stop = love.audio.newSource( "/externalassets/dialogs/level68/es-la/10stop.ogg","stream" )
						sure = love.audio.newSource( "/externalassets/dialogs/level68/es-la/12sure.ogg","stream" )
						ohno = love.audio.newSource( "/externalassets/dialogs/level68/es-la/14ohno.ogg","stream" )
						noanimation = love.audio.newSource( "/externalassets/dialogs/level68/es-la/16noanimation.ogg","stream" )
						nomusic = love.audio.newSource( "/externalassets/dialogs/level68/es-la/18nomusic.ogg","stream" )
						butfortunately = love.audio.newSource( "/externalassets/dialogs/level68/es-la/20butfortunately.ogg","stream" )
						waita = love.audio.newSource( "/externalassets/dialogs/level68/es-la/23waita.ogg","stream" )
						wecant = love.audio.newSource( "/externalassets/dialogs/level68/es-la/25wecant.ogg","stream" )
						sowellgo = love.audio.newSource( "/externalassets/dialogs/level68/es-la/27sowellgo.ogg","stream" )
						wearenot = love.audio.newSource( "/externalassets/dialogs/level68/es-la/29wearenot.ogg","stream" )
						goodness = love.audio.newSource( "/externalassets/dialogs/level68/es-la/30goodness.ogg","stream" )
					end

				end
				
				if language2=="en" then
						if accent2=="br" then
					elseif accent2=="us" then
						wouldntithelp = love.audio.newSource( "/externalassets/dialogs/level68/en-us/2wouldntithelp.ogg","stream" )
						whatifwetryto = love.audio.newSource( "/externalassets/dialogs/level68/en-us/3whatifwetryto.ogg","stream" )
						whataboutshutting = love.audio.newSource( "/externalassets/dialogs/level68/en-us/5whataboutshutting.ogg","stream" )
						whataboutgiving = love.audio.newSource( "/externalassets/dialogs/level68/en-us/6whataboutgiving.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level68/en-us/8canyouseethat.ogg","stream" )
						idontneedtocount = love.audio.newSource( "/externalassets/dialogs/level68/en-us/8idontneedtocount.ogg","stream" )
						thissystemhas = love.audio.newSource( "/externalassets/dialogs/level68/en-us/11thissystemhas.ogg","stream" )
						canyouseethatbig = love.audio.newSource( "/externalassets/dialogs/level68/en-us/13canyouseethatbig.ogg","stream" )
						itranonthe = love.audio.newSource( "/externalassets/dialogs/level68/en-us/15itranonthe.ogg","stream" )
						notalking = love.audio.newSource( "/externalassets/dialogs/level68/en-us/17notalking.ogg","stream" )
						onlyabeep = love.audio.newSource( "/externalassets/dialogs/level68/en-us/19onlyabeep.ogg","stream" )
						butidloketoplayit = love.audio.newSource( "/externalassets/dialogs/level68/en-us/21butidloketoplayit.ogg","stream" )
						icantmovethis = love.audio.newSource( "/externalassets/dialogs/level68/en-us/22icantmovethis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/en-us/24youareright.ogg","stream" )
						yesbutttheplayer = love.audio.newSource( "/externalassets/dialogs/level68/en-us/26yesbuttheplayer.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/en-us/28youareright.ogg","stream" )
						ohmyhetook = love.audio.newSource( "/externalassets/dialogs/level68/en-us/31ohmyhetook.ogg","stream" )
					end
				elseif language2=="cs" then
						wouldntithelp = love.audio.newSource( "/externalassets/dialogs/level68/cs/2wouldntithelp.ogg","stream" )
						whatifwetryto = love.audio.newSource( "/externalassets/dialogs/level68/cs/3whatifwetryto.ogg","stream" )
						whataboutshutting = love.audio.newSource( "/externalassets/dialogs/level68/cs/5whataboutshutting.ogg","stream" )
						whataboutgiving = love.audio.newSource( "/externalassets/dialogs/level68/cs/6whataboutgiving.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level68/cs/8canyouseethat.ogg","stream" )
						idontneedtocount = love.audio.newSource( "/externalassets/dialogs/level68/cs/8idontneedtocount.ogg","stream" )
						thissystemhas = love.audio.newSource( "/externalassets/dialogs/level68/cs/11thissystemhas.ogg","stream" )
						canyouseethatbig = love.audio.newSource( "/externalassets/dialogs/level68/cs/13canyouseethatbig.ogg","stream" )
						itranonthe = love.audio.newSource( "/externalassets/dialogs/level68/cs/15itranonthe.ogg","stream" )
						notalking = love.audio.newSource( "/externalassets/dialogs/level68/cs/17notalking.ogg","stream" )
						onlyabeep = love.audio.newSource( "/externalassets/dialogs/level68/cs/19onlyabeep.ogg","stream" )
						butidloketoplayit = love.audio.newSource( "/externalassets/dialogs/level68/cs/21butidloketoplayit.ogg","stream" )
						icantmovethis = love.audio.newSource( "/externalassets/dialogs/level68/cs/22icantmovethis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/cs/24youareright.ogg","stream" )
						yesbutttheplayer = love.audio.newSource( "/externalassets/dialogs/level68/cs/26yesbuttheplayer.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/cs/28youareright.ogg","stream" )
						ohmyhetook = love.audio.newSource( "/externalassets/dialogs/level68/cs/31ohmyhetook.ogg","stream" )
				
				elseif language2=="pl" then
						wouldntithelp = love.audio.newSource( "/externalassets/dialogs/level68/pl/2wouldntithelp.ogg","stream" )
						whatifwetryto = love.audio.newSource( "/externalassets/dialogs/level68/pl/3whatifwetryto.ogg","stream" )
						whataboutshutting = love.audio.newSource( "/externalassets/dialogs/level68/pl/5whataboutshutting.ogg","stream" )
						whataboutgiving = love.audio.newSource( "/externalassets/dialogs/level68/pl/6whataboutgiving.ogg","stream" )
						canyouseethat = love.audio.newSource( "/externalassets/dialogs/level68/pl/8canyouseethat.ogg","stream" )
						idontneedtocount = love.audio.newSource( "/externalassets/dialogs/level68/pl/8idontneedtocount.ogg","stream" )
						thissystemhas = love.audio.newSource( "/externalassets/dialogs/level68/pl/11thissystemhas.ogg","stream" )
						canyouseethatbig = love.audio.newSource( "/externalassets/dialogs/level68/pl/13canyouseethatbig.ogg","stream" )
						itranonthe = love.audio.newSource( "/externalassets/dialogs/level68/pl/15itranonthe.ogg","stream" )
						notalking = love.audio.newSource( "/externalassets/dialogs/level68/pl/17notalking.ogg","stream" )
						onlyabeep = love.audio.newSource( "/externalassets/dialogs/level68/pl/19onlyabeep.ogg","stream" )
						butidloketoplayit = love.audio.newSource( "/externalassets/dialogs/level68/pl/21butidloketoplayit.ogg","stream" )
						icantmovethis = love.audio.newSource( "/externalassets/dialogs/level68/pl/22icantmovethis.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/pl/24youareright.ogg","stream" )
						yesbutttheplayer = love.audio.newSource( "/externalassets/dialogs/level68/pl/26yesbuttheplayer.ogg","stream" )
						youareright = love.audio.newSource( "/externalassets/dialogs/level68/pl/28youareright.ogg","stream" )
						ohmyhetook = love.audio.newSource( "/externalassets/dialogs/level68/pl/31ohmyhetook.ogg","stream" )
				end
						--fish 1
						thisishow:setEffect('myEffect')
						didyoucount:setEffect('myEffect')
						stop:setEffect('myEffect')
						sure:setEffect('myEffect')
						ohno:setEffect('myEffect')
						noanimation:setEffect('myEffect')
						nomusic:setEffect('myEffect')
						butfortunately:setEffect('myEffect')
						waita:setEffect('myEffect')
						wecant:setEffect('myEffect')
						sowellgo:setEffect('myEffect')
						wearenot:setEffect('myEffect')
						goodness:setEffect('myEffect')
						
						--fish 2
						wouldntithelp:setEffect('myEffect')
						whatifwetryto:setEffect('myEffect')
						whataboutshutting:setEffect('myEffect')
						whataboutgiving:setEffect('myEffect')
						canyouseethat:setEffect('myEffect')
						idontneedtocount:setEffect('myEffect')
						thissystemhas:setEffect('myEffect')
						canyouseethatbig:setEffect('myEffect')
						itranonthe:setEffect('myEffect')
						notalking:setEffect('myEffect')
						onlyabeep:setEffect('myEffect')
						butidloketoplayit:setEffect('myEffect')
						icantmovethis:setEffect('myEffect')
						youareright:setEffect('myEffect')
						yesbutttheplayer:setEffect('myEffect')
						youareright:setEffect('myEffect')
						ohmyhetook:setEffect('myEffect')
						
			elseif nLevel==69 then
				if language=="en" then
					if accent=="br" then
						imcompletely = love.audio.newSource( "/externalassets/dialogs/level69/en/1imcompletely.ogg","stream" )
						theremustbe = love.audio.newSource( "/externalassets/dialogs/level69/en/3theremustbe.ogg","stream" )
						thereare = love.audio.newSource( "/externalassets/dialogs/level69/en/4thereare.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level69/en/5whatdo.ogg","stream" )
						whatsdown = love.audio.newSource( "/externalassets/dialogs/level69/en/6whatsdown.ogg","stream" )
						hey = love.audio.newSource( "/externalassets/dialogs/level69/en/12hey.ogg","stream" )
						wehave = love.audio.newSource( "/externalassets/dialogs/level69/en/15wehave.ogg","stream" )
					elseif accent=="us" then
						imcompletely = love.audio.newSource( "/externalassets/dialogs/level69/en-us/1imcompletely.ogg","stream" )
						theremustbe = love.audio.newSource( "/externalassets/dialogs/level69/en-us/3theremustbe.ogg","stream" )
						thereare = love.audio.newSource( "/externalassets/dialogs/level69/en-us/4thereare.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level69/en-us/5whatdo.ogg","stream" )
						whatsdown = love.audio.newSource( "/externalassets/dialogs/level69/en-us/6whatsdown.ogg","stream" )
						hey = love.audio.newSource( "/externalassets/dialogs/level69/en-us/12hey.ogg","stream" )
						wehave = love.audio.newSource( "/externalassets/dialogs/level69/en-us/15wehave.ogg","stream" )
					end
				elseif language=="fr" then
				
					imcompletely = love.audio.newSource( "/externalassets/dialogs/level69/fr/1imcompletely.ogg","stream" )
					theremustbe = love.audio.newSource( "/externalassets/dialogs/level69/fr/3theremustbe.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level69/fr/4thereare.ogg","stream" )
					whatdo = love.audio.newSource( "/externalassets/dialogs/level69/fr/5whatdo.ogg","stream" )
					whatsdown = love.audio.newSource( "/externalassets/dialogs/level69/fr/6whatsdown.ogg","stream" )
					hey = love.audio.newSource( "/externalassets/dialogs/level69/fr/12hey.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level69/fr/15wehave.ogg","stream" )
					
					
				elseif language=="es" then
						if accent=="es" then
					elseif accent=="la" then
						imcompletely = love.audio.newSource( "/externalassets/dialogs/level69/es-la/1imcompletely.ogg","stream" )
						theremustbe = love.audio.newSource( "/externalassets/dialogs/level69/es-la/3theremustbe.ogg","stream" )
						thereare = love.audio.newSource( "/externalassets/dialogs/level69/es-la/4thereare.ogg","stream" )
						whatdo = love.audio.newSource( "/externalassets/dialogs/level69/es-la/5whatdo.ogg","stream" )
						whatsdown = love.audio.newSource( "/externalassets/dialogs/level69/es-la/6whatsdown.ogg","stream" )
						hey = love.audio.newSource( "/externalassets/dialogs/level69/es-la/12hey.ogg","stream" )
						wehave = love.audio.newSource( "/externalassets/dialogs/level69/es-la/15wehave.ogg","stream" )
					end
				end

				if language2=="en" then
						if accent2=="br" then
					elseif accent2=="us" then
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level69/en-us/2whatareyoudoing.ogg","stream" )
						therearesomeprinted = love.audio.newSource( "/externalassets/dialogs/level69/en-us/7therearesomeprinted.ogg","stream" )
						therearesomeintegrated = love.audio.newSource( "/externalassets/dialogs/level69/en-us/8therearesomeintegrated.ogg","stream" )
						iamsurroundedby = love.audio.newSource( "/externalassets/dialogs/level69/en-us/9iamsurroundedby.ogg","stream" )
						computerstheyve = love.audio.newSource( "/externalassets/dialogs/level69/en-us/10computerstheyve.ogg","stream" )
						canitwillyou = love.audio.newSource( "/externalassets/dialogs/level69/en-us/13canitwillyou.ogg","stream" )
						getout = love.audio.newSource( "/externalassets/dialogs/level69/en-us/14getout.ogg","stream" )
						itdoesntseem = love.audio.newSource( "/externalassets/dialogs/level69/en-us/16itdoesntseem.ogg","stream" )
					end
				elseif language2=="cs" then
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level69/cs/2whatareyoudoing.ogg","stream" )
						therearesomeprinted = love.audio.newSource( "/externalassets/dialogs/level69/cs/7therearesomeprinted.ogg","stream" )
						therearesomeintegrated = love.audio.newSource( "/externalassets/dialogs/level69/cs/8therearesomeintegrated.ogg","stream" )
						iamsurroundedby = love.audio.newSource( "/externalassets/dialogs/level69/cs/9iamsurroundedby.ogg","stream" )
						computerstheyve = love.audio.newSource( "/externalassets/dialogs/level69/cs/10computerstheyve.ogg","stream" )
						canitwillyou = love.audio.newSource( "/externalassets/dialogs/level69/cs/13canitwillyou.ogg","stream" )
						getout = love.audio.newSource( "/externalassets/dialogs/level69/cs/14getout.ogg","stream" )
						itdoesntseem = love.audio.newSource( "/externalassets/dialogs/level69/cs/16itdoesntseem.ogg","stream" )
				elseif language2=="pl" then
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level69/pl/2whatareyoudoing.ogg","stream" )
						therearesomeprinted = love.audio.newSource( "/externalassets/dialogs/level69/pl/7therearesomeprinted.ogg","stream" )
						therearesomeintegrated = love.audio.newSource( "/externalassets/dialogs/level69/pl/8therearesomeintegrated.ogg","stream" )
						iamsurroundedby = love.audio.newSource( "/externalassets/dialogs/level69/pl/9iamsurroundedby.ogg","stream" )
						computerstheyve = love.audio.newSource( "/externalassets/dialogs/level69/pl/10computerstheyve.ogg","stream" )
						canitwillyou = love.audio.newSource( "/externalassets/dialogs/level69/pl/13canitwillyou.ogg","stream" )
						getout = love.audio.newSource( "/externalassets/dialogs/level69/pl/14getout.ogg","stream" )
						itdoesntseem = love.audio.newSource( "/externalassets/dialogs/level69/pl/16itdoesntseem.ogg","stream" )
				end
				
					--fish 1
					imcompletely:setEffect('myEffect')
					theremustbe:setEffect('myEffect')
					thereare:setEffect('myEffect')
					whatdo:setEffect('myEffect')
					whatsdown:setEffect('myEffect')
					hey:setEffect('myEffect')
					wehave:setEffect('myEffect')
					
					--fish 2
						whatareyoudoing:setEffect('myEffect')
						therearesomeprinted:setEffect('myEffect')
						therearesomeintegrated:setEffect('myEffect')
						iamsurroundedby:setEffect('myEffect')
						computerstheyve:setEffect('myEffect')
						canitwillyou:setEffect('myEffect')
						getout:setEffect('myEffect')
						itdoesntseem:setEffect('myEffect')
					
					
					
					
		elseif nLevel==70 then
			if language=="en" then
			
			-- end
			thegreatestearthquake  = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/1thegreatestearthquake.ogg","stream" )
			whendragonslair = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/2whendragonslair.ogg","stream" )
			dragonslairplus = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/3dragonslairplus.ogg","stream" )
			theworldof = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/4theworldof.ogg","stream" )
			dragonslairplus2 = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/5dragonslairplus.ogg","stream" )
			itsanewgame = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/6itsanewgame.ogg","stream" )
			plentyofnew = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/7plentyofnew.ogg","stream" )
			newoccupations = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/8newoccupations.ogg","stream" )
			newsystemofcharacter = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/9newsystemofcharacter.ogg","stream" )
			whereinstead = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/10whereinstead.ogg","stream" )
			newelegant = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/11newelegant.ogg","stream" )
			agamefor = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/12agamefor.ogg","stream" )
			gameshavechanged = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/13gameshavechanged.ogg","stream" )
			thisiswhatwe = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/14thisiswhatwe.ogg","stream" )
			weareschocked = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/15weareschocked.ogg","stream" )
			youmustprevent = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/16youmustprevent.ogg","stream" )
			andsubscribe = love.audio.newSource( "/externalassets/ends/8secretcomputer/audios/17andsubscribe.ogg","stream" )
		
			
					if accent=="br" then
					itlooks = love.audio.newSource( "/externalassets/dialogs/level70/en/1itlooks.ogg","stream" )
					lookhow = love.audio.newSource( "/externalassets/dialogs/level70/en/4lookhow.ogg","stream" )
					itwillbe = love.audio.newSource( "/externalassets/dialogs/level70/en/7itwillbe.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level70/en/8idontthink.ogg","stream" )
					shutup = love.audio.newSource( "/externalassets/dialogs/level70/en/9shutup.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level70/en/10canyousee.ogg","stream" )
					whynot = love.audio.newSource( "/externalassets/dialogs/level70/en/13whynot.ogg","stream" )
					shallwe = love.audio.newSource( "/externalassets/dialogs/level70/en/15shallwe.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level70/en/16never.ogg","stream" )
					suit = love.audio.newSource( "/externalassets/dialogs/level70/en/17suit.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level70/en/19idont.ogg","stream" )
					thesemust = love.audio.newSource( "/externalassets/dialogs/level70/en/21thesemust.ogg","stream" )
					aboveall = love.audio.newSource( "/externalassets/dialogs/level70/en/23aboveall.ogg","stream" )
					
					elseif accent=="us" then
					itlooks = love.audio.newSource( "/externalassets/dialogs/level70/en-us/1itlooks.ogg","stream" )
					lookhow = love.audio.newSource( "/externalassets/dialogs/level70/en-us/4lookhow.ogg","stream" )
					itwillbe = love.audio.newSource( "/externalassets/dialogs/level70/en-us/7itwillbe.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level70/en-us/8idontthink.ogg","stream" )
					shutup = love.audio.newSource( "/externalassets/dialogs/level70/en-us/9shutup.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level70/en-us/10canyousee.ogg","stream" )
					whynot = love.audio.newSource( "/externalassets/dialogs/level70/en-us/13whynot.ogg","stream" )
					shallwe = love.audio.newSource( "/externalassets/dialogs/level70/en-us/15shallwe.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level70/en-us/16never.ogg","stream" )
					suit = love.audio.newSource( "/externalassets/dialogs/level70/en-us/17suit.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level70/en-us/19idont.ogg","stream" )
					thesemust = love.audio.newSource( "/externalassets/dialogs/level70/en-us/21thesemust.ogg","stream" )
					aboveall = love.audio.newSource( "/externalassets/dialogs/level70/en-us/23aboveall.ogg","stream" )
					end
			elseif language=="fr" then
					itlooks = love.audio.newSource( "/externalassets/dialogs/level70/fr/1itlooks.ogg","stream" )
					lookhow = love.audio.newSource( "/externalassets/dialogs/level70/fr/4lookhow.ogg","stream" )
					itwillbe = love.audio.newSource( "/externalassets/dialogs/level70/fr/7itwillbe.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level70/fr/8idontthink.ogg","stream" )
					shutup = love.audio.newSource( "/externalassets/dialogs/level70/fr/9shutup.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level70/fr/10canyousee.ogg","stream" )
					whynot = love.audio.newSource( "/externalassets/dialogs/level70/fr/13whynot.ogg","stream" )
					shallwe = love.audio.newSource( "/externalassets/dialogs/level70/fr/15shallwe.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level70/fr/16never.ogg","stream" )
					suit = love.audio.newSource( "/externalassets/dialogs/level70/fr/17suit.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level70/fr/19idont.ogg","stream" )
					thesemust = love.audio.newSource( "/externalassets/dialogs/level70/fr/21thesemust.ogg","stream" )
					aboveall = love.audio.newSource( "/externalassets/dialogs/level70/fr/23aboveall.ogg","stream" )
					
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					itlooks = love.audio.newSource( "/externalassets/dialogs/level70/es-la/1itlooks.ogg","stream" )
					lookhow = love.audio.newSource( "/externalassets/dialogs/level70/es-la/4lookhow.ogg","stream" )
					itwillbe = love.audio.newSource( "/externalassets/dialogs/level70/es-la/7itwillbe.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level70/es-la/8idontthink.ogg","stream" )
					shutup = love.audio.newSource( "/externalassets/dialogs/level70/es-la/9shutup.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level70/es-la/10canyousee.ogg","stream" )
					whynot = love.audio.newSource( "/externalassets/dialogs/level70/es-la/13whynot.ogg","stream" )
					shallwe = love.audio.newSource( "/externalassets/dialogs/level70/es-la/15shallwe.ogg","stream" )
					never = love.audio.newSource( "/externalassets/dialogs/level70/es-la/16never.ogg","stream" )
					suit = love.audio.newSource( "/externalassets/dialogs/level70/es-la/17suit.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level70/es-la/19idont.ogg","stream" )
					thesemust = love.audio.newSource( "/externalassets/dialogs/level70/es-la/21thesemust.ogg","stream" )
					aboveall = love.audio.newSource( "/externalassets/dialogs/level70/es-la/23aboveall.ogg","stream" )
				end
					
		end
		if language2=="en" then
			
				if accent2=="br" then
			elseif accent2=="us" then
				doyouthinkthisisit = love.audio.newSource( "/externalassets/dialogs/level70/en-us/2doyouthinkthisisit.ogg","stream" )
				howcoulditall = love.audio.newSource( "/externalassets/dialogs/level70/en-us/3howcoulditall.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level70/en-us/5thatsbetter.ogg","stream" )
				doyouthinkwe = love.audio.newSource( "/externalassets/dialogs/level70/en-us/6doyouthinkwe.ogg","stream" )
				dontbealways = love.audio.newSource( "/externalassets/dialogs/level70/en-us/11dontbealways.ogg","stream" )
				wouldntitbe = love.audio.newSource( "/externalassets/dialogs/level70/en-us/12wouldntitbe.ogg","stream" )
				ohdarn = love.audio.newSource( "/externalassets/dialogs/level70/en-us/14ohdarn.ogg","stream" )
				whatifthereis = love.audio.newSource( "/externalassets/dialogs/level70/en-us/18whatifthereis.ogg","stream" )
				whatkindoflittle = love.audio.newSource( "/externalassets/dialogs/level70/en-us/20whatkindoflittle.ogg","stream" )
				yucksome = love.audio.newSource( "/externalassets/dialogs/level70/en-us/22yucksome.ogg","stream" )
				ourgoalistopush = love.audio.newSource( "/externalassets/dialogs/level70/en-us/24ourgoalistopush.ogg","stream" )
			end
		elseif language2=="cs" then
				doyouthinkthisisit = love.audio.newSource( "/externalassets/dialogs/level70/cs/2doyouthinkthisisit.ogg","stream" )
				howcoulditall = love.audio.newSource( "/externalassets/dialogs/level70/cs/3howcoulditall.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level70/cs/5thatsbetter.ogg","stream" )
				doyouthinkwe = love.audio.newSource( "/externalassets/dialogs/level70/cs/6doyouthinkwe.ogg","stream" )
				dontbealways = love.audio.newSource( "/externalassets/dialogs/level70/cs/11dontbealways.ogg","stream" )
				wouldntitbe = love.audio.newSource( "/externalassets/dialogs/level70/cs/12wouldntitbe.ogg","stream" )
				ohdarn = love.audio.newSource( "/externalassets/dialogs/level70/cs/14ohdarn.ogg","stream" )
				whatifthereis = love.audio.newSource( "/externalassets/dialogs/level70/cs/18whatifthereis.ogg","stream" )
				whatkindoflittle = love.audio.newSource( "/externalassets/dialogs/level70/cs/20whatkindoflittle.ogg","stream" )
				yucksome = love.audio.newSource( "/externalassets/dialogs/level70/cs/22yucksome.ogg","stream" )
				ourgoalistopush = love.audio.newSource( "/externalassets/dialogs/level70/cs/24ourgoalistopush.ogg","stream" )	
		
		elseif language2=="pl" then
				doyouthinkthisisit = love.audio.newSource( "/externalassets/dialogs/level70/pl/2doyouthinkthisisit.ogg","stream" )
				howcoulditall = love.audio.newSource( "/externalassets/dialogs/level70/pl/3howcoulditall.ogg","stream" )
				thatsbetter = love.audio.newSource( "/externalassets/dialogs/level70/pl/5thatsbetter.ogg","stream" )
				doyouthinkwe = love.audio.newSource( "/externalassets/dialogs/level70/pl/6doyouthinkwe.ogg","stream" )
				dontbealways = love.audio.newSource( "/externalassets/dialogs/level70/pl/11dontbealways.ogg","stream" )
				wouldntitbe = love.audio.newSource( "/externalassets/dialogs/level70/pl/12wouldntitbe.ogg","stream" )
				ohdarn = love.audio.newSource( "/externalassets/dialogs/level70/pl/14ohdarn.ogg","stream" )
				whatifthereis = love.audio.newSource( "/externalassets/dialogs/level70/pl/18whatifthereis.ogg","stream" )
				whatkindoflittle = love.audio.newSource( "/externalassets/dialogs/level70/pl/20whatkindoflittle.ogg","stream" )
				yucksome = love.audio.newSource( "/externalassets/dialogs/level70/pl/22yucksome.ogg","stream" )
				ourgoalistopush = love.audio.newSource( "/externalassets/dialogs/level70/pl/24ourgoalistopush.ogg","stream" )	
		end
		
				--end
					thegreatestearthquake:setEffect('myEffect')
					whendragonslair:setEffect('myEffect')
					dragonslairplus:setEffect('myEffect')
					theworldof:setEffect('myEffect')
					dragonslairplus2:setEffect('myEffect')
					itsanewgame:setEffect('myEffect')
					plentyofnew:setEffect('myEffect')
					newoccupations:setEffect('myEffect')
					newsystemofcharacter:setEffect('myEffect')
					whereinstead:setEffect('myEffect')
					newelegant:setEffect('myEffect')
					agamefor:setEffect('myEffect')
					gameshavechanged:setEffect('myEffect')
					thisiswhatwe:setEffect('myEffect')
					weareschocked:setEffect('myEffect')
					youmustprevent:setEffect('myEffect')
					andsubscribe:setEffect('myEffect')
					
				--fish 1
					itlooks:setEffect('myEffect')
					lookhow:setEffect('myEffect')
					itwillbe:setEffect('myEffect')
					idontthink:setEffect('myEffect')
					shutup:setEffect('myEffect')
					canyousee:setEffect('myEffect')
					whynot:setEffect('myEffect')
					shallwe:setEffect('myEffect')
					never:setEffect('myEffect')
					suit:setEffect('myEffect')
					idont:setEffect('myEffect')
					thesemust:setEffect('myEffect')
					aboveall:setEffect('myEffect')
					
			--fish 2
			
					doyouthinkthisisit:setEffect('myEffect')
					howcoulditall:setEffect('myEffect')
					thatsbetter:setEffect('myEffect')
					doyouthinkwe:setEffect('myEffect')
					dontbealways:setEffect('myEffect')
					wouldntitbe:setEffect('myEffect')
					ohdarn:setEffect('myEffect')
					whatifthereis:setEffect('myEffect')
					whatkindoflittle:setEffect('myEffect')
					yucksome:setEffect('myEffect')
					ourgoalistopush:setEffect('myEffect')
					
					
		elseif nLevel==71 then
			if language=="en" then
				if accent=="br" then
					weare = love.audio.newSource( "/externalassets/dialogs/level71/en/1weare.ogg","stream" )
					whyareyousosure = love.audio.newSource( "/externalassets/dialogs/level71/en/2whyareyousosure.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level71/en/8idontknow.ogg","stream" )
					howwould = love.audio.newSource( "/externalassets/dialogs/level71/en/10howwould.ogg","stream" )
					youthink = love.audio.newSource( "/externalassets/dialogs/level71/en/12youthink.ogg","stream" )
					thatcouldbe = love.audio.newSource( "/externalassets/dialogs/level71/en/14thatcouldbe.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level71/en/16idont.ogg","stream" )
					thenweshould = love.audio.newSource( "/externalassets/dialogs/level71/en/18thenweshould.ogg","stream" )
					where = love.audio.newSource( "/externalassets/dialogs/level71/en/20where.ogg","stream" )
					perhaps = love.audio.newSource( "/externalassets/dialogs/level71/en/22perhaps.ogg","stream" )
					itisan = love.audio.newSource( "/externalassets/dialogs/level71/en/23itisan.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level71/en/24idontthink.ogg","stream" )
					
				elseif accent=="us" then
					weare = love.audio.newSource( "/externalassets/dialogs/level71/en-us/1weare.ogg","stream" )
					whyareyousosure = love.audio.newSource( "/externalassets/dialogs/level71/en-us/2whyareyousosure.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level71/en-us/8idontknow.ogg","stream" )
					howwould = love.audio.newSource( "/externalassets/dialogs/level71/en-us/10howwould.ogg","stream" )
					youthink = love.audio.newSource( "/externalassets/dialogs/level71/en-us/12youthink.ogg","stream" )
					thatcouldbe = love.audio.newSource( "/externalassets/dialogs/level71/en-us/14thatcouldbe.ogg","stream" )
					idont = love.audio.newSource( "/externalassets/dialogs/level71/en-us/16idont.ogg","stream" )
					thenweshould = love.audio.newSource( "/externalassets/dialogs/level71/en-us/18thenweshould.ogg","stream" )
					where = love.audio.newSource( "/externalassets/dialogs/level71/en-us/20where.ogg","stream" )
					perhaps = love.audio.newSource( "/externalassets/dialogs/level71/en-us/22perhaps.ogg","stream" )
					itisan = love.audio.newSource( "/externalassets/dialogs/level71/en-us/23itisan.ogg","stream" )
					idontthink = love.audio.newSource( "/externalassets/dialogs/level71/en-us/24idontthink.ogg","stream" )
					lookatthecrystal = love.audio.newSource( "/externalassets/dialogs/level71/en-us/26lookatthecrystal.ogg","stream" )
					dilicium = love.audio.newSource( "/externalassets/dialogs/level71/en-us/28dilicium.ogg","stream" )
					dihydrogenmonoxide = love.audio.newSource( "/externalassets/dialogs/level71/en-us/29dihydrogenmonoxide.ogg","stream" )
					sodiumchloride = love.audio.newSource( "/externalassets/dialogs/level71/en-us/30sodiumchloride.ogg","stream" )
					nonferrous = love.audio.newSource( "/externalassets/dialogs/level71/en-us/31nonferrous.ogg","stream" )
					glasswithcolored = love.audio.newSource( "/externalassets/dialogs/level71/en-us/32glasswithcolored.ogg","stream" )
					analienalloy = love.audio.newSource( "/externalassets/dialogs/level71/en-us/33analienalloy.ogg","stream" )
					whydothemagnets = love.audio.newSource( "/externalassets/dialogs/level71/en-us/35whydothemagnets.ogg","stream" )
					isee = love.audio.newSource( "/externalassets/dialogs/level71/en-us/37isee.ogg","stream" )
					anotherfuel = love.audio.newSource( "/externalassets/dialogs/level71/en-us/38anotherfuel.ogg","stream" )
					perhapsweshould = love.audio.newSource( "/externalassets/dialogs/level71/en-us/40perhapsweshould.ogg","stream" )
					anunidentified = love.audio.newSource( "/externalassets/dialogs/level71/en-us/44anunidentified.ogg","stream" )
					asisasiditis = love.audio.newSource( "/externalassets/dialogs/level71/en-us/46asisaiditis.ogg","stream" )
					ifiknewthat = love.audio.newSource( "/externalassets/dialogs/level71/en-us/48ifiknewthat.ogg","stream" )
					thatticles = love.audio.newSource( "/externalassets/dialogs/level71/en-us/52thattickles.ogg","stream" )
					thatprickles = love.audio.newSource( "/externalassets/dialogs/level71/en-us/53thatprickles.ogg","stream" )
					thatishot = love.audio.newSource( "/externalassets/dialogs/level71/en-us/54thatishot.ogg","stream" )
					oops = love.audio.newSource( "/externalassets/dialogs/level71/en-us/59oops.ogg","stream" )
					itwasntmyfault = love.audio.newSource( "/externalassets/dialogs/level71/en-us/62itwasntmyfault.ogg","stream" )
					illtrytoremember = love.audio.newSource( "/externalassets/dialogs/level71/en-us/63illtrytoremember.ogg","stream" )
					whyarent = love.audio.newSource( "/externalassets/dialogs/level71/en-us/65whyarent.ogg","stream" )
					
					
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					wewillfind = love.audio.newSource( "/externalassets/dialogs/level71/en-us/2wewillfind.ogg","stream" )
					wevealways = love.audio.newSource( "/externalassets/dialogs/level71/en-us/4wevealways.ogg","stream" )
					wehavecome = love.audio.newSource( "/externalassets/dialogs/level71/en-us/5wehavecome.ogg","stream" )
					thetopicof = love.audio.newSource( "/externalassets/dialogs/level71/en-us/6thetopicof.ogg","stream" )
					whatwould = love.audio.newSource( "/externalassets/dialogs/level71/en-us/7whatwould.ogg","stream" )
					weshouldturn = love.audio.newSource( "/externalassets/dialogs/level71/en-us/9weshouldturn.ogg","stream" )
					soletsmove = love.audio.newSource( "/externalassets/dialogs/level71/en-us/11soletsmove.ogg","stream" )
					atleastitshould = love.audio.newSource( "/externalassets/dialogs/level71/en-us/13atleastitshould.ogg","stream" )
					perhapswehave = love.audio.newSource( "/externalassets/dialogs/level71/en-us/15perhapswehave.ogg","stream" )
					whatifthemagnets = love.audio.newSource( "/externalassets/dialogs/level71/en-us/17whatifthemagnets.ogg","stream" )
					lookalien = love.audio.newSource( "/externalassets/dialogs/level71/en-us/19lookalien.ogg","stream" )
					wellthesmall = love.audio.newSource( "/externalassets/dialogs/level71/en-us/21wellthesmall.ogg","stream" )
					anywayitisgarbage = love.audio.newSource( "/externalassets/dialogs/level71/en-us/25anywayitisgarbage.ogg","stream" )
					nicewhatmaterial = love.audio.newSource( "/externalassets/dialogs/level71/en-us/27nicewhatmaterial.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level71/en-us/34idontthinkso.ogg","stream" )
					theyarealien = love.audio.newSource( "/externalassets/dialogs/level71/en-us/36theyarealien.ogg","stream" )
					ihopeweare = love.audio.newSource( "/externalassets/dialogs/level71/en-us/39ihopeweare.ogg","stream" )
					thenwewouldstill = love.audio.newSource( "/externalassets/dialogs/level71/en-us/41thenwewouldstill.ogg","stream" )
					perhapsitwasnot = love.audio.newSource( "/externalassets/dialogs/level71/en-us/42perhapsitwasnot.ogg","stream" )
					whatisthatlonelything = love.audio.newSource( "/externalassets/dialogs/level71/en-us/43whatisthatlonelything.ogg","stream" )
					theangularonewhich = love.audio.newSource( "/externalassets/dialogs/level71/en-us/45theangularonewhich.ogg","stream" )
					yesbutwhatis = love.audio.newSource( "/externalassets/dialogs/level71/en-us/47yesbutwhatis.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level71/en-us/49ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level71/en-us/50ouch.ogg","stream" )
					ouchy = love.audio.newSource( "/externalassets/dialogs/level71/en-us/51ouchy.ogg","stream" )
					payattention = love.audio.newSource( "/externalassets/dialogs/level71/en-us/55payattention.ogg","stream" )
					thatweapon = love.audio.newSource( "/externalassets/dialogs/level71/en-us/56thatweapon.ogg","stream" )
					lookatthehole = love.audio.newSource( "/externalassets/dialogs/level71/en-us/57lookatthehole.ogg","stream" )
					younearly = love.audio.newSource( "/externalassets/dialogs/level71/en-us/58younearly.ogg","stream" )
					nowonderthat = love.audio.newSource( "/externalassets/dialogs/level71/en-us/60nowonderthat.ogg","stream" )
					youshouldntuse = love.audio.newSource( "/externalassets/dialogs/level71/en-us/61youshouldntuse.ogg","stream" )
					thegoodthing = love.audio.newSource( "/externalassets/dialogs/level71/en-us/64thegoodthing.ogg","stream" )
					iwonderthesame = love.audio.newSource( "/externalassets/dialogs/level71/en-us/66iwonderthesame.ogg","stream" )
				end
			elseif language2=="cs" then
					wewillfind = love.audio.newSource( "/externalassets/dialogs/level71/cs/2wewillfind.ogg","stream" )
					wevealways = love.audio.newSource( "/externalassets/dialogs/level71/cs/4wevealways.ogg","stream" )
					wehavecome = love.audio.newSource( "/externalassets/dialogs/level71/cs/5wehavecome.ogg","stream" )
					thetopicof = love.audio.newSource( "/externalassets/dialogs/level71/cs/6thetopicof.ogg","stream" )
					whatwould = love.audio.newSource( "/externalassets/dialogs/level71/cs/7whatwould.ogg","stream" )
					weshouldturn = love.audio.newSource( "/externalassets/dialogs/level71/cs/9weshouldturn.ogg","stream" )
					soletsmove = love.audio.newSource( "/externalassets/dialogs/level71/cs/11soletsmove.ogg","stream" )
					atleastitshould = love.audio.newSource( "/externalassets/dialogs/level71/cs/13atleastitshould.ogg","stream" )
					perhapswehave = love.audio.newSource( "/externalassets/dialogs/level71/cs/15perhapswehave.ogg","stream" )
					whatifthemagnets = love.audio.newSource( "/externalassets/dialogs/level71/cs/17whatifthemagnets.ogg","stream" )
					lookalien = love.audio.newSource( "/externalassets/dialogs/level71/cs/19lookalien.ogg","stream" )
					wellthesmall = love.audio.newSource( "/externalassets/dialogs/level71/cs/21wellthesmall.ogg","stream" )
					anywayitisgarbage = love.audio.newSource( "/externalassets/dialogs/level71/cs/25anywayitisgarbage.ogg","stream" )
					nicewhatmaterial = love.audio.newSource( "/externalassets/dialogs/level71/cs/27nicewhatmaterial.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level71/cs/34idontthinkso.ogg","stream" )
					theyarealien = love.audio.newSource( "/externalassets/dialogs/level71/cs/36theyarealien.ogg","stream" )
					ihopeweare = love.audio.newSource( "/externalassets/dialogs/level71/cs/39ihopeweare.ogg","stream" )
					thenwewouldstill = love.audio.newSource( "/externalassets/dialogs/level71/cs/41thenwewouldstill.ogg","stream" )
					perhapsitwasnot = love.audio.newSource( "/externalassets/dialogs/level71/cs/42perhapsitwasnot.ogg","stream" )
					whatisthatlonelything = love.audio.newSource( "/externalassets/dialogs/level71/cs/43whatisthatlonelything.ogg","stream" )
					theangularonewhich = love.audio.newSource( "/externalassets/dialogs/level71/cs/45theangularonewhich.ogg","stream" )
					yesbutwhatis = love.audio.newSource( "/externalassets/dialogs/level71/cs/47yesbutwhatis.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level71/cs/49ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level71/cs/50ouch.ogg","stream" )
					ouchy = love.audio.newSource( "/externalassets/dialogs/level71/cs/51ouchy.ogg","stream" )
					payattention = love.audio.newSource( "/externalassets/dialogs/level71/cs/55payattention.ogg","stream" )
					thatweapon = love.audio.newSource( "/externalassets/dialogs/level71/cs/56thatweapon.ogg","stream" )
					lookatthehole = love.audio.newSource( "/externalassets/dialogs/level71/cs/57lookatthehole.ogg","stream" )
					younearly = love.audio.newSource( "/externalassets/dialogs/level71/cs/58younearly.ogg","stream" )
					nowonderthat = love.audio.newSource( "/externalassets/dialogs/level71/cs/60nowonderthat.ogg","stream" )
					youshouldntuse = love.audio.newSource( "/externalassets/dialogs/level71/cs/61youshouldntuse.ogg","stream" )
					thegoodthing = love.audio.newSource( "/externalassets/dialogs/level71/cs/64thegoodthing.ogg","stream" )
					iwonderthesame = love.audio.newSource( "/externalassets/dialogs/level71/cs/66iwonderthesame.ogg","stream" )
			
			elseif language2=="pl" then
					wewillfind = love.audio.newSource( "/externalassets/dialogs/level71/pl/2wewillfind.ogg","stream" )
					wevealways = love.audio.newSource( "/externalassets/dialogs/level71/pl/4wevealways.ogg","stream" )
					wehavecome = love.audio.newSource( "/externalassets/dialogs/level71/pl/5wehavecome.ogg","stream" )
					thetopicof = love.audio.newSource( "/externalassets/dialogs/level71/pl/6thetopicof.ogg","stream" )
					whatwould = love.audio.newSource( "/externalassets/dialogs/level71/pl/7whatwould.ogg","stream" )
					weshouldturn = love.audio.newSource( "/externalassets/dialogs/level71/pl/9weshouldturn.ogg","stream" )
					soletsmove = love.audio.newSource( "/externalassets/dialogs/level71/pl/11soletsmove.ogg","stream" )
					atleastitshould = love.audio.newSource( "/externalassets/dialogs/level71/pl/13atleastitshould.ogg","stream" )
					perhapswehave = love.audio.newSource( "/externalassets/dialogs/level71/pl/15perhapswehave.ogg","stream" )
					whatifthemagnets = love.audio.newSource( "/externalassets/dialogs/level71/pl/17whatifthemagnets.ogg","stream" )
					lookalien = love.audio.newSource( "/externalassets/dialogs/level71/pl/19lookalien.ogg","stream" )
					wellthesmall = love.audio.newSource( "/externalassets/dialogs/level71/pl/21wellthesmall.ogg","stream" )
					anywayitisgarbage = love.audio.newSource( "/externalassets/dialogs/level71/pl/25anywayitisgarbage.ogg","stream" )
					nicewhatmaterial = love.audio.newSource( "/externalassets/dialogs/level71/pl/27nicewhatmaterial.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level71/pl/34idontthinkso.ogg","stream" )
					theyarealien = love.audio.newSource( "/externalassets/dialogs/level71/pl/36theyarealien.ogg","stream" )
					ihopeweare = love.audio.newSource( "/externalassets/dialogs/level71/pl/39ihopeweare.ogg","stream" )
					thenwewouldstill = love.audio.newSource( "/externalassets/dialogs/level71/pl/41thenwewouldstill.ogg","stream" )
					perhapsitwasnot = love.audio.newSource( "/externalassets/dialogs/level71/pl/42perhapsitwasnot.ogg","stream" )
					whatisthatlonelything = love.audio.newSource( "/externalassets/dialogs/level71/pl/43whatisthatlonelything.ogg","stream" )
					theangularonewhich = love.audio.newSource( "/externalassets/dialogs/level71/pl/45theangularonewhich.ogg","stream" )
					yesbutwhatis = love.audio.newSource( "/externalassets/dialogs/level71/pl/47yesbutwhatis.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level71/pl/49ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level71/pl/50ouch.ogg","stream" )
					ouchy = love.audio.newSource( "/externalassets/dialogs/level71/pl/51ouchy.ogg","stream" )
					payattention = love.audio.newSource( "/externalassets/dialogs/level71/pl/55payattention.ogg","stream" )
					thatweapon = love.audio.newSource( "/externalassets/dialogs/level71/pl/56thatweapon.ogg","stream" )
					lookatthehole = love.audio.newSource( "/externalassets/dialogs/level71/pl/57lookatthehole.ogg","stream" )
					younearly = love.audio.newSource( "/externalassets/dialogs/level71/pl/58younearly.ogg","stream" )
					nowonderthat = love.audio.newSource( "/externalassets/dialogs/level71/pl/60nowonderthat.ogg","stream" )
					youshouldntuse = love.audio.newSource( "/externalassets/dialogs/level71/pl/61youshouldntuse.ogg","stream" )
					thegoodthing = love.audio.newSource( "/externalassets/dialogs/level71/pl/64thegoodthing.ogg","stream" )
					iwonderthesame = love.audio.newSource( "/externalassets/dialogs/level71/pl/66iwonderthesame.ogg","stream" )
			end
				--fish 1
				weare:setEffect('myEffect')
				whyareyousosure:setEffect('myEffect')
				idontknow:setEffect('myEffect')
				howwould:setEffect('myEffect')
				youthink:setEffect('myEffect')
				thatcouldbe:setEffect('myEffect')
				idont:setEffect('myEffect')
				thenweshould:setEffect('myEffect')
				where:setEffect('myEffect')
				perhaps:setEffect('myEffect')
				itisan:setEffect('myEffect')
				idontthink:setEffect('myEffect')
					lookatthecrystal:setEffect('myEffect')
					dilicium:setEffect('myEffect')
					dihydrogenmonoxide:setEffect('myEffect')
					sodiumchloride:setEffect('myEffect')
					nonferrous:setEffect('myEffect')
					glasswithcolored:setEffect('myEffect')
					analienalloy:setEffect('myEffect')
					whydothemagnets:setEffect('myEffect')
					isee:setEffect('myEffect')
					anotherfuel:setEffect('myEffect')
					perhapsweshould:setEffect('myEffect')
					anunidentified:setEffect('myEffect')
					asisasiditis:setEffect('myEffect')
					ifiknewthat:setEffect('myEffect')
					-- When standing in the alienmagnet
					thatticles:setEffect('myEffect')
					thatprickles:setEffect('myEffect')
					thatishot:setEffect('myEffect')
					-- When shooting the laser
					oops:setEffect('myEffect')
					itwasntmyfault:setEffect('myEffect')
					illtrytoremember:setEffect('myEffect')
					whyarent:setEffect('myEffect')
				
				--fish 2
					wewillfind:setEffect('myEffect')
					wevealways:setEffect('myEffect')
					wehavecome:setEffect('myEffect')
					thetopicof:setEffect('myEffect')
					whatwould:setEffect('myEffect')
					weshouldturn:setEffect('myEffect')
					soletsmove:setEffect('myEffect')
					atleastitshould:setEffect('myEffect')
					perhapswehave:setEffect('myEffect')
					whatifthemagnets:setEffect('myEffect')
					lookalien:setEffect('myEffect')
					wellthesmall:setEffect('myEffect')
					anywayitisgarbage:setEffect('myEffect')
					nicewhatmaterial:setEffect('myEffect')
					idontthinkso:setEffect('myEffect')
					theyarealien:setEffect('myEffect')
					ihopeweare:setEffect('myEffect')
					thenwewouldstill:setEffect('myEffect')
					perhapsitwasnot:setEffect('myEffect')
					whatisthatlonelything:setEffect('myEffect')
					theangularonewhich:setEffect('myEffect')
					yesbutwhatis:setEffect('myEffect')
					-- When standing in the alienmagnet
					ow:setEffect('myEffect')
					ouch:setEffect('myEffect')
					ouchy:setEffect('myEffect')
					-- When shooting the laser
					payattention:setEffect('myEffect')
					thatweapon:setEffect('myEffect')
					lookatthehole:setEffect('myEffect')
					younearly:setEffect('myEffect')
					nowonderthat:setEffect('myEffect')
					youshouldntuse:setEffect('myEffect')
					thegoodthing:setEffect('myEffect')
					iwonderthesame:setEffect('myEffect')
				
		elseif nLevel==72 then
			if language=="en" then
					if accent=="br" then
				elseif accent=="us" then
					waitseethered = love.audio.newSource( "/externalassets/dialogs/level72/en-us/1waitseethered.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level72/en-us/3ithink.ogg","stream" )
					doyouseeit = love.audio.newSource( "/externalassets/dialogs/level72/en-us/6doyouseeit.ogg","stream" )
					lookthegreen = love.audio.newSource( "/externalassets/dialogs/level72/en-us/7lookthegreen.ogg","stream" )
					noitdoesnt = love.audio.newSource( "/externalassets/dialogs/level72/en-us/11noitdoesnt.ogg","stream" )
					itwouldbebetter = love.audio.newSource( "/externalassets/dialogs/level72/en-us/16itwouldbebetter.ogg","stream" )
					seeweareiconographic = love.audio.newSource( "/externalassets/dialogs/level72/en-us/17seeweareiconographic.ogg","stream" )
					underwater = love.audio.newSource( "/externalassets/dialogs/level72/en-us/19underwater.ogg","stream" )
					whyareyoudrivelling = love.audio.newSource( "/externalassets/dialogs/level72/en-us/21whyareyoudrivelling.ogg","stream" )
					whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level72/en-us/23whatareyoudoing.ogg","stream" )
					whatthetraffic = love.audio.newSource( "/externalassets/dialogs/level72/en-us/25whatthetraffic.ogg","stream" )
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					thisisthebuilding = love.audio.newSource( "/externalassets/dialogs/level72/en-us/2thisisthebuilding.ogg","stream" )
					butourhomeholds = love.audio.newSource( "/externalassets/dialogs/level72/en-us/4butourhomeholds.ogg","stream" )
					sorryitisntagood = love.audio.newSource( "/externalassets/dialogs/level72/en-us/5sorryitisntagood.ogg","stream" )
					theseahorsewinks = love.audio.newSource( "/externalassets/dialogs/level72/en-us/10theseahorsewinks.ogg","stream" )
					lookrightnow = love.audio.newSource( "/externalassets/dialogs/level72/en-us/12lookrightnow.ogg","stream" )
					lookrightnow2 = love.audio.newSource( "/externalassets/dialogs/level72/en-us/13lookrightnow.ogg","stream" )
					lookrightnow3 = love.audio.newSource( "/externalassets/dialogs/level72/en-us/14lookrightnow.ogg","stream" )
					lookrightnow4 = love.audio.newSource( "/externalassets/dialogs/level72/en-us/15lookrightnow.ogg","stream" )
					yeswearetheir = love.audio.newSource( "/externalassets/dialogs/level72/en-us/18yeswearetheir.ogg","stream" )
					youmaybeless = love.audio.newSource( "/externalassets/dialogs/level72/en-us/20youmaybeless.ogg","stream" )
					youarentcomplete = love.audio.newSource( "/externalassets/dialogs/level72/en-us/22youarentcomplete.ogg","stream" )
					whyarewe = love.audio.newSource( "/externalassets/dialogs/level72/en-us/24whyarewe.ogg","stream" )
				
				end
			elseif language2=="cs" then
					thisisthebuilding = love.audio.newSource( "/externalassets/dialogs/level72/cs/2thisisthebuilding.ogg","stream" )
					butourhomeholds = love.audio.newSource( "/externalassets/dialogs/level72/cs/4butourhomeholds.ogg","stream" )
					sorryitisntagood = love.audio.newSource( "/externalassets/dialogs/level72/cs/5sorryitisntagood.ogg","stream" )
					theseahorsewinks = love.audio.newSource( "/externalassets/dialogs/level72/cs/10theseahorsewinks.ogg","stream" )
					lookrightnow = love.audio.newSource( "/externalassets/dialogs/level72/cs/12lookrightnow.ogg","stream" )
					lookrightnow2 = love.audio.newSource( "/externalassets/dialogs/level72/cs/13lookrightnow.ogg","stream" )
					lookrightnow3 = love.audio.newSource( "/externalassets/dialogs/level72/cs/14lookrightnow.ogg","stream" )
					lookrightnow4 = love.audio.newSource( "/externalassets/dialogs/level72/cs/15lookrightnow.ogg","stream" )
					yeswearetheir = love.audio.newSource( "/externalassets/dialogs/level72/cs/18yeswearetheir.ogg","stream" )
					youmaybeless = love.audio.newSource( "/externalassets/dialogs/level72/cs/20youmaybeless.ogg","stream" )
					youarentcomplete = love.audio.newSource( "/externalassets/dialogs/level72/cs/22youarentcomplete.ogg","stream" )
					whyarewe = love.audio.newSource( "/externalassets/dialogs/level72/cs/24whyarewe.ogg","stream" )
			
			elseif language2=="pl" then
					thisisthebuilding = love.audio.newSource( "/externalassets/dialogs/level72/pl/2thisisthebuilding.ogg","stream" )
					butourhomeholds = love.audio.newSource( "/externalassets/dialogs/level72/pl/4butourhomeholds.ogg","stream" )
					sorryitisntagood = love.audio.newSource( "/externalassets/dialogs/level72/pl/5sorryitisntagood.ogg","stream" )
					theseahorsewinks = love.audio.newSource( "/externalassets/dialogs/level72/pl/10theseahorsewinks.ogg","stream" )
					lookrightnow = love.audio.newSource( "/externalassets/dialogs/level72/pl/12lookrightnow.ogg","stream" )
					lookrightnow2 = love.audio.newSource( "/externalassets/dialogs/level72/pl/13lookrightnow.ogg","stream" )
					lookrightnow3 = love.audio.newSource( "/externalassets/dialogs/level72/pl/14lookrightnow.ogg","stream" )
					lookrightnow4 = love.audio.newSource( "/externalassets/dialogs/level72/pl/15lookrightnow.ogg","stream" )
					yeswearetheir = love.audio.newSource( "/externalassets/dialogs/level72/pl/18yeswearetheir.ogg","stream" )
					youmaybeless = love.audio.newSource( "/externalassets/dialogs/level72/pl/20youmaybeless.ogg","stream" )
					youarentcomplete = love.audio.newSource( "/externalassets/dialogs/level72/pl/22youarentcomplete.ogg","stream" )
					whyarewe = love.audio.newSource( "/externalassets/dialogs/level72/pl/24whyarewe.ogg","stream" )
			
			end
					--fish 1
					waitseethered:setEffect('myEffect')
					ithink:setEffect('myEffect')
					doyouseeit:setEffect('myEffect')
					lookthegreen:setEffect('myEffect')
					noitdoesnt:setEffect('myEffect')
					itwouldbebetter:setEffect('myEffect')
					seeweareiconographic:setEffect('myEffect')
					underwater:setEffect('myEffect')
					whyareyoudrivelling:setEffect('myEffect')
					whatareyoudoing:setEffect('myEffect')
					whatthetraffic:setEffect('myEffect')
					
					--fish 2
					thisisthebuilding:setEffect('myEffect')
					butourhomeholds:setEffect('myEffect')
					sorryitisntagood:setEffect('myEffect')
					theseahorsewinks:setEffect('myEffect')
					lookrightnow:setEffect('myEffect')
					lookrightnow2:setEffect('myEffect')
					lookrightnow3:setEffect('myEffect')
					lookrightnow4:setEffect('myEffect')
					yeswearetheir:setEffect('myEffect')
					youmaybeless:setEffect('myEffect')
					youarentcomplete:setEffect('myEffect')
					whyarewe:setEffect('myEffect')
			
		elseif nLevel==73 then
			if language=="en" then
					if accent=="br" then
				elseif accent=="us" then
					itisalreadythere = love.audio.newSource( "/externalassets/dialogs/level73/en-us/1itisalreadythere.ogg","stream" )
					eitherigetout = love.audio.newSource( "/externalassets/dialogs/level73/en-us/3eitherigetout.ogg","stream" )
					itshouldbeme = love.audio.newSource( "/externalassets/dialogs/level73/en-us/5itshouldbeme.ogg","stream" )
					buttheplayer = love.audio.newSource( "/externalassets/dialogs/level73/en-us/6buttheplayer.ogg","stream" )
					itisntfair = love.audio.newSource( "/externalassets/dialogs/level73/en-us/9itisntfair.ogg","stream" )
					thesmallfish = love.audio.newSource( "/externalassets/dialogs/level73/en-us/10thesmallfish.ogg","stream" )
					itisntwhat = love.audio.newSource( "/externalassets/dialogs/level73/en-us/11itisntwhat.ogg","stream" )
					somebody = love.audio.newSource( "/externalassets/dialogs/level73/en-us/13somebody.ogg","stream" )
					howcantheplayer = love.audio.newSource( "/externalassets/dialogs/level73/en-us/16howcantheplayer.ogg","stream" )
					finallyipraise = love.audio.newSource( "/externalassets/dialogs/level73/en-us/17finallyipraise.ogg","stream" )
					hedoesntknow = love.audio.newSource( "/externalassets/dialogs/level73/en-us/18hedoesntknow.ogg","stream" )
					ohwell = love.audio.newSource( "/externalassets/dialogs/level73/en-us/20ohwell.ogg","stream" )
					ifishouldsay = love.audio.newSource( "/externalassets/dialogs/level73/en-us/23ifishouldsay.ogg","stream" )
					ithinkwhatever = love.audio.newSource( "/externalassets/dialogs/level73/en-us/25ithinkwhatever.ogg","stream" )
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					what = love.audio.newSource( "/externalassets/dialogs/level73/en-us/2what.ogg","stream" )
					andwhowill = love.audio.newSource( "/externalassets/dialogs/level73/en-us/4andwhowill.ogg","stream" )
					whatareyou = love.audio.newSource( "/externalassets/dialogs/level73/en-us/7whatareyou.ogg","stream" )
					hewillbe = love.audio.newSource( "/externalassets/dialogs/level73/en-us/8hewillbe.ogg","stream" )
					ishouldnot = love.audio.newSource( "/externalassets/dialogs/level73/en-us/12ishouldnot.ogg","stream" )
					butinfact = love.audio.newSource( "/externalassets/dialogs/level73/en-us/14butinfact.ogg","stream" )
					butdoihave = love.audio.newSource( "/externalassets/dialogs/level73/en-us/15butdoihave.ogg","stream" )
					hemaylookforward = love.audio.newSource( "/externalassets/dialogs/level73/en-us/17hemaylookforward.ogg","stream" )
					possibly = love.audio.newSource( "/externalassets/dialogs/level73/en-us/19possibly.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level73/en-us/21ofcourse.ogg","stream" )
					yousaidit = love.audio.newSource( "/externalassets/dialogs/level73/en-us/22yousaidit.ogg","stream" )
					maybenowadays = love.audio.newSource( "/externalassets/dialogs/level73/en-us/24maybenowadays.ogg","stream" )
					beashamed = love.audio.newSource( "/externalassets/dialogs/level73/en-us/26beashamed.ogg","stream" )
				end
			elseif language2=="cs" then
					what = love.audio.newSource( "/externalassets/dialogs/level73/cs/2what.ogg","stream" )
					andwhowill = love.audio.newSource( "/externalassets/dialogs/level73/cs/4andwhowill.ogg","stream" )
					whatareyou = love.audio.newSource( "/externalassets/dialogs/level73/cs/7whatareyou.ogg","stream" )
					hewillbe = love.audio.newSource( "/externalassets/dialogs/level73/cs/8hewillbe.ogg","stream" )
					ishouldnot = love.audio.newSource( "/externalassets/dialogs/level73/cs/12ishouldnot.ogg","stream" )
					butinfact = love.audio.newSource( "/externalassets/dialogs/level73/cs/14butinfact.ogg","stream" )
					butdoihave = love.audio.newSource( "/externalassets/dialogs/level73/cs/15butdoihave.ogg","stream" )
					hemaylookforward = love.audio.newSource( "/externalassets/dialogs/level73/cs/17hemaylookforward.ogg","stream" )
					possibly = love.audio.newSource( "/externalassets/dialogs/level73/cs/19possibly.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level73/cs/21ofcourse.ogg","stream" )
					yousaidit = love.audio.newSource( "/externalassets/dialogs/level73/cs/22yousaidit.ogg","stream" )
					maybenowadays = love.audio.newSource( "/externalassets/dialogs/level73/cs/24maybenowadays.ogg","stream" )
					beashamed = love.audio.newSource( "/externalassets/dialogs/level73/cs/26beashamed.ogg","stream" )		
				
			elseif language2=="pl" then
					what = love.audio.newSource( "/externalassets/dialogs/level73/pl/2what.ogg","stream" )
					andwhowill = love.audio.newSource( "/externalassets/dialogs/level73/pl/4andwhowill.ogg","stream" )
					whatareyou = love.audio.newSource( "/externalassets/dialogs/level73/pl/7whatareyou.ogg","stream" )
					hewillbe = love.audio.newSource( "/externalassets/dialogs/level73/pl/8hewillbe.ogg","stream" )
					ishouldnot = love.audio.newSource( "/externalassets/dialogs/level73/pl/12ishouldnot.ogg","stream" )
					butinfact = love.audio.newSource( "/externalassets/dialogs/level73/pl/14butinfact.ogg","stream" )
					butdoihave = love.audio.newSource( "/externalassets/dialogs/level73/pl/15butdoihave.ogg","stream" )
					hemaylookforward = love.audio.newSource( "/externalassets/dialogs/level73/pl/17hemaylookforward.ogg","stream" )
					possibly = love.audio.newSource( "/externalassets/dialogs/level73/pl/19possibly.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level73/pl/21ofcourse.ogg","stream" )
					yousaidit = love.audio.newSource( "/externalassets/dialogs/level73/pl/22yousaidit.ogg","stream" )
					maybenowadays = love.audio.newSource( "/externalassets/dialogs/level73/pl/24maybenowadays.ogg","stream" )
					beashamed = love.audio.newSource( "/externalassets/dialogs/level73/pl/26beashamed.ogg","stream" )		
				end
					--fish 1
					itisalreadythere:setEffect('myEffect')
					eitherigetout:setEffect('myEffect')
					itshouldbeme:setEffect('myEffect')
					buttheplayer:setEffect('myEffect')
					itisntfair:setEffect('myEffect')
					thesmallfish:setEffect('myEffect')
					itisntwhat:setEffect('myEffect')
					somebody:setEffect('myEffect')
					howcantheplayer:setEffect('myEffect')
					finallyipraise:setEffect('myEffect')
					hedoesntknow:setEffect('myEffect')
					ohwell:setEffect('myEffect')
					ifishouldsay:setEffect('myEffect')
					ithinkwhatever:setEffect('myEffect')
					
					--fish 2
					what:setEffect('myEffect')
					andwhowill:setEffect('myEffect')
					whatareyou:setEffect('myEffect')
					hewillbe:setEffect('myEffect')
					ishouldnot:setEffect('myEffect')
					butinfact:setEffect('myEffect')
					butdoihave:setEffect('myEffect')
					hemaylookforward:setEffect('myEffect')
					possibly:setEffect('myEffect')
					ofcourse:setEffect('myEffect')
					yousaidit:setEffect('myEffect')
					maybenowadays:setEffect('myEffect')
					beashamed:setEffect('myEffect')
			
		elseif nLevel==74 then
			if language=="en" then
					if accent=="br" then
				elseif accent=="us" then
					ifearitwillbe = love.audio.newSource( "/externalassets/dialogs/level74/en-us/2ifearitwillbe.ogg","stream" )
					howcanitbeyours = love.audio.newSource( "/externalassets/dialogs/level74/en-us/13howcanitbeyours.ogg","stream" )
					icantdemur = love.audio.newSource( "/externalassets/dialogs/level74/en-us/15icantdemur.ogg","stream" )
					playerifyouare = love.audio.newSource( "/externalassets/dialogs/level74/en-us/16playerifyouare.ogg","stream" )
					tryit = love.audio.newSource( "/externalassets/dialogs/level74/en-us/18tryit.ogg","stream" )
					putitback = love.audio.newSource( "/externalassets/dialogs/level74/en-us/20putitback.ogg","stream" )
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					itisahaunted = love.audio.newSource( "/externalassets/dialogs/level74/en-us/1itisahaunted.ogg","stream" )
					hereyouare = love.audio.newSource( "/externalassets/dialogs/level74/en-us/3hereyouare.ogg","stream" )
					sorryweneed = love.audio.newSource( "/externalassets/dialogs/level74/en-us/12sorryweneed.ogg","stream" )
					youcantbean = love.audio.newSource( "/externalassets/dialogs/level74/en-us/14youcantbean.ogg","stream" )
					itmaybeamagic = love.audio.newSource( "/externalassets/dialogs/level74/en-us/17itmaybeamagic.ogg","stream" )
					helloiwishto = love.audio.newSource( "/externalassets/dialogs/level74/en-us/19helloiwishto.ogg","stream" )
				end
			elseif language2=="cs" then
					itisahaunted = love.audio.newSource( "/externalassets/dialogs/level74/cs/1itisahaunted.ogg","stream" )
					hereyouare = love.audio.newSource( "/externalassets/dialogs/level74/cs/3hereyouare.ogg","stream" )
					sorryweneed = love.audio.newSource( "/externalassets/dialogs/level74/cs/12sorryweneed.ogg","stream" )
					youcantbean = love.audio.newSource( "/externalassets/dialogs/level74/cs/14youcantbean.ogg","stream" )
					itmaybeamagic = love.audio.newSource( "/externalassets/dialogs/level74/cs/17itmaybeamagic.ogg","stream" )
					helloiwishto = love.audio.newSource( "/externalassets/dialogs/level74/cs/19helloiwishto.ogg","stream" )
			
			elseif language2=="pl" then
					itisahaunted = love.audio.newSource( "/externalassets/dialogs/level74/pl/1itisahaunted.ogg","stream" )
					hereyouare = love.audio.newSource( "/externalassets/dialogs/level74/pl/3hereyouare.ogg","stream" )
					sorryweneed = love.audio.newSource( "/externalassets/dialogs/level74/pl/12sorryweneed.ogg","stream" )
					youcantbean = love.audio.newSource( "/externalassets/dialogs/level74/pl/14youcantbean.ogg","stream" )
					itmaybeamagic = love.audio.newSource( "/externalassets/dialogs/level74/pl/17itmaybeamagic.ogg","stream" )
					helloiwishto = love.audio.newSource( "/externalassets/dialogs/level74/pl/19helloiwishto.ogg","stream" )
			end
					--fish 1
					ifearitwillbe:setEffect('myEffect')
					howcanitbeyours:setEffect('myEffect')
					icantdemur:setEffect('myEffect')
					playerifyouare:setEffect('myEffect')
					tryit:setEffect('myEffect')
					putitback:setEffect('myEffect')
					
					--fish 2
					itisahaunted:setEffect('myEffect')
					hereyouare:setEffect('myEffect')
					sorryweneed:setEffect('myEffect')
					youcantbean:setEffect('myEffect')
					itmaybeamagic:setEffect('myEffect')
					helloiwishto:setEffect('myEffect')
			
		elseif nLevel==75 then
			if language=="en" then
					if accent=="br" then
				elseif accent=="us" then
					wheredoyouseealock  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/2wheredoyouseealock.ogg","stream" )
					icantleave  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/4icantleave.ogg","stream" )
					couldyouplease  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/6couldyouplease.ogg","stream" )
					lookabroom  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/8lookabroom.ogg","stream" )
					whyareyou  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/10whyareyou.ogg","stream" )
					yestoosmall  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/13yestoosmall.ogg","stream" )
					thengogetit  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/15thengogetit.ogg","stream" )
					okiwontsay  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/17okiwontsay.ogg","stream" )
					hmmok  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/19hmmok.ogg","stream" )
					doyouthink  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/20doyouthink.ogg","stream" )
					wellneverget  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/22wellneverget.ogg","stream" )
					couldtheicicle  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/24couldtheicicle.ogg","stream" )
					wejusthave  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/26wejusthave.ogg","stream" )
					ofcourseitsacactus  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/29ofcourseitsacactus.ogg","stream" )
					whatsitdoing  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/33whatsitdoing.ogg","stream" )
					wevegotit  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/34wevegotit.ogg","stream" )
					youdbetterlearn  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/37youdbetterlearn.ogg","stream" )
					pleasehold  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/38pleasehold.ogg","stream" )
					nothedoor  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/40nothedoor.ogg","stream" )
					grmbl  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/42grmbl.ogg","stream" )
					outchthatcactus  = love.audio.newSource( "/externalassets/dialogs/level75/en-us/outchthatcactus.ogg","stream" )
					
				
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					looksomeone = love.audio.newSource( "/externalassets/dialogs/level75/en-us/1looksomeone.ogg","stream" )
					thereisalock = love.audio.newSource( "/externalassets/dialogs/level75/en-us/3thereisalock.ogg","stream" )
					thatswhywe = love.audio.newSource( "/externalassets/dialogs/level75/en-us/5thatswhywe.ogg","stream" )
					iwouldificould = love.audio.newSource( "/externalassets/dialogs/level75/en-us/7iwouldificould.ogg","stream" )
					idontwant = love.audio.newSource( "/externalassets/dialogs/level75/en-us/9idontwant.ogg","stream" )
					withoutme = love.audio.newSource( "/externalassets/dialogs/level75/en-us/11withoutme.ogg","stream" )
					wecouldjust = love.audio.newSource( "/externalassets/dialogs/level75/en-us/12wecouldjust.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level75/en-us/14ithinkweshould.ogg","stream" )
					benicetome = love.audio.newSource( "/externalassets/dialogs/level75/en-us/16benicetome.ogg","stream" )
					donteventhinkthem = love.audio.newSource( "/externalassets/dialogs/level75/en-us/18donteventhinkthem.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level75/en-us/21idontthinkso.ogg","stream" )
					dontgiveup = love.audio.newSource( "/externalassets/dialogs/level75/en-us/23dontgiveup.ogg","stream" )
					wecantgetitout = love.audio.newSource( "/externalassets/dialogs/level75/en-us/25wecantgetitout.ogg","stream" )
					perhaps = love.audio.newSource( "/externalassets/dialogs/level75/en-us/27perhaps.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level75/en-us/28ouch.ogg","stream" )
					whatsitdoing = love.audio.newSource( "/externalassets/dialogs/level75/en-us/30whatsitdoing.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level75/en-us/32ofcourse.ogg","stream" )
					noproblem = love.audio.newSource( "/externalassets/dialogs/level75/en-us/35noproblem.ogg","stream" )
					assonas = love.audio.newSource( "/externalassets/dialogs/level75/en-us/36assonas.ogg","stream" )
					illtry = love.audio.newSource( "/externalassets/dialogs/level75/en-us/39illtry.ogg","stream" )
					soopenit = love.audio.newSource( "/externalassets/dialogs/level75/en-us/41soopenit.ogg","stream" )
					staycalm = love.audio.newSource( "/externalassets/dialogs/level75/en-us/43staycalm.ogg","stream" )
				end
			elseif language2=="cs" then
					looksomeone = love.audio.newSource( "/externalassets/dialogs/level75/cs/1looksomeone.ogg","stream" )
					thereisalock = love.audio.newSource( "/externalassets/dialogs/level75/cs/3thereisalock.ogg","stream" )
					thatswhywe = love.audio.newSource( "/externalassets/dialogs/level75/cs/5thatswhywe.ogg","stream" )
					iwouldificould = love.audio.newSource( "/externalassets/dialogs/level75/cs/7iwouldificould.ogg","stream" )
					idontwant = love.audio.newSource( "/externalassets/dialogs/level75/cs/9idontwant.ogg","stream" )
					withoutme = love.audio.newSource( "/externalassets/dialogs/level75/cs/11withoutme.ogg","stream" )
					wecouldjust = love.audio.newSource( "/externalassets/dialogs/level75/cs/12wecouldjust.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level75/cs/14ithinkweshould.ogg","stream" )
					benicetome = love.audio.newSource( "/externalassets/dialogs/level75/cs/16benicetome.ogg","stream" )
					donteventhinkthem = love.audio.newSource( "/externalassets/dialogs/level75/cs/18donteventhinkthem.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level75/cs/21idontthinkso.ogg","stream" )
					dontgiveup = love.audio.newSource( "/externalassets/dialogs/level75/cs/23dontgiveup.ogg","stream" )
					wecantgetitout = love.audio.newSource( "/externalassets/dialogs/level75/cs/25wecantgetitout.ogg","stream" )
					perhaps = love.audio.newSource( "/externalassets/dialogs/level75/cs/27perhaps.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level75/cs/28ouch.ogg","stream" )
					whatsitdoing = love.audio.newSource( "/externalassets/dialogs/level75/cs/30whatsitdoing.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level75/cs/32ofcourse.ogg","stream" )
					noproblem = love.audio.newSource( "/externalassets/dialogs/level75/cs/35noproblem.ogg","stream" )
					assonas = love.audio.newSource( "/externalassets/dialogs/level75/cs/36assonas.ogg","stream" )
					illtry = love.audio.newSource( "/externalassets/dialogs/level75/cs/39illtry.ogg","stream" )
					soopenit = love.audio.newSource( "/externalassets/dialogs/level75/cs/41soopenit.ogg","stream" )
					staycalm = love.audio.newSource( "/externalassets/dialogs/level75/cs/43staycalm.ogg","stream" )		
			
			elseif language2=="pl" then
					looksomeone = love.audio.newSource( "/externalassets/dialogs/level75/pl/1looksomeone.ogg","stream" )
					thereisalock = love.audio.newSource( "/externalassets/dialogs/level75/pl/3thereisalock.ogg","stream" )
					thatswhywe = love.audio.newSource( "/externalassets/dialogs/level75/pl/5thatswhywe.ogg","stream" )
					iwouldificould = love.audio.newSource( "/externalassets/dialogs/level75/pl/7iwouldificould.ogg","stream" )
					idontwant = love.audio.newSource( "/externalassets/dialogs/level75/pl/9idontwant.ogg","stream" )
					withoutme = love.audio.newSource( "/externalassets/dialogs/level75/pl/11withoutme.ogg","stream" )
					wecouldjust = love.audio.newSource( "/externalassets/dialogs/level75/pl/12wecouldjust.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level75/pl/14ithinkweshould.ogg","stream" )
					benicetome = love.audio.newSource( "/externalassets/dialogs/level75/pl/16benicetome.ogg","stream" )
					donteventhinkthem = love.audio.newSource( "/externalassets/dialogs/level75/pl/18donteventhinkthem.ogg","stream" )
					idontthinkso = love.audio.newSource( "/externalassets/dialogs/level75/pl/21idontthinkso.ogg","stream" )
					dontgiveup = love.audio.newSource( "/externalassets/dialogs/level75/pl/23dontgiveup.ogg","stream" )
					wecantgetitout = love.audio.newSource( "/externalassets/dialogs/level75/pl/25wecantgetitout.ogg","stream" )
					perhaps = love.audio.newSource( "/externalassets/dialogs/level75/pl/27perhaps.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level75/pl/28ouch.ogg","stream" )
					whatsitdoing = love.audio.newSource( "/externalassets/dialogs/level75/pl/30whatsitdoing.ogg","stream" )
					ofcourse = love.audio.newSource( "/externalassets/dialogs/level75/pl/32ofcourse.ogg","stream" )
					noproblem = love.audio.newSource( "/externalassets/dialogs/level75/pl/35noproblem.ogg","stream" )
					assonas = love.audio.newSource( "/externalassets/dialogs/level75/pl/36assonas.ogg","stream" )
					illtry = love.audio.newSource( "/externalassets/dialogs/level75/pl/39illtry.ogg","stream" )
					soopenit = love.audio.newSource( "/externalassets/dialogs/level75/pl/41soopenit.ogg","stream" )
					staycalm = love.audio.newSource( "/externalassets/dialogs/level75/pl/43staycalm.ogg","stream" )		
			end
					--fish1
					wheredoyouseealock:setEffect('myEffect')
					icantleave:setEffect('myEffect')
					couldyouplease:setEffect('myEffect')
					lookabroom:setEffect('myEffect')
					whyareyou:setEffect('myEffect')
					yestoosmall:setEffect('myEffect')
					thengogetit:setEffect('myEffect')
					okiwontsay:setEffect('myEffect')
					hmmok:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					wellneverget:setEffect('myEffect')
					couldtheicicle:setEffect('myEffect')
					wejusthave:setEffect('myEffect')
					ofcourseitsacactus:setEffect('myEffect')
					whatsitdoing:setEffect('myEffect')
					wevegotit:setEffect('myEffect')
					youdbetterlearn:setEffect('myEffect')
					pleasehold:setEffect('myEffect')
					nothedoor:setEffect('myEffect')
					grmbl:setEffect('myEffect')
					outchthatcactus:setEffect('myEffect')
					
					--fish 2
					looksomeone:setEffect('myEffect')
					thereisalock:setEffect('myEffect')
					thatswhywe:setEffect('myEffect')
					iwouldificould:setEffect('myEffect')
					idontwant:setEffect('myEffect')
					withoutme:setEffect('myEffect')
					wecouldjust:setEffect('myEffect')
					ithinkweshould:setEffect('myEffect')
					benicetome:setEffect('myEffect')
					donteventhinkthem:setEffect('myEffect')
					idontthinkso:setEffect('myEffect')
					dontgiveup:setEffect('myEffect')
					wecantgetitout:setEffect('myEffect')
					perhaps:setEffect('myEffect')
					ouch:setEffect('myEffect')
					whatsitdoing:setEffect('myEffect')
					ofcourse:setEffect('myEffect')
					noproblem:setEffect('myEffect')
					assonas:setEffect('myEffect')
					illtry:setEffect('myEffect')
					soopenit:setEffect('myEffect')
					staycalm:setEffect('myEffect')
			
		elseif nLevel==76 then
			if language=="en" then
					if accent=="br" then
				elseif accent=="us" then
					whatdidyousay  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/2whatdidyousay.ogg","stream" )
					soyouweretalking  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/4soyouweretalking.ogg","stream" )
					whatdoesthat  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/6whatdoesthat.ogg","stream" )
					aha	 = love.audio.newSource( "/externalassets/dialogs/level76/en-us/8aha.ogg","stream" )
					ohwhereishe  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/10ohwhereishe.ogg","stream" )
					thenhowdoyouknow  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/12thenhowdoyouknow.ogg","stream" )
					abrickwall  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/14abrickwall.ogg","stream" )
					steel  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/15steel.ogg","stream" )
					you  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/16you.ogg","stream" )
					water  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/17water.ogg","stream" )
					anexitwecan  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/18anexitwecan.ogg","stream" )
					darkness  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/19darkness.ogg","stream" )
					whichofthekeys  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/22whichofthekeys.ogg","stream" )
					atleastthekeys  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/24atleastthekeys.ogg","stream" )
					whydontyoulook  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/26whydontyoulook.ogg","stream" )
					thatwouldbecheating  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/28thatwouldbecheating.ogg","stream" )
					thiswouldbe  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/29thiswouldbe.ogg","stream" )
					doyouthink  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/32doyouthink.ogg","stream" )
					whynot  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/34whynot.ogg","stream" )
					ofcourse1  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/36ofcourse1.ogg","stream" )
					ofcourse2  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/36ofcourse2.ogg","stream" )
					ofcourse3  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/36ofcourse3.ogg","stream" )
					isee  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/40isee.ogg","stream" )
					ifallthelocks  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/41ifallthelocks.ogg","stream" )
					weshouldtake  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/43weshouldtake.ogg","stream" )
					whydontwejust  = love.audio.newSource( "/externalassets/dialogs/level76/en-us/45whydontwejust.ogg","stream" )
					
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					lookasisaid = love.audio.newSource( "/externalassets/dialogs/level76/en-us/1lookasisaid.ogg","stream" )
					thatsomeonehas = love.audio.newSource( "/externalassets/dialogs/level76/en-us/3thatsomeonehas.ogg","stream" )
					intheprevious = love.audio.newSource( "/externalassets/dialogs/level76/en-us/5intheprevious.ogg","stream" )
					therearefourlocks = love.audio.newSource( "/externalassets/dialogs/level76/en-us/7therearefourlocks.ogg","stream" )
					wearegettingcloser = love.audio.newSource( "/externalassets/dialogs/level76/en-us/9wearegettingcloser.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level76/en-us/11idontknow.ogg","stream" )
					lookaround = love.audio.newSource( "/externalassets/dialogs/level76/en-us/13lookaround.ogg","stream" )
					whatelse = love.audio.newSource( "/externalassets/dialogs/level76/en-us/20whatelse.ogg","stream" )
					igiveup = love.audio.newSource( "/externalassets/dialogs/level76/en-us/21igiveup.ogg","stream" )
					allornone = love.audio.newSource( "/externalassets/dialogs/level76/en-us/23allornone.ogg","stream" )
					whydontyou = love.audio.newSource( "/externalassets/dialogs/level76/en-us/25whydontyou.ogg","stream" )
					maybetheplayer = love.audio.newSource( "/externalassets/dialogs/level76/en-us/27maybetheplayer.ogg","stream" )
					thatstrue = love.audio.newSource( "/externalassets/dialogs/level76/en-us/30thatstrue.ogg","stream" )
					butthelevelwould = love.audio.newSource( "/externalassets/dialogs/level76/en-us/31butthelevelwould.ogg","stream" )
					ofcoursenot = love.audio.newSource( "/externalassets/dialogs/level76/en-us/33ofcoursenot.ogg","stream" )
					thinkalittlebit = love.audio.newSource( "/externalassets/dialogs/level76/en-us/35thinkalittlebit.ogg","stream" )
					nothelock = love.audio.newSource( "/externalassets/dialogs/level76/en-us/39nothelock.ogg","stream" )
					thatisthepoint = love.audio.newSource( "/externalassets/dialogs/level76/en-us/42thatisthepoint.ogg","stream" )
					goodidea = love.audio.newSource( "/externalassets/dialogs/level76/en-us/44goodidea.ogg","stream" )
					ok = love.audio.newSource( "/externalassets/dialogs/level76/en-us/46ok.ogg","stream" )
				end
			elseif language2=="cs" then
					lookasisaid = love.audio.newSource( "/externalassets/dialogs/level76/cs/1lookasisaid.ogg","stream" )
					thatsomeonehas = love.audio.newSource( "/externalassets/dialogs/level76/cs/3thatsomeonehas.ogg","stream" )
					intheprevious = love.audio.newSource( "/externalassets/dialogs/level76/cs/5intheprevious.ogg","stream" )
					therearefourlocks = love.audio.newSource( "/externalassets/dialogs/level76/cs/7therearefourlocks.ogg","stream" )
					wearegettingcloser = love.audio.newSource( "/externalassets/dialogs/level76/cs/9wearegettingcloser.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level76/cs/11idontknow.ogg","stream" )
					lookaround = love.audio.newSource( "/externalassets/dialogs/level76/cs/13lookaround.ogg","stream" )
					whatelse = love.audio.newSource( "/externalassets/dialogs/level76/cs/20whatelse.ogg","stream" )
					igiveup = love.audio.newSource( "/externalassets/dialogs/level76/cs/21igiveup.ogg","stream" )
					allornone = love.audio.newSource( "/externalassets/dialogs/level76/cs/23allornone.ogg","stream" )
					whydontyou = love.audio.newSource( "/externalassets/dialogs/level76/cs/25whydontyou.ogg","stream" )
					maybetheplayer = love.audio.newSource( "/externalassets/dialogs/level76/cs/27maybetheplayer.ogg","stream" )
					thatstrue = love.audio.newSource( "/externalassets/dialogs/level76/cs/30thatstrue.ogg","stream" )
					butthelevelwould = love.audio.newSource( "/externalassets/dialogs/level76/cs/31butthelevelwould.ogg","stream" )
					ofcoursenot = love.audio.newSource( "/externalassets/dialogs/level76/cs/33ofcoursenot.ogg","stream" )
					thinkalittlebit = love.audio.newSource( "/externalassets/dialogs/level76/cs/35thinkalittlebit.ogg","stream" )
					nothelock = love.audio.newSource( "/externalassets/dialogs/level76/cs/39nothelock.ogg","stream" )
					thatisthepoint = love.audio.newSource( "/externalassets/dialogs/level76/cs/42thatisthepoint.ogg","stream" )
					goodidea = love.audio.newSource( "/externalassets/dialogs/level76/cs/44goodidea.ogg","stream" )
					ok = love.audio.newSource( "/externalassets/dialogs/level76/cs/46ok.ogg","stream" )	
			
			elseif language2=="pl" then
					lookasisaid = love.audio.newSource( "/externalassets/dialogs/level76/pl/1lookasisaid.ogg","stream" )
					thatsomeonehas = love.audio.newSource( "/externalassets/dialogs/level76/pl/3thatsomeonehas.ogg","stream" )
					intheprevious = love.audio.newSource( "/externalassets/dialogs/level76/pl/5intheprevious.ogg","stream" )
					therearefourlocks = love.audio.newSource( "/externalassets/dialogs/level76/pl/7therearefourlocks.ogg","stream" )
					wearegettingcloser = love.audio.newSource( "/externalassets/dialogs/level76/pl/9wearegettingcloser.ogg","stream" )
					idontknow = love.audio.newSource( "/externalassets/dialogs/level76/pl/11idontknow.ogg","stream" )
					lookaround = love.audio.newSource( "/externalassets/dialogs/level76/pl/13lookaround.ogg","stream" )
					whatelse = love.audio.newSource( "/externalassets/dialogs/level76/pl/20whatelse.ogg","stream" )
					igiveup = love.audio.newSource( "/externalassets/dialogs/level76/pl/21igiveup.ogg","stream" )
					allornone = love.audio.newSource( "/externalassets/dialogs/level76/pl/23allornone.ogg","stream" )
					whydontyou = love.audio.newSource( "/externalassets/dialogs/level76/pl/25whydontyou.ogg","stream" )
					maybetheplayer = love.audio.newSource( "/externalassets/dialogs/level76/pl/27maybetheplayer.ogg","stream" )
					thatstrue = love.audio.newSource( "/externalassets/dialogs/level76/pl/30thatstrue.ogg","stream" )
					butthelevelwould = love.audio.newSource( "/externalassets/dialogs/level76/pl/31butthelevelwould.ogg","stream" )
					ofcoursenot = love.audio.newSource( "/externalassets/dialogs/level76/pl/33ofcoursenot.ogg","stream" )
					thinkalittlebit = love.audio.newSource( "/externalassets/dialogs/level76/pl/35thinkalittlebit.ogg","stream" )
					nothelock = love.audio.newSource( "/externalassets/dialogs/level76/pl/39nothelock.ogg","stream" )
					thatisthepoint = love.audio.newSource( "/externalassets/dialogs/level76/pl/42thatisthepoint.ogg","stream" )
					goodidea = love.audio.newSource( "/externalassets/dialogs/level76/pl/44goodidea.ogg","stream" )
					ok = love.audio.newSource( "/externalassets/dialogs/level76/pl/46ok.ogg","stream" )	
			end
					--fish 1
					whatdidyousay:setEffect('myEffect')
					soyouweretalking:setEffect('myEffect')
					whatdoesthat:setEffect('myEffect')
					aha:setEffect('myEffect')
					ohwhereishe:setEffect('myEffect')
					thenhowdoyouknow:setEffect('myEffect')
					abrickwall:setEffect('myEffect')
					steel:setEffect('myEffect')
					you:setEffect('myEffect')
					water:setEffect('myEffect')
					anexitwecan:setEffect('myEffect')
					darkness:setEffect('myEffect')
					whichofthekeys:setEffect('myEffect')
					atleastthekeys:setEffect('myEffect')
					whydontyoulook:setEffect('myEffect')
					thatwouldbecheating:setEffect('myEffect')
					thiswouldbe:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					whynot:setEffect('myEffect')
					ofcourse1:setEffect('myEffect')
					ofcourse2:setEffect('myEffect')
					ofcourse3:setEffect('myEffect')
					isee:setEffect('myEffect')
					ifallthelocks:setEffect('myEffect')
					weshouldtake:setEffect('myEffect')
					whydontwejust:setEffect('myEffect')
					
					--fish 2
					lookasisaid:setEffect('myEffect')
					thatsomeonehas:setEffect('myEffect')
					intheprevious:setEffect('myEffect')
					therearefourlocks:setEffect('myEffect')
					wearegettingcloser:setEffect('myEffect')
					idontknow:setEffect('myEffect')
					lookaround:setEffect('myEffect')
					whatelse:setEffect('myEffect')
					igiveup:setEffect('myEffect')
					allornone:setEffect('myEffect')
					whydontyou:setEffect('myEffect')
					maybetheplayer:setEffect('myEffect')
					thatstrue:setEffect('myEffect')
					butthelevelwould:setEffect('myEffect')
					ofcoursenot:setEffect('myEffect')
					thinkalittlebit:setEffect('myEffect')
					nothelock:setEffect('myEffect')
					thatisthepoint:setEffect('myEffect')
					goodidea:setEffect('myEffect')
					ok:setEffect('myEffect')
					
					
		elseif nLevel==77 then
		
		loadBorderDialogs()
		
			if language=="en" then
			--end
			
			whenwesaw = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/1whenwesaw.ogg","stream" )
			wedidntunderstand = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/2wedidntunderstand.ogg","stream" )
			butfinally = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/3butfinally.ogg","stream" )
			fromthenon = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/4fromthenon.ogg","stream" )
			althought = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/5althought.ogg","stream" )
			iwouldnthaveleft = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/6iwouldnthaveleft.ogg","stream" )
			andtheyarequite = love.audio.newSource( "/externalassets/ends/9nextgeneration/audios/7andtheyarequite.ogg","stream" )
			
			--the linux users
			
				frozenbubbles= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/1frozenbubbles.ogg","stream" )
				ifyoudropthree= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/2ifyoudropthree.ogg","stream" )
				onesuperjoke= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/3onesuperjoke.ogg","stream" )
				itsmoreembarrasing= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/4itsmoreembarrasing.ogg","stream" )
				anddoyouknow= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/5anddoyouknow.ogg","stream" )
				lookthisroom= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/6lookthisroom.ogg","stream" )
				whydidyousaid= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/7whydidyousaid.ogg","stream" )
				iamrecitingin= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/8iamrecitingin.ogg","stream" )
				soyoushould= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/9soyoushould.ogg","stream" )
				sorryiforgot= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/10sorryiforgot.ogg","stream" )
				whydidyousaydebian= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/11whydidyousaydebian.ogg","stream" )
				thisgamewas= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/12thisgamewas.ogg","stream" )
				whywasntitdone= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/13whywasntitdone.ogg","stream" )
				ubuntuisjust= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/14ubuntuisjust.ogg","stream" )
				haveyoutried= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/15haveyoutried.ogg","stream" )
				canyoulogin= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/16canyoulogin.ogg","stream" )
				ubuntudoesntneed= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/17ubuntudoesntneed.ogg","stream" )
				butitisbased= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/18butitisbased.ogg","stream" )
				aninterface= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/19aninterface.ogg","stream" )
				whataboutslackware= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/20whataboutslackware.ogg","stream" )
				ohyes= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/21ohyes.ogg","stream" )
				atleastitsowned= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/22atleastitsowned.ogg","stream" )
				doyouknowwhy= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/23doyouknowwhy.ogg","stream" )
				ofcourse= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/24ofcourse.ogg","stream" )
				whendidc= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/25whendidc.ogg","stream" )
				cisancient= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/26cisancient.ogg","stream" )
				java= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/27java.ogg","stream" )
				imnotinterested= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/28imnotinterested.ogg","stream" )
				yesyouare= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/29yesyouare.ogg","stream" )
				lookwilber= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/30lookwilber.ogg","stream" )
				itisthemascot= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/31itisthemascot.ogg","stream" )
				whichistrying= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/32whichistrying.ogg","stream" )
				thegimp= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/33thegimp.ogg","stream" )
				thegimpdevelopers= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/34thegimpdevelopers.ogg","stream" )
				imbored= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/35imbored.ogg","stream" )
				couldntyou= love.audio.newSource( "/externalassets/dialogs/level77/en-us/linuxusers/36couldntyou.ogg","stream" )
				
			
					if accent=="br" then
				elseif accent=="us" then
					whoelsewould= love.audio.newSource( "/externalassets/dialogs/level77/en-us/2whoelsewould.ogg","stream" )
					theywere= love.audio.newSource( "/externalassets/dialogs/level77/en-us/5theywere.ogg","stream" )
					thereisonelast= love.audio.newSource( "/externalassets/dialogs/level77/en-us/7thereisonelast.ogg","stream" )
					wemust= love.audio.newSource( "/externalassets/dialogs/level77/en-us/8wemust.ogg","stream" )
					butifwe= love.audio.newSource( "/externalassets/dialogs/level77/en-us/10butifwe.ogg","stream" )
					noidont= love.audio.newSource( "/externalassets/dialogs/level77/en-us/12noidont.ogg","stream" )
					oratleast= love.audio.newSource( "/externalassets/dialogs/level77/en-us/14oratleast.ogg","stream" )
					buttheyhacked= love.audio.newSource( "/externalassets/dialogs/level77/en-us/16butheyhacked.ogg","stream" )
					wow= love.audio.newSource( "/externalassets/dialogs/level77/en-us/17wow.ogg","stream" )
					iwouldlike= love.audio.newSource( "/externalassets/dialogs/level77/en-us/19iwouldlike.ogg","stream" )
					playerif= love.audio.newSource( "/externalassets/dialogs/level77/en-us/21playerif.ogg","stream" )
					andtheyareusing= love.audio.newSource( "/externalassets/dialogs/level77/en-us/24andtheyareusing.ogg","stream" )
					whathaveyougot= love.audio.newSource( "/externalassets/dialogs/level77/en-us/25whathaveyougot.ogg","stream" )
					
				end
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					fatherwasright = love.audio.newSource( "/externalassets/dialogs/level77/en-us/1fatherwasright.ogg","stream" )
					totopitalloff = love.audio.newSource( "/externalassets/dialogs/level77/en-us/3totopitalloff.ogg","stream" )
					itisclearnow = love.audio.newSource( "/externalassets/dialogs/level77/en-us/4itisclearnow.ogg","stream" )
					thelinuxusers = love.audio.newSource( "/externalassets/dialogs/level77/en-us/6thelinuxusers.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level77/en-us/9ithinkweshould.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level77/en-us/11doyouthink.ogg","stream" )
					ourgoalisto = love.audio.newSource( "/externalassets/dialogs/level77/en-us/13ourgoalisto.ogg","stream" )
					inmyopinion = love.audio.newSource( "/externalassets/dialogs/level77/en-us/15inmyopinion.ogg","stream" )
					butitisntenough = love.audio.newSource( "/externalassets/dialogs/level77/en-us/18butitisntenough.ogg","stream" )
					hewasprobably = love.audio.newSource( "/externalassets/dialogs/level77/en-us/20hewasprobably.ogg","stream" )
					wearenttalkingabout = love.audio.newSource( "/externalassets/dialogs/level77/en-us/22wearenttalkingabout.ogg","stream" )
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level77/en-us/23itsinteresting.ogg","stream" )
				end
			elseif language2=="cs" then
					fatherwasright = love.audio.newSource( "/externalassets/dialogs/level77/cs/1fatherwasright.ogg","stream" )
					totopitalloff = love.audio.newSource( "/externalassets/dialogs/level77/cs/3totopitalloff.ogg","stream" )
					itisclearnow = love.audio.newSource( "/externalassets/dialogs/level77/cs/4itisclearnow.ogg","stream" )
					thelinuxusers = love.audio.newSource( "/externalassets/dialogs/level77/cs/6thelinuxusers.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level77/cs/9ithinkweshould.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level77/cs/11doyouthink.ogg","stream" )
					ourgoalisto = love.audio.newSource( "/externalassets/dialogs/level77/cs/13ourgoalisto.ogg","stream" )
					inmyopinion = love.audio.newSource( "/externalassets/dialogs/level77/cs/15inmyopinion.ogg","stream" )
					butitisntenough = love.audio.newSource( "/externalassets/dialogs/level77/cs/18butitisntenough.ogg","stream" )
					hewasprobably = love.audio.newSource( "/externalassets/dialogs/level77/cs/20hewasprobably.ogg","stream" )
					wearenttalkingabout = love.audio.newSource( "/externalassets/dialogs/level77/cs/22wearenttalkingabout.ogg","stream" )
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level77/cs/23itsinteresting.ogg","stream" )
			
			elseif language2=="pl" then
					fatherwasright = love.audio.newSource( "/externalassets/dialogs/level77/pl/1fatherwasright.ogg","stream" )
					totopitalloff = love.audio.newSource( "/externalassets/dialogs/level77/pl/3totopitalloff.ogg","stream" )
					itisclearnow = love.audio.newSource( "/externalassets/dialogs/level77/pl/4itisclearnow.ogg","stream" )
					thelinuxusers = love.audio.newSource( "/externalassets/dialogs/level77/pl/6thelinuxusers.ogg","stream" )
					ithinkweshould = love.audio.newSource( "/externalassets/dialogs/level77/pl/9ithinkweshould.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level77/pl/11doyouthink.ogg","stream" )
					ourgoalisto = love.audio.newSource( "/externalassets/dialogs/level77/pl/13ourgoalisto.ogg","stream" )
					inmyopinion = love.audio.newSource( "/externalassets/dialogs/level77/pl/15inmyopinion.ogg","stream" )
					butitisntenough = love.audio.newSource( "/externalassets/dialogs/level77/pl/18butitisntenough.ogg","stream" )
					hewasprobably = love.audio.newSource( "/externalassets/dialogs/level77/pl/20hewasprobably.ogg","stream" )
					wearenttalkingabout = love.audio.newSource( "/externalassets/dialogs/level77/pl/22wearenttalkingabout.ogg","stream" )
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level77/pl/23itsinteresting.ogg","stream" )
			end
					--end
					whenwesaw:setEffect('myEffect')
					wedidntunderstand:setEffect('myEffect')
					butfinally:setEffect('myEffect')
					fromthenon:setEffect('myEffect')
					althought:setEffect('myEffect')
					iwouldnthaveleft:setEffect('myEffect')
					andtheyarequite:setEffect('myEffect')
			
			
					--fish 1
					whoelsewould:setEffect('myEffect')
					theywere:setEffect('myEffect')
					thereisonelast:setEffect('myEffect')
					wemust:setEffect('myEffect')
					butifwe:setEffect('myEffect')
					noidont:setEffect('myEffect')
					oratleast:setEffect('myEffect')
					buttheyhacked:setEffect('myEffect')
					wow:setEffect('myEffect')
					iwouldlike:setEffect('myEffect')
					playerif:setEffect('myEffect')
					andtheyareusing:setEffect('myEffect')
					whathaveyougot:setEffect('myEffect')
					
					--fish 2
					fatherwasright:setEffect('myEffect')
					totopitalloff:setEffect('myEffect')
					itisclearnow:setEffect('myEffect')
					thelinuxusers:setEffect('myEffect')
					ithinkweshould:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					ourgoalisto:setEffect('myEffect')
					inmyopinion:setEffect('myEffect')
					butitisntenough:setEffect('myEffect')
					hewasprobably:setEffect('myEffect')
					wearenttalkingabout:setEffect('myEffect')
					itsinteresting:setEffect('myEffect')
					
					--the linux users
					frozenbubbles:setEffect('myEffect')
					ifyoudropthree:setEffect('myEffect')
					onesuperjoke:setEffect('myEffect')
					itsmoreembarrasing:setEffect('myEffect')
					anddoyouknow:setEffect('myEffect')
					lookthisroom:setEffect('myEffect')
					whydidyousaid:setEffect('myEffect')
					iamrecitingin:setEffect('myEffect')
					soyoushould:setEffect('myEffect')
					sorryiforgot:setEffect('myEffect')
					whydidyousaydebian:setEffect('myEffect')
					thisgamewas:setEffect('myEffect')
					whywasntitdone:setEffect('myEffect')
					ubuntuisjust:setEffect('myEffect')
					haveyoutried:setEffect('myEffect')
					canyoulogin:setEffect('myEffect')
					ubuntudoesntneed:setEffect('myEffect')
					butitisbased:setEffect('myEffect')
					aninterface:setEffect('myEffect')
					whataboutslackware:setEffect('myEffect')
					ohyes:setEffect('myEffect')
					atleastitsowned:setEffect('myEffect')
					doyouknowwhy:setEffect('myEffect')
					ofcourse:setEffect('myEffect')
					whendidc:setEffect('myEffect')
					cisancient:setEffect('myEffect')
					java:setEffect('myEffect')
					imnotinterested:setEffect('myEffect')
					yesyouare:setEffect('myEffect')
					lookwilber:setEffect('myEffect')
					itisthemascot:setEffect('myEffect')
					whichistrying:setEffect('myEffect')
					thegimp:setEffect('myEffect')
					thegimpdevelopers:setEffect('myEffect')
					imbored:setEffect('myEffect')
					couldntyou:setEffect('myEffect')
					
		elseif nLevel==79 then
			if language=="en" then
				--end	
				 goodmorningfish = love.audio.newSource( "/externalassets/ends/END/en/1goodmorningfish.ogg","stream" )
				 againyoudidnt = love.audio.newSource( "/externalassets/ends/END/en/2againyoudidnt.ogg","stream" )
				 generalcomittee = love.audio.newSource( "/externalassets/ends/END/en/3generalcomittee.ogg","stream" )
				 theyaremade = love.audio.newSource( "/externalassets/ends/END/en/4theyaremade.ogg","stream" )
				 boss = love.audio.newSource( "/externalassets/ends/END/en/5boss.ogg","stream" )
				 iunderstand = love.audio.newSource( "/externalassets/ends/END/en/6iunderstand.ogg","stream" )
				 sothatwecanprovide = love.audio.newSource( "/externalassets/ends/END/en/7sothatwecanprovide.ogg","stream" )
				 tellme = love.audio.newSource( "/externalassets/ends/END/en/8tellme.ogg","stream" )
				 iwishhewon = love.audio.newSource( "/externalassets/ends/END/en/9iwishhewon.ogg","stream" )
				
				
					if accent=="br" then
				elseif accent=="us" then
					theplayershould = love.audio.newSource( "/externalassets/dialogs/level79/en-us/1theplayershould.ogg","stream" )
					iwonderif = love.audio.newSource( "/externalassets/dialogs/level79/en-us/4iwonderif.ogg","stream" )
					theplayershouldlearn = love.audio.newSource( "/externalassets/dialogs/level79/en-us/6theplayershouldlearn.ogg","stream" )
					whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level79/en-us/10whatareyoudoing.ogg","stream" )
					
					
				end
			elseif language2=="nl" then
				 goodmorningfish = love.audio.newSource( "/externalassets/ends/END/nl/1goodmorningfish.ogg","stream" )
				 againyoudidnt = love.audio.newSource( "/externalassets/ends/END/nl/2againyoudidnt.ogg","stream" )
				 generalcomittee = love.audio.newSource( "/externalassets/ends/END/nl/3generalcomittee.ogg","stream" )
				 theyaremade = love.audio.newSource( "/externalassets/ends/END/nl/4theyaremade.ogg","stream" )
				 boss = love.audio.newSource( "/externalassets/ends/END/nl/5boss.ogg","stream" )
				 iunderstand = love.audio.newSource( "/externalassets/ends/END/nl/6iunderstand.ogg","stream" )
				 sothatwecanprovide = love.audio.newSource( "/externalassets/ends/END/nl/7sothatwecanprovide.ogg","stream" )
				 tellme = love.audio.newSource( "/externalassets/ends/END/nl/8tellme.ogg","stream" )
				 iwishhewon = love.audio.newSource( "/externalassets/ends/END/nl/9iwishhewon.ogg","stream" )
			end
			if language2=="en" then
					if accent2=="br" then
				elseif accent2=="us" then
					byaninvisible = love.audio.newSource( "/externalassets/dialogs/level79/en-us/2byaninvisible.ogg","stream" )
					somebody = love.audio.newSource( "/externalassets/dialogs/level79/en-us/3somebody.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level79/en-us/5whatdoyoumean.ogg","stream" )
					onthecontrary = love.audio.newSource( "/externalassets/dialogs/level79/en-us/7onthecontrary.ogg","stream" )
					weshouldgetout = love.audio.newSource( "/externalassets/dialogs/level79/en-us/8weshouldgetout.ogg","stream" )
				end
			elseif language2=="cs" then
					byaninvisible = love.audio.newSource( "/externalassets/dialogs/level79/cs/2byaninvisible.ogg","stream" )
					somebody = love.audio.newSource( "/externalassets/dialogs/level79/cs/3somebody.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level79/cs/5whatdoyoumean.ogg","stream" )
					onthecontrary = love.audio.newSource( "/externalassets/dialogs/level79/cs/7onthecontrary.ogg","stream" )
					weshouldgetout = love.audio.newSource( "/externalassets/dialogs/level79/cs/8weshouldgetout.ogg","stream" )				
			
			elseif language2=="pl" then
					byaninvisible = love.audio.newSource( "/externalassets/dialogs/level79/pl/2byaninvisible.ogg","stream" )
					somebody = love.audio.newSource( "/externalassets/dialogs/level79/pl/3somebody.ogg","stream" )
					whatdoyoumean = love.audio.newSource( "/externalassets/dialogs/level79/pl/5whatdoyoumean.ogg","stream" )
					onthecontrary = love.audio.newSource( "/externalassets/dialogs/level79/pl/7onthecontrary.ogg","stream" )
					weshouldgetout = love.audio.newSource( "/externalassets/dialogs/level79/pl/8weshouldgetout.ogg","stream" )			
			end
				--end	
				if not goodmorningfish==nil then
				 goodmorningfish:setEffect('myEffect')
				 againyoudidnt:setEffect('myEffect')
				 generalcomittee:setEffect('myEffect')
				 theyaremade:setEffect('myEffect')
				 boss:setEffect('myEffect')
				 iunderstand:setEffect('myEffect')
				 sothatwecanprovide:setEffect('myEffect')
				 tellme:setEffect('myEffect')
				 iwishhewon:setEffect('myEffect')
				end
					--fish 1
					theplayershould:setEffect('myEffect')
					iwonderif:setEffect('myEffect')
					theplayershouldlearn:setEffect('myEffect')
					whatareyoudoing:setEffect('myEffect')
					
					--fish 2
					byaninvisible:setEffect('myEffect')
					somebody:setEffect('myEffect')
					whatdoyoumean:setEffect('myEffect')
					onthecontrary:setEffect('myEffect')
					weshouldgetout:setEffect('myEffect')

		end
end
