local level = {}


level.name = 'level-47'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0},
{3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,6,6,6},
{1,1,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,3,3,0,0,0},
{3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,3,3,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6},
{3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,6,6,6},
{1,1,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,3,1,1,3,0,0,0},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0},
{1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,3,0,0,0},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
{name = 'ocel1',
	heavy = true,
	x = 4,
	y = 16,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	
{name = 'delo',
	heavy = false,
	x = 3,
	y = 3,
	form = 
		{
			{1,1,1,1,1,0},
			{1,1,0,0,1,1},
			{0,0,0,0,1,1},
		},
	},
	
{name = 'ocel1',
	heavy = true,
	x = 26,
	y = 16,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	
{name = 'ocel1',
	heavy = true,
	x = 5,
	y = 9,
	form = 
		{
			{1,1},
			{1,1},
		},
	},

{name = 'ocel1',
	heavy = true,
	x = 25,
	y = 9,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	
{name = 'delorev',
	heavy = false,
	x = 23,
	y = 3,
	form = 
		{
			{0,1,1,1,1,1},
			{1,1,0,0,1,1},
			{1,1,0,0,0,0},
		},
	},
	
{name = 'sud',
	heavy = false,
	x = 12,
	y = 22,
	form = 
		{
			{1,1},
			{1,1},
			{1,1},
		},
	},
	
{name = 'sud',
	heavy = false,
	x = 17,
	y = 5,
	form = 
		{
			{1,1},
			{1,1},
			{1,1},
		},
	},
	
{name = 'mec',
	heavy = false,
	x = 13,
	y = 17,
	form = 
		{
			{0,0,0,0,1,0,0},
			{1,1,1,1,1,1,1},
			{0,0,0,0,1,0,0},
		},
	},
	
{name = 'sekera',
	heavy = false,
	x = 10,
	y = 14,
	form = 
		{
			{1,1},
			{1,0},
			{1,0},
			{1,0},
		},
	},
	
{name = 'delo',
	heavy = false,
	x = 2,
	y = 14,
	form = 
		{
			{1,1,1,1,1,0},
			{1,1,0,0,1,0},
			{0,0,0,0,1,1},
		},
	},

{name = 'delorev',
	heavy = false,
	x = 24,
	y = 14,
	form = 
		{
			{0,1,1,1,1,1},
			{1,1,0,0,1,1},
			{1,1,0,0,0,0},
		},
	},
	
{name = 'koulea',
	heavy = false,
	x = 10,
	y = 24,
	form = 
		{
			{1},
		},
	},
	
{name = 'kouleb',
	heavy = false,
	x = 11,
	y = 24,
	form = 
		{
			{1},
		},
	},
	
{name = 'koulec',
	heavy = false,
	x = 12,
	y = 20,
	form = 
		{
			{1},
		},
	},
	
{name = 'kouled',
	heavy = false,
	x = 9,
	y = 10,
	form = 
		{
			{1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 15,
	y = 8,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 18,
	y = 8,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)

return level
