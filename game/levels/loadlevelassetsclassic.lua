function loadlevelassetsclassic()

--level 1
	
if nLevel==1 then

	-- levels
	if skinupscaled==false then
		--level1frg2 = loadAsset("externalassets/level1/level1frg2.webp", "Image")
		level1frgcl = loadAsset("externalassets/classic/level1/level1frg.webp", "Image")
		level1bckcl = loadAsset("/externalassets/classic/level1/level1bck.webp", "Image")
		level1bck2cl = loadAsset("/externalassets/classic/level1/level1bck2.webp", "Image")
		level1bck3cl = loadAsset("/externalassets/classic/level1/level1bck3.webp", "Image")
	elseif skinupscaled==true then
		--level1frg2 = loadAsset("externalassets/upscaled/level1/level1frg2.webp", "Image")
		--level1frgcl = loadAsset("externalassets/classic/upscaled/level1/level1frg.webp", "Image")
		--level1frgcl = loadAsset("externalassets/classic/upscaled/level1/level1frg.webp", "Image")
		--level1bckcl = readFile("/externalassets/classic/upscaled/level1/level1bck.webp") -- testing lazyloading
		level1frgcl = loadAsset("/externalassets/classic/upscaled/level1/level1frg.webp", "Image")
		level1bckcl =  loadAsset("/externalassets/classic/upscaled/level1/level1bck.webp", "Image")
		level1bck2cl = loadAsset("/externalassets/classic/upscaled/level1/level1bck2.webp", "Image")
		level1bck3cl = loadAsset("/externalassets/classic/upscaled/level1/level1bck3.webp", "Image")
	end
	
	loadwhale() -- load whale animation
	
	c64level1 = loadAsset("/externalassets/levels/level1/retro/c64level1.webp", "Image")
	sinclairlevel1 = loadAsset("/externalassets/levels/level1/retro/sinclairlevel1.webp", "Image")
	neslevel1 = loadAsset("/externalassets/levels/level1/retro/neslevel1.webp", "Image")
	
	--whale sounds
	whalecalllow = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallLow(echo)9.ogg","static" )
	whalecallmid = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallMid(echo)8.ogg","static" )
				
				--objects
				--[[chair1= loadAsset("/externalassets/objects/level1/silla.webp", "Image")
				chair2= loadAsset("/externalassets/objects/level1/silla2.webp", "Image")
				pipe1= loadAsset("/externalassets/objects/level1/pipe1.webp", "Image")
				cushion1= loadAsset("/externalassets/objects/level1/cushion1.webp", "Image")
				table1= loadAsset("/externalassets/objects/level1/table1.webp", "Image")--]]
			if skinupscaled==false then
				chair1= loadAsset("/externalassets/classic/level1/objects/silla.webp", "Image")
				chair2= loadAsset("/externalassets/classic/level1/objects/silla2.webp", "Image")
				pipe1= loadAsset("/externalassets/classic/level1/objects/pipe1.webp", "Image")
				cushion1= loadAsset("/externalassets/classic/level1/objects/cushion1.webp", "Image")
				table1= loadAsset("/externalassets/classic/level1/objects/table1.webp", "Image")
			elseif skinupscaled==true then
				chair1= loadAsset("/externalassets/classic/upscaled/level1/objects/silla.webp", "Image")
				chair2= loadAsset("/externalassets/classic/upscaled/level1/objects/silla2.webp", "Image")
				pipe1= loadAsset("/externalassets/classic/upscaled/level1/objects/pipe1.webp", "Image")
				cushion1= loadAsset("/externalassets/classic/upscaled/level1/objects/cushion1.webp", "Image")
				table1= loadAsset("/externalassets/classic/upscaled/level1/objects/table1.webp", "Image")
			end
				
			--retroobjects
				--sinclair
				z80chair1= loadAsset("/externalassets/objects/level1/sinclair/silla.webp", "Image")
				z80chair2= loadAsset("/externalassets/objects/level1/sinclair/silla2.webp", "Image")
				z80pipe1= loadAsset("/externalassets/objects/level1/sinclair/pipe1.webp", "Image")
				z80cushion1= loadAsset("/externalassets/objects/level1/sinclair/cushion1.webp", "Image")
				z80table1= loadAsset("/externalassets/objects/level1/sinclair/table1.webp", "Image")
				--nes
				neschair1= loadAsset("/externalassets/objects/level1/nes/silla.webp", "Image")
				neschair2= loadAsset("/externalassets/objects/level1/nes/silla2.webp", "Image")
				nespipe1= loadAsset("/externalassets/objects/level1/nes/pipe1.webp", "Image")
				nescushion1= loadAsset("/externalassets/objects/level1/nes/cushion1.webp", "Image")
				nestable1= loadAsset("/externalassets/objects/level1/nes/table1.webp", "Image")
				--c64
				c64chair1= loadAsset("/externalassets/objects/level1/c64/silla.webp", "Image")
				c64chair2= loadAsset("/externalassets/objects/level1/c64/silla2.webp", "Image")
				c64pipe1= loadAsset("/externalassets/objects/level1/c64/pipe1.webp", "Image")
				c64cushion1= loadAsset("/externalassets/objects/level1/c64/cushion1.webp", "Image")
				c64table1= loadAsset("/externalassets/objects/level1/c64/table1.webp", "Image")
				
			
			
elseif nLevel==2 then

 
	
			--background
			if skinupscaled==false then
				level2frgcl  = loadAsset("/externalassets/classic/level2/level2frg.webp", "Image")
				level2bckcl  = loadAsset("/externalassets/classic/level2/level2bck.webp", "Image")
				level2bck2cl = loadAsset("/externalassets/classic/level2/level2bck2.webp", "Image")
			elseif skinupscaled==true then
				level2frgcl  = loadAsset("/externalassets/classic/upscaled/level2/level2frg.webp", "Image")
				--level2bckcl = readFile("/externalassets/classic/upscaled/level2/level2bck.webp") -- testing lazyloading
				level2bckcl  = loadAsset("/externalassets/classic/upscaled/level2/level2bck.webp", "Image")
				level2bck2cl = loadAsset("/externalassets/classic/upscaled/level2/level2bck2.webp", "Image")
			end
		--objects
	if skinupscaled==false then
		warning = loadAsset("/externalassets/classic/level2/objects/warning.webp", "Image")
		briefcase = loadAsset("/externalassets/classic/level2/objects/briefcase.webp", "Image")
		briefcaseopen = loadAsset("/externalassets/classic/level2/objects/briefcaseopen.webp", "Image")
		briefcaseopen2 = loadAsset("/externalassets/classic/level2/objects/briefcaseopen2.webp", "Image")
		bottle = loadAsset("/externalassets/classic/level2/objects/bottle.webp", "Image")
		L = loadAsset("/externalassets/classic/level2/objects/L.webp", "Image")
		hexagonalnut = loadAsset("/externalassets/classic/level2/objects/hexagonalnut.webp", "Image")
		hammer = loadAsset("/externalassets/classic/level2/objects/hammer.webp", "Image")
		pliers = loadAsset("/externalassets/classic/level2/objects/pliers.webp", "Image")
		pipehorizontal = loadAsset("/externalassets/classic/level2/objects/pipehorizontal.webp", "Image")
	elseif skinupscaled==true then
		warning = loadAsset("/externalassets/classic/level2/objects/upscaled/warning.webp", "Image")
		briefcase = loadAsset("/externalassets/classic/level2/objects/upscaled/briefcase.webp", "Image")
		briefcaseopen = loadAsset("/externalassets/classic/level2/objects/upscaled/briefcaseopen.webp", "Image")
		briefcaseopen2 = loadAsset("/externalassets/classic/level2/objects/upscaled/briefcaseopen2.webp", "Image")
		bottle = loadAsset("/externalassets/classic/level2/objects/upscaled/bottle.webp", "Image")
		L = loadAsset("/externalassets/classic/level2/objects/upscaled/L.webp", "Image")
		hexagonalnut = loadAsset("/externalassets/classic/level2/objects/upscaled/hexagonalnut.webp", "Image")
		hammer = loadAsset("/externalassets/classic/level2/objects/upscaled/hammer.webp", "Image")
		pliers = loadAsset("/externalassets/classic/level2/objects/upscaled/pliers.webp", "Image")
		pipehorizontal = loadAsset("/externalassets/classic/level2/objects/upscaled/pipehorizontal.webp", "Image")
	end
	
	--Normals
	
		warningNormal = loadAsset("/externalassets/objects/level2/normals/warningNormals.webp", "Image")
		briefcaseNormal = loadAsset("/externalassets/objects/level2/normals/briefcaseNormals.webp", "Image")
		briefcaseopenNormal = loadAsset("/externalassets/objects/level2/normals/briefcaseopenNormals.webp", "Image")
		briefcaseopen2Normal = loadAsset("/externalassets/objects/level2/normals/briefcaseopenNormals.webp", "Image")
		bottleNormal = loadAsset("/externalassets/objects/level2/normals/bottleNormals.webp", "Image")
		LNormal = loadAsset("/externalassets/objects/level2/normals/LNormals.webp", "Image")
		hexagonalnutNormal = loadAsset("/externalassets/objects/level2/normals/hexagonalnutNormals.webp", "Image")
		hammerNormal = loadAsset("/externalassets/objects/level2/normals/hammerNormals.webp", "Image")
		pliersNormal = loadAsset("/externalassets/objects/level2/normals/pliersNormals.webp", "Image")
		pipehorizontalNormal = loadAsset("/externalassets/objects/level2/normals/pipehorizontalNormals.webp", "Image")
		
		--height maps
	
		warningHeight = loadAsset("/externalassets/objects/level2/heightmaps/warningheightmap.webp", "Image")
		briefcaseHeight = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseheightmap.webp", "Image")
		briefcaseopenHeight = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseopenheightmap.webp", "Image")
		briefcaseopen2Height = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseopen2heightmap.webp", "Image")
		bottleHeight = loadAsset("/externalassets/objects/level2/heightmaps/bottleheightmap.webp", "Image")
		LHeight = loadAsset("/externalassets/objects/level2/heightmaps/Lheightmap.webp", "Image")
		hexagonalnutHeight = loadAsset("/externalassets/objects/level2/heightmaps/hexagonalnutheightmap.webp", "Image")
		hammerHeight = loadAsset("/externalassets/objects/level2/heightmaps/hammerheightmap.webp", "Image")
		pliersHeight = loadAsset("/externalassets/objects/level2/heightmaps/pliersheightmap.webp", "Image")
		pipehorizontalHeight = loadAsset("/externalassets/objects/level2/heightmaps/pipehorizontalheightmap.webp", "Image")
	
elseif nLevel==3 then
		--shader2=false
		if skinupscaled==false then
			level3frg = loadAsset("/externalassets/classic/level3/level3frg.webp", "Image")
			level3bck = loadAsset("/externalassets/classic/level3/level3bck.webp", "Image")
		elseif skinupscaled==true then
			level3frg = loadAsset("/externalassets/classic/upscaled/level3/level3frg.webp", "Image")
			level3bck = loadAsset("/externalassets/classic/upscaled/level3/level3bck.webp", "Image")
		end
			ffish1 = loadAsset("/externalassets/levels/level3/ffish2.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
			--objects
		if skinupscaled==false then
			shortpipe1 = loadAsset("/externalassets/classic/level3/objects/shortpipe1.webp", "Image")
			minipipe = loadAsset("/externalassets/classic/level3/objects/minipipe.webp", "Image")
			pipelonghorizontal = loadAsset("/externalassets/classic/level3/objects/pipelonghorizontal.webp", "Image")
			booklv3 = loadAsset("/externalassets/classic/level3/objects/booklv3.webp", "Image")
			longpipehorizontal = loadAsset("/externalassets/classic/level3/objects/longpipehorizontal.webp", "Image")
			apricosemarmelade = loadAsset("/externalassets/classic/level3/objects/apricosemarmelade.webp", "Image")
			strawberrymarmelade = loadAsset("/externalassets/classic/level3/objects/strawberrymarmelade.webp", "Image")
			honey = loadAsset("/externalassets/classic/level3/objects/honey.webp", "Image")
			oillamp = loadAsset("/externalassets/classic/level3/objects/oillamp.webp", "Image")
			snail = loadAsset("/externalassets/classic/level3/objects/shell.webp", "Image")
			shell = loadAsset("/externalassets/classic/level3/objects/snail.webp", "Image")
			coffee = loadAsset("/externalassets/classic/level3/objects/coffee.webp", "Image")
			plate = loadAsset("/externalassets/classic/level3/objects/plate.webp", "Image")
			axe = loadAsset("/externalassets/classic/level3/objects/axe.webp", "Image")
	elseif skinupscaled==true then
			shortpipe1 = loadAsset("/externalassets/classic/level3/objects/upscaled/shortpipe1.webp", "Image")
			minipipe = loadAsset("/externalassets/classic/level3/objects/upscaled/minipipe.webp", "Image")
			pipelonghorizontal = loadAsset("/externalassets/classic/level3/objects/upscaled/pipelonghorizontal.webp", "Image")
			booklv3 = loadAsset("/externalassets/classic/level3/objects/upscaled/booklv3.webp", "Image")
			longpipehorizontal = loadAsset("/externalassets/classic/level3/objects/upscaled/longpipehorizontal.webp", "Image")
			apricosemarmelade = loadAsset("/externalassets/classic/level3/objects/upscaled/apricosemarmelade.webp", "Image")
			strawberrymarmelade = loadAsset("/externalassets/classic/level3/objects/upscaled/strawberrymarmelade.webp", "Image")
			honey = loadAsset("/externalassets/classic/level3/objects/upscaled/honey.webp", "Image")
			oillamp = loadAsset("/externalassets/classic/level3/objects/upscaled/oillamp.webp", "Image")
			snail = loadAsset("/externalassets/classic/level3/objects/upscaled/shell.webp", "Image")
			shell = loadAsset("/externalassets/classic/level3/objects/upscaled/snail.webp", "Image")
			coffee = loadAsset("/externalassets/classic/level3/objects/upscaled/coffee.webp", "Image")
			plate = loadAsset("/externalassets/classic/level3/objects/upscaled/plate.webp", "Image")
			axe = loadAsset("/externalassets/classic/level3/objects/upscaled/axe.webp", "Image")
	end
elseif nLevel==4 then
	
		if skinupscaled==false then
			level4bck1 = loadAsset("/externalassets/classic/level4/level4bck.webp", "Image")
			level4bck2 = loadAsset("/externalassets/classic/level4/level4bck2.webp", "Image")
			level4frg1 = loadAsset("/externalassets/classic/level4/level4frg.webp", "Image")
		elseif skinupscaled==true then
			level4bck1 = loadAsset("/externalassets/classic/upscaled/level4/level4bck.webp", "Image")
			level4bck2 = loadAsset("/externalassets/classic/upscaled/level4/level4bck2.webp", "Image")
			level4frg1 = loadAsset("/externalassets/classic/upscaled/level4/level4frg.webp", "Image")
		end	
		
		if skinupscaled==false then
			ostnatec_00 = loadAsset("/externalassets/classic/level4/objects/ostnatec_00.webp", "Image")
			ostnatec_01 = loadAsset("/externalassets/classic/level4/objects/ostnatec_01.webp", "Image")
			ostnatec_02 = loadAsset("/externalassets/classic/level4/objects/ostnatec_02.webp", "Image")
			cpipe = loadAsset("/externalassets/classic/level4/objects/cpipe.webp", "Image")
			book1 = loadAsset("/externalassets/classic/level4/objects/book1.webp", "Image")
			book2 = loadAsset("/externalassets/classic/level4/objects/book2.webp", "Image")
			book3 = loadAsset("/externalassets/classic/level4/objects/book3.webp", "Image")
			book4 = loadAsset("/externalassets/classic/level4/objects/book4.webp", "Image")
			book5 = loadAsset("/externalassets/classic/level4/objects/book5.webp", "Image")
			book6 = loadAsset("/externalassets/classic/level4/objects/book6.webp", "Image")
			book7 = loadAsset("/externalassets/classic/level4/objects/book7.webp", "Image")
			book8 = loadAsset("/externalassets/classic/level4/objects/book8.webp", "Image")
			book9 = loadAsset("/externalassets/classic/level4/objects/book9.webp", "Image")
			bookbig1 = loadAsset("/externalassets/classic/level4/objects/bookbig1.webp", "Image")
			booksmall1 = loadAsset("/externalassets/classic/level4/objects/booksmall1.webp", "Image")
			booksmall2 = loadAsset("/externalassets/classic/level4/objects/booksmall2.webp", "Image")
			booksmall3 = loadAsset("/externalassets/classic/level4/objects/booksmall3.webp", "Image")
			bookhoriz1 = loadAsset("/externalassets/classic/level4/objects/bookhoriz1.webp", "Image")
			bookhoriz2 = loadAsset("/externalassets/classic/level4/objects/bookhoriz2.webp", "Image")
		elseif skinupscaled==true then
			ostnatec_00 = loadAsset("/externalassets/classic/level4/objects/upscaled/ostnatec_00.webp", "Image")
			ostnatec_01 = loadAsset("/externalassets/classic/level4/objects/upscaled/ostnatec_01.webp", "Image")
			ostnatec_02 = loadAsset("/externalassets/classic/level4/objects/upscaled/ostnatec_02.webp", "Image")
			cpipe = loadAsset("/externalassets/classic/level4/objects/upscaled/cpipe.webp", "Image")
			book1 = loadAsset("/externalassets/classic/level4/objects/upscaled/book1.webp", "Image")
			book2 = loadAsset("/externalassets/classic/level4/objects/upscaled/book2.webp", "Image")
			book3 = loadAsset("/externalassets/classic/level4/objects/upscaled/book3.webp", "Image")
			book4 = loadAsset("/externalassets/classic/level4/objects/upscaled/book4.webp", "Image")
			book5 = loadAsset("/externalassets/classic/level4/objects/upscaled/book5.webp", "Image")
			book6 = loadAsset("/externalassets/classic/level4/objects/upscaled/book6.webp", "Image")
			book7 = loadAsset("/externalassets/classic/level4/objects/upscaled/book7.webp", "Image")
			book8 = loadAsset("/externalassets/classic/level4/objects/upscaled/book8.webp", "Image")
			book9 = loadAsset("/externalassets/classic/level4/objects/upscaled/book9.webp", "Image")
			bookbig1 = loadAsset("/externalassets/classic/level4/objects/upscaled/bookbig1.webp", "Image")
			booksmall1 = loadAsset("/externalassets/classic/level4/objects/upscaled/booksmall1.webp", "Image")
			booksmall2 = loadAsset("/externalassets/classic/level4/objects/upscaled/booksmall2.webp", "Image")
			booksmall3 = loadAsset("/externalassets/classic/level4/objects/upscaled/booksmall3.webp", "Image")
			bookhoriz1 = loadAsset("/externalassets/classic/level4/objects/upscaled/bookhoriz1.webp", "Image")
			bookhoriz2 = loadAsset("/externalassets/classic/level4/objects/upscaled/bookhoriz2.webp", "Image")
		end
elseif nLevel==5 then

		if skinupscaled==false then
			level5frg = loadAsset("/externalassets/classic/level5/level5frg.webp", "Image")
			level5bck = loadAsset("/externalassets/classic/level5/level5bck.webp", "Image")
			level5bck2 = loadAsset("/externalassets/classic/level5/level5bck2.webp", "Image")
			level5bck3 = loadAsset("/externalassets/classic/level5/level5bck3.webp", "Image")
	elseif skinupscaled==true then
			level5frg = loadAsset("/externalassets/classic/upscaled/level5/level5frg.webp", "Image")
			level5bck = loadAsset("/externalassets/classic/upscaled/level5/level5bck.webp", "Image")
			level5bck2 = loadAsset("/externalassets/classic/upscaled/level5/level5bck2.webp", "Image")
			level5bck3 = loadAsset("/externalassets/classic/upscaled/level5/level5bck3.webp", "Image")
	end
        --objects
        if skinupscaled==false then
			chair1upside= loadAsset("/externalassets/classic/level5/objects/sillaupside1.webp", "Image")
			chair2upside= loadAsset("/externalassets/classic/level5/objects/sillaupside2.webp", "Image")
			table1= loadAsset("/externalassets/classic/level5/objects/table1.webp", "Image")
			plant= loadAsset("/externalassets/classic/level5/objects/plant.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
			
			plz00= loadAsset("/externalassets/classic/level5/objects/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/classic/level5/objects/plz01.webp", "Image")
			plz02= loadAsset("/externalassets/classic/level5/objects/plz02.webp", "Image")
			plz03= loadAsset("/externalassets/classic/level5/objects/plz03.webp", "Image")
			plz04= loadAsset("/externalassets/classic/level5/objects/plz04.webp", "Image")
			plz05= loadAsset("/externalassets/classic/level5/objects/plz05.webp", "Image")
			plz06= loadAsset("/externalassets/classic/level5/objects/plz06.webp", "Image")
		elseif skinupscaled==true then
			chair1upside= loadAsset("/externalassets/classic/level5/objects/upscaled/sillaupside1.webp", "Image")
			chair2upside= loadAsset("/externalassets/classic/level5/objects/upscaled/sillaupside2.webp", "Image")
			table1= loadAsset("/externalassets/classic/level5/objects/upscaled/table1.webp", "Image")
			plant= loadAsset("/externalassets/classic/level5/objects/upscaled/plant.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
			
			plz00= loadAsset("/externalassets/classic/level5/objects/upscaled/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/classic/level5/objects/upscaled/plz01.webp", "Image")
			plz02= loadAsset("/externalassets/classic/level5/objects/upscaled/plz02.webp", "Image")
			plz03= loadAsset("/externalassets/classic/level5/objects/upscaled/plz03.webp", "Image")
			plz04= loadAsset("/externalassets/classic/level5/objects/upscaled/plz04.webp", "Image")
			plz05= loadAsset("/externalassets/classic/level5/objects/upscaled/plz05.webp", "Image")
			plz06= loadAsset("/externalassets/classic/level5/objects/upscaled/plz06.webp", "Image")
		
		end	
		


elseif nLevel==6 then

		if skinupscaled==false then
			level6frg= loadAsset("/externalassets/classic/level6/level6frg.webp", "Image")
			level6bck= loadAsset("/externalassets/classic/level6/level6bck.webp", "Image")
			level6bck2= loadAsset("/externalassets/classic/level6/level6bck2.webp", "Image")
			level6bck3= loadAsset("/externalassets/classic/level6/level6bck3.webp", "Image")
		elseif skinupscaled==true then
			level6frg= loadAsset("/externalassets/classic/upscaled/level6/level6frg.webp", "Image")
			level6bck= loadAsset("/externalassets/classic/upscaled/level6/level6bck.webp", "Image")
			level6bck2= loadAsset("/externalassets/classic/upscaled/level6/level6bck2.webp", "Image")
			level6bck3= loadAsset("/externalassets/classic/upscaled/level6/level6bck3.webp", "Image")
		end
			
			--object
			if skinupscaled==false then
				koste_00= loadAsset("/externalassets/classic/level6/objects/koste_00.webp", "Image")
				koste_01= loadAsset("/externalassets/classic/level6/objects/koste_01.webp", "Image")
				koste_02= loadAsset("/externalassets/classic/level6/objects/koste_02.webp", "Image")
				rock= loadAsset("/externalassets/classic/level6/objects/rock.webp", "Image")
				rock2= loadAsset("/externalassets/classic/level6/objects/rock2.webp", "Image")
				rock3= loadAsset("/externalassets/classic/level6/objects/rock3.webp", "Image")
				rock4= loadAsset("/externalassets/classic/level6/objects/rock4.webp", "Image")
				coal= loadAsset("/externalassets/classic/level6/objects/coal.webp", "Image")
				longpipe= loadAsset("/externalassets/classic/level6/objects/longpipe.webp", "Image")
				wood= loadAsset("/externalassets/classic/level6/objects/wood.webp", "Image")
				wood2= loadAsset("/externalassets/classic/level6/objects/wood2.webp", "Image")
		elseif skinupscaled==true then
				koste_00= loadAsset("/externalassets/classic/level6/objects/upscaled/koste_00.webp", "Image")
				koste_01= loadAsset("/externalassets/classic/level6/objects/upscaled/koste_01.webp", "Image")
				koste_02= loadAsset("/externalassets/classic/level6/objects/upscaled/koste_02.webp", "Image")
				rock= loadAsset("/externalassets/classic/level6/objects/upscaled/rock.webp", "Image")
				rock2= loadAsset("/externalassets/classic/level6/objects/upscaled/rock2.webp", "Image")
				rock3= loadAsset("/externalassets/classic/level6/objects/upscaled/rock3.webp", "Image")
				rock4= loadAsset("/externalassets/classic/level6/objects/upscaled/rock4.webp", "Image")
				coal= loadAsset("/externalassets/classic/level6/objects/upscaled/coal.webp", "Image")
				longpipe= loadAsset("/externalassets/classic/level6/objects/upscaled/longpipe.webp", "Image")
				wood= loadAsset("/externalassets/classic/level6/objects/upscaled/wood.webp", "Image")
				wood2= loadAsset("/externalassets/classic/level6/objects/upscaled/wood2.webp", "Image")
			
		end

elseif nLevel==7 then

		if skinupscaled==false then
			level7frgcl = loadAsset("/externalassets/classic/level7/level7frg.webp", "Image")
			level7bckcl = loadAsset("/externalassets/classic/level7/level7bck.webp", "Image")
		elseif skinupscaled==true then
			level7frgcl = loadAsset("/externalassets/classic/upscaled/level7/level7frg.webp", "Image")
			level7bckcl = loadAsset("/externalassets/classic/upscaled/level7/level7bck.webp", "Image")
		end	
      -- objects
      	if skinupscaled==false then
			
			ocel= loadAsset("/externalassets/classic/level7/objects/ocel.webp", "Image")
			matrace = loadAsset("/externalassets/classic/level7/objects/matrace.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
			
			musla = loadAsset("/externalassets/classic/level7/objects/musla.webp", "Image")
			plz00= loadAsset("/externalassets/classic/level5/objects/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/classic/level5/objects/plz01.webp", "Image")
			plz02= loadAsset("/externalassets/classic/level5/objects/plz02.webp", "Image")
			plz03= loadAsset("/externalassets/classic/level5/objects/plz03.webp", "Image")
			plz04= loadAsset("/externalassets/classic/level5/objects/plz04.webp", "Image")
			plz05= loadAsset("/externalassets/classic/level5/objects/plz05.webp", "Image")
			plz06= loadAsset("/externalassets/classic/level5/objects/plz06.webp", "Image")
		elseif skinupscaled==true then
			ocel= loadAsset("/externalassets/classic/level7/objects/upscaled/ocel.webp", "Image")
			matrace = loadAsset("/externalassets/classic/level7/objects/upscaled/matrace.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
			
			musla = loadAsset("/externalassets/classic/level7/objects/upscaled/musla.webp", "Image")
			plz00= loadAsset("/externalassets/classic/level5/objects/upscaled/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/classic/level5/objects/upscaled/plz01.webp", "Image")
			plz02= loadAsset("/externalassets/classic/level5/objects/upscaled/plz02.webp", "Image")
			plz03= loadAsset("/externalassets/classic/level5/objects/upscaled/plz03.webp", "Image")
			plz04= loadAsset("/externalassets/classic/level5/objects/upscaled/plz04.webp", "Image")
			plz05= loadAsset("/externalassets/classic/level5/objects/upscaled/plz05.webp", "Image")
			plz06= loadAsset("/externalassets/classic/level5/objects/upscaled/plz06.webp", "Image")
	   	end
	   	
      
		--fishs					
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
      
      --dolphin
		dolphin1= loadAsset("/externalassets/objects/level7/dolphin/1.webp", "Image")
		dolphin2= loadAsset("/externalassets/objects/level7/dolphin/2.webp", "Image")
		dolphin3= loadAsset("/externalassets/objects/level7/dolphin/3.webp", "Image")
		dolphin4= loadAsset("/externalassets/objects/level7/dolphin/4.webp", "Image")
		dolphin5= loadAsset("/externalassets/objects/level7/dolphin/5.webp", "Image")
		dolphin6= loadAsset("/externalassets/objects/level7/dolphin/6.webp", "Image")
		dolphin7= loadAsset("/externalassets/objects/level7/dolphin/7.webp", "Image")
		dolphin8= loadAsset("/externalassets/objects/level7/dolphin/8.webp", "Image")
		dolphin9= loadAsset("/externalassets/objects/level7/dolphin/9.webp", "Image")
		dolphin10= loadAsset("/externalassets/objects/level7/dolphin/10.webp", "Image")
		dolphin11= loadAsset("/externalassets/objects/level7/dolphin/11.webp", "Image")
		dolphin12= loadAsset("/externalassets/objects/level7/dolphin/12.webp", "Image")
		dolphin13= loadAsset("/externalassets/objects/level7/dolphin/13.webp", "Image")
		dolphin14= loadAsset("/externalassets/objects/level7/dolphin/14.webp", "Image")
		dolphin15= loadAsset("/externalassets/objects/level7/dolphin/15.webp", "Image")
		dolphin16= loadAsset("/externalassets/objects/level7/dolphin/16.webp", "Image")
		dolphin17= loadAsset("/externalassets/objects/level7/dolphin/17.webp", "Image")
		dolphin18= loadAsset("/externalassets/objects/level7/dolphin/18.webp", "Image")
		dolphin19= loadAsset("/externalassets/objects/level7/dolphin/19.webp", "Image")
		dolphin20= loadAsset("/externalassets/objects/level7/dolphin/20.webp", "Image")
		dolphin21= loadAsset("/externalassets/objects/level7/dolphin/21.webp", "Image")
		dolphin22= loadAsset("/externalassets/objects/level7/dolphin/22.webp", "Image")
		dolphin23= loadAsset("/externalassets/objects/level7/dolphin/23.webp", "Image")
		dolphin24= loadAsset("/externalassets/objects/level7/dolphin/24.webp", "Image")
		dolphin25= loadAsset("/externalassets/objects/level7/dolphin/25.webp", "Image")
     


elseif nLevel==8 then
	if skinupscaled==false then
		level8bckcl= loadAsset("/externalassets/classic/level8/level8bck.webp", "Image")
		level8bck2cl= loadAsset("/externalassets/classic/level8/level8bck2.webp", "Image")
		level8bck3cl= loadAsset("/externalassets/classic/level8/level8bck3.webp", "Image")
		level8frgcl= loadAsset("/externalassets/classic/level8/level8frg.webp", "Image")
	elseif skinupscaled==true then
		level8bckcl= loadAsset("/externalassets/classic/upscaled/level8/level8bck.webp", "Image")
		level8bck2cl= loadAsset("/externalassets/classic/upscaled/level8/level8bck2.webp", "Image")
		level8bck3cl= loadAsset("/externalassets/classic/upscaled/level8/level8bck3.webp", "Image")
		level8frgcl= loadAsset("/externalassets/classic/upscaled/level8/level8frg.webp", "Image")
	end
	--objects
	if skinupscaled==false then
		paper= loadAsset("/externalassets/classic/level8/objects/paper.webp", "Image")
		wc= loadAsset("/externalassets/classic/level8/objects/wc.webp", "Image")
		pipe= loadAsset("/externalassets/classic/level8/objects/pipe.webp", "Image")
		pipe2= loadAsset("/externalassets/classic/level8/objects/pipe2.webp", "Image")
	elseif skinupscaled==true then
		paper= loadAsset("/externalassets/classic/level8/objects/upscaled/paper.webp", "Image")
		wc= loadAsset("/externalassets/classic/level8/objects/upscaled/wc.webp", "Image")
		pipe= loadAsset("/externalassets/classic/level8/objects/upscaled/pipe.webp", "Image")
		pipe2= loadAsset("/externalassets/classic/level8/objects/upscaled/pipe2.webp", "Image")
	end
elseif nLevel==9 then
	shader2=false
	
		if skinupscaled==false then
			level9bckcl= loadAsset("/externalassets/classic/level9/level9bck.webp", "Image")
			level9frgcl= loadAsset("/externalassets/classic/level9/level9frg.webp", "Image")
		elseif skinupscaled==true then
			level9bckcl= loadAsset("/externalassets/classic/upscaled/level9/level9bck.webp", "Image")
			level9frgcl= loadAsset("/externalassets/classic/upscaled/level9/level9frg.webp", "Image")
		end
		
		--objects
		mirrorpositionx=0
		if skinupscaled==false then
			mirror= loadAsset("/externalassets/classic/level9/objects/mirror.webp", "Image")
			kriz= loadAsset("/externalassets/classic/level9/objects/kriz.webp", "Image")
			lahev= loadAsset("/externalassets/classic/level9/objects/lahev.webp", "Image")
			matka_a= loadAsset("/externalassets/classic/level9/objects/hexagonalnut.webp", "Image")
			naboj= loadAsset("/externalassets/classic/level9/objects/naboj.webp", "Image")
			peri00= loadAsset("/externalassets/classic/level9/objects/peri00.webp", "Image")
			peri01= loadAsset("/externalassets/classic/level9/objects/peri01.webp", "Image")
			peri02= loadAsset("/externalassets/classic/level9/objects/peri02.webp", "Image")
			peri03= loadAsset("/externalassets/classic/level9/objects/peri03.webp", "Image")
			peri04= loadAsset("/externalassets/classic/level9/objects/peri04.webp", "Image")
			peri05= loadAsset("/externalassets/classic/level9/objects/peri05.webp", "Image")
			peri06= loadAsset("/externalassets/classic/level9/objects/peri06.webp", "Image")
			peri07= loadAsset("/externalassets/classic/level9/objects/peri07.webp", "Image")
		elseif skinupscaled==true then
			mirror= loadAsset("/externalassets/classic/level9/objects/upscaled/mirror.webp", "Image")
			kriz= loadAsset("/externalassets/classic/level9/objects/upscaled/kriz.webp", "Image")
			lahev= loadAsset("/externalassets/classic/level9/objects/upscaled/lahev.webp", "Image")
			matka_a= loadAsset("/externalassets/classic/level9/objects/upscaled/hexagonalnut.webp", "Image")
			naboj= loadAsset("/externalassets/classic/level9/objects/upscaled/naboj.webp", "Image")
			peri00= loadAsset("/externalassets/classic/level9/objects/upscaled/peri00.webp", "Image")
			peri01= loadAsset("/externalassets/classic/level9/objects/upscaled/peri01.webp", "Image")
			peri02= loadAsset("/externalassets/classic/level9/objects/upscaled/peri02.webp", "Image")
			peri03= loadAsset("/externalassets/classic/level9/objects/upscaled/peri03.webp", "Image")
			peri04= loadAsset("/externalassets/classic/level9/objects/upscaled/peri04.webp", "Image")
			peri05= loadAsset("/externalassets/classic/level9/objects/upscaled/peri05.webp", "Image")
			peri06= loadAsset("/externalassets/classic/level9/objects/upscaled/peri06.webp", "Image")
			peri07= loadAsset("/externalassets/classic/level9/objects/upscaled/peri07.webp", "Image")
		end
		if skinupscaled==false then
			player1mirror = loadAsset("assets/players/classic/mirror/player1mirror.webp", "Image")
			player1mirror2 = loadAsset("assets/players/classic/mirror/player1mirror2.webp", "Image")
			player1mirror3 = loadAsset("assets/players/classic/mirror/player1mirror3.webp", "Image")
		
			player1mirrorleft = loadAsset("assets/players/classic/mirror/player1mirrorleft.webp", "Image")
			player1mirror2left = loadAsset("assets/players/classic/mirror/player1mirror2left.webp", "Image")
			player1mirror3left = loadAsset("assets/players/classic/mirror/player1mirror3left.webp", "Image")
		elseif skinupscaled==true then
			player1mirror = loadAsset("assets/players/classic/upscaled/mirror/player1mirror.webp", "Image")
			player1mirror2 = loadAsset("assets/players/classic/upscaled/mirror/player1mirror2.webp", "Image")
			player1mirror3 = loadAsset("assets/players/classic/upscaled/mirror/player1mirror3.webp", "Image")
		
			player1mirrorleft = loadAsset("assets/players/classic/upscaled/mirror/player1mirrorleft.webp", "Image")
			player1mirror2left = loadAsset("assets/players/classic/upscaled/mirror/player1mirror2left.webp", "Image")
			player1mirror3left = loadAsset("assets/players/classic/upscaled/mirror/player1mirror3left.webp", "Image")
		
		
		end
		
elseif nLevel==10 or nLevel==18 then
	shader2=false

		
			if skinupscaled==false and nLevel==10 then
				level10frgcl= loadAsset("/externalassets/classic/level10/level10frg.webp", "Image")
				level10bckcl= loadAsset("/externalassets/classic/level10/level10bck.webp", "Image")
			elseif skinupscaled==false and nLevel==18 then
				level18frgcl= loadAsset("/externalassets/classic/level18/level18frg.webp", "Image")
				level18bckcl= loadAsset("/externalassets/classic/level18/level18bck.webp", "Image")
			elseif skinupscaled==true and nLevel==10 then
				level10frgcl= loadAsset("/externalassets/classic/upscaled/level10/level10frg.webp", "Image")
				level10bckcl= loadAsset("/externalassets/classic/upscaled/level10/level10bck.webp", "Image")
			elseif skinupscaled==true and nLevel==18 then
				level18frgcl= loadAsset("/externalassets/classic/upscaled/level18/level18frg.webp", "Image")
				level18bckcl= loadAsset("/externalassets/classic/upscaled/level18/level18bck.webp", "Image")
			end
		
		if skinupscaled==false then
			pipe = loadAsset("/externalassets/classic/level10/objects/pipe.webp", "Image")
			cruiseship = loadAsset("/externalassets/classic/level10/objects/cruise.webp", "Image")
			glass = loadAsset("/externalassets/classic/level10/objects/sklenicka_00.webp", "Image")
			glass2 = loadAsset("/externalassets/classic/level10/objects/sklenicka_01.webp", "Image")
			glass3 = loadAsset("/externalassets/classic/level10/objects/sklenicka_02.webp", "Image")
			
			glassstand = loadAsset("/externalassets/classic/level10/objects/tacek_00.webp", "Image")
			glassstand2 = loadAsset("/externalassets/classic/level10/objects/tacek_01.webp", "Image")
			glassstand3 = loadAsset("/externalassets/classic/level10/objects/tacek_02.webp", "Image")
			
			
			 dama00= loadAsset("/externalassets/classic/level10/objects/dama_00.webp", "Image")
			 dama01= loadAsset("/externalassets/classic/level10/objects/dama_01.webp", "Image")
			 dama02= loadAsset("/externalassets/classic/level10/objects/dama_02.webp", "Image")
			 dama03= loadAsset("/externalassets/classic/level10/objects/dama_03.webp", "Image")
			 dama04= loadAsset("/externalassets/classic/level10/objects/dama_04.webp", "Image")
			 dama05= loadAsset("/externalassets/classic/level10/objects/dama_05.webp", "Image")
			 dama06= loadAsset("/externalassets/classic/level10/objects/dama_06.webp", "Image")
			 dama07= loadAsset("/externalassets/classic/level10/objects/dama_07.webp", "Image")
			 dama08= loadAsset("/externalassets/classic/level10/objects/dama_08.webp", "Image")
			 dama09= loadAsset("/externalassets/classic/level10/objects/dama_09.webp", "Image")
			 dama10= loadAsset("/externalassets/classic/level10/objects/dama_10.webp", "Image")
			 dama11= loadAsset("/externalassets/classic/level10/objects/dama_11.webp", "Image")
			 dama12= loadAsset("/externalassets/classic/level10/objects/dama_12.webp", "Image")
			 dama13= loadAsset("/externalassets/classic/level10/objects/dama_13.webp", "Image")
			 dama14= loadAsset("/externalassets/classic/level10/objects/dama_14.webp", "Image")
			 dama15= loadAsset("/externalassets/classic/level10/objects/dama_15.webp", "Image")
			 
			 frkavec00= loadAsset("/externalassets/classic/level10/objects/frkavec_00.webp", "Image")
			 frkavec01= loadAsset("/externalassets/classic/level10/objects/frkavec_01.webp", "Image")
			 frkavec02= loadAsset("/externalassets/classic/level10/objects/frkavec_02.webp", "Image")
			 frkavec03= loadAsset("/externalassets/classic/level10/objects/frkavec_03.webp", "Image")
			 frkavec04= loadAsset("/externalassets/classic/level10/objects/frkavec_04.webp", "Image")
			 frkavec05= loadAsset("/externalassets/classic/level10/objects/frkavec_05.webp", "Image")
			 frkavec06= loadAsset("/externalassets/classic/level10/objects/frkavec_06.webp", "Image")
			 
			 kap00= loadAsset("/externalassets/classic/level10/objects/kap_00.webp", "Image")
			 kap01= loadAsset("/externalassets/classic/level10/objects/kap_01.webp", "Image")
			 kap02= loadAsset("/externalassets/classic/level10/objects/kap_02.webp", "Image")
			 kap03= loadAsset("/externalassets/classic/level10/objects/kap_03.webp", "Image")
			 kap04= loadAsset("/externalassets/classic/level10/objects/kap_04.webp", "Image")
			 kap05= loadAsset("/externalassets/classic/level10/objects/kap_05.webp", "Image")
			 kap06= loadAsset("/externalassets/classic/level10/objects/kap_06.webp", "Image")
			 kap07= loadAsset("/externalassets/classic/level10/objects/kap_07.webp", "Image")
			 kap08= loadAsset("/externalassets/classic/level10/objects/kap_08.webp", "Image")
			 kap09= loadAsset("/externalassets/classic/level10/objects/kap_09.webp", "Image")
			 kap10= loadAsset("/externalassets/classic/level10/objects/kap_10.webp", "Image")
			 kap11= loadAsset("/externalassets/classic/level10/objects/kap_11.webp", "Image")
			 kap12= loadAsset("/externalassets/classic/level10/objects/kap_12.webp", "Image")
			 kap13= loadAsset("/externalassets/classic/level10/objects/kap_13.webp", "Image")
			 kap14= loadAsset("/externalassets/classic/level10/objects/kap_14.webp", "Image")
			 kap15= loadAsset("/externalassets/classic/level10/objects/kap_15.webp", "Image")
			 kap16= loadAsset("/externalassets/classic/level10/objects/kap_16.webp", "Image")
			 kap17= loadAsset("/externalassets/classic/level10/objects/kap_17.webp", "Image")
			 kap18= loadAsset("/externalassets/classic/level10/objects/kap_18.webp", "Image")
			 
			 lodnik00= loadAsset("/externalassets/classic/level10/objects/lodnik_00.webp", "Image")
			 lodnik01= loadAsset("/externalassets/classic/level10/objects/lodnik_01.webp", "Image")
			 lodnik02= loadAsset("/externalassets/classic/level10/objects/lodnik_02.webp", "Image")
			 lodnik03= loadAsset("/externalassets/classic/level10/objects/lodnik_03.webp", "Image")
			 lodnik04= loadAsset("/externalassets/classic/level10/objects/lodnik_04.webp", "Image")
			 lodnik05= loadAsset("/externalassets/classic/level10/objects/lodnik_05.webp", "Image")
			 lodnik06= loadAsset("/externalassets/classic/level10/objects/lodnik_06.webp", "Image")
			 lodnik07= loadAsset("/externalassets/classic/level10/objects/lodnik_07.webp", "Image")
			 lodnik08= loadAsset("/externalassets/classic/level10/objects/lodnik_08.webp", "Image")
			 lodnik09= loadAsset("/externalassets/classic/level10/objects/lodnik_09.webp", "Image")
			 lodnik10= loadAsset("/externalassets/classic/level10/objects/lodnik_10.webp", "Image")
			 lodnik11= loadAsset("/externalassets/classic/level10/objects/lodnik_11.webp", "Image")
			 lodnik12= loadAsset("/externalassets/classic/level10/objects/lodnik_12.webp", "Image")
			 lodnik13= loadAsset("/externalassets/classic/level10/objects/lodnik_13.webp", "Image")
			 lodnik14= loadAsset("/externalassets/classic/level10/objects/lodnik_14.webp", "Image")
			 lodnik15= loadAsset("/externalassets/classic/level10/objects/lodnik_15.webp", "Image")
			 lodnik16= loadAsset("/externalassets/classic/level10/objects/lodnik_16.webp", "Image")
			 lodnik17= loadAsset("/externalassets/classic/level10/objects/lodnik_17.webp", "Image")
			 lodnik18= loadAsset("/externalassets/classic/level10/objects/lodnik_18.webp", "Image")
			 lodnik19= loadAsset("/externalassets/classic/level10/objects/lodnik_19.webp", "Image")
			 lodnik20= loadAsset("/externalassets/classic/level10/objects/lodnik_20.webp", "Image")
			 lodnik21= loadAsset("/externalassets/classic/level10/objects/lodnik_21.webp", "Image")
			 lodnik22= loadAsset("/externalassets/classic/level10/objects/lodnik_22.webp", "Image")
		elseif skinupscaled==true then
			pipe = loadAsset("/externalassets/classic/level10/objects/upscaled/pipe.webp", "Image")
			cruiseship = loadAsset("/externalassets/classic/level10/objects/upscaled/cruise.webp", "Image")
			glass = loadAsset("/externalassets/classic/level10/objects/upscaled/sklenicka_00.webp", "Image")
			glass2 = loadAsset("/externalassets/classic/level10/objects/upscaled/sklenicka_01.webp", "Image")
			glass3 = loadAsset("/externalassets/classic/level10/objects/upscaled/sklenicka_02.webp", "Image")
			
			glassstand = loadAsset("/externalassets/classic/level10/objects/upscaled/tacek_00.webp", "Image")
			glassstand2 = loadAsset("/externalassets/classic/level10/objects/upscaled/tacek_01.webp", "Image")
			glassstand3 = loadAsset("/externalassets/classic/level10/objects/upscaled/tacek_02.webp", "Image")
			
			
			 dama00= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_00.webp", "Image")
			 dama01= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_01.webp", "Image")
			 dama02= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_02.webp", "Image")
			 dama03= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_03.webp", "Image")
			 dama04= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_04.webp", "Image")
			 dama05= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_05.webp", "Image")
			 dama06= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_06.webp", "Image")
			 dama07= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_07.webp", "Image")
			 dama08= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_08.webp", "Image")
			 dama09= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_09.webp", "Image")
			 dama10= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_10.webp", "Image")
			 dama11= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_11.webp", "Image")
			 dama12= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_12.webp", "Image")
			 dama13= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_13.webp", "Image")
			 dama14= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_14.webp", "Image")
			 dama15= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_15.webp", "Image")
			 
			 frkavec00= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_00.webp", "Image")
			 frkavec01= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_01.webp", "Image")
			 frkavec02= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_02.webp", "Image")
			 frkavec03= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_03.webp", "Image")
			 frkavec04= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_04.webp", "Image")
			 frkavec05= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_05.webp", "Image")
			 frkavec06= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_06.webp", "Image")
			 
			 kap00= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_00.webp", "Image")
			 kap01= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_01.webp", "Image")
			 kap02= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_02.webp", "Image")
			 kap03= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_03.webp", "Image")
			 kap04= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_04.webp", "Image")
			 kap05= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_05.webp", "Image")
			 kap06= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_06.webp", "Image")
			 kap07= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_07.webp", "Image")
			 kap08= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_08.webp", "Image")
			 kap09= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_09.webp", "Image")
			 kap10= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_10.webp", "Image")
			 kap11= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_11.webp", "Image")
			 kap12= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_12.webp", "Image")
			 kap13= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_13.webp", "Image")
			 kap14= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_14.webp", "Image")
			 kap15= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_15.webp", "Image")
			 kap16= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_16.webp", "Image")
			 kap17= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_17.webp", "Image")
			 kap18= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_18.webp", "Image")
			 
			 lodnik00= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_00.webp", "Image")
			 lodnik01= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_01.webp", "Image")
			 lodnik02= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_02.webp", "Image")
			 lodnik03= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_03.webp", "Image")
			 lodnik04= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_04.webp", "Image")
			 lodnik05= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_05.webp", "Image")
			 lodnik06= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_06.webp", "Image")
			 lodnik07= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_07.webp", "Image")
			 lodnik08= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_08.webp", "Image")
			 lodnik09= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_09.webp", "Image")
			 lodnik10= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_10.webp", "Image")
			 lodnik11= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_11.webp", "Image")
			 lodnik12= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_12.webp", "Image")
			 lodnik13= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_13.webp", "Image")
			 lodnik14= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_14.webp", "Image")
			 lodnik15= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_15.webp", "Image")
			 lodnik16= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_16.webp", "Image")
			 lodnik17= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_17.webp", "Image")
			 lodnik18= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_18.webp", "Image")
			 lodnik19= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_19.webp", "Image")
			 lodnik20= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_20.webp", "Image")
			 lodnik21= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_21.webp", "Image")
			 lodnik22= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_22.webp", "Image")
			
		end
		
elseif nLevel==11 then
	elkframe=0
	deadparrotframe=0
	snailframe=0
		if skinupscaled==false then
			level11bckcl= loadAsset("/externalassets/classic/level11/level11bck.webp", "Image")
      		level11frgcl= loadAsset("/externalassets/classic/level11/level11frg.webp", "Image")
		elseif skinupscaled==true then
			level11bckcl= loadAsset("/externalassets/classic/upscaled/level11/level11bck.webp", "Image")
      		level11frgcl= loadAsset("/externalassets/classic/upscaled/level11/level11frg.webp", "Image")
		end
			--objects
		if skinupscaled==false then
			pipe11= loadAsset("/externalassets/classic/level11/objects/pipe11.webp", "Image")
			pipe4_11= loadAsset("/externalassets/classic/level11/objects/pipe4_11.webp", "Image")
			maly_snek_00= loadAsset("/externalassets/classic/level11/objects/maly_snek_00.webp", "Image")
			maly_snek_01= loadAsset("/externalassets/classic/level11/objects/maly_snek_01.webp", "Image")
			maly_snek_02= loadAsset("/externalassets/classic/level11/objects/maly_snek_02.webp", "Image")
			maly_snek_03= loadAsset("/externalassets/classic/level11/objects/maly_snek_03.webp", "Image")
			bullet11= loadAsset("/externalassets/classic/level11/objects/bullet11.webp", "Image")
			elk = loadAsset("/externalassets/classic/level11/objects/elk.webp", "Image")
			los_00 = loadAsset("/externalassets/classic/level11/objects/los_00.webp", "Image")
			los_01 = loadAsset("/externalassets/classic/level11/objects/los_01.webp", "Image")
			los_02 = loadAsset("/externalassets/classic/level11/objects/los_02.webp", "Image")
			los_03 = loadAsset("/externalassets/classic/level11/objects/los_03.webp", "Image")
			papoucha_00 = loadAsset("/externalassets/classic/level11/objects/papoucha_00.webp", "Image")
			papoucha_01 = loadAsset("/externalassets/classic/level11/objects/papoucha_01.webp", "Image")
			
	elseif skinupscaled==true then
			pipe11= loadAsset("/externalassets/classic/level11/objects/upscaled/pipe11.webp", "Image")
			pipe4_11= loadAsset("/externalassets/classic/level11/objects/upscaled/pipe4_11.webp", "Image")
			maly_snek_00= loadAsset("/externalassets/classic/level11/objects/upscaled/maly_snek_00.webp", "Image")
			maly_snek_01= loadAsset("/externalassets/classic/level11/objects/upscaled/maly_snek_01.webp", "Image")
			maly_snek_02= loadAsset("/externalassets/classic/level11/objects/upscaled/maly_snek_02.webp", "Image")
			maly_snek_03= loadAsset("/externalassets/classic/level11/objects/upscaled/maly_snek_03.webp", "Image")
			bullet11= loadAsset("/externalassets/classic/level11/objects/upscaled/bullet11.webp", "Image")
			elk = loadAsset("/externalassets/classic/level11/objects/upscaled/elk.webp", "Image")
			los_00 = loadAsset("/externalassets/classic/level11/objects/upscaled/los_00.webp", "Image")
			los_01 = loadAsset("/externalassets/classic/level11/objects/upscaled/los_01.webp", "Image")
			los_02 = loadAsset("/externalassets/classic/level11/objects/upscaled/los_02.webp", "Image")
			los_03 = loadAsset("/externalassets/classic/level11/objects/upscaled/los_03.webp", "Image")
			papoucha_00 = loadAsset("/externalassets/classic/level11/objects/upscaled/papoucha_00.webp", "Image")
			papoucha_01 = loadAsset("/externalassets/classic/level11/objects/upscaled/papoucha_01.webp", "Image")
	end

elseif nLevel==12 then
	medusaframe=0
	rybickaframe=0
	if skinupscaled==false then
		level12frgcl = loadAsset("/externalassets/classic/level12/level12frg.webp", "Image")
		level12bckcl = loadAsset("/externalassets/classic/level12/level12bck.webp", "Image")
	elseif skinupscaled==true then	
		level12frgcl = loadAsset("/externalassets/classic/upscaled/level12/level12frg.webp", "Image")
		level12bckcl = loadAsset("/externalassets/classic/upscaled/level12/level12bck.webp", "Image")
	end
	
	
		if skinupscaled==false then
			cepicka = loadAsset("/externalassets/classic/level12/objects/cepicka.webp", "Image")
			rybicka_00 = loadAsset("/externalassets/classic/level12/objects/rybicka_h_00.webp", "Image")
			rybicka_01 = loadAsset("/externalassets/classic/level12/objects/rybicka_h_01.webp", "Image")
			rybicka_02 = loadAsset("/externalassets/classic/level12/objects/rybicka_h_02.webp", "Image")
			rybicka_03 = loadAsset("/externalassets/classic/level12/objects/rybicka_h_03.webp", "Image")
			hat = loadAsset("/externalassets/classic/level12/objects/hat.webp", "Image")
			medusa_00 = loadAsset("/externalassets/classic/level12/objects/medusa_00.webp", "Image")
			medusa_01 = loadAsset("/externalassets/classic/level12/objects/medusa_01.webp", "Image")
			medusa_02 = loadAsset("/externalassets/classic/level12/objects/medusa_02.webp", "Image")
			muslicka = loadAsset("/externalassets/classic/level12/objects/muslicka.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level12/objects/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level12/objects/pipe2.webp", "Image")
			
	elseif skinupscaled==true then
			cepicka = loadAsset("/externalassets/classic/level12/objects/upscaled/cepicka.webp", "Image")
			rybicka_00 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_00.webp", "Image")
			rybicka_01 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_01.webp", "Image")
			rybicka_02 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_02.webp", "Image")
			rybicka_03 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_03.webp", "Image")
			hat = loadAsset("/externalassets/classic/level12/objects/upscaled/hat.webp", "Image")
			medusa_00 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_00.webp", "Image")
			medusa_01 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_01.webp", "Image")
			medusa_02 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_02.webp", "Image")
			muslicka = loadAsset("/externalassets/classic/level12/objects/upscaled/muslicka.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level12/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level12/objects/upscaled/pipe2.webp", "Image")
	end
		
		elseif nLevel==13 or nLevel==17 then
		shader2=false
				vikingframe=0
				snailframe=0
					if nLevel==13 then
						if skinupscaled==false then
							level13frgcl = loadAsset("/externalassets/classic/level13/level13frg.webp", "Image")
							level13bckcl= loadAsset("/externalassets/classic/level13/level13bck.webp", "Image")
						elseif skinupscaled==true then
							level13frgcl = loadAsset("/externalassets/classic/upscaled/level13/level13frg.webp", "Image")
							level13bckcl= loadAsset("/externalassets/classic/upscaled/level13/level13bck.webp", "Image")
						end
					elseif nLevel==17 then
						if skinupscaled==false then
							level17bckcl= loadAsset("/externalassets/classic/level17/level17bck.webp", "Image")
							level17frgcl= loadAsset("/externalassets/classic/level17/level17frg.webp", "Image")
						elseif skinupscaled==true then
							level17bckcl= loadAsset("/externalassets/classic/upscaled/level17/level17bck.webp", "Image")
							level17frgcl= loadAsset("/externalassets/classic/upscaled/level17/level17frg.webp", "Image")
						end
					end
						
						--objects
					if skinupscaled==false then
						axe1 = loadAsset("/externalassets/classic/level13/objects/axe1.webp", "Image")
						axe2 = loadAsset("/externalassets/classic/level13/objects/axe2.webp", "Image")
						shield1 = loadAsset("/externalassets/classic/level13/objects/shield1.webp", "Image")
						shield2 = loadAsset("/externalassets/classic/level13/objects/shield2.webp", "Image")
						snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
						snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
						snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
						snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
				
						vik1_00 = loadAsset("/externalassets/classic/level17/objects/vik1_00.webp", "Image")
						vik1_01 = loadAsset("/externalassets/classic/level17/objects/vik1_01.webp", "Image")
						vik1_02 = loadAsset("/externalassets/classic/level17/objects/vik1_02.webp", "Image")
						vik1_03 = loadAsset("/externalassets/classic/level17/objects/vik1_03.webp", "Image")
						vik1_04 = loadAsset("/externalassets/classic/level17/objects/vik1_04.webp", "Image")
						
						vik2_00 = loadAsset("/externalassets/classic/level17/objects/vik2_00.webp", "Image")
						vik2_01 = loadAsset("/externalassets/classic/level17/objects/vik2_01.webp", "Image")
						vik2_02 = loadAsset("/externalassets/classic/level17/objects/vik2_02.webp", "Image")
						vik2_03 = loadAsset("/externalassets/classic/level17/objects/vik2_03.webp", "Image")
						vik2_04 = loadAsset("/externalassets/classic/level17/objects/vik2_04.webp", "Image")
						
						vik3_00= loadAsset("/externalassets//classic/level17/objects/vik3_00.webp", "Image")
						vik3_01= loadAsset("/externalassets//classic/level17/objects/vik3_01.webp", "Image")
						vik3_02= loadAsset("/externalassets//classic/level17/objects/vik3_02.webp", "Image")
						vik3_03= loadAsset("/externalassets//classic/level17/objects/vik3_03.webp", "Image")
						vik3_04= loadAsset("/externalassets//classic/level17/objects/vik3_04.webp", "Image")
						
						vik5_00= loadAsset("/externalassets/classic/level17/objects/vik5_00.webp", "Image")
						vik5_01= loadAsset("/externalassets/classic/level17/objects/vik5_01.webp", "Image")
						vik5_02= loadAsset("/externalassets/classic/level17/objects/vik5_02.webp", "Image")
						vik5_03= loadAsset("/externalassets/classic/level17/objects/vik5_03.webp", "Image")
						
						vik6_00= loadAsset("/externalassets/classic/level17/objects/vik6_00.webp", "Image")
						vik6_01= loadAsset("/externalassets/classic/level17/objects/vik6_01.webp", "Image")
						vik6_02= loadAsset("/externalassets/classic/level17/objects/vik6_02.webp", "Image")
						vik6_03= loadAsset("/externalassets/classic/level17/objects/vik6_03.webp", "Image")
						
						vik7_00= loadAsset("/externalassets/classic/level17/objects/vik7_00.webp", "Image")
						vik7_01= loadAsset("/externalassets/classic/level17/objects/vik7_01.webp", "Image")
						vik7_02= loadAsset("/externalassets/classic/level17/objects/vik7_02.webp", "Image")
						vik7_03= loadAsset("/externalassets/classic/level17/objects/vik7_03.webp", "Image")
						vik7_04= loadAsset("/externalassets/classic/level17/objects/vik7_04.webp", "Image")
						
						skull= loadAsset("/externalassets/classic/level13/objects/skull.webp", "Image")
						
						--mastil
						drakar00= loadAsset("/externalassets/classic/level17/objects/drakar-hlava_00.webp", "Image")
						drakar01= loadAsset("/externalassets/classic/level17/objects/drakar-hlava_01.webp", "Image")
						drakar02= loadAsset("/externalassets/classic/level17/objects/drakar-hlava_02.webp", "Image")
						--dog
						pesos_00 = loadAsset("/externalassets/classic/level17/objects/pesos_00.webp", "Image")
						pesos_01 = loadAsset("/externalassets/classic/level17/objects/pesos_01.webp", "Image")
						pesos_02 = loadAsset("/externalassets/classic/level17/objects/pesos_02.webp", "Image")
						--pipes
						pipe= loadAsset("/externalassets/classic/level17/objects/pipe.webp", "Image")
						pipe2= loadAsset("/externalassets/classic/level17/objects/pipe2.webp", "Image")
						
						korunka= loadAsset("/externalassets/classic/level17/objects/korunka.webp", "Image")
						shield= loadAsset("/externalassets/classic/level17/objects/shield.webp", "Image")
						
				elseif skinupscaled==true then
						axe1 = loadAsset("/externalassets/classic/level13/objects/upscaled/axe1.webp", "Image")
						axe2 = loadAsset("/externalassets/classic/level13/objects/upscaled/axe2.webp", "Image")
						shield1 = loadAsset("/externalassets/classic/level13/objects/upscaled/shield1.webp", "Image")
						shield2 = loadAsset("/externalassets/classic/level13/objects/upscaled/shield2.webp", "Image")
						
						snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
						snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
						snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
						snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
				
						vik1_00 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik1_00.webp", "Image")
						vik1_01 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik1_01.webp", "Image")
						vik1_02 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik1_02.webp", "Image")
						vik1_03 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik1_03.webp", "Image")
						vik1_04 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik1_04.webp", "Image")
						
						vik2_00 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik2_00.webp", "Image")
						vik2_01 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik2_01.webp", "Image")
						vik2_02 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik2_02.webp", "Image")
						vik2_03 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik2_03.webp", "Image")
						vik2_04 = loadAsset("/externalassets/classic/level17/objects/upscaled/vik2_04.webp", "Image")
						
						vik3_00= loadAsset("/externalassets//classic/level17/objects/upscaled/vik3_00.webp", "Image")
						vik3_01= loadAsset("/externalassets//classic/level17/objects/upscaled/vik3_01.webp", "Image")
						vik3_02= loadAsset("/externalassets//classic/level17/objects/upscaled/vik3_02.webp", "Image")
						vik3_03= loadAsset("/externalassets//classic/level17/objects/upscaled/vik3_03.webp", "Image")
						vik3_04= loadAsset("/externalassets//classic/level17/objects/upscaled/vik3_04.webp", "Image")
						
						vik4_00= loadAsset("/externalassets//classic/level17/objects/upscaled/vik7_00.webp", "Image")
						vik4_01= loadAsset("/externalassets//classic/level17/objects/upscaled/vik7_01.webp", "Image")
						vik4_02= loadAsset("/externalassets//classic/level17/objects/upscaled/vik7_02.webp", "Image")
						vik4_03= loadAsset("/externalassets//classic/level17/objects/upscaled/vik7_03.webp", "Image")
						vik4_04= loadAsset("/externalassets//classic/level17/objects/upscaled/vik7_04.webp", "Image")
		
						vik5_00= loadAsset("/externalassets/classic/level17/objects/upscaled/vik5_00.webp", "Image")
						vik5_01= loadAsset("/externalassets/classic/level17/objects/upscaled/vik5_01.webp", "Image")
						vik5_02= loadAsset("/externalassets/classic/level17/objects/upscaled/vik5_02.webp", "Image")
						vik5_03= loadAsset("/externalassets/classic/level17/objects/upscaled/vik5_03.webp", "Image")
						
						vik6_00= loadAsset("/externalassets/classic/level17/objects/upscaled/vik6_00.webp", "Image")
						vik6_01= loadAsset("/externalassets/classic/level17/objects/upscaled/vik6_01.webp", "Image")
						vik6_02= loadAsset("/externalassets/classic/level17/objects/upscaled/vik6_02.webp", "Image")
						vik6_03= loadAsset("/externalassets/classic/level17/objects/upscaled/vik6_03.webp", "Image")
						
						vik7_00= loadAsset("/externalassets/classic/level17/objects/upscaled/vik7_00.webp", "Image")
						vik7_01= loadAsset("/externalassets/classic/level17/objects/upscaled/vik7_01.webp", "Image")
						vik7_02= loadAsset("/externalassets/classic/level17/objects/upscaled/vik7_02.webp", "Image")
						vik7_03= loadAsset("/externalassets/classic/level17/objects/upscaled/vik7_03.webp", "Image")
						vik7_04= loadAsset("/externalassets/classic/level17/objects/upscaled/vik7_04.webp", "Image")
						
						
						viking1 = loadAsset("/externalassets/classic/level13/objects/upscaled/viking1.webp", "Image")
						viking2 = loadAsset("/externalassets/classic/level13/objects/upscaled/viking2.webp", "Image")
						viking3= loadAsset("/externalassets//classic/level13/objects/upscaled/viking3.webp", "Image")
						viking4= loadAsset("/externalassets/classic/level13/objects/upscaled/viking4.webp", "Image")
						viking5= loadAsset("/externalassets/classic/level13/objects/upscaled/viking5.webp", "Image")
						viking6= loadAsset("/externalassets/classic/level13/objects/upscaled/viking6.webp", "Image")
						skull= loadAsset("/externalassets/classic/level13/objects/upscaled/skull.webp", "Image")
						
						--mastil
						drakar00= loadAsset("/externalassets/classic/level17/objects/upscaled/drakar-hlava_00.webp", "Image")
						drakar01= loadAsset("/externalassets/classic/level17/objects/upscaled/drakar-hlava_01.webp", "Image")
						drakar02= loadAsset("/externalassets/classic/level17/objects/upscaled/drakar-hlava_02.webp", "Image")
						--dog
						pesos_00 = loadAsset("/externalassets/classic/level17/objects/upscaled/pesos_00.webp", "Image")
						pesos_01 = loadAsset("/externalassets/classic/level17/objects/upscaled/pesos_01.webp", "Image")
						pesos_02 = loadAsset("/externalassets/classic/level17/objects/upscaled/pesos_02.webp", "Image")
						--pipes
						pipe= loadAsset("/externalassets/classic/level17/objects/upscaled/pipe.webp", "Image")
						pipe2= loadAsset("/externalassets/classic/level17/objects/upscaled/pipe2.webp", "Image")
						
						korunka= loadAsset("/externalassets/classic/level17/objects/upscaled/korunka.webp", "Image")
						shield= loadAsset("/externalassets/classic/level17/objects/upscaled/shield.webp", "Image")
						
				end			
						skullpushed=false
		
						skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==14 then
		shader2=false
		eyeframe=0
			if skinupscaled==false then
				level14frgcl= loadAsset("/externalassets/classic/level14/level14frg.webp", "Image")
				level14bckcl= loadAsset("/externalassets/classic/level14/level14bck.webp", "Image")
			elseif skinupscaled==true then
				level14frgcl= loadAsset("/externalassets/classic/upscaled/level14/level14frg.webp", "Image")
				level14bckcl= loadAsset("/externalassets/classic/upscaled/level14/level14bck.webp", "Image")
			end
		
		--fish
		
			--ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		--objects
		if skinupscaled==false then
			chairleft = loadAsset("/externalassets/classic/level14/objects/chairleft.webp", "Image")
			chairright = loadAsset("/externalassets/classic/level14/objects/chairright.webp", "Image")
			chairup = loadAsset("/externalassets/classic/level14/objects/chairup.webp", "Image")
			chairdown = loadAsset("/externalassets/classic/level14/objects/chairdown.webp", "Image")
			oko_00 = loadAsset("/externalassets/classic/level14/objects/oko_00.webp", "Image")
			oko_01 = loadAsset("/externalassets/classic/level14/objects/oko_01.webp", "Image")
			oko_02 = loadAsset("/externalassets/classic/level14/objects/oko_02.webp", "Image")
			oko_03 = loadAsset("/externalassets/classic/level14/objects/oko_03.webp", "Image")
			oko_04 = loadAsset("/externalassets/classic/level14/objects/oko_04.webp", "Image")
			pipe1 = loadAsset("/externalassets/classic/level14/objects/pipe1.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level14/objects/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level14/objects/pipe3.webp", "Image")
		elseif skinupscaled==true then
			chairleft = loadAsset("/externalassets/classic/level14/objects/upscaled/chairleft.webp", "Image")
			chairright = loadAsset("/externalassets/classic/level14/objects/upscaled/chairright.webp", "Image")
			chairup = loadAsset("/externalassets/classic/level14/objects/upscaled/chairup.webp", "Image")
			chairdown = loadAsset("/externalassets/classic/level14/objects/upscaled/chairdown.webp", "Image")
			oko_00 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_00.webp", "Image")
			oko_01 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_01.webp", "Image")
			oko_02 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_02.webp", "Image")
			oko_03 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_03.webp", "Image")
			oko_04 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_04.webp", "Image")
			pipe1 = loadAsset("/externalassets/classic/level14/objects/upscaled/pipe1.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level14/objects/upscaled/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level14/objects/upscaled/pipe3.webp", "Image")
		end
		elseif nLevel==15 then
		shader2=false
		if skinupscaled==false then
			level15frgcl= loadAsset("/externalassets/classic/level15/level15frg.webp", "Image")
			level15bckcl= loadAsset("/externalassets/classic/level15/level15bck.webp", "Image")
		elseif skinupscaled==true then
			level15frgcl= loadAsset("/externalassets/classic/upscaled/level15/level15frg.webp", "Image")
			level15bckcl= loadAsset("/externalassets/classic/upscaled/level15/level15bck.webp", "Image")
		end
			
			
			--objects
		if skinupscaled==false then
			pipe= loadAsset("/externalassets/classic/level15/objects/pipe.webp", "Image")
			mikroskop_00= loadAsset("/externalassets/classic/level15/objects/mikroskop_00.webp", "Image")
			mikroskop_01= loadAsset("/externalassets/classic/level15/objects/mikroskop_01.webp", "Image")
			mikroskop_02= loadAsset("/externalassets/classic/level15/objects/mikroskop_02.webp", "Image")
			basephone= loadAsset("/externalassets/classic/level15/objects/basephone.webp", "Image") 
			basephone2= loadAsset("/externalassets/classic/level15/objects/basephone2.webp", "Image")
			phone= loadAsset("/externalassets/classic/level15/objects/phone.webp", "Image") 
			phone2= loadAsset("/externalassets/classic/level15/objects/phone2.webp", "Image")
			snek_00= loadAsset("/externalassets/classic/level15/objects/snek_00.webp", "Image")
			snek_01= loadAsset("/externalassets/classic/level15/objects/snek_01.webp", "Image")
			budik_00= loadAsset("/externalassets/classic/level15/objects/budik_00.webp", "Image")
			budik_01= loadAsset("/externalassets/classic/level15/objects/budik_01.webp", "Image")
			dalekohled_00= loadAsset("/externalassets/classic/level15/objects/dalekohled_00.webp", "Image")
			dalekohled_01= loadAsset("/externalassets/classic/level15/objects/dalekohled_01.webp", "Image")
		elseif skinupscaled==true then
			pipe= loadAsset("/externalassets/classic/level15/objects/upscaled/pipe.webp", "Image")
			mikroskop_00= loadAsset("/externalassets/classic/level15/objects/upscaled/mikroskop_00.webp", "Image")
			mikroskop_01= loadAsset("/externalassets/classic/level15/objects/upscaled/mikroskop_01.webp", "Image")
			mikroskop_02= loadAsset("/externalassets/classic/level15/objects/upscaled/mikroskop_02.webp", "Image")
			basephone= loadAsset("/externalassets/classic/level15/objects/upscaled/basephone.webp", "Image") 
			basephone2= loadAsset("/externalassets/classic/level15/objects/upscaled/basephone2.webp", "Image")
			phone= loadAsset("/externalassets/classic/level15/objects/upscaled/phone.webp", "Image") 
			phone2= loadAsset("/externalassets/classic/level15/objects/upscaled/phone2.webp", "Image")
			snek_00= loadAsset("/externalassets/classic/level15/objects/upscaled/snek_00.webp", "Image")
			snek_01= loadAsset("/externalassets/classic/level15/objects/upscaled/snek_01.webp", "Image")
			budik_00= loadAsset("/externalassets/classic/level15/objects/upscaled/budik_00.webp", "Image")
			budik_01= loadAsset("/externalassets/classic/level15/objects/upscaled/budik_01.webp", "Image")
			dalekohled_00= loadAsset("/externalassets/classic/level15/objects/upscaled/dalekohled_00.webp", "Image")
			dalekohled_01= loadAsset("/externalassets/classic/level15/objects/upscaled/dalekohled_01.webp", "Image")
		end
			
			
		--shark
		shark1= loadAsset("/externalassets/objects/level15/shark/1.webp", "Image")
		shark2= loadAsset("/externalassets/objects/level15/shark/2.webp", "Image")
		shark3= loadAsset("/externalassets/objects/level15/shark/3.webp", "Image")
		shark4= loadAsset("/externalassets/objects/level15/shark/4.webp", "Image")
		shark5= loadAsset("/externalassets/objects/level15/shark/5.webp", "Image")
		shark6= loadAsset("/externalassets/objects/level15/shark/6.webp", "Image")
		shark7= loadAsset("/externalassets/objects/level15/shark/7.webp", "Image")
		shark8= loadAsset("/externalassets/objects/level15/shark/8.webp", "Image")
		shark9= loadAsset("/externalassets/objects/level15/shark/9.webp", "Image")
		shark10= loadAsset("/externalassets/objects/level15/shark/10.webp", "Image")
		shark11= loadAsset("/externalassets/objects/level15/shark/11.webp", "Image")
		shark12= loadAsset("/externalassets/objects/level15/shark/12.webp", "Image")
		shark13= loadAsset("/externalassets/objects/level15/shark/13.webp", "Image")
		shark14= loadAsset("/externalassets/objects/level15/shark/14.webp", "Image")
		shark15= loadAsset("/externalassets/objects/level15/shark/15.webp", "Image")
		shark16= loadAsset("/externalassets/objects/level15/shark/16.webp", "Image")
		shark17= loadAsset("/externalassets/objects/level15/shark/17.webp", "Image")
		shark18= loadAsset("/externalassets/objects/level15/shark/18.webp", "Image")
		shark19= loadAsset("/externalassets/objects/level15/shark/19.webp", "Image")
		shark20= loadAsset("/externalassets/objects/level15/shark/20.webp", "Image")
		shark21= loadAsset("/externalassets/objects/level15/shark/21.webp", "Image")
		shark22= loadAsset("/externalassets/objects/level15/shark/22.webp", "Image")
		shark23= loadAsset("/externalassets/objects/level15/shark/23.webp", "Image")
		shark24= loadAsset("/externalassets/objects/level15/shark/24.webp", "Image")
		shark25= loadAsset("/externalassets/objects/level15/shark/25.webp", "Image")
		shark26= loadAsset("/externalassets/objects/level15/shark/26.webp", "Image")
		shark27= loadAsset("/externalassets/objects/level15/shark/27.webp", "Image")
		shark28= loadAsset("/externalassets/objects/level15/shark/28.webp", "Image")
		shark29= loadAsset("/externalassets/objects/level15/shark/29.webp", "Image")
		shark30= loadAsset("/externalassets/objects/level15/shark/30.webp", "Image")
		
		elseif nLevel==16 then
		
		
			if skinupscaled==false then
				level16bckcl= loadAsset("/externalassets/classic/level16/level16bck.webp", "Image")
				level16frgcl= loadAsset("/externalassets/classic/level16/level16frg.webp", "Image")
			elseif skinupscaled==true then
				level16bckcl= loadAsset("/externalassets/classic/upscaled/level16/level16bck.webp", "Image")
				level16frgcl= loadAsset("/externalassets/classic/upscaled/level16/level16frg.webp", "Image")
			end
		
		--objects
			if skinupscaled==false then
				 bullet = loadAsset("/externalassets/classic/level16/objects/bullet.webp", "Image")
				 bulletleft = loadAsset("/externalassets/classic/level16/objects/bulletleft.webp", "Image")
				 bulletright = loadAsset("/externalassets/classic/level16/objects/bulletright.webp", "Image")
				 pipe = loadAsset("/externalassets/classic/level16/objects/pipe.webp", "Image")
				 pipe2 = loadAsset("/externalassets/classic/level16/objects/pipe2.webp", "Image")
				 pipe3 = loadAsset("/externalassets/classic/level16/objects/pipe3.webp", "Image")
				 pipe4 = loadAsset("/externalassets/classic/level16/objects/pipe4.webp", "Image")
				 pipe5 = loadAsset("/externalassets/classic/level16/objects/pipe5.webp", "Image")
				 robothand = loadAsset("/externalassets/classic/level16/objects/robothand.webp", "Image")
				 snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
				snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
				snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
				snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
				 stairs = loadAsset("/externalassets/classic/level16/objects/stairs.webp", "Image")
				 
		
		elseif skinupscaled==true then
				 bullet = loadAsset("/externalassets/classic/level16/objects/upscaled/bullet.webp", "Image")
				 bulletleft = loadAsset("/externalassets/classic/level16/objects/upscaled/bulletleft.webp", "Image")
				 bulletright = loadAsset("/externalassets/classic/level16/objects/upscaled/bulletright.webp", "Image")
				 pipe = loadAsset("/externalassets/classic/level16/objects/upscaled/pipe.webp", "Image")
				 pipe2 = loadAsset("/externalassets/classic/level16/objects/upscaled/pipe2.webp", "Image")
				 pipe3 = loadAsset("/externalassets/classic/level16/objects/upscaled/pipe3.webp", "Image")
				 pipe4 = loadAsset("/externalassets/classic/level16/objects/upscaled/pipe4.webp", "Image")
				 pipe5 = loadAsset("/externalassets/classic/level16/objects/upscaled/pipe5.webp", "Image")
				 robothand = loadAsset("/externalassets/classic/level16/objects/upscaled/robothand.webp", "Image")
				 snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
				snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
				snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
				snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
				 stairs = loadAsset("/externalassets/classic/level16/objects/upscaled/stairs.webp", "Image")

		end
		
		
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		
	
		elseif nLevel==19 then
		shader2=false
			
			if skinupscaled==false then
				level19frgcl= loadAsset("/externalassets/classic/level19/level19frg.webp", "Image")
				level19bckcl= loadAsset("/externalassets/classic/level19/level19bck.webp", "Image")
			elseif skinupscaled==true then
				level19frgcl= loadAsset("/externalassets/classic/upscaled/level19/level19frg.webp", "Image")
				level19bckcl= loadAsset("/externalassets/classic/upscaled/level19/level19bck.webp", "Image")
			end
		
		--objects
		if skinupscaled==false then
			poseidon= loadAsset("/externalassets/classic/level19/objects/poseidon.webp", "Image")
			neptun= loadAsset("/externalassets/classic/level19/objects/neptun.webp", "Image")
			bota = loadAsset("/externalassets/classic/level19/objects/bota.webp", "Image")
			domino = loadAsset("/externalassets/classic/level19/objects/domino.webp", "Image")
			hul = loadAsset("/externalassets/classic/level19/objects/hul.webp", "Image")
			kuzelka = loadAsset("/externalassets/classic/level19/objects/kuzelka.webp", "Image")
			palka = loadAsset("/externalassets/classic/level19/objects/palka.webp", "Image")
			ping = loadAsset("/externalassets/classic/level19/objects/ping.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level19/objects/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level19/objects/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level19/objects/pipe3.webp", "Image")
			tenisak = loadAsset("/externalassets/classic/level19/objects/tenisak.webp", "Image")
			
			poseidon00= loadAsset("/externalassets/classic/level19/objects/poseidon_00.webp", "Image")
			poseidon01= loadAsset("/externalassets/classic/level19/objects/poseidon_01.webp", "Image")
			poseidon02= loadAsset("/externalassets/classic/level19/objects/poseidon_02.webp", "Image")
			poseidon03= loadAsset("/externalassets/classic/level19/objects/poseidon_03.webp", "Image")
			poseidon04= loadAsset("/externalassets/classic/level19/objects/poseidon_04.webp", "Image")
			poseidon05= loadAsset("/externalassets/classic/level19/objects/poseidon_05.webp", "Image")
			poseidon06= loadAsset("/externalassets/classic/level19/objects/poseidon_06.webp", "Image")
			poseidon07= loadAsset("/externalassets/classic/level19/objects/poseidon_07.webp", "Image")
			poseidon08= loadAsset("/externalassets/classic/level19/objects/poseidon_08.webp", "Image")
			poseidon09= loadAsset("/externalassets/classic/level19/objects/poseidon_09.webp", "Image")
			poseidon10= loadAsset("/externalassets/classic/level19/objects/poseidon_10.webp", "Image")
			poseidon11= loadAsset("/externalassets/classic/level19/objects/poseidon_11.webp", "Image")
			poseidon12= loadAsset("/externalassets/classic/level19/objects/poseidon_12.webp", "Image")
			poseidon13= loadAsset("/externalassets/classic/level19/objects/poseidon_13.webp", "Image")
			poseidon14= loadAsset("/externalassets/classic/level19/objects/poseidon_14.webp", "Image")
			poseidon15= loadAsset("/externalassets/classic/level19/objects/poseidon_15.webp", "Image")
			poseidon16= loadAsset("/externalassets/classic/level19/objects/poseidon_16.webp", "Image")
			poseidon17= loadAsset("/externalassets/classic/level19/objects/poseidon_17.webp", "Image")
			poseidon18= loadAsset("/externalassets/classic/level19/objects/poseidon_18.webp", "Image")
			poseidon19= loadAsset("/externalassets/classic/level19/objects/poseidon_19.webp", "Image")
			poseidon20= loadAsset("/externalassets/classic/level19/objects/poseidon_20.webp", "Image")
			poseidon21= loadAsset("/externalassets/classic/level19/objects/poseidon_21.webp", "Image")
			poseidon22= loadAsset("/externalassets/classic/level19/objects/poseidon_22.webp", "Image")
			poseidon23= loadAsset("/externalassets/classic/level19/objects/poseidon_23.webp", "Image")
			poseidon24= loadAsset("/externalassets/classic/level19/objects/poseidon_24.webp", "Image")
			poseidon25= loadAsset("/externalassets/classic/level19/objects/poseidon_25.webp", "Image")
			poseidon26= loadAsset("/externalassets/classic/level19/objects/poseidon_26.webp", "Image")
			poseidon27= loadAsset("/externalassets/classic/level19/objects/poseidon_27.webp", "Image")
			poseidon28= loadAsset("/externalassets/classic/level19/objects/poseidon_28.webp", "Image")
			poseidon29= loadAsset("/externalassets/classic/level19/objects/poseidon_29.webp", "Image")
			poseidon30= loadAsset("/externalassets/classic/level19/objects/poseidon_30.webp", "Image")
			poseidon31= loadAsset("/externalassets/classic/level19/objects/poseidon_31.webp", "Image")
			poseidon32= loadAsset("/externalassets/classic/level19/objects/poseidon_32.webp", "Image")
			poseidon33= loadAsset("/externalassets/classic/level19/objects/poseidon_33.webp", "Image")
			poseidon34= loadAsset("/externalassets/classic/level19/objects/poseidon_34.webp", "Image")
			poseidon35= loadAsset("/externalassets/classic/level19/objects/poseidon_35.webp", "Image")
			poseidon36= loadAsset("/externalassets/classic/level19/objects/poseidon_36.webp", "Image")
			poseidon37= loadAsset("/externalassets/classic/level19/objects/poseidon_37.webp", "Image")
			poseidon38= loadAsset("/externalassets/classic/level19/objects/poseidon_38.webp", "Image")
			poseidon39= loadAsset("/externalassets/classic/level19/objects/poseidon_39.webp", "Image")
			poseidon40= loadAsset("/externalassets/classic/level19/objects/poseidon_40.webp", "Image")
			poseidon41= loadAsset("/externalassets/classic/level19/objects/poseidon_41.webp", "Image")
			poseidon42= loadAsset("/externalassets/classic/level19/objects/poseidon_42.webp", "Image")
			poseidon43= loadAsset("/externalassets/classic/level19/objects/poseidon_43.webp", "Image")
			poseidon44= loadAsset("/externalassets/classic/level19/objects/poseidon_44.webp", "Image")
			poseidon45= loadAsset("/externalassets/classic/level19/objects/poseidon_45.webp", "Image")
			poseidon46= loadAsset("/externalassets/classic/level19/objects/poseidon_46.webp", "Image")
			poseidon47= loadAsset("/externalassets/classic/level19/objects/poseidon_47.webp", "Image")
			poseidon48= loadAsset("/externalassets/classic/level19/objects/poseidon_48.webp", "Image")
			
			neptun00= loadAsset("/externalassets/classic/level19/objects/neptun_00.webp", "Image")
			neptun01= loadAsset("/externalassets/classic/level19/objects/neptun_01.webp", "Image")
			neptun02= loadAsset("/externalassets/classic/level19/objects/neptun_02.webp", "Image")
			neptun03= loadAsset("/externalassets/classic/level19/objects/neptun_03.webp", "Image")
			neptun04= loadAsset("/externalassets/classic/level19/objects/neptun_04.webp", "Image")
			neptun05= loadAsset("/externalassets/classic/level19/objects/neptun_05.webp", "Image")
			neptun06= loadAsset("/externalassets/classic/level19/objects/neptun_06.webp", "Image")
			neptun07= loadAsset("/externalassets/classic/level19/objects/neptun_07.webp", "Image")
			neptun08= loadAsset("/externalassets/classic/level19/objects/neptun_08.webp", "Image")
			neptun09= loadAsset("/externalassets/classic/level19/objects/neptun_09.webp", "Image")
			neptun10= loadAsset("/externalassets/classic/level19/objects/neptun_10.webp", "Image")
			neptun11= loadAsset("/externalassets/classic/level19/objects/neptun_11.webp", "Image")
			neptun12= loadAsset("/externalassets/classic/level19/objects/neptun_12.webp", "Image")
			neptun13= loadAsset("/externalassets/classic/level19/objects/neptun_13.webp", "Image")
			neptun14= loadAsset("/externalassets/classic/level19/objects/neptun_14.webp", "Image")
			neptun15= loadAsset("/externalassets/classic/level19/objects/neptun_15.webp", "Image")
			neptun16= loadAsset("/externalassets/classic/level19/objects/neptun_16.webp", "Image")
			neptun17= loadAsset("/externalassets/classic/level19/objects/neptun_17.webp", "Image")
			neptun18= loadAsset("/externalassets/classic/level19/objects/neptun_18.webp", "Image")
			neptun19= loadAsset("/externalassets/classic/level19/objects/neptun_19.webp", "Image")
			neptun20= loadAsset("/externalassets/classic/level19/objects/neptun_20.webp", "Image")
			neptun21= loadAsset("/externalassets/classic/level19/objects/neptun_21.webp", "Image")
			neptun22= loadAsset("/externalassets/classic/level19/objects/neptun_22.webp", "Image")
			neptun23= loadAsset("/externalassets/classic/level19/objects/neptun_23.webp", "Image")
			neptun24= loadAsset("/externalassets/classic/level19/objects/neptun_24.webp", "Image")
			neptun25= loadAsset("/externalassets/classic/level19/objects/neptun_25.webp", "Image")
			neptun26= loadAsset("/externalassets/classic/level19/objects/neptun_26.webp", "Image")
			neptun27= loadAsset("/externalassets/classic/level19/objects/neptun_27.webp", "Image")
			neptun28= loadAsset("/externalassets/classic/level19/objects/neptun_28.webp", "Image")
			neptun29= loadAsset("/externalassets/classic/level19/objects/neptun_29.webp", "Image")
			neptun30= loadAsset("/externalassets/classic/level19/objects/neptun_30.webp", "Image")
			neptun31= loadAsset("/externalassets/classic/level19/objects/neptun_31.webp", "Image")
			neptun32= loadAsset("/externalassets/classic/level19/objects/neptun_32.webp", "Image")
			neptun33= loadAsset("/externalassets/classic/level19/objects/neptun_33.webp", "Image")
			neptun34= loadAsset("/externalassets/classic/level19/objects/neptun_34.webp", "Image")
			neptun35= loadAsset("/externalassets/classic/level19/objects/neptun_35.webp", "Image")
			neptun36= loadAsset("/externalassets/classic/level19/objects/neptun_36.webp", "Image")
			neptun37= loadAsset("/externalassets/classic/level19/objects/neptun_37.webp", "Image")
			neptun38= loadAsset("/externalassets/classic/level19/objects/neptun_38.webp", "Image")
			neptun39= loadAsset("/externalassets/classic/level19/objects/neptun_39.webp", "Image")
			neptun40= loadAsset("/externalassets/classic/level19/objects/neptun_40.webp", "Image")
			neptun41= loadAsset("/externalassets/classic/level19/objects/neptun_41.webp", "Image")
			neptun42= loadAsset("/externalassets/classic/level19/objects/neptun_42.webp", "Image")
			neptun43= loadAsset("/externalassets/classic/level19/objects/neptun_43.webp", "Image")
			neptun44= loadAsset("/externalassets/classic/level19/objects/neptun_44.webp", "Image")
			neptun45= loadAsset("/externalassets/classic/level19/objects/neptun_45.webp", "Image")
			
		elseif skinupscaled==true then
			poseidon00= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_00.webp", "Image")
			poseidon01= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_01.webp", "Image")
			poseidon02= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_02.webp", "Image")
			poseidon03= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_03.webp", "Image")
			poseidon04= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_04.webp", "Image")
			poseidon05= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_05.webp", "Image")
			poseidon06= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_06.webp", "Image")
			poseidon07= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_07.webp", "Image")
			poseidon08= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_08.webp", "Image")
			poseidon09= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_09.webp", "Image")
			poseidon10= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_10.webp", "Image")
			poseidon11= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_11.webp", "Image")
			poseidon12= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_12.webp", "Image")
			poseidon13= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_13.webp", "Image")
			poseidon14= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_14.webp", "Image")
			poseidon15= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_15.webp", "Image")
			poseidon16= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_16.webp", "Image")
			poseidon17= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_17.webp", "Image")
			poseidon18= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_18.webp", "Image")
			poseidon19= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_19.webp", "Image")
			poseidon20= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_20.webp", "Image")
			poseidon21= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_21.webp", "Image")
			poseidon22= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_22.webp", "Image")
			poseidon23= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_23.webp", "Image")
			poseidon24= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_24.webp", "Image")
			poseidon25= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_25.webp", "Image")
			poseidon26= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_26.webp", "Image")
			poseidon27= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_27.webp", "Image")
			poseidon28= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_28.webp", "Image")
			poseidon29= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_29.webp", "Image")
			poseidon30= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_30.webp", "Image")
			poseidon31= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_31.webp", "Image")
			poseidon32= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_32.webp", "Image")
			poseidon33= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_33.webp", "Image")
			poseidon34= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_34.webp", "Image")
			poseidon35= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_35.webp", "Image")
			poseidon36= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_36.webp", "Image")
			poseidon37= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_37.webp", "Image")
			poseidon38= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_38.webp", "Image")
			poseidon39= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_39.webp", "Image")
			poseidon40= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_40.webp", "Image")
			poseidon41= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_41.webp", "Image")
			poseidon42= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_42.webp", "Image")
			poseidon43= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_43.webp", "Image")
			poseidon44= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_44.webp", "Image")
			poseidon45= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_45.webp", "Image")
			poseidon46= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_46.webp", "Image")
			poseidon47= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_47.webp", "Image")
			poseidon48= loadAsset("/externalassets/classic/level19/objects/upscaled/poseidon_48.webp", "Image")
			
			neptun00= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_00.webp", "Image")
			neptun01= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_01.webp", "Image")
			neptun02= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_02.webp", "Image")
			neptun03= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_03.webp", "Image")
			neptun04= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_04.webp", "Image")
			neptun05= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_05.webp", "Image")
			neptun06= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_06.webp", "Image")
			neptun07= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_07.webp", "Image")
			neptun08= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_08.webp", "Image")
			neptun09= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_09.webp", "Image")
			neptun10= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_10.webp", "Image")
			neptun11= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_11.webp", "Image")
			neptun12= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_12.webp", "Image")
			neptun13= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_13.webp", "Image")
			neptun14= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_14.webp", "Image")
			neptun15= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_15.webp", "Image")
			neptun16= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_16.webp", "Image")
			neptun17= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_17.webp", "Image")
			neptun18= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_18.webp", "Image")
			neptun19= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_19.webp", "Image")
			neptun20= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_20.webp", "Image")
			neptun21= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_21.webp", "Image")
			neptun22= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_22.webp", "Image")
			neptun23= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_23.webp", "Image")
			neptun24= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_24.webp", "Image")
			neptun25= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_25.webp", "Image")
			neptun26= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_26.webp", "Image")
			neptun27= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_27.webp", "Image")
			neptun28= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_28.webp", "Image")
			neptun29= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_29.webp", "Image")
			neptun30= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_30.webp", "Image")
			neptun31= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_31.webp", "Image")
			neptun32= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_32.webp", "Image")
			neptun33= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_33.webp", "Image")
			neptun34= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_34.webp", "Image")
			neptun35= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_35.webp", "Image")
			neptun36= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_36.webp", "Image")
			neptun37= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_37.webp", "Image")
			neptun38= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_38.webp", "Image")
			neptun39= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_39.webp", "Image")
			neptun40= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_40.webp", "Image")
			neptun41= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_41.webp", "Image")
			neptun42= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_42.webp", "Image")
			neptun43= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_43.webp", "Image")
			neptun44= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_44.webp", "Image")
			neptun45= loadAsset("/externalassets/classic/level19/objects/upscaled/neptun_45.webp", "Image")
			
			
			
			bota = loadAsset("/externalassets/classic/level19/objects/upscaled/bota.webp", "Image")
			domino = loadAsset("/externalassets/classic/level19/objects/upscaled/domino.webp", "Image")
			hul = loadAsset("/externalassets/classic/level19/objects/upscaled/hul.webp", "Image")
			kuzelka = loadAsset("/externalassets/classic/level19/objects/upscaled/kuzelka.webp", "Image")
			palka = loadAsset("/externalassets/classic/level19/objects/upscaled/palka.webp", "Image")
			ping = loadAsset("/externalassets/classic/level19/objects/upscaled/ping.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level19/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level19/objects/upscaled/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level19/objects/upscaled/pipe3.webp", "Image")
			tenisak = loadAsset("/externalassets/classic/level19/objects/upscaled/tenisak.webp", "Image")
		end	
		
		--fish
		
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==20 then
		shader2=false
		timer=0

		
			if skinupscaled==false then
				level20frgcl= loadAsset("/externalassets/classic/level20/level20frg.webp", "Image")
				level20bckcl= loadAsset("/externalassets/classic/level20/level20bck.webp", "Image")
			elseif skinupscaled==true then
				level20frgcl= loadAsset("/externalassets/classic/upscaled/level20/level20frg.webp", "Image")
				level20bckcl= loadAsset("/externalassets/classic/upscaled/level20/level20bck.webp", "Image")
			end
			
			--fishs
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
				--objects
		if skinupscaled==false then
			skull= loadAsset("/externalassets/classic/level20/objects/skull.webp", "Image")
			amphora= loadAsset("/externalassets/classic/level20/objects/amphora.webp", "Image")
			shell= loadAsset("/externalassets/classic/level20/objects/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/classic/level20/objects/sculpture.webp", "Image")
			sculpture1= loadAsset("/externalassets/classic/level20/objects/sculpture/1.webp", "Image")
			sculpture2= loadAsset("/externalassets/classic/level20/objects/sculpture/2.webp", "Image")
			cross= loadAsset("/externalassets/classic/level20/objects/cross.webp", "Image")
			cross1= loadAsset("/externalassets/classic/level20/objects/cross/1.webp", "Image")
			cross2= loadAsset("/externalassets/classic/level20/objects/cross/2.webp", "Image")
			
			elevator1= loadAsset("/externalassets/classic/level20/objects/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/classic/level20/objects/elevator2.webp", "Image")
			
		elseif skinupscaled==true then
			skull= loadAsset("/externalassets/classic/level20/objects/upscaled/skull.webp", "Image")
			amphora= loadAsset("/externalassets/classic/level20/objects/upscaled/amphora.webp", "Image")
			shell= loadAsset("/externalassets/classic/level20/objects/upscaled/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture.webp", "Image")
			sculpture1= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/1.webp", "Image")
			sculpture2= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/2.webp", "Image")
			elevator1= loadAsset("/externalassets/classic/level20/objects/upscaled/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/classic/level20/objects/upscaled/elevator2.webp", "Image")
			cross= loadAsset("/externalassets/classic/level20/objects/upscaled/cross.webp", "Image")
			cross1= loadAsset("/externalassets/classic/level20/objects/upscaled/cross/1.webp", "Image")
			cross2= loadAsset("/externalassets/classic/level20/objects/upscaled/cross/2.webp", "Image")
		end	
				skullpushed=false
		
				skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==21 then
		shader2=false
		timer=0

			
			--background
			if skinupscaled==false then
				level21frgcl= loadAsset("/externalassets/classic/level21/level21frg.webp", "Image")
				level21bckcl= loadAsset("/externalassets/classic/level21/level21bck.webp", "Image")
			elseif skinupscaled==true then
				level21frgcl= loadAsset("/externalassets/classic/upscaled/level21/level21frg.webp", "Image")
				level21bckcl= loadAsset("/externalassets/classic/upscaled/level21/level21bck.webp", "Image")
			end
			
			--objects
		if skinupscaled==false then	
				krab_00  = loadAsset("externalassets/classic/level29/objects/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/krab_09.webp", "Image")
				
				
			snail= loadAsset("/externalassets/classic/level21/objects/snail.webp", "Image")
			crab= loadAsset("/externalassets/classic/level21/objects/crab.webp", "Image")
			pedestal= loadAsset("/externalassets/classic/level21/objects/pedestal.webp", "Image")
			pipelong= loadAsset("/externalassets/classic/level21/objects/pipelong.webp", "Image")
			pipeshort= loadAsset("/externalassets/classic/level21/objects/pipeshort.webp", "Image")
			arm= loadAsset("/externalassets/classic/level21/objects/arm.webp", "Image")
			platform= loadAsset("/externalassets/classic/level21/objects/platform.webp", "Image")
			statuecl= loadAsset("/externalassets/classic/level21/objects/statue.webp", "Image")
			statue1 = loadAsset("/externalassets/classic/level21/objects/statue/1.webp", "Image")
			statue2 = loadAsset("/externalassets/classic/level21/objects/statue/2.webp", "Image")
			statue3 = loadAsset("/externalassets/classic/level21/objects/statue/3.webp", "Image")
			statue4 = loadAsset("/externalassets/classic/level21/objects/statue/4.webp", "Image")
			statue5 = loadAsset("/externalassets/classic/level21/objects/statue/5.webp", "Image")
			statue6 = loadAsset("/externalassets/classic/level21/objects/statue/6.webp", "Image")
			statue7 = loadAsset("/externalassets/classic/level21/objects/statue/7.webp", "Image")
			statue8 = loadAsset("/externalassets/classic/level21/objects/statue/8.webp", "Image")
			statue9 = loadAsset("/externalassets/classic/level21/objects/statue/9.webp", "Image")
			statue10 = loadAsset("/externalassets/classic/level21/objects/statue/10.webp", "Image")
			statue11 = loadAsset("/externalassets/classic/level21/objects/statue/11.webp", "Image")
			statue12 = loadAsset("/externalassets/classic/level21/objects/statue/12.webp", "Image")
			statue13 = loadAsset("/externalassets/classic/level21/objects/statue/13.webp", "Image")
			statue14 = loadAsset("/externalassets/classic/level21/objects/statue/14.webp", "Image")
			statue15 = loadAsset("/externalassets/classic/level21/objects/statue/15.webp", "Image")
			statue16 = loadAsset("/externalassets/classic/level21/objects/statue/16.webp", "Image")
			statue17 = loadAsset("/externalassets/classic/level21/objects/statue/17.webp", "Image")
			statue18 = loadAsset("/externalassets/classic/level21/objects/statue/18.webp", "Image")
			statue19 = loadAsset("/externalassets/classic/level21/objects/statue/19.webp", "Image")
		elseif skinupscaled==true then
			snail= loadAsset("/externalassets/classic/level21/objects/upscaled/snail.webp", "Image")
			--crab= loadAsset("/externalassets/classic/level21/objects/upscaled/crab.webp", "Image")
			
				krab_00  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_09.webp", "Image")
				snail = loadAsset("externalassets/classic/level29/objects/upscaled/snail.webp", "Image")
			
			pedestal= loadAsset("/externalassets/classic/level21/objects/upscaled/pedestal.webp", "Image")
			pipelong= loadAsset("/externalassets/classic/level21/objects/upscaled/pipelong.webp", "Image")
			pipeshort= loadAsset("/externalassets/classic/level21/objects/upscaled/pipeshort.webp", "Image")
			arm= loadAsset("/externalassets/classic/level21/objects/upscaled/arm.webp", "Image")
			platform= loadAsset("/externalassets/classic/level21/objects/upscaled/platform.webp", "Image")
			statuecl= loadAsset("/externalassets/classic/level21/objects/upscaled/statue.webp", "Image")
			statue1 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/1.webp", "Image")
			statue2 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/2.webp", "Image")
			statue3 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/3.webp", "Image")
			statue4 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/4.webp", "Image")
			statue5 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/5.webp", "Image")
			statue6 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/6.webp", "Image")
			statue7 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/7.webp", "Image")
			statue8 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/8.webp", "Image")
			statue9 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/9.webp", "Image")
			statue10 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/10.webp", "Image")
			statue11 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/11.webp", "Image")
			statue12 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/12.webp", "Image")
			statue13 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/13.webp", "Image")
			statue14 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/14.webp", "Image")
			statue15 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/15.webp", "Image")
			statue16 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/16.webp", "Image")
			statue17 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/17.webp", "Image")
			statue18 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/18.webp", "Image")
			statue19 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/19.webp", "Image")
		end	
			--background fish
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==22 then
		shader2=false
			if skinupscaled==false then
				level22frgcl= loadAsset("/externalassets/classic/level22/level22frg.webp", "Image")
				level22bckcl= loadAsset("/externalassets/classic/level22/level22bck.webp", "Image")
			elseif skinupscaled==true then
				level22frgcl= loadAsset("/externalassets/classic/upscaled/level22/level22frg.webp", "Image")
				level22bckcl= loadAsset("/externalassets/classic/upscaled/level22/level22bck.webp", "Image")
			end
				
			
			--objects
				if skinupscaled==false then
					pipe1= loadAsset("/externalassets/classic/level22/objects/pipe1.webp", "Image")
					pipe2= loadAsset("/externalassets/classic/level22/objects/pipe2.webp", "Image")
					pipe3= loadAsset("/externalassets/classic/level22/objects/pipe3.webp", "Image")
					pipe4= loadAsset("/externalassets/classic/level22/objects/pipe4.webp", "Image")
					pipe5= loadAsset("/externalassets/classic/level22/objects/pipe5.webp", "Image")
					pipe6= loadAsset("/externalassets/classic/level22/objects/pipe6.webp", "Image")
					pipe7= loadAsset("/externalassets/classic/level22/objects/pipe7.webp", "Image")
					pipe8= loadAsset("/externalassets/classic/level22/objects/pipe8.webp", "Image")
					pipe9= loadAsset("/externalassets/classic/level22/objects/pipe9.webp", "Image")
					pipe10= loadAsset("/externalassets/classic/level22/objects/pipe10.webp", "Image")
					platform= loadAsset("/externalassets/classic/level22/objects/platform.webp", "Image")
					platform2= loadAsset("/externalassets/classic/level22/objects/platform2.webp", "Image")
					platform3= loadAsset("/externalassets/classic/level22/objects/platform3.webp", "Image")
					stairs= loadAsset("/externalassets/classic/level22/objects/stairs.webp", "Image")
					sculpture= loadAsset("/externalassets/classic/level20/objects/sculpture.webp", "Image")
					sculpture1= loadAsset("/externalassets/classic/level20/objects/sculpture/1.webp", "Image")
					sculpture2= loadAsset("/externalassets/classic/level20/objects/sculpture/2.webp", "Image")
				
			elseif skinupscaled==true then
					pipe1= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe1.webp", "Image")
					pipe2= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe2.webp", "Image")
					pipe3= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe3.webp", "Image")
					pipe4= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe4.webp", "Image")
					pipe5= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe5.webp", "Image")
					pipe6= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe6.webp", "Image")
					pipe7= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe7.webp", "Image")
					pipe8= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe8.webp", "Image")
					pipe9= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe9.webp", "Image")
					pipe10= loadAsset("/externalassets/classic/level22/objects/upscaled/pipe10.webp", "Image")
					platform= loadAsset("/externalassets/classic/level22/objects/upscaled/platform.webp", "Image")
					platform2= loadAsset("/externalassets/classic/level22/objects/upscaled/platform2.webp", "Image")
					platform3= loadAsset("/externalassets/classic/level22/objects/upscaled/platform3.webp", "Image")
					stairs= loadAsset("/externalassets/classic/level22/objects/upscaled/stairs.webp", "Image")
					sculpture= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture.webp", "Image")
					sculpture1= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/1.webp", "Image")
					sculpture2= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/2.webp", "Image")
				
			end
			
			
			
		elseif nLevel==23 then
		shader2=false
			if skinupscaled==false then
				level23frgcl= loadAsset("/externalassets/classic/level23/level23frg.webp", "Image")
				level23bckcl= loadAsset("/externalassets/classic/level23/level23bck.webp", "Image")
			elseif skinupscaled==true then
				level23frgcl= loadAsset("/externalassets/classic/upscaled/level23/level23frg.webp", "Image")
				level23bckcl= loadAsset("/externalassets/classic/upscaled/level23/level23bck.webp", "Image")
			end
		
			zeusfalled=false
			--objects
			if skinupscaled==false then
				column1 = loadAsset("/externalassets/classic/level23/objects/column1.webp", "Image")
				column2 = loadAsset("/externalassets/classic/level23/objects/column2.webp", "Image")
				column3 = loadAsset("/externalassets/classic/level23/objects/column3.webp", "Image")
				zeus = loadAsset("/externalassets/classic/level23/objects/zeus.webp", "Image")
				temple1 = loadAsset("/externalassets/classic/level23/objects/temple1.webp", "Image")
				temple2 = loadAsset("/externalassets/classic/level23/objects/temple2.webp", "Image")
				stairs = loadAsset("/externalassets/classic/level23/objects/stairs.webp", "Image")
			elseif skinupscaled==true then
				column1 = loadAsset("/externalassets/classic/level23/objects/upscaled/column1.webp", "Image")
				column2 = loadAsset("/externalassets/classic/level23/objects/upscaled/column2.webp", "Image")
				column3 = loadAsset("/externalassets/classic/level23/objects/upscaled/column3.webp", "Image")
				zeus = loadAsset("/externalassets/classic/level23/objects/upscaled/zeus.webp", "Image")
				temple1 = loadAsset("/externalassets/classic/level23/objects/upscaled/temple1.webp", "Image")
				temple2 = loadAsset("/externalassets/classic/level23/objects/upscaled/temple2.webp", "Image")
				stairs = loadAsset("/externalassets/classic/level23/objects/upscaled/stairs.webp", "Image")
			end
		
		elseif nLevel==24 then
		shader2=false
			if skinupscaled==false then
				level24frgcl= loadAsset("/externalassets/classic/level24/level24frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/level24/level24bck.webp", "Image")
			elseif skinupscaled==true then
				level24frgcl= loadAsset("/externalassets/classic/upscaled/level24/level24frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/upscaled/level24/level24bck.webp", "Image")
			end
		
		--objects
		if skinupscaled==false then
			statue = loadAsset("/externalassets/classic/level24/objects/statue.webp", "Image")
			col = loadAsset("/externalassets/classic/level24/objects/col.webp", "Image")
			col2 = loadAsset("/externalassets/classic/level24/objects/col2.webp", "Image")
			col3 = loadAsset("/externalassets/classic/level24/objects/col3.webp", "Image")
			col4 = loadAsset("/externalassets/classic/level24/objects/col4.webp", "Image")
			col5 = loadAsset("/externalassets/classic/level24/objects/col5.webp", "Image")
			columns = loadAsset("/externalassets/classic/level24/objects/columns.webp", "Image")
			snail = loadAsset("/externalassets/classic/level24/objects/snail.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level24/objects/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level24/objects/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level24/objects/pipe3.webp", "Image")
			pipe4 = loadAsset("/externalassets/classic/level24/objects/pipe4.webp", "Image")
			platform = loadAsset("/externalassets/classic/level24/objects/platform.webp", "Image")
			platform2 = loadAsset("/externalassets/classic/level24/objects/platform2.webp", "Image")
			platform3 = loadAsset("/externalassets/classic/level24/objects/platform3.webp", "Image")
			platform4 = loadAsset("/externalassets/classic/level24/objects/platform4.webp", "Image")
			pedestal = loadAsset("/externalassets/classic/level24/objects/pedestal.webp", "Image")
			octopus = loadAsset("/externalassets/classic/level24/objects/octopus.webp", "Image")
			
		elseif skinupscaled==true then
			statue = loadAsset("/externalassets/classic/level24/objects/upscaled/statue.webp", "Image")
			col = loadAsset("/externalassets/classic/level24/objects/upscaled/col.webp", "Image")
			col2 = loadAsset("/externalassets/classic/level24/objects/upscaled/col2.webp", "Image")
			col3 = loadAsset("/externalassets/classic/level24/objects/upscaled/col3.webp", "Image")
			col4 = loadAsset("/externalassets/classic/level24/objects/upscaled/col4.webp", "Image")
			col5 = loadAsset("/externalassets/classic/level24/objects/upscaled/col5.webp", "Image")
			columns = loadAsset("/externalassets/classic/level24/objects/upscaled/columns.webp", "Image")
			snail = loadAsset("/externalassets/classic/level24/objects/upscaled/snail.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level24/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level24/objects/upscaled/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/classic/level24/objects/upscaled/pipe3.webp", "Image")
			pipe4 = loadAsset("/externalassets/classic/level24/objects/upscaled/pipe4.webp", "Image")
			platform = loadAsset("/externalassets/classic/level24/objects/upscaled/platform.webp", "Image")
			platform2 = loadAsset("/externalassets/classic/level24/objects/upscaled/platform2.webp", "Image")
			platform3 = loadAsset("/externalassets/classic/level24/objects/upscaled/platform3.webp", "Image")
			platform4 = loadAsset("/externalassets/classic/level24/objects/upscaled/platform4.webp", "Image")
			pedestal = loadAsset("/externalassets/classic/level24/objects/upscaled/pedestal.webp", "Image")
			octopus = loadAsset("/externalassets/classic/level24/objects/upscaled/octopus.webp", "Image")
		end	
			
		
		elseif nLevel==25 then
		shader2=false
			if skinupscaled==false then
				level25frgcl = loadAsset("/externalassets/classic/level25/level25frg.webp", "Image")
				level25bckcl = loadAsset("/externalassets/classic/level25/level25bck.webp", "Image")
			elseif skinupscaled==true then
				level25frgcl = loadAsset("/externalassets/classic/upscaled/level25/level25frg.webp", "Image")
				level25bckcl = loadAsset("/externalassets/classic/upscaled/level25/level25bck.webp", "Image")
			end
				
				--objects
			if skinupscaled==false then	
				egyptianwoman = loadAsset("/externalassets/classic/level25/objects/egyptianwoman.webp", "Image")
				desticka = loadAsset("/externalassets/classic/level25/objects/desticka.webp", "Image")
				faraon = loadAsset("/externalassets/classic/level25/objects/faraon.webp", "Image")
				mumycat = loadAsset("/externalassets/classic/level25/objects/mumycat.webp", "Image")
				mumysokol = loadAsset("/externalassets/classic/level25/objects/mumysokol.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level25/objects/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/classic/level25/objects/pipe2.webp", "Image")
				scarab = loadAsset("/externalassets/classic/level25/objects/scarab.webp", "Image")
				stul = loadAsset("/externalassets/classic/level25/objects/stul.webp", "Image")
				
		elseif skinupscaled==true then
				egyptianwoman = loadAsset("/externalassets/classic/level25/objects/upscaled/egyptianwoman.webp", "Image")
				desticka = loadAsset("/externalassets/classic/level25/objects/upscaled/desticka.webp", "Image")
				faraon = loadAsset("/externalassets/classic/level25/objects/upscaled/faraon.webp", "Image")
				mumycat = loadAsset("/externalassets/classic/level25/objects/upscaled/mumycat.webp", "Image")
				mumysokol = loadAsset("/externalassets/classic/level25/objects/upscaled/mumysokol.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level25/objects/upscaled/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/classic/level25/objects/upscaled/pipe2.webp", "Image")
				scarab = loadAsset("/externalassets/classic/level25/objects/upscaled/scarab.webp", "Image")
				stul = loadAsset("/externalassets/classic/level25/objects/upscaled/stul.webp", "Image")
		end
		
		elseif nLevel==26 then
		shader2=false
			if skinupscaled==false then
				level26frgcl= loadAsset("/externalassets/classic/level26/level26frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/level24/level24bck.webp", "Image")
			elseif skinupscaled==true then
				level26frgcl= loadAsset("/externalassets/classic/upscaled/level26/level26frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/upscaled/level24/level24bck.webp", "Image")
			end
			
			--objects
		if skinupscaled==false then
			crab= loadAsset("/externalassets/classic/level26/objects/crab.webp", "Image")
			craba2= loadAsset("/externalassets/classic/level26/objects/crab2.webp", "Image")
			craba3= loadAsset("/externalassets/classic/level26/objects/crab3.webp", "Image")
			speaker= loadAsset("/externalassets/classic/level26/objects/speaker.webp", "Image")
			speaker2= loadAsset("/externalassets/classic/level26/objects/speaker2.webp", "Image")
			pipe1= loadAsset("/externalassets/classic/level26/objects/pipe1.webp", "Image")
			pipe5= loadAsset("/externalassets/classic/level26/objects/pipe5.webp", "Image")
			bigpipe= loadAsset("/externalassets/classic/level26/objects/bigpipe.webp", "Image")
			column1= loadAsset("/externalassets/classic/level26/objects/column1.webp", "Image")
			column2= loadAsset("/externalassets/classic/level26/objects/column2.webp", "Image")
			column3= loadAsset("/externalassets/classic/level26/objects/column3.webp", "Image")
			column4= loadAsset("/externalassets/classic/level26/objects/column4.webp", "Image")
			skull= loadAsset("/externalassets/classic/level26/objects/skull.webp", "Image")
			statue= loadAsset("/externalassets/classic/level26/objects/statue.webp", "Image")
			face= loadAsset("/externalassets/classic/level26/objects/face.webp", "Image")
			statue= loadAsset("/externalassets/classic/level26/objects/statue.webp", "Image")
		elseif skinupscaled==true then
			crab= loadAsset("/externalassets/classic/level26/objects/upscaled/crab.webp", "Image")
			craba2= loadAsset("/externalassets/classic/level26/objects/upscaled/crab2.webp", "Image")
			craba3= loadAsset("/externalassets/classic/level26/objects/upscaled/crab3.webp", "Image")
			speaker= loadAsset("/externalassets/classic/level26/objects/upscaled/speaker.webp", "Image")
			speaker2= loadAsset("/externalassets/classic/level26/objects/upscaled/speaker2.webp", "Image")
			pipe1= loadAsset("/externalassets/classic/level26/objects/upscaled/pipe1.webp", "Image")
			pipe5= loadAsset("/externalassets/classic/level26/objects/upscaled/pipe5.webp", "Image")
			bigpipe= loadAsset("/externalassets/classic/level26/objects/upscaled/bigpipe.webp", "Image")
			column1= loadAsset("/externalassets/classic/level26/objects/upscaled/column1.webp", "Image")
			column2= loadAsset("/externalassets/classic/level26/objects/upscaled/column2.webp", "Image")
			column3= loadAsset("/externalassets/classic/level26/objects/upscaled/column3.webp", "Image")
			column4= loadAsset("/externalassets/classic/level26/objects/upscaled/column4.webp", "Image")
			skull= loadAsset("/externalassets/classic/level26/objects/upscaled/skull.webp", "Image")
			statue= loadAsset("/externalassets/classic/level26/objects/upscaled/statue.webp", "Image")
			face= loadAsset("/externalassets/classic/level26/objects/upscaled/face.webp", "Image")
			statue= loadAsset("/externalassets/classic/level26/objects/upscaled/statue.webp", "Image")
		end
		
		elseif nLevel==27 then
		shader2=false
		
			if skinupscaled==false then
				level27frgcl= loadAsset("/externalassets/classic/level27/level27frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/level27/level27bck.webp", "Image")
			elseif skinupscaled==true then
				level27frgcl= loadAsset("/externalassets/classic/upscaled/level27/level27frg.webp", "Image")
				level24bckcl= loadAsset("/externalassets/classic/upscaled/level27/level27bck.webp", "Image")
			end
			
		--objects
			if skinupscaled==false then
				anticka_hlava_00 = loadAsset("/externalassets/classic/level27/objects/anticka_hlava_00.webp", "Image")
				anticka_hlava_01 = loadAsset("/externalassets/classic/level27/objects/anticka_hlava_01.webp", "Image")
				anticka_hlava_02 = loadAsset("/externalassets/classic/level27/objects/anticka_hlava_02.webp", "Image")
				anticka_hlava_03 = loadAsset("/externalassets/classic/level27/objects/anticka_hlava_03.webp", "Image")
				anticka_hlava_04 = loadAsset("/externalassets/classic/level27/objects/anticka_hlava_ulomena.webp", "Image")
				
				balonek_00 = loadAsset("/externalassets/classic/level27/objects/balonek_00.webp", "Image")
				balonek_01 = loadAsset("/externalassets/classic/level27/objects/balonek_01.webp", "Image")
				balonek_02 = loadAsset("/externalassets/classic/level27/objects/balonek_02.webp", "Image")
				balonek_03 = loadAsset("/externalassets/classic/level27/objects/balonek_03.webp", "Image")
				
				shrimp_00 = loadAsset("/externalassets/classic/level27/objects/shrimp_01.webp", "Image")
				shrimp_01 = loadAsset("/externalassets/classic/level27/objects/shrimp_01.webp", "Image")
				shrimp_02 = loadAsset("/externalassets/classic/level27/objects/shrimp_02.webp", "Image")
				shrimp_03 = loadAsset("/externalassets/classic/level27/objects/shrimp_03.webp", "Image")
				shrimp_04 = loadAsset("/externalassets/classic/level27/objects/shrimp_04.webp", "Image")
				
				krab_00  = loadAsset("externalassets/classic/level29/objects/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/krab_09.webp", "Image")
				
				
				kr_00 = loadAsset("/externalassets/classic/level27/objects/kr_00.webp", "Image")
				kr_01 = loadAsset("/externalassets/classic/level27/objects/kr_01.webp", "Image")
				kr_02 = loadAsset("/externalassets/classic/level27/objects/kr_02.webp", "Image")
				kr_03 = loadAsset("/externalassets/classic/level27/objects/kr_03.webp", "Image")
				kr_04 = loadAsset("/externalassets/classic/level27/objects/kr_04.webp", "Image")
				kr_05 = loadAsset("/externalassets/classic/level27/objects/kr_05.webp", "Image")
				kr_06 = loadAsset("/externalassets/classic/level27/objects/kr_06.webp", "Image")
				kr_07 = loadAsset("/externalassets/classic/level27/objects/kr_07.webp", "Image")
				kr_08 = loadAsset("/externalassets/classic/level27/objects/kr_08.webp", "Image")
				kr_09 = loadAsset("/externalassets/classic/level27/objects/kr_09.webp", "Image")
				kr_10 = loadAsset("/externalassets/classic/level27/objects/kr_10.webp", "Image")
				kr_11 = loadAsset("/externalassets/classic/level27/objects/kr_11.webp", "Image")
				kr_12 = loadAsset("/externalassets/classic/level27/objects/kr_12.webp", "Image")
				kr_13 = loadAsset("/externalassets/classic/level27/objects/kr_13.webp", "Image")
				kr_14 = loadAsset("/externalassets/classic/level27/objects/kr_14.webp", "Image")
				kr_15 = loadAsset("/externalassets/classic/level27/objects/kr_15.webp", "Image")
				kr_16 = loadAsset("/externalassets/classic/level27/objects/kr_16.webp", "Image")
				kr_17 = loadAsset("/externalassets/classic/level27/objects/kr_17.webp", "Image")
				
			statuecl= loadAsset("/externalassets/classic/level21/objects/statue.webp", "Image")
			statue1 = loadAsset("/externalassets/classic/level21/objects/statue/1.webp", "Image")
			statue2 = loadAsset("/externalassets/classic/level21/objects/statue/2.webp", "Image")
			statue3 = loadAsset("/externalassets/classic/level21/objects/statue/3.webp", "Image")
			statue4 = loadAsset("/externalassets/classic/level21/objects/statue/4.webp", "Image")
			statue5 = loadAsset("/externalassets/classic/level21/objects/statue/5.webp", "Image")
			statue6 = loadAsset("/externalassets/classic/level21/objects/statue/6.webp", "Image")
			statue7 = loadAsset("/externalassets/classic/level21/objects/statue/7.webp", "Image")
			statue8 = loadAsset("/externalassets/classic/level21/objects/statue/8.webp", "Image")
			statue9 = loadAsset("/externalassets/classic/level21/objects/statue/9.webp", "Image")
			statue10 = loadAsset("/externalassets/classic/level21/objects/statue/10.webp", "Image")
			statue11 = loadAsset("/externalassets/classic/level21/objects/statue/11.webp", "Image")
			statue12 = loadAsset("/externalassets/classic/level21/objects/statue/12.webp", "Image")
			statue13 = loadAsset("/externalassets/classic/level21/objects/statue/13.webp", "Image")
			statue14 = loadAsset("/externalassets/classic/level21/objects/statue/14.webp", "Image")
			statue15 = loadAsset("/externalassets/classic/level21/objects/statue/15.webp", "Image")
			statue16 = loadAsset("/externalassets/classic/level21/objects/statue/16.webp", "Image")
			statue17 = loadAsset("/externalassets/classic/level21/objects/statue/17.webp", "Image")
			statue18 = loadAsset("/externalassets/classic/level21/objects/statue/18.webp", "Image")
			statue19 = loadAsset("/externalassets/classic/level21/objects/statue/19.webp", "Image")
				
				crab = loadAsset("/externalassets/classic/level27/objects/crab.webp", "Image")
				
				pipe = loadAsset("/externalassets/classic/level27/objects/pipe.webp", "Image")

				stone = loadAsset("/externalassets/classic/level27/objects/stone.webp", "Image")
				stonesmall = loadAsset("/externalassets/classic/level27/objects/stonesmall.webp", "Image")
	
			elseif skinupscaled==true then
				anticka_hlava_00 = loadAsset("/externalassets/classic/level27/objects/upscaled/anticka_hlava_00.webp", "Image")
				anticka_hlava_01 = loadAsset("/externalassets/classic/level27/objects/upscaled/anticka_hlava_01.webp", "Image")
				anticka_hlava_02 = loadAsset("/externalassets/classic/level27/objects/upscaled/anticka_hlava_02.webp", "Image")
				anticka_hlava_03 = loadAsset("/externalassets/classic/level27/objects/upscaled/anticka_hlava_03.webp", "Image")
				anticka_hlava_04 = loadAsset("/externalassets/classic/level27/objects/upscaled/anticka_hlava_ulomena.webp", "Image")
				
				balonek_00 = loadAsset("/externalassets/classic/level27/objects/upscaled/balonek_00.webp", "Image")
				balonek_01 = loadAsset("/externalassets/classic/level27/objects/upscaled/balonek_01.webp", "Image")
				balonek_02 = loadAsset("/externalassets/classic/level27/objects/upscaled/balonek_02.webp", "Image")
				balonek_03 = loadAsset("/externalassets/classic/level27/objects/upscaled/balonek_03.webp", "Image")
				
				shrimp_00 = loadAsset("/externalassets/classic/level27/objects/upscaled/shrimp_01.webp", "Image")
				shrimp_01 = loadAsset("/externalassets/classic/level27/objects/upscaled/shrimp_01.webp", "Image")
				shrimp_02 = loadAsset("/externalassets/classic/level27/objects/upscaled/shrimp_02.webp", "Image")
				shrimp_03 = loadAsset("/externalassets/classic/level27/objects/upscaled/shrimp_03.webp", "Image")
				shrimp_04 = loadAsset("/externalassets/classic/level27/objects/upscaled/shrimp_04.webp", "Image")
				
				krab_00  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_09.webp", "Image")
				
				
				kr_00 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_00.webp", "Image")
				kr_01 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_01.webp", "Image")
				kr_02 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_02.webp", "Image")
				kr_03 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_03.webp", "Image")
				kr_04 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_04.webp", "Image")
				kr_05 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_05.webp", "Image")
				kr_06 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_06.webp", "Image")
				kr_07 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_07.webp", "Image")
				kr_08 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_08.webp", "Image")
				kr_09 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_09.webp", "Image")
				kr_10 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_10.webp", "Image")
				kr_11 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_11.webp", "Image")
				kr_12 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_12.webp", "Image")
				kr_13 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_13.webp", "Image")
				kr_14 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_14.webp", "Image")
				kr_15 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_15.webp", "Image")
				kr_16 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_16.webp", "Image")
				kr_17 = loadAsset("/externalassets/classic/level27/objects/upscaled/kr_17.webp", "Image")
				
			statuecl= loadAsset("/externalassets/classic/level21/objects/upscaled/statue.webp", "Image")
			statue1 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/1.webp", "Image")
			statue2 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/2.webp", "Image")
			statue3 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/3.webp", "Image")
			statue4 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/4.webp", "Image")
			statue5 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/5.webp", "Image")
			statue6 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/6.webp", "Image")
			statue7 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/7.webp", "Image")
			statue8 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/8.webp", "Image")
			statue9 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/9.webp", "Image")
			statue10 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/10.webp", "Image")
			statue11 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/11.webp", "Image")
			statue12 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/12.webp", "Image")
			statue13 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/13.webp", "Image")
			statue14 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/14.webp", "Image")
			statue15 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/15.webp", "Image")
			statue16 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/16.webp", "Image")
			statue17 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/17.webp", "Image")
			statue18 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/18.webp", "Image")
			statue19 = loadAsset("/externalassets/classic/level21/objects/upscaled/statue/19.webp", "Image")
				
				crab = loadAsset("/externalassets/classic/level27/objects/upscaled/crab.webp", "Image")
				
				pipe = loadAsset("/externalassets/classic/level27/objects/upscaled/pipe.webp", "Image")

				stone = loadAsset("/externalassets/classic/level27/objects/upscaled/stone.webp", "Image")
				stonesmall = loadAsset("/externalassets/classic/level27/objects/upscaled/stonesmall.webp", "Image")
			end
		
			
		elseif nLevel==28 then
		shader2=false
		

			--background
			if skinupscaled==false then
				level28frgcl= loadAsset("/externalassets/classic/level28/level28frg.webp", "Image")
				level28bckcl= loadAsset("/externalassets/classic/level28/level28bck.webp", "Image")
			elseif skinupscaled==true then
				level28frgcl= loadAsset("/externalassets/classic/upscaled/level28/level28frg.webp", "Image")
				level28bckcl= loadAsset("/externalassets/classic/upscaled/level28/level28bck.webp", "Image")
			end
			
			--background second version
			--level28frg= loadAsset("/externalassets/levels/level28/level28frg.webp", "Image")
			--level28bck4= loadAsset("/externalassets/levels/level21/screensaver/level21screen2.webp", "Image")
			--level28bck= loadAsset("/externalassets/levels/level21/underwater_fantasy1.webp", "Image")
			--level28bck3= loadAsset("/externalassets/levels/level21/screensaver/level21screen1.webp", "Image")
			--level28bck2= loadAsset("/externalassets/levels/level21/screensaver/level21bck2.webp", "Image")
			
					--objects
		if skinupscaled==false then
			skull= loadAsset("/externalassets/classic/level20/objects/skull.webp", "Image")
			amphora= loadAsset("/externalassets/classic/level20/objects/amphora.webp", "Image")
			shell= loadAsset("/externalassets/classic/level20/objects/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/classic/level20/objects/sculpture.webp", "Image")
			sculpture1= loadAsset("/externalassets/classic/level20/objects/sculpture/1.webp", "Image")
			sculpture2= loadAsset("/externalassets/classic/level20/objects/sculpture/2.webp", "Image")
			cross= loadAsset("/externalassets/classic/level20/objects/cross.webp", "Image")
			cross1= loadAsset("/externalassets/classic/level20/objects/cross/1.webp", "Image")
			cross2= loadAsset("/externalassets/classic/level20/objects/cross/2.webp", "Image")
			
			elevator1= loadAsset("/externalassets/classic/level20/objects/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/classic/level20/objects/elevator2.webp", "Image")
			
		elseif skinupscaled==true then
			skull= loadAsset("/externalassets/classic/level20/objects/upscaled/skull.webp", "Image")
			amphora= loadAsset("/externalassets/classic/level20/objects/upscaled/amphora.webp", "Image")
			shell= loadAsset("/externalassets/classic/level20/objects/upscaled/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture.webp", "Image")
			sculpture1= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/1.webp", "Image")
			sculpture2= loadAsset("/externalassets/classic/level20/objects/upscaled/sculpture/2.webp", "Image")
			elevator1= loadAsset("/externalassets/classic/level20/objects/upscaled/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/classic/level20/objects/upscaled/elevator2.webp", "Image")
			cross= loadAsset("/externalassets/classic/level20/objects/upscaled/cross.webp", "Image")
			cross1= loadAsset("/externalassets/classic/level20/objects/upscaled/cross/1.webp", "Image")
			cross2= loadAsset("/externalassets/classic/level20/objects/upscaled/cross/2.webp", "Image")
		end	

		
			--fishs
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
				skullpushed=false
		
				skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==29 then
		shader2=false
	
		--background
			if skinupscaled==false then
				level29frgcl= loadAsset("/externalassets/classic/level29/level29frg.webp", "Image")
				level29bckcl= loadAsset("/externalassets/classic/level29/level29bck.webp", "Image")
			elseif skinupscaled==true then
				level29frgcl= loadAsset("/externalassets/classic/upscaled/level29/level29frg.webp", "Image")
				level29bckcl= loadAsset("/externalassets/classic/upscaled/level29/level29bck.webp", "Image")
			end
			
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
			--objects
		if skinupscaled==false then
				plug = loadAsset("/externalassets/classic/level29/objects/plug.webp", "Image")
				pipe = loadAsset("externalassets/classic/level29/objects/pipe.webp", "Image")
				pipe2 = loadAsset("externalassets/classic/level29/objects/pipe2.webp", "Image")
				pipe3 = loadAsset("externalassets/classic/level29/objects/pipe3.webp", "Image")
				pipe1block = loadAsset("externalassets/classic/level29/objects/pipe1block.webp", "Image")
				pipe2blocks = loadAsset("externalassets/classic/level29/objects/pipe2blocks.webp", "Image")
				column = loadAsset("externalassets/classic/level29/objects/column.webp", "Image")
				column2 = loadAsset("externalassets/classic/level29/objects/column2.webp", "Image")
				crab = loadAsset("externalassets/classic/level29/objects/crab.webp", "Image")
				snail = loadAsset("externalassets/classic/level29/objects/snail.webp", "Image")
				
				krab_00  = loadAsset("externalassets/classic/level29/objects/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/krab_09.webp", "Image")
				
				snail00 = loadAsset("externalassets/classic/level29/objects/maly_snek_00.webp", "Image")
				snail01 = loadAsset("externalassets/classic/level29/objects/maly_snek_01.webp", "Image")
				snail02 = loadAsset("externalassets/classic/level29/objects/maly_snek_02.webp", "Image")
				snail03 = loadAsset("externalassets/classic/level29/objects/maly_snek_03.webp", "Image")
				
				
				maly_snek_00 = loadAsset("externalassets/classic/level29/objects/maly_snek_00.webp", "Image")
				maly_snek_01 = loadAsset("externalassets/classic/level29/objects/maly_snek_01.webp", "Image")
				maly_snek_02 = loadAsset("externalassets/classic/level29/objects/maly_snek_02.webp", "Image")
				maly_snek_03 = loadAsset("externalassets/classic/level29/objects/maly_snek_03.webp", "Image")
				
				sculpture = loadAsset("externalassets/classic/level29/objects/hlava_m-_00.webp", "Image")
				sculpture1 = loadAsset("externalassets/classic/level29/objects/hlava_m-_01.webp", "Image")
				sculpture2 = loadAsset("externalassets/classic/level29/objects/hlava_m-_02.webp", "Image")
				
				
		elseif skinupscaled==true then
				plug = loadAsset("/externalassets/classic/level29/objects/upscaled/plug.webp", "Image")
				pipe = loadAsset("externalassets/classic/level29/objects/upscaled/pipe.webp", "Image")
				pipe2 = loadAsset("externalassets/classic/level29/objects/upscaled/pipe2.webp", "Image")
				pipe3 = loadAsset("externalassets/classic/level29/objects/upscaled/pipe3.webp", "Image")
				pipe1block = loadAsset("externalassets/classic/level29/objects/upscaled/pipe1block.webp", "Image")
				pipe2blocks = loadAsset("externalassets/classic/level29/objects/upscaled/pipe2blocks.webp", "Image")
				column = loadAsset("externalassets/classic/level29/objects/upscaled/column.webp", "Image")
				column2 = loadAsset("externalassets/classic/level29/objects/upscaled/column2.webp", "Image")
				crab = loadAsset("externalassets/classic/level29/objects/upscaled/crab.webp", "Image")
				krab_00  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/upscaled/krab_09.webp", "Image")
				snail = loadAsset("externalassets/classic/level29/objects/upscaled/snail.webp", "Image")
				snail00 = loadAsset("externalassets/classic/level29/objects/upscaled/maly_snek_00.webp", "Image")
				snail01 = loadAsset("externalassets/classic/level29/objects/upscaled/maly_snek_01.webp", "Image")
				snail02 = loadAsset("externalassets/classic/level29/objects/upscaled/maly_snek_02.webp", "Image")
				snail03 = loadAsset("externalassets/classic/level29/objects/upscaled/maly_snek_03.webp", "Image")
				
				sculpture = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_00.webp", "Image")
				sculpture1 = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_01.webp", "Image")
				sculpture2 = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_02.webp", "Image")
		end
					
					
		elseif nLevel==30 then
		shader2=false
			--background
			if skinupscaled==false then
				level30frgcl= loadAsset("/externalassets/classic/level30/level30frg.webp", "Image")
				level30bckcl= loadAsset("/externalassets/classic/level30/level30bck.webp", "Image")
			elseif skinupscaled==true then
				level30frgcl= loadAsset("/externalassets/classic/upscaled/level30/level30frg.webp", "Image")
				level30bckcl= loadAsset("/externalassets/classic/upscaled/level30/level30bck.webp", "Image")
			end
			
			--ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
			--objects
			if skinupscaled==false then
				crab = loadAsset("/externalassets/classic/level30/objects/crab.webp", "Image")
				piece1= loadAsset("/externalassets/classic/level30/objects/piece1.webp", "Image")
				piece2= loadAsset("/externalassets/classic/level30/objects/piece2.webp", "Image")
				piece3= loadAsset("/externalassets/classic/level30/objects/piece3.webp", "Image")
				pipe= loadAsset("/externalassets/classic/level30/objects/pipe.webp", "Image")
			
			elseif skinupscaled==true then
				crab = loadAsset("/externalassets/classic/level30/objects/upscaled/crab.webp", "Image")
				piece1= loadAsset("/externalassets/classic/level30/objects/upscaled/piece1.webp", "Image")
				piece2= loadAsset("/externalassets/classic/level30/objects/upscaled/piece2.webp", "Image")
				piece3= loadAsset("/externalassets/classic/level30/objects/upscaled/piece3.webp", "Image")
				pipe= loadAsset("/externalassets/classic/level30/objects/upscaled/pipe.webp", "Image")
			
			
			end

		
		elseif nLevel==31 then
		shader2=false
		
		--background
		if skinupscaled==false then
			level31frgcl= loadAsset("/externalassets/classic/level31/level31frg.webp", "Image")
			level31bckcl= loadAsset("/externalassets/classic/level31/level31bck.webp", "Image")
		elseif skinupscaled==true then
			level31frgcl= loadAsset("/externalassets/classic/upscaled/level31/level31frg.webp", "Image")
			level31bckcl= loadAsset("/externalassets/classic/upscaled/level31/level31bck.webp", "Image")
		end
		
		--ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		--objects
			if skinupscaled==false then
				labyrinth = loadAsset("/externalassets/classic/level31/objects/labyrinth.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level31/objects/pipe.webp", "Image")
				snail = loadAsset("/externalassets/classic/level31/objects/pipe.webp", "Image")
			elseif skinupscaled==true then
				labyrinth = loadAsset("/externalassets/classic/level31/objects/upscaled/labyrinth.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level31/objects/upscaled/pipe.webp", "Image")
				snail = loadAsset("/externalassets/classic/level31/objects/upscaled/pipe.webp", "Image")
			end
			
			
			
		
		elseif nLevel==32 then
		shader2=false
	
		if skinupscaled==false then
			level32frgcl= loadAsset("/externalassets/classic/level32/level32frg.webp", "Image")
			level32bckcl= loadAsset("/externalassets/classic/level32/level32bck.webp", "Image")
		elseif skinupscaled==true then
			level32frgcl= loadAsset("/externalassets/classic/upscaled/level32/level32frg.webp", "Image")
			level32bckcl= loadAsset("/externalassets/classic/upscaled/level32/level32bck.webp", "Image")
		end
		
		--objects
			if skinupscaled==false then
				coral_cerv = loadAsset("/externalassets/classic/level32/objects/coral_cerv.webp", "Image")
				seahorse00 = loadAsset("/externalassets/classic/level32/objects/seahorse00.webp", "Image")
				seahorse01 = loadAsset("/externalassets/classic/level32/objects/seahorse01.webp", "Image")
				seahorse02 = loadAsset("/externalassets/classic/level32/objects/seahorse02.webp", "Image")
				seahorse03 = loadAsset("/externalassets/classic/level32/objects/seahorse03.webp", "Image")
				korala = loadAsset("/externalassets/classic/level32/objects/korala.webp", "Image")
				koralb = loadAsset("/externalassets/classic/level32/objects/koralb.webp", "Image")
				koral_bily = loadAsset("/externalassets/classic/level32/objects/koral_bily.webp", "Image")
				koralc = loadAsset("/externalassets/classic/level32/objects/koralc.webp", "Image")
				korald = loadAsset("/externalassets/classic/level32/objects/korald.webp", "Image")
				koral_dlouhy = loadAsset("/externalassets/classic/level32/objects/koral_dlouhy.webp", "Image")
				koral_zel = loadAsset("/externalassets/classic/level32/objects/koral_zel.webp", "Image")
				musle_troj = loadAsset("/externalassets/classic/level32/objects/musle_troj.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level32/objects/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/classic/level32/objects/pipe2.webp", "Image")
				pipe3 = loadAsset("/externalassets/classic/level32/objects/pipe3.webp", "Image")
				pipe4 = loadAsset("/externalassets/classic/level32/objects/pipe4.webp", "Image")
				pipe5 = loadAsset("/externalassets/classic/level32/objects/pipe5.webp", "Image")
				sasanka00 = loadAsset("/externalassets/classic/level32/objects/sasanka00.webp", "Image")
				sasanka01 = loadAsset("/externalassets/classic/level32/objects/sasanka01.webp", "Image")
				sasanka02 = loadAsset("/externalassets/classic/level32/objects/sasanka02.webp", "Image")
				sasanka03 = loadAsset("/externalassets/classic/level32/objects/sasanka03.webp", "Image")
				sasanka04 = loadAsset("/externalassets/classic/level32/objects/sasanka04.webp", "Image")
				sasanka05 = loadAsset("/externalassets/classic/level32/objects/sasanka05.webp", "Image")
				sasanka06 = loadAsset("/externalassets/classic/level32/objects/sasanka06.webp", "Image")
				sasanka07 = loadAsset("/externalassets/classic/level32/objects/sasanka07.webp", "Image")
				snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
				snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
				snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
				snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
			elseif skinupscaled==true then
				coral_cerv = loadAsset("/externalassets/classic/level32/objects/upscaled/coral_cerv.webp", "Image")
				seahorse00 = loadAsset("/externalassets/classic/level32/objects/upscaled/seahorse00.webp", "Image")
				seahorse01 = loadAsset("/externalassets/classic/level32/objects/upscaled/seahorse01.webp", "Image")
				seahorse02 = loadAsset("/externalassets/classic/level32/objects/upscaled/seahorse02.webp", "Image")
				seahorse03 = loadAsset("/externalassets/classic/level32/objects/upscaled/seahorse03.webp", "Image")
				korala = loadAsset("/externalassets/classic/level32/objects/upscaled/korala.webp", "Image")
				koralb = loadAsset("/externalassets/classic/level32/objects/upscaled/koralb.webp", "Image")
				koral_bily = loadAsset("/externalassets/classic/level32/objects/upscaled/koral_bily.webp", "Image")
				koralc = loadAsset("/externalassets/classic/level32/objects/upscaled/koralc.webp", "Image")
				korald = loadAsset("/externalassets/classic/level32/objects/upscaled/korald.webp", "Image")
				koral_dlouhy = loadAsset("/externalassets/classic/level32/objects/upscaled/koral_dlouhy.webp", "Image")
				koral_zel = loadAsset("/externalassets/classic/level32/objects/upscaled/koral_zel.webp", "Image")
				musle_troj = loadAsset("/externalassets/classic/level32/objects/upscaled/musle_troj.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe2.webp", "Image")
				pipe3 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe3.webp", "Image")
				pipe4 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe4.webp", "Image")
				pipe5 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe5.webp", "Image")
				sasanka00 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka01 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka02 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka03 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka04 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka05 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka06 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				sasanka07 = loadAsset("/externalassets/classic/level32/objects/upscaled/sasanka00.webp", "Image")
				snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
				snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
				snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
				snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
			end
			
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==33 then
		shader2=false
	
		
		if skinupscaled==false then
			level33frgcl= loadAsset("/externalassets/classic/level33/level33frg.webp", "Image")
			level33bckcl= loadAsset("/externalassets/classic/level33/level33bck.webp", "Image")
	elseif skinupscaled==true then
			level33frgcl= loadAsset("/externalassets/classic/upscaled/level33/level33frg.webp", "Image")
			level33bckcl= loadAsset("/externalassets/classic/upscaled/level33/level33bck.webp", "Image")
		end
			if skinupscaled==false then
			crab00 = loadAsset("/externalassets/classic/level33/objects/crab00.webp", "Image")
			crab01 = loadAsset("/externalassets/classic/level33/objects/crab01.webp", "Image")
			crab02 = loadAsset("/externalassets/classic/level33/objects/crab02.webp", "Image")
			crab03 = loadAsset("/externalassets/classic/level33/objects/crab03.webp", "Image")
			
			crab200 = loadAsset("/externalassets/classic/level33/objects/crab200.webp", "Image")
			crab201 = loadAsset("/externalassets/classic/level33/objects/crab201.webp", "Image")
			crab202 = loadAsset("/externalassets/classic/level33/objects/crab202.webp", "Image")
			crab203 = loadAsset("/externalassets/classic/level33/objects/crab203.webp", "Image")
			
			
			crab300 = loadAsset("/externalassets/classic/level33/objects/crab300.webp", "Image")
			crab301 = loadAsset("/externalassets/classic/level33/objects/crab301.webp", "Image")
			crab302 = loadAsset("/externalassets/classic/level33/objects/crab302.webp", "Image")
			crab303 = loadAsset("/externalassets/classic/level33/objects/crab303.webp", "Image")
			
			
			crab400 = loadAsset("/externalassets/classic/level33/objects/crab400.webp", "Image")
			crab401 = loadAsset("/externalassets/classic/level33/objects/crab401.webp", "Image")
			crab402 = loadAsset("/externalassets/classic/level33/objects/crab402.webp", "Image")
			crab403 = loadAsset("/externalassets/classic/level33/objects/crab403.webp", "Image")
			
			
			pipe = loadAsset("/externalassets/classic/level33/objects/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level33/objects/pipe2.webp", "Image")
			
			seahorse00 = loadAsset("/externalassets/classic/level33/objects/seahorse00.webp", "Image")
			seahorse01 = loadAsset("/externalassets/classic/level33/objects/seahorse01.webp", "Image")
			seahorse02 = loadAsset("/externalassets/classic/level33/objects/seahorse02.webp", "Image")
			seahorse03 = loadAsset("/externalassets/classic/level33/objects/seahorse03.webp", "Image")
			
			snail00 = loadAsset("/externalassets/classic/level33/objects/snail_00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level33/objects/snail_01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level33/objects/snail_02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level33/objects/snail_03.webp", "Image")
			
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_02.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_03.webp", "Image")
		
		elseif skinupscaled==true then
			crab00 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab00.webp", "Image")
			crab01 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab01.webp", "Image")
			crab02 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab02.webp", "Image")
			crab03 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab03.webp", "Image")
			
			crab200 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab200.webp", "Image")
			crab201 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab201.webp", "Image")
			crab202 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab202.webp", "Image")
			crab203 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab203.webp", "Image")
			
			
			crab300 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab300.webp", "Image")
			crab301 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab301.webp", "Image")
			crab302 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab302.webp", "Image")
			crab303 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab303.webp", "Image")
			
			
			crab400 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab400.webp", "Image")
			crab401 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab401.webp", "Image")
			crab402 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab402.webp", "Image")
			crab403 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab403.webp", "Image")
			
			
			pipe = loadAsset("/externalassets/classic/level33/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level33/objects/upscaled/pipe2.webp", "Image")
			
			seahorse00 = loadAsset("/externalassets/classic/level33/objects/upscaled/seahorse00.webp", "Image")
			seahorse01 = loadAsset("/externalassets/classic/level33/objects/upscaled/seahorse01.webp", "Image")
			seahorse02 = loadAsset("/externalassets/classic/level33/objects/upscaled/seahorse02.webp", "Image")
			seahorse03 = loadAsset("/externalassets/classic/level33/objects/upscaled/seahorse03.webp", "Image")
			
			snail00 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_03.webp", "Image")
			
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
		
		end
		--objects
		
		
		
		elseif nLevel==34 then
		shader2=false

		
		loadwhale()
		
		if res=="1080p"or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
		
		--background
		if skinupscaled==false then
			level34frgcl= loadAsset("/externalassets/classic/level34/level34frg.webp", "Image")
			level34bckcl= loadAsset("/externalassets/classic/level34/level34bck.webp", "Image")
		elseif skinupscaled==true then
			level34frgcl= loadAsset("/externalassets/classic/upscaled/level34/level34frg.webp", "Image")
			level34bckcl= loadAsset("/externalassets/classic/upscaled/level34/level34bck.webp", "Image")
		end	
		
			
		elseif res=="8k" then	-- 8k background and foreground
		
			level34frg= loadAsset("/externalassets/levels/level34/8k/level34frg8k.webp", "Image")
			level34frg2= loadAsset("/externalassets/levels/level34/8k/level34frg28k.webp", "Image")
			level34bck= loadAsset("/externalassets/levels/level34/8k/level34bck8k.webp", "Image")
			level34bck2= loadAsset("/externalassets/levels/level34/8k/level34bck28k.webp", "Image")
		end
			
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
			
			-- objects
			if skinupscaled==false then
				ocel1 = loadAsset("/externalassets/classic/level34/objects/ocel-1.webp", "Image")
				ocel2 = loadAsset("/externalassets/classic/level34/objects/ocel-2.webp", "Image")
				ocel3 = loadAsset("/externalassets/classic/level34/objects/ocel-3.webp", "Image")
				balal_00 = loadAsset("/externalassets/classic/level34/objects/balal_00.webp", "Image")
				balal_01 = loadAsset("/externalassets/classic/level34/objects/balal_01.webp", "Image")
				balal_02 = loadAsset("/externalassets/classic/level34/objects/balal_02.webp", "Image")
				balal_03 = loadAsset("/externalassets/classic/level34/objects/balal_03.webp", "Image")
				balal_04 = loadAsset("/externalassets/classic/level34/objects/balal_04.webp", "Image")
				balal_05 = loadAsset("/externalassets/classic/level34/objects/balal_05.webp", "Image")
				balal_06 = loadAsset("/externalassets/classic/level34/objects/balal_06.webp", "Image")
				balal_07 = loadAsset("/externalassets/classic/level34/objects/balal_07.webp", "Image")
				balal_08 = loadAsset("/externalassets/classic/level34/objects/balal_08.webp", "Image")
				balal_09 = loadAsset("/externalassets/classic/level34/objects/balal_09.webp", "Image")
				balal_10 = loadAsset("/externalassets/classic/level34/objects/balal_10.webp", "Image")
				
				krab_00 = loadAsset("/externalassets/classic/level34/objects/krab_00.webp", "Image")
				krab_01 = loadAsset("/externalassets/classic/level34/objects/krab_01.webp", "Image")
				krab_02 = loadAsset("/externalassets/classic/level34/objects/krab_02.webp", "Image")
				krab_03 = loadAsset("/externalassets/classic/level34/objects/krab_03.webp", "Image")
				krab_04 = loadAsset("/externalassets/classic/level34/objects/krab_04.webp", "Image")
				krab_05 = loadAsset("/externalassets/classic/level34/objects/krab_05.webp", "Image")
				krab_06 = loadAsset("/externalassets/classic/level34/objects/krab_06.webp", "Image")
				krab_07 = loadAsset("/externalassets/classic/level34/objects/krab_07.webp", "Image")
				krab_08 = loadAsset("/externalassets/classic/level34/objects/krab_08.webp", "Image")
				krab_09 = loadAsset("/externalassets/classic/level34/objects/krab_09.webp", "Image")
				maly_snek_00 = loadAsset("/externalassets/classic/level34/objects/maly_snek_00.webp", "Image")
				maly_snek_01 = loadAsset("/externalassets/classic/level34/objects/maly_snek_01.webp", "Image")
				maly_snek_02 = loadAsset("/externalassets/classic/level34/objects/maly_snek_02.webp", "Image")
				maly_snek_03 = loadAsset("/externalassets/classic/level34/objects/maly_snek_03.webp", "Image")
				sasanka_00 = loadAsset("/externalassets/classic/level34/objects/sasanka_00.webp", "Image")
				sasanka_01 = loadAsset("/externalassets/classic/level34/objects/sasanka_01.webp", "Image")
				sasanka_02 = loadAsset("/externalassets/classic/level34/objects/sasanka_02.webp", "Image")
				sasanka_03 = loadAsset("/externalassets/classic/level34/objects/sasanka_03.webp", "Image")
				sasanka_04 = loadAsset("/externalassets/classic/level34/objects/sasanka_04.webp", "Image")
				sasanka_05 = loadAsset("/externalassets/classic/level34/objects/sasanka_05.webp", "Image")
				sasanka_06 = loadAsset("/externalassets/classic/level34/objects/sasanka_06.webp", "Image")
				sasanka_07 = loadAsset("/externalassets/classic/level34/objects/sasanka_07.webp", "Image")
				sepie_00 = loadAsset("/externalassets/classic/level34/objects/sepie_00.webp", "Image")
				sepie_01 = loadAsset("/externalassets/classic/level34/objects/sepie_01.webp", "Image")
				sepie_02 = loadAsset("/externalassets/classic/level34/objects/sepie_02.webp", "Image")
				sepie_03 = loadAsset("/externalassets/classic/level34/objects/sepie_03.webp", "Image")
				sepie_04 = loadAsset("/externalassets/classic/level34/objects/sepie_04.webp", "Image")
				sepie_05 = loadAsset("/externalassets/classic/level34/objects/sepie_05.webp", "Image")
				sepie_06 = loadAsset("/externalassets/classic/level34/objects/sepie_06.webp", "Image")
				sepie_07 = loadAsset("/externalassets/classic/level34/objects/sepie_07.webp", "Image")
				sepie_08 = loadAsset("/externalassets/classic/level34/objects/sepie_08.webp", "Image")
				sepie_09 = loadAsset("/externalassets/classic/level34/objects/sepie_09.webp", "Image")
				sepie_10 = loadAsset("/externalassets/classic/level34/objects/sepie_10.webp", "Image")
				sepie_11 = loadAsset("/externalassets/classic/level34/objects/sepie_11.webp", "Image")
				sepie_12 = loadAsset("/externalassets/classic/level34/objects/sepie_12.webp", "Image")
				shell1 = loadAsset("/externalassets/classic/level34/objects/shell1.webp", "Image")
				
				
			elseif skinupscaled==true then
				ocel1 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-1.webp", "Image")
				ocel2 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-2.webp", "Image")
				ocel3 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-3.webp", "Image")
				balal_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_00.webp", "Image")
				balal_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_01.webp", "Image")
				balal_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_02.webp", "Image")
				balal_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_03.webp", "Image")
				balal_04 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_04.webp", "Image")
				balal_05 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_05.webp", "Image")
				balal_06 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_06.webp", "Image")
				balal_07 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_07.webp", "Image")
				balal_08 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_08.webp", "Image")
				balal_09 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_09.webp", "Image")
				balal_10 = loadAsset("/externalassets/classic/level34/objects/upscaled/balal_10.webp", "Image")
				
				krab_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_00.webp", "Image")
				krab_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_01.webp", "Image")
				krab_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_02.webp", "Image")
				krab_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_03.webp", "Image")
				krab_04 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_04.webp", "Image")
				krab_05 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_05.webp", "Image")
				krab_06 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_06.webp", "Image")
				krab_07 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_07.webp", "Image")
				krab_08 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_08.webp", "Image")
				krab_09 = loadAsset("/externalassets/classic/level34/objects/upscaled/krab_09.webp", "Image")
				maly_snek_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_00.webp", "Image")
				maly_snek_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_01.webp", "Image")
				maly_snek_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_02.webp", "Image")
				maly_snek_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_03.webp", "Image")
				sasanka_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_00.webp", "Image")
				sasanka_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_01.webp", "Image")
				sasanka_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_02.webp", "Image")
				sasanka_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_03.webp", "Image")
				sasanka_04 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_04.webp", "Image")
				sasanka_05 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_05.webp", "Image")
				sasanka_06 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_06.webp", "Image")
				sasanka_07 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_07.webp", "Image")
				sepie_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_00.webp", "Image")
				sepie_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_01.webp", "Image")
				sepie_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_02.webp", "Image")
				sepie_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_03.webp", "Image")
				sepie_04 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_04.webp", "Image")
				sepie_05 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_05.webp", "Image")
				sepie_06 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_06.webp", "Image")
				sepie_07 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_07.webp", "Image")
				sepie_08 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_08.webp", "Image")
				sepie_09 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_09.webp", "Image")
				sepie_10 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_10.webp", "Image")
				sepie_11 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_11.webp", "Image")
				sepie_12 = loadAsset("/externalassets/classic/level34/objects/upscaled/sepie_12.webp", "Image")
				shell1 = loadAsset("/externalassets/classic/level34/objects/upscaled/shell1.webp", "Image")
				
			end
			
		elseif nLevel==35 then
		shader2=false
		if res=="720p" or res=="1080p" or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
			if skinupscaled==false then
				level35bckcl= loadAsset("/externalassets/classic/level35/level35bck.webp", "Image")
				level35frgcl= loadAsset("/externalassets/classic/level35/level35frg.webp", "Image")
			elseif skinupscaled==true then
				level35bckcl= loadAsset("/externalassets/classic/upscaled/level35/level35bck.webp", "Image")
				level35frgcl= loadAsset("/externalassets/classic/upscaled/level35/level35frg.webp", "Image")
			end
		end
			
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
			-- objects
			if skinupscaled==false then
			krab_00= loadAsset("/externalassets/classic//level35/objects/krab_00.webp", "Image")
			krab_01= loadAsset("/externalassets/classic//level35/objects/krab_01.webp", "Image")
			krab_02= loadAsset("/externalassets/classic//level35/objects/krab_02.webp", "Image")
			krab_03= loadAsset("/externalassets/classic//level35/objects/krab_03.webp", "Image")
			krab_04= loadAsset("/externalassets/classic//level35/objects/krab_04.webp", "Image")
			krab_05= loadAsset("/externalassets/classic//level35/objects/krab_05.webp", "Image")
			krab_06= loadAsset("/externalassets/classic//level35/objects/krab_06.webp", "Image")
			krab_07= loadAsset("/externalassets/classic//level35/objects/krab_07.webp", "Image")
			krab_08= loadAsset("/externalassets/classic//level35/objects/krab_08.webp", "Image")
			krab_09= loadAsset("/externalassets/classic//level35/objects/krab_09.webp", "Image")
			muslicka = loadAsset("/externalassets/classic//level35/objects/muslicka.webp", "Image")
			rejnok_00 = loadAsset("/externalassets/classic//level35/objects/rejnok_00.webp", "Image")
			rejnok_01 = loadAsset("/externalassets/classic//level35/objects/rejnok_01.webp", "Image")
			rejnok_02 = loadAsset("/externalassets/classic//level35/objects/rejnok_02.webp", "Image")
			rejnok_03 = loadAsset("/externalassets/classic//level35/objects/rejnok_03.webp", "Image")
			rejnok_04 = loadAsset("/externalassets/classic//level35/objects/rejnok_04.webp", "Image")
			rejnok_05 = loadAsset("/externalassets/classic//level35/objects/rejnok_05.webp", "Image")
			rejnok_06 = loadAsset("/externalassets/classic//level35/objects/rejnok_06.webp", "Image")
			rejnok_07 = loadAsset("/externalassets/classic//level35/objects/rejnok_07.webp", "Image")
			rejnok_08 = loadAsset("/externalassets/classic//level35/objects/rejnok_08.webp", "Image")
			rejnok_09 = loadAsset("/externalassets/classic//level35/objects/rejnok_09.webp", "Image")
			rejnok_10 = loadAsset("/externalassets/classic//level35/objects/rejnok_10.webp", "Image")
			rejnok_11 = loadAsset("/externalassets/classic//level35/objects/rejnok_11.webp", "Image")
			sasanka_00 = loadAsset("/externalassets/classic//level35/objects/sasanka_00.webp", "Image")
			sasanka_01 = loadAsset("/externalassets/classic//level35/objects/sasanka_01.webp", "Image")
			sasanka_02 = loadAsset("/externalassets/classic//level35/objects/sasanka_02.webp", "Image")
			sasanka_03 = loadAsset("/externalassets/classic//level35/objects/sasanka_03.webp", "Image")
			sasanka_04 = loadAsset("/externalassets/classic//level35/objects/sasanka_04.webp", "Image")
			sasanka_05 = loadAsset("/externalassets/classic//level35/objects/sasanka_05.webp", "Image")
			sasanka_06 = loadAsset("/externalassets/classic//level35/objects/sasanka_06.webp", "Image")
			sasanka_07 = loadAsset("/externalassets/classic//level35/objects/sasanka_07.webp", "Image")
			sepie_00 = loadAsset("/externalassets/classic//level35/objects/sepie_00.webp", "Image")
			sepie_01 = loadAsset("/externalassets/classic//level35/objects/sepie_01.webp", "Image")
			sepie_02 = loadAsset("/externalassets/classic//level35/objects/sepie_02.webp", "Image")
			sepie_03 = loadAsset("/externalassets/classic//level35/objects/sepie_03.webp", "Image")
			sepie_04 = loadAsset("/externalassets/classic//level35/objects/sepie_04.webp", "Image")
			sepie_05 = loadAsset("/externalassets/classic//level35/objects/sepie_05.webp", "Image")
			sepie_06 = loadAsset("/externalassets/classic//level35/objects/sepie_06.webp", "Image")
			sepie_07 = loadAsset("/externalassets/classic//level35/objects/sepie_07.webp", "Image")
			sepie_08 = loadAsset("/externalassets/classic//level35/objects/sepie_08.webp", "Image")
			sepie_09 = loadAsset("/externalassets/classic//level35/objects/sepie_09.webp", "Image")
			sepie_10 = loadAsset("/externalassets/classic//level35/objects/sepie_10.webp", "Image")
			sepie_11 = loadAsset("/externalassets/classic//level35/objects/sepie_11.webp", "Image")
			sepie_12 = loadAsset("/externalassets/classic//level35/objects/sepie_12.webp", "Image")
			shell1 = loadAsset("/externalassets/classic//level35/objects/shell1.webp", "Image")
			tecko = loadAsset("/externalassets/classic//level35/objects/tecko.webp", "Image")
			kankan7= loadAsset("/externalassets/classic//level35/objects/kankan-7.webp", "Image")
			kankan8= loadAsset("/externalassets/classic//level35/objects/kankan-8.webp", "Image")
			kankan9= loadAsset("/externalassets/classic//level35/objects/kankan-9.webp", "Image")
			kankan12= loadAsset("/externalassets/classic//level35/objects/kankan-12.webp", "Image")
			kankan17= loadAsset("/externalassets/classic//level35/objects/kankan-17.webp", "Image")
			klavir_00= loadAsset("/externalassets/classic//level35/objects/klavir_00.webp", "Image")
			klavir_01= loadAsset("/externalassets/classic//level35/objects/klavir_01.webp", "Image")
			klavir_02= loadAsset("/externalassets/classic//level35/objects/klavir_02.webp", "Image")
			klavir_03= loadAsset("/externalassets/classic//level35/objects/klavir_03.webp", "Image")
			klavir_04= loadAsset("/externalassets/classic//level35/objects/klavir_04.webp", "Image")
			klavir_05= loadAsset("/externalassets/classic//level35/objects/klavir_05.webp", "Image")
			klavir_06= loadAsset("/externalassets/classic//level35/objects/klavir_06.webp", "Image")
			klavir_07= loadAsset("/externalassets/classic//level35/objects/klavir_07.webp", "Image")
			klavir_08= loadAsset("/externalassets/classic//level35/objects/klavir_08.webp", "Image")
			klavir_09= loadAsset("/externalassets/classic//level35/objects/klavir_09.webp", "Image")
			korals= loadAsset("/externalassets/classic//level35/objects/koral_s.webp", "Image")
			tecko = loadAsset("/externalassets/classic//level35/objects/tecko.webp", "Image")
			
			elseif skinupscaled==true then			
			
			krab_00= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_00.webp", "Image")
			krab_01= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_01.webp", "Image")
			krab_02= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_02.webp", "Image")
			krab_03= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_03.webp", "Image")
			krab_04= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_04.webp", "Image")
			krab_05= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_05.webp", "Image")
			krab_06= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_06.webp", "Image")
			krab_07= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_07.webp", "Image")
			krab_08= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_08.webp", "Image")
			krab_09= loadAsset("/externalassets/classic//level34/objects/upscaled/krab_09.webp", "Image")
			
			muslicka = loadAsset("/externalassets/classic//level35/objects/upscaled/muslicka.webp", "Image")
			rejnok_00 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_00.webp", "Image")
			rejnok_01 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_01.webp", "Image")
			rejnok_02 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_02.webp", "Image")
			rejnok_03 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_03.webp", "Image")
			rejnok_04 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_04.webp", "Image")
			rejnok_05 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_05.webp", "Image")
			rejnok_06 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_06.webp", "Image")
			rejnok_07 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_07.webp", "Image")
			rejnok_08 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_08.webp", "Image")
			rejnok_09 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_09.webp", "Image")
			rejnok_10 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_10.webp", "Image")
			rejnok_11 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_11.webp", "Image")
			
			sasanka_00 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_00.webp", "Image")
			sasanka_01 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_01.webp", "Image")
			sasanka_02 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_02.webp", "Image")
			sasanka_03 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_03.webp", "Image")
			sasanka_04 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_04.webp", "Image")
			sasanka_05 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_05.webp", "Image")
			sasanka_06 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_06.webp", "Image")
			sasanka_07 = loadAsset("/externalassets/classic//level34/objects/upscaled/sasanka_07.webp", "Image")
			sepie_00 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_00.webp", "Image")
			sepie_01 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_01.webp", "Image")
			sepie_02 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_02.webp", "Image")
			sepie_03 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_03.webp", "Image")
			sepie_04 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_04.webp", "Image")
			sepie_05 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_05.webp", "Image")
			sepie_06 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_06.webp", "Image")
			sepie_07 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_07.webp", "Image")
			sepie_08 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_08.webp", "Image")
			sepie_09 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_09.webp", "Image")
			sepie_10 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_10.webp", "Image")
			sepie_11 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_11.webp", "Image")
			sepie_12 = loadAsset("/externalassets/classic//level34/objects/upscaled/sepie_12.webp", "Image")
			shell1 = loadAsset("/externalassets/classic//level35/objects/upscaled/shell1.webp", "Image")
			tecko = loadAsset("/externalassets/classic//level35/objects/upscaled/tecko.webp", "Image")
			
			
			
			kankan7= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-7.webp", "Image")
			kankan8= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-8.webp", "Image")
			kankan9= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-9.webp", "Image")
			kankan12= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-12.webp", "Image")
			kankan17= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-17.webp", "Image")
			klavir_00= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_00.webp", "Image")
			klavir_01= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_01.webp", "Image")
			klavir_02= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_02.webp", "Image")
			klavir_03= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_03.webp", "Image")
			klavir_04= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_04.webp", "Image")
			klavir_05= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_05.webp", "Image")
			klavir_06= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_06.webp", "Image")
			klavir_07= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_07.webp", "Image")
			klavir_08= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_08.webp", "Image")
			klavir_09= loadAsset("/externalassets/classic//level35/objects/upscaled/klavir_09.webp", "Image")
			
			korals= loadAsset("/externalassets/classic//level35/objects/upscaled/koral_s.webp", "Image")
			end
			
			--Manta ray
		mantaray23= loadAsset("/externalassets/objects/level35/mantaray/23.webp", "Image")
		mantaray24= loadAsset("/externalassets/objects/level35/mantaray/24.webp", "Image")
		mantaray25= loadAsset("/externalassets/objects/level35/mantaray/25.webp", "Image")
		mantaray26= loadAsset("/externalassets/objects/level35/mantaray/26.webp", "Image")
		mantaray27= loadAsset("/externalassets/objects/level35/mantaray/27.webp", "Image")
		mantaray28= loadAsset("/externalassets/objects/level35/mantaray/28.webp", "Image")
		mantaray29= loadAsset("/externalassets/objects/level35/mantaray/29.webp", "Image")
		mantaray30= loadAsset("/externalassets/objects/level35/mantaray/30.webp", "Image")
		mantaray31= loadAsset("/externalassets/objects/level35/mantaray/31.webp", "Image")
		mantaray32= loadAsset("/externalassets/objects/level35/mantaray/32.webp", "Image")
		mantaray33= loadAsset("/externalassets/objects/level35/mantaray/33.webp", "Image")
		mantaray34= loadAsset("/externalassets/objects/level35/mantaray/34.webp", "Image")
		mantaray35= loadAsset("/externalassets/objects/level35/mantaray/35.webp", "Image")
		mantaray36= loadAsset("/externalassets/objects/level35/mantaray/36.webp", "Image")
		mantaray37= loadAsset("/externalassets/objects/level35/mantaray/37.webp", "Image")
		mantaray38= loadAsset("/externalassets/objects/level35/mantaray/38.webp", "Image")
		mantaray39= loadAsset("/externalassets/objects/level35/mantaray/39.webp", "Image")
		mantaray40= loadAsset("/externalassets/objects/level35/mantaray/40.webp", "Image")
		mantaray41= loadAsset("/externalassets/objects/level35/mantaray/41.webp", "Image")
		mantaray42= loadAsset("/externalassets/objects/level35/mantaray/42.webp", "Image")
		mantaray43= loadAsset("/externalassets/objects/level35/mantaray/43.webp", "Image")
		mantaray44= loadAsset("/externalassets/objects/level35/mantaray/44.webp", "Image")
		mantaray45= loadAsset("/externalassets/objects/level35/mantaray/45.webp", "Image")
		mantaray46= loadAsset("/externalassets/objects/level35/mantaray/46.webp", "Image")
		mantaray47= loadAsset("/externalassets/objects/level35/mantaray/47.webp", "Image")
		mantaray48= loadAsset("/externalassets/objects/level35/mantaray/48.webp", "Image")
		mantaray49= loadAsset("/externalassets/objects/level35/mantaray/49.webp", "Image")
		mantaray50= loadAsset("/externalassets/objects/level35/mantaray/50.webp", "Image")
		mantaray51= loadAsset("/externalassets/objects/level35/mantaray/51.webp", "Image")
		mantaray52= loadAsset("/externalassets/objects/level35/mantaray/52.webp", "Image")
		mantaray53= loadAsset("/externalassets/objects/level35/mantaray/53.webp", "Image")
		mantaray54= loadAsset("/externalassets/objects/level35/mantaray/54.webp", "Image")
		mantaray55= loadAsset("/externalassets/objects/level35/mantaray/55.webp", "Image")
		mantaray56= loadAsset("/externalassets/objects/level35/mantaray/56.webp", "Image")
		mantaray57= loadAsset("/externalassets/objects/level35/mantaray/57.webp", "Image")
		mantaray58= loadAsset("/externalassets/objects/level35/mantaray/58.webp", "Image")
		mantaray59= loadAsset("/externalassets/objects/level35/mantaray/59.webp", "Image")
		mantaray60= loadAsset("/externalassets/objects/level35/mantaray/60.webp", "Image")
		mantaray61= loadAsset("/externalassets/objects/level35/mantaray/61.webp", "Image")
		
		
		elseif nLevel==36 then
	
		
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		if skinupscaled==false then
			level36bckcl= loadAsset("/externalassets/classic/level36/level36bck.webp", "Image")
			level36frgcl= loadAsset("/externalassets/classic/level36/level36frg.webp", "Image")
		elseif skinupscaled==true then
			level36bckcl= loadAsset("/externalassets/classic/upscaled/level36/level36bck.webp", "Image")
			level36frgcl= loadAsset("/externalassets/classic/upscaled/level36/level36frg.webp", "Image")
		end
			shader2=false
			
			--objects
				if skinupscaled==false then
					ocel1 = loadAsset("/externalassets/classic/level36/objects/1-ocel.webp", "Image")
					ocel2 = loadAsset("/externalassets/classic/level36/objects/2-ocel.webp", "Image")
					koral = loadAsset("/externalassets/classic/level36/objects/koral.webp", "Image")
					musla = loadAsset("/externalassets/classic/level36/objects/musla.webp", "Image")
					musle_troj = loadAsset("/externalassets/classic/level36/objects/musle_troj.webp", "Image")
					perla_00 = loadAsset("/externalassets/classic/level36/objects/perla_00.webp", "Image")
					perla_01 = loadAsset("/externalassets/classic/level36/objects/perla_01.webp", "Image")
					perla_02 = loadAsset("/externalassets/classic/level36/objects/perla_02.webp", "Image")
					perla_03 = loadAsset("/externalassets/classic/level36/objects/perla_03.webp", "Image")
					zeva_00 = loadAsset("/externalassets/classic/level36/objects/zeva_00.webp", "Image")
					zeva_01 = loadAsset("/externalassets/classic/level36/objects/zeva_01.webp", "Image")
					zeva_02 = loadAsset("/externalassets/classic/level36/objects/zeva_02.webp", "Image")
					zeva_03 = loadAsset("/externalassets/classic/level36/objects/zeva_03.webp", "Image")
					zeva_04 = loadAsset("/externalassets/classic/level36/objects/zeva_04.webp", "Image")
					zeva_05 = loadAsset("/externalassets/classic/level36/objects/zeva_05.webp", "Image")
					zeva_06 = loadAsset("/externalassets/classic/level36/objects/zeva_06.webp", "Image")
					zeva_07 = loadAsset("/externalassets/classic/level36/objects/zeva_07.webp", "Image")
					
			elseif skinupscaled==true then
			
					ocel1 = loadAsset("/externalassets/classic/level36/objects/upscaled/1-ocel.webp", "Image")
					ocel2 = loadAsset("/externalassets/classic/level36/objects/upscaled/2-ocel.webp", "Image")
					koral = loadAsset("/externalassets/classic/level36/objects/upscaled/koral.webp", "Image")
					musla = loadAsset("/externalassets/classic/level36/objects/upscaled/musla.webp", "Image")
					musle_troj = loadAsset("/externalassets/classic/level36/objects/upscaled/musle_troj.webp", "Image")
					perla_00 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_00.webp", "Image")
					perla_01 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_01.webp", "Image")
					perla_02 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_02.webp", "Image")
					perla_03 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_03.webp", "Image")
					zeva_00 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_00.webp", "Image")
					zeva_01 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_01.webp", "Image")
					zeva_02 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_02.webp", "Image")
					zeva_03 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_03.webp", "Image")
					zeva_04 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_04.webp", "Image")
					zeva_05 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_05.webp", "Image")
					zeva_06 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_06.webp", "Image")
					zeva_07 = loadAsset("/externalassets/classic/level36/objects/upscaled/zeva_07.webp", "Image")
			end
			
			
			
		elseif nLevel==37 then
		shader2=false
		if res=="720p" or res=="1080p" or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
				if skinupscaled==false then
				level37bckcl= loadAsset("/externalassets/classic/level37/level37bck.webp", "Image")
				level37frgcl= loadAsset("/externalassets/classic/level37/level37frg.webp", "Image")
			elseif skinupscaled==true then
				level37bckcl= loadAsset("/externalassets/classic/upscaled/level37/level37bck.webp", "Image")
				--level37bck2= loadAsset("/externalassets/levels/level37/4k/level37_bck2_4k.webp", "Image")
				--level37creature= loadAsset("/externalassets/levels/level37/4k/level37_creature4k.webp", "Image")
				level37frgcl= loadAsset("/externalassets/classic/upscaled/level37/level37frg.webp", "Image")
			end
		end
			
			--objects
				if skinupscaled==false then
			ocel3 = loadAsset("/externalassets/classic/level37/objects/3-ocel.webp", "Image")
			ocel5 = loadAsset("/externalassets/classic/level37/objects/5-ocel.webp", "Image")
			ocel14 = loadAsset("/externalassets/classic/level37/objects/14-ocel.webp", "Image")
			ocel15 = loadAsset("/externalassets/classic/level37/objects/15-ocel.webp", "Image")
			ocel16 = loadAsset("/externalassets/classic/level37/objects/16-ocel.webp", "Image")
			ocel17 = loadAsset("/externalassets/classic/level37/objects/17-ocel.webp", "Image")
			ocel20 = loadAsset("/externalassets/classic/level37/objects/20-ocel.webp", "Image")
			ocel25 = loadAsset("/externalassets/classic/level37/objects/25-ocel.webp", "Image")
			
			koral1 = loadAsset("/externalassets/classic/level37/objects/koral1.webp", "Image")
			koral2 = loadAsset("/externalassets/classic/level37/objects/koral2.webp", "Image")
			koral3 = loadAsset("/externalassets/classic/level37/objects/koral3.webp", "Image")
			koral4 = loadAsset("/externalassets/classic/level37/objects/koral4.webp", "Image")
			koral5 = loadAsset("/externalassets/classic/level37/objects/koral5.webp", "Image")
			koral6 = loadAsset("/externalassets/classic/level37/objects/koral6.webp", "Image")
			koral7 = loadAsset("/externalassets/classic/level37/objects/koral7.webp", "Image")
			koral8 = loadAsset("/externalassets/classic/level37/objects/koral8.webp", "Image")
			koral9 = loadAsset("/externalassets/classic/level37/objects/koral9.webp", "Image")
			koral10 = loadAsset("/externalassets/classic/level37/objects/koral10.webp", "Image")
			musle_troj = loadAsset("/externalassets/classic/level37/objects/musle_troj.webp", "Image")
			perla_00 = loadAsset("/externalassets/classic/level36/objects/perla_00.webp", "Image")
			perla_01 = loadAsset("/externalassets/classic/level36/objects/perla_01.webp", "Image")
			perla_02 = loadAsset("/externalassets/classic/level36/objects/perla_02.webp", "Image")
			perla_03 = loadAsset("/externalassets/classic/level36/objects/perla_03.webp", "Image")
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_02.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/rybicka_h_03.webp", "Image")
			z_00 = loadAsset("/externalassets/classic/level37/objects/z_00.webp", "Image")
			z_01 = loadAsset("/externalassets/classic/level37/objects/z_01.webp", "Image")
			z_02 = loadAsset("/externalassets/classic/level37/objects/z_02.webp", "Image")
			z_03 = loadAsset("/externalassets/classic/level37/objects/z_03.webp", "Image")
			z_04 = loadAsset("/externalassets/classic/level37/objects/z_04.webp", "Image")
			z_05 = loadAsset("/externalassets/classic/level37/objects/z_05.webp", "Image")
			z_06 = loadAsset("/externalassets/classic/level37/objects/z_06.webp", "Image")
			z_07 = loadAsset("/externalassets/classic/level37/objects/z_07.webp", "Image")
			z_08 = loadAsset("/externalassets/classic/level37/objects/z_08.webp", "Image")
			z_09 = loadAsset("/externalassets/classic/level37/objects/z_09.webp", "Image")
			z_10 = loadAsset("/externalassets/classic/level37/objects/z_10.webp", "Image")
			z_11 = loadAsset("/externalassets/classic/level37/objects/z_11.webp", "Image")
			z_12 = loadAsset("/externalassets/classic/level37/objects/z_12.webp", "Image")
			z_13 = loadAsset("/externalassets/classic/level37/objects/z_13.webp", "Image")
			z_14 = loadAsset("/externalassets/classic/level37/objects/z_14.webp", "Image")
			z_15 = loadAsset("/externalassets/classic/level37/objects/z_15.webp", "Image")
			z_16 = loadAsset("/externalassets/classic/level37/objects/z_16.webp", "Image")
			z_17 = loadAsset("/externalassets/classic/level37/objects/z_17.webp", "Image")
			z_18 = loadAsset("/externalassets/classic/level37/objects/z_18.webp", "Image")
			z_19 = loadAsset("/externalassets/classic/level37/objects/z_19.webp", "Image")
			z_20 = loadAsset("/externalassets/classic/level37/objects/z_20.webp", "Image")
			z_21 = loadAsset("/externalassets/classic/level37/objects/z_21.webp", "Image")
			z_22 = loadAsset("/externalassets/classic/level37/objects/z_22.webp", "Image")
			z_23 = loadAsset("/externalassets/classic/level37/objects/z_23.webp", "Image")
			z_24 = loadAsset("/externalassets/classic/level37/objects/z_24.webp", "Image")
			z_25 = loadAsset("/externalassets/classic/level37/objects/z_25.webp", "Image")
			z_26 = loadAsset("/externalassets/classic/level37/objects/z_26.webp", "Image")
			z_27 = loadAsset("/externalassets/classic/level37/objects/z_27.webp", "Image")
			z_28 = loadAsset("/externalassets/classic/level37/objects/z_28.webp", "Image")
			z_29 = loadAsset("/externalassets/classic/level37/objects/z_29.webp", "Image")
			z_30 = loadAsset("/externalassets/classic/level37/objects/z_30.webp", "Image")
			z_31 = loadAsset("/externalassets/classic/level37/objects/z_31.webp", "Image")
			z_32 = loadAsset("/externalassets/classic/level37/objects/z_32.webp", "Image")
			z_33 = loadAsset("/externalassets/classic/level37/objects/z_33.webp", "Image")
			z_34 = loadAsset("/externalassets/classic/level37/objects/z_34.webp", "Image")
			z_35 = loadAsset("/externalassets/classic/level37/objects/z_35.webp", "Image")
			z_36 = loadAsset("/externalassets/classic/level37/objects/z_36.webp", "Image")
			z_37 = loadAsset("/externalassets/classic/level37/objects/z_37.webp", "Image")
			z_38 = loadAsset("/externalassets/classic/level37/objects/z_38.webp", "Image")
			z_39 = loadAsset("/externalassets/classic/level37/objects/z_39.webp", "Image")
			z_40 = loadAsset("/externalassets/classic/level37/objects/z_40.webp", "Image")
			z_41 = loadAsset("/externalassets/classic/level37/objects/z_41.webp", "Image")
			z_42 = loadAsset("/externalassets/classic/level37/objects/z_42.webp", "Image")
			z_43 = loadAsset("/externalassets/classic/level37/objects/z_43.webp", "Image")
			z_44 = loadAsset("/externalassets/classic/level37/objects/z_44.webp", "Image")
			z_45 = loadAsset("/externalassets/classic/level37/objects/z_45.webp", "Image")
			elseif skinupscaled==true then	
				
			ocel3 = loadAsset("/externalassets/classic/level37/objects/upscaled/3-ocel.webp", "Image")
			ocel5 = loadAsset("/externalassets/classic/level37/objects/upscaled/5-ocel.webp", "Image")
			ocel14 = loadAsset("/externalassets/classic/level37/objects/upscaled/14-ocel.webp", "Image")
			ocel15 = loadAsset("/externalassets/classic/level37/objects/upscaled/15-ocel.webp", "Image")
			ocel16 = loadAsset("/externalassets/classic/level37/objects/upscaled/16-ocel.webp", "Image")
			ocel17 = loadAsset("/externalassets/classic/level37/objects/upscaled/17-ocel.webp", "Image")
			ocel20 = loadAsset("/externalassets/classic/level37/objects/upscaled/20-ocel.webp", "Image")
			ocel25 = loadAsset("/externalassets/classic/level37/objects/upscaled/14-ocel.webp", "Image")
			
			
			koral1 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral1.webp", "Image")
			koral2 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral2.webp", "Image")
			koral3 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral3.webp", "Image")
			koral4 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral4.webp", "Image")
			koral5 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral5.webp", "Image")
			koral6 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral6.webp", "Image")
			koral7 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral7.webp", "Image")
			koral8 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral8.webp", "Image")
			koral9 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral9.webp", "Image")
			koral10 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral10.webp", "Image")
			musle_troj = loadAsset("/externalassets/classic/level37/objects/upscaled/musle_troj.webp", "Image")
			perla_00 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_00.webp", "Image")
			perla_01 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_01.webp", "Image")
			perla_02 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_02.webp", "Image")
			perla_03 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_03.webp", "Image")
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka02.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka03.webp", "Image")
			z_00 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_00.webp", "Image")
			z_01 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_01.webp", "Image")
			z_02 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_02.webp", "Image")
			z_03 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_03.webp", "Image")
			z_04 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_04.webp", "Image")
			z_05 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_05.webp", "Image")
			z_06 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_06.webp", "Image")
			z_07 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_07.webp", "Image")
			z_08 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_08.webp", "Image")
			z_09 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_09.webp", "Image")
			z_10 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_10.webp", "Image")
			z_11 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_11.webp", "Image")
			z_12 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_12.webp", "Image")
			z_13 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_13.webp", "Image")
			z_14 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_14.webp", "Image")
			z_15 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_15.webp", "Image")
			z_16 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_16.webp", "Image")
			z_17 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_17.webp", "Image")
			z_18 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_18.webp", "Image")
			z_19 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_19.webp", "Image")
			z_20 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_20.webp", "Image")
			z_21 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_21.webp", "Image")
			z_22 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_22.webp", "Image")
			z_23 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_23.webp", "Image")
			z_24 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_24.webp", "Image")
			z_25 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_25.webp", "Image")
			z_26 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_26.webp", "Image")
			z_27 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_27.webp", "Image")
			z_28 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_28.webp", "Image")
			z_29 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_29.webp", "Image")
			z_30 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_30.webp", "Image")
			z_31 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_31.webp", "Image")
			z_32 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_32.webp", "Image")
			z_33 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_33.webp", "Image")
			z_34 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_34.webp", "Image")
			z_35 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_35.webp", "Image")
			z_36 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_36.webp", "Image")
			z_37 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_37.webp", "Image")
			z_38 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_38.webp", "Image")
			z_39 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_39.webp", "Image")
			z_40 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_40.webp", "Image")
			z_41 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_41.webp", "Image")
			z_42 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_42.webp", "Image")
			z_43 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_43.webp", "Image")
			z_44 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_44.webp", "Image")
			z_45 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_45.webp", "Image")
			
			
			end
		
		elseif nLevel==38 then
		shader2=false
			if skinupscaled==false then
				level38frgcl= loadAsset("/externalassets/classic/level38/level38frg.webp", "Image")
				level38bckcl= loadAsset("/externalassets/classic/level38/level38bck.webp", "Image")
			elseif skinupscaled==true then
				level38frgcl= loadAsset("/externalassets/classic/upscaled/level38/level38frg.webp", "Image")
				level38bckcl= loadAsset("/externalassets/classic/upscaled/level38/level38bck.webp", "Image")
			end
		
		--screen= loadAsset("/externalassets/objects/level38/screen.webp", "Image")
		--screenlight= loadAsset("/externalassets/objects/level38/screenlight.webp", "Image")
		--computerbase= loadAsset("/externalassets/objects/level38/computerbase.webp", "Image")
		
			--objects
				if skinupscaled==false then
					ocel4= loadAsset("/externalassets/classic/level38/objects/4-ocel.webp", "Image")
					cola= loadAsset("/externalassets/classic/level38/objects/cola.webp", "Image")
					kanystr= loadAsset("/externalassets/classic/level38/objects/kanystr.webp", "Image")
					computerbase= loadAsset("/externalassets/classic/level38/objects/pocitac.webp", "Image")
					keyboard= loadAsset("/externalassets/classic/level38/objects/klavesnice.webp", "Image")
					screen= loadAsset("/externalassets/classic/level38/objects/monitor.webp", "Image")
					screenlight= loadAsset("/externalassets/classic/level38/objects/screenlight.webp", "Image")
					roura_st= loadAsset("/externalassets/classic/level38/objects/roura_st.webp", "Image")
					roura_st_a= loadAsset("/externalassets/classic/level38/objects/roura_st_a.webp", "Image")
					vyvrtka= loadAsset("/externalassets/classic/level38/objects/vyvrtka.webp", "Image")
			elseif skinupscaled==true then
					ocel4= loadAsset("/externalassets/classic/level38/objects/upscaled/4-ocel.webp", "Image")
					cola= loadAsset("/externalassets/classic/level38/objects/upscaled/cola.webp", "Image")
					kanystr= loadAsset("/externalassets/classic/level38/objects/upscaled/kanystr.webp", "Image")
					keyboard= loadAsset("/externalassets/classic/level38/objects/upscaled/klavesnice.webp", "Image")
					computerbase= loadAsset("/externalassets/classic/level38/objects/upscaled/pocitac.webp", "Image")
					screen= loadAsset("/externalassets/classic/level38/objects/upscaled/monitor.webp", "Image")
					screenlight= loadAsset("/externalassets/classic/level38/objects/upscaled/screenlight.webp", "Image")
					roura_st= loadAsset("/externalassets/classic/level38/objects/upscaled/roura_st.webp", "Image")
					roura_st_a= loadAsset("/externalassets/classic/level38/objects/upscaled/roura_st_a.webp", "Image")
					vyvrtka= loadAsset("/externalassets/classic/level38/objects/upscaled/vyvrtka.webp", "Image")
			end
		
		
		elseif nLevel==39 then
		shader2=false
			if skinupscaled==false then
				level39frgcl= loadAsset("/externalassets/classic/level39/level39frg.webp", "Image")
				level39bckcl= loadAsset("/externalassets/classic/level39/level39bck.webp", "Image")
			elseif skinupscaled==true then
				level39frgcl= loadAsset("/externalassets/classic/upscaled/level39/level39frg.webp", "Image")
				level39bckcl= loadAsset("/externalassets/classic/upscaled/level39/level39bck.webp", "Image")
			end
			
			--objects
				if skinupscaled==false then
					drak_m_00 = loadAsset("/externalassets/classic/level39/objects/drak_m_00.webp", "Image")
					hvezdy2 = loadAsset("/externalassets/classic/level39/objects/hvezdy2.webp", "Image")
					krab_00 = loadAsset("/externalassets/classic/level39/objects/krab_00.webp", "Image")
					matrace = loadAsset("/externalassets/classic/level39/objects/matrace.webp", "Image")
					mocel = loadAsset("/externalassets/classic/level39/objects/mocel.webp", "Image")
					netopejr_00 = loadAsset("/externalassets/classic/level39/objects/netopejr_00.webp", "Image")
					perla_00 = loadAsset("/externalassets/classic/level39/objects/perla_00.webp", "Image")
					plz_00 = loadAsset("/externalassets/classic/level39/objects/plz_00.webp", "Image")
					zlato3 = loadAsset("/externalassets/classic/level39/objects/zlato3.webp", "Image")
					
			elseif skinupscaled==true then
					drak_m_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/drak_m_00.webp", "Image")
					hvezdy2 = loadAsset("/externalassets/classic/level39/objects/upscaled/hvezdy2.webp", "Image")
					krab_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/krab_00.webp", "Image")
					matrace = loadAsset("/externalassets/classic/level39/objects/upscaled/matrace.webp", "Image")
					mocel = loadAsset("/externalassets/classic/level39/objects/upscaled/mocel.webp", "Image")
					netopejr_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/netopejr_00.webp", "Image")
					perla_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/perla_00.webp", "Image")
					plz_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/plz_00.webp", "Image")
					zlato3 = loadAsset("/externalassets/classic/level39/objects/upscaled/zlato3.webp", "Image")
			end
		elseif nLevel>39 and nLevel<60 	then loadlevelassetsclassic40()
		elseif nLevel>59 				then loadlevelassetsclassic60()
		end

end

function loadwhale()
--whale animation
	
	whalespeed = 32 
	--whalepositionx=500 
	--whalepositiony=0
	whaleframe=0 
	whaledirection="right" 
	--whalescalex=0.6
	--whalescaley=0.6
	
	level1whalex = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale1 = loadAsset("/externalassets/levels/level1/whale/whale1.webp", "Image")
	level1whale2 = loadAsset("/externalassets/levels/level1/whale/whale2.webp", "Image")
	level1whale3 = loadAsset("/externalassets/levels/level1/whale/whale3.webp", "Image")
	level1whale4 = loadAsset("/externalassets/levels/level1/whale/whale4.webp", "Image")
	level1whale5 = loadAsset("/externalassets/levels/level1/whale/whale5.webp", "Image")
	level1whale6 = loadAsset("/externalassets/levels/level1/whale/whale6.webp", "Image")
	level1whale7 = loadAsset("/externalassets/levels/level1/whale/whale7.webp", "Image")
	level1whale8 = loadAsset("/externalassets/levels/level1/whale/whale8.webp", "Image")
	level1whale9 = loadAsset("/externalassets/levels/level1/whale/whale9.webp", "Image")
	level1whale10 = loadAsset("/externalassets/levels/level1/whale/whale10.webp", "Image")
	level1whale11 = loadAsset("/externalassets/levels/level1/whale/whale11.webp", "Image")
	level1whale12 = loadAsset("/externalassets/levels/level1/whale/whale12.webp", "Image")
	level1whale13 = loadAsset("/externalassets/levels/level1/whale/whale13.webp", "Image")
	level1whale14 = loadAsset("/externalassets/levels/level1/whale/whale14.webp", "Image")
	level1whale15 = loadAsset("/externalassets/levels/level1/whale/whale15.webp", "Image")
	level1whale16 = loadAsset("/externalassets/levels/level1/whale/whale16.webp", "Image")
	level1whale17 = loadAsset("/externalassets/levels/level1/whale/whale17.webp", "Image")
	level1whale18 = loadAsset("/externalassets/levels/level1/whale/whale18.webp", "Image")
	level1whale19 = loadAsset("/externalassets/levels/level1/whale/whale19.webp", "Image")
	level1whale20 = loadAsset("/externalassets/levels/level1/whale/whale20.webp", "Image")
	level1whale21 = loadAsset("/externalassets/levels/level1/whale/whale21.webp", "Image")
	level1whale22 = loadAsset("/externalassets/levels/level1/whale/whale22.webp", "Image")
	level1whale23 = loadAsset("/externalassets/levels/level1/whale/whale23.webp", "Image")
	level1whale24 = loadAsset("/externalassets/levels/level1/whale/whale24.webp", "Image")
	level1whale25 = loadAsset("/externalassets/levels/level1/whale/whale25.webp", "Image")
	level1whale26 = loadAsset("/externalassets/levels/level1/whale/whale26.webp", "Image")
	level1whale27 = loadAsset("/externalassets/levels/level1/whale/whale27.webp", "Image")
	level1whale28 = loadAsset("/externalassets/levels/level1/whale/whale28.webp", "Image")
	level1whale29 = loadAsset("/externalassets/levels/level1/whale/whale29.webp", "Image")
	level1whale30 = loadAsset("/externalassets/levels/level1/whale/whale30.webp", "Image")
	level1whale31 = loadAsset("/externalassets/levels/level1/whale/whale31.webp", "Image")
	level1whale32 = loadAsset("/externalassets/levels/level1/whale/whale32.webp", "Image")
end

