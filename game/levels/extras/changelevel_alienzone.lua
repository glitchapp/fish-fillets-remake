

function changelevel_alienzone()
if caustics==true then shader2=true end
		shaders = require "shaders"					-- Shadertoy shaders
		selectshadertoy("shaders/wadingcaustic.glsl")
		--selectshadertoy("shaders/waterdistortion.glsl")
blockscolor={0,0.5,1}
edgescolor={0,0.8,0}
	if nLevel==110 then
	
	blockscolor={0,0.8,0}
	edgescolor={0,0.8,0}

		
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.extras.level-110')
			palette=1
			
	elseif nLevel==111 then
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.extras.level-111')
			palette=1
			
			-- Screensavers

		
			elseif nLevel==101 then
			package.loaded['game.levels.screensaver.level-101'] = nil
			level = require ('game.levels.screensaver.level-101')
			
			talkies=false
			
						--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.6
			ffish1scaley=0.6
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			--level3frg = love.graphics.newImage("/externalassets/levels/level3/level3frg.png")
			ffish1 = love.graphics.newImage("/externalassets/levels/level3/ffish1.png")
			fishs1 = love.graphics.newImage("/externalassets/levels/level3/fishs1.png")
			fishs2 = love.graphics.newImage("/externalassets/levels/level3/fishs2.png")
			level3bck = love.graphics.newImage("/externalassets/levels/level3/level3bck1.png")
			level3bck2 = love.graphics.newImage("/externalassets/levels/level3/level3bck2.png")
			
		
			
			palette=1
			shader2=true
			
			nLevel=101
			
			
			elseif nLevel==102 then
			package.loaded['game.levels.screensaver.level-102'] = nil
			level = require ('game.levels.screensaver.level-102')
			
			palette=1
			shader2=true
			
			talkies=false
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			fishs1 = love.graphics.newImage("/externalassets/levels/level3/fishs1.png")
			fishs2 = love.graphics.newImage("/externalassets/levels/level3/fishs2.png")
			
			--background
			underwater_fantasy1= love.graphics.newImage("/externalassets/levels/level21/underwater_fantasy1.png")
			--underwater_fantasy2= love.graphics.newImage("/externalassets/levels/level21/underwater_fantasy2.png")
			level21screensaver1= love.graphics.newImage("/externalassets/levels/level21/screensaver/level21screen1.png")
			level21screensaver2= love.graphics.newImage("/externalassets/levels/level21/screensaver/level21screen2.png")
			
			
			elseif nLevel==103 then
			package.loaded['game.levels.screensaver.level-103'] = nil
			level = require ('game.levels.screensaver.level-103')
			
			palette=1
			shader2=true
			
			talkies=false
			
								--fishs
			
			fishs1speed = 20
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=400 
			fishs2positionx=300
			fishs2positiony=800 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			fishs1 = love.graphics.newImage("/externalassets/levels/level3/fishs1.png")
			fishs2 = love.graphics.newImage("/externalassets/levels/level3/fishs2.png")
			
						--background
			--level34frg= love.graphics.newImage("/externalassets/levels/level34/level34frg.png")
			level34frg2= love.graphics.newImage("/externalassets/levels/level34/screensaver/level34frg2.png")
			level34bck= love.graphics.newImage("/externalassets/levels/level34/screensaver/level34bck.png")
			level34bck2= love.graphics.newImage("/externalassets/levels/level34/screensaver/level34bck2.png")
			
				--whale sounds
			whalecalllow = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallLow(echo)9.ogg","static" )
			whalecallmid = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallMid(echo)8.ogg","static" )

			loadwhale()
			whalescalex=1
			whalescaley=1
			whalepositionx=0
			whalepositiony=200

			
		end
		
		
		--load everything
		
		loadvoices2()				-- load voices		
		loadlevelmusic2() 			-- load music
		loadlevelsounds2()			-- load sounds
		loadlevelassets2()			-- load assets
		--loadwhale()					-- load whale animation
		loadsubtitles2()				-- load subtitles
		loadleveltitle2()			-- load level title

		
		
		
		-- Screensavers
		
		if android==true then shader2=false touchinterfaceison=true end
			love.graphics.clear()
			pb:load (level)
			createcanvas()
			
			pb:drawMap ()
            --my personal colors
			
			--draw_level()
			--draw_gamegui()
			--draw_levelnumber()

			if debugmode==true then pb:drawBackgroundGrid () end
			--draw_wallborders()
			--love.graphics.setCanvas()
end


