local level = {}


level.name = 'level-75'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,3,3,3,3,3,3,1,3,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,1,1,1,1,1},
{1,3,0,3,0,3,0,3,1,3,0,0,0,3,0,0,0,0,0,0,3,0,0,0,3,1,1,1,1,1,1,1,1,1,1,1,1},
{1,3,0,3,0,3,0,3,1,3,3,3,0,3,3,0,0,0,0,0,0,0,0,3,3,3,3,3,3,1,1,1,1,1,1,1,1},
{1,3,0,3,0,3,0,3,3,3,3,3,0,0,0,0,0,0,0,0,3,3,3,3,3,0,0,0,0,3,3,3,3,1,1,1,1},
{1,3,0,3,0,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1},
{3,3,0,3,0,3,0,3,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{6,6,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,3,1},
{6,6,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,3,1},
{6,3,0,3,0,3,0,3,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{6,3,0,3,0,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,3},
{1,1,3,1,3,3,3,0,0,0,0,0,0,3,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{1,1,1,1,3,3,0,0,0,0,0,0,3,3,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{1,1,1,3,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,1,1},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,1,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,1,1,1,1},
{0,0,0,0,0,0,0,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,3,1,1,1,1,3,1,1,1,1},
{3,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,3,1,1,3,0,3,1,1,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,1,3,0,3,1,1,3,0,3,1,1,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,1,3,0,3,1,1,3,0,3,1,1,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,1,3,0,3,1,1,3,0,3,1,1,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,3,3,0,3,3,3,3,0,3,1,1,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
{1,1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
{1,1,1,1,1,1,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,1,1,1,1,1,1},
{1,1,1,1,1,1,1,1,1,1,1,3,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,1},
{1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	
	{name = 'stone',
	heavy = false,
	x = 15,
	y = 1,
	form = 
		{
			{0,1,1,1,1,1,1,1,1},
			{1,1,0,0,0,0,0,1,1},
			{0,1,0,0,0,1,1,1,0},
			{0,1,0,0,0,0,0,0,0},
		},
	},
	
	{name = 'barrellay',
	heavy = false,
	x = 28,
	y = 13,
	form = 
		{
			{1,1,1},
			{1,1,1},
		},
	},
	
	{name = 'broom',
	heavy = false,
	x = 21,
	y = 14,
	form = 
		{
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{1,1,1},
		},
	},
	
	{name = 'key',
	heavy = false,
	x = 13,
	y = 30,
	form = 
		{
			{0,0,0,0,1,0,0,0,1,0,0,0,0,1},
			{0,0,0,0,1,0,0,0,1,0,0,0,0,1},
			{0,0,0,0,1,0,0,0,1,0,0,0,0,1},
			{1,1,1,0,1,0,0,0,1,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,1,1,0,0,0,0,0,0,0,0,0,0,0},
		},
	},
	
	{name = 'pearl',
	heavy = false,
	x = 31,
	y = 13,
	form = 
		{
			{1},
		},
	},
	
	{name = 'steelangle',
	heavy = false,
	x = 11,
	y = 2,
	form = 
		{
			{1,1,1},
			{0,0,1},
			{0,0,1},
		},
	},
	
	{name = 'stones',
	heavy = false,
	x = 4,
	y = 30,
	form = 
		{
			{1},
		},
	},
	
	{name = 'leftsteel',
	heavy = true,
	x = 3,
	y = 2,
	form = 
		{
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
		},
	},
	
	{name = 'barrelstand',
	heavy = false,
	x = 29,
	y = 31,
	form = 
		{
			{1,1},
			{1,1},
			{1,1},
		},
	},
	
	{name = 'icicle',
	heavy = false,
	x = 18,
	y = 2,
	form = 
		{
			{1,1,1},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
		},
	},
	
	{name = 'middlesteel',
	heavy = true,
	x = 5,
	y = 4,
	form = 
		{
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			
		},
	},
	
	{name = 'locksteel',
	heavy = false,
	x = 2,
	y = 5,
	form = 
		{
			{0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,0,0,1},
			{1,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0},
		},
	},
	
	{name = 'animal',
	heavy = false,
	x = 11,
	y = 16,
	form = 
		{
			{1,1,1,0},
			{0,1,1,1},
		},
	},
	
	{name = 'canister',
	heavy = false,
	x = 29,
	y = 29,
	form = 
		{
			{1,1,1},
			{0,1,1},
		},
	},
	
	{name = 'rightsteel',
	heavy = true,
	x = 7,
	y = 3,
	form = 
		{
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
		},
	},
	
	{name = 'ladder',
	heavy = false,
	x = 16,
	y = 6,
	form = 
		{
			{1},
			{1},
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'cactus',
	heavy = false,
	x = 25,
	y = 25,
	form = 
		{
			{0,0,0,1,0,0},
			{0,0,0,1,0,0},
			{0,0,0,1,0,0},
			{0,0,0,1,0,0},
			{1,1,1,1,0,0},
			{0,0,0,1,0,1},
			{0,0,0,1,1,1},
		},
	},
	
	{name = 'hammer',
	heavy = false,
	x = 15,
	y = 28,
	form = 
		{
			{1,1,1,0},
			{1,1,1,1},
			{0,1,0,0},
			{0,1,0,0},
		},
	},
--[[		
	{name = 'stones',
	heavy = false,
	x = 15,
	y = 1,
	form = 
		{
			{1},
		},
	},
	--]]
	{name = '3steel',
	heavy = false,
	x = 4,
	y = 7,
	form = 
		{
			{1,0,1,0,1},
			{1,0,1,0,1},
		},
	},
--[[
	{name = 'stones',
	heavy = false,
	x = 32,
	y = 13,
	form = 
		{
			{1},
		},
	},
--]]
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 9,
	y = 18,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 7,
	y = 20,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
