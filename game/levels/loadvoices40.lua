function loadvoices40()
	if nLevel==40 then
			if language=="en" then
				if accent=="br" then
					itsquitecomfortable  = love.audio.newSource( "/externalassets/dialogs/level40/en/1itsquitecomfortable.ogg","stream" )
					someunderwater  = love.audio.newSource( "/externalassets/dialogs/level40/en/3someunderwater.ogg","stream" )
					donttalk  = love.audio.newSource( "/externalassets/dialogs/level40/en/8donttalk.ogg","stream" )
					pleasenojokes  = love.audio.newSource( "/externalassets/dialogs/level40/en/9pleasenojokes.ogg","stream" )
					yesiknow  = love.audio.newSource( "/externalassets/dialogs/level40/en/10yesiknow.ogg","stream" )
					stopit  = love.audio.newSource( "/externalassets/dialogs/level40/en/12stopit.ogg","stream" )
					maybeitspraysair  = love.audio.newSource( "/externalassets/dialogs/level40/en/14maybeitspraysair.ogg","stream" )
					hardly  = love.audio.newSource( "/externalassets/dialogs/level40/en/16hardly.ogg","stream" )
					weshould  = love.audio.newSource( "/externalassets/dialogs/level40/en/18weshould.ogg","stream" )
					thismustbe  = love.audio.newSource( "/externalassets/dialogs/level40/en/21thismustbe.ogg","stream" )
					heseems  = love.audio.newSource( "/externalassets/dialogs/level40/en/23heseems.ogg","stream" )
					
				elseif accent=="us" then
					itsquitecomfortable  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/1itsquitecomfortable.ogg","stream" )
					someunderwater  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/3someunderwater.ogg","stream" )
					donttalk  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/8donttalk.ogg","stream" )
					pleasenojokes  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/9pleasenojokes.ogg","stream" )
					yesiknow  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/10yesiknow.ogg","stream" )
					stopit  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/12stopit.ogg","stream" )
					maybeitspraysair  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/14maybeitspraysair.ogg","stream" )
					hardly  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/16hardly.ogg","stream" )
					weshould  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/18weshould.ogg","stream" )
					thismustbe  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/21thismustbe.ogg","stream" )
					heseems  = love.audio.newSource( "/externalassets/dialogs/level40/en-us/23heseems.ogg","stream" )
				end
			elseif language=="fr" then
					itsquitecomfortable  = love.audio.newSource( "/externalassets/dialogs/level40/fr/1itsquitecomfortable.ogg","stream" )
					someunderwater  = love.audio.newSource( "/externalassets/dialogs/level40/fr/3someunderwater.ogg","stream" )
					donttalk  = love.audio.newSource( "/externalassets/dialogs/level40/fr/8donttalk.ogg","stream" )
					pleasenojokes  = love.audio.newSource( "/externalassets/dialogs/level40/fr/9pleasenojokes.ogg","stream" )
					yesiknow  = love.audio.newSource( "/externalassets/dialogs/level40/fr/10yesiknow.ogg","stream" )
					stopit  = love.audio.newSource( "/externalassets/dialogs/level40/fr/12stopit.ogg","stream" )
					maybeitspraysair  = love.audio.newSource( "/externalassets/dialogs/level40/fr/14maybeitspraysair.ogg","stream" )
					hardly  = love.audio.newSource( "/externalassets/dialogs/level40/fr/16hardly.ogg","stream" )
					weshould  = love.audio.newSource( "/externalassets/dialogs/level40/fr/18weshould.ogg","stream" )
					thismustbe  = love.audio.newSource( "/externalassets/dialogs/level40/fr/21thismustbe.ogg","stream" )
					heseems  = love.audio.newSource( "/externalassets/dialogs/level40/fr/23heseems.ogg","stream" )
			
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
				
					itsquitecomfortable  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/1itsquitecomfortable.ogg","stream" )
					someunderwater  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/3someunderwater.ogg","stream" )
					donttalk  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/8donttalk.ogg","stream" )
					pleasenojokes  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/9pleasenojokes.ogg","stream" )
					yesiknow  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/10yesiknow.ogg","stream" )
					stopit  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/12stopit.ogg","stream" )
					maybeitspraysair  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/14maybeitspraysair.ogg","stream" )
					hardly  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/16hardly.ogg","stream" )
					weshould  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/18weshould.ogg","stream" )
					thismustbe  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/21thismustbe.ogg","stream" )
					heseems  = love.audio.newSource( "/externalassets/dialogs/level40/es-la/23heseems.ogg","stream" )
				end
			end
			
			if language2=="en" then
				if accent=="br" then
				elseif accent=="us" then
					whatdoyouthinkplumbman = love.audio.newSource( "/externalassets/dialogs/level40/en-us/2whatdoyouthink.ogg","stream" )
					wecallthem = love.audio.newSource( "/externalassets/dialogs/level40/en-us/4wecallthem.ogg","stream" )
					didyounotice = love.audio.newSource( "/externalassets/dialogs/level40/en-us/5didyounotice.ogg","stream" )
					dontyouthink = love.audio.newSource( "/externalassets/dialogs/level40/en-us/6dontyouthink.ogg","stream" )
					isntitstrange = love.audio.newSource( "/externalassets/dialogs/level40/en-us/7isntitstrange.ogg","stream" )
					yeahbutwhy = love.audio.newSource( "/externalassets/dialogs/level40/en-us/11yeahbutwhy.ogg","stream" )
					iveseenmany = love.audio.newSource( "/externalassets/dialogs/level40/en-us/13iveseenmany.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level40/en-us/15doyouthink.ogg","stream" )
					nowthisisreal = love.audio.newSource( "/externalassets/dialogs/level40/en-us/17nowthisisreal.ogg","stream" )
					anitdoesnt = love.audio.newSource( "/externalassets/dialogs/level40/en-us/19anitdoesnt.ogg","stream" )
					helloinside = love.audio.newSource( "/externalassets/dialogs/level40/en-us/20helloinside.ogg","stream" )
					iwouldlove = love.audio.newSource( "/externalassets/dialogs/level40/en-us/22iwouldlove.ogg","stream" )
				end
			elseif language2=="pl" then
					whatdoyouthinkplumbman = love.audio.newSource( "/externalassets/dialogs/level40/pl/2whatdoyouthink.ogg","stream" )
					wecallthem = love.audio.newSource( "/externalassets/dialogs/level40/pl/4wecallthem.ogg","stream" )
					didyounotice = love.audio.newSource( "/externalassets/dialogs/level40/pl/5didyounotice.ogg","stream" )
					dontyouthink = love.audio.newSource( "/externalassets/dialogs/level40/pl/6dontyouthink.ogg","stream" )
					isntitstrange = love.audio.newSource( "/externalassets/dialogs/level40/pl/7isntitstrange.ogg","stream" )
					yeahbutwhy = love.audio.newSource( "/externalassets/dialogs/level40/pl/11yeahbutwhy.ogg","stream" )
					iveseenmany = love.audio.newSource( "/externalassets/dialogs/level40/pl/13iveseenmany.ogg","stream" )
					doyouthink = love.audio.newSource( "/externalassets/dialogs/level40/pl/15doyouthink.ogg","stream" )
					nowthisisreal = love.audio.newSource( "/externalassets/dialogs/level40/pl/17nowthisisreal.ogg","stream" )
					anitdoesnt = love.audio.newSource( "/externalassets/dialogs/level40/pl/19anitdoesnt.ogg","stream" )
					helloinside = love.audio.newSource( "/externalassets/dialogs/level40/pl/20helloinside.ogg","stream" )
					iwouldlove = love.audio.newSource( "/externalassets/dialogs/level40/pl/22iwouldlove.ogg","stream" )
			end
					--fish 1
					itsquitecomfortable:setEffect('myEffect')
					someunderwater:setEffect('myEffect')
					donttalk:setEffect('myEffect')
					pleasenojokes:setEffect('myEffect')
					yesiknow:setEffect('myEffect')
					stopit:setEffect('myEffect')
					maybeitspraysair:setEffect('myEffect')
					hardly:setEffect('myEffect')
					weshould:setEffect('myEffect')
					thismustbe:setEffect('myEffect')
					heseems:setEffect('myEffect')
					
					--fish 2
					whatdoyouthinkplumbman:setEffect('myEffect')
					wecallthem:setEffect('myEffect')
					didyounotice:setEffect('myEffect')
					dontyouthink:setEffect('myEffect')
					isntitstrange:setEffect('myEffect')
					yeahbutwhy:setEffect('myEffect')
					iveseenmany:setEffect('myEffect')
					doyouthink:setEffect('myEffect')
					nowthisisreal:setEffect('myEffect')
					anitdoesnt:setEffect('myEffect')
					helloinside:setEffect('myEffect')
					iwouldlove:setEffect('myEffect')
			
			elseif nLevel==41 then
		if language=="en" then
					if accent=="br" then
					
					doesntthis = love.audio.newSource( "/externalassets/dialogs/level41/en/1doesntthis.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level41/en/4canyousee.ogg","stream" )
					nowthiswasajoke = love.audio.newSource( "/externalassets/dialogs/level41/en/7nowthiswasajoke.ogg","stream" )
					butabeautifullywavy = love.audio.newSource( "/externalassets/dialogs/level41/en/9butabeautifullywavy.ogg","stream" )
					moveon = love.audio.newSource( "/externalassets/dialogs/level41/en/10moveon.ogg","stream" )
					andwhatabout = love.audio.newSource( "/externalassets/dialogs/level41/en/11andwhatabout.ogg","stream" )
					aminotenough = love.audio.newSource( "/externalassets/dialogs/level41/en/12aminotenough.ogg","stream" )

					
				elseif accent=="us" then
					doesntthis = love.audio.newSource( "/externalassets/dialogs/level41/en-us/1doesntthis.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level41/en-us/4canyousee.ogg","stream" )
					nowthiswasajoke = love.audio.newSource( "/externalassets/dialogs/level41/en-us/7nowthiswasajoke.ogg","stream" )
					butabeautifullywavy = love.audio.newSource( "/externalassets/dialogs/level41/en-us/9butabeautifullywavy.ogg","stream" )
					moveon = love.audio.newSource( "/externalassets/dialogs/level41/en-us/10moveon.ogg","stream" )
					andwhatabout = love.audio.newSource( "/externalassets/dialogs/level41/en-us/11andwhatabout.ogg","stream" )
					aminotenough = love.audio.newSource( "/externalassets/dialogs/level41/en-us/12aminotenough.ogg","stream" )

				end
		elseif language=="fr" then
		
					doesntthis = love.audio.newSource( "/externalassets/dialogs/level41/fr/1doesntthis.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level41/fr/4canyousee.ogg","stream" )
					nowthiswasajoke = love.audio.newSource( "/externalassets/dialogs/level41/fr/7nowthiswasajoke.ogg","stream" )
					butabeautifullywavy = love.audio.newSource( "/externalassets/dialogs/level41/fr/9butabeautifullywavy.ogg","stream" )
					moveon = love.audio.newSource( "/externalassets/dialogs/level41/fr/10moveon.ogg","stream" )
					andwhatabout = love.audio.newSource( "/externalassets/dialogs/level41/fr/11andwhatabout.ogg","stream" )
					aminotenough = love.audio.newSource( "/externalassets/dialogs/level41/fr/12aminotenough.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					doesntthis = love.audio.newSource( "/externalassets/dialogs/level41/es-la/1doesntthis.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level41/es-la/4canyousee.ogg","stream" )
					nowthiswasajoke = love.audio.newSource( "/externalassets/dialogs/level41/es-la/7nowthiswasajoke.ogg","stream" )
					butabeautifullywavy = love.audio.newSource( "/externalassets/dialogs/level41/es-la/9butabeautifullywavy.ogg","stream" )
					moveon = love.audio.newSource( "/externalassets/dialogs/level41/es-la/10moveon.ogg","stream" )
					andwhatabout = love.audio.newSource( "/externalassets/dialogs/level41/es-la/11andwhatabout.ogg","stream" )
					aminotenough = love.audio.newSource( "/externalassets/dialogs/level41/es-la/12aminotenough.ogg","stream" )
				end
		end
		
		if language2=="en" then
			if accent=="br" then
			elseif accent=="us" then
				ithinkitlooks  = love.audio.newSource( "/externalassets/dialogs/level41/en-us/2ithinkitlooks.ogg","stream" )
				ithinkitlooksinflatable  = love.audio.newSource( "/externalassets/dialogs/level41/en-us/3ithinkitlooksinflatable.ogg","stream" )
				whatdoyoumean  = love.audio.newSource( "/externalassets/dialogs/level41/en-us/5whatdoyoumean.ogg","stream" )
				trytoopenthat  = love.audio.newSource( "/externalassets/dialogs/level41/en-us/6trytoopenthat.ogg","stream" )
				andthereisnteven  = love.audio.newSource( "/externalassets/dialogs/level41/en-us/8andthereisnteven.ogg","stream" )
				
			end
		elseif language2=="pl" then
				ithinkitlooks  = love.audio.newSource( "/externalassets/dialogs/level41/pl/2ithinkitlooks.ogg","stream" )
				ithinkitlooksinflatable  = love.audio.newSource( "/externalassets/dialogs/level41/pl/3ithinkitlooksinflatable.ogg","stream" )
				whatdoyoumean  = love.audio.newSource( "/externalassets/dialogs/level41/pl/5whatdoyoumean.ogg","stream" )
				trytoopenthat  = love.audio.newSource( "/externalassets/dialogs/level41/pl/6trytoopenthat.ogg","stream" )
				andthereisnteven  = love.audio.newSource( "/externalassets/dialogs/level41/pl/8andthereisnteven.ogg","stream" )
		end
					--fish 1
					doesntthis:setEffect('myEffect')
					canyousee:setEffect('myEffect')
					nowthiswasajoke:setEffect('myEffect')
					butabeautifullywavy:setEffect('myEffect')
					moveon:setEffect('myEffect')
					andwhatabout:setEffect('myEffect')
					aminotenough:setEffect('myEffect')
		
					--fish 2
					
					ithinkitlooks:setEffect('myEffect')
					ithinkitlooksinflatable:setEffect('myEffect')
					whatdoyoumean:setEffect('myEffect')
					trytoopenthat:setEffect('myEffect')
					andthereisnteven:setEffect('myEffect')
		
		
			
	elseif nLevel==42 then
			if language=="en" then
				if accent=="br" then
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level42/en/2itsinteresting.ogg","stream" )
					imsure = love.audio.newSource( "/externalassets/dialogs/level42/en/3imsure.ogg","stream" )
					itstaxing = love.audio.newSource( "/externalassets/dialogs/level42/en/9itstaxing.ogg","stream" )
					itsquite = love.audio.newSource( "/externalassets/dialogs/level42/en/10itsquite.ogg","stream" )
					itwasalot = love.audio.newSource( "/externalassets/dialogs/level42/en/11itwasalot.ogg","stream" )
					whatnonsense = love.audio.newSource( "/externalassets/dialogs/level42/en/14whatnonsense.ogg","stream" )
				elseif accent=="us" then
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level42/en-us/2itsinteresting.ogg","stream" )
					imsure = love.audio.newSource( "/externalassets/dialogs/level42/en-us/3imsure.ogg","stream" )
					itstaxing = love.audio.newSource( "/externalassets/dialogs/level42/en-us/9itstaxing.ogg","stream" )
					itsquite = love.audio.newSource( "/externalassets/dialogs/level42/en-us/10itsquite.ogg","stream" )
					itwasalot = love.audio.newSource( "/externalassets/dialogs/level42/en-us/11itwasalot.ogg","stream" )
					whatnonsense = love.audio.newSource( "/externalassets/dialogs/level42/en-us/14whatnonsense.ogg","stream" )
				end
			elseif language=="fr" then
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level42/fr/2itsinteresting.ogg","stream" )
					imsure = love.audio.newSource( "/externalassets/dialogs/level42/fr/3imsure.ogg","stream" )
					itstaxing = love.audio.newSource( "/externalassets/dialogs/level42/fr/9itstaxing.ogg","stream" )
					itsquite = love.audio.newSource( "/externalassets/dialogs/level42/fr/10itsquite.ogg","stream" )
					itwasalot = love.audio.newSource( "/externalassets/dialogs/level42/fr/11itwasalot.ogg","stream" )
					whatnonsense = love.audio.newSource( "/externalassets/dialogs/level42/fr/14whatnonsense.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					itsinteresting = love.audio.newSource( "/externalassets/dialogs/level42/es-la/2itsinteresting.ogg","stream" )
					imsure = love.audio.newSource( "/externalassets/dialogs/level42/es-la/3imsure.ogg","stream" )
					itstaxing = love.audio.newSource( "/externalassets/dialogs/level42/es-la/9itstaxing.ogg","stream" )
					itsquite = love.audio.newSource( "/externalassets/dialogs/level42/es-la/10itsquite.ogg","stream" )
					itwasalot = love.audio.newSource( "/externalassets/dialogs/level42/es-la/11itwasalot.ogg","stream" )
					whatnonsense = love.audio.newSource( "/externalassets/dialogs/level42/es-la/14whatnonsense.ogg","stream" )
				end
			end
			
			if language2=="en" then
				if accent=="br" then
				elseif accent=="us" then
					lookwhatwe = love.audio.newSource( "/externalassets/dialogs/level42/en-us/1lookwhatwe.ogg","stream" )
					youareprobablywrong = love.audio.newSource( "/externalassets/dialogs/level42/en-us/4youareprobablywrong.ogg","stream" )
					lookatthatthing = love.audio.newSource( "/externalassets/dialogs/level42/en-us/5lookatthatthing.ogg","stream" )
					lookatthatpld = love.audio.newSource( "/externalassets/dialogs/level42/en-us/6lookatthatpld.ogg","stream" )
					ohmythisis = love.audio.newSource( "/externalassets/dialogs/level42/en-us/7ohmythisis.ogg","stream" )
					repulsive = love.audio.newSource( "/externalassets/dialogs/level42/en-us/8repulsive.ogg","stream" )
					butitpaidoff = love.audio.newSource( "/externalassets/dialogs/level42/en-us/12butitpaidoff.ogg","stream" )
					justimagineitwas = love.audio.newSource( "/externalassets/dialogs/level42/en-us/12justimagineitwas.ogg","stream" )
				end
			elseif language2=="pl" then
					lookwhatwe = love.audio.newSource( "/externalassets/dialogs/level42/pl/1lookwhatwe.ogg","stream" )
					youareprobablywrong = love.audio.newSource( "/externalassets/dialogs/level42/pl/4youareprobablywrong.ogg","stream" )
					lookatthatthing = love.audio.newSource( "/externalassets/dialogs/level42/pl/5lookatthatthing.ogg","stream" )
					lookatthatpld = love.audio.newSource( "/externalassets/dialogs/level42/pl/6lookatthatpld.ogg","stream" )
					ohmythisis = love.audio.newSource( "/externalassets/dialogs/level42/pl/7ohmythisis.ogg","stream" )
					repulsive = love.audio.newSource( "/externalassets/dialogs/level42/pl/8repulsive.ogg","stream" )
					butitpaidoff = love.audio.newSource( "/externalassets/dialogs/level42/pl/12butitpaidoff.ogg","stream" )
					justimagineitwas = love.audio.newSource( "/externalassets/dialogs/level42/pl/12justimagineitwas.ogg","stream" )
			end
				--fish 1
					itsinteresting:setEffect('myEffect')
					imsure:setEffect('myEffect')
					itstaxing:setEffect('myEffect')
					itsquite:setEffect('myEffect')
					itwasalot:setEffect('myEffect')
					whatnonsense:setEffect('myEffect')
					
				--fish 2
				
					lookwhatwe:setEffect('myEffect')
					youareprobablywrong:setEffect('myEffect')
					lookatthatthing:setEffect('myEffect')
					lookatthatpld:setEffect('myEffect')
					ohmythisis:setEffect('myEffect')
					repulsive:setEffect('myEffect')
					butitpaidoff:setEffect('myEffect')
					justimagineitwas:setEffect('myEffect')
	
		elseif nLevel==43 then
		
			if language=="en" then
				
				imjogging = love.audio.newSource( "/externalassets/dialogs/level43/en/22imjogging.ogg","stream" )
			if accent=="br" then
				thisone = love.audio.newSource( "/externalassets/dialogs/level43/en/2thisone.ogg","stream" )
				butno = love.audio.newSource( "/externalassets/dialogs/level43/en/3butno.ogg","stream" )
				noithink = love.audio.newSource( "/externalassets/dialogs/level43/en/4noithink.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level43/en/5look.ogg","stream" )
				itsworse = love.audio.newSource( "/externalassets/dialogs/level43/en/9itsworse.ogg","stream" )
				thereissomuch = love.audio.newSource( "/externalassets/dialogs/level43/en/10thereissomuch.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/en/11canyousee.ogg","stream" )
				itsold = love.audio.newSource( "/externalassets/dialogs/level43/en/13itsold.ogg","stream" )
				hesprobably = love.audio.newSource( "/externalassets/dialogs/level43/en/15hesprobably.ogg","stream" )
				thecorpses = love.audio.newSource( "/externalassets/dialogs/level43/en/17thecorpses.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/en/18canyousee.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level43/en/19everythingis.ogg","stream" )
				justlookatit = love.audio.newSource( "/externalassets/dialogs/level43/en/21justlookatit.ogg","stream" )
			elseif accent=="us" then
				thisone = love.audio.newSource( "/externalassets/dialogs/level43/en-us/2thisone.ogg","stream" )
				butno = love.audio.newSource( "/externalassets/dialogs/level43/en-us/3butno.ogg","stream" )
				noithink = love.audio.newSource( "/externalassets/dialogs/level43/en-us/4noithink.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level43/en-us/5look.ogg","stream" )
				itsworse = love.audio.newSource( "/externalassets/dialogs/level43/en-us/9itsworse.ogg","stream" )
				thereissomuch = love.audio.newSource( "/externalassets/dialogs/level43/en-us/10thereissomuch.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/en-us/11canyousee.ogg","stream" )
				itsold = love.audio.newSource( "/externalassets/dialogs/level43/en-us/13itsold.ogg","stream" )
				hesprobably = love.audio.newSource( "/externalassets/dialogs/level43/en-us/15hesprobably.ogg","stream" )
				thecorpses = love.audio.newSource( "/externalassets/dialogs/level43/en-us/17thecorpses.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/en-us/18canyousee.ogg","stream" )
				everything = love.audio.newSource( "/externalassets/dialogs/level43/en-us/19everythingis.ogg","stream" )
				justlookatit = love.audio.newSource( "/externalassets/dialogs/level43/en-us/21justlookatit.ogg","stream" )
			end
			
		
			elseif language=="fr" then
				
				imjogging = love.audio.newSource( "/externalassets/dialogs/level43/fr/22imjogging.ogg","stream" )
				
		
			
				thisone = love.audio.newSource( "/externalassets/dialogs/level43/fr/2thisone.ogg","stream" )
				butno = love.audio.newSource( "/externalassets/dialogs/level43/fr/3butno.ogg","stream" )
				noithink = love.audio.newSource( "/externalassets/dialogs/level43/fr/4noithink.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level43/fr/5look.ogg","stream" )
				itsworse = love.audio.newSource( "/externalassets/dialogs/level43/fr/9itsworse.ogg","stream" )
				thereissomuch = love.audio.newSource( "/externalassets/dialogs/level43/fr/10thereissomuch.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/fr/11canyousee.ogg","stream" )
				itsold = love.audio.newSource( "/externalassets/dialogs/level43/fr/13itsold.ogg","stream" )
				hesprobably = love.audio.newSource( "/externalassets/dialogs/level43/fr/15hesprobably.ogg","stream" )
				thecorpses = love.audio.newSource( "/externalassets/dialogs/level43/fr/17thecorpses.ogg","stream" )
				canyousee = love.audio.newSource( "/externalassets/dialogs/level43/fr/18canyousee.ogg","stream" )
				everythingis = love.audio.newSource( "/externalassets/dialogs/level43/fr/19everythingis.ogg","stream" )
				justlookatit = love.audio.newSource( "/externalassets/dialogs/level43/fr/21justlookatit.ogg","stream" )
			elseif language=="cs" then
				
				imjogging = love.audio.newSource( "/externalassets/dialogs/level43/cs/22imjogging.ogg","stream" )
			
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					thisone = love.audio.newSource( "/externalassets/dialogs/level43/es-la/2thisone.ogg","stream" )
					butno = love.audio.newSource( "/externalassets/dialogs/level43/es-la/3butno.ogg","stream" )
					noithink = love.audio.newSource( "/externalassets/dialogs/level43/es-la/4noithink.ogg","stream" )
					look = love.audio.newSource( "/externalassets/dialogs/level43/es-la/5look.ogg","stream" )
					itsworse = love.audio.newSource( "/externalassets/dialogs/level43/es-la/9itsworse.ogg","stream" )
					thereissomuch = love.audio.newSource( "/externalassets/dialogs/level43/es-la/10thereissomuch.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level43/es-la/11canyousee.ogg","stream" )
					itsold = love.audio.newSource( "/externalassets/dialogs/level43/es-la/13itsold.ogg","stream" )
					hesprobably = love.audio.newSource( "/externalassets/dialogs/level43/es-la/15hesprobably.ogg","stream" )
					thecorpses = love.audio.newSource( "/externalassets/dialogs/level43/es-la/17thecorpses.ogg","stream" )
					canyousee = love.audio.newSource( "/externalassets/dialogs/level43/es-la/18canyousee.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level43/es-la/19everythingis.ogg","stream" )
					justlookatit = love.audio.newSource( "/externalassets/dialogs/level43/es-la/21justlookatit.ogg","stream" )
				end
							
			end
				
			if language2=="en" then
					if accent=="br" then
				elseif accent=="us" then
					wehavebeen = love.audio.newSource( "/externalassets/dialogs/level43/en-us/1wehavebeen.ogg","stream" )
					doyoufeelallright = love.audio.newSource( "/externalassets/dialogs/level43/en-us/6doyoufeelallright.ogg","stream" )
					thisisapile = love.audio.newSource( "/externalassets/dialogs/level43/en-us/7thisisapile.ogg","stream" )
					thismustbeawreck = love.audio.newSource( "/externalassets/dialogs/level43/en-us/8thismustbeawreck.ogg","stream" )
					theonewiththat = love.audio.newSource( "/externalassets/dialogs/level43/en-us/12theonewiththat.ogg","stream" )
					andwhatdoeshedo = love.audio.newSource( "/externalassets/dialogs/level43/en-us/14andwhatdoeshedo.ogg","stream" )
					orpainted = love.audio.newSource( "/externalassets/dialogs/level43/en-us/18orpainted.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level43/en-us/20why.ogg","stream" )
				end
			elseif language2=="pl" then
					wehavebeen = love.audio.newSource( "/externalassets/dialogs/level43/pl/1wehavebeen.ogg","stream" )
					doyoufeelallright = love.audio.newSource( "/externalassets/dialogs/level43/pl/6doyoufeelallright.ogg","stream" )
					thisisapile = love.audio.newSource( "/externalassets/dialogs/level43/pl/7thisisapile.ogg","stream" )
					thismustbeawreck = love.audio.newSource( "/externalassets/dialogs/level43/pl/8thismustbeawreck.ogg","stream" )
					theonewiththat = love.audio.newSource( "/externalassets/dialogs/level43/pl/12theonewiththat.ogg","stream" )
					andwhatdoeshedo = love.audio.newSource( "/externalassets/dialogs/level43/pl/14andwhatdoeshedo.ogg","stream" )
					orpainted = love.audio.newSource( "/externalassets/dialogs/level43/pl/18orpainted.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level43/pl/20why.ogg","stream" )
			end
				
				--fish 1
				thisone:setEffect('myEffect')
				butno:setEffect('myEffect')
				noithink:setEffect('myEffect')
				look:setEffect('myEffect')
				itsworse:setEffect('myEffect')
				thereissomuch:setEffect('myEffect')
				canyousee:setEffect('myEffect')
				itsold:setEffect('myEffect')
				hesprobably:setEffect('myEffect')
				thecorpses:setEffect('myEffect')
				canyousee:setEffect('myEffect')
				everything:setEffect('myEffect')
				justlookatit:setEffect('myEffect')
				imjogging:setEffect('myEffect')
				
				--fish 2
					wehavebeen:setEffect('myEffect')
					doyoufeelallright:setEffect('myEffect')
					thisisapile:setEffect('myEffect')
					thismustbeawreck:setEffect('myEffect')
					theonewiththat:setEffect('myEffect')
					andwhatdoeshedo:setEffect('myEffect')
					orpainted:setEffect('myEffect')
					why:setEffect('myEffect')
				
		elseif nLevel==44 then
			loadBorderDialogs()
		
			if language=="en" then
				--end
				 greetings = love.audio.newSource( "/externalassets/ends/7dump/audios/en/1greetings.ogg","stream" )
				 yourssincerely = love.audio.newSource( "/externalassets/ends/7dump/audios/en/2yourssincerely.ogg","stream" )
				 agents = love.audio.newSource( "/externalassets/ends/7dump/audios/en/3agents.ogg","stream" )
				 itsgreattobehere = love.audio.newSource( "/externalassets/ends/7dump/audios/en/4itsgreattobehere.ogg","stream" )
				 
				 if accent=="br" then
					someonefrom = love.audio.newSource( "/externalassets/dialogs/level44/en/1someonefrom.ogg","stream" )
					whatarewe = love.audio.newSource( "/externalassets/dialogs/level44/en/3whatarewe.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level44/en/5ithink.ogg","stream" )
					andthecause = love.audio.newSource( "/externalassets/dialogs/level44/en/7andthecause.ogg","stream" )
					ifonly = love.audio.newSource( "/externalassets/dialogs/level44/en/10ifonly.ogg","stream" )
					ifonlythey = love.audio.newSource( "/externalassets/dialogs/level44/en/11ifonlythey.ogg","stream" )
					suchalovely = love.audio.newSource( "/externalassets/dialogs/level44/en/12suchalovely.ogg","stream" )
					ifwetake = love.audio.newSource( "/externalassets/dialogs/level44/en/15ifwetake.ogg","stream" )
					icanhear = love.audio.newSource( "/externalassets/dialogs/level44/en/17icanhear.ogg","stream" )
					maybeeven = love.audio.newSource( "/externalassets/dialogs/level44/en/20maybeeven.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level44/en/23lookatthat.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level44/en/25yes.ogg","stream" )
					maybeitwas = love.audio.newSource( "/externalassets/dialogs/level44/en/27maybeitwas.ogg","stream" )
					thatpoorcrab = love.audio.newSource( "/externalassets/dialogs/level44/en/28thatpoorcrab.ogg","stream" )
					butwehave = love.audio.newSource( "/externalassets/dialogs/level44/en/31butwehave.ogg","stream" )
				elseif accent=="us" then
					someonefrom = love.audio.newSource( "/externalassets/dialogs/level44/en-us/1someonefrom.ogg","stream" )
					whatarewe = love.audio.newSource( "/externalassets/dialogs/level44/en-us/3whatarewe.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level44/en-us/5ithink.ogg","stream" )
					andthecause = love.audio.newSource( "/externalassets/dialogs/level44/en-us/7andthecause.ogg","stream" )
					ifonly = love.audio.newSource( "/externalassets/dialogs/level44/en-us/10ifonly.ogg","stream" )
					ifonlythey = love.audio.newSource( "/externalassets/dialogs/level44/en-us/11ifonlythey.ogg","stream" )
					suchalovely = love.audio.newSource( "/externalassets/dialogs/level44/en-us/12suchalovely.ogg","stream" )
					ifwetake = love.audio.newSource( "/externalassets/dialogs/level44/en-us/15ifwetake.ogg","stream" )
					icanhear = love.audio.newSource( "/externalassets/dialogs/level44/en-us/17icanhear.ogg","stream" )
					maybeeven = love.audio.newSource( "/externalassets/dialogs/level44/en-us/20maybeeven.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level44/en-us/23lookatthat.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level44/en-us/25yes.ogg","stream" )
					maybeitwas = love.audio.newSource( "/externalassets/dialogs/level44/en-us/27maybeitwas.ogg","stream" )
					thatpoorcrab = love.audio.newSource( "/externalassets/dialogs/level44/en-us/28thatpoorcrab.ogg","stream" )
					butwehave = love.audio.newSource( "/externalassets/dialogs/level44/en-us/31butwehave.ogg","stream" )		
				end
			elseif language=="fr" then
					someonefrom = love.audio.newSource( "/externalassets/dialogs/level44/fr/1someonefrom.ogg","stream" )
					whatarewe = love.audio.newSource( "/externalassets/dialogs/level44/fr/3whatarewe.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level44/fr/5ithink.ogg","stream" )
					andthecause = love.audio.newSource( "/externalassets/dialogs/level44/fr/7andthecause.ogg","stream" )
					ifonly = love.audio.newSource( "/externalassets/dialogs/level44/fr/10ifonly.ogg","stream" )
					ifonlythey = love.audio.newSource( "/externalassets/dialogs/level44/fr/11ifonlythey.ogg","stream" )
					suchalovely = love.audio.newSource( "/externalassets/dialogs/level44/fr/12suchalovely.ogg","stream" )
					ifwetake = love.audio.newSource( "/externalassets/dialogs/level44/fr/15ifwetake.ogg","stream" )
					icanhear = love.audio.newSource( "/externalassets/dialogs/level44/fr/17icanhear.ogg","stream" )
					maybeeven = love.audio.newSource( "/externalassets/dialogs/level44/fr/20maybeeven.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level44/fr/23lookatthat.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level44/fr/25yes.ogg","stream" )
					maybeitwas = love.audio.newSource( "/externalassets/dialogs/level44/fr/27maybeitwas.ogg","stream" )
					thatpoorcrab = love.audio.newSource( "/externalassets/dialogs/level44/fr/28thatpoorcrab.ogg","stream" )
					butwehave = love.audio.newSource( "/externalassets/dialogs/level44/fr/31butwehave.ogg","stream" )
			elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
					someonefrom = love.audio.newSource( "/externalassets/dialogs/level44/es-la/1someonefrom.ogg","stream" )
					whatarewe = love.audio.newSource( "/externalassets/dialogs/level44/es-la/3whatarewe.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level44/es-la/5ithink.ogg","stream" )
					andthecause = love.audio.newSource( "/externalassets/dialogs/level44/es-la/7andthecause.ogg","stream" )
					ifonly = love.audio.newSource( "/externalassets/dialogs/level44/es-la/10ifonly.ogg","stream" )
					ifonlythey = love.audio.newSource( "/externalassets/dialogs/level44/es-la/11ifonlythey.ogg","stream" )
					suchalovely = love.audio.newSource( "/externalassets/dialogs/level44/es-la/12suchalovely.ogg","stream" )
					ifwetake = love.audio.newSource( "/externalassets/dialogs/level44/es-la/15ifwetake.ogg","stream" )
					icanhear = love.audio.newSource( "/externalassets/dialogs/level44/es-la/17icanhear.ogg","stream" )
					maybeeven = love.audio.newSource( "/externalassets/dialogs/level44/es-la/20maybeeven.ogg","stream" )
					lookatthat = love.audio.newSource( "/externalassets/dialogs/level44/es-la/23lookatthat.ogg","stream" )
					yes = love.audio.newSource( "/externalassets/dialogs/level44/es-la/25yes.ogg","stream" )
					maybeitwas = love.audio.newSource( "/externalassets/dialogs/level44/es-la/27maybeitwas.ogg","stream" )
					thatpoorcrab = love.audio.newSource( "/externalassets/dialogs/level44/es-la/28thatpoorcrab.ogg","stream" )
					butwehave = love.audio.newSource( "/externalassets/dialogs/level44/es-la/31butwehave.ogg","stream" )
			end
			end
			
			if language2=="en" then
					if accent=="br" then
				elseif accent=="us" then
					somebodyfrom = love.audio.newSource( "/externalassets/dialogs/level44/en-us/2somebodyfrom.ogg","stream" )
					wehavetoexterminate = love.audio.newSource( "/externalassets/dialogs/level44/en-us/3wehavetoexterminate.ogg","stream" )
					wellyoumaybe = love.audio.newSource( "/externalassets/dialogs/level44/en-us/6wellyoumaybe.ogg","stream" )
					yesletsgettowork = love.audio.newSource( "/externalassets/dialogs/level44/en-us/8yesletsgettowork.ogg","stream" )
					ithinkimgrowing = love.audio.newSource( "/externalassets/dialogs/level44/en-us/9ithinkimgrowing.ogg","stream" )
					thatwonthappenany = love.audio.newSource( "/externalassets/dialogs/level44/en-us/13thatwonthappenany.ogg","stream" )
					youaregoing = love.audio.newSource( "/externalassets/dialogs/level44/en-us/14youaregoing.ogg","stream" )
					andfdto = love.audio.newSource( "/externalassets/dialogs/level44/en-us/16andfdto.ogg","stream" )
					sorryboss = love.audio.newSource( "/externalassets/dialogs/level44/en-us/18sorryboss.ogg","stream" )
					youknowifinally = love.audio.newSource( "/externalassets/dialogs/level44/en-us/19youknowifinally.ogg","stream" )
					ithinkthathis = love.audio.newSource( "/externalassets/dialogs/level44/en-us/21ithinkthathis.ogg","stream" )
					thatfootis = love.audio.newSource( "/externalassets/dialogs/level44/en-us/22thatfootis.ogg","stream" )
					butevenitharbors = love.audio.newSource( "/externalassets/dialogs/level44/en-us/24butevenitharbors.ogg","stream" )
					thisfishlooks = love.audio.newSource( "/externalassets/dialogs/level44/en-us/26thisfishlooks.ogg","stream" )
					nomanisalone = love.audio.newSource( "/externalassets/dialogs/level44/en-us/29nomanisalone.ogg","stream" )
					thistimeourgoal = love.audio.newSource( "/externalassets/dialogs/level44/en-us/30thistimeourgoal.ogg","stream" )
				end
			elseif language2=="pl" then
					somebodyfrom = love.audio.newSource( "/externalassets/dialogs/level44/pl/2somebodyfrom.ogg","stream" )
					wehavetoexterminate = love.audio.newSource( "/externalassets/dialogs/level44/pl/3wehavetoexterminate.ogg","stream" )
					wellyoumaybe = love.audio.newSource( "/externalassets/dialogs/level44/pl/6wellyoumaybe.ogg","stream" )
					yesletsgettowork = love.audio.newSource( "/externalassets/dialogs/level44/pl/8yesletsgettowork.ogg","stream" )
					ithinkimgrowing = love.audio.newSource( "/externalassets/dialogs/level44/pl/9ithinkimgrowing.ogg","stream" )
					thatwonthappenany = love.audio.newSource( "/externalassets/dialogs/level44/pl/13thatwonthappenany.ogg","stream" )
					youaregoing = love.audio.newSource( "/externalassets/dialogs/level44/pl/14youaregoing.ogg","stream" )
					andfdto = love.audio.newSource( "/externalassets/dialogs/level44/pl/16andfdto.ogg","stream" )
					sorryboss = love.audio.newSource( "/externalassets/dialogs/level44/pl/18sorryboss.ogg","stream" )
					youknowifinally = love.audio.newSource( "/externalassets/dialogs/level44/pl/19youknowifinally.ogg","stream" )
					ithinkthathis = love.audio.newSource( "/externalassets/dialogs/level44/pl/21ithinkthathis.ogg","stream" )
					thatfootis = love.audio.newSource( "/externalassets/dialogs/level44/pl/22thatfootis.ogg","stream" )
					butevenitharbors = love.audio.newSource( "/externalassets/dialogs/level44/pl/24butevenitharbors.ogg","stream" )
					thisfishlooks = love.audio.newSource( "/externalassets/dialogs/level44/pl/26thisfishlooks.ogg","stream" )
					nomanisalone = love.audio.newSource( "/externalassets/dialogs/level44/pl/29nomanisalone.ogg","stream" )
					thistimeourgoal = love.audio.newSource( "/externalassets/dialogs/level44/pl/30thistimeourgoal.ogg","stream" )
			
			end
			
			--fish 1
			someonefrom:setEffect('myEffect')
			whatarewe:setEffect('myEffect')
			ithink:setEffect('myEffect')
			andthecause:setEffect('myEffect')
			ifonly:setEffect('myEffect')
			ifonlythey:setEffect('myEffect')
			suchalovely:setEffect('myEffect')
			ifwetake:setEffect('myEffect')
			icanhear:setEffect('myEffect')
			maybeeven:setEffect('myEffect')
			lookatthat:setEffect('myEffect')
			yes:setEffect('myEffect')
			maybeitwas:setEffect('myEffect')
			thatpoorcrab:setEffect('myEffect')
			butwehave:setEffect('myEffect')
			
			--fish 2
			somebodyfrom:setEffect('myEffect')
			wehavetoexterminate:setEffect('myEffect')
			wellyoumaybe:setEffect('myEffect')
			yesletsgettowork:setEffect('myEffect')
			ithinkimgrowing:setEffect('myEffect')
			thatwonthappenany:setEffect('myEffect')
			youaregoing:setEffect('myEffect')
			andfdto:setEffect('myEffect')
			sorryboss:setEffect('myEffect')
			youknowifinally:setEffect('myEffect')
			ithinkthathis:setEffect('myEffect')
			thatfootis:setEffect('myEffect')
			butevenitharbors:setEffect('myEffect')
			thisfishlooks:setEffect('myEffect')
			nomanisalone:setEffect('myEffect')
			thistimeourgoal:setEffect('myEffect')
			
			--end
			greetings:setEffect('myEffect')
			yourssincerely:setEffect('myEffect')
			agents:setEffect('myEffect')
			itsgreattobehere:setEffect('myEffect')
		
		elseif nLevel==45 then
		
			--parrot
				cruecruel = love.audio.newSource( "externalassets/dialogs/level45/en/parrot/1cruelcruel.ogg","stream" )
				aycaramba = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/2aycaramba.ogg","stream" )
				cruelcaptain = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/3cruelcaptain.ogg","stream" )
				treasureyoufools = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/4treasureyoufools.ogg","stream" )
				goodgrief = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/5goodgrief.ogg","stream" )
				thatsridiulous = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/6thatsridiculous.ogg","stream" )
				dontpushme = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/7dontpushme.ogg","stream" )
				thastbetter = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/8thatsbetter.ogg","stream" )
				anditscurtainsforyou = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/9anditscurtainsforyou.ogg","stream" )
				imgoingtobeinyourway = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/10imgoingtobeinyourway.ogg","stream" )
				trickyproblem = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/11trickyproblem.ogg","stream" )
				dangit = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/12dangit.ogg","stream" )
				youaretoofat = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/13youaretoofat.ogg","stream" )
				pollywantsacracker = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/14pollywantsacracker.ogg","stream" )
				beatitsir = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/15beatitsir.ogg","stream" )
				leavemealone = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/16leavemealone.ogg","stream" )
		
		 if language=="en" then
				
			if accent=="br" then
			elseif accent=="us" then
				whoknows = love.audio.newSource( "/externalassets/dialogs/level45/en-us/2whoknows.ogg","stream" )
				insuchasmall = love.audio.newSource( "/externalassets/dialogs/level45/en-us/4insuchasmallchest.ogg","stream" )
				noproblem = love.audio.newSource( "/externalassets/dialogs/level45/en-us/7noproblem.ogg","stream" )
				imsorry = love.audio.newSource( "/externalassets/dialogs/level45/en-us/9imsorry.ogg","stream" )
				becareful = love.audio.newSource( "/externalassets/dialogs/level45/en-us/10becareful.ogg","stream" )
				maybeitbelonged = love.audio.newSource( "/externalassets/dialogs/level45/en-us/13maybeitbelonged.ogg","stream" )
				why = love.audio.newSource( "/externalassets/dialogs/level45/en-us/15why.ogg","stream" )
				wellmaybe = love.audio.newSource( "/externalassets/dialogs/level45/en-us/17wellmaybe.ogg","stream" )
				canyousense = love.audio.newSource( "/externalassets/dialogs/level45/en-us/19canyousense.ogg","stream" )
				ohyoudonthave = love.audio.newSource( "/externalassets/dialogs/level45/en-us/21ohyoudonthave.ogg","stream" )

			end
		elseif language=="fr" then
				
				whoknows = love.audio.newSource( "/externalassets/dialogs/level45/fr/18whoknows.ogg","stream" )
				insuchasmall = love.audio.newSource( "/externalassets/dialogs/level45/fr/20insuchasmall.ogg","stream" )
				noproblem = love.audio.newSource( "/externalassets/dialogs/level45/fr/23noproblem.ogg","stream" )
				imsorry = love.audio.newSource( "/externalassets/dialogs/level45/fr/25imsorry.ogg","stream" )
				becareful = love.audio.newSource( "/externalassets/dialogs/level45/fr/26becareful.ogg","stream" )
				maybeitbelonged = love.audio.newSource( "/externalassets/dialogs/level45/fr/29maybeitbelonged.ogg","stream" )
				why = love.audio.newSource( "/externalassets/dialogs/level45/fr/31why.ogg","stream" )
				wellmaybe = love.audio.newSource( "/externalassets/dialogs/level45/fr/33wellmaybe.ogg","stream" )
				canyousense = love.audio.newSource( "/externalassets/dialogs/level45/fr/35canyousense.ogg","stream" )
				ohyoudonthave = love.audio.newSource( "/externalassets/dialogs/level45/fr/37ohyoudonthave.ogg","stream" )
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
				whoknows = love.audio.newSource( "/externalassets/dialogs/level45/es-la/2whoknows.ogg","stream" )
				insuchasmall = love.audio.newSource( "/externalassets/dialogs/level45/es-la/4insuchasmallchest.ogg","stream" )
				noproblem = love.audio.newSource( "/externalassets/dialogs/level45/es-la/7noproblem.ogg","stream" )
				imsorry = love.audio.newSource( "/externalassets/dialogs/level45/es-la/9imsorry.ogg","stream" )
				becareful = love.audio.newSource( "/externalassets/dialogs/level45/es-la/10becareful.ogg","stream" )
				maybeitbelonged = love.audio.newSource( "/externalassets/dialogs/level45/es-la/13maybeitbelonged.ogg","stream" )
				why = love.audio.newSource( "/externalassets/dialogs/level45/es-la/15why.ogg","stream" )
				wellmaybe = love.audio.newSource( "/externalassets/dialogs/level45/es-la/17wellmaybe.ogg","stream" )
				canyousense = love.audio.newSource( "/externalassets/dialogs/level45/es-la/19canyousense.ogg","stream" )
				ohyoudonthave = love.audio.newSource( "/externalassets/dialogs/level45/es-la/21ohyoudonthave.ogg","stream" )
			end
		end
		if language2=="en" then
		
			
		
			if accent2=="br" then
				
				whatcouldbe = love.audio.newSource( "/externalassets/dialogs/level45/en/1whatcouldbe.ogg","stream" )
				couldthisbe = love.audio.newSource( "/externalassets/dialogs/level45/en/3couldthisbe.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level45/en/5yuck.ogg","stream" )
				thanks = love.audio.newSource( "/externalassets/dialogs/level45/en/6thanks.ogg","stream" )
				whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level45/en/8whatareyoudoing.ogg","stream" )
				itsmoretheotherway = love.audio.newSource( "/externalassets/dialogs/level45/en/11itsmoretheother.ogg","stream" )
				thatssomeoctopus = love.audio.newSource( "/externalassets/dialogs/level45/en/12thatssomeoctopus.ogg","stream" )
				whoever = love.audio.newSource( "/externalassets/dialogs/level45/en/14whoeverlived.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level45/en/16lookhehad.ogg","stream" )
				youthinkso = love.audio.newSource( "/externalassets/dialogs/level45/en/18youthinkso.ogg","stream" )
				wheredoyousee = love.audio.newSource( "/externalassets/dialogs/level45/en/20wheredoyousee.ogg","stream" )
				
			elseif accent2=="us" then
				whatcouldbe = love.audio.newSource( "/externalassets/dialogs/level45/en-us/1whatcouldbe.ogg","stream" )
				couldthisbe = love.audio.newSource( "/externalassets/dialogs/level45/en-us/3couldthisbe.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level45/en-us/5yuck.ogg","stream" )
				thanks = love.audio.newSource( "/externalassets/dialogs/level45/en-us/6thanks.ogg","stream" )
				whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level45/en-us/8whatareyoudoing.ogg","stream" )
				itsmoretheotherway = love.audio.newSource( "/externalassets/dialogs/level45/en-us/11itsmoretheother.ogg","stream" )
				thatssomeoctopus = love.audio.newSource( "/externalassets/dialogs/level45/en-us/12thatssomeoctopus.ogg","stream" )
				whoever = love.audio.newSource( "/externalassets/dialogs/level45/en-us/14whoeverlived.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level45/en-us/16lookhehad.ogg","stream" )
				youthinkso = love.audio.newSource( "/externalassets/dialogs/level45/en-us/18youthinkso.ogg","stream" )
				wheredoyousee = love.audio.newSource( "/externalassets/dialogs/level45/en-us/20wheredoyousee.ogg","stream" )	
			end

			elseif language2=="pl" then
				whatcouldbe = love.audio.newSource( "/externalassets/dialogs/level45/pl/1whatcouldbe.ogg","stream" )
				couldthisbe = love.audio.newSource( "/externalassets/dialogs/level45/pl/3couldthisbe.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level45/pl/5yuck.ogg","stream" )
				thanks = love.audio.newSource( "/externalassets/dialogs/level45/pl/6thanks.ogg","stream" )
				whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level45/pl/8whatareyoudoing.ogg","stream" )
				itsmoretheotherway = love.audio.newSource( "/externalassets/dialogs/level45/pl/11itsmoretheother.ogg","stream" )
				thatssomeoctopus = love.audio.newSource( "/externalassets/dialogs/level45/pl/12thatssomeoctopus.ogg","stream" )
				whoever = love.audio.newSource( "/externalassets/dialogs/level45/pl/14whoeverlived.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level45/pl/16lookhehad.ogg","stream" )
				youthinkso = love.audio.newSource( "/externalassets/dialogs/level45/pl/18youthinkso.ogg","stream" )
				wheredoyousee = love.audio.newSource( "/externalassets/dialogs/level45/pl/20wheredoyousee.ogg","stream" )	
		
			elseif language2=="cs" then
				whatcouldbe = love.audio.newSource( "/externalassets/dialogs/level45/cs/1whatcouldbe.ogg","stream" )
				couldthisbe = love.audio.newSource( "/externalassets/dialogs/level45/cs/3couldthisbe.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level45/cs/5yuck.ogg","stream" )
				thanks = love.audio.newSource( "/externalassets/dialogs/level45/cs/6thanks.ogg","stream" )
				whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level45/cs/8whatareyoudoing.ogg","stream" )
				itsmoretheotherway = love.audio.newSource( "/externalassets/dialogs/level45/cs/11itsmoretheother.ogg","stream" )
				thatssomeoctopus = love.audio.newSource( "/externalassets/dialogs/level45/cs/12thatssomeoctopus.ogg","stream" )
				whoever = love.audio.newSource( "/externalassets/dialogs/level45/cs/14whoeverlived.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level45/cs/16lookhehad.ogg","stream" )
				youthinkso = love.audio.newSource( "/externalassets/dialogs/level45/cs/18youthinkso.ogg","stream" )
				wheredoyousee = love.audio.newSource( "/externalassets/dialogs/level45/cs/20wheredoyousee.ogg","stream" )		
		elseif language2=="fr" then
				whatcouldbe = love.audio.newSource( "/externalassets/dialogs/level45/fr/1whatcouldbe.ogg","stream" )
				couldthisbe = love.audio.newSource( "/externalassets/dialogs/level45/fr/3couldthisbe.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level45/fr/5yuck.ogg","stream" )
				thanks = love.audio.newSource( "/externalassets/dialogs/level45/fr/6thanks.ogg","stream" )
				whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level45/fr/8whatareyoudoing.ogg","stream" )
				itsmoretheotherway = love.audio.newSource( "/externalassets/dialogs/level45/fr/11itsmoretheother.ogg","stream" )
				thatssomeoctopus = love.audio.newSource( "/externalassets/dialogs/level45/fr/12thatssomeoctopus.ogg","stream" )
				whoever = love.audio.newSource( "/externalassets/dialogs/level45/fr/14whoeverlived.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/level45/fr/16lookhehad.ogg","stream" )
				youthinkso = love.audio.newSource( "/externalassets/dialogs/level45/fr/18youthinkso.ogg","stream" )
				wheredoyousee = love.audio.newSource( "/externalassets/dialogs/level45/fr/20wheredoyousee.ogg","stream" )		
				end
		
				--parrot's effects
				if language=="en" then			
				cruecruel:setEffect('myEffect')
				aycaramba:setEffect('myEffect')
				cruelcaptain:setEffect('myEffect')
				treasureyoufools:setEffect('myEffect')
				goodgrief:setEffect('myEffect')
				thatsridiulous:setEffect('myEffect')
				dontpushme:setEffect('myEffect')
				thastbetter:setEffect('myEffect')
				anditscurtainsforyou:setEffect('myEffect')
				imgoingtobeinyourway:setEffect('myEffect')
				trickyproblem:setEffect('myEffect')
				dangit:setEffect('myEffect')
				youaretoofat:setEffect('myEffect')
				pollywantsacracker:setEffect('myEffect')
				beatitsir:setEffect('myEffect')
				leavemealone:setEffect('myEffect')
				end
				wheredoyousee:setEffect('myEffect')
				whatcouldbe:setEffect('myEffect')
				couldthisbe:setEffect('myEffect')
				yuck:setEffect('myEffect')
				thanks:setEffect('myEffect')
				whatareyoudoing:setEffect('myEffect')
				itsmoretheotherway:setEffect('myEffect')
				thatssomeoctopus:setEffect('myEffect')
				whoever:setEffect('myEffect')
				look:setEffect('myEffect')
				youthinkso:setEffect('myEffect')
						
			if not language=="en" and accent=="en" then
				whoknows:setEffect('myEffect')
				insuchasmall:setEffect('myEffect')
				noproblem:setEffect('myEffect')
				imsorry:setEffect('myEffect')
				becareful:setEffect('myEffect')
				maybeitbelonged:setEffect('myEffect')
				why:setEffect('myEffect')
				wellmaybe:setEffect('myEffect')
				canyousense:setEffect('myEffect')
				ohyoudonthave:setEffect('myEffect')
			end
				
		elseif nLevel==46 then
		
			 if language=="en" then
				if accent=="br" then
					nowonder = love.audio.newSource( "/externalassets/dialogs/level46/en/2nowonder.ogg","stream" )
					wellthats = love.audio.newSource( "/externalassets/dialogs/level46/en/4wellthats.ogg","stream" )
					atleastthesnowman = love.audio.newSource( "/externalassets/dialogs/level46/en/5atleastthesnowman.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level46/en/10ivenever.ogg","stream" )

				elseif accent=="us" then
					nowonder = love.audio.newSource( "/externalassets/dialogs/level46/en-us/2nowonder.ogg","stream" )
					wellthats = love.audio.newSource( "/externalassets/dialogs/level46/en-us/4wellthats.ogg","stream" )
					atleastthesnowman = love.audio.newSource( "/externalassets/dialogs/level46/en-us/5atleastthesnowman.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level46/en-us/10ivenever.ogg","stream" )
					
				end
							

			elseif language=="fr" then
					nowonder = love.audio.newSource( "/externalassets/dialogs/level46/fr/2nowonder.ogg","stream" )
					wellthats = love.audio.newSource( "/externalassets/dialogs/level46/fr/4wellthats.ogg","stream" )
					atleastthesnowman = love.audio.newSource( "/externalassets/dialogs/level46/fr/5atleastthesnowman.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level46/fr/10ivenever.ogg","stream" )
		
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					nowonder = love.audio.newSource( "/externalassets/dialogs/level46/es-la/2nowonder.ogg","stream" )
					wellthats = love.audio.newSource( "/externalassets/dialogs/level46/es-la/4wellthats.ogg","stream" )
					atleastthesnowman = love.audio.newSource( "/externalassets/dialogs/level46/es-la/5atleastthesnowman.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level46/es-la/10ivenever.ogg","stream" )
				end
			end
			
			if language2=="en" then
				if accent2=="br" then
					imkindof = love.audio.newSource( "/externalassets/dialogs/level46/en/1imkindof.ogg","stream" )
					imcold = love.audio.newSource( "/externalassets/dialogs/level46/en/3imcold.ogg","stream" )
					maybeyoushould = love.audio.newSource( "/externalassets/dialogs/level46/en/6maybeyoushould.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level46/en/7everything.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level46/en/8ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level46/en/9ouch.ogg","stream" )

				elseif accent2=="us" then
					imkindof = love.audio.newSource( "/externalassets/dialogs/level46/en-us/1imkindof.ogg","stream" )
					imcold = love.audio.newSource( "/externalassets/dialogs/level46/en-us/3imcold.ogg","stream" )
					maybeyoushould = love.audio.newSource( "/externalassets/dialogs/level46/en-us/6maybeyoushould.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level46/en-us/7everything.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level46/en-us/8ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level46/en-us/9ouch.ogg","stream" )
	
				end	
		
			elseif language2=="pl" then
					imkindof = love.audio.newSource( "/externalassets/dialogs/level46/pl/1imkindof.ogg","stream" )
					imcold = love.audio.newSource( "/externalassets/dialogs/level46/pl/3imcold.ogg","stream" )
					maybeyoushould = love.audio.newSource( "/externalassets/dialogs/level46/pl/6maybeyoushould.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level46/pl/7everything.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level46/pl/8ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level46/pl/9ouch.ogg","stream" )
			
			elseif language2=="cs" then
					imkindof = love.audio.newSource( "/externalassets/dialogs/level46/cs/1imkindof.ogg","stream" )
					imcold = love.audio.newSource( "/externalassets/dialogs/level46/cs/3imcold.ogg","stream" )
					maybeyoushould = love.audio.newSource( "/externalassets/dialogs/level46/cs/6maybeyoushould.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level46/cs/7everything.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level46/cs/8ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level46/cs/9ouch.ogg","stream" )
			elseif language2=="fr" then
					imkindof = love.audio.newSource( "/externalassets/dialogs/level46/fr/1imkindof.ogg","stream" )
					imcold = love.audio.newSource( "/externalassets/dialogs/level46/fr/3imcold.ogg","stream" )
					maybeyoushould = love.audio.newSource( "/externalassets/dialogs/level46/fr/6maybeyoushould.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level46/fr/7everything.ogg","stream" )
					ow = love.audio.newSource( "/externalassets/dialogs/level46/fr/8ow.ogg","stream" )
					ouch = love.audio.newSource( "/externalassets/dialogs/level46/fr/9ouch.ogg","stream" )
				end	
					
					--effects fish1
					nowonder:setEffect('myEffect')
					wellthats:setEffect('myEffect')
					atleastthesnowman:setEffect('myEffect')
					ivenever:setEffect('myEffect')
					--effects fish2
					imkindof:setEffect('myEffect')
					imcold:setEffect('myEffect')
					maybeyoushould:setEffect('myEffect')
					everything:setEffect('myEffect')
					ow:setEffect('myEffect')
					ouch:setEffect('myEffect')
		
		elseif nLevel==47 then
		
		 if language=="en" then
				if accent=="br" then
						see = love.audio.newSource( "/externalassets/dialogs/level47/en/1see.ogg","stream" )
						noithink = love.audio.newSource( "/externalassets/dialogs/level47/en/4noithink.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level47/en/5ihaveafeeling.ogg","stream" )
					
						
				elseif accent=="us" then
						see = love.audio.newSource( "/externalassets/dialogs/level47/en-us/1see.ogg","stream" )
						noithink = love.audio.newSource( "/externalassets/dialogs/level47/en-us/4noithink.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level47/en-us/5ihaveafeeling.ogg","stream" )
				end
			
					
		 
		elseif language=="fr" then
						see = love.audio.newSource( "/externalassets/dialogs/level47/fr/1see.ogg","stream" )
						noithink = love.audio.newSource( "/externalassets/dialogs/level47/fr/4noithink.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level47/fr/5ihaveafeeling.ogg","stream" )
		
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
						see = love.audio.newSource( "/externalassets/dialogs/level47/es-la/1see.ogg","stream" )
						noithink = love.audio.newSource( "/externalassets/dialogs/level47/es-la/4noithink.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level47/es-la/5ihaveafeeling.ogg","stream" )
				end
		end
		if language2=="en" then
				if accent2=="br" then
					youmeaninthewater = love.audio.newSource( "/externalassets/dialogs/level47/en/2youmeaninthewater.ogg","stream" )
					whoseship = love.audio.newSource( "/externalassets/dialogs/level47/en/3whoseship.ogg","stream" )
					youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level47/en/6youandyourfeelings.ogg","stream" )
					thatwasalittle = love.audio.newSource( "/externalassets/dialogs/level47/en/7thatwasalittle.ogg","stream" )
					coulditpossibly = love.audio.newSource( "/externalassets/dialogs/level47/en/8coulditpossibly.ogg","stream" )
					theremustbesomekind = love.audio.newSource( "/externalassets/dialogs/level47/en/9theremustbesomekind.ogg","stream" )
		
				
				elseif accent2=="us" then
					youmeaninthewater = love.audio.newSource( "/externalassets/dialogs/level47/en-us/2youmeaninthewater.ogg","stream" )
					whoseship = love.audio.newSource( "/externalassets/dialogs/level47/en-us/3whoseship.ogg","stream" )
					youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level47/en-us/6youandyourfeelings.ogg","stream" )
					thatwasalittle = love.audio.newSource( "/externalassets/dialogs/level47/en-us/7thatwasalittle.ogg","stream" )
					coulditpossibly = love.audio.newSource( "/externalassets/dialogs/level47/en-us/8coulditpossibly.ogg","stream" )
					theremustbesomekind = love.audio.newSource( "/externalassets/dialogs/level47/en-us/9theremustbesomekind.ogg","stream" )

				end

		elseif language2=="pl" then
					youmeaninthewater = love.audio.newSource( "/externalassets/dialogs/level47/pl/2youmeaninthewater.ogg","stream" )
					whoseship = love.audio.newSource( "/externalassets/dialogs/level47/pl/3whoseship.ogg","stream" )
					youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level47/pl/6youandyourfeelings.ogg","stream" )
					thatwasalittle = love.audio.newSource( "/externalassets/dialogs/level47/pl/7thatwasalittle.ogg","stream" )
					coulditpossibly = love.audio.newSource( "/externalassets/dialogs/level47/pl/8coulditpossibly.ogg","stream" )
					theremustbesomekind = love.audio.newSource( "/externalassets/dialogs/level47/pl/9theremustbesomekind.ogg","stream" )

		elseif language2=="cs" then
					youmeaninthewater = love.audio.newSource( "/externalassets/dialogs/level47/cs/2youmeaninthewater.ogg","stream" )
					whoseship = love.audio.newSource( "/externalassets/dialogs/level47/cs/3whoseship.ogg","stream" )
					youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level47/cs/6youandyourfeelings.ogg","stream" )
					thatwasalittle = love.audio.newSource( "/externalassets/dialogs/level47/cs/7thatwasalittle.ogg","stream" )
					coulditpossibly = love.audio.newSource( "/externalassets/dialogs/level47/cs/8coulditpossibly.ogg","stream" )
					theremustbesomekind = love.audio.newSource( "/externalassets/dialogs/level47/cs/9theremustbesomekind.ogg","stream" )
		elseif language2=="fr" then
					youmeaninthewater = love.audio.newSource( "/externalassets/dialogs/level47/fr/2youmeaninthewater.ogg","stream" )
					whoseship = love.audio.newSource( "/externalassets/dialogs/level47/fr/3whoseship.ogg","stream" )
					youandyourfeelings = love.audio.newSource( "/externalassets/dialogs/level47/fr/6youandyourfeelings.ogg","stream" )
					thatwasalittle = love.audio.newSource( "/externalassets/dialogs/level47/fr/7thatwasalittle.ogg","stream" )
					coulditpossibly = love.audio.newSource( "/externalassets/dialogs/level47/fr/8coulditpossibly.ogg","stream" )
					theremustbesomekind = love.audio.newSource( "/externalassets/dialogs/level47/fr/9theremustbesomekind.ogg","stream" )
		end
				
						see:setEffect('myEffect')
						noithink:setEffect('myEffect')
						ihaveafeeling:setEffect('myEffect')
					
					youmeaninthewater:setEffect('myEffect')
					whoseship:setEffect('myEffect')
					youandyourfeelings:setEffect('myEffect')
					thatwasalittle:setEffect('myEffect')
					coulditpossibly:setEffect('myEffect')
					theremustbesomekind:setEffect('myEffect')
		
		elseif nLevel==48 then
		
			if language=="en" then
				if accent=="br" then
					idontthinkhe = love.audio.newSource( "/externalassets/dialogs/level48/en/5idontthinkhe.ogg","stream" )
					butthisisnot = love.audio.newSource( "/externalassets/dialogs/level48/en/6butthisisnot.ogg","stream" )
					butjusthavealook = love.audio.newSource( "/externalassets/dialogs/level48/en/8butjusthavealook.ogg","stream" )
					icantunderstand = love.audio.newSource( "/externalassets/dialogs/level48/en/9icantunderstand.ogg","stream" )
					thesetables = love.audio.newSource( "/externalassets/dialogs/level48/en/10thesetables.ogg","stream" )
					theselittle = love.audio.newSource( "/externalassets/dialogs/level48/en/11theselittle.ogg","stream" )
					icantgetoutof = love.audio.newSource( "/externalassets/dialogs/level48/en/12icantgetoutof.ogg","stream" )
					helpmesomehow = love.audio.newSource( "/externalassets/dialogs/level48/en/14helpmesomehow.ogg","stream" )
					iftherewasnochair = love.audio.newSource( "/externalassets/dialogs/level48/en/20iftherewasnochair.ogg","stream" )
					especiallymyself = love.audio.newSource( "/externalassets/dialogs/level48/en/21especiallymyself.ogg","stream" )
					thereshouldbe = love.audio.newSource( "/externalassets/dialogs/level48/en/23thereshouldbe.ogg","stream" )
					theyaremass = love.audio.newSource( "/externalassets/dialogs/level48/en/26theyaremass.ogg","stream" )
					imafraidthatonly = love.audio.newSource( "/externalassets/dialogs/level48/en/28imafraidthatonly.ogg","stream" )
					whyisthatsword = love.audio.newSource( "/externalassets/dialogs/level48/en/30whyisthatsword.ogg","stream" )
					certainly = love.audio.newSource( "/externalassets/dialogs/level48/en/32certainly.ogg","stream" )
					theremustbesome = love.audio.newSource( "/externalassets/dialogs/level48/en/34theremustbesome.ogg","stream" )
					theremustbesomeculinary = love.audio.newSource( "/externalassets/dialogs/level48/en/35theremustbesomeculinary.ogg","stream" )
			
				elseif accent=="us" then
					idontthinkhe = love.audio.newSource( "/externalassets/dialogs/level48/en-us/5idontthinkhe.ogg","stream" )
					butthisisnot = love.audio.newSource( "/externalassets/dialogs/level48/en-us/6butthisisnot.ogg","stream" )
					butjusthavealook = love.audio.newSource( "/externalassets/dialogs/level48/en-us/8butjusthavealook.ogg","stream" )
					icantunderstand = love.audio.newSource( "/externalassets/dialogs/level48/en-us/9icantunderstand.ogg","stream" )
					thesetables = love.audio.newSource( "/externalassets/dialogs/level48/en-us/10thesetables.ogg","stream" )
					theselittle = love.audio.newSource( "/externalassets/dialogs/level48/en-us/11theselittle.ogg","stream" )
					icantgetoutof = love.audio.newSource( "/externalassets/dialogs/level48/en-us/12icantgetoutof.ogg","stream" )
					helpmesomehow = love.audio.newSource( "/externalassets/dialogs/level48/en-us/14helpmesomehow.ogg","stream" )
					iftherewasnochair = love.audio.newSource( "/externalassets/dialogs/level48/en-us/20iftherewasnochair.ogg","stream" )
					especiallymyself = love.audio.newSource( "/externalassets/dialogs/level48/en-us/21especiallymyself.ogg","stream" )
					thereshouldbe = love.audio.newSource( "/externalassets/dialogs/level48/en-us/23thereshouldbe.ogg","stream" )
					theyaremass = love.audio.newSource( "/externalassets/dialogs/level48/en-us/26theyaremass.ogg","stream" )
					imafraidthatonly = love.audio.newSource( "/externalassets/dialogs/level48/en-us/28imafraidthatonly.ogg","stream" )
					whyisthatsword = love.audio.newSource( "/externalassets/dialogs/level48/en-us/30whyisthatsword.ogg","stream" )
					certainly = love.audio.newSource( "/externalassets/dialogs/level48/en-us/32certainly.ogg","stream" )
					theremustbesome = love.audio.newSource( "/externalassets/dialogs/level48/en-us/34theremustbesome.ogg","stream" )
					theremustbesomeculinary = love.audio.newSource( "/externalassets/dialogs/level48/en-us/35theremustbesomeculinary.ogg","stream" )
				
				end
			elseif language=="fr" then
				idontthinkhe = love.audio.newSource( "/externalassets/dialogs/level48/fr/5idontthinkhe.ogg","stream" )
				butthisisnot = love.audio.newSource( "/externalassets/dialogs/level48/fr/6butthisisnot.ogg","stream" )
				butjusthavealook = love.audio.newSource( "/externalassets/dialogs/level48/fr/8butjusthavealook.ogg","stream" )
				icantunderstand = love.audio.newSource( "/externalassets/dialogs/level48/fr/9icantunderstand.ogg","stream" )
				thesetables = love.audio.newSource( "/externalassets/dialogs/level48/fr/10thesetables.ogg","stream" )
				theselittle = love.audio.newSource( "/externalassets/dialogs/level48/fr/11theselittle.ogg","stream" )
				icantgetoutof = love.audio.newSource( "/externalassets/dialogs/level48/fr/12icantgetoutof.ogg","stream" )
				helpmesomehow = love.audio.newSource( "/externalassets/dialogs/level48/fr/14helpmesomehow.ogg","stream" )
				iftherewasnochair = love.audio.newSource( "/externalassets/dialogs/level48/fr/20iftherewasnochair.ogg","stream" )
				especiallymyself = love.audio.newSource( "/externalassets/dialogs/level48/fr/21especiallymyself.ogg","stream" )
				thereshouldbe = love.audio.newSource( "/externalassets/dialogs/level48/fr/23thereshouldbe.ogg","stream" )
				theyaremass = love.audio.newSource( "/externalassets/dialogs/level48/fr/26theyaremass.ogg","stream" )
				imafraidthatonly = love.audio.newSource( "/externalassets/dialogs/level48/fr/28imafraidthatonly.ogg","stream" )
				whyisthatsword = love.audio.newSource( "/externalassets/dialogs/level48/fr/30whyisthatsword.ogg","stream" )
				certainly = love.audio.newSource( "/externalassets/dialogs/level48/fr/32certainly.ogg","stream" )
				theremustbesome = love.audio.newSource( "/externalassets/dialogs/level48/fr/34theremustbesome.ogg","stream" )
				theremustbesomeculinary = love.audio.newSource( "/externalassets/dialogs/level48/fr/35theremustbesomeculinary.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					idontthinkhe = love.audio.newSource( "/externalassets/dialogs/level48/es-la/5idontthinkhe.ogg","stream" )
					butthisisnot = love.audio.newSource( "/externalassets/dialogs/level48/es-la/6butthisisnot.ogg","stream" )
					butjusthavealook = love.audio.newSource( "/externalassets/dialogs/level48/es-la/8butjusthavealook.ogg","stream" )
					icantunderstand = love.audio.newSource( "/externalassets/dialogs/level48/es-la/9icantunderstand.ogg","stream" )
					thesetables = love.audio.newSource( "/externalassets/dialogs/level48/es-la/10thesetables.ogg","stream" )
					theselittle = love.audio.newSource( "/externalassets/dialogs/level48/es-la/11theselittle.ogg","stream" )
					icantgetoutof = love.audio.newSource( "/externalassets/dialogs/level48/es-la/12icantgetoutof.ogg","stream" )
					helpmesomehow = love.audio.newSource( "/externalassets/dialogs/level48/es-la/14helpmesomehow.ogg","stream" )
					iftherewasnochair = love.audio.newSource( "/externalassets/dialogs/level48/es-la/20iftherewasnochair.ogg","stream" )
					especiallymyself = love.audio.newSource( "/externalassets/dialogs/level48/es-la/21especiallymyself.ogg","stream" )
					thereshouldbe = love.audio.newSource( "/externalassets/dialogs/level48/es-la/23thereshouldbe.ogg","stream" )
					theyaremass = love.audio.newSource( "/externalassets/dialogs/level48/es-la/26theyaremass.ogg","stream" )
					imafraidthatonly = love.audio.newSource( "/externalassets/dialogs/level48/es-la/28imafraidthatonly.ogg","stream" )
					whyisthatsword = love.audio.newSource( "/externalassets/dialogs/level48/es-la/30whyisthatsword.ogg","stream" )
					certainly = love.audio.newSource( "/externalassets/dialogs/level48/es-la/32certainly.ogg","stream" )
					theremustbesome = love.audio.newSource( "/externalassets/dialogs/level48/es-la/34theremustbesome.ogg","stream" )
					theremustbesomeculinary = love.audio.newSource( "/externalassets/dialogs/level48/es-la/35theremustbesomeculinary.ogg","stream" )
				end
			end
			if language2=="en" then
				
				if accent2=="br" then
				ohmy = love.audio.newSource( "/externalassets/dialogs/level48/en/1ohmy.ogg","stream" )
				yeahwe = love.audio.newSource( "/externalassets/dialogs/level48/en/2yeahwe.ogg","stream" )
				darnedkitchen = love.audio.newSource( "/externalassets/dialogs/level48/en/3darnedkitchen.ogg","stream" )
				andnowweare = love.audio.newSource( "/externalassets/dialogs/level48/en/4andnowweare.ogg","stream" )
				therearenotmany = love.audio.newSource( "/externalassets/dialogs/level48/en/7therearenotmany.ogg","stream" )
				imjustthinking = love.audio.newSource( "/externalassets/dialogs/level48/en/13imjustthinking.ogg","stream" )
				imonmyway = love.audio.newSource( "/externalassets/dialogs/level48/en/16imonmyway.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level48/en/17yuck.ogg","stream" )
				thispotwasnt = love.audio.newSource( "/externalassets/dialogs/level48/en/18thispotwasnt.ogg","stream" )
				nobody = love.audio.newSource( "/externalassets/dialogs/level48/en/19nobody.ogg","stream" )
				whatisachair = love.audio.newSource( "/externalassets/dialogs/level48/en/22whatisachair.ogg","stream" )
				everybodywant = love.audio.newSource( "/externalassets/dialogs/level48/en/24everybodywant.ogg","stream" )
				ihaveseenthesetables = love.audio.newSource( "/externalassets/dialogs/level48/en/25ihaveseenthesetables.ogg","stream" )
				butheyarenice = love.audio.newSource( "/externalassets/dialogs/level48/en/27butheyarenice.ogg","stream" )
				itseemsthateven = love.audio.newSource( "/externalassets/dialogs/level48/en/29itseemsthateven.ogg","stream" )
				maybetheyusedit = love.audio.newSource( "/externalassets/dialogs/level48/en/31maybetheyusedit.ogg","stream" )
				wearelucky = love.audio.newSource( "/externalassets/dialogs/level48/en/33wearelucky.ogg","stream" )
				possibly = love.audio.newSource( "/externalassets/dialogs/level48/en/36possibly.ogg","stream" )
	
				
				elseif accent2=="us" then
					ohmy = love.audio.newSource( "/externalassets/dialogs/level48/en-us/1ohmy.ogg","stream" )
					yeahwe = love.audio.newSource( "/externalassets/dialogs/level48/en-us/2yeahwe.ogg","stream" )
					darnedkitchen = love.audio.newSource( "/externalassets/dialogs/level48/en-us/3darnedkitchen.ogg","stream" )
					andnowweare = love.audio.newSource( "/externalassets/dialogs/level48/en-us/4andnowweare.ogg","stream" )
					therearenotmany = love.audio.newSource( "/externalassets/dialogs/level48/en-us/7therearenotmany.ogg","stream" )
					imjustthinking = love.audio.newSource( "/externalassets/dialogs/level48/en-us/13imjustthinking.ogg","stream" )
					imonmyway = love.audio.newSource( "/externalassets/dialogs/level48/en-us/16imonmyway.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level48/en-us/17yuck.ogg","stream" )
					thispotwasnt = love.audio.newSource( "/externalassets/dialogs/level48/en-us/18thispotwasnt.ogg","stream" )
					nobody = love.audio.newSource( "/externalassets/dialogs/level48/en-us/19nobody.ogg","stream" )
					whatisachair = love.audio.newSource( "/externalassets/dialogs/level48/en-us/22whatisachair.ogg","stream" )
					everybodywant = love.audio.newSource( "/externalassets/dialogs/level48/en-us/24everybodywant.ogg","stream" )
					ihaveseenthesetables = love.audio.newSource( "/externalassets/dialogs/level48/en-us/25ihaveseenthesetables.ogg","stream" )
					butheyarenice = love.audio.newSource( "/externalassets/dialogs/level48/en-us/27butheyarenice.ogg","stream" )
					itseemsthateven = love.audio.newSource( "/externalassets/dialogs/level48/en-us/29itseemsthateven.ogg","stream" )
					maybetheyusedit = love.audio.newSource( "/externalassets/dialogs/level48/en-us/31maybetheyusedit.ogg","stream" )
					wearelucky = love.audio.newSource( "/externalassets/dialogs/level48/en-us/33wearelucky.ogg","stream" )
					possibly = love.audio.newSource( "/externalassets/dialogs/level48/en-us/36possibly.ogg","stream" )
				
				end
				
	
		elseif language2=="pl" then
				ohmy = love.audio.newSource( "/externalassets/dialogs/level48/pl/1ohmy.ogg","stream" )
				yeahwe = love.audio.newSource( "/externalassets/dialogs/level48/pl/2yeahwe.ogg","stream" )
				darnedkitchen = love.audio.newSource( "/externalassets/dialogs/level48/pl/3darnedkitchen.ogg","stream" )
				andnowweare = love.audio.newSource( "/externalassets/dialogs/level48/pl/4andnowweare.ogg","stream" )
				therearenotmany = love.audio.newSource( "/externalassets/dialogs/level48/pl/7therearenotmany.ogg","stream" )
				imjustthinking = love.audio.newSource( "/externalassets/dialogs/level48/pl/13imjustthinking.ogg","stream" )
				imonmyway = love.audio.newSource( "/externalassets/dialogs/level48/pl/16imonmyway.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level48/pl/17yuck.ogg","stream" )
				thispotwasnt = love.audio.newSource( "/externalassets/dialogs/level48/pl/18thispotwasnt.ogg","stream" )
				nobody = love.audio.newSource( "/externalassets/dialogs/level48/pl/19nobody.ogg","stream" )
				whatisachair = love.audio.newSource( "/externalassets/dialogs/level48/pl/22whatisachair.ogg","stream" )
				everybodywant = love.audio.newSource( "/externalassets/dialogs/level48/pl/24everybodywant.ogg","stream" )
				ihaveseenthesetables = love.audio.newSource( "/externalassets/dialogs/level48/pl/25ihaveseenthesetables.ogg","stream" )
				butheyarenice = love.audio.newSource( "/externalassets/dialogs/level48/pl/27butheyarenice.ogg","stream" )
				itseemsthateven = love.audio.newSource( "/externalassets/dialogs/level48/pl/29itseemsthateven.ogg","stream" )
				maybetheyusedit = love.audio.newSource( "/externalassets/dialogs/level48/pl/31maybetheyusedit.ogg","stream" )
				wearelucky = love.audio.newSource( "/externalassets/dialogs/level48/pl/33wearelucky.ogg","stream" )
				possibly = love.audio.newSource( "/externalassets/dialogs/level48/pl/36possibly.ogg","stream" )
				
		elseif language2=="cs" then
				ohmy = love.audio.newSource( "/externalassets/dialogs/level48/cs/1ohmy.ogg","stream" )
				yeahwe = love.audio.newSource( "/externalassets/dialogs/level48/cs/2yeahwe.ogg","stream" )
				darnedkitchen = love.audio.newSource( "/externalassets/dialogs/level48/cs/3darnedkitchen.ogg","stream" )
				andnowweare = love.audio.newSource( "/externalassets/dialogs/level48/cs/4andnowweare.ogg","stream" )
				therearenotmany = love.audio.newSource( "/externalassets/dialogs/level48/cs/7therearenotmany.ogg","stream" )
				imjustthinking = love.audio.newSource( "/externalassets/dialogs/level48/cs/13imjustthinking.ogg","stream" )
				imonmyway = love.audio.newSource( "/externalassets/dialogs/level48/cs/16imonmyway.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level48/cs/17yuck.ogg","stream" )
				thispotwasnt = love.audio.newSource( "/externalassets/dialogs/level48/cs/18thispotwasnt.ogg","stream" )
				nobody = love.audio.newSource( "/externalassets/dialogs/level48/cs/19nobody.ogg","stream" )
				whatisachair = love.audio.newSource( "/externalassets/dialogs/level48/cs/22whatisachair.ogg","stream" )
				everybodywant = love.audio.newSource( "/externalassets/dialogs/level48/cs/24everybodywant.ogg","stream" )
				ihaveseenthesetables = love.audio.newSource( "/externalassets/dialogs/level48/cs/25ihaveseenthesetables.ogg","stream" )
				butheyarenice = love.audio.newSource( "/externalassets/dialogs/level48/cs/27butheyarenice.ogg","stream" )
				itseemsthateven = love.audio.newSource( "/externalassets/dialogs/level48/cs/29itseemsthateven.ogg","stream" )
				maybetheyusedit = love.audio.newSource( "/externalassets/dialogs/level48/cs/31maybetheyusedit.ogg","stream" )
				wearelucky = love.audio.newSource( "/externalassets/dialogs/level48/cs/33wearelucky.ogg","stream" )
				possibly = love.audio.newSource( "/externalassets/dialogs/level48/cs/36possibly.ogg","stream" )		
		elseif language2=="fr" then
				ohmy = love.audio.newSource( "/externalassets/dialogs/level48/fr/1ohmy.ogg","stream" )
				yeahwe = love.audio.newSource( "/externalassets/dialogs/level48/fr/2yeahwe.ogg","stream" )
				darnedkitchen = love.audio.newSource( "/externalassets/dialogs/level48/fr/3darnedkitchen.ogg","stream" )
				andnowweare = love.audio.newSource( "/externalassets/dialogs/level48/fr/4andnowweare.ogg","stream" )
				therearenotmany = love.audio.newSource( "/externalassets/dialogs/level48/fr/7therearenotmany.ogg","stream" )
				imjustthinking = love.audio.newSource( "/externalassets/dialogs/level48/fr/13imjustthinking.ogg","stream" )
				imonmyway = love.audio.newSource( "/externalassets/dialogs/level48/fr/16imonmyway.ogg","stream" )
				yuck = love.audio.newSource( "/externalassets/dialogs/level48/fr/17yuck.ogg","stream" )
				thispotwasnt = love.audio.newSource( "/externalassets/dialogs/level48/fr/18thispotwasnt.ogg","stream" )
				nobody = love.audio.newSource( "/externalassets/dialogs/level48/fr/19nobody.ogg","stream" )
				whatisachair = love.audio.newSource( "/externalassets/dialogs/level48/fr/22whatisachair.ogg","stream" )
				everybodywant = love.audio.newSource( "/externalassets/dialogs/level48/fr/24everybodywant.ogg","stream" )
				ihaveseenthesetables = love.audio.newSource( "/externalassets/dialogs/level48/fr/25ihaveseenthesetables.ogg","stream" )
				butheyarenice = love.audio.newSource( "/externalassets/dialogs/level48/fr/27butheyarenice.ogg","stream" )
				itseemsthateven = love.audio.newSource( "/externalassets/dialogs/level48/fr/29itseemsthateven.ogg","stream" )
				maybetheyusedit = love.audio.newSource( "/externalassets/dialogs/level48/fr/31maybetheyusedit.ogg","stream" )
				wearelucky = love.audio.newSource( "/externalassets/dialogs/level48/fr/33wearelucky.ogg","stream" )
				possibly = love.audio.newSource( "/externalassets/dialogs/level48/fr/36possibly.ogg","stream" )	
		end
				--fish 2 effects
				ohmy:setEffect('myEffect')
				yeahwe:setEffect('myEffect')
				darnedkitchen:setEffect('myEffect')
				andnowweare:setEffect('myEffect')
				therearenotmany:setEffect('myEffect')
				imjustthinking:setEffect('myEffect')
				imonmyway:setEffect('myEffect')
				yuck:setEffect('myEffect')
				thispotwasnt:setEffect('myEffect')
				nobody:setEffect('myEffect')
				whatisachair:setEffect('myEffect')
				everybodywant:setEffect('myEffect')
				ihaveseenthesetables:setEffect('myEffect')
				butheyarenice:setEffect('myEffect')
				itseemsthateven:setEffect('myEffect')
				maybetheyusedit:setEffect('myEffect')
				wearelucky:setEffect('myEffect')
				possibly:setEffect('myEffect')
				
				--fish 1 effects
				idontthinkhe:setEffect('myEffect')
				butthisisnot:setEffect('myEffect')
				butjusthavealook:setEffect('myEffect')
				icantunderstand:setEffect('myEffect')
				thesetables:setEffect('myEffect')
				theselittle:setEffect('myEffect')
				icantgetoutof:setEffect('myEffect')
				helpmesomehow:setEffect('myEffect')
				iftherewasnochair:setEffect('myEffect')
				especiallymyself:setEffect('myEffect')
				thereshouldbe:setEffect('myEffect')
				theyaremass:setEffect('myEffect')
				imafraidthatonly:setEffect('myEffect')
				whyisthatsword:setEffect('myEffect')
				certainly:setEffect('myEffect')
				theremustbesome:setEffect('myEffect')
				theremustbesomeculinary:setEffect('myEffect')
		
		elseif nLevel==49 then
		
		if language=="en" then
		--parrot
				cruecruel = love.audio.newSource( "externalassets/dialogs/level45/en/parrot/1cruelcruel.ogg","stream" )
				cruelcaptain = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/3cruelcaptain.ogg","stream" )
				goodgrief = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/5goodgrief.ogg","stream" )
				trickyproblem = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/11trickyproblem.ogg","stream" )
				pollywantsacracker = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/14pollywantsacracker.ogg","stream" )
				beatitsir = love.audio.newSource( "/externalassets/dialogs/level45/en/parrot/15beatitsir.ogg","stream" )
		
			if accent=="br" then
					--player 1
					didntwesee = love.audio.newSource( "/externalassets/dialogs/level49/en/1didntwesee.ogg","stream" )
					lookatthatparrot = love.audio.newSource( "/externalassets/dialogs/level49/en/3lookatthatparrot.ogg","stream" )
					andwhatwould = love.audio.newSource( "/externalassets/dialogs/level49/en/6andwhatwould.ogg","stream" )
					theytoldus = love.audio.newSource( "/externalassets/dialogs/level49/en/7theytoldus.ogg","stream" )
					dontbesoannoying = love.audio.newSource( "/externalassets/dialogs/level49/en/8dontbesoannoying.ogg","stream" )
					sowhatifwe = love.audio.newSource( "/externalassets/dialogs/level49/en/10sowhatifwe.ogg","stream" )
					imsorry = love.audio.newSource( "/externalassets/dialogs/level49/en/11imsorry.ogg","stream" )
					skeletons = love.audio.newSource( "/externalassets/dialogs/level49/en/14skeletons.ogg","stream" )
					letstrytothink = love.audio.newSource( "/externalassets/dialogs/level49/en/17letstrytothink.ogg","stream" )
					mybackaches = love.audio.newSource( "/externalassets/dialogs/level49/en/20mybackaches.ogg","stream" )
			elseif accent=="us" then
			--player 1
					didntwesee = love.audio.newSource( "/externalassets/dialogs/level49/en-us/1didntwesee.ogg","stream" )
					lookatthatparrot = love.audio.newSource( "/externalassets/dialogs/level49/en-us/3lookatthatparrot.ogg","stream" )
					andwhatwould = love.audio.newSource( "/externalassets/dialogs/level49/en-us/6andwhatwould.ogg","stream" )
					theytoldus = love.audio.newSource( "/externalassets/dialogs/level49/en-us/7theytoldus.ogg","stream" )
					dontbesoannoying = love.audio.newSource( "/externalassets/dialogs/level49/en-us/8dontbesoannoying.ogg","stream" )
					sowhatifwe = love.audio.newSource( "/externalassets/dialogs/level49/en-us/10sowhatifwe.ogg","stream" )
					imsorry = love.audio.newSource( "/externalassets/dialogs/level49/en-us/11imsorry.ogg","stream" )
					skeletons = love.audio.newSource( "/externalassets/dialogs/level49/en-us/14skeletons.ogg","stream" )
					letstrytothink = love.audio.newSource( "/externalassets/dialogs/level49/en-us/17letstrytothink.ogg","stream" )
					mybackaches = love.audio.newSource( "/externalassets/dialogs/level49/en-us/20mybackaches.ogg","stream" )
			end
		elseif language=="fr" then
				--player 1
					didntwesee = love.audio.newSource( "/externalassets/dialogs/level49/fr/1didntwesee.ogg","stream" )
					lookatthatparrot = love.audio.newSource( "/externalassets/dialogs/level49/fr/3lookatthatparrot.ogg","stream" )
					andwhatwould = love.audio.newSource( "/externalassets/dialogs/level49/fr/6andwhatwould.ogg","stream" )
					theytoldus = love.audio.newSource( "/externalassets/dialogs/level49/fr/7theytoldus.ogg","stream" )
					dontbesoannoying = love.audio.newSource( "/externalassets/dialogs/level49/fr/8dontbesoannoying.ogg","stream" )
					sowhatifwe = love.audio.newSource( "/externalassets/dialogs/level49/fr/10sowhatifwe.ogg","stream" )
					imsorry = love.audio.newSource( "/externalassets/dialogs/level49/fr/11imsorry.ogg","stream" )
					skeletons = love.audio.newSource( "/externalassets/dialogs/level49/fr/14skeletons.ogg","stream" )
					letstrytothink = love.audio.newSource( "/externalassets/dialogs/level49/fr/17letstrytothink.ogg","stream" )
					mybackaches = love.audio.newSource( "/externalassets/dialogs/level49/fr/20mybackaches.ogg","stream" )

		end
		
		if language2=="en" then
			if accent2=="br" then		
					--[[--player 2
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/en/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/en/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/en/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/en/13hmmthanks.ogg","stream" )
					whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/en/15whoareprobably.ogg","stream" )
					hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/en/17hmmidont.ogg","stream" )
					solesshed = love.audio.newSource( "/externalassets/dialogs/level49/en/18soletsshed.ogg","stream" )--]]
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/en-us/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/en-us/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/en-us/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en-us/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en-us/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/en-us/13hmmthanks.ogg","stream" )
					whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/en-us/15whoareprobably.ogg","stream" )
					hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/en-us/17hmmidont.ogg","stream" )
					solesshed = love.audio.newSource( "/externalassets/dialogs/level49/en-us/18soletsshed.ogg","stream" )
			elseif accent2=="us" then
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/en-us/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/en-us/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/en-us/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en-us/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/en-us/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/en-us/13hmmthanks.ogg","stream" )
					whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/en-us/15whoareprobably.ogg","stream" )
					hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/en-us/17hmmidont.ogg","stream" )
					solesshed = love.audio.newSource( "/externalassets/dialogs/level49/en-us/18soletsshed.ogg","stream" )
			end
		elseif language2=="pl" then
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/pl/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/pl/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/pl/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/pl/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/pl/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/pl/13hmmthanks.ogg","stream" )
					--whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/pl/15whoareprobably.ogg","stream" )
					--hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/pl/17hmmidont.ogg","stream" )
					--solesshed = love.audio.newSource( "/externalassets/dialogs/level49/pl/18soletsshed.ogg","stream" )
		elseif language2=="cs" then
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/cs/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/cs/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/cs/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/cs/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/cs/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/cs/13hmmthanks.ogg","stream" )
					--whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/cs/15whoareprobably.ogg","stream" )
					--hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/cs/17hmmidont.ogg","stream" )
					--solesshed = love.audio.newSource( "/externalassets/dialogs/level49/cs/18soletsshed.ogg","stream" )
		elseif language2=="fr" then
					noyouarewrong = love.audio.newSource( "/externalassets/dialogs/level49/fr/2noyouarewrong.ogg","stream" )
					sowhat = love.audio.newSource( "/externalassets/dialogs/level49/fr/4sowhat.ogg","stream" )
					canyouseeit = love.audio.newSource( "/externalassets/dialogs/level49/fr/5canyouseeit.ogg","stream" )
					thisoctopus = love.audio.newSource( "/externalassets/dialogs/level49/fr/9thisoctopus.ogg","stream" )
					littleoctopus = love.audio.newSource( "/externalassets/dialogs/level49/fr/12littleoctopus.ogg","stream" )
					hmmthanks = love.audio.newSource( "/externalassets/dialogs/level49/fr/13hmmthanks.ogg","stream" )
					whoareprobably = love.audio.newSource( "/externalassets/dialogs/level49/fr/15whoareprobably.ogg","stream" )
					hmmidont = love.audio.newSource( "/externalassets/dialogs/level49/fr/17hmmidont.ogg","stream" )
					solesshed = love.audio.newSource( "/externalassets/dialogs/level49/fr/18soletsshed.ogg","stream" )
		end		
					
					--fish 2 effects
					noyouarewrong:setEffect('myEffect')
					sowhat:setEffect('myEffect')
					canyouseeit:setEffect('myEffect')
					thisoctopus:setEffect('myEffect')
					littleoctopus:setEffect('myEffect')
					hmmthanks:setEffect('myEffect')
					--whoareprobably:setEffect('myEffect')
					--hmmidont:setEffect('myEffect')
					--solesshed:setEffect('myEffect')
					
					--fish 1 effects	
					didntwesee:setEffect('myEffect')
					lookatthatparrot:setEffect('myEffect')
					andwhatwould:setEffect('myEffect')
					theytoldus:setEffect('myEffect')
					dontbesoannoying:setEffect('myEffect')
					sowhatifwe:setEffect('myEffect')
					imsorry:setEffect('myEffect')
					skeletons:setEffect('myEffect')
					letstrytothink:setEffect('myEffect')
					mybackaches:setEffect('myEffect')
		
				--parrot's effects
							
				cruecruel:setEffect('myEffect')
				cruelcaptain:setEffect('myEffect')
				goodgrief:setEffect('myEffect')
				trickyproblem:setEffect('myEffect')
				pollywantsacracker:setEffect('myEffect')
				beatitsir:setEffect('myEffect')

elseif nLevel==50 then
		
		
		if language=="en" then
			
				--skeleton
				haventyouseenmyeye = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/1haventyouseenmyeye.ogg","stream" )
				thisscarfisveryimportant = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/2thisscarfisveryimportant.ogg","stream" )
				thehumanskull = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/3thehumanskull.ogg","stream" )
				afterthatunfortunateaccident = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/4afterthatunfortunateaccident.ogg","stream" )
				whyamihereafterall = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/5whyamIhereafterall.ogg","stream" )
				asiftheycantputsomechest = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/6asiftheycantputsomechest.ogg","stream" )
				doyouappreciatemyfacial = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/7doyouappreciatemyfacial.ogg","stream" )
				notbadforaskeleton = love.audio.newSource( "/externalassets/dialogs/level50/en/skeleton/8notbadforaskeleton.ogg","stream" )

				if accent=="br" then
					thiswassurely = love.audio.newSource( "/externalassets/dialogs/level50/en/2thiswassurely.ogg","stream" )
					whatwouldyouexpect = love.audio.newSource( "/externalassets/dialogs/level50/en/3whatwouldyouexpect.ogg","stream" )
					thisisashiphook = love.audio.newSource( "/externalassets/dialogs/level50/en/5thisisashiphook.ogg","stream" )
					thiseyesquint = love.audio.newSource( "/externalassets/dialogs/level50/en/7thiseyesquint.ogg","stream" )
				elseif accent=="us" then
					thiswassurely = love.audio.newSource( "/externalassets/dialogs/level50/en-us/2thiswassurely.ogg","stream" )
					whatwouldyouexpect = love.audio.newSource( "/externalassets/dialogs/level50/en-us/3whatwouldyouexpect.ogg","stream" )
					thisisashiphook = love.audio.newSource( "/externalassets/dialogs/level50/en-us/5thisisashiphook.ogg","stream" )
					thiseyesquint = love.audio.newSource( "/externalassets/dialogs/level50/en-us/7thiseyesquint.ogg","stream" )
				end
		elseif language=="fr" then
				--skeleton
				haventyouseenmyeye = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/1haventyouseenmyeye.ogg","stream" )
				thisscarfisveryimportant = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/2thisscarfisveryimportant.ogg","stream" )
				thehumanskull = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/3thehumanskull.ogg","stream" )
				afterthatunfortunateaccident = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/4afterthatunfortunateaccident.ogg","stream" )
				whyamihereafterall = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/5whyamIhereafterall.ogg","stream" )
				asiftheycantputsomechest = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/6asiftheycantputsomechest.ogg","stream" )
				doyouappreciatemyfacial = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/7doyouappreciatemyfacial.ogg","stream" )
				notbadforaskeleton = love.audio.newSource( "/externalassets/dialogs/level50/fr/skeleton/8notbadforaskeleton.ogg","stream" )
				
				
					thiswassurely = love.audio.newSource( "/externalassets/dialogs/level50/fr/2thiswassurely.ogg","stream" )
					whatwouldyouexpect = love.audio.newSource( "/externalassets/dialogs/level50/fr/3whatwouldyouexpect.ogg","stream" )
					thisisashiphook = love.audio.newSource( "/externalassets/dialogs/level50/fr/5thisisashiphook.ogg","stream" )
					thiseyesquint = love.audio.newSource( "/externalassets/dialogs/level50/fr/7thiseyesquint.ogg","stream" )
		
		elseif language=="es" then
				
					if accent=="es" then
				elseif accent=="la" then
					thiswassurely = love.audio.newSource( "/externalassets/dialogs/level50/es-la/2thiswassurely.ogg","stream" )
					whatwouldyouexpect = love.audio.newSource( "/externalassets/dialogs/level50/es-la/3whatwouldyouexpect.ogg","stream" )
					thisisashiphook = love.audio.newSource( "/externalassets/dialogs/level50/es-la/5thisisashiphook.ogg","stream" )
					thiseyesquint = love.audio.newSource( "/externalassets/dialogs/level50/es-la/7thiseyesquint.ogg","stream" )
				end
				
				--skeleton
				haventyouseenmyeye = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/1haventyouseenmyeye.ogg","stream" )
				thisscarfisveryimportant = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/2thisscarfisveryimportant.ogg","stream" )
				thehumanskull = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/3thehumanskull.ogg","stream" )
				afterthatunfortunateaccident = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/4afterthatunfortunateaccident.ogg","stream" )
				whyamihereafterall = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/5whyamIhereafterall.ogg","stream" )
				asiftheycantputsomechest = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/6asiftheycantputsomechest.ogg","stream" )
				doyouappreciatemyfacial = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/7doyouappreciatemyfacial.ogg","stream" )
				notbadforaskeleton = love.audio.newSource( "/externalassets/dialogs/level50/es-la/skeleton/8notbadforaskeleton.ogg","stream" )

		end
		
			if language2=="en" then
				if accent2=="br" then
					thereisalotofgarbage = love.audio.newSource( "/externalassets/dialogs/level50/en/1thereisalotofgarbage.ogg","stream" )
					doyouthinkthatsilver = love.audio.newSource( "/externalassets/dialogs/level50/en/4doyouthinkthatsilver.ogg","stream" )
					itsastrangelookingeye = love.audio.newSource( "/externalassets/dialogs/level50/en/6itsastrangelookingeye.ogg","stream" )				
				elseif accent2=="us" then
				
					thereisalotofgarbage = love.audio.newSource( "/externalassets/dialogs/level50/en-us/1thereisalotofgarbage.ogg","stream" )
					doyouthinkthatsilver = love.audio.newSource( "/externalassets/dialogs/level50/en-us/4doyouthinkthatsilver.ogg","stream" )
					itsastrangelookingeye = love.audio.newSource( "/externalassets/dialogs/level50/en-us/6itsastrangelookingeye.ogg","stream" )				
				end
			
		elseif language2=="pl" then
		
				--skeleton
				haventyouseenmyeye = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/1haventyouseenmyeye.ogg","stream" )
				thisscarfisveryimportant = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/2thisscarfisveryimportant.ogg","stream" )
				thehumanskull = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/3thehumanskull.ogg","stream" )
				afterthatunfortunateaccident = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/4afterthatunfortunateaccident.ogg","stream" )
				whyamihereafterall = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/5whyamIhereafterall.ogg","stream" )
				asiftheycantputsomechest = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/6asiftheycantputsomechest.ogg","stream" )
				doyouappreciatemyfacial = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/7doyouappreciatemyfacial.ogg","stream" )
				notbadforaskeleton = love.audio.newSource( "/externalassets/dialogs/level50/pl/skeleton/8notbadforaskeleton.ogg","stream" )
		
					thereisalotofgarbage = love.audio.newSource( "/externalassets/dialogs/level50/pl/1thereisalotofgarbage.ogg","stream" )
					doyouthinkthatsilver = love.audio.newSource( "/externalassets/dialogs/level50/pl/4doyouthinkthatsilver.ogg","stream" )
					itsastrangelookingeye = love.audio.newSource( "/externalassets/dialogs/level50/pl/6itsastrangelookingeye.ogg","stream" )	
	
		elseif language2=="cs" then
				
					thereisalotofgarbage = love.audio.newSource( "/externalassets/dialogs/level50/cs/1thereisalotofgarbage.ogg","stream" )
					doyouthinkthatsilver = love.audio.newSource( "/externalassets/dialogs/level50/cs/4doyouthinkthatsilver.ogg","stream" )
					itsastrangelookingeye = love.audio.newSource( "/externalassets/dialogs/level50/cs/6itsastrangelookingeye.ogg","stream" )	
		
		elseif language2=="fr" then
				
					thereisalotofgarbage = love.audio.newSource( "/externalassets/dialogs/level50/fr/1thereisalotofgarbage.ogg","stream" )
					doyouthinkthatsilver = love.audio.newSource( "/externalassets/dialogs/level50/fr/4doyouthinkthatsilver.ogg","stream" )
					itsastrangelookingeye = love.audio.newSource( "/externalassets/dialogs/level50/fr/6itsastrangelookingeye.ogg","stream" )
					
				--skeleton effects
				haventyouseenmyeye:setEffect('myEffect')
				thisscarfisveryimportant:setEffect('myEffect')
				thehumanskull:setEffect('myEffect')
				afterthatunfortunateaccident:setEffect('myEffect')
				whyamihereafterall:setEffect('myEffect')
				asiftheycantputsomechest:setEffect('myEffect')
				doyouappreciatemyfacial:setEffect('myEffect')
				notbadforaskeleton:setEffect('myEffect')
		
				--fish 2 effects
				thereisalotofgarbage:setEffect('myEffect')
				doyouthinkthatsilver:setEffect('myEffect')
				itsastrangelookingeye:setEffect('myEffect')
				--fish 1 effects
				thiswassurely:setEffect('myEffect')
				whatwouldyouexpect:setEffect('myEffect')
				thisisashiphook:setEffect('myEffect')
				thiseyesquint:setEffect('myEffect')
		end
		
		elseif nLevel==51 then
		
		if language=="en" then
		
		loadBorderDialogs()
		
		--end
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/en/6andcannotremember.ogg","stream" )
		
		if accent=="br" then
		-- player 1
		
			ohwell = love.audio.newSource( "/externalassets/dialogs/level51/en/1ohwell.ogg","stream" )
			ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/en/4ofcourse.ogg","stream" )
			ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/en/6ourgoal.ogg","stream" )
			whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/en/7whatcanbe.ogg","stream" )
			doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/en/8doyouthink.ogg","stream" )
			youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/en/11youmeanescargots.ogg","stream" )
			weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en/12weneedmore.ogg","stream" )
			theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/en/13theeasiestway.ogg","stream" )
			letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/en/16letskeepup.ogg","stream" )

			elseif accent=="us" then
			
			ohwell = love.audio.newSource( "/externalassets/dialogs/level51/en-us/1ohwell.ogg","stream" )
			ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/en-us/4ofcourse.ogg","stream" )
			ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/en-us/6ourgoal.ogg","stream" )
			whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/en-us/7whatcanbe.ogg","stream" )
			doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/en-us/8doyouthink.ogg","stream" )
			youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/en-us/11youmeanescargots.ogg","stream" )
			weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en-us/12weneedmore.ogg","stream" )
			theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/en-us/13theeasiestway.ogg","stream" )
			letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/en-us/16letskeepup.ogg","stream" )
		end
		
		elseif language=="fr" then
		
		-- player 1
		
		ohwell = love.audio.newSource( "/externalassets/dialogs/level51/fr/1ohwell.ogg","stream" )
		ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/fr/4ofcourse.ogg","stream" )
		ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/fr/6ourgoal.ogg","stream" )
		whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/fr/7whatcanbe.ogg","stream" )
		doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/fr/8doyouthink.ogg","stream" )
		youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/fr/11youmeanescargots.ogg","stream" )
		weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/fr/12weneedmore.ogg","stream" )
		theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/fr/13theeasiestway.ogg","stream" )
		letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/fr/16letskeepup.ogg","stream" )
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/fr/6andcannotremember.ogg","stream" )

		elseif language=="de" then
		
		-- player 1		English temporarily till an actor is found
		
		ohwell = love.audio.newSource( "/externalassets/dialogs/level51/en/1ohwell.ogg","stream" )
		ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/en/4ofcourse.ogg","stream" )
		ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/en/6ourgoal.ogg","stream" )
		whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/en/7whatcanbe.ogg","stream" )
		doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/en/8doyouthink.ogg","stream" )
		youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/en/11youmeanescargots.ogg","stream" )
		weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en/12weneedmore.ogg","stream" )
		theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/en/13theeasiestway.ogg","stream" )
		letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/en/16letskeepup.ogg","stream" )
		
			--end
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/de/6andcannotremember.ogg","stream" )
		elseif language=="es" then
		
		-- player 1		English temporarily till an actor is found
		
		ohwell = love.audio.newSource( "/externalassets/dialogs/level51/es-la/1ohwell.ogg","stream" )
		ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/es-la/4ofcourse.ogg","stream" )
		ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/es-la/6ourgoal.ogg","stream" )
		whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/es-la/7whatcanbe.ogg","stream" )
		doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/es-la/8doyouthink.ogg","stream" )
		youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/es-la/11youmeanescargots.ogg","stream" )
		weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/es-la/12weneedmore.ogg","stream" )
		theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/es-la/13theeasiestway.ogg","stream" )
		letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/es-la/16letskeepup.ogg","stream" )
		
		--Narrator
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/es/6andcannotremember.ogg","stream" )
		
		elseif language=="sv" then
		
		-- player 1		English temporarily till an actor is found
		
		ohwell = love.audio.newSource( "/externalassets/dialogs/level51/en/1ohwell.ogg","stream" )
		ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/en/4ofcourse.ogg","stream" )
		ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/en/6ourgoal.ogg","stream" )
		whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/en/7whatcanbe.ogg","stream" )
		doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/en/8doyouthink.ogg","stream" )
		youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/en/11youmeanescargots.ogg","stream" )
		weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en/12weneedmore.ogg","stream" )
		theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/en/13theeasiestway.ogg","stream" )
		letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/en/16letskeepup.ogg","stream" )
		
			--end
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/sv/6andcannotremember.ogg","stream" )
		
		elseif language=="nl" then
		
		-- player 1
		
		ohwell = love.audio.newSource( "/externalassets/dialogs/level51/nl/1ohwell.ogg","stream" )
		ofcourse = love.audio.newSource( "/externalassets/dialogs/level51/nl/4ofcourse.ogg","stream" )
		ourgoal = love.audio.newSource( "/externalassets/dialogs/level51/nl/6ourgoal.ogg","stream" )
		whatcanbe = love.audio.newSource( "/externalassets/dialogs/level51/nl/7whatcanbe.ogg","stream" )
		doyouthink = love.audio.newSource( "/externalassets/dialogs/level51/nl/8doyouthink.ogg","stream" )
		youmeanescargots = love.audio.newSource( "/externalassets/dialogs/level51/nl/11youmeanescargots.ogg","stream" )
		weneedmore = love.audio.newSource( "/externalassets/dialogs/level51/nl/12weneedmore.ogg","stream" )
		theeasiestway = love.audio.newSource( "/externalassets/dialogs/level51/nl/13theeasiestway.ogg","stream" )
		letskeepup = love.audio.newSource( "/externalassets/dialogs/level51/nl/16letskeepup.ogg","stream" )
		
		aftermuchhardship  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/1aftermuchhardship.ogg","stream" )
		initialenthusiasm  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/2initialenthusiasm.ogg","stream" )
		whenwefound  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/3whenwefound.ogg","stream" )
		buttheplaceofresidence  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/4buttheplaceofresidence.ogg","stream" )
		thatunfortunately  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/5thatunfortunately.ogg","stream" )
		andcannotremember  = love.audio.newSource( "/externalassets/ends/2silversship/audios/nl/6andcannotremember.ogg","stream" )
		
		end
		if language2=="en" then
			if accent=="br" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/en-us/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/en-us/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/en-us/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/en-us/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en-us/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/en-us/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/en-us/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/en-us/17yesitsalmostdone.ogg","stream" )
			elseif accent2=="us" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/en-us/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/en-us/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/en-us/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/en-us/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en-us/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/en-us/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/en-us/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/en-us/17yesitsalmostdone.ogg","stream" )
				
		
				
			end
		elseif language2=="pl" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/pl/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/pl/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/pl/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/pl/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/pl/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/pl/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/pl/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/pl/17yesitsalmostdone.ogg","stream" )
		elseif language2=="cs" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/cs/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/cs/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/cs/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/cs/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/cs/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/cs/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/cs/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/cs/17yesitsalmostdone.ogg","stream" )
				
				
		elseif language2=="es" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/en-us/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/en-us/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/en-us/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/en-us/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/en-us/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/en-us/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/en-us/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/en-us/17yesitsalmostdone.ogg","stream" )
		
		elseif language2=="fr" then
				okhereisthemap = love.audio.newSource( "/externalassets/dialogs/level51/fr/2okhereisthemap.ogg","stream" )
				soitsclearnow = love.audio.newSource( "/externalassets/dialogs/level51/fr/3soitsclearnow.ogg","stream" )
				donttalknonsense = love.audio.newSource( "/externalassets/dialogs/level51/fr/5donttalknonsense.ogg","stream" )
				weshall = love.audio.newSource( "/externalassets/dialogs/level51/fr/9weshall.ogg","stream" )
				wedneedmore = love.audio.newSource( "/externalassets/dialogs/level51/fr/10wedneedmore.ogg","stream" )
				dontpretend = love.audio.newSource( "/externalassets/dialogs/level51/fr/14dontpretend.ogg","stream" )
				sowemanaged = love.audio.newSource( "/externalassets/dialogs/level51/fr/15sowemanaged.ogg","stream" )
				yesitsalmostdone = love.audio.newSource( "/externalassets/dialogs/level51/fr/17yesitsalmostdone.ogg","stream" )
		end
		
		
		--Narrator
		aftermuchhardship:setEffect('myEffect')
		initialenthusiasm:setEffect('myEffect')
		whenwefound:setEffect('myEffect')
		buttheplaceofresidence:setEffect('myEffect')
		thatunfortunately:setEffect('myEffect')
		andcannotremember:setEffect('myEffect')
		
		--fish 1
		
		ohwell:setEffect('myEffect')
		ofcourse:setEffect('myEffect')
		ourgoal:setEffect('myEffect')
		whatcanbe:setEffect('myEffect')
		doyouthink:setEffect('myEffect')
		youmeanescargots:setEffect('myEffect')
		weneedmore:setEffect('myEffect')
		theeasiestway:setEffect('myEffect')
		letskeepup:setEffect('myEffect')
		
		--fish 2
		
				okhereisthemap:setEffect('myEffect')
				soitsclearnow:setEffect('myEffect')
				donttalknonsense:setEffect('myEffect')
				weshall:setEffect('myEffect')
				wedneedmore:setEffect('myEffect')
				dontpretend:setEffect('myEffect')
				sowemanaged:setEffect('myEffect')
				yesitsalmostdone:setEffect('myEffect')
		
		elseif nLevel==52 then
		
		
			if language=="en" then
				if accent=="br" then
				
					whatcoulditbe = love.audio.newSource( "/externalassets/dialogs/level52/en/2whatcoulditbe.ogg","stream" )
					theylooklike = love.audio.newSource( "/externalassets/dialogs/level52/en/5theylooklike.ogg","stream" )
					unless = love.audio.newSource( "/externalassets/dialogs/level52/en/6unless.ogg","stream" )
					thatsounds = love.audio.newSource( "/externalassets/dialogs/level52/en/8thatsounds.ogg","stream" )
					sowhatisit = love.audio.newSource( "/externalassets/dialogs/level52/en/10sowhatisit.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level52/en/12idrather.ogg","stream" )
					welljusthowmany = love.audio.newSource( "/externalassets/dialogs/level52/en/14welljusthowmany.ogg","stream" )
					yousee = love.audio.newSource( "/externalassets/dialogs/level52/en/15yousee.ogg","stream" )
					dontthink = love.audio.newSource( "/externalassets/dialogs/level52/en/17dontthink.ogg","stream" )

				elseif accent=="us" then
					whatcoulditbe = love.audio.newSource( "/externalassets/dialogs/level52/en-us/2whatcoulditbe.ogg","stream" )
					theylooklike = love.audio.newSource( "/externalassets/dialogs/level52/en-us/5theylooklike.ogg","stream" )
					unless = love.audio.newSource( "/externalassets/dialogs/level52/en-us/6unless.ogg","stream" )
					thatsounds = love.audio.newSource( "/externalassets/dialogs/level52/en-us/8thatsounds.ogg","stream" )
					sowhatisit = love.audio.newSource( "/externalassets/dialogs/level52/en-us/10sowhatisit.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level52/en-us/12idrather.ogg","stream" )
					welljusthowmany = love.audio.newSource( "/externalassets/dialogs/level52/en-us/14welljusthowmany.ogg","stream" )
					yousee = love.audio.newSource( "/externalassets/dialogs/level52/en-us/15yousee.ogg","stream" )
					dontthink = love.audio.newSource( "/externalassets/dialogs/level52/en-us/17dontthink.ogg","stream" )
				end
			elseif language=="fr" then
					whatcoulditbe = love.audio.newSource( "/externalassets/dialogs/level52/fr/2whatcoulditbe.ogg","stream" )
					theylooklike = love.audio.newSource( "/externalassets/dialogs/level52/fr/5theylooklike.ogg","stream" )
					unless = love.audio.newSource( "/externalassets/dialogs/level52/fr/6unless.ogg","stream" )
					thatsounds = love.audio.newSource( "/externalassets/dialogs/level52/fr/8thatsounds.ogg","stream" )
					sowhatisit = love.audio.newSource( "/externalassets/dialogs/level52/fr/10sowhatisit.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level52/fr/12idrather.ogg","stream" )
					welljusthowmany = love.audio.newSource( "/externalassets/dialogs/level52/fr/14welljusthowmany.ogg","stream" )
					yousee = love.audio.newSource( "/externalassets/dialogs/level52/fr/15yousee.ogg","stream" )
					dontthink = love.audio.newSource( "/externalassets/dialogs/level52/fr/17dontthink.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					whatcoulditbe = love.audio.newSource( "/externalassets/dialogs/level52/es-la/2whatcoulditbe.ogg","stream" )
					theylooklike = love.audio.newSource( "/externalassets/dialogs/level52/es-la/5theylooklike.ogg","stream" )
					unless = love.audio.newSource( "/externalassets/dialogs/level52/es-la/6unless.ogg","stream" )
					thatsounds = love.audio.newSource( "/externalassets/dialogs/level52/es-la/8thatsounds.ogg","stream" )
					sowhatisit = love.audio.newSource( "/externalassets/dialogs/level52/es-la/10sowhatisit.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level52/es-la/12idrather.ogg","stream" )
					welljusthowmany = love.audio.newSource( "/externalassets/dialogs/level52/es-la/14welljusthowmany.ogg","stream" )
					yousee = love.audio.newSource( "/externalassets/dialogs/level52/es-la/15yousee.ogg","stream" )
					dontthink = love.audio.newSource( "/externalassets/dialogs/level52/es-la/17dontthink.ogg","stream" )
				end
			end
			
			if language2=="en" then
				
				if accent2=="br" then
				
					mygod = love.audio.newSource( "/externalassets/dialogs/level52/en/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/en/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/en/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/en/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/en/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/en/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/en/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/en/16ihope.ogg","stream" )

				elseif accent2=="us" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level52/en-us/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/en-us/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/en-us/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/en-us/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/en-us/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/en-us/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/en-us/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/en-us/16ihope.ogg","stream" )

				end
			elseif language2=="nl" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level52/nl/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/nl/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/nl/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/nl/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/nl/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/nl/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/nl/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/nl/16ihope.ogg","stream" )	
			
			elseif language2=="pl" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level52/pl/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/pl/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/pl/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/pl/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/pl/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/pl/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/pl/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/pl/16ihope.ogg","stream" )	
			elseif language2=="cs" then
					mygod = love.audio.newSource( "/externalassets/dialogs/level52/cs/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/cs/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/cs/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/cs/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/cs/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/cs/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/cs/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/cs/16ihope.ogg","stream" )		
			elseif language2=="fr" then
					--[[mygod = love.audio.newSource( "/externalassets/dialogs/level52/fr/1mygod.ogg","stream" )
					ihavenoidea = love.audio.newSource( "/externalassets/dialogs/level52/fr/3ihavenoidea.ogg","stream" )
					whatdoyouthinkufo = love.audio.newSource( "/externalassets/dialogs/level52/fr/4whatdoyouthink.ogg","stream" )
					itcouldofcourse = love.audio.newSource( "/externalassets/dialogs/level52/fr/7itcouldofcourse.ogg","stream" )
					thenagain = love.audio.newSource( "/externalassets/dialogs/level52/fr/9thenagain.ogg","stream" )
					howshouldiknow = love.audio.newSource( "/externalassets/dialogs/level52/fr/11howshouldiknow.ogg","stream" )
					buthowdo = love.audio.newSource( "/externalassets/dialogs/level52/fr/13buthowdo.ogg","stream" )
					ihope = love.audio.newSource( "/externalassets/dialogs/level52/fr/16ihope.ogg","stream" )
						--]]
					--fish 2 effects
					mygod:setEffect('myEffect')
					ihavenoidea:setEffect('myEffect')
					whatdoyouthink:setEffect('myEffect')
					itcouldofcourse:setEffect('myEffect')
					thenagain:setEffect('myEffect')
					howshouldiknow:setEffect('myEffect')
					buthowdo:setEffect('myEffect')
					ihope:setEffect('myEffect')
	
					-- fish 1 effects
					whatcoulditbe:setEffect('myEffect')
					theylooklike:setEffect('myEffect')
					unless:setEffect('myEffect')
					thatsounds:setEffect('myEffect')
					sowhatisit:setEffect('myEffect')
					idrather:setEffect('myEffect')
					welljusthowmany:setEffect('myEffect')
					yousee:setEffect('myEffect')
					dontthink:setEffect('myEffect')
			end
			
		elseif nLevel==53 then
		
		
			if language=="en" then
				if accent=="br" then
		
					
					lookufo = love.audio.newSource( "/externalassets/dialogs/level53/en/1look.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level53/en/3ithink.ogg","stream" )
					ithinkthatufo = love.audio.newSource( "/externalassets/dialogs/level53/en/5ithinkthatufo.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level53/en/8howcould.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/en/10thisis.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/en/13maybe.ogg","stream" )
					stoppointing = love.audio.newSource( "/externalassets/dialogs/level53/en/15stoppointing.ogg","stream" )
					dontforget = love.audio.newSource( "/externalassets/dialogs/level53/en/18dontforget.ogg","stream" )
					isquiet = love.audio.newSource( "/externalassets/dialogs/level53/en/21itsquiet.ogg","stream" )
					thismagnetic = love.audio.newSource( "/externalassets/dialogs/level53/en/23thismagnetic.ogg","stream" )
					
				elseif accent=="us" then
					lookufo = love.audio.newSource( "/externalassets/dialogs/level53/en-us/1look.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level53/en-us/3ithink.ogg","stream" )
					ithinkthatufo = love.audio.newSource( "/externalassets/dialogs/level53/en-us/5ithinkthatufo.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level53/en-us/8howcould.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/en-us/10thisis.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/en-us/13maybe.ogg","stream" )
					stoppointing = love.audio.newSource( "/externalassets/dialogs/level53/en-us/15stoppointing.ogg","stream" )
					dontforget = love.audio.newSource( "/externalassets/dialogs/level53/en-us/18dontforget.ogg","stream" )
					isquiet = love.audio.newSource( "/externalassets/dialogs/level53/en-us/21itsquiet.ogg","stream" )
					thismagnetic = love.audio.newSource( "/externalassets/dialogs/level53/en-us/23thismagnetic.ogg","stream" )
				end
			elseif language=="fr" then
					lookufo = love.audio.newSource( "/externalassets/dialogs/level53/fr/1look.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level53/fr/3ithink.ogg","stream" )
					ithinkthatufo = love.audio.newSource( "/externalassets/dialogs/level53/fr/5ithinkthatufo.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level53/fr/8howcould.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/fr/10thisis.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/fr/13maybe.ogg","stream" )
					stoppointing = love.audio.newSource( "/externalassets/dialogs/level53/fr/15stoppointing.ogg","stream" )
					dontforget = love.audio.newSource( "/externalassets/dialogs/level53/fr/18dontforget.ogg","stream" )
					isquiet = love.audio.newSource( "/externalassets/dialogs/level53/fr/21itsquiet.ogg","stream" )
					thismagnetic = love.audio.newSource( "/externalassets/dialogs/level53/fr/23thismagnetic.ogg","stream" )
			elseif language=="es" then
					if accent=="es" then
				elseif accent=="la" then
					lookufo = love.audio.newSource( "/externalassets/dialogs/level53/es-la/1look.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level53/es-la/3ithink.ogg","stream" )
					ithinkthatufo = love.audio.newSource( "/externalassets/dialogs/level53/es-la/5ithinkthatufo.ogg","stream" )
					howcould = love.audio.newSource( "/externalassets/dialogs/level53/es-la/8howcould.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/es-la/10thisis.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/es-la/13maybe.ogg","stream" )
					stoppointing = love.audio.newSource( "/externalassets/dialogs/level53/es-la/15stoppointing.ogg","stream" )
					dontforget = love.audio.newSource( "/externalassets/dialogs/level53/es-la/18dontforget.ogg","stream" )
					isquiet = love.audio.newSource( "/externalassets/dialogs/level53/es-la/21itsquiet.ogg","stream" )
					thismagnetic = love.audio.newSource( "/externalassets/dialogs/level53/es-la/23thismagnetic.ogg","stream" )
				end
			end
			
			if language2=="en" then
		
				if accent2=="br" then
					
					dontworry = love.audio.newSource( "/externalassets/dialogs/level53/en/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/en/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/en/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/en/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/en/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/en/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/en/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/en/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/en/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/en/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/en/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/en/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/en/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/en/24itsdoing.ogg","stream" )
			
				elseif accent2=="us" then
					dontworry = love.audio.newSource( "/externalassets/dialogs/level53/en-us/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/en-us/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/en-us/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/en-us/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/en-us/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/en-us/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/en-us/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/en-us/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/en-us/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/en-us/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/en-us/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/en-us/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/en-us/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/en-us/24itsdoing.ogg","stream" )
				end
			elseif language2=="nl" then
					dontworry = love.audio.newSource( "/externalassets/dialogs/level53/nl/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/nl/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/nl/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/nl/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/nl/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/nl/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/nl/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/nl/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/nl/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/nl/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/nl/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/nl/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/nl/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/nl/24itsdoing.ogg","stream" )
					
			elseif language2=="pl" then
					dontworry = love.audio.newSource( "/externalassets/dialogs/level53/pl/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/pl/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/pl/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/pl/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/pl/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/pl/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/pl/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/pl/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/pl/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/pl/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/pl/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/pl/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/pl/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/pl/24itsdoing.ogg","stream" )
			elseif language2=="cs" then
					dontworry = love.audio.newSource( "/externalassets/dialogs/level53/cs/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/cs/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/cs/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/cs/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/cs/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/cs/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/cs/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/cs/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/cs/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/cs/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/cs/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/cs/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/cs/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/cs/24itsdoing.ogg","stream" )
			elseif language2=="fr" then
					--[[dontworry = love.audio.newSource( "/externalassets/dialogs/level53/fr/2dontworry.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level53/fr/4thisis.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level53/fr/6stop.ogg","stream" )
					thislooks = love.audio.newSource( "/externalassets/dialogs/level53/fr/7thislooks.ogg","stream" )					
					letitbe = love.audio.newSource( "/externalassets/dialogs/level53/fr/9letitbe.ogg","stream" )
					what = love.audio.newSource( "/externalassets/dialogs/level53/fr/11what.ogg","stream" )
					itlooks = love.audio.newSource( "/externalassets/dialogs/level53/fr/12itlooks.ogg","stream" )
					itdoesnt = love.audio.newSource( "/externalassets/dialogs/level53/fr/14itdoesnt.ogg","stream" )
					asifyoudont = love.audio.newSource( "/externalassets/dialogs/level53/fr/16asifyoudont.ogg","stream" )
					thereare = love.audio.newSource( "/externalassets/dialogs/level53/fr/17thereare.ogg","stream" )
					maybe = love.audio.newSource( "/externalassets/dialogs/level53/fr/19maybe.ogg","stream" )
					noicant = love.audio.newSource( "/externalassets/dialogs/level53/fr/20noicant.ogg","stream" )
					thesemagnets = love.audio.newSource( "/externalassets/dialogs/level53/fr/22thesemagnets.ogg","stream" )
					itsdoing = love.audio.newSource( "/externalassets/dialogs/level53/fr/24itsdoing.ogg","stream" )		
					--]]
			end
			
					-- fish 1 effects
					lookufo:setEffect('myEffect')
					ithink:setEffect('myEffect')
					ithinkthatufo:setEffect('myEffect')
					howcould:setEffect('myEffect')
					thisis:setEffect('myEffect')
					maybe:setEffect('myEffect')
					stoppointing:setEffect('myEffect')
					dontforget:setEffect('myEffect')
					isquiet:setEffect('myEffect')
					thismagnetic:setEffect('myEffect')
					
					--fish 2 effects
					dontworry:setEffect('myEffect')
					thisis:setEffect('myEffect')
					stop:setEffect('myEffect')
					thislooks:setEffect('myEffect')					
					letitbe:setEffect('myEffect')
					what:setEffect('myEffect')
					itlooks:setEffect('myEffect')					
					itdoesnt:setEffect('myEffect')
					asifyoudont:setEffect('myEffect')
					thereare:setEffect('myEffect')
					maybe:setEffect('myEffect')					
					noicant:setEffect('myEffect')
					thesemagnets:setEffect('myEffect')
					itsdoing:setEffect('myEffect')
				
		elseif nLevel==54 then

				if language=="en" then
					if accent=="br" then	
						thislooks = love.audio.newSource( "/externalassets/dialogs/level54/en/2thislooks.ogg","stream" )
						dontbesohasty = love.audio.newSource( "/externalassets/dialogs/level54/en/4dontbesohasty.ogg","stream" )
						maybeits = love.audio.newSource( "/externalassets/dialogs/level54/en/8maybeits.ogg","stream" )
						imrather = love.audio.newSource( "/externalassets/dialogs/level54/en/9imrather.ogg","stream" )
						weshould = love.audio.newSource( "/externalassets/dialogs/level54/en/10weshould.ogg","stream" )
						
						--engine on
						engineononce=false
						whathaveyouactivated = love.audio.newSource( "/externalassets/dialogs/level54/en/engineon/3whathaveyouactivated.ogg","stream" )
						
						whathaveyouactivated:setEffect('myEffect')
						icant = love.audio.newSource( "/externalassets/dialogs/level54/en/engineon/5icant.ogg","stream" )
						idontknowhow = love.audio.newSource( "/externalassets/dialogs/level54/en/engineon/6idontknow.ogg","stream" )
					
						--engine off
						ohfinally = love.audio.newSource( "/externalassets/dialogs/level54/en/engineoff/ohfinally.ogg","stream" )
						whatarelief  = love.audio.newSource( "/externalassets/dialogs/level54/en/engineoff/whatarelief.ogg","stream" )
						imonlyafraid  = love.audio.newSource( "/externalassets/dialogs/level54/en/engineoff/imonlyafraid.ogg","stream" )
							
						--aproach engine
						aproachengineonce=false
							
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level54/en/aproach/1whatareyoudoing.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level54/en/aproach/2careful.ogg","stream" )
		
					elseif accent=="us" then
						thislooks = love.audio.newSource( "/externalassets/dialogs/level54/en-us/2thislooks.ogg","stream" )
						dontbesohasty = love.audio.newSource( "/externalassets/dialogs/level54/en-us/4dontbesohasty.ogg","stream" )
						maybeits = love.audio.newSource( "/externalassets/dialogs/level54/en-us/8maybeits.ogg","stream" )
						imrather = love.audio.newSource( "/externalassets/dialogs/level54/en-us/9imrather.ogg","stream" )
						weshould = love.audio.newSource( "/externalassets/dialogs/level54/en-us/10weshould.ogg","stream" )
						
						--engine on
						engineononce=false
						whathaveyouactivated = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/3whathaveyouactivated.ogg","stream" )
						
						whathaveyouactivated:setEffect('myEffect')
						icant = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/5icant.ogg","stream" )
						idontknowhow = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/6idontknow.ogg","stream" )
					
						--engine off
						
						
						ohfinally = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/finally2.ogg","stream" )
						whatarelief  = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/whatarelief.ogg","stream" )
						imonlyafraid  = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/imonlyafraid.ogg","stream" )
	
						
						--aproach engine
						aproachengineonce=false
							
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level54/en-us/aproach/1whatareyoudoing.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level54/en-us/aproach/2careful.ogg","stream" )
					
					end

				elseif language=="fr" then
								
						--aproach engine
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level54/fr/aproach/1whatareyoudoing.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level54/fr/aproach/2careful.ogg","stream" )
						--engine on
						whathaveyouactivated = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/3whathaveyouactivated.ogg","stream" )

						
						thislooks = love.audio.newSource( "/externalassets/dialogs/level54/fr/2thislooks.ogg","stream" )
						dontbesohasty = love.audio.newSource( "/externalassets/dialogs/level54/fr/4dontbesohasty.ogg","stream" )
						maybeits = love.audio.newSource( "/externalassets/dialogs/level54/fr/8maybeits.ogg","stream" )
						imrather = love.audio.newSource( "/externalassets/dialogs/level54/fr/9imrather.ogg","stream" )
						weshould = love.audio.newSource( "/externalassets/dialogs/level54/fr/10weshould.ogg","stream" )
										
						--engine on
						engineononce=false
					
						icant = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/5icant.ogg","stream" )
						idontknowhow = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/6idontknow.ogg","stream" )
					
						--engine off
						
						
						ohfinally = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineoff/ohfinally.ogg","stream" )
						whatarelief  = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineoff/whatarelief.ogg","stream" )
						imonlyafraid  = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineoff/imonlyafraid.ogg","stream" )
					
						ohfinally:setEffect('myEffect')
						whatarelief:setEffect('myEffect')
						imonlyafraid:setEffect('myEffect')
						
						--aproach engine
						aproachengineonce=false
						
				elseif language=="es" then
						if accent=="es" then
					elseif accent=="la" then
						thislooks = love.audio.newSource( "/externalassets/dialogs/level54/es-la/2thislooks.ogg","stream" )
						dontbesohasty = love.audio.newSource( "/externalassets/dialogs/level54/es-la/4dontbesohasty.ogg","stream" )
						maybeits = love.audio.newSource( "/externalassets/dialogs/level54/es-la/8maybeits.ogg","stream" )
						imrather = love.audio.newSource( "/externalassets/dialogs/level54/es-la/9imrather.ogg","stream" )
						weshould = love.audio.newSource( "/externalassets/dialogs/level54/es-la/10weshould.ogg","stream" )
						
						--engine on
						engineononce=false
						whathaveyouactivated = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineon/3whathaveyouactivated.ogg","stream" )
						
						whathaveyouactivated:setEffect('myEffect')
						icant = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineon/5icant.ogg","stream" )
						idontknowhow = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineon/6idontknow.ogg","stream" )
					
						--engine off
						ohfinally = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineoff/ohfinally.ogg","stream" )
						whatarelief  = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineoff/whatarelief.ogg","stream" )
						imonlyafraid  = love.audio.newSource( "/externalassets/dialogs/level54/es-la/engineoff/imonlyafraid.ogg","stream" )
							
						--aproach engine
						aproachengineonce=false
							
						whatareyoudoing = love.audio.newSource( "/externalassets/dialogs/level54/es-la/aproach/1whatareyoudoing.ogg","stream" )
						careful = love.audio.newSource( "/externalassets/dialogs/level54/es-la/aproach/2careful.ogg","stream" )
					end
				end
			if language2=="en" then
						if accent2=="br" then
						--us accent temporarily till dubs are completed
						
							--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/en-us/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/en-us/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/en-us/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/en-us/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/en-us/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/en-us/aproach/3icantharm.ogg","stream" )
						
					elseif accent2=="us" then
						--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/en-us/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/en-us/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/en-us/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/en-us/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/en-us/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/en-us/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/en-us/aproach/3icantharm.ogg","stream" )
					end
			elseif language2=="nl" then
						--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/nl/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/nl/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/nl/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/nl/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/nl/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/nl/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/nl/aproach/3icantharm.ogg","stream" )
		
			elseif language2=="pl" then
						--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/pl/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/pl/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/pl/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/pl/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/pl/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/pl/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/pl/aproach/3icantharm.ogg","stream" )			
			
			elseif language2=="cs" then
						--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/cs/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/cs/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/cs/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/cs/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/cs/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/cs/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/cs/aproach/3icantharm.ogg","stream" )	
			
				elseif language2=="fr" then
					--[[	--fish 2
						ithinkwecangain = love.audio.newSource( "/externalassets/dialogs/level54/fr/1ithinkwecangain.ogg","stream" )
						sowefoundthedrive = love.audio.newSource( "/externalassets/dialogs/level54/fr/3sowefoundthedrive.ogg","stream" )
						imsorry = love.audio.newSource( "/externalassets/dialogs/level54/fr/5imsorry.ogg","stream" )
						iwould = love.audio.newSource( "/externalassets/dialogs/level54/fr/6iwould.ogg","stream" )
						iwonder = love.audio.newSource( "/externalassets/dialogs/level54/fr/7iwonder.ogg","stream" )
			
						--engine on
						--fish 2
						whathaveyoudone = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/1whathaveyoudone.ogg","stream" )
						thisisterrible = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/3thisisterrible.ogg","stream" )
						mayday = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/5mayday.ogg","stream" )
						howcani = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/6howcani.ogg","stream" )
						--icant2  = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineon/icant.ogg","stream" )
				
						--engine off
						--fish 2
						finally = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineoff/8finally.ogg","stream" )
						thanks = love.audio.newSource( "/externalassets/dialogs/level54/fr/engineoff/3thanks.ogg","stream" )
						--engine off effects	
						--fish 2		
						finally:setEffect('myEffect')
						thanks:setEffect('myEffect')
						
						--aproach engine
						--fish 2
						icantharm = love.audio.newSource( "/externalassets/dialogs/level54/fr/aproach/3icantharm.ogg","stream" )
			--]]
			end
						--fish 1 effects
						thislooks:setEffect('myEffect')
						dontbesohasty:setEffect('myEffect')
						maybeits:setEffect('myEffect')
						imrather:setEffect('myEffect')
						weshould:setEffect('myEffect')
	
						
						whatareyoudoing:setEffect('myEffect')
						careful:setEffect('myEffect')
						whathaveyouactivated:setEffect('myEffect')

						
						--fish 1		
						ohfinally:setEffect('myEffect')
						whatarelief:setEffect('myEffect')
						imonlyafraid:setEffect('myEffect')
						
			
		elseif nLevel==55 then
			if language=="en" then
				 if accent=="br" then
				 elseif accent2=="us" then
				 end
			end
			
			if language2=="en" then
					if accent2=="br" then
						--us accents temporarily till dubs are completed
						itso = love.audio.newSource( "/externalassets/dialogs/level55/en-us/1itso.ogg","stream" )
						boy = love.audio.newSource( "/externalassets/dialogs/level55/en-us/2boy.ogg","stream" )
				elseif accent2=="us" then
					itso = love.audio.newSource( "/externalassets/dialogs/level55/en-us/1itso.ogg","stream" )
					boy = love.audio.newSource( "/externalassets/dialogs/level55/en-us/2boy.ogg","stream" )
				end
			elseif language2=="nl" then
					itso = love.audio.newSource( "/externalassets/dialogs/level55/nl/1itso.ogg","stream" )
					boy = love.audio.newSource( "/externalassets/dialogs/level55/nl/2boy.ogg","stream" )
			
			elseif language2=="pl" then
					itso = love.audio.newSource( "/externalassets/dialogs/level55/pl/1itso.ogg","stream" )
					boy = love.audio.newSource( "/externalassets/dialogs/level55/pl/2boy.ogg","stream" )
			elseif language2=="cs" then
					itso = love.audio.newSource( "/externalassets/dialogs/level55/cs/1itso.ogg","stream" )
					boy = love.audio.newSource( "/externalassets/dialogs/level55/cs/2boy.ogg","stream" )
			elseif language2=="fr" then
					--itso = love.audio.newSource( "/externalassets/dialogs/level55/fr/1itso.ogg","stream" )
					--boy = love.audio.newSource( "/externalassets/dialogs/level55/fr/2boy.ogg","stream" )
			end
			
			
			itso:setEffect('myEffect')
			boy:setEffect('myEffect')
		
		
		elseif nLevel==56 then
			if language=="en" then
			
						--robodog
						
						dontlisten = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/12dontlisten.ogg","stream" )
						dontlisten2 = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/13dontlisten2.ogg","stream" )
						dontlisten3 = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/14dontlisten3.ogg","stream" )
						iambut = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/15iambut.ogg","stream" )
						though = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/16though.ogg","stream" )
						though2 = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/17though.ogg","stream" )
						though3 = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/18though.ogg","stream" )
						though4 = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/19though.ogg","stream" )
						andivisited = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/20andivisited.ogg","stream" )
						andeverybody = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/21andeverybody.ogg","stream" )
						andwhocares = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/22andwhocares.ogg","stream" )
						andtheytoldme = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/23andtheytoldme.ogg","stream" )
				if accent=="br" then
						--fish 1
						wait = love.audio.newSource( "/externalassets/dialogs/level56/en/4wait.ogg","stream" )
						icant = love.audio.newSource( "/externalassets/dialogs/level56/en/5icant.ogg","stream" )
						turnonyourself = love.audio.newSource( "/externalassets/dialogs/level56/en/6turnonyourself.ogg","stream" )
						whereareyou1 = love.audio.newSource( "/externalassets/dialogs/level56/en/7whereareyou.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level56/en/8hello.ogg","stream" )
						saysomething = love.audio.newSource( "/externalassets/dialogs/level56/en/9saysomething.ogg","stream" )
						dontbeafraid = love.audio.newSource( "/externalassets/dialogs/level56/en/16dontbeafraid.ogg","stream" )
						stopwhining = love.audio.newSource( "/externalassets/dialogs/level56/en/17stopwhining.ogg","stream" )
						theresnothing = love.audio.newSource( "/externalassets/dialogs/level56/en/18theresnothing.ogg","stream" )
												
						lookthat = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/1look.ogg","stream" )
						becareful = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/3becareful.ogg","stream" )
						wellnow = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/5wellnow.ogg","stream" )
						nowicansee = love.audio.newSource( "/externalassets/dialogs/level56/en/robodog/10nowicansee.ogg","stream" )			
						
				elseif accent=="us" then
						wait = love.audio.newSource( "/externalassets/dialogs/level56/en-us/4wait.ogg","stream" )
						icant = love.audio.newSource( "/externalassets/dialogs/level56/en-us/5icant.ogg","stream" )
						turnonyourself = love.audio.newSource( "/externalassets/dialogs/level56/en-us/6turnonyourself.ogg","stream" )
						whereareyou1 = love.audio.newSource( "/externalassets/dialogs/level56/en-us/7whereareyou.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level56/en-us/8hello.ogg","stream" )
						saysomething = love.audio.newSource( "/externalassets/dialogs/level56/en-us/9saysomething.ogg","stream" )
						dontbeafraid = love.audio.newSource( "/externalassets/dialogs/level56/en-us/16dontbeafraid.ogg","stream" )
						stopwhining = love.audio.newSource( "/externalassets/dialogs/level56/en-us/17stopwhining.ogg","stream" )
						theresnothing = love.audio.newSource( "/externalassets/dialogs/level56/en-us/18theresnothing.ogg","stream" )
						
						lookthat = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/1look.ogg","stream" )
						becareful = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/3becareful.ogg","stream" )
						wellnow = love.audio.newSource( "/externalassets/dialogs/level56/en-us/26wellnowitallmakessense.ogg","stream" )
						nowicansee = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/10nowicansee.ogg","stream" )			
				end
		
			elseif language=="fr" then
				
						wait = love.audio.newSource( "/externalassets/dialogs/level56/fr/4wait.ogg","stream" )
						icant = love.audio.newSource( "/externalassets/dialogs/level56/fr/5icant.ogg","stream" )
						turnonyourself = love.audio.newSource( "/externalassets/dialogs/level56/fr/6turnonyourself.ogg","stream" )
						whereareyou1 = love.audio.newSource( "/externalassets/dialogs/level56/fr/7whereareyou.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level56/fr/8hello.ogg","stream" )
						saysomething = love.audio.newSource( "/externalassets/dialogs/level56/fr/9saysomething.ogg","stream" )
						dontbeafraid = love.audio.newSource( "/externalassets/dialogs/level56/fr/16dontbeafraid.ogg","stream" )
						stopwhining = love.audio.newSource( "/externalassets/dialogs/level56/fr/17stopwhining.ogg","stream" )
						theresnothing = love.audio.newSource( "/externalassets/dialogs/level56/fr/18theresnothing.ogg","stream" )
						lookthat = love.audio.newSource( "/externalassets/dialogs/level56/fr/23look.ogg","stream" )
						becareful = love.audio.newSource( "/externalassets/dialogs/level56/fr/24becareful.ogg","stream" )
						wellnowitallmakessense = love.audio.newSource( "/externalassets/dialogs/level56/fr/26wellnowitallmakessense.ogg","stream" )
						nowicansee = love.audio.newSource( "/externalassets/dialogs/level56/fr/31nowicansee.ogg","stream" )
			elseif language=="es" then
					if acccent=="es" then
				elseif accent=="la" then
						wait = love.audio.newSource( "/externalassets/dialogs/level56/es-la/4wait.ogg","stream" )
						icant = love.audio.newSource( "/externalassets/dialogs/level56/es-la/5icant.ogg","stream" )
						turnonyourself = love.audio.newSource( "/externalassets/dialogs/level56/es-la/6turnonyourself.ogg","stream" )
						whereareyou1 = love.audio.newSource( "/externalassets/dialogs/level56/es-la/7whereareyou.ogg","stream" )
						hello = love.audio.newSource( "/externalassets/dialogs/level56/es-la/8hello.ogg","stream" )
						saysomething = love.audio.newSource( "/externalassets/dialogs/level56/es-la/9saysomething.ogg","stream" )
						dontbeafraid = love.audio.newSource( "/externalassets/dialogs/level56/es-la/16dontbeafraid.ogg","stream" )
						stopwhining = love.audio.newSource( "/externalassets/dialogs/level56/es-la/17stopwhining.ogg","stream" )
						theresnothing = love.audio.newSource( "/externalassets/dialogs/level56/es-la/18theresnothing.ogg","stream" )
						lookthat = love.audio.newSource( "/externalassets/dialogs/level56/es-la/23look.ogg","stream" )
						becareful = love.audio.newSource( "/externalassets/dialogs/level56/es-la/24becareful.ogg","stream" )
						wellnowitallmakessense = love.audio.newSource( "/externalassets/dialogs/level56/es-la/26wellnowitallmakessense.ogg","stream" )
						nowicansee = love.audio.newSource( "/externalassets/dialogs/level56/es-la/31nowicansee.ogg","stream" )
				end
			end
			
			if language2=="en" then
					if accent2=="br" then
						--us temporarily till dubs are completed
						turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/en-us/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/en-us/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/en-us/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/en-us/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/en-us/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/en-us/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/en-us/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/en-us/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/en-us/15pleasedontleave.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/en-us/19ihopeit.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/en-us/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/en-us/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/en-us/22finallythelights.ogg","stream" )
						what = love.audio.newSource( "/externalassets/dialogs/level56/en-us/25what.ogg","stream" )
						including = love.audio.newSource( "/externalassets/dialogs/level56/en-us/27including.ogg","stream" )
						includingjohnlehnon = love.audio.newSource( "/externalassets/dialogs/level56/en-us/28includingjohnlehnon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/en-us/29includingbull.ogg","stream" )
						includingchernobil = love.audio.newSource( "/externalassets/dialogs/level56/en-us/30includingchernobil.ogg","stream" )
						andalsothemeaning = love.audio.newSource( "/externalassets/dialogs/level56/en-us/32andalsothemeaning.ogg","stream" )
				
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/2whatthatrobodog.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/4ihope.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/6includingthecuban.ogg","stream" )
						includingjohnlennon = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/7includingjohnlennon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/8includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/11andalsothemeaning.ogg","stream" )					
				elseif accent2=="us" then

						turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/en-us/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/en-us/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/en-us/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/en-us/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/en-us/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/en-us/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/en-us/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/en-us/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/en-us/15pleasedontleave.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/en-us/19ihopeit.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/en-us/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/en-us/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/en-us/22finallythelights.ogg","stream" )
						what = love.audio.newSource( "/externalassets/dialogs/level56/en-us/25what.ogg","stream" )
						including = love.audio.newSource( "/externalassets/dialogs/level56/en-us/27including.ogg","stream" )
						includingjohnlehnon = love.audio.newSource( "/externalassets/dialogs/level56/en-us/28includingjohnlehnon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/en-us/29includingbull.ogg","stream" )
						includingchernobil = love.audio.newSource( "/externalassets/dialogs/level56/en-us/30includingchernobil.ogg","stream" )
						andalsothemeaning = love.audio.newSource( "/externalassets/dialogs/level56/en-us/32andalsothemeaning.ogg","stream" )
				
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/2whatthatrobodog.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/4ihope.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/6includingthecuban.ogg","stream" )
						includingjohnlennon = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/7includingjohnlennon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/8includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/en-us/robodog/11andalsothemeaning.ogg","stream" )						
				end
			elseif language2=="nl" then
						
						turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/nl/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/nl/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/nl/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/nl/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/nl/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/nl/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/nl/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/nl/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/nl/15pleasedontleave.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/nl/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/nl/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/nl/22finallythelights.ogg","stream" )

																
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/2whatthatrobodog.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/4ihopeit.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/6includingthecuban.ogg","stream" )
						includingjohnlehnon = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/7includingjohnlennon.ogg","stream" )
						--includingbull = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/7includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/nl/robodog/11andalsothemeaning.ogg","stream" )						
			elseif language2=="pl" then
						turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/pl/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/pl/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/pl/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/pl/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/pl/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/pl/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/pl/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/pl/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/pl/15pleasedontleave.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/pl/19ihopeit.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/pl/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/pl/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/pl/22finallythelights.ogg","stream" )
				
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/2whatthatrobodog.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/4ihope.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/6includingthecuban.ogg","stream" )
						includingjohnlennon = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/7includingjohnlennon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/8includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/pl/robodog/11andalsothemeaning.ogg","stream" )
		
			elseif language2=="cs" then
						turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/cs/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/cs/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/cs/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/cs/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/cs/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/cs/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/cs/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/cs/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/cs/15pleasedontleave.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/cs/19ihopeit.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/cs/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/cs/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/cs/22finallythelights.ogg","stream" )
				
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/2whatthatrobodog.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/4ihope.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/6includingthecuban.ogg","stream" )
						includingjohnlehnon = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/7includingjohnlennon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/8includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/cs/robodog/11andalsothemeaning.ogg","stream" )
				elseif language2=="fr" then
					--[[	turnonthelights = love.audio.newSource( "/externalassets/dialogs/level56/fr/1turnonthelights.ogg","stream" )
						whatdoyouthinkyou = love.audio.newSource( "/externalassets/dialogs/level56/fr/2whatdoyouthink.ogg","stream" )
						cutitout = love.audio.newSource( "/externalassets/dialogs/level56/fr/3cutitout.ogg","stream" )
						here = love.audio.newSource( "/externalassets/dialogs/level56/fr/10here.ogg","stream" )
						hereiam = love.audio.newSource( "/externalassets/dialogs/level56/fr/11hereiam.ogg","stream" )
						iamrightover = love.audio.newSource( "/externalassets/dialogs/level56/fr/12iamrightover.ogg","stream" )
						whereareyou = love.audio.newSource( "/externalassets/dialogs/level56/fr/13whereareyou.ogg","stream" )
						imafraidof = love.audio.newSource( "/externalassets/dialogs/level56/fr/14imafraidof.ogg","stream" )
						pleasedontleave = love.audio.newSource( "/externalassets/dialogs/level56/fr/15pleasedontleave.ogg","stream" )
						ihopeit = love.audio.newSource( "/externalassets/dialogs/level56/fr/19ihopeit.ogg","stream" )
						thisflickering = love.audio.newSource( "/externalassets/dialogs/level56/fr/20thisflickering.ogg","stream" )
						ineverthought = love.audio.newSource( "/externalassets/dialogs/level56/fr/21ineverthought.ogg","stream" )
						finallythelights = love.audio.newSource( "/externalassets/dialogs/level56/fr/22finallythelights.ogg","stream" )
				
						whatthatrobodog = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/2whatthatrobodog.ogg","stream" )
						ihope = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/4ihope.ogg","stream" )
						includingthecuban = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/6includingthecuban.ogg","stream" )
						includingjohnlehnon = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/7includingjohnlennon.ogg","stream" )
						includingbull = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/8includingbull.ogg","stream" )
						includingchernobyl = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/9includingchernobyl.ogg","stream" )
						andalsothemeaning  = love.audio.newSource( "/externalassets/dialogs/level56/fr/robodog/11andalsothemeaning.ogg","stream" )
			--]]
			end
			--fish 1 effects
						wait:setEffect('myEffect')
						icant:setEffect('myEffect')
						turnonyourself:setEffect('myEffect')
						whereareyou1:setEffect('myEffect')
						hello:setEffect('myEffect')
						saysomething:setEffect('myEffect')
						dontbeafraid:setEffect('myEffect')
						stopwhining:setEffect('myEffect')
						theresnothing:setEffect('myEffect')
						lookthat:setEffect('myEffect')
						becareful:setEffect('myEffect')
						wellnow:setEffect('myEffect')
						nowicansee:setEffect('myEffect')
			--fish 2 effects
						turnonthelights:setEffect('myEffect')
						whatdoyouthinkyou:setEffect('myEffect')
						cutitout:setEffect('myEffect')
						here:setEffect('myEffect')
						hereiam:setEffect('myEffect')
						iamrightover:setEffect('myEffect')
						whereareyou:setEffect('myEffect')
						imafraidof:setEffect('myEffect')
						pleasedontleave:setEffect('myEffect')
						ihopeit:setEffect('myEffect')
						thisflickering:setEffect('myEffect')
						ineverthought:setEffect('myEffect')
						finallythelights:setEffect('myEffect')
						whatthatrobodog:setEffect('myEffect')
						includingthecuban:setEffect('myEffect')
						includingjohnlehnon:setEffect('myEffect')
						includingchernobyl:setEffect('myEffect')
						andalsothemeaning:setEffect('myEffect')
						
			--robodog effects
						
						dontlisten:setEffect('myEffect')
						dontlisten2:setEffect('myEffect')
						dontlisten3:setEffect('myEffect')
						iambut:setEffect('myEffect')
						though:setEffect('myEffect')
						though2:setEffect('myEffect')
						though3:setEffect('myEffect')
						though4:setEffect('myEffect')
						andivisited:setEffect('myEffect')
						andeverybody:setEffect('myEffect')
						andwhocares:setEffect('myEffect')
						andtheytoldme:setEffect('myEffect')
			
		elseif nLevel==57 then
			if language=="en" then
				
				if accent=="br" then
					theseexperiments = love.audio.newSource( "/externalassets/dialogs/level57/en/4theseexperiments.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level57/en/5ithink.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level57/en/7everything.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level57/en/11stop.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level57/en/13there.ogg","stream" )
					mygoodness = love.audio.newSource( "/externalassets/dialogs/level57/en/14mygoodness.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level57/en/21icant.ogg","stream" )
					icant2 = love.audio.newSource( "/externalassets/dialogs/level57/en/22icant2.ogg","stream" )
					icant3 = love.audio.newSource( "/externalassets/dialogs/level57/en/23icant3.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level57/en/25idrather.ogg","stream" )
					youknow = love.audio.newSource( "/externalassets/dialogs/level57/en/26youknow.ogg","stream" )
					theodor = love.audio.newSource( "/externalassets/dialogs/level57/en/27theodor.ogg","stream" )
					
					
				elseif accent=="us" then
					theseexperiments = love.audio.newSource( "/externalassets/dialogs/level57/en-us/4theseexperiments.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level57/en-us/5ithink.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level57/en-us/7everything.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level57/en-us/11stop.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level57/en-us/13there.ogg","stream" )
					mygoodness = love.audio.newSource( "/externalassets/dialogs/level57/en-us/14mygoodness.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level57/en-us/21icant.ogg","stream" )
					icant2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/22icant2.ogg","stream" )
					icant3 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/23icant3.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level57/en-us/25idrather.ogg","stream" )
					youknow = love.audio.newSource( "/externalassets/dialogs/level57/en-us/26youknow.ogg","stream" )
					theodor = love.audio.newSource( "/externalassets/dialogs/level57/en-us/27theodor.ogg","stream" )
					
				end
			elseif language=="fr" then
					theseexperiments = love.audio.newSource( "/externalassets/dialogs/level57/fr/4theseexperiments.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level57/fr/5ithink.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level57/fr/7everything.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level57/fr/11stop.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level57/fr/13there.ogg","stream" )
					mygoodness = love.audio.newSource( "/externalassets/dialogs/level57/fr/14mygoodness.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level57/fr/21icant.ogg","stream" )
					icant2 = love.audio.newSource( "/externalassets/dialogs/level57/fr/22icant2.ogg","stream" )
					icant3 = love.audio.newSource( "/externalassets/dialogs/level57/fr/23icant3.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level57/fr/25idrather.ogg","stream" )
					youknow = love.audio.newSource( "/externalassets/dialogs/level57/fr/26youknow.ogg","stream" )
					theodor = love.audio.newSource( "/externalassets/dialogs/level57/fr/27theodor.ogg","stream" )
			elseif language=="es" then
					if acccent=="es" then
				elseif accent=="la" then
					theseexperiments = love.audio.newSource( "/externalassets/dialogs/level57/es-la/4theseexperiments.ogg","stream" )
					ithink = love.audio.newSource( "/externalassets/dialogs/level57/es-la/5ithink.ogg","stream" )
					everything = love.audio.newSource( "/externalassets/dialogs/level57/es-la/7everything.ogg","stream" )
					stop = love.audio.newSource( "/externalassets/dialogs/level57/es-la/11stop.ogg","stream" )
					there = love.audio.newSource( "/externalassets/dialogs/level57/es-la/13there.ogg","stream" )
					mygoodness = love.audio.newSource( "/externalassets/dialogs/level57/es-la/14mygoodness.ogg","stream" )
					icant = love.audio.newSource( "/externalassets/dialogs/level57/es-la/21icant.ogg","stream" )
					icant2 = love.audio.newSource( "/externalassets/dialogs/level57/es-la/22icant2.ogg","stream" )
					icant3 = love.audio.newSource( "/externalassets/dialogs/level57/es-la/23icant3.ogg","stream" )
					idrather = love.audio.newSource( "/externalassets/dialogs/level57/es-la/25idrather.ogg","stream" )
					youknow = love.audio.newSource( "/externalassets/dialogs/level57/es-la/26youknow.ogg","stream" )
					theodor = love.audio.newSource( "/externalassets/dialogs/level57/es-la/27theodor.ogg","stream" )
				end
			end
			
			if language2=="en" then
					if accent2=="br" then
						--us voices till british dubs are completed
					this = love.audio.newSource( "/externalassets/dialogs/level57/en-us/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/en-us/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/en-us/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/en-us/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/en-us/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/en-us/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/en-us/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/en-us/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/en-us/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/en-us/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/en-us/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/en-us/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/en-us/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/en-us/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/en-us/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/en-us/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/en-us/32this.ogg","stream" )
					
				elseif accent2=="us" then
					this = love.audio.newSource( "/externalassets/dialogs/level57/en-us/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/en-us/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/en-us/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/en-us/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/en-us/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/en-us/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/en-us/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/en-us/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/en-us/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/en-us/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/en-us/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/en-us/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/en-us/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/en-us/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/en-us/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/en-us/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/en-us/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/en-us/32this.ogg","stream" )
				end
			elseif language2=="nl" then
					this = love.audio.newSource( "/externalassets/dialogs/level57/nl/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/nl/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/nl/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/nl/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/nl/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/nl/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/nl/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/nl/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/nl/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/nl/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/nl/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/nl/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/nl/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/nl/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/nl/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/nl/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/nl/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/nl/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/nl/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/nl/32this.ogg","stream" )
			elseif language2=="pl" then
					this = love.audio.newSource( "/externalassets/dialogs/level57/pl/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/pl/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/pl/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/pl/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/pl/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/pl/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/pl/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/pl/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/pl/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/pl/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/pl/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/pl/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/pl/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/pl/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/pl/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/pl/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/pl/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/pl/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/pl/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/pl/32this.ogg","stream" )
			elseif language2=="cs" then
					this = love.audio.newSource( "/externalassets/dialogs/level57/cs/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/cs/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/cs/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/cs/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/cs/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/cs/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/cs/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/cs/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/cs/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/cs/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/cs/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/cs/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/cs/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/cs/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/cs/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/cs/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/cs/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/cs/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/cs/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/cs/32this.ogg","stream" )
			elseif language2=="fr" then
				--[[	this = love.audio.newSource( "/externalassets/dialogs/level57/fr/1this.ogg","stream" )
					itseems = love.audio.newSource( "/externalassets/dialogs/level57/fr/2itseems.ogg","stream" )
					this2 = love.audio.newSource( "/externalassets/dialogs/level57/fr/3this.ogg","stream" )
					mygod = love.audio.newSource( "/externalassets/dialogs/level57/fr/6mygod.ogg","stream" )
					shh = love.audio.newSource( "/externalassets/dialogs/level57/fr/8shh.ogg","stream" )
					im = love.audio.newSource( "/externalassets/dialogs/level57/fr/9im.ogg","stream" )
					why = love.audio.newSource( "/externalassets/dialogs/level57/fr/10why.ogg","stream" )
					ivenever = love.audio.newSource( "/externalassets/dialogs/level57/fr/12ivenever.ogg","stream" )
					ivenever2 = love.audio.newSource( "/externalassets/dialogs/level57/fr/15ivenever.ogg","stream" )
					help = love.audio.newSource( "/externalassets/dialogs/level57/fr/16help.ogg","stream" )
					yuck = love.audio.newSource( "/externalassets/dialogs/level57/fr/17yuck.ogg","stream" )
					thathand = love.audio.newSource( "/externalassets/dialogs/level57/fr/18thathand.ogg","stream" )
					theres = love.audio.newSource( "/externalassets/dialogs/level57/fr/19theres.ogg","stream" )
					all = love.audio.newSource( "/externalassets/dialogs/level57/fr/20all.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level57/fr/24wehave.ogg","stream" )
					ups = love.audio.newSource( "/externalassets/dialogs/level57/fr/28ups.ogg","stream" )
					ithink2 = love.audio.newSource( "/externalassets/dialogs/level57/fr/29ithink.ogg","stream" )
					thisis = love.audio.newSource( "/externalassets/dialogs/level57/fr/30thisis.ogg","stream" )
					howmorbid = love.audio.newSource( "/externalassets/dialogs/level57/fr/31howmorbid.ogg","stream" )
					thiscorpse = love.audio.newSource( "/externalassets/dialogs/level57/fr/32this.ogg","stream" )
				--]]
			end
				--fish 1 effects
					theseexperiments:setEffect('myEffect')
					ithink:setEffect('myEffect')
					everything:setEffect('myEffect')
					stop:setEffect('myEffect')
					there:setEffect('myEffect')
					mygoodness:setEffect('myEffect')
					icant:setEffect('myEffect')
					icant2:setEffect('myEffect')
					icant3:setEffect('myEffect')
					idrather:setEffect('myEffect')
					youknow:setEffect('myEffect')
					theodor:setEffect('myEffect')
				
				--fish 2 effects
					this:setEffect('myEffect')
					itseems:setEffect('myEffect')
					this2:setEffect('myEffect')
					mygod:setEffect('myEffect')
					shh:setEffect('myEffect')
					im:setEffect('myEffect')
					why:setEffect('myEffect')
					ivenever:setEffect('myEffect')
					ivenever2:setEffect('myEffect')
					help:setEffect('myEffect')
					yuck:setEffect('myEffect')
					thathand:setEffect('myEffect')
					theres:setEffect('myEffect')
					all:setEffect('myEffect')
					wehave:setEffect('myEffect')
					ups:setEffect('myEffect')
					ithink:setEffect('myEffect')
					thisis:setEffect('myEffect')
					howmorbid:setEffect('myEffect')
					thiscorpse:setEffect('myEffect')
		
		elseif nLevel==58 then
			if language=="en" then
				--ufo end
				
					  squirrel = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/1squirrel.ogg","stream" )
					  thisisalso = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/2thisisalso.ogg","stream" )
					  yes = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/3yes.ogg","stream" )
					  specially = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/4specially.ogg","stream" )
					  butnoone = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/5butnoone.ogg","stream" )
					  nowweknow = love.audio.newSource( "/externalassets/ends/4ufo/audios/en/6nowweknow.ogg","stream" )
			
				if accent=="br" then
					
					sothatshow = love.audio.newSource( "/externalassets/dialogs/level58/en/1sothatshow.ogg","stream" )
					biologicalpower = love.audio.newSource( "/externalassets/dialogs/level58/en/3biologicalpower.ogg","stream" )
					cantyousee = love.audio.newSource( "/externalassets/dialogs/level58/en/5cantyousee.ogg","stream" )
					itmakes = love.audio.newSource( "/externalassets/dialogs/level58/en/7itmakes.ogg","stream" )
					nobodyis = love.audio.newSource( "/externalassets/dialogs/level58/en/8nobodyis.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level58/en/10wehave.ogg","stream" )
					thecagein = love.audio.newSource( "/externalassets/dialogs/level58/en/12thecagein.ogg","stream" )
					look = love.audio.newSource( "/externalassets/dialogs/level58/en/14look.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level58/en/16well.ogg","stream" )
					ourgoal = love.audio.newSource( "/externalassets/dialogs/level58/en/18ourgoal.ogg","stream" )
					inever = love.audio.newSource( "/externalassets/dialogs/level58/en/20inever.ogg","stream" )
				
				
				elseif accent=="us" then
					sothatshow = love.audio.newSource( "/externalassets/dialogs/level58/en-us/1sothatshow.ogg","stream" )
					biologicalpower = love.audio.newSource( "/externalassets/dialogs/level58/en-us/3biologicalpower.ogg","stream" )
					cantyousee = love.audio.newSource( "/externalassets/dialogs/level58/en-us/5cantyousee.ogg","stream" )
					itmakes = love.audio.newSource( "/externalassets/dialogs/level58/en-us/7itmakes.ogg","stream" )
					nobodyis = love.audio.newSource( "/externalassets/dialogs/level58/en-us/8nobodyis.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level58/en-us/10wehave.ogg","stream" )
					thecagein = love.audio.newSource( "/externalassets/dialogs/level58/en-us/12thecagein.ogg","stream" )
					look = love.audio.newSource( "/externalassets/dialogs/level58/en-us/14look.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level58/en-us/16well.ogg","stream" )
					ourgoal = love.audio.newSource( "/externalassets/dialogs/level58/en-us/18ourgoal.ogg","stream" )
					inever = love.audio.newSource( "/externalassets/dialogs/level58/en-us/20inever.ogg","stream" )
					
					--[[
					--alternate
					--dev
					whatpleasedont = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/2whatpleasedont.ogg","stream" )
					Ipromise = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/5Ipromise.ogg","stream" )
					Illbeback = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/6Illbeback.ogg","stream" )
					sohereitis = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/7sohereitis.ogg","stream" )
					westart = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/8westart.ogg","stream" )
	
					--fish1
					sheisright = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/3sheisright.ogg","stream" )
					itcannot = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/4itcannot.ogg","stream" )
					itsaportal = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/itsaportal.ogg","stream" )
					--]]
					
					
				end
			elseif language=="fr" then
					sothatshow = love.audio.newSource( "/externalassets/dialogs/level58/fr/1sothatshow.ogg","stream" )
					biologicalpower = love.audio.newSource( "/externalassets/dialogs/level58/fr/3biologicalpower.ogg","stream" )
					cantyousee = love.audio.newSource( "/externalassets/dialogs/level58/fr/5cantyousee.ogg","stream" )
					itmakes = love.audio.newSource( "/externalassets/dialogs/level58/fr/7itmakes.ogg","stream" )
					nobodyis = love.audio.newSource( "/externalassets/dialogs/level58/fr/8nobodyis.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level58/fr/10wehave.ogg","stream" )
					thecagein = love.audio.newSource( "/externalassets/dialogs/level58/fr/12thecagein.ogg","stream" )
					look = love.audio.newSource( "/externalassets/dialogs/level58/fr/14look.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level58/fr/16well.ogg","stream" )
					ourgoal = love.audio.newSource( "/externalassets/dialogs/level58/fr/18ourgoal.ogg","stream" )
					inever = love.audio.newSource( "/externalassets/dialogs/level58/fr/20inever.ogg","stream" )
			
			elseif language=="es" then
					if acccent=="es" then
				elseif accent=="la" then
					sothatshow = love.audio.newSource( "/externalassets/dialogs/level58/es-la/1sothatshow.ogg","stream" )
					biologicalpower = love.audio.newSource( "/externalassets/dialogs/level58/es-la/3biologicalpower.ogg","stream" )
					cantyousee = love.audio.newSource( "/externalassets/dialogs/level58/es-la/5cantyousee.ogg","stream" )
					itmakes = love.audio.newSource( "/externalassets/dialogs/level58/es-la/7itmakes.ogg","stream" )
					nobodyis = love.audio.newSource( "/externalassets/dialogs/level58/es-la/8nobodyis.ogg","stream" )
					wehave = love.audio.newSource( "/externalassets/dialogs/level58/es-la/10wehave.ogg","stream" )
					thecagein = love.audio.newSource( "/externalassets/dialogs/level58/es-la/12thecagein.ogg","stream" )
					look = love.audio.newSource( "/externalassets/dialogs/level58/es-la/14look.ogg","stream" )
					well = love.audio.newSource( "/externalassets/dialogs/level58/es-la/16well.ogg","stream" )
					ourgoal = love.audio.newSource( "/externalassets/dialogs/level58/es-la/18ourgoal.ogg","stream" )
					inever = love.audio.newSource( "/externalassets/dialogs/level58/es-la/20inever.ogg","stream" )
				end
			end
				
				if language2=="en" then
				
					if accent2=="br" then
						--us voices till british dubs are completed
						ohno = love.audio.newSource( "/externalassets/dialogs/level58/en-us/2ohno.ogg","stream" )
						andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/en-us/4andwhatwas.ogg","stream" )
						whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/en-us/7whataboutthatmotor.ogg","stream" )
						maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/en-us/9maybeyes.ogg","stream" )
						ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/en-us/11ificanseeit.ogg","stream" )
						okay = love.audio.newSource( "/externalassets/dialogs/level58/en-us/13okay.ogg","stream" )
						ourmission = love.audio.newSource( "/externalassets/dialogs/level58/en-us/15ourmission.ogg","stream" )
						goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/en-us/17goodafternoon.ogg","stream" )
						goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/en-us/18goodafternoon2.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/en-us/19ihaveafeeling.ogg","stream" )
						andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/en-us/20andwhatdidyou.ogg","stream" )
					
					
					elseif accent2=="us" then
						ohno = love.audio.newSource( "/externalassets/dialogs/level58/en-us/2ohno.ogg","stream" )
						andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/en-us/4andwhatwas.ogg","stream" )
						whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/en-us/7whataboutthatmotor.ogg","stream" )
						maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/en-us/9maybeyes.ogg","stream" )
						ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/en-us/11ificanseeit.ogg","stream" )
						okay = love.audio.newSource( "/externalassets/dialogs/level58/en-us/13okay.ogg","stream" )
						ourmission = love.audio.newSource( "/externalassets/dialogs/level58/en-us/15ourmission.ogg","stream" )
						goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/en-us/17goodafternoon.ogg","stream" )
						goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/en-us/18goodafternoon2.ogg","stream" )
						ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/en-us/19ihaveafeeling.ogg","stream" )
						andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/en-us/20andwhatdidyou.ogg","stream" )
					
					--alternate
					--indeed = love.audio.newSource( "/externalassets/dialogs/level58/en-us/alternate/1indeed.ogg","stream" )
					
					end
				elseif language2=="nl" then
					ohno = love.audio.newSource( "/externalassets/dialogs/level58/nl/2ohno.ogg","stream" )
					andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/nl/4andwhatwas.ogg","stream" )
					whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/nl/7whataboutthatmotor.ogg","stream" )
					maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/nl/9maybeyes.ogg","stream" )
					ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/nl/11ificanseeit.ogg","stream" )
					okay = love.audio.newSource( "/externalassets/dialogs/level58/nl/13okay.ogg","stream" )
					ourmission = love.audio.newSource( "/externalassets/dialogs/level58/nl/15ourmission.ogg","stream" )
					goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/nl/17goodafternoon.ogg","stream" )
					goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/nl/18goodafternoon2.ogg","stream" )
					ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/nl/19ihaveafeeling.ogg","stream" )
					andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/nl/20andwhatdidyou.ogg","stream" )
				
				elseif language2=="pl" then
					ohno = love.audio.newSource( "/externalassets/dialogs/level58/pl/2ohno.ogg","stream" )
					andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/pl/4andwhatwas.ogg","stream" )
					whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/pl/7whataboutthatmotor.ogg","stream" )
					maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/pl/9maybeyes.ogg","stream" )
					ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/pl/11ificanseeit.ogg","stream" )
					okay = love.audio.newSource( "/externalassets/dialogs/level58/pl/13okay.ogg","stream" )
					ourmission = love.audio.newSource( "/externalassets/dialogs/level58/pl/15ourmission.ogg","stream" )
					goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/pl/17goodafternoon.ogg","stream" )
					goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/pl/18goodafternoon2.ogg","stream" )
					ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/pl/19ihaveafeeling.ogg","stream" )
					andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/pl/20andwhatdidyou.ogg","stream" )
				elseif language2=="cs" then
					ohno = love.audio.newSource( "/externalassets/dialogs/level58/cs/2ohno.ogg","stream" )
					andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/cs/4andwhatwas.ogg","stream" )
					whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/cs/7whataboutthatmotor.ogg","stream" )
					maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/cs/9maybeyes.ogg","stream" )
					ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/cs/11ificanseeit.ogg","stream" )
					okay = love.audio.newSource( "/externalassets/dialogs/level58/cs/13okay.ogg","stream" )
					ourmission = love.audio.newSource( "/externalassets/dialogs/level58/cs/15ourmission.ogg","stream" )
					goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/cs/17goodafternoon.ogg","stream" )
					goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/cs/18goodafternoon2.ogg","stream" )
					ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/cs/19ihaveafeeling.ogg","stream" )
					andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/cs/20andwhatdidyou.ogg","stream" )
				elseif language2=="fr" then
					--[[ohno = love.audio.newSource( "/externalassets/dialogs/level58/fr/2ohno.ogg","stream" )
					andwhatwas = love.audio.newSource( "/externalassets/dialogs/level58/fr/4andwhatwas.ogg","stream" )
					whataboutthatmotor = love.audio.newSource( "/externalassets/dialogs/level58/fr/7whataboutthatmotor.ogg","stream" )
					maybeyes = love.audio.newSource( "/externalassets/dialogs/level58/fr/9maybeyes.ogg","stream" )
					ifcanseeit = love.audio.newSource( "/externalassets/dialogs/level58/fr/11ificanseeit.ogg","stream" )
					okay = love.audio.newSource( "/externalassets/dialogs/level58/fr/13okay.ogg","stream" )
					ourmission = love.audio.newSource( "/externalassets/dialogs/level58/fr/15ourmission.ogg","stream" )
					goodafternoon = love.audio.newSource( "/externalassets/dialogs/level58/fr/17goodafternoon.ogg","stream" )
					goodafternoon2 = love.audio.newSource( "/externalassets/dialogs/level58/fr/18goodafternoon2.ogg","stream" )
					ihaveafeeling = love.audio.newSource( "/externalassets/dialogs/level58/fr/19ihaveafeeling.ogg","stream" )
					andwhatdidyou = love.audio.newSource( "/externalassets/dialogs/level58/fr/20andwhatdidyou.ogg","stream" )
				--]]
				end
	
			
					--fish 1 effects
					sothatshow:setEffect('myEffect')
					biologicalpower:setEffect('myEffect')
					cantyousee:setEffect('myEffect')
					itmakes:setEffect('myEffect')
					nobodyis:setEffect('myEffect')
					wehave:setEffect('myEffect')
					thecagein:setEffect('myEffect')
					look:setEffect('myEffect')
					well:setEffect('myEffect')
					ourgoal:setEffect('myEffect')
					inever:setEffect('myEffect')
					
					--alternate
					--sheisright:setEffect('myEffect')
					--itcannot:setEffect('myEffect')
					--itsaportal:setEffect('myEffect')
					
					
					-- fish 2 effects
					ohno:setEffect('myEffect')
					andwhatwas:setEffect('myEffect')
					whataboutthatmotor:setEffect('myEffect')
					maybeyes:setEffect('myEffect')
					ifcanseeit:setEffect('myEffect')
					okay:setEffect('myEffect')
					ourmission:setEffect('myEffect')
					goodafternoon:setEffect('myEffect')
					goodafternoon2:setEffect('myEffect')
					ihaveafeeling:setEffect('myEffect')
					andwhatdidyou:setEffect('myEffect')
					
					--alternate
					
					--indeed:setEffect('myEffect')
					
					--dev
					--[[whatpleasedont:setEffect('myEffect')
					Ipromise:setEffect('myEffect')
					Illbeback:setEffect('myEffect')
					sohereitis:setEffect('myEffect')
					westart:setEffect('myEffect')
					--]]
					
					--ufo end effects
					  squirrel:setEffect('myEffect')
					  thisisalso:setEffect('myEffect')
					  yes:setEffect('myEffect')
					  specially:setEffect('myEffect')
					  butnoone:setEffect('myEffect')
					  nowweknow:setEffect('myEffect')
		
		elseif nLevel==59 then
		
				skullbottom_pushed=false
				skullupleft_pushed=false
				skullupleftplus_pushed=false
				skullupright_pushed=false
				skullmidright_pushed=false

		
		
			if language=="en" then
					if accent=="br" then
						itisblocked = love.audio.newSource( "/externalassets/dialogs/level59/en/2itisblocked.ogg","stream" )
						itgot = love.audio.newSource( "/externalassets/dialogs/level59/en/3itgot.ogg","stream" )
						youprobably = love.audio.newSource( "/externalassets/dialogs/level59/en/5youprobably.ogg","stream" )
						didyounoticed = love.audio.newSource( "/externalassets/dialogs/level59/en/7didyounoticed.ogg","stream" )
						well = love.audio.newSource( "/externalassets/dialogs/level59/en/10well.ogg","stream" )
						dontforget = love.audio.newSource( "/externalassets/dialogs/level59/en/11dontforget.ogg","stream" )
						thistotem = love.audio.newSource( "/externalassets/dialogs/level59/en/13thistotem.ogg","stream" )
						thatskull = love.audio.newSource( "/externalassets/dialogs/level59/en/14thatskull.ogg","stream" )
				elseif accent=="us" then
						itisblocked = love.audio.newSource( "/externalassets/dialogs/level59/en-us/2itisblocked.ogg","stream" )
						itgot = love.audio.newSource( "/externalassets/dialogs/level59/en-us/3itgot.ogg","stream" )
						youprobably = love.audio.newSource( "/externalassets/dialogs/level59/en-us/5youprobably.ogg","stream" )
						didyounoticed = love.audio.newSource( "/externalassets/dialogs/level59/en-us/7didyounoticed.ogg","stream" )
						well = love.audio.newSource( "/externalassets/dialogs/level59/en-us/10well.ogg","stream" )
						dontforget = love.audio.newSource( "/externalassets/dialogs/level59/en-us/11dontforget.ogg","stream" )
						thistotem = love.audio.newSource( "/externalassets/dialogs/level59/en-us/13thistotem.ogg","stream" )
						thatskull = love.audio.newSource( "/externalassets/dialogs/level59/en-us/14thatskull.ogg","stream" )
				end
			elseif language=="fr" then
					itisblocked = love.audio.newSource( "/externalassets/dialogs/level59/fr/2itisblocked.ogg","stream" )
						itgot = love.audio.newSource( "/externalassets/dialogs/level59/fr/3itgot.ogg","stream" )
						youprobably = love.audio.newSource( "/externalassets/dialogs/level59/fr/5youprobably.ogg","stream" )
						didyounoticed = love.audio.newSource( "/externalassets/dialogs/level59/fr/7didyounoticed.ogg","stream" )
						well = love.audio.newSource( "/externalassets/dialogs/level59/fr/10well.ogg","stream" )
						dontforget = love.audio.newSource( "/externalassets/dialogs/level59/fr/11dontforget.ogg","stream" )
						thistotem = love.audio.newSource( "/externalassets/dialogs/level59/fr/13thistotem.ogg","stream" )
						thatskull = love.audio.newSource( "/externalassets/dialogs/level59/fr/14thatskull.ogg","stream" )
			elseif language=="es" then
				if accent=="es" then
				elseif accent=="la" then
						itisblocked = love.audio.newSource( "/externalassets/dialogs/level59/es-la/2itisblocked.ogg","stream" )
						itgot = love.audio.newSource( "/externalassets/dialogs/level59/es-la/3itgot.ogg","stream" )
						youprobably = love.audio.newSource( "/externalassets/dialogs/level59/es-la/5youprobably.ogg","stream" )
						didyounoticed = love.audio.newSource( "/externalassets/dialogs/level59/es-la/7didyounoticed.ogg","stream" )
						well = love.audio.newSource( "/externalassets/dialogs/level59/es-la/10well.ogg","stream" )
						dontforget = love.audio.newSource( "/externalassets/dialogs/level59/es-la/11dontforget.ogg","stream" )
						thistotem = love.audio.newSource( "/externalassets/dialogs/level59/es-la/13thistotem.ogg","stream" )
						thatskull = love.audio.newSource( "/externalassets/dialogs/level59/es-la/14thatskull.ogg","stream" )
				end
			end
			if language2=="en" then
					if accent2=="br" then
					canyousee = love.audio.newSource( "/externalassets/dialogs/level59/en-us/1canyousee.ogg","stream" )
					iwonder = love.audio.newSource( "/externalassets/dialogs/level59/en-us/4iwonder.ogg","stream" )
					finally = love.audio.newSource( "/externalassets/dialogs/level59/en-us/6finally.ogg","stream" )
					itlookslike = love.audio.newSource( "/externalassets/dialogs/level59/en-us/8itlookslike.ogg","stream" )
					theauthors = love.audio.newSource( "/externalassets/dialogs/level59/en-us/12theauthors.ogg","stream" )
					isitalive = love.audio.newSource( "/externalassets/dialogs/level59/en-us/15isitalive.ogg","stream" )
				elseif accent2=="us" then
					canyousee = love.audio.newSource( "/externalassets/dialogs/level59/en-us/1canyousee.ogg","stream" )
					iwonder = love.audio.newSource( "/externalassets/dialogs/level59/en-us/4iwonder.ogg","stream" )
					finally = love.audio.newSource( "/externalassets/dialogs/level59/en-us/6finally.ogg","stream" )
					itlookslike = love.audio.newSource( "/externalassets/dialogs/level59/en-us/8itlookslike.ogg","stream" )
					theauthors = love.audio.newSource( "/externalassets/dialogs/level59/en-us/12theauthors.ogg","stream" )
					isitalive = love.audio.newSource( "/externalassets/dialogs/level59/en-us/15isitalive.ogg","stream" )
					
				end
		
			elseif language2=="cs" then
					canyousee = love.audio.newSource( "/externalassets/dialogs/level59/cs/1canyousee.ogg","stream" )
					iwonder = love.audio.newSource( "/externalassets/dialogs/level59/cs/4iwonder.ogg","stream" )
					finally = love.audio.newSource( "/externalassets/dialogs/level59/cs/6finally.ogg","stream" )
					itlookslike = love.audio.newSource( "/externalassets/dialogs/level59/cs/8itlookslike.ogg","stream" )
					theauthors = love.audio.newSource( "/externalassets/dialogs/level59/cs/12theauthors.ogg","stream" )
					isitalive = love.audio.newSource( "/externalassets/dialogs/level59/cs/15isitalive.ogg","stream" )
			elseif language2=="pl" then
					canyousee = love.audio.newSource( "/externalassets/dialogs/level59/pl/1canyousee.ogg","stream" )
					iwonder = love.audio.newSource( "/externalassets/dialogs/level59/pl/4iwonder.ogg","stream" )
					finally = love.audio.newSource( "/externalassets/dialogs/level59/pl/6finally.ogg","stream" )
					itlookslike = love.audio.newSource( "/externalassets/dialogs/level59/pl/8itlookslike.ogg","stream" )
					theauthors = love.audio.newSource( "/externalassets/dialogs/level59/pl/12theauthors.ogg","stream" )
					isitalive = love.audio.newSource( "/externalassets/dialogs/level59/pl/15isitalive.ogg","stream" )
			end
			
					--fish 1
						itisblocked:setEffect('myEffect')
						itgot:setEffect('myEffect')
						youprobably:setEffect('myEffect')
						didyounoticed:setEffect('myEffect')
						well:setEffect('myEffect')
						dontforget:setEffect('myEffect')
						thistotem:setEffect('myEffect')
						thatskull:setEffect('myEffect')
			
					--fish 2
					canyousee:setEffect('myEffect')
					iwonder:setEffect('myEffect')
					finally:setEffect('myEffect')
					itlookslike:setEffect('myEffect')
					theauthors:setEffect('myEffect')
					isitalive:setEffect('myEffect')
					
					
			
		
		end
end
