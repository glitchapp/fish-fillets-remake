
function assignfrgbckclassic()
 scolor4={1,1,1,0.95}
 currentfrg2=false
 if shader1type=="bluenoise" then shader2=true end	-- if bluenoise is enabled shader2 is enabled otherwise objects will not render
	if nLevel==1 then  currentbck=level1bckcl   currentbck2=level1bck2cl currentbck3=level1bck3cl currentbck4=whale 	currentfrg=level1frgcl currentfrg2=false
						scolor1={1,1,1,0.95} scolor2={1,1,1,0.7}  scolor3={1,1,1,1} scolor4={1,1,1,0.85}  bckx=0 bcky=-10 bck2x=0 bck2y=-10 frgx=0 frgy=0
elseif nLevel==2 then  currentbck=level2bckcl currentbck2=level2bck2cl currentfrg=level2frgcl						scolor1={1,1,1,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==3 then  currentbck=level3bck   currentbck2=level3bck2 currentbck3=false 	currentfrg=level3frg 			scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==4 then  currentbck=level4bck1  currentbck2=level4bck2 currentfrg=level4frg1  					 		scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  scolor4={1,1,1,0.90} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==5 then  currentbck=level5bck   currentbck2=level5bck2 currentfrg=level5frg   	currentbck3=level5bck3		scolor1={1,1,1,0.75} scolor2={1,1,1,0.95} scolor3={1,1,1,1}  scolor4={1,1,1,0.90}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==6 then  currentbck=level6bck   currentbck2=level6bck2 currentbck3=level6bck3 currentfrg=level6frg 		scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  scolor4={1,1,1,0.90} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==7 then  currentbck=level7bckcl currentbck2=false currentbck3=false currentfrg=level7frgcl currentfrg2=false  	scolor1={1,1,1,0.92} scolor2={1,1,1,0.95} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==8 then  currentbck=level8bckcl  currentbck2=level8bck2cl currentbck3=level8bck3cl  currentfrg=level8frgcl 		scolor1={1,1,1,0.95} scolor2={1,1,1,1} scolor3={1,1,1,0.85}   scolor4={1,1,1,0.9} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==9 then  currentbck=level9bckcl  currentbck2=false  currentfrg=level9frgcl								scolor1={1,1,1,0.95}  scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==10 then currentbck=level10bckcl currentbck2=false currentbck3=false currentfrg=level10frgcl 				scolor1={1,1,1,0.95} scolor2={1,1,1,0.75} scolor3={1,1,1,0.65} scolor4={1,1,1,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==11 then currentbck2=false currentbck3=false currentbck=level11bckcl currentfrg=level11frgcl 	 currentfrg2=false		scolor1={1,1,1,1} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==12 then currentbck=level12bckcl	currentbck2=false 	currentfrg=level12frgcl 										scolor1={1,1,1,1} 	 scolor2={1,1,1,0.60} scolor3={1,1,1,1}  bckx=100 bcky=0 bck2x=0 bck2y=-50 frgx=0 frgy=0
elseif nLevel==13 then currentbck=level13bckcl currentbck2=false	currentbck3=false		 currentfrg=level13frgcl 				scolor1={1,0.2,0.2,0.8} 	scolor2={1,0.2,0.2,0.60} scolor3={1,1,1,1} scolor4={1,0.2,0.2,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=-50 frgx=0 frgy=0
elseif nLevel==14 then currentbck=level14bckcl currentbck2=false currentbck3=false  currentfrg=level14frgcl currentfrg2=false			scolor1={0.6,0.2,1,0.9}	scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==15 then currentbck=level15bckcl currentbck2=false currentbck3=false currentbck4=false currentfrg=level15frgcl currentfrg2=false  scolor1={1,1,1,1}	scolor2={1,1,1,0.95} scolor3={1,1,1,0.90} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==16 then currentbck=level16bckcl currentbck2=false currentbck3=false currentfrg=level16frgcl 										scolor1={1,1,1,1}	scolor2={0.2,0.1,0.6,0.85} scolor3={1,1,1,1} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==17 then currentbck=level17bckcl currentbck2=false currentbck3=false currentfrg=level17frgcl						scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==18 then currentbck=level18bckcl currentbck2=false currentbck3=false currentfrg=level18frgcl 							scolor1={1,1,1,0.95} scolor2={1,1,1,0.75} scolor3={1,1,1,0.65} scolor4={1,1,1,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==19 then currentbck=level19bckcl currentbck2=false  currentbck3=false currentbck4=false currentfrg=level19frgcl	 scolor1={1,0,1,0.95} scolor2={1,0,1,0.95} scolor3={1,1,1,1} scolor4={1,0,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==20 then currentbck=level20bckcl currentbck2=false currentbck3=false currentfrg=level20frgcl 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==21 then currentbck=level21bckcl currentbck2=false currentbck3=false currentfrg=level21frgcl 				scolor1={1,1,1,0.95} scolor2={1,1,1,0.8}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==22 then currentbck=level22bckcl currentbck2=false currentfrg=level22frgcl 											scolor1={1,1,1,1} 	 scolor2={1,1,1,1}	  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==23 then currentbck=level23bckcl currentbck2=false currentbck3=false currentfrg=level23frgcl 					scolor1={1,1,1,0.85} scolor2={1,1,1,1} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==24 then currentbck=level24bckcl currentbck2=false currentfrg=level24frgcl 															scolor1={1,1,1,0.85} scolor2={1,1,1,1} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==25 then currentbck=level25bckcl currentbck2=false currentfrg=level25frgcl 					
										scolor1={1,1,1,0.85} scolor2={1,1,1,0.85} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==26 then currentbck=level24bckcl currentbck2=false currentbck3=false currentfrg=level26frgcl 							scolor1={1,1,1,0.7} scolor2={1,1,1,0.8}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==27 then currentbck=level24bckcl currentbck2=false currentfrg=level27frgcl 											scolor1={1,0.5,1,0.8} scolor2={1,0.5,1,0.7}	scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==28 then currentbck=level28bckcl currentbck2=false currentbck3=level28bck3  currentfrg=level28frgcl 					scolor1={1,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==29 then currentbck=level29bckcl currentbck2=false currentfrg=level29frgcl currentfrg2=false 						scolor1={1,1,1,0.90} scolor2={1,0,0.5,0.94} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==30 then currentbck=level30bckcl currentbck2=false currentfrg=level30frgcl 										scolor1={1,1,1,0.95} scolor2={1,1,1,0.95} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==31 then currentbck=level31bckcl currentbck2=false currentbck3=false currentfrg=level31frgcl 					scolor1={1,1,1,0.95} scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==32 then currentbck=level32bckcl currentbck2=false currentfrg=level32frgcl 										scolor1={1,1,1,0.93} scolor2={1,1,1,0.95}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==33 then currentbck=level33bckcl currentbck2=false currentfrg=level33frgcl   currentfrg2=false 					scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==34 then currentbck=level34bckcl currentbck2=false currentfrg=level34frgcl   currentfrg2=false 					scolor1={1,0.65,0.8,0.85} scolor2={1,0.65,0.8,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==35 then currentbck=level35bckcl currentbck2=false currentbck3=false currentfrg=level35frgcl 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==36 then currentbck=level36bckcl currentbck2=false currentbck3=false currentfrg=level36frgcl 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==37 then currentbck=level37bckcl currentbck2=false currentbck3=false currentfrg=level37frgcl currentfrg2=false  scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==38 then currentbck=level38bckcl currentbck2=false currentbck3=false currentfrg=level38frgcl currentfrg2=false		scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==39 then currentbck=level39bckcl currentbck2=false currentbck3=false currentfrg=false currentfrg2=false		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==40 then currentbck=level40bckcl currentbck2=false currentbck3=false currentfrg=level40frgcl currentfrg2=false		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==41 then currentbck=level41bckcl currentbck2=false currentbck3=false currentfrg=level41frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==42 then currentbck=level42bckcl currentbck2=false currentbck3=false currentfrg=level42frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==43 then currentbck=level43bckcl currentbck2=false currentbck3=false currentfrg=level43frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==44 then currentbck=level44bckcl currentbck2=false currentbck3=false currentfrg=level44frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==45 then currentbck=level45bckcl currentbck2=false currentbck3=false currentfrg=level45frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==46 then currentbck=level46bckcl currentbck2=false currentbck3=false currentfrg=level46frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==47 then currentbck=level47bckcl currentbck2=false currentbck3=false currentfrg=level47frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==48 then currentbck=level48bckcl currentbck2=false currentbck3=false currentfrg=level48frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==49 then currentbck=level49bckcl currentbck2=false currentbck3=false currentfrg=level49frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==50 then currentbck=level50bckcl currentbck2=false currentbck3=false currentfrg=level50frgcl currentfrg2=false 		scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==51 then currentbck=level51bckcl currentfrg=level51frgcl														 expx=2.04 expy=2.04		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==52 then currentbck=level52bckcl currentbck2=false currentbck3=false currentfrg=level52frgcl  expx=2.25 expy=2.25		scolor1={1,0.65,0.8,0.6} scolor2={1,1,1,0.90}	scolor3={1,1,1,1}   bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==53 then currentbck=level53bckcl currentbck2=false currentbck3=false currentfrg=level53frgcl 					 expx=2.18 expy=2.18		scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==54 then currentbck=level54bckcl currentbck2=false currentbck3=false currentfrg=level54frgcl currentfrg2=false expx=2.51 expy=2.51		scolor1={1,1,1,1} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={0,1,0.5,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==55 then currentbck=level55bckcl currentbck2=false currentbck3=false currentfrg=level55frgcl 					 expx=2.7 expy=2.7		scolor1={0,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==56 then currentbck=level56bckcl currentbck2=false currentbck3=false currentfrg=level56frgcl 					 expx=1.84 expy=1.84		scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=-600 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==57 then currentbck=level57bckcl currentbck2=false currentbck3=false currentfrg=level57frgcl 					 expx=2.1 expy=2.1		scolor1={0,1,0,1} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==58 then currentbck=level58bckcl currentbck2=false currentbck3=false currentfrg=level58frgcl 					 expx=1.78 expy=1.78		scolor1={0.8,1,0,1} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=-300 bcky=-250 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==59 then currentbck=level59bckcl currentbck2=false currentbck3=false currentfrg=level59frgcl					 expx=1.73 expy=1.73		scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==60 then currentbck=level60bckcl currentbck2=false currentbck3=false currentfrg=level60frgcl 					 expx=3.74 expy=3.74		scolor1={1,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==61 then currentbck=level61bckcl currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level61frgcl expx=2.18 expy=2.18		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==62 then currentbck=level61bckcl currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level62frgcl expx=2.25 expy=2.25		scolor1={0,1,1,1} scolor2={0,1,1,0.92} scolor3={0,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==63 then currentbck=level63bckcl currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level63frgcl expx=2.4 expy=2.4		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==64 then currentbck=level64bckcl currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level64frgcl expx=1.87 expy=1.87		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==65 then currentbck=level65bckcl currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level65frgcl expx=2.12 expy=2.12	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==66 then currentbck=level66bckcl currentbck2=false currentbck3=false currentfrg=level66frgcl 	expx=1.87 expy=1.87	scolor1={1,1,1,0} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==67 then currentbck=level67bckcl currentbck2=false currentbck3=false currentfrg=level67frgcl 	expx=1.82 expy=1.82	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==68 then currentbck=level68bckcl currentbck2=false currentbck3=false currentfrg=level68frgcl 	expx=2.07 expy=2.07	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=-10
elseif nLevel==69 then currentbck=level69bckcl currentbck2=false currentbck3=false currentfrg=level69frgcl 	expx=2.5 expy=2.5	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==70 then currentbck=level70bckcl currentbck2=false currentbck3=false currentfrg=level70frgcl 	expx=1.93 expy=1.93	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==71 then currentbck=level71bckcl currentbck2=false currentbck3=false currentfrg=level71frgcl 	expx=2.67 expy=2.67	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==72 then currentbck=level72bckcl currentbck2=false currentbck3=false currentfrg=level72frgcl 	expx=2.97 expy=2.97	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==73 then currentbck=level73bckcl currentbck2=false currentbck3=false currentfrg=level73frgcl 	expx=2.25 expy=2.25	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==74 then currentbck=level74bckcl currentbck2=false currentbck3=false currentfrg=level74frgcl 	expx=1.69 expy=1.69	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==75 then currentbck=level75bckcl currentbck2=false currentbck3=false currentfrg=level75frgcl 	expx=1.88 expy=1.88	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==76 then currentbck=level76bckcl currentbck2=false currentbck3=false currentfrg=level76frgcl 	expx=3.05 expy=3.05	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==77 then currentbck=level77bckcl currentbck2=false currentbck3=false currentfrg=level77frgcl 	expx=2.1 expy=2.1	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==78 then currentbck=level78bckcl currentbck2=false currentbck3=false currentfrg=level78frgcl 	expx=4.63 expy=4.63	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==79 then currentbck=level79bckcl currentbck2=false currentbck3=false currentfrg=level79frgcl 	expx=1.83 expy=1.83	scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

--screensavers

elseif nLevel==101 then currentbck=level3bck currentbck2=level3bck2 currentbck3=false currentfrg=false  									scolor1={1,1,1,0.85} 	  scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0 expx=0.5 expy=0.5
elseif nLevel==102 then currentbck=underwater_fantasy1 currentbck2=level21screensaver1 currentbck3=level21screensaver2 currentfrg=false 	scolor1={1,1,1,0.95} 	  scolor2={1,1,1,0.5}  scolor3={1,1,1,1} scolor4={1,1,1,0.9}  bckx=0 bcky=210 bck2x=0 bck2y=0 frgx=0 frgy=0 expx=0.5 expy=0.5
elseif nLevel==103 then currentbck=level34bck currentbck2=level34bck2 currentfrg=false currentfrg2=level34frg2 	scolor1={1,0.65,0.8,0.85}   scolor2={1,0.65,0.8,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0 expx=0.5 expy=0.5

--extra

elseif nLevel==80 then currentbck=level80bck currentbck2=false currentfrg=level80frg currentfrg2=false 	scolor1={1,1,1,1}   scolor2={1,1,1,0.80} scolor3={1,1,1,0.8} scolor4={1,1,1,0.9} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==81 then currentbck=level81bck currentbck2=level81bck2 currentfrg=level81frg currentfrg2=level81frg2 currentbck3=false 	scolor1={1,1,1,0.85}   scolor2={1,1,1,0.90} scolor3={1,1,1,0.8} scolor4={1,1,1,0.9}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

end
end

function assignsizenormal()						-- this function resize the classic backgrounds to fit the game's grid
																							--Grid size
		if nLevel==1 and res=="720p"	then expx=2.44 expy=2.44	dividx=2.48 dividy=2.48	--29x26
	elseif nLevel==1 and res=="1080p"	then expx=2.67 expy=2.67	dividx=2.65 dividy=2.65	
	elseif nLevel==1 and res=="1440p" 	then expx=3.7 expy=3.7	dividx=3.8 dividy=3.8		
	elseif nLevel==1 and res=="4k" 		then expx=5.52 expy=5.52	dividx=5.55 dividy=5.55
	
	elseif nLevel==2 and res=="720p"  then expx=1.72 	 expy=1.72		dividx=3.3 dividy=3.3	--48x37
	elseif nLevel==2 and res=="1080p" then expx=1.82 expy=1.82  dividx=3.5 dividy=3.5
	elseif nLevel==2 and res=="1440p" then expx=2.6 expy=2.6 dividx=5 dividy=5
	elseif nLevel==2 and res=="4k"	  then expx=3.83 expy=3.83	dividx=7.1 dividy=7.1
	
	elseif nLevel==3 and res=="720p" then expx=1.8 expy=1.8 dividx=3.6 dividy=3.6				--40x35
	elseif nLevel==3 and res=="1080p" then expx=1.93 expy=1.93 dividx=3.9 dividy=3.9
	elseif nLevel==3 and res=="1440p" then expx=1.93*1.42 expy=1.93*1.42 dividx=3.9*1.42 dividy=3.9*1.42
	elseif nLevel==3 and res=="4k" then  expx=4.12 expy=4.12	dividx=8.43 dividy=8.43
	
	elseif nLevel==4 and res=="720p" then expx=1.72 expy=1.72 dividx=1.7 dividy=1.7			--21x37
	elseif nLevel==4 and res=="1080p" then expx=1.82 expy=1.82 dividx=1.8 dividy=1.8			--21x37
	elseif nLevel==4 and res=="1440p" then expx=2.58 expy=2.58 dividx=2.5 dividy=2.5
	
	elseif nLevel==5 and res=="720p" 	then expx=2.12 expy=2.12 dividx=2.17 dividy=2.17			--40x30
	elseif nLevel==5 and res=="1080p" 	then expx=2.25 expy=2.25 dividx=2.3 dividy=2.3			--40x30
	elseif nLevel==5 and res=="1440p"	then expx=1 expy=1 dividx=3.2 dividy=3.2
	--elseif nLevel==5 and res=="4k" 		then expx=3.2 expy=3.2 dividx=3.2 dividy=3.2
	
	
	elseif nLevel==6 and res=="1080p" then expx=2.05 expy=2.05 dividx=4.1 dividy=4.1			--36x33
	elseif nLevel==6 and res=="1440p" then expx=2.91 expy=2.91 dividx=5.87 dividy=5.87
	
	
	elseif nLevel==7 and res=="1080p" then expx=2.46 expy=2.46 dividx=2.48 dividy=2.48			--52x15
	elseif nLevel==7 and res=="1440p" then expx=3.3 expy=3.3 dividx=3.2 dividy=3.2
	
	elseif nLevel==8 and res=="1080p" then expx=3.05 expy=3.05 dividx=6.1 dividy=6.1			--23x22
	elseif nLevel==8 and res=="1440p" then expx=4.4 expy=4.4 dividx=8.8 dividy=8.8
	
	elseif nLevel==9 and res=="1080p" then expx=3.76 expy=3.76 dividx=7.6 dividy=7.6			--34x13
	elseif nLevel==9 and res=="1440p" then expx=5 expy=5 dividx=10 dividy=10
	
	elseif nLevel==10 and res=="720p" then expx=1.64 expy=1.64 dividx=1.66 dividy=1.66			--52x33
	elseif nLevel==10 and res=="1080p" then expx=2.12 expy=2.12 dividx=2.09 dividy=2.09
	elseif nLevel==10 and res=="1440p" then expx=2.9 expy=2.9 dividx=2.84 dividy=2.84
	
	elseif nLevel==11 and res=="1080p" then expx=2.1 expy=2.1 dividx=2.1 dividy=2.1								--45x32
	elseif nLevel==11 and res=="1440p" then expx=3 expy=3 dividx=3 dividy=3								--45x32
	
	elseif nLevel==12 and res=="1080p" then expx=3.35 expy=3.35 dividx=3.4 dividy=3.4			--28x20
	elseif nLevel==12 and res=="1440p" then expx=4.8 expy=4.8 dividx=4.8 dividy=4.8
	
	elseif nLevel==13 and res=="1080p" then expx=2.3 expy=2.3 dividx=4.48 dividy=4.48			--35x29
	elseif nLevel==13 and res=="1440p" then expx=3.3 expy=3.3  dividx=6.72 dividy=6.72
	
	
	elseif nLevel==14 and res=="1080p" then expx=2.48 expy=2.48 dividx=4.9 dividy=4.9							--45x27
	elseif nLevel==14 and res=="1440p" then expx=3.56 expy=3.56 dividx=7 dividy=7
	
	elseif nLevel==15 and res=="1080p" then expx=3 expy=3 dividx=5.8 dividy=5.8					--43x19
	elseif nLevel==15 and res=="1440p" then expx=3.95 expy=3.95 dividx=7.5 dividy=7.5

	elseif nLevel==16 and res=="1080p" then expx=1.8 expy=1.8 dividx=3.5 dividy=3.5								--50x37
	elseif nLevel==16 and res=="1440p" then expx=2.6 expy=2.6 dividx=5 dividy=5
	
	elseif nLevel==17 and res=="1080p" then expx=2.41 expy=2.41 dividx=4.8 dividy=4.8							--52x28
	elseif nLevel==17 and res=="1440p" then expx=3.28 expy=3.28 dividx=6.6 dividy=6.6
	
	elseif nLevel==18 and res=="1080p" then expx=2.05 expy=2.05 dividx=1 dividy=1								--52x33
	elseif nLevel==18 and res=="1440p" then expx=2.9 expy=2.9 dividx=2.88 dividy=2.88
	
	elseif nLevel==19 and res=="1080p" then expx=1.778 expy=1.778 dividx=3.5 dividy=3.5							--46x38
	elseif nLevel==19 and res=="1440p" then expx=2.52 expy=2.52 dividx=5 dividy=5
	
	elseif nLevel==20 and res=="1080p" then expx=1.87 expy=1.87 dividx=3.7 dividy=3.7							--34x36
	elseif nLevel==20 and res=="1440p" then expx=2.66 expy=2.66 dividx=5.27 dividy=5.27
	elseif nLevel==21 and res=="1080p" then expx=2.55 expy=2.55 dividx=5 dividy=5								--50x23
	elseif nLevel==21 and res=="1440p" then expx=3.41 expy=3.41 dividx=6.87 dividy=6.87
	
	elseif nLevel==22  and res=="1080p" then expx=2.25 expy=2.25 dividx=4.7 dividy=4.7							--48x30
	elseif nLevel==22  and res=="1440p" then expx=3.2 expy=3.2 dividx=6.4 dividy=6.4
	
	elseif nLevel==23 and res=="1080p" then expx=2.25 expy=2.25 dividx=4.5 dividy=4.5							--40x30
	elseif nLevel==23 and res=="1440p" then expx=3.2 expy=3.2 dividx=6.4 dividy=6.4
	
	elseif nLevel==24 and res=="1080p" then expx=1.87 expy=1.87 dividx=3.8 dividy=3.8							--47x36
	elseif nLevel==24 and res=="1440p" then expx=2.66 expy=2.66 dividx=5.4 dividy=5.4
	
	elseif nLevel==25 and res=="1080p" then expx=2.41 expy=2.41 dividx=4.8 dividy=4.8							--43x28
	elseif nLevel==25 and res=="1440p" then expx=3.42 expy=3.42 dividx=6.78 dividy=6.78
	elseif nLevel==26 and res=="1080p" then expx=2.26 expy=2.26 dividx=4.6 dividy=4.6							--40x30
	elseif nLevel==26 and res=="1440p" then expx=3.2 expy=3.2 dividx=6.4 dividy=6.4
	elseif nLevel==27 and res=="1080p" then expx=2.11 expy=2.11 dividx=4.6 dividy=4.6							--39x32
	elseif nLevel==27 and res=="1440p" then expx=3 expy=3 dividx=6 dividy=6
	elseif nLevel==28 and res=="1080p" then expx=1.87 expy=1.87 dividx=3.7 dividy=3.7							--34x36
	elseif nLevel==28 and res=="1440p" then expx=2.66 expy=2.66 dividx=5.27 dividy=5.27
	
	
	elseif nLevel==29 and res=="1080p" then expx=1.93 expy=1.93 dividx=3.7 dividy=3.7							--50x35
	elseif nLevel==29 and res=="1440p" then expx=2.74 expy=2.74 dividx=5.4 dividy=5.4
	
	elseif nLevel==30 and res=="1080p" then expx=2.77 expy=2.77 dividx=5.4 dividy=5.4							--28x25
	elseif nLevel==30 and res=="1440p" then expx=3.84 expy=3.84 dividx=8 dividy=8
	
	elseif nLevel==31 and res=="1080p" then expx=1.84 expy=1.84 dividx=3.64 dividy=3.64							--43x38
	elseif nLevel==31 and res=="1440p" then expx=2.521 expy=2.521 dividx=5.05 dividy=5.05
	
	
	elseif nLevel==32 and res=="1080p" then expx=1.8 expy=1.8 dividx=3.55 dividy=3.55							--47x39
	elseif nLevel==32 and res=="1440p" then expx=2.44 expy=2.44 dividx=5 dividy=5	
	
	elseif nLevel==33 and res=="1080p" then expx=4.79 expy=4.79 dividx=9.6 dividy=9.6							--24x14
	elseif nLevel==33 and res=="1440p" then expx=6.8 expy=6.8 dividx=14 dividy=14
	
	elseif nLevel==34 and res=="1080p" then expx=1.92 expy=1.92 dividx=3.8 dividy=3.8							--43x35
	elseif nLevel==34 and res=="1440p" then expx=2.75 expy=2.75 dividx=5.5 dividy=5.5
	
	elseif nLevel==35 and res=="1080p" then expx=1.93 expy=1.93  dividx=3.8 dividy=3.8							--41x35
	elseif nLevel==35 and res=="1440p" then expx=2.75 expy=2.75 dividx=5.5 dividy=5.5
	
	elseif nLevel==36 and res=="1080p" then expx=2.117 expy=2.117 dividx=4 dividy=4								--33x32
	elseif nLevel==36 and res=="1440p" then expx=3 expy=3 dividx=5.8 dividy=5.8
	
	elseif nLevel==37 and res=="1080p" then expx=1.82 expy=1.82 dividx=3.68 dividy=3.68							--50x37
	elseif nLevel==37 and res=="1440p" then expx=2.6 expy=2.6 dividx=5.3 dividy=5.3
	
	elseif nLevel==38  and res=="1080p" then expx=2.5 expy=2.5 dividx=4.8 dividy=4.8			--36x27
	elseif nLevel==38  and res=="1440p" then expx=3.53 expy=3.53 dividx=7.1 dividy=7.1
	
	elseif nLevel==39 and res=="1080p" then expx=0.5 expy=0.5													--15x19
	elseif nLevel==39 and res=="1440p" then expx=7 expy=7 dividx=7.7 dividy=7.7
	
	
	elseif nLevel==40 and res=="1080p" then expx=2.1 expy=2.1 dividx=4 dividy=4									--31x32
	elseif nLevel==40 and res=="1440p" then expx=2.98 expy=2.98 dividx=5.9 dividy=5.9
	
	elseif nLevel==41 and res=="1080p" then expx=3.2 expy=3.2													--27x21
	elseif nLevel==41 and res=="1440p" then expx=4.6 expy=4.6 dividx=9 dividy=9
	
	elseif nLevel==42 and res=="1080p" then expx=1.72 expy=1.72													--53x39
	elseif nLevel==42 and res=="1440p" then expx=2.45 expy=2.45 dividx=4.95 dividy=4.95
	
	elseif nLevel==43 and res=="1080p"  then expx=2.1 expy=2.1 dividx=4.2 dividy=4.2								--43x32
	elseif nLevel==43 and res=="1440p"  then expx=3 expy=3 dividx=6 dividy=6
	
	elseif nLevel==44 and res=="1080p" then expx=1.73 expy=1.73 dividx=3.43 dividy=3.43							--50x39
	elseif nLevel==44 and res=="1440p" then expx=2.45 expy=2.45 dividx=4.9 dividy=4.9
	
	elseif nLevel==45 and res=="1080p" then expx=3.04 expy=3.04 dividx=6 dividy=6								--24x22
	elseif nLevel==45 and res=="1440p" then  expx=4.35 expy=4.35 dividx=8.9 dividy=8.9
	
	elseif nLevel==46 then expx=3.06 expy=3.06 dividx=6.12 dividy=6.12							--29x22
	elseif nLevel==47 then expx=2.5 expy=2.5 dividx=5.1 dividy=5.1								--30x27
	elseif nLevel==48 then expx=1.98 expy=1.98 dividx=4 dividy=4								--51x34
	elseif nLevel==49 then expx=2.8 expy=2.8 dividx=5.6 dividy=5.6								--28x24
	
	elseif nLevel==50 and res=="1080p" then expx=2.33 expy=2.33 dividx=4.75 dividy=4.75							--42x29
	elseif nLevel==50 and res=="1440p" then expx=3.3 expy=3.3 dividx=6.5 dividy=6.5							--42x29
	
	elseif nLevel==51 then expx=2.04 expy=2.04 dividx=4 dividy=4								--48x33
	
	elseif nLevel==52 and res=="1080p" then expx=2.24 expy=2.24 dividx=3.8 dividy=3.8			--44x30
	elseif nLevel==52 and res=="1440p" then expx=3.2 expy=3.2 dividx=6 dividy=6
	
	
	elseif nLevel==53 and res=="1080p" then expx=2.18 expy=2.18 dividx=4.4 dividy=4.4							--35x31
	elseif nLevel==53 and res=="1440p" then expx=3.09 expy=3.09 dividx=6.2 dividy=6.2
	
	elseif nLevel==54 and res=="1080p" then expx=2.5 expy=2.5 dividx=5 dividy=5									--41x27
	elseif nLevel==54 and res=="1440p" then expx=3.55 expy=3.55 dividx=7.2 dividy=7.2
	
	elseif nLevel==55 and res=="1080p" then expx=3.55 expy=3.55 dividx=7.2 dividy=7.2
	elseif nLevel==55 and res=="1440p" then expx=3.87 expy=3.87 dividx=7.4 dividy=7.4
	
	elseif nLevel==56 and res=="1080p" then expx=3.55 expy=3.55 dividx=7.2 dividy=7.2
	elseif nLevel==56 and res=="1440p" then expx=2.6 expy=2.6 dividx=5.2 dividy=5.2
	
	elseif nLevel==57 and res=="1080p" then expx=2.12 expy=2.12 dividx=4.3 dividy=4.3							--45x32
	elseif nLevel==57 and res=="1440p" then expx=3 expy=3 dividx=6 dividy=6
	
	elseif nLevel==58 and res=="1080p" then expx=1.78 expy=1.78 dividx=3.6 dividy=3.6							--41x38
	elseif nLevel==58 and res=="1440p" then expx=2.52 expy=2.52 dividx=5 dividy=5
	
	elseif nLevel==59 and res=="1080p" then expx=1.72 expy=1.72 dividx=3.5 dividy=3.5							--48x39
	elseif nLevel==59 and res=="1440p" then expx=2.45 expy=2.45 dividx=4.9 dividy=4.9
	
	elseif nLevel==60 and res=="1080p" then expx=1.72 expy=1.72 dividx=3.5 dividy=3.5
	elseif nLevel==60 and res=="1440p" then expx=5.35 expy=5.35 dividx=1.35 dividy=1.35
	
	elseif nLevel==61 and res=="1080p" then expx=2.21 expy=2.21 dividx=2.18 dividy=2.18							--39x31
	elseif nLevel==61 and res=="1440p" then expx=3.1 expy=3.1 dividx=3.1 dividy=3.1
	
	elseif nLevel==62 and res=="1080p" then expx=2.25 expy=2.25 dividx=2.2 dividy=2.2							--40x30
	elseif nLevel==62 and res=="1440p" then expx=3.2 expy=3.2 dividx=3.1 dividy=3.1
	
	elseif nLevel==63 and res=="1080p" then expx=2.25 expy=2.25 dividx=2.2 dividy=2.2							--40x30
	elseif nLevel==63 and res=="1440p" then expx=3.45 expy=3.45 dividx=0.8 dividy=0.8
	
	elseif nLevel==64 and res=="1080p" then expx=2.25 expy=2.25 dividx=2.2 dividy=2.2
	elseif nLevel==64 and res=="1440p" then expx=2.67 expy=2.67 dividx=0.7 dividy=0.7
	
	elseif nLevel==65 and res=="1080p" then expx=2.25 expy=2.25 dividx=2.2 dividy=2.2
	elseif nLevel==65 and res=="1440p" then expx=3 expy=3 dividx=0.1 dividy=0.1
	
	elseif nLevel==66 and res=="1080p" then expx=2.25 expy=2.25 dividx=2.2 dividy=2.2
	elseif nLevel==66 and res=="1440p" then expx=2.65 expy=2.65 dividx=1.4 dividy=1.4
	
	elseif nLevel==67 and res=="1080p" then expx=1.82 expy=1.82 dividx=3.6 dividy=3.6							--52x37
	elseif nLevel==67 and res=="1440p" then expx=2.6 expy=2.6 dividx=5.1 dividy=5.1
	
	elseif nLevel==68 and res=="1080p" then expx=1.82 expy=1.82 dividx=3.6 dividy=3.6							--45x33
	elseif nLevel==68 and res=="1440p" then expx=2.95 expy=2.95 dividx=0.7 dividy=0.7
	
	elseif nLevel==69 and res=="1080p" then expx=2.5 expy=2.5 dividx=2.5 dividy=2.5								--40x27
	elseif nLevel==69 and res=="1440p" then expx=3.6 expy=3.6 dividx=3.5 dividy=3.5
	
	elseif nLevel==70 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2								--51x35
	elseif nLevel==70 and res=="1440p" then expx=2.72 expy=2.72 dividx=2.8 dividy=2.8
	
	elseif nLevel==71 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2								--51x35
	elseif nLevel==71 and res=="1440p" then expx=3.55 expy=3.55 dividx=3.5 dividy=3.5
	
	elseif nLevel==72 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2
	elseif nLevel==72 and res=="1440p" then expx=4.2 expy=4.2 dividx=4.2 dividy=4.2
	
	elseif nLevel==73 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2
	elseif nLevel==73 and res=="1440p" then expx=3 expy=3 dividx=2.95 dividy=2.95
	
	elseif nLevel==74 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2
	elseif nLevel==74 and res=="1440p" then expx=2.4 expy=2.4 dividx=2.4 dividy=2.4
	
	elseif nLevel==75 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2
	elseif nLevel==75 and res=="1440p" then expx=2.65 expy=2.65 dividx=2.65 dividy=2.65
	
	elseif nLevel==76 and res=="1080p" then expx=1.93 expy=1.93 dividx=2 dividy=2
	elseif nLevel==76 and res=="1440p" then expx=4.4 expy=4.4 dividx=4.3 dividy=4.3
	
	elseif nLevel==77 and res=="1080p" then expx=2.12 expy=2.12 dividx=2.1 dividy=2.1							--29x32
	elseif nLevel==77 and res=="1440p" then expx=3 expy=3 dividx=3 dividy=3
	
	elseif nLevel==78 and res=="1080p" then expx=2.12 expy=2.12 dividx=2.1 dividy=2.1			--28x14
	elseif nLevel==78 and res=="1440p" then expx=6 expy=6 dividx=6 dividy=6
	
	elseif nLevel==79 and res=="1080p" then expx=2.12 expy=2.12 dividx=2.1 dividy=2.1
	elseif nLevel==79 and res=="1440p" then expx=2.6 expy=2.6 dividx=2.6 dividy=2.6
	
	end
end

function assignsizeupscaled()
		
		if nLevel==1 and res=="720p" then expx=0.49 expy=0.49 dividx=0.49 dividy=0.49			--29x26
	elseif nLevel==1 and res=="1080p" then expx=0.69 expy=0.69 dividx=0.69 dividy=0.69 --dividx=0.67 dividy=0.67
	elseif nLevel==1 and res=="1440p" then expx=0.87 expy=0.87 dividx=0.88 dividy=0.88
	elseif nLevel==1 and res=="4k" then expx=1.38 expy=1.38 dividx=1.4 dividy=1.4
	
	elseif nLevel==2 and res=="720p" then expx=0.425 expy=0.425 dividx=0.83 dividy=0.83			--48x37
	elseif nLevel==2 and res=="1080p" then expx=0.47 expy=0.47 dividx=0.89 dividy=0.89
	elseif nLevel==2 and res=="1440p" then expx=0.6461 expy=0.6461 dividx=1.2638 dividy=1.2638
	elseif nLevel==2 and res=="4k" then expx=0.96 expy=0.96 dividx=1.8 dividy=1.8
	
	elseif nLevel==3 and res=="720p" then expx=0.45 expy=0.45 dividx=0.9 dividy=0.9				--40x35
	elseif nLevel==3 and res=="1080p" then expx=0.5 expy=0.5 dividx=1 dividy=1
	elseif nLevel==3 and res=="1440p" then expx=0.68728 expy=0.68728 dividx=1.3916 dividy=1.3916
	elseif nLevel==3 and res=="4k" then expx=1.03 expy=1.03 dividx=2.08 dividy=2.08
	
	elseif nLevel==4 and res=="720p" then expx=0.43 expy=0.43 dividx=0.45 dividy=0.45
	elseif nLevel==4 and res=="1080p" then expx=0.47 expy=0.47 dividx=0.45 dividy=0.45			--21x37
	elseif nLevel==4 and res=="1440p" then expx=0.6461 expy=0.6461 dividx=0.639 dividy=0.639
	
	elseif nLevel==5 and res=="720p" 	then expx=0.53 expy=0.53 dividx=0.53 dividy=0.53		--40x30
	elseif nLevel==5 and res=="1080p" 	then expx=0.578 expy=0.578 dividx=0.57 dividy=0.57		--40x30
	elseif nLevel==5 and res=="1440p" 	then expx=0.75 expy=0.75 dividx=0.8 dividy=0.8
	elseif nLevel==5 and res=="4k" 		then expx=1.2 expy=1.2 dividx=1.2 dividy=1.2
	
	elseif nLevel==6 and res=="1080p" then expx=0.53 expy=0.53 dividx=1.06 dividy=1.06				--36x33
	elseif nLevel==6 and res=="1440p" then expx=0.728 expy=0.728 dividx=1.468 dividy=1.468
		
	elseif nLevel==7 and res=="1080p" then expx=0.612 expy=0.612 dividx=0.62 dividy=0.62		--52x15
	elseif nLevel==7 and res=="1440p" then expx=0.82 expy=0.82 dividx=0.82 dividy=0.82
	elseif nLevel==7 and res=="4k" 	then expx=1.23 expy=1.23 dividx=1.2 dividy=1.2
	
	elseif nLevel==8 and res=="1080p" then expx=0.77 expy=0.77 dividx=1.55 dividy=1.55			--23xx2
	elseif nLevel==8 and res=="1440p" then expx=1.1 expy=1.1 dividx=2.2 dividy=2.2
	elseif nLevel==8 and res=="4k" then expx=1.65 expy=1.65 dividx=3.3 dividy=3.3
	
	elseif nLevel==9 and res=="1080p" then expx=0.93 expy=0.93 dividx=1.82 dividy=1.82			--34x13
	elseif nLevel==9 and res=="1440p" then expx=1.25 expy=1.25 dividx=2.5 dividy=2.5
	
	elseif nLevel==10 and res=="720p" then expx=0.41 expy=0.41 dividx=0.41 dividy=0.41			--52x33
	elseif nLevel==10 and res=="1080p" then expx=0.53 expy=0.53 dividx=0.52 dividy=0.52
	elseif nLevel==10 and res=="1440p" then expx=0.725 expy=0.725 dividx=0.72 dividy=0.72
	
	elseif nLevel==11 and res=="1080p" then expx=0.548 expy=0.548 dividx=0.55 dividy=0.55			--45x32
	elseif nLevel==11 and res=="1440p" then expx=0.75 expy=0.75 dividx=0.75 dividy=0.75
	
	elseif nLevel==12 and res=="1080p" then expx=0.872 expy=0.872 dividx=0.88 dividy=0.88			--28x20
	elseif nLevel==12 and res=="1440p" then expx=1.2 expy=1.2 dividx=1.2 dividy=1.2
	
	elseif nLevel==13 and res=="1080p" then expx=0.58 expy=0.58	dividx=1.18 dividy=1.18			--35x29
	elseif nLevel==13 and res=="1440p" then expx=0.829 expy=0.829 dividx=1.62 dividy=1.62
	
	elseif nLevel==14 and res=="1080p" then expx=0.65 expy=0.65 dividx=1.3 dividy=1.3		--45x27
	elseif nLevel==14 and res=="1440p" then expx=0.89 expy=0.89 dividx=1.8 dividy=1.8
	
	elseif nLevel==15 and res=="1080p" then expx=0.748 expy=0.748 dividx=1.5 dividy=1.5			--43x19
	elseif nLevel==15 and res=="1440p" then expx=0.99 expy=0.99 dividx=1.9 dividy=1.9
	
	elseif nLevel==16 and res=="1080p" then expx=0.47 expy=0.47 dividx=0.95 dividy=0.95			--50x37
	elseif nLevel==16 and res=="1440p" then expx=0.65 expy=0.65 dividx=1.3 dividy=1.3
		
	elseif nLevel==17  and res=="1080p" then expx=0.612 expy=0.612 dividx=1.2 dividy=1.2
	elseif nLevel==17  and res=="1440p" then expx=0.82 expy=0.82 dividx=1.6 dividy=1.6
		
	elseif nLevel==18 and res=="720p" then expx=0.41 expy=0.41 dividx=0.41 dividy=0.41			--52x33
	elseif nLevel==18 and res=="1080p" then expx=0.53 expy=0.53 dividx=0.52 dividy=0.52
	elseif nLevel==18 and res=="1440p" then expx=0.725 expy=0.725 dividx=0.72 dividy=0.72
		
	elseif nLevel==19 and res=="1080p" then expx=0.46 expy=0.46 dividx=0.9 dividy=0.9
	elseif nLevel==19 and res=="1440p" then expx=0.63 expy=0.63 dividx=1.28 dividy=1.28
		
	elseif nLevel==20  and res=="1080p" then expx=0.485 expy=0.485 dividx=0.95 dividy=0.95
	elseif nLevel==20  and res=="1440p" then expx=0.665 expy=0.665 dividx=1.32 dividy=1.32
		
	elseif nLevel==21  and res=="1080p" then expx=0.64 expy=0.64 dividx=1.33 dividy=1.33
	elseif nLevel==21  and res=="1440p" then expx=0.852 expy=0.852 dividx=1.7 dividy=1.7
		
	elseif nLevel==22  and res=="1080p" then expx=0.582 expy=0.582 dividx=1.15 dividy=1.15
	elseif nLevel==22  and res=="1440p" then expx=0.8 expy=0.8 dividx=1.6 dividy=1.6
		
	elseif nLevel==23  and res=="1080p" then expx=0.585 expy=0.585 dividx=1.18 dividy=1.18
	elseif nLevel==23  and res=="1440p" then expx=0.8 expy=0.8 dividx=1.6 dividy=1.6
	
	elseif nLevel==24 and res=="1080p" then expx=0.486 expy=0.486 dividx=0.95 dividy=0.95
	elseif nLevel==24 and res=="1440p" then expx=0.665 expy=0.665 dividx=1.35 dividy=1.35
	
	
	elseif nLevel==25 and res=="1080p" then expx=0.625 expy=0.625 dividx=1.25 dividy=1.25
	elseif nLevel==25 and res=="1440p" then expx=0.855 expy=0.855 dividx=1.7 dividy=1.7
	
	elseif nLevel==26 and res=="1080p" then expx=0.584 expy=0.584 dividx=1.2 dividy=1.2
	elseif nLevel==26 and res=="1440p" then expx=0.8 expy=0.8 dividx=1.6 dividy=1.6
	
	elseif nLevel==27 and res=="1080p" then expx=0.546 expy=0.546 dividx=1.05 dividy=1.05
	elseif nLevel==27 and res=="1440p" then  expx=0.75 expy=0.75 dividx=1.5 dividy=1.5
	
	elseif nLevel==28 and res=="1080p" then expx=0.485 expy=0.485 dividx=0.95 dividy=0.95
	elseif nLevel==28 and res=="1440p" then expx=0.665 expy=0.665 dividx=1.32 dividy=1.32
		
	elseif nLevel==29  and res=="1080p" then expx=0.5 expy=0.5 dividx=1 dividy=1
	elseif nLevel==29  and res=="1440p" then expx=0.685 expy=0.685 dividx=1.36 dividy=1.36
	
	elseif nLevel==30 and res=="1080p" then expx=0.69 expy=0.69 dividx=1.38 dividy=1.38
	elseif nLevel==30 and res=="1440p" then expx=0.96 expy=0.96 dividx=2 dividy=2
	
	elseif nLevel==31 and res=="1080p" then expx=0.46 expy=0.46 dividx=0.91 dividy=0.91
	elseif nLevel==31 and res=="1440p" then expx=0.63 expy=0.63 dividx=1.26 dividy=1.26
	
	elseif nLevel==32 and res=="1080p" then expx=0.448 expy=0.448 dividx=0.89 dividy=0.89
	elseif nLevel==32 and res=="1440p" then expx=0.61 expy=0.61 dividx=1.26 dividy=1.26
		
	elseif nLevel==33 and res=="1080p" then expx=1.25 expy=1.25 dividx=2.5 dividy=2.5
	elseif nLevel==33 and res=="1440p" then expx=1.7 expy=1.7 dividx=3.5 dividy=3.5
	
	elseif nLevel==34 and res=="1080p" then expx=0.5 expy=0.5 dividx=1 dividy=1
	elseif nLevel==34 and res=="1440p" then expx=0.685 expy=0.685 dividx=1.4 dividy=1.4
	
	elseif nLevel==35  and res=="1080p"  then expx=0.5 expy=0.5 dividx=1 dividy=1
	elseif nLevel==35  and res=="1440p"  then expx=0.685 expy=0.685 dividx=1.38 dividy=1.38
		
	elseif nLevel==36  and res=="1080p" then expx=0.548 expy=0.548 dividx=1.105 dividy=1.105
	elseif nLevel==36  and res=="1440p" then expx=0.75 expy=0.75 dividx=1.55 dividy=1.55
	
	elseif nLevel==37 and res=="1080p" then expx=0.473 expy=0.473 dividx=0.92 dividy=0.92
	elseif nLevel==37 and res=="1440p" then expx=0.65 expy=0.65 dividx=1.3 dividy=1.3
	
	elseif nLevel==38 and res=="1080p" then expx=0.65 expy=0.65 dividx=1.3 dividy=1.3
	elseif nLevel==38 and res=="1440p" then expx=0.885 expy=0.885 dividx=1.78 dividy=1.78
	
	elseif nLevel==39 and res=="1080p" then expx=0.885 expy=0.885 dividx=1.4 dividy=1.4
	elseif nLevel==39 and res=="1440p" then expx=1.265 expy=1.265 dividx=1.91 dividy=1.91
	
	elseif nLevel==40 and res=="1080p" then expx=2.2 expy=2.2 dividx=1.1 dividy=1.1
	elseif nLevel==40 and res=="1440p" then expx=2.99 expy=2.99 dividx=1.5 dividy=1.5
	
	elseif nLevel==41 and res=="1080p" then expx=0.83 expy=0.83 dividix=1.5 dividy=1.5
	elseif nLevel==41 and res=="1440p" then expx=1.15 expy=1.15 dividx=2.25 dividy=2.25
	
	elseif nLevel==42 and res=="1080p" then expx=1.78 expy=1.78 dividx=0.9 dividy=0.9
	elseif nLevel==42 and res=="1440p" then expx=2.45 expy=2.45 dividx=1.23 dividy=1.23
	
	elseif nLevel==43 and res=="1080p" then expx=0.545 expy=0.545 dividx=1.1 dividy=1.1
	elseif nLevel==43 and res=="1440p" then expx=0.75 expy=0.75 dividx=1.5 dividy=1.5
	
	elseif nLevel==44 and res=="1080p" then expx=0.449 expy=0.449 dividx=0.9 dividy=0.9
	elseif nLevel==44 and res=="1440p" then expx=0.615 expy=0.615 dividx=1.22 dividy=1.22
	
	elseif nLevel==45 and res=="1080p" then expx=0.8 expy=0.8 dividx=1.6 dividy=1.6
	elseif nLevel==45 and res=="1440p" then expx=1.09 expy=1.09 dividx=2.2 dividy=2.2
	
	elseif nLevel==46 and res=="1080p" then expx=0.79 expy=0.79 dividx=1.55 dividy=1.55
	elseif nLevel==46 and res=="1440p" then expx=1.09 expy=1.09 dividx=2.2 dividy=2.2
		
	elseif nLevel==47 and res=="1080p" then expx=0.64 expy=0.64 dividx=1.28 dividy=1.28
	elseif nLevel==47 and res=="1440p" then expx=0.89 expy=0.89 dividx=1.8 dividy=1.8
	
	elseif nLevel==48 and res=="1080p" then expx=0.512 expy=0.515 dividx=1 dividy=1
	elseif nLevel==48 and res=="1440p" then expx=0.7 expy=0.7 dividx=1.4 dividy=1.4
	
	elseif nLevel==49 and res=="1080p" then expx=0.73 expy=0.73 dividx=1.5 dividy=1.5
	elseif nLevel==49 and res=="1440p" then expx=1 expy=1 dividx=2 dividy=2
	
	
	elseif nLevel==50 and res=="1080p" then expx=0.6 expy=0.6 dividx=1.18 dividy=1.18
	elseif nLevel==50 and res=="1440p" then expx=0.825 expy=0.825 dividx=1.65 dividy=1.65
	
	elseif nLevel==51 and res=="1080p" then expx=0.53 expy=0.53 dividx=1.05 dividy=1.05
	elseif nLevel==51 and res=="1440p" then expx=0.725 expy=0.725 dividx=1.45 dividy=1.45
	
	elseif nLevel==52 and res=="1080p" then expx=0.582 expy=0.582 dividx=1.2 dividy=1.2
	elseif nLevel==52 and res=="1440p" then expx=0.8 expy=0.8 dividx=1.7 dividy=1.7
	
	elseif nLevel==53 and res=="1080p" then expx=0.57 expy=0.57 dividx=1.15 dividy=1.15
	elseif nLevel==53 and res=="1440p" then expx=0.772 expy=0.772 dividx=1.55 dividy=1.55
	
	elseif nLevel==54 and res=="1080p" then expx=0.648 expy=0.648 dividx=1.28 dividy=1.28
	elseif nLevel==54 and res=="1440p" then expx=0.888 expy=0.888 dividx=1.8 dividy=1.8
	
	elseif nLevel==55 and res=="1080p" then expx=0.7 expy=0.7 dividx=1.4 dividy=1.4
	elseif nLevel==55 and res=="1440p" then expx=0.97 expy=0.97 dividx=1.9 dividy=1.9

	elseif nLevel==56 and res=="1080p" then expx=0.475 expy=0.475 dividx=0.9 dividy=0.9
	elseif nLevel==56 and res=="1440p" then expx=0.65 expy=0.65 dividx=1.3 dividy=1.3
	
	elseif nLevel==57  and res=="1080p" then expx=0.545 expy=0.545 dividx=1.1 dividy=1.1
	elseif nLevel==57  and res=="1440p" then expx=0.75 expy=0.75 dividx=1.5 dividy=1.5
	
	elseif nLevel==58  and res=="1080p" then expx=0.46 expy=0.46 dividx=0.9 dividy=0.9
	elseif nLevel==58  and res=="1440p" then expx=0.63 expy=0.63 dividx=1.25 dividy=1.25
	
	elseif nLevel==59 and res=="1080p" then expx=0.448 expy=0.448 dividx=0.92 dividy=0.92
	elseif nLevel==59 and res=="1440p" then expx=0.615 expy=0.615 dividx=1.25 dividy=1.25
		
	elseif nLevel==60 and res=="1080p" then expx=0.97 expy=0.97 dividx=1 dividy=1
	elseif nLevel==60 and res=="1440p" then expx=1.33 expy=1.33 dividx=1.3 dividy=1.3
	
	elseif nLevel==61 and res=="1080p" then expx=0.56 expy=0.56 dividx=0.545 dividy=0.545
	elseif nLevel==61 and res=="1440p" then expx=0.77 expy=0.77 dividx=0.77 dividy=0.77
		
	elseif nLevel==62 and res=="1080p" then expx=0.58 expy=0.58 dividx=0.55 dividy=0.55
	elseif nLevel==62 and res=="1440p" then expx=0.8 expy=0.8 dividx=0.8 dividy=0.8
	
	elseif nLevel==63 and res=="1080p" then expx=0.624 expy=0.624 dividx=0.62 dividy=0.62
	elseif nLevel==63 and res=="1440p" then expx=0.86 expy=0.86 dividx=0.8 dividy=0.8
	
	elseif nLevel==64 and res=="1080p" then expx=0.487 expy=0.487 dividx=0.5 dividy=0.5
	elseif nLevel==64 and res=="1440p" then expx=0.665 expy=0.665 dividx=0.7 dividy=0.7
	
	elseif nLevel==65 and res=="1080p" then expx=0.55 expy=0.55 dividx=1.1 dividy=1.1
	elseif nLevel==65 and res=="1440p" then expx=0.75 expy=0.75 dividx=1.49 dividy=1.49
	
	elseif nLevel==66 and res=="1080p" then expx=0.49 expy=0.49 dividx=1 dividy=1
	elseif nLevel==66 and res=="1440p" then expx=0.667 expy=0.667 dividx=1.4 dividy=1.4
	
	elseif nLevel==67 and res=="1080p" then expx=0.467 expy=0.467 dividx=0.9 dividy=0.9
	elseif nLevel==67 and res=="1440p" then expx=0.648 expy=0.648 dividx=1.3 dividy=1.3
		
	elseif nLevel==68 and res=="1080p" then expx=0.54 expy=0.54 dividx=0.5 dividy=0.5
	elseif nLevel==68 and res=="1440p" then expx=0.74 expy=0.74 dividx=0.73 dividy=0.73
	
	elseif nLevel==69 and res=="1080p" then expx=0.65 expy=0.65 dividx=0.65 dividy=0.65
	elseif nLevel==69 and res=="1440p" then expx=0.89 expy=0.89 dividx=0.9 dividy=0.9
	
	elseif nLevel==70 and res=="1080p" then expx=0.5 expy=0.5 dividx=0.5 dividy=0.5
	elseif nLevel==70 and res=="1440p" then expx=0.685 expy=0.685 dividx=0.7 dividy=0.7
	
	elseif nLevel==71 and res=="1080p" then expx=0.665 expy=0.665 dividx=0.65 dividy=0.65
	elseif nLevel==71 and res=="1440p" then expx=0.89 expy=0.89 dividx=0.9 dividy=0.9
	
	elseif nLevel==72 and res=="1080p" then expx=0.74 expy=0.74 dividx=0.73 dividy=0.73
	elseif nLevel==72 and res=="1440p" then expx=1.05 expy=1.05 dividx=1.05 dividy=1.05
		
	elseif nLevel==73 and res=="1080p" then expx=0.562 expy=0.562 dividx=0.57 dividy=0.57
	elseif nLevel==73 and res=="1440p" then expx=0.748 expy=0.748 dividx=0.74 dividy=0.74
		
	elseif nLevel==74 and res=="1080p" then expx=0.439 expy=0.439 dividx=0.44 dividy=0.44
	elseif nLevel==74 and res=="1440p" then expx=0.6 expy=0.6 dividx=0.6 dividy=0.6
		
	elseif nLevel==75 and res=="1080p" then expx=0.485 expy=0.485 dividx=0.48 dividy=0.48
	elseif nLevel==75 and res=="1440p" then expx=0.665 expy=0.665 dividx=0.65 dividy=0.65
		
	elseif nLevel==76 and res=="1080p" then expx=0.8 expy=0.8 dividx=0.78 dividy=0.78
	elseif nLevel==76 and res=="1440p" then expx=1.09 expy=1.09 dividx=1.1 dividy=1.1

	elseif nLevel==77 and res=="1080p" then expx=0.55 expy=0.55 dividx=0.52 dividy=0.52
	elseif nLevel==77 and res=="1440p" then expx=0.75 expy=0.75 dividx=0.75 dividy=0.75
	
	elseif nLevel==78 and res=="1080p" then expx=1.14 expy=1.14 dividx=1.15 dividy=1.15
	elseif nLevel==78 and res=="1440p" then expx=1.5 expy=1.5 dividx=1.5 dividy=1.5
	
	elseif nLevel==79 and res=="1080p" then expx=0.48 expy=0.48 dividx=0.45 dividy=0.45
	elseif nLevel==79 and res=="1440p" then expx=0.65 expy=0.65 dividx=0.65 dividy=0.65
		
		--screensaver
		elseif nLevel==101 and res=="1440p" then expx=0.715 expy=0.715 dividx=1.42 dividy=1.42
		elseif nLevel==102 and res=="1440p" then expx=0.715 expy=0.715 dividx=1.42 dividy=1.42
		elseif nLevel==103 and res=="1440p" then expx=0.715 expy=0.715 dividx=1.42 dividy=1.42
	end
end

function drawforshader2classic()


if background=="yes" and palette==1 then

		if nLevel==1 then love.graphics.setColor(scolor1,scolor1[4]) animatewhale()
	elseif nLevel==3 or nLevel==52 or nLevel==101 then love.graphics.setColor(scolor1,scolor1[4]) animateffish1()
	elseif nLevel==7 then animatedolphin()
	elseif nLevel==15  then animatedshark()
	elseif nLevel==34 or nLevel==103 then love.graphics.setColor(scolor1,scolor1[4]) animatewhale() animatefishs(dt)
	elseif nLevel==35 then love.graphics.setColor(scolor1,scolor1[4]) animatemantaray() animatefishs()
	--elseif nLevel==36 then love.graphics.setColor(1,1,1,0.85) animatewhale() 
	elseif nLevel==14 or nLevel==102 or nLevel==103 then animatefishs()
	end	-- animate the whale
--print(beat)

			if not currentbck4==false then
			if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,scolor4[4]}
				love.graphics.setColor(scolor4)
				end
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor3)
					end
				end
				-- Chromatic aberration
						if chromaab==false then	love.graphics.draw (currentbck4, bckx,bcky,0,expx,expy)
					elseif chromaab==true then
						love.graphics.setColor(1,0,0,scolor4[4]/3)
						love.graphics.draw (currentbck4, bckx,bcky,0,expx,expy)
						love.graphics.setColor(0,1,0,scolor4[4]/3)
						love.graphics.draw (currentbck4, bckx+5,bcky-5,0,expx,expy)
						love.graphics.setColor(0,0,1,scolor4[4]/3)
						love.graphics.draw (currentbck4, bckx+10,bcky-10,0,expx,expy)
					end
			end
	
			if not currentbck3==false then

			if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,scolor4[4]}
				love.graphics.setColor(scolor4)
				end
			
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor4)
					end
				end
				if nLevel==36 then love.graphics.setColor(1,1,1,0.85) animatewhale() end
				if nLevel==54 and enginepushed then	love.graphics.setColor(1/subbeat/2,0,0,1)
						if timer<5 then
						love.graphics.draw (currentbck3, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
						elseif timer >4 then
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("godsray")
							setflangereffect()
							--audio effect
							engineon:setEffect('myEffect')
						if isgamepad==true and vibration==true then joystickvibration = joystick:setVibration(0.05, 0.05,-1) end							
							
						love.graphics.setShader()
						effect(function()
						love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
						end)
						end
				else
						setreverbeffect()
						-- Chromatic aberration
						if chromaab==false then love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
					elseif chromaab==true then
						love.graphics.setColor(1,0,0,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
						love.graphics.setColor(0,1,0,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x+5,bck2y-5,0,expx,expy)
						love.graphics.setColor(0,0,1,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x+10,bck2y-10,0,expx,expy)
					end
					
				end
			end
			
			if not currentbck2==false and shader1type=="bluenoise" then
			-- Set the dithering shader
				love.graphics.setShader(blueNoiseDitherShader)
				
				love.graphics.draw (currentbck2, frgx,frgy,0,expx,expy)
				-- Reset the shader
				love.graphics.setShader()blueNoiseDitherShader:send("intensity", ditherIntensity)
			end
			if not currentbck2==false and not (shader1type=="bluenoise") then
			

				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor2)		-- adjust colors
				else scolor2={adjustr,adjustg,adjustb,scolor2[4]}
				love.graphics.setColor(scolor2)
				end
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat/15,beat/15,beat/15,0.85)
					else love.graphics.setColor(scolor2)
					end
					elseif nLevel==1 or nLevel==5 or nLevel==6 or nLevel==8 or nLevel==13 or nLevel==17 or nLevel==102 then
						--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader4:send("time", timer)
						love.graphics.setShader(warpshader4)
						love.graphics.setCanvas()
				end
						
					if nLevel==26 then
						--if timer<74 then
							--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader2:send("time", timer)
						love.graphics.setShader(warpshader1)
						love.graphics.setCanvas()
						--elseif timer>74 then love.graphics.setColor(subbeat/25,subbeat/25,subbeat*5,0.7)
						--end
					end
					
					if nLevel==54 and enginepushed then
							love.graphics.setColor(1/subbeat/2,0,0,1)
						love.graphics.draw (currentbck2, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
					end
					
					-- Chromatic aberration
						if chromaab==false then love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
					elseif chromaab==true then
						love.graphics.setColor(1,0,0,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
						love.graphics.setColor(0,1,0,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x+5,bck2y-5,0,expx,expy)
						love.graphics.setColor(0,0,1,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x+10,bck2y-10,0,expx,expy)
					end
				
		

		
				--No power animation
				if nLevel==15 then
					love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,1)
					love.graphics.draw (level15nopower, bck2x,bck2y,0,expx,expy)
				end
			end
			if nLevel==16 or nLevel==32 then animatefishs() animateffish1() end
			
		if nLevel==7 or nLevel==30 or nLevel==31 or nLevel==34 or nLevel==35 or nLevel==36 then animatefishs() end
		if nLevel==29 then love.graphics.setColor(1,1,1,0.8) animatefishs() end
		
		--tetris
		if tetris==true then blocksdraw(dt) end
		
			if not currentbck==false and shader1type=="bluenoise" then
				-- Set the dithering shader
				love.graphics.setShader(blueNoiseDitherShader)
				-- Pass the intensity value to the shader
				love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)	-- Render the background inside the blue noise dithering shader
				-- Reset the shader
				love.graphics.setShader()blueNoiseDitherShader:send("intensity", ditherIntensity)
			end
		
			if not currentbck==false and not (shader1type=="bluenoise") then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor1)		-- adjust colors
				else scolor1={adjustr,adjustg,adjustb,scolor1[4]}
				love.graphics.setColor(scolor1)
				end
				
				if nLevel==1 or nLevel==8 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader2:send("time", timer)
						love.graphics.setShader(warpshader2)
						love.graphics.setCanvas()
					
						love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
				end
			
				--if nLevel==2 or nLevel==3 or nLevel==4 or nLevel==5 or nLevel==57 or nLevel==58 or nLevel>8 and not nLevel==54 then
					--water warp shader
					if not ((nLevel==1) or (nLevel==7) or (nLevel==8) or (nLevel==11) or (nLevel==16) or (nLevel==32) or (nLevel==56)) then
						love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()
						love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
				end
			
					if nLevel==20 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/80,beat/20,beat/25,0.85)
						else love.graphics.setColor(scolor1)
						end
					end
						
					if nLevel==26 or nLevel==27 or nLevel==53 or (nLevel==56 and lightswitchon==true) or nLevel==81 then
						--if timer <15.8 or timer>74 then
							--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader2:send("time", timer)
						love.graphics.setShader(warpshader2)
						love.graphics.setCanvas()
			
					end
						
						if nLevel==16 or nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/15,beat/25,beat*5,0.85)
						else love.graphics.setColor(scolor1)
						end
					end
					
						
					if nLevel==38 and timer>39 and timer <136 then love.graphics.setColor(1,1,subbeat*3,0.65) 
					end
					
					if nLevel==52 then
						if beat<50 then mybeat=beat
						love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.2)
							love.graphics.setColor(beat/40,beat/30,beat/40,0.85)
						else love.graphics.setColor(scolor3)
							--water warp shader
								love.graphics.setCanvas(canvas)
								warpshader3:send("time", timer)
								love.graphics.setShader(warpshader3)
								love.graphics.setCanvas()
						end
					end
					
					if nLevel==54 and enginepushed then love.graphics.setColor(subbeat,1,0.3,0.3) end
					if nLevel==55 and pipepushed==true then love.graphics.setColor(0.3,0,0,1) end
					if nLevel==56 then
							if lightswitchon==false then love.graphics.setColor(0,0,0,0)
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(0.6-(1/subbeat/3.2),0.4,0.4,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(0.5-(1/subbeat/3.2),0.3,0.3,1)
						end
					end
					if nLevel==66 then love.graphics.setColor(1,1,1,timer/10) end
			
				love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
					love.graphics.setShader()
			end
							if nLevel==3 or nLevel==19 or nLevel==20 or nLevel==28 or nLevel==21 or nLevel==101 then love.graphics.setColor(1,1,1,0.85) animatefishs() end
			

			
			if not currentbck3==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,scolor4[4]}
				love.graphics.setColor(scolor4)
				end
				if nLevel==20 then
					if beat>50 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()
						love.graphics.draw (currentbck3, bckx,bcky,0,expx,expy)
						love.graphics.setShader()
					end

				elseif nLevel==23 or nLevel==28 or nLevel==61 then
				--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()
						love.graphics.draw (currentbck3, bckx,bcky,0,expx,expy)
						love.graphics.setShader()
				end
				--if nLevel==19 or nLevel==20 or nLevel==28 then animatefishs() end
			end	
			

			
			if not currentfrg==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor3)		-- adjust colors
				else scolor3={adjustr,adjustg,adjustb,scolor3[4]}
				love.graphics.setColor(scolor3)
				end
					
					if nLevel==15 or nLevel==20 or nLevel==32 or nLevel==52 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/40,beat/30,beat/40,0.85)
						else love.graphics.setColor(scolor3)
							
						end
					end
					
				
					if nLevel==33 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()
					end
					
					if nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/10,beat/12,beat*5,0.85)
				
						else love.graphics.setColor(scolor1)
						end
					end
					
					--fade in animation level 38
				--[[if nLevel==38 then 
							--love.graphics.setColor(subbeat*5,subbeat*5,subbeat*5,1)
						if timer<39	then love.graphics.setColor(beat/35,beat/35,beat/35,0.85) 
					elseif timer>39 and timer <74 then love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.5)
					end
				end--]]
					
				if nLevel==55 and pipepushed==true then love.graphics.setColor(1/subbeat/3+0.35,0,0,1) end
					if nLevel==56 then
							if lightswitchon==false then love.graphics.setColor(0,0,0,0)
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.4,0.4,1)
						end
					end
				
				if nLevel==61 then
					if beat<50 then mybeat=beat
							love.graphics.setColor(beat/20,beat/15,0,0.6)
					else love.graphics.setColor(1,1,0,0.6)
					end
				end
				
				--shake screen when heavy object falls
					if screenshake==true then
					if shader1type=="bluenoise" then	-- draw the blue noise shader if enabled
						love.graphics.setShader(blueNoiseDitherShader)		 		-- Set the dithering shader
						blueNoiseDitherShader:send("intensity", ditherIntensity)	-- Pass the intensity value to the shader
						love.graphics.draw (currentfrg, frgx+ math.random(0,2),frgy+ math.random(0,2),0,expx,expy)
					else
						love.graphics.draw (currentfrg, frgx+ math.random(0,2),frgy+ math.random(0,2),0,expx,expy)
					end
				elseif screenshake==false then
					if shader1type=="bluenoise" then	-- draw the blue noise shader if enabled
						love.graphics.setShader(blueNoiseDitherShader)		 		-- Set the dithering shader
						blueNoiseDitherShader:send("intensity", ditherIntensity)	-- Pass the intensity value to the shader
						love.graphics.draw (currentfrg, frgx,frgy,0,expx,expy)
					else
						love.graphics.draw (currentfrg, frgx,frgy,0,expx,expy)
					end
				end
				
				if nLevel==32 or nLevel==33 then love.graphics.setShader() end
				
				if nLevel==52 and not currentbck2==false then
							love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.2)
							love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
				end
				
			
					if nLevel==54 and enginepushed then
						love.graphics.setColor(1/subbeat/3,0,0,1)
						love.graphics.draw (currentfrg, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
					end

			

			end

end


if not (palette==1) then
				love.graphics.setColor(0.146, 0.73, 0.73)
				if not (shader1type=="godsgrid") then
						if res=="1080p" then love.graphics.rectangle('fill', 000, 0,1920, 1080)
					elseif res=="1440p" then love.graphics.rectangle('fill', 000, 0,2560, 1440)
					end
				end
pb:drawMap ()
--pb:drawBlocks ()
end


	if shader2==true then
		if nLevel==32 then			--water effect to objects
						--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader2:send("time", timer)
						love.graphics.setShader(warpshader2)
						love.graphics.setCanvas()    
							pb:drawBlocks ()
						love.graphics.setShader()
		else
						pb:drawBlocks ()
		end
		
	end

    if playerscells=="yes" then
		pb:drawAgents ()
    end
    
    -- Render frg2 inside blue noise dithering shader
			if not currentfrg2==false and (shader1type=="bluenoise") then
					-- Set the dithering shader
					love.graphics.setShader(blueNoiseDitherShader)
					-- Pass the intensity value to the shader
					blueNoiseDitherShader:send("intensity", ditherIntensity)
					-- Render the foreground
					love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)
					-- Reset the shader
					love.graphics.setShader()
				end
	-- Render frg2
    if not currentfrg2==false and not (shader1type=="bluenoise") then
    		if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor3)		-- adjust colors
				else scolor3={adjustr,adjustg,adjustb,scolor3[4]}
				love.graphics.setColor(scolor3)
				end
    			
    						
   
				if nLevel==14 or nLevel==29 or nLevel==81 then
				love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()
				
				if nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/20,beat/24,beat*7,0.85)
						else love.graphics.setColor(scolor3)
						end
				elseif nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor3)
					end
				elseif nLevel==54 and not (enginepushed) then
						love.graphics.setColor(subbeat*5,subbeat*5,subbeat*3,0.5)
						love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)
				elseif nLevel==54 and enginepushed then
						love.graphics.setColor(subbeat*5,0,0,1)
						love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)

				end
			end
			
		--water warp shader
			love.graphics.setCanvas(canvas)
			if nLevel==15 then
			warpshader2:send("time", timer)
			love.graphics.setShader(warpshader2)
			elseif nLevel==11 or nLevel==34 or nLevel==103 then
				if palette==1 then
					warpshader:send("time", timer)
					love.graphics.setShader(warpshader)
				end
			end
			love.graphics.setCanvas()
			if palette==1 then
				love.graphics.draw (currentfrg2, frgx,frgy,0,expx,expy)
			end
	end
	
	

	showhelp()		-- call game(interface/showhelp function

optionmessagesfunction()
	      
end

function optionmessagesfunction()
--option messages:

--save / load option message
	--if savebuttonpressed==true and timer2<3 then love.graphics.print ("The save / load function is under development ", 300,500,0,1) end
	love.graphics.setFont(poorfishmiddle)
		if loadbuttonpressed==true and timer2<3 then love.graphics.print ("Settings loaded ", 500,500,0,1)
	elseif savebuttonpressed==true and timer2<3 then love.graphics.print ("Settings saved ", 500,500,0,1)		
	 end
	 
		if musicbuttonpressed==true and timer2<3 and musicison==true then
			love.graphics.print ("Music on", 300,500,0,1) 
	elseif musicbuttonpressed==true and timer2<3 and musicison==false then
			love.graphics.print ("Music off", 300,500,0,1)
	end
	
		if soundbuttonpressed==true and timer2<3 and soundon==true then
			love.graphics.print ("sound on", 300,500,0,1) 
	elseif soundbuttonpressed==true and timer2<3 and soundon==false then
			love.graphics.print ("sound off", 300,500,0,1)
	elseif soundbuttonpressed==true and timer2>3 then soundbuttonpressed=false
	end
	
		if talkiesbuttonpressed==true and timer2<3 and talkies==true then
			love.graphics.print ("Dubs & subtitles on", 300,500,0,1) 
	elseif talkiesbuttonpressed==true and timer2<3 and talkies==false then
			love.graphics.print ("Dubs & subtitles off", 300,500,0,1)
	elseif talkiesbuttonpressed==true and timer2>3 then talkiesbuttonpressed=false
	end
	
		if shadersbuttonpressed==true and timer2<3 and shader2==true then
			love.graphics.print ("Caustics effects on", 300,500,0,1) 
	elseif shadersbuttonpressed==true and timer2<3 and shader2==false then
			love.graphics.print ("Caustics effects off", 300,500,0,1)
	elseif shadersbuttonpressed==true and timer2>3 then shadersbuttonpressed=false
	end
	
		if crtbuttonpressed==true and timer2<3 and shader1==true then
			love.graphics.print ("CRT effect on", 300,500,0,1) 
	elseif crtbuttonpressed==true and timer2<3 and shader1==false then
			love.graphics.print ("CRT effect off", 300,500,0,1)
	end
	
		if skinbuttonpressed==true and timer2<3 and skin=="remake" then
			--love.graphics.print ("Remake graphics", 300,500,0,1) 
	elseif skinbuttonpressed==true and timer2<3 and skin=="classic" then
			--love.graphics.print ("Classic graphics", 300,500,0,1)
	elseif skinbuttonpressed==true and timer2>3 then skinbuttonpressed=false
	end
	
		if resolutionbuttonpressed==true and timer2<3 and res==1366 then
			love.graphics.print ("Resolution: 720p", 300,500,0,1) 
	elseif resolutionbuttonpressed==true and timer2<3 and res=="1080p" then
			love.graphics.print ("Resolution: 1080p", 300,500,0,1)
	elseif resolutionbuttonpressed==true and timer2>3 then resolutionbuttonpressed=false
	end
	
		if threedbuttonpressed==true and timer2<3 and threeD==true and screensaver==false then
			love.graphics.print ("3d engine on", 300,500,0,1) 
	elseif threedbuttonpressed==true and timer2<3 and threeD==false and screensaver==false then
			love.graphics.print ("3d engine off", 300,500,0,1)
	elseif threedbuttonpressed==true and timer2>3 then threedbuttonpressed=false
	end
	
		if dioramabuttonpressed==true and timer2<3 and screensaver==true then
			love.graphics.print ("Diorama on", 300,500,0,1) 
	elseif dioramabuttonpressed==true and timer2<3 and screensaver==true then
			love.graphics.print ("Diorama off", 300,500,0,1)
	elseif dioramabuttonpressed==true and timer2>3 then dioramabuttonpressed=false			
	end
	
		if touchbuttonpressed==true and timer2<3 and touchinterfaceison==true then
			love.graphics.print ("touch controls on", 300,500,0,1) 
	elseif touchbuttonpressed==true and timer2<3 and touchinterfaceison==false then
			love.graphics.print ("touch controls off", 300,500,0,1)
	elseif touchbuttonpressed==true and timer2>3 then touchbuttonpressed=false			
	end
	
		if debugbuttonpressed==true and timer2<3 and debugmode=="no" then
			love.graphics.print ("debug mode on", 300,500,0,1) 
	elseif debugbuttonpressed==true and timer2<3 and debugmode=="yes" then
			love.graphics.print ("debug mode off", 300,500,0,1)
	elseif debugbuttonpressed==true and timer2>3 then debugbuttonpressed=false			
	end
	
		if backgroundbuttonpressed==true and timer2<3 and background=="no" then
			love.graphics.print ("background on", 300,500,0,1) 
	elseif backgroundbuttonpressed==true and timer2<3 and background=="yes" then
			love.graphics.print ("background off", 300,500,0,1)
	elseif backgroundbuttonpressed==true and timer2>3 then backgroundbuttonpressed=false
	end
	
		if objectsbuttonpressed==true and timer2<3 and objectscells=="no" then
			love.graphics.print ("objects on", 300,500,0,1) 
	elseif objectsbuttonpressed==true and timer2<3 and objectscells=="yes" then
			love.graphics.print ("objects off", 300,500,0,1)
	elseif objectsbuttonpressed==true and timer2>3 then objectsbuttonpressed=false
	end
	
	
		if playersbuttonpressed==true and timer2<3 and playerscells=="no" then
			love.graphics.print ("players on", 300,500,0,1) 
	elseif playersbuttonpressed==true and timer2<3 and playerscells=="yes" then
			love.graphics.print ("players off", 300,500,0,1)
	elseif playersbuttonpressed==true and timer2>3 then playersbuttonpressed=false
	end
	
		
			if retrobuttonpressed==true and timer2<3 and palette==1 then love.graphics.print ("retro nes color scheme", 300,500,0,1) 
	elseif retrobuttonpressed==true and timer2<3 and palette==2 then love.graphics.print ("retro c64 color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2<3 and palette==3 then love.graphics.print ("retro nes color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2<3 and palette==4 then love.graphics.print ("retro speccy color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2<3 and palette==5 then love.graphics.print ("retro Megadrive color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2<3 and palette==6 then love.graphics.print ("retro High contrast color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2<3 and palette==7 then love.graphics.print ("retro grid color scheme", 300,500,0,1)
	elseif retrobuttonpressed==true and timer2>3 then retrobuttonpressed=false
	end
end

function drawallcontentclassic()

	 if threeD==true then
			draw3dborders()
			end
			
			if objectscells=="yes" and shader2==false then				-- if objects enabled in debug mode draw them
			
			if nLevel==30 or nLevel==31 then			--water effect to objects
						--water warp shader
						love.graphics.setCanvas(canvas)
						warpshader3:send("time", timer)
						love.graphics.setShader(warpshader3)
						love.graphics.setCanvas()    
							pb:drawBlocks ()
						love.graphics.setShader()
			else
						pb:drawBlocks ()
			end
		
		end

			if background=="yes" and shader2==true then 
				pb:drawMap () 
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor1)		-- adjust colors
				else scolor1={adjustr,adjustg,adjustb,scolor1[4]}
				love.graphics.setColor(scolor1)
				end
				-- set the size that the shaders draw
					if nLevel==1 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.253,0.5)
				elseif nLevel==1 and res=="1440p" then love.graphics.draw (smask, 0,0,0,0.392,0.5)
				elseif nLevel==2 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.34,0.5)
				elseif nLevel==2 and res=="1440p" then love.graphics.draw (smask, 0,0,0,0.483,0.5)
				--elseif nLevel==3 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==4 then love.graphics.draw (smask, 0,0,0,0.15,0.5)
				
				elseif nLevel==5 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==5 and res=="1440p" then love.graphics.draw (smask, 0,0,0,0.4,expy)
				elseif nLevel==6 then love.graphics.draw (smask, 0,0,0,0.28,0.5)
				
				elseif nLevel==7 then love.graphics.draw (smask, 0,0,0,expx*0.9,expy*0.4)
				elseif nLevel==8 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.15,0.5)
				elseif nLevel==8 and res=="1440p" then love.graphics.draw (smask, 0,0,0,0.395,0.5)
				--elseif nLevel==9 then love.graphics.draw (smask, 0,0,0,0.499,0.25)
				elseif nLevel==10 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==11 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
			--elseif nLevel==12 then love.graphics.draw (level1bck, 0,0,0,1/expx,1/expy)
				elseif nLevel==13 then love.graphics.draw (smask, 0,0,0,0.30,0.5)
				elseif nLevel==14 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==15 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==16 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==16 and res=="1080p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				elseif nLevel==17 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==18 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==19 then love.graphics.draw (smask, 0,0,0,0.35,0.5)
				elseif nLevel==20 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==21 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==22 then love.graphics.draw (smask, -100,-300,0,0.25/expx,0.2335/expy)
				elseif nLevel==23 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==25 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==26 then love.graphics.draw (smask, 0,0,0,0.17125/expx,0.2335/expy)
				elseif nLevel==27 then love.graphics.draw (smask, 0,0,0,0.17125/expx,0.2335/expy)
				elseif nLevel==28 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==29 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==30 then love.graphics.draw (smask, 0,0,0,0.294,0.5)
				elseif nLevel==31 then love.graphics.draw (smask, 0,0,0,0.294,0.5)
				elseif nLevel==32 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.155/expx,0.2335/expy)
				elseif nLevel==32 and res=="1440p" then love.graphics.draw (smask, 0,0,0,expx/1.6,expy)
				elseif nLevel==33 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==34 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==35 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				--elseif nLevel==36 then love.graphics.draw (smask, 0,0,0,0.18/expx,0.23/expy)
				elseif nLevel==37 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==38 then love.graphics.draw (smask, 0,0,0,0.175/expx,0.2335/expy)
				elseif nLevel==39 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==40 then love.graphics.draw (smask, 0,0,0,0.13/expx,0.2335/expy)
				elseif nLevel==46 then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				--elseif nLevel==52 then love.graphics.draw (smask, 0,0,0,0.357/expx,0.235/expy)
				--elseif nLevel==53 then love.graphics.draw (smask, 0,0,0,0.297/expx,0.235/expy)
				--elseif nLevel==54 then love.graphics.draw (smask, 0,0,0,0.297/expx,0.235/expy)
				elseif nLevel==59 then love.graphics.draw (smask, 0,0,0,0.30/expx,0.467/expy)
				elseif nLevel==60 then love.graphics.draw (smask, 0,0,0,0.5/expx,0.467/expy)
				elseif nLevel==61 then love.graphics.draw (smask, 0,0,0,0.35/expx,0.467/expy)
				elseif nLevel==81 then  love.graphics.draw (smask, 0,0,0,0.375/expx,0.4/expy)
				elseif nLevel==101 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==101 and res=="1440p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				elseif nLevel==102 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==102 and res=="1440p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				elseif nLevel==103 and res=="1080p" then love.graphics.draw (smask, 0,0,0,0.25/expx,0.2335/expy)
				elseif nLevel==103 and res=="1440p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				
				--elseif nLevel==34 then love.graphics.draw (smask, 0,0,0,0.31/expx,0.1125/expy)
				end

      end
			
			love.graphics.setColor(edgescolor)


		if playerscells=="yes" and shader2==false then				-- if players enabled in debug mode draw them
			pb:drawAgents ()
		end
		if debugmode=="yes" then 									-- If debug mode enabled draw
		pb:drawBackgroundGrid()

			
		end	
		
		
    
    if debugmode=="yes" and livereloaded=="yes" then				--If livemode is enabled you can update code by saving and click key -r-
			love.graphics.draw(reload,130,550)
			love.graphics.print("main.lua reloaded, press -r- to close this info",180,560)
	end
	
	

end		

