function loadlevelassets()

			if (nLevel==8)
			or (nLevel==19)
			or  (nLevel==29)
			or  (nLevel==37)
			or  (nLevel==44)
			or  (nLevel==51)
			or  (nLevel==58)
			or  (nLevel==64)
			or  (nLevel==70)
			or  (nLevel==79)
			then
				LoadLogoAnimation()
			end
--level 1
	
if nLevel==1 then

totalAssetsToLoad = getTotalAssetsToLoad(nLevel)
loadedAssetsCount = 0

	-- level 1 assets
	
			--level1frg2 = loadAsset("externalassets/levels/level1/level1frg2.webp", "Image")
			--level1frg = loadAsset("externalassets/levels/level1/level1frg.webp", "Image")
			--level1bck = loadAsset("/externalassets/levels/level1/level1bck.webp", "Image")
			--level1bck2 = loadAsset("/externalassets/levels/level1/level1bck2.webp", "Image")
			
		level1frg2 = loadAsset("/externalassets/levels/level1/level1frg2.webp", "Image")	 
		loadedAssetsCount= updateLoadingBar(loadedAssetsCount + 1, totalAssetsToLoad)
        level1frg = loadAsset("/externalassets/levels/level1/level1frg.webp", "Image")		 
        level1frgNormal = loadAsset("/externalassets/levels/level1/normals/level1frg.webp", "Image")		 
        level1frgHeight = loadAsset("/externalassets/levels/level1/height/level1frgheightmap.webp", "Image")		 
        loadedAssetsCount= updateLoadingBar(loadedAssetsCount + 1, totalAssetsToLoad)
        level1bck = loadAsset("/externalassets/levels/level1/level1bck.webp", "Image")		 
        level1bckNormal = loadAsset("/externalassets/levels/level1/normals/level1bckNormal.webp", "Image")		 
        level1bckHeight = loadAsset("/externalassets/levels/level1/height/level1bckheightmap.webp", "Image")		 
        loadedAssetsCount = updateLoadingBar(loadedAssetsCount + 1, totalAssetsToLoad)
        level1bck2 = loadAsset("/externalassets/levels/level1/level1bck2.webp", "Image")	 
        loadedAssetsCount = updateLoadingBar(loadedAssetsCount + 1, totalAssetsToLoad)

 envMap = love.graphics.newCubeImage({
					"/externalassets/objects/level1/cubemap/nx.png", "/externalassets/objects/level1/cubemap/ny.png", "/externalassets/objects/level1/cubemap/nz.png", "/externalassets/objects/level1/cubemap/px.png", "/externalassets/objects/level1/cubemap/py.png", "/externalassets/objects/level1/cubemap/pz.png"
					})
					

	
	loadwhale() -- load whale animation
	
	c64level1 = loadAsset("/externalassets/levels/level1/retro/c64level1.webp", "Image")
	sinclairlevel1 = loadAsset("/externalassets/levels/level1/retro/sinclairlevel1.webp", "Image")
	neslevel1 = loadAsset("/externalassets/levels/level1/retro/neslevel1.webp", "Image")
	
	--whale sounds
	whalecalllow = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallLow(echo)9.ogg","static" )
	whalecallmid = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallMid(echo)8.ogg","static" )
				
				--objects
				chair1= loadAsset("/externalassets/objects/level1/silla.webp", "Image")
				chair2= loadAsset("/externalassets/objects/level1/silla2.webp", "Image")
				pipe1= loadAsset("/externalassets/objects/level1/pipe1.webp", "Image")
				cushion1= loadAsset("/externalassets/objects/level1/cushion1.webp", "Image")
				table1= loadAsset("/externalassets/objects/level1/table1.webp", "Image")
				
				--Normal maps
				chair1Normal = love.graphics.newImage("/externalassets/objects/level1/sillaNormal.png")
				chair2Normal = love.graphics.newImage("/externalassets/objects/level1/silla2Normal.png")
				pipe1Normal = love.graphics.newImage("/externalassets/objects/level1/pipeNormal.png")
				cushion1Normal = love.graphics.newImage("/externalassets/objects/level1/cushion1Normal.png")
				table1Normal = love.graphics.newImage("/externalassets/objects/level1/table1Normal.png")
				
				-- height maps
				chair1Height = loadAsset("/externalassets/objects/level1/sillaHeight.webp", "Image")
				chair2Height = loadAsset("/externalassets/objects/level1/silla2Height.webp", "Image")
				pipe1Height = loadAsset("/externalassets/objects/level1/pipe1Height.webp", "Image")
				cushion1Height = loadAsset("/externalassets/objects/level1/cushion1Height.webp", "Image")
				table1Height = loadAsset("/externalassets/objects/level1/table1Height.webp", "Image")
				
			--retroobjects
				--sinclair
				z80chair1= loadAsset("/externalassets/objects/level1/sinclair/silla.webp", "Image")
				z80chair2= loadAsset("/externalassets/objects/level1/sinclair/silla2.webp", "Image")
				z80pipe1= loadAsset("/externalassets/objects/level1/sinclair/pipe1.webp", "Image")
				z80cushion1= loadAsset("/externalassets/objects/level1/sinclair/cushion1.webp", "Image")
				z80table1= loadAsset("/externalassets/objects/level1/sinclair/table1.webp", "Image")
				--nes
				neschair1= loadAsset("/externalassets/objects/level1/nes/silla.webp", "Image")
				neschair2= loadAsset("/externalassets/objects/level1/nes/silla2.webp", "Image")
				nespipe1= loadAsset("/externalassets/objects/level1/nes/pipe1.webp", "Image")
				nescushion1= loadAsset("/externalassets/objects/level1/nes/cushion1.webp", "Image")
				nestable1= loadAsset("/externalassets/objects/level1/nes/table1.webp", "Image")
				--c64
				c64chair1= loadAsset("/externalassets/objects/level1/c64/silla.webp", "Image")
				c64chair2= loadAsset("/externalassets/objects/level1/c64/silla2.webp", "Image")
				c64pipe1= loadAsset("/externalassets/objects/level1/c64/pipe1.webp", "Image")
				c64cushion1= loadAsset("/externalassets/objects/level1/c64/cushion1.webp", "Image")
				c64table1= loadAsset("/externalassets/objects/level1/c64/table1.webp", "Image")
				
		
	-- Reset the loading bar when all assets are loaded
        resetLoadingBar()	
			
elseif nLevel==2 then

 

			--background
			level2frg= loadAsset("/externalassets/levels/level2/level2frg.webp", "Image")
			level2frgNormals= loadAsset("/externalassets/levels/level2/normals/level2frg.webp", "Image")
			level2frgHeight= loadAsset("/externalassets/levels/level2/height/level2frgheightmap.webp", "Image")
			level2bck= loadAsset("/externalassets/levels/level2/level2bck.webp", "Image")
			level2bckNormals= loadAsset("/externalassets/levels/level2/normals/level2bckNormals.webp", "Image")
			level2bckHeight= loadAsset("/externalassets/levels/level2/height/level2bckheightmap.webp", "Image")
			level2bck2= loadAsset("/externalassets/levels/level2/level2bck2.webp", "Image")

		--objects
	
		warning = loadAsset("/externalassets/objects/level2/warning.webp", "Image")
		briefcase = loadAsset("/externalassets/objects/level2/briefcase.webp", "Image")
		briefcaseopen = loadAsset("/externalassets/objects/level2/briefcaseopen.webp", "Image")
		briefcaseopen2 = loadAsset("/externalassets/objects/level2/briefcaseopen2.webp", "Image")
		bottle = loadAsset("/externalassets/objects/level2/bottle.webp", "Image")
		L = loadAsset("/externalassets/objects/level2/L.webp", "Image")
		hexagonalnut = loadAsset("/externalassets/objects/level2/hexagonalnut.webp", "Image")
		hammer = loadAsset("/externalassets/objects/level2/hammer.webp", "Image")
		pliers = loadAsset("/externalassets/objects/level2/pliers.webp", "Image")
		pipehorizontal = loadAsset("/externalassets/objects/level2/pipehorizontal.webp", "Image")
		
		--Normals
	
		warningNormal = loadAsset("/externalassets/objects/level2/normals/warningNormals.webp", "Image")
		briefcaseNormal = loadAsset("/externalassets/objects/level2/normals/briefcaseNormals.webp", "Image")
		briefcaseopenNormal = loadAsset("/externalassets/objects/level2/normals/briefcaseopenNormals.webp", "Image")
		briefcaseopen2Normal = loadAsset("/externalassets/objects/level2/normals/briefcaseopenNormals.webp", "Image")
		bottleNormal = loadAsset("/externalassets/objects/level2/normals/bottleNormals.webp", "Image")
		LNormal = loadAsset("/externalassets/objects/level2/normals/LNormals.webp", "Image")
		hexagonalnutNormal = loadAsset("/externalassets/objects/level2/normals/hexagonalnutNormals.webp", "Image")
		hammerNormal = loadAsset("/externalassets/objects/level2/normals/hammerNormals.webp", "Image")
		pliersNormal = loadAsset("/externalassets/objects/level2/normals/pliersNormals.webp", "Image")
		pipehorizontalNormal = loadAsset("/externalassets/objects/level2/normals/pipehorizontalNormals.webp", "Image")
		
		--height maps
	
		warningHeight = loadAsset("/externalassets/objects/level2/heightmaps/warningheightmap.webp", "Image")
		briefcaseHeight = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseheightmap.webp", "Image")
		briefcaseopenHeight = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseopenheightmap.webp", "Image")
		briefcaseopen2Height = loadAsset("/externalassets/objects/level2/heightmaps/briefcaseopen2heightmap.webp", "Image")
		bottleHeight = loadAsset("/externalassets/objects/level2/heightmaps/bottleheightmap.webp", "Image")
		LHeight = loadAsset("/externalassets/objects/level2/heightmaps/Lheightmap.webp", "Image")
		hexagonalnutHeight = loadAsset("/externalassets/objects/level2/heightmaps/hexagonalnutheightmap.webp", "Image")
		hammerHeight = loadAsset("/externalassets/objects/level2/heightmaps/hammerheightmap.webp", "Image")
		pliersHeight = loadAsset("/externalassets/objects/level2/heightmaps/pliersheightmap.webp", "Image")
		pipehorizontalHeight = loadAsset("/externalassets/objects/level2/heightmaps/pipehorizontalheightmap.webp", "Image")

elseif nLevel==3 then
			--boids.img = loadAsset("/externalassets/levels/level3/boid.webp", "Image") 
			level3frg = loadAsset("/externalassets/levels/level3/level3frg.webp", "Image")
			ffish1 = loadAsset("/externalassets/levels/level3/ffish2.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			level3bck = loadAsset("/externalassets/levels/level3/level3bck1.webp", "Image")
			level3bck2 = loadAsset("/externalassets/levels/level3/level3bck12.webp", "Image")
			level3bck3 = loadAsset("/externalassets/levels/level3/level3bck2.webp", "Image")
			
			level3bckNormals = loadAsset("/externalassets/levels/level3/normals/level3bck1.webp", "Image")
			level3bck2Normals = loadAsset("/externalassets/levels/level3/normals/level3bck12.webp", "Image")
			level3frgNormals = loadAsset("/externalassets/levels/level3/normals/level3frg.webp", "Image")
			
			level3bckHeight = loadAsset("/externalassets/levels/level3/height/level3bck1heightmap.webp", "Image")
			level3bck2Height = loadAsset("/externalassets/levels/level3/height/level3bck12heightmap.webp", "Image")
			level3frgHeight = loadAsset("/externalassets/levels/level3/height/level3frgheightmap.webp", "Image")
			
			--objects
			shortpipe1 = loadAsset("/externalassets/objects/level3/shortpipe1.webp", "Image")
			minipipe = loadAsset("/externalassets/objects/level3/minipipe.webp", "Image")
			pipelonghorizontal = loadAsset("/externalassets/objects/level3/longpipehorizontal.webp", "Image")
			booklv3 = loadAsset("/externalassets/objects/level3/booklv3.webp", "Image")
			longpipehorizontal = loadAsset("/externalassets/objects/level3/longpipehorizontal.webp", "Image")
			apricosemarmelade = loadAsset("/externalassets/objects/level3/apricosemarmelade.webp", "Image")
			strawberrymarmelade = loadAsset("/externalassets/objects/level3/strawberrymarmelade.webp", "Image")
			honey = loadAsset("/externalassets/objects/level3/honey.webp", "Image")
			oillamp = loadAsset("/externalassets/objects/level3/oillamp.webp", "Image")
			snail = loadAsset("/externalassets/objects/level3/shell.webp", "Image")
			shell = loadAsset("/externalassets/objects/level3/snail.webp", "Image")
			coffee = loadAsset("/externalassets/objects/level3/coffee.webp", "Image")
			plate = loadAsset("/externalassets/objects/level3/plate.webp", "Image")
			axe = loadAsset("/externalassets/objects/level3/axe.webp", "Image")
	
elseif nLevel==4 then

			level4bck1 = loadAsset("/externalassets/levels/level4/level4bck1.webp", "Image")
			level4bck2 = loadAsset("/externalassets/levels/level4/level4bck2.webp", "Image")
			level4frg1 = loadAsset("/externalassets/levels/level4/level4frg1.webp", "Image")
			
			ostnatec_00 = loadAsset("/externalassets/objects/level4/ostnatec_00.webp", "Image")
			ostnatec_01 = loadAsset("/externalassets/objects/level4/ostnatec_00.webp", "Image")
			ostnatec_02 = loadAsset("/externalassets/objects/level4/ostnatec_00.webp", "Image")
			cpipe = loadAsset("/externalassets/objects/level4/cpipe.webp", "Image")
			book1 = loadAsset("/externalassets/objects/level4/book1.webp", "Image")
			book2 = loadAsset("/externalassets/objects/level4/book2.webp", "Image")
			book3 = loadAsset("/externalassets/objects/level4/book3.webp", "Image")
			book4 = loadAsset("/externalassets/objects/level4/book4.webp", "Image")
			book5 = loadAsset("/externalassets/objects/level4/book5.webp", "Image")
			book6 = loadAsset("/externalassets/objects/level4/book6.webp", "Image")
			book7 = loadAsset("/externalassets/objects/level4/book7.webp", "Image")
			book8 = loadAsset("/externalassets/objects/level4/book8.webp", "Image")
			book9 = loadAsset("/externalassets/objects/level4/book9.webp", "Image")
			bookbig1 = loadAsset("/externalassets/objects/level4/bookbig1.webp", "Image")
			booksmall1 = loadAsset("/externalassets/objects/level4/booksmall1.webp", "Image")
			booksmall2 = loadAsset("/externalassets/objects/level4/booksmall2.webp", "Image")
			booksmall3 = loadAsset("/externalassets/objects/level4/booksmall3.webp", "Image")
			bookhoriz1 = loadAsset("/externalassets/objects/level4/bookhoriz1.webp", "Image")
			bookhoriz2 = loadAsset("/externalassets/objects/level4/bookhoriz2.webp", "Image")

elseif nLevel==5 then

		level5frg = loadAsset("/externalassets/levels/level5/level5frg.webp", "Image")
        level5bck = loadAsset("/externalassets/levels/level5/level5bck.webp", "Image")
        level5bck2 = loadAsset("/externalassets/levels/level5/level5bck2.webp", "Image")
        level5bck3 = loadAsset("/externalassets/levels/level5/level5bck3.webp", "Image")
        
        --normals
        level5frgNormals = loadAsset("/externalassets/levels/level5/normals/level5frg.webp", "Image")
        level5bckNormals = loadAsset("/externalassets/levels/level5/normals/level5bck.webp", "Image")
        level5bck2Normals = loadAsset("/externalassets/levels/level5/normals/level5bck2.webp", "Image")
        
        --height maps
        level5frgheightmap = loadAsset("/externalassets/levels/level5/height/level5frgheightmap.webp", "Image")
        level5bckheightmap = loadAsset("/externalassets/levels/level5/height/level5bckheightmap.webp", "Image")
        level5bck2heightmap = loadAsset("/externalassets/levels/level5/height/level5bck2heightmap.webp", "Image")
        
        --objects
        --[[
		chair1upside= loadAsset("/externalassets/objects/level5/sillaupside1.webp", "Image")
		chair2upside= loadAsset("/externalassets/objects/level5/sillaupside2.webp", "Image")
		table1= loadAsset("/externalassets/objects/level5/table1.webp", "Image")
		plant= loadAsset("/externalassets/objects/level5/plant.webp", "Image")
		--snail00= loadAsset("/externalassets/objects/level5/snak.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/snail03.webp", "Image")
		plz00= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz01= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz02= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz03= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz04= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz05= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		plz06= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		--]]
		chair1upside= loadAsset("/externalassets/objects/level5/sillaupside1.webp", "Image")
			chair2upside= loadAsset("/externalassets/objects/level5/sillaupside2.webp", "Image")
			table1= loadAsset("/externalassets/objects/level5/table1.webp", "Image")
			plant= loadAsset("/externalassets/objects/level5/plant.webp", "Image")
			snail00 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level32/objects/upscaled/snail03.webp", "Image")
			
			plz00= loadAsset("/externalassets/classic/level5/objects/upscaled/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/classic/level5/objects/upscaled/plz01.webp", "Image")
			plz02= loadAsset("/externalassets/classic/level5/objects/upscaled/plz02.webp", "Image")
			plz03= loadAsset("/externalassets/classic/level5/objects/upscaled/plz03.webp", "Image")
			plz04= loadAsset("/externalassets/classic/level5/objects/upscaled/plz04.webp", "Image")
			plz05= loadAsset("/externalassets/classic/level5/objects/upscaled/plz05.webp", "Image")
			plz06= loadAsset("/externalassets/classic/level5/objects/upscaled/plz06.webp", "Image")
		

elseif nLevel==6 then


			level6frg= loadAsset("/externalassets/levels/level6/level6frg.webp", "Image")
			level6bck= loadAsset("/externalassets/levels/level6/level6bck.webp", "Image")
			level6bck2= loadAsset("/externalassets/levels/level6/level6bck2.webp", "Image")
			
			--object
			
			koste_00 = loadAsset("/externalassets/objects/level6/koste_00.webp", "Image")
			koste_01 = loadAsset("/externalassets/objects/level6/koste_00.webp", "Image")
			koste_02 = loadAsset("/externalassets/objects/level6/koste_00.webp", "Image")
			rock= loadAsset("/externalassets/objects/level6/rock.webp", "Image")
			rock2= loadAsset("/externalassets/objects/level6/rock2.webp", "Image")
			rock3= loadAsset("/externalassets/objects/level6/rock3.webp", "Image")
			rock4= loadAsset("/externalassets/objects/level6/rock4.webp", "Image")
			coal= loadAsset("/externalassets/objects/level6/coal.webp", "Image")
			longpipe= loadAsset("/externalassets/objects/level6/longpipe.webp", "Image")
			wood= loadAsset("/externalassets/objects/level6/wood.webp", "Image")
			wood2= loadAsset("/externalassets/objects/level6/wood2.webp", "Image")

elseif nLevel==7 then

			level7frg = loadAsset("/externalassets/levels/level7/level7frg.webp", "Image")
			level7bck = loadAsset("/externalassets/levels/level7/level7bck.webp", "Image")
			level7bck2 = loadAsset("/externalassets/levels/level7/level7bck2.webp", "Image")
			level7bck3 = loadAsset("/externalassets/levels/level7/level7bck3.webp", "Image")
      
							
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
      
      --dolphin
		dolphin1= loadAsset("/externalassets/objects/level7/dolphin/1.webp", "Image")
		dolphin2= loadAsset("/externalassets/objects/level7/dolphin/2.webp", "Image")
		dolphin3= loadAsset("/externalassets/objects/level7/dolphin/3.webp", "Image")
		dolphin4= loadAsset("/externalassets/objects/level7/dolphin/4.webp", "Image")
		dolphin5= loadAsset("/externalassets/objects/level7/dolphin/5.webp", "Image")
		dolphin6= loadAsset("/externalassets/objects/level7/dolphin/6.webp", "Image")
		dolphin7= loadAsset("/externalassets/objects/level7/dolphin/7.webp", "Image")
		dolphin8= loadAsset("/externalassets/objects/level7/dolphin/8.webp", "Image")
		dolphin9= loadAsset("/externalassets/objects/level7/dolphin/9.webp", "Image")
		dolphin10= loadAsset("/externalassets/objects/level7/dolphin/10.webp", "Image")
		dolphin11= loadAsset("/externalassets/objects/level7/dolphin/11.webp", "Image")
		dolphin12= loadAsset("/externalassets/objects/level7/dolphin/12.webp", "Image")
		dolphin13= loadAsset("/externalassets/objects/level7/dolphin/13.webp", "Image")
		dolphin14= loadAsset("/externalassets/objects/level7/dolphin/14.webp", "Image")
		dolphin15= loadAsset("/externalassets/objects/level7/dolphin/15.webp", "Image")
		dolphin16= loadAsset("/externalassets/objects/level7/dolphin/16.webp", "Image")
		dolphin17= loadAsset("/externalassets/objects/level7/dolphin/17.webp", "Image")
		dolphin18= loadAsset("/externalassets/objects/level7/dolphin/18.webp", "Image")
		dolphin19= loadAsset("/externalassets/objects/level7/dolphin/19.webp", "Image")
		dolphin20= loadAsset("/externalassets/objects/level7/dolphin/20.webp", "Image")
		dolphin21= loadAsset("/externalassets/objects/level7/dolphin/21.webp", "Image")
		dolphin22= loadAsset("/externalassets/objects/level7/dolphin/22.webp", "Image")
		dolphin23= loadAsset("/externalassets/objects/level7/dolphin/23.webp", "Image")
		dolphin24= loadAsset("/externalassets/objects/level7/dolphin/24.webp", "Image")
		dolphin25= loadAsset("/externalassets/objects/level7/dolphin/25.webp", "Image")
     
      --objects
      
      --snail00= loadAsset("/externalassets/objects/level11/snail1.webp", "Image")
      
      	--[[	snail00 = loadAsset("/externalassets/classic/level7/objects/snak.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level7/objects/snak.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level7/objects/snak.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level7/objects/snak.webp", "Image")
			
			matrace= loadAsset("/externalassets/objects/level7/matrace.webp", "Image")
			musla= loadAsset("/externalassets/objects/level7/musla.webp", "Image")
			ocel= loadAsset("/externalassets/objects/level7/ocel.webp", "Image")
			plz00= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz01= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz02= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz03= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz04= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz05= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
			plz06= loadAsset("/externalassets/objects/level5/plz00.webp", "Image")
		--]]
		ocel= loadAsset("/externalassets/objects/level7/ocel.webp", "Image")
			matrace = loadAsset("/externalassets/objects/level7/matrace.webp", "Image")
			snail00 = loadAsset("/externalassets/objects/level7/snail.webp", "Image")
			snail01 = loadAsset("/externalassets/objects/level7/snail.webp", "Image")
			snail02 = loadAsset("/externalassets/objects/level7/snail.webp", "Image")
			snail03 = loadAsset("/externalassets/objects/level7/snail.webp", "Image")
			
			musla = loadAsset("/externalassets/objects/level7/musla.webp", "Image")
			plz00= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz01= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz02= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz03= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz04= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz05= loadAsset("/externalassets/objects/level7/plz.webp", "Image")
			plz06= loadAsset("/externalassets/objects/level7/plz.webp", "Image")

elseif nLevel==8 then



	  level8bck= loadAsset("/externalassets/levels/level8/level8bck.webp", "Image")
      level8bck2= loadAsset("/externalassets/levels/level8/level8bck2.webp", "Image")
      level8bck3= loadAsset("/externalassets/levels/level8/level8bck3.webp", "Image")
      level8frg= loadAsset("/externalassets/levels/level8/level8frg.webp", "Image")

		paper= loadAsset("/externalassets/objects/level8/paper.webp", "Image")
		wc= loadAsset("/externalassets/objects/level8/wc.webp", "Image")
		pipe= loadAsset("/externalassets/objects/level8/pipe.webp", "Image")
		pipe2= loadAsset("/externalassets/objects/level8/pipe2.webp", "Image")

elseif nLevel==9 then
		
		level9bck= loadAsset("/externalassets/levels/level9/level9bck.webp", "Image")
		level9bck2= loadAsset("/externalassets/levels/level9/level9bck2.webp", "Image")
		level9frg= loadAsset("/externalassets/levels/level9/level9frg.webp", "Image")
		--[[		
		mirror= loadAsset("/externalassets/objects/level9/mirror.webp", "Image")
		kriz= loadAsset("/externalassets/objects/level9/kriz.webp", "Image")
		lahev= loadAsset("/externalassets/objects/level9/lahev.webp", "Image")
		matka_a= loadAsset("/externalassets/objects/level9/matka_a.webp", "Image")
		naboj= loadAsset("/externalassets/objects/level9/naboj.webp", "Image")
		peri00= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
		
			--peri00= loadAsset("/externalassets/classic/level9/objects/peri00.webp", "Image")
			peri01= loadAsset("/externalassets/classic/level9/objects/peri01.webp", "Image")
			peri02= loadAsset("/externalassets/classic/level9/objects/peri02.webp", "Image")
			peri03= loadAsset("/externalassets/classic/level9/objects/peri03.webp", "Image")
			peri04= loadAsset("/externalassets/classic/level9/objects/peri04.webp", "Image")
			peri05= loadAsset("/externalassets/classic/level9/objects/peri05.webp", "Image")
			peri06= loadAsset("/externalassets/classic/level9/objects/peri06.webp", "Image")
			peri07= loadAsset("/externalassets/classic/level9/objects/peri07.webp", "Image")
		--]]
			mirror= loadAsset("/externalassets/objects/level9/mirror.webp", "Image")
			kriz= loadAsset("/externalassets/objects/level9/kriz.webp", "Image")
			lahev= loadAsset("/externalassets/objects/level9/lahev.webp", "Image")
			matka_a= loadAsset("/externalassets/objects/level9/hexagonalnut.webp", "Image")
			naboj= loadAsset("/externalassets/objects/level9/naboj.webp", "Image")
			peri00= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri01= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri02= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri03= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri04= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri05= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri06= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			peri07= loadAsset("/externalassets/objects/level9/peri.webp", "Image")
			
		player1mirror = loadAsset("assets/players/mirror/player1mirror.webp", "Image")
		player1mirror2 = loadAsset("assets/players/mirror/player1mirror2.webp", "Image")
		player1mirror3 = loadAsset("assets/players/mirror/player1mirror3.webp", "Image")
		
		player1mirrorleft = loadAsset("assets/players/mirror/player1mirrorleft.webp", "Image")
		player1mirror2left = loadAsset("assets/players/mirror/player1mirror2left.webp", "Image")
		player1mirror3left = loadAsset("assets/players/mirror/player1mirror3left.webp", "Image")

elseif nLevel==10 or nLevel==18 then
	
		if nLevel==10 then
			level10frg= loadAsset("/externalassets/levels/level10/level10frg.webp", "Image")
			--level10bck4= loadAsset("/externalassets/levels/level10/level10bck.webp", "Image")
			level18bck= loadAsset("/externalassets/levels/level18/level18bck.webp", "Image")
			level18bck2= loadAsset("/externalassets/levels/level18/level18bck2.webp", "Image")
			level18bck3= loadAsset("/externalassets/levels/level18/level18bck3.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level10/pipe.webp", "Image")
		elseif nLevel==18 then
			level18frg= loadAsset("/externalassets/levels/level18/level18frg.webp", "Image")
			--level18bck4= loadAsset("/externalassets/levels/level10/level10bck.webp", "Image")
			level18bck= loadAsset("/externalassets/levels/level18/level18bck.webp", "Image")
			level18bck2= loadAsset("/externalassets/levels/level18/level18bck2.webp", "Image")
			level18bck3= loadAsset("/externalassets/levels/level18/level18bck3.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level18/pipe.webp", "Image")
		end

			
			cruiseship = loadAsset("/externalassets/objects/level10/cruise.webp", "Image")
			glass = loadAsset("/externalassets/objects/level10/glass.webp", "Image")
			glass2 = loadAsset("/externalassets/objects/level10/glass.webp", "Image")
			glass3 = loadAsset("/externalassets/objects/level10/glass.webp", "Image")
			glassstand = loadAsset("/externalassets/objects/level10/glassstand.webp", "Image")
			glassstand2 = loadAsset("/externalassets/objects/level10/glassstand.webp", "Image")
			glassstand3 = loadAsset("/externalassets/objects/level10/glassstand.webp", "Image")
			
			 dama00= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_00.webp", "Image")
			 dama01= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_01.webp", "Image")
			 dama02= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_02.webp", "Image")
			 dama03= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_03.webp", "Image")
			 dama04= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_04.webp", "Image")
			 dama05= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_05.webp", "Image")
			 dama06= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_06.webp", "Image")
			 dama07= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_07.webp", "Image")
			 dama08= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_08.webp", "Image")
			 dama09= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_09.webp", "Image")
			 dama10= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_10.webp", "Image")
			 dama11= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_11.webp", "Image")
			 dama12= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_12.webp", "Image")
			 dama13= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_13.webp", "Image")
			 dama14= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_14.webp", "Image")
			 dama15= loadAsset("/externalassets/classic/level10/objects/upscaled/dama_15.webp", "Image")
			 
			 frkavec00= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_00.webp", "Image")
			 frkavec01= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_01.webp", "Image")
			 frkavec02= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_02.webp", "Image")
			 frkavec03= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_03.webp", "Image")
			 frkavec04= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_04.webp", "Image")
			 frkavec05= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_05.webp", "Image")
			 frkavec06= loadAsset("/externalassets/classic/level10/objects/upscaled/frkavec_06.webp", "Image")
			 
			 kap00= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_00.webp", "Image")
			 kap01= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_01.webp", "Image")
			 kap02= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_02.webp", "Image")
			 kap03= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_03.webp", "Image")
			 kap04= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_04.webp", "Image")
			 kap05= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_05.webp", "Image")
			 kap06= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_06.webp", "Image")
			 kap07= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_07.webp", "Image")
			 kap08= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_08.webp", "Image")
			 kap09= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_09.webp", "Image")
			 kap10= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_10.webp", "Image")
			 kap11= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_11.webp", "Image")
			 kap12= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_12.webp", "Image")
			 kap13= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_13.webp", "Image")
			 kap14= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_14.webp", "Image")
			 kap15= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_15.webp", "Image")
			 kap16= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_16.webp", "Image")
			 kap17= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_17.webp", "Image")
			 kap18= loadAsset("/externalassets/classic/level10/objects/upscaled/kap_18.webp", "Image")
			 
			 lodnik00= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_00.webp", "Image")
			 lodnik01= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_01.webp", "Image")
			 lodnik02= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_02.webp", "Image")
			 lodnik03= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_03.webp", "Image")
			 lodnik04= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_04.webp", "Image")
			 lodnik05= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_05.webp", "Image")
			 lodnik06= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_06.webp", "Image")
			 lodnik07= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_07.webp", "Image")
			 lodnik08= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_08.webp", "Image")
			 lodnik09= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_09.webp", "Image")
			 lodnik10= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_10.webp", "Image")
			 lodnik11= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_11.webp", "Image")
			 lodnik12= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_12.webp", "Image")
			 lodnik13= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_13.webp", "Image")
			 lodnik14= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_14.webp", "Image")
			 lodnik15= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_15.webp", "Image")
			 lodnik16= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_16.webp", "Image")
			 lodnik17= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_17.webp", "Image")
			 lodnik18= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_18.webp", "Image")
			 lodnik19= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_19.webp", "Image")
			 lodnik20= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_20.webp", "Image")
			 lodnik21= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_21.webp", "Image")
			 lodnik22= loadAsset("/externalassets/classic/level10/objects/upscaled/lodnik_22.webp", "Image")

elseif nLevel==11 then
	
			UnderwaterBGBlank=  loadAsset("/externalassets/levels/level11/Underwater_BG_Blank.webp", "Image")
      		level11bck2= 		loadAsset("/externalassets/levels/level11/level11bck2.webp", "Image")
      		level11frg2= loadAsset("/externalassets/levels/level11/level11frg2.webp", "Image")
      		--plantsshadow= loadAsset("/externalassets/levels/level11/plantsshadow.webp", "Image")
      		foreground11= 		loadAsset("/externalassets/levels/level11/foreground11.webp", "Image")

			--objects
			pipe11= loadAsset("/externalassets/objects/level11/pipe11.webp", "Image")
			pipe4_11= loadAsset("/externalassets/objects/level11/pipe4_11.webp", "Image")
			maly_snek_00= loadAsset("/externalassets/objects/level11/snail1.webp", "Image")
			maly_snek_01= loadAsset("/externalassets/objects/level11/snail1.webp", "Image")
			maly_snek_02= loadAsset("/externalassets/objects/level11/snail1.webp", "Image")
			maly_snek_03= loadAsset("/externalassets/objects/level11/snail1.webp", "Image")
			bullet11= loadAsset("/externalassets/objects/level11/bullet11.webp", "Image")
			los_00 = loadAsset("/externalassets/objects/level11/elk.webp", "Image")
			los_01 = loadAsset("/externalassets/objects/level11/elk.webp", "Image")
			los_02 = loadAsset("/externalassets/objects/level11/elk.webp", "Image")
			los_03 = loadAsset("/externalassets/objects/level11/elk.webp", "Image")
			
			papoucha_00 = loadAsset("/externalassets/objects/level11/parrot.webp", "Image")
			papoucha_01 = loadAsset("/externalassets/objects/level11/parrot.webp", "Image")

elseif nLevel==12 then

	level12frg = loadAsset("/externalassets/levels/level12/level12frg.webp", "Image")
	level12bck = loadAsset("/externalassets/levels/level12/level12bck.webp", "Image")
	
	-- objects
	--[[
			cepicka = loadAsset("/externalassets/objects/level12/cepicka.webp", "Image")
			fi1 = loadAsset("/externalassets/objects/level12/fi1.webp", "Image")
			hat = loadAsset("/externalassets/objects/level12/hat.webp", "Image")
			medusa = loadAsset("/externalassets/objects/level12/medusa.webp", "Image")
			muslicka = loadAsset("/externalassets/objects/level12/muslicka.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level12/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/objects/level12/pipe2.webp", "Image")
		--]]	
			cepicka = loadAsset("/externalassets/objects/level12/cepicka.webp", "Image")
			rybicka_00 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_00.webp", "Image")
			rybicka_01 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_01.webp", "Image")
			rybicka_02 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_02.webp", "Image")
			rybicka_03 = loadAsset("/externalassets/classic/level12/objects/upscaled/rybicka_h_03.webp", "Image")
			hat = loadAsset("/externalassets/objects/level12/hat.webp", "Image")
			medusa_00 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_00.webp", "Image")
			medusa_01 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_01.webp", "Image")
			medusa_02 = loadAsset("/externalassets/classic/level12/objects/upscaled/medusa_02.webp", "Image")
			muslicka = loadAsset("/externalassets/classic/level12/objects/upscaled/muslicka.webp", "Image")
			pipe = loadAsset("/externalassets/classic/level12/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level12/objects/upscaled/pipe2.webp", "Image")
			
		elseif nLevel==13 or nLevel==17 then
				
				vikingframe=0
				snailframe=0		
						
		
		
			if nLevel==13 then
						--level13bck = loadAsset("/externalassets/levels/level13/level13bck.webp", "Image")
						level13frg = loadAsset("/externalassets/levels/level13/level13frg.webp", "Image")
			
						level17bck= loadAsset("/externalassets/levels/level17/level17bck.webp", "Image")
						level17bck2= loadAsset("/externalassets/levels/level13/level17bck2.webp", "Image")
						level17bck3= loadAsset("/externalassets/levels/level17/level17bck3.webp", "Image")
			elseif nLevel==17 then		
						level17bck= loadAsset("/externalassets/levels/level17/level17bck.webp", "Image")
						level17bck2= loadAsset("/externalassets/levels/level17/level17bck2.webp", "Image")
						level17bck3= loadAsset("/externalassets/levels/level17/level17bck3.webp", "Image")
						level17frg= loadAsset("/externalassets/levels/level17/level17frg.webp", "Image")
						boids.img = loadAsset("/externalassets/levels/level3/boidx16.webp", "Image") 
			end
			
				
						--objects
			
						axe1 = loadAsset("/externalassets/objects/level13/axe1.webp", "Image")
						axe2 = loadAsset("/externalassets/objects/level13/axe2.webp", "Image")
						shield1 = loadAsset("/externalassets/objects/level13/shield1.webp", "Image")
						shield2 = loadAsset("/externalassets/objects/level13/shield2.webp", "Image")
						snail00 = loadAsset("/externalassets/objects/level13/snail.webp", "Image")
						snail01 = loadAsset("/externalassets/objects/level13/snail.webp", "Image")
						snail02 = loadAsset("/externalassets/objects/level13/snail.webp", "Image")
						snail03 = loadAsset("/externalassets/objects/level13/snail.webp", "Image")
				
						vik1_00 = loadAsset("/externalassets/objects/level13/vik1.webp", "Image")
						vik1_01 = loadAsset("/externalassets/objects/level13/vik1.webp", "Image")
						vik1_02 = loadAsset("/externalassets/objects/level13/vik1.webp", "Image")
						vik1_03 = loadAsset("/externalassets/objects/level13/vik1.webp", "Image")
						vik1_04 = loadAsset("/externalassets/objects/level13/vik1.webp", "Image")
						
						vik2_00 = loadAsset("/externalassets/objects/level13/vik2.webp", "Image")
						vik2_01 = loadAsset("/externalassets/objects/level13/vik2.webp", "Image")
						vik2_02 = loadAsset("/externalassets/objects/level13/vik2.webp", "Image")
						vik2_03 = loadAsset("/externalassets/objects/level13/vik2.webp", "Image")
						vik2_04 = loadAsset("/externalassets/objects/level13/vik2.webp", "Image")
						
						vik3_00= loadAsset("/externalassets//objects/level13/vik3.webp", "Image")
						vik3_01= loadAsset("/externalassets//objects/level13/vik3.webp", "Image")
						vik3_02= loadAsset("/externalassets//objects/level13/vik3.webp", "Image")
						vik3_03= loadAsset("/externalassets//objects/level13/vik3.webp", "Image")
						vik3_04= loadAsset("/externalassets//objects/level13/vik3.webp", "Image")
						
						vik4_00= loadAsset("/externalassets//objects/level13/vik4.webp", "Image")
						vik4_01= loadAsset("/externalassets//objects/level13/vik4.webp", "Image")
						vik4_02= loadAsset("/externalassets//objects/level13/vik4.webp", "Image")
						vik4_03= loadAsset("/externalassets//objects/level13/vik4.webp", "Image")
						vik4_04= loadAsset("/externalassets//objects/level13/vik4.webp", "Image")
						
						vik5_00= loadAsset("/externalassets/objects/level13/vik5.webp", "Image")
						vik5_01= loadAsset("/externalassets/objects/level13/vik5.webp", "Image")
						vik5_02= loadAsset("/externalassets/objects/level13/vik5.webp", "Image")
						vik5_03= loadAsset("/externalassets/objects/level13/vik5.webp", "Image")
					
						vik6_00= loadAsset("/externalassets/objects/level13/vik6.webp", "Image")
						vik6_01= loadAsset("/externalassets/objects/level13/vik6.webp", "Image")
						vik6_02= loadAsset("/externalassets/objects/level13/vik6.webp", "Image")
						vik6_03= loadAsset("/externalassets/objects/level13/vik6.webp", "Image")
						
						vik7_00= loadAsset("/externalassets/objects/level13/vik7.webp", "Image")
						vik7_01= loadAsset("/externalassets/objects/level13/vik7.webp", "Image")
						vik7_02= loadAsset("/externalassets/objects/level13/vik7.webp", "Image")
						vik7_03= loadAsset("/externalassets/objects/level13/vik7.webp", "Image")
						vik7_04= loadAsset("/externalassets/objects/level13/vik7.webp", "Image")
						
						skull= loadAsset("/externalassets/objects/level13/skull.webp", "Image")
						
						--mastil
						drakar00= loadAsset("/externalassets/objects/level17/drakar-hlava_00.webp", "Image")
						drakar01= loadAsset("/externalassets/objects/level17/drakar-hlava_01.webp", "Image")
						drakar02= loadAsset("/externalassets/objects/level17/drakar-hlava_02.webp", "Image")
						--dog
						pesos_00 = loadAsset("/externalassets/objects/level13/pesos_00.webp", "Image")
						pesos_01 = loadAsset("/externalassets/objects/level13/pesos_00.webp", "Image")
						pesos_02 = loadAsset("/externalassets/objects/level13/pesos_00.webp", "Image")
						--pipes
						pipe= loadAsset("/externalassets/objects/level17/pipe.webp", "Image")
						pipe2= loadAsset("/externalassets/objects/level17/pipe2.webp", "Image")
						
						korunka= loadAsset("/externalassets/objects/level17/korunka.webp", "Image")
						shield= loadAsset("/externalassets/objects/level17/shield.webp", "Image")
						
						skullpushed=false
		
						skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==14 then
		
		level14frg= loadAsset("/externalassets/levels/level14/level14frg.webp", "Image")
		level14frg2= loadAsset("/externalassets/levels/level14/level14frg2.webp", "Image")
		level14bck= loadAsset("/externalassets/levels/level36/level36bck3.webp", "Image")
		
		--fish
		
			--ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		--objects
			--[[
			eye= loadAsset("/externalassets/objects/level14/eye.webp", "Image")
			chairleft= loadAsset("/externalassets/objects/level14/chairleft.webp", "Image")
			chairright= loadAsset("/externalassets/objects/level14/chairright.webp", "Image")
			chairup= loadAsset("/externalassets/objects/level14/chairup.webp", "Image")
			chairdown= loadAsset("/externalassets/objects/level14/chairdown.webp", "Image")
			pipe1= loadAsset("/externalassets/objects/level14/pipe1.webp", "Image")
			pipe2= loadAsset("/externalassets/objects/level14/pipe2.webp", "Image")
			pipe3= loadAsset("/externalassets/objects/level14/pipe3.webp", "Image")
		--]]
			chairleft = loadAsset("/externalassets/objects/level14/chairleft.webp", "Image")
			chairright = loadAsset("/externalassets/objects/level14/chairright.webp", "Image")
			chairup = loadAsset("/externalassets/objects/level14/chairup.webp", "Image")
			chairdown = loadAsset("/externalassets/objects/level14/chairdown.webp", "Image")
			oko_00 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_00.webp", "Image")
			oko_01 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_01.webp", "Image")
			oko_02 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_02.webp", "Image")
			oko_03 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_03.webp", "Image")
			oko_04 = loadAsset("/externalassets/classic/level14/objects/upscaled/oko_04.webp", "Image")
			pipe1 = loadAsset("/externalassets/objects/level14/pipe1.webp", "Image")
			pipe2 = loadAsset("/externalassets/objects/level14/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/objects/level14/pipe3.webp", "Image")
		
		elseif nLevel==15 then
			
			level15frg= loadAsset("/externalassets/levels/level15/level15frg.webp", "Image")
			level15frg2= loadAsset("/externalassets/levels/level15/level15frg2.webp", "Image")
			level15bck= loadAsset("/externalassets/levels/level15/level15bck.webp", "Image")
			level15bck2= loadAsset("/externalassets/levels/level15/level15bck2.webp", "Image")
			level15bck3= loadAsset("/externalassets/levels/level15/level15bck3.webp", "Image")
			level15bck4= loadAsset("/externalassets/levels/level15/level15bck4.webp", "Image")
			level15bck5= loadAsset("/externalassets/levels/level15/level15bck5.webp", "Image")
			level15nopower= loadAsset("/externalassets/levels/level15/level15nopower.webp", "Image")
			bubbles= loadAsset("/externalassets/levels/level15/bubbles3840.webp", "Image")
			--[[
			--HDR
			level15frg= loadAsset("/externalassets/levels/level15/HDR/level15frg.webp", "Image")
			level15frg2= loadAsset("/externalassets/levels/level15/HDR/level15frg2.webp", "Image")
			level15bck= loadAsset("/externalassets/levels/level15/HDR/level15bck.webp", "Image")
			level15bck2= loadAsset("/externalassets/levels/level15/HDR/level15bck2.webp", "Image")
			level15bck3= loadAsset("/externalassets/levels/level15/HDR/level15bck3.webp", "Image")
			level15bck4= loadAsset("/externalassets/levels/level15/HDR/level15bck4.webp", "Image")
			level15nopower= loadAsset("/externalassets/levels/level15/HDR/level15nopower.webp", "Image")
			--]]
			--objects
			
			pipe= loadAsset("/externalassets/objects/level15/pipe.webp", "Image")
			mikroskop_00= loadAsset("/externalassets/objects/level15/mikroskop_00.webp", "Image")
			mikroskop_01= loadAsset("/externalassets/objects/level15/mikroskop_01.webp", "Image")
			mikroskop_02= loadAsset("/externalassets/objects/level15/mikroskop_02.webp", "Image")
			basephone= loadAsset("/externalassets/objects/level15/basephone.webp", "Image") 
			basephone2= loadAsset("/externalassets/objects/level15/basephone2.webp", "Image")
			phone= loadAsset("/externalassets/objects/level15/phone.webp", "Image") 
			phone2= loadAsset("/externalassets/objects/level15/phone2.webp", "Image")
			snek_00= loadAsset("/externalassets/objects/level15/snek_00.webp", "Image")
			snek_01= loadAsset("/externalassets/objects/level15/snek_00.webp", "Image")
			budik_00= loadAsset("/externalassets/objects/level15/budik_00.webp", "Image")
			alarmbig= loadAsset("/externalassets/objects/level15/alarmbig.webp", "Image")
			alarmclock3= loadAsset("/externalassets/objects/level15/alarmclock3.webp", "Image")
			budik_01= loadAsset("/externalassets/objects/level15/budik_01.webp", "Image")
			dalekohled_00= loadAsset("/externalassets/objects/level15/dalekohled_00.webp", "Image")
			dalekohled_01= loadAsset("/externalassets/objects/level15/dalekohled_00.webp", "Image")
		
		
		
		--shark
		shark1= loadAsset("/externalassets/objects/level15/shark/1.webp", "Image")
		shark2= loadAsset("/externalassets/objects/level15/shark/2.webp", "Image")
		shark3= loadAsset("/externalassets/objects/level15/shark/3.webp", "Image")
		shark4= loadAsset("/externalassets/objects/level15/shark/4.webp", "Image")
		shark5= loadAsset("/externalassets/objects/level15/shark/5.webp", "Image")
		shark6= loadAsset("/externalassets/objects/level15/shark/6.webp", "Image")
		shark7= loadAsset("/externalassets/objects/level15/shark/7.webp", "Image")
		shark8= loadAsset("/externalassets/objects/level15/shark/8.webp", "Image")
		shark9= loadAsset("/externalassets/objects/level15/shark/9.webp", "Image")
		shark10= loadAsset("/externalassets/objects/level15/shark/10.webp", "Image")
		shark11= loadAsset("/externalassets/objects/level15/shark/11.webp", "Image")
		shark12= loadAsset("/externalassets/objects/level15/shark/12.webp", "Image")
		shark13= loadAsset("/externalassets/objects/level15/shark/13.webp", "Image")
		shark14= loadAsset("/externalassets/objects/level15/shark/14.webp", "Image")
		shark15= loadAsset("/externalassets/objects/level15/shark/15.webp", "Image")
		shark16= loadAsset("/externalassets/objects/level15/shark/16.webp", "Image")
		shark17= loadAsset("/externalassets/objects/level15/shark/17.webp", "Image")
		shark18= loadAsset("/externalassets/objects/level15/shark/18.webp", "Image")
		shark19= loadAsset("/externalassets/objects/level15/shark/19.webp", "Image")
		shark20= loadAsset("/externalassets/objects/level15/shark/20.webp", "Image")
		shark21= loadAsset("/externalassets/objects/level15/shark/21.webp", "Image")
		shark22= loadAsset("/externalassets/objects/level15/shark/22.webp", "Image")
		shark23= loadAsset("/externalassets/objects/level15/shark/23.webp", "Image")
		shark24= loadAsset("/externalassets/objects/level15/shark/24.webp", "Image")
		shark25= loadAsset("/externalassets/objects/level15/shark/25.webp", "Image")
		shark26= loadAsset("/externalassets/objects/level15/shark/26.webp", "Image")
		shark27= loadAsset("/externalassets/objects/level15/shark/27.webp", "Image")
		shark28= loadAsset("/externalassets/objects/level15/shark/28.webp", "Image")
		shark29= loadAsset("/externalassets/objects/level15/shark/29.webp", "Image")
		shark30= loadAsset("/externalassets/objects/level15/shark/30.webp", "Image")
		
		elseif nLevel==16 then
	
			level16bck= loadAsset("/externalassets/levels/level16/level16bck.webp", "Image")
			level16bck2 = loadAsset("/externalassets/levels/level36/level36bck3.webp", "Image")
			level16frg= loadAsset("/externalassets/levels/level16/level16frg.webp", "Image")
		
		--objects
				--[[
				bullet = loadAsset("/externalassets/objects/level16/bullet.webp", "Image")
				 bulletleft = loadAsset("/externalassets/objects/level16/bulletleft.webp", "Image")
				 bulletright = loadAsset("/externalassets/objects/level16/bulletright.webp", "Image")
				 pipe = loadAsset("/externalassets/objects/level16/pipe.webp", "Image")
				 pipe2 = loadAsset("/externalassets/objects/level16/pipe2.webp", "Image")
				 pipe3 = loadAsset("/externalassets/objects/level16/pipe3.webp", "Image")
				 pipe4 = loadAsset("/externalassets/objects/level16/pipe4.webp", "Image")
				 pipe5 = loadAsset("/externalassets/objects/level16/pipe5.webp", "Image")
				 robothand = loadAsset("/externalassets/objects/level16/robothand.webp", "Image")
				 snail = loadAsset("/externalassets/objects/level16/snail.webp", "Image")
				 stairs = loadAsset("/externalassets/objects/level16/stairs.webp", "Image")
		--]]
				bullet = loadAsset("/externalassets/objects/level16/bullet.webp", "Image")
				 bulletleft = loadAsset("/externalassets/objects/level16/bulletleft.webp", "Image")
				 bulletright = loadAsset("/externalassets/objects/level16/bulletright.webp", "Image")
				 pipe = loadAsset("/externalassets/objects/level16/pipe.webp", "Image")
				 pipe2 = loadAsset("/externalassets/objects/level16/pipe2.webp", "Image")
				 pipe3 = loadAsset("/externalassets/objects/level16/pipe3.webp", "Image")
				 pipe4 = loadAsset("/externalassets/objects/level16/pipe4.webp", "Image")
				 pipe5 = loadAsset("/externalassets/objects/level16/pipe5.webp", "Image")
				 robothand = loadAsset("/externalassets/objects/level16/robothand.webp", "Image")
				 snail00 = loadAsset("/externalassets/objects/level16/snail.webp", "Image")
				snail01 = loadAsset("/externalassets/objects/level16/snail.webp", "Image")
				snail02 = loadAsset("/externalassets/objects/level16/snail.webp", "Image")
				snail03 = loadAsset("/externalassets/objects/level16/snail.webp", "Image")
				 stairs = loadAsset("/externalassets/objects/level16/stairs.webp", "Image")

		
			ffish1 = loadAsset("/externalassets/levels/level3/ffish2.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		

		
		
		elseif nLevel==19 then
		
		level19frg= loadAsset("/externalassets/levels/level19/level19frg.webp", "Image")
		--level19frg2= loadAsset("/externalassets/levels/level11/level11frg2.webp", "Image")
			level19bck= loadAsset("/externalassets/levels/level17/level17bck2.webp", "Image")
			level19bck2= loadAsset("/externalassets/levels/level18/level18bck.webp", "Image")
			level19bck3= loadAsset("/externalassets/levels/level18/level18bck2.webp", "Image")
			level19bck4= loadAsset("/externalassets/levels/level18/level18bck3.webp", "Image")
		
		--objects
		
			poseidon= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			neptun= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			bota = loadAsset("/externalassets/objects/level19/bota.webp", "Image")
			domino = loadAsset("/externalassets/objects/level19/domino.webp", "Image")
			hul = loadAsset("/externalassets/objects/level19/hul.webp", "Image")
			kuzelka = loadAsset("/externalassets/objects/level19/kuzelka.webp", "Image")
			palka = loadAsset("/externalassets/objects/level19/palka.webp", "Image")
			ping = loadAsset("/externalassets/objects/level19/ping.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level19/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/objects/level19/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/objects/level19/pipe3.webp", "Image")
			tenisak = loadAsset("/externalassets/objects/level19/tenisak.webp", "Image")
			
			poseidon00= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon01= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon02= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon03= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon04= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon05= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon06= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon07= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon08= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon09= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon10= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon11= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon12= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon13= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon14= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon15= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon16= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon17= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon18= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon19= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon20= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon21= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon22= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon23= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon24= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon25= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon26= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon27= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon28= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon29= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon30= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon31= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon32= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon33= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon34= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon35= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon36= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon37= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon38= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon39= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon40= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon41= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon42= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon43= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon44= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon45= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon46= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon47= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
			poseidon48= loadAsset("/externalassets/objects/level19/poseidon_00.webp", "Image")
		
			neptun00= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun01= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun02= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun03= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun04= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun05= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun06= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun07= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun08= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun09= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun10= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun11= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun12= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun13= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun14= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun15= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun16= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun17= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun18= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun19= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun20= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun21= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun22= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun23= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun24= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun25= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun26= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun27= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun28= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun29= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun30= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun31= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun32= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun33= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun34= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun35= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun36= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun37= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun38= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun39= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun40= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun41= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun42= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun43= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun44= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
			neptun45= loadAsset("/externalassets/objects/level19/neptun_00.webp", "Image")
	
		
			
		--fish
		
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==20 then
				
			level20frg= loadAsset("/externalassets/levels/level20/level20frg.webp", "Image")
			level20bck= loadAsset("/externalassets/levels/level20/level20bck.webp", "Image")
			level20bck2= loadAsset("/externalassets/levels/level20/level20bck2.webp", "Image")
			level20bck3= loadAsset("/externalassets/levels/level20/level20bck3.webp", "Image")
			
			--fishs
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
				--objects
			
			skull= loadAsset("/externalassets/objects/level20/skull.webp", "Image")
			amphora= loadAsset("/externalassets/objects/level20/amphora.webp", "Image")
			shell= loadAsset("/externalassets/objects/level20/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/objects/level20/sculpture.webp", "Image")
			elevator1= loadAsset("/externalassets/objects/level20/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/objects/level20/elevator2.webp", "Image")
			cross= loadAsset("/externalassets/objects/level20/cross.webp", "Image")
			
				skullpushed=false
		
				skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==21 then
			
			--background
			level21frg= loadAsset("/externalassets/levels/level21/level21frg.webp", "Image")
			--level21bck2= loadAsset("/externalassets/levels/level21/level21bck2.webp", "Image")
			underwater_fantasy1= loadAsset("/externalassets/levels/level21/underwater_fantasy1.webp", "Image")
			underwater_fantasy1_2= loadAsset("/externalassets/levels/level21/underwater_fantasy1_2.webp", "Image")
			
			--objects
			
			snail= loadAsset("/externalassets/objects/level21/snail.webp", "Image")
			krab_00 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_01 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_02 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_03 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_04 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_05 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_06 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_07 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_08 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			krab_09 = loadAsset("/externalassets/objects/level21/crab.webp", "Image")
			pedestal= loadAsset("/externalassets/objects/level21/pedestal.webp", "Image")
			pipelong= loadAsset("/externalassets/objects/level21/pipelong.webp", "Image")
			pipeshort= loadAsset("/externalassets/objects/level21/pipeshort.webp", "Image")
			arm= loadAsset("/externalassets/objects/level21/arm.webp", "Image")
			platform= loadAsset("/externalassets/objects/level21/platform.webp", "Image")
			statue= loadAsset("/externalassets/objects/level21/statue.webp", "Image")
			
			--background fish
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==22 then
			
			level22frg= loadAsset("/externalassets/levels/level22/level22frg.webp", "Image")
			level22bck= loadAsset("/externalassets/levels/level22/level22bck.webp", "Image")
			level22bck2= loadAsset("/externalassets/levels/level22/level22bck2.webp", "Image")
			level22bck3= loadAsset("/externalassets/levels/level22/level22bck3.webp", "Image")
			
			--objects
			
			pipe1= loadAsset("/externalassets/objects/level22/pipe1.webp", "Image")
			pipe2= loadAsset("/externalassets/objects/level22/pipe2.webp", "Image")
			pipe3= loadAsset("/externalassets/objects/level22/pipe3.webp", "Image")
			pipe4= loadAsset("/externalassets/objects/level22/pipe4.webp", "Image")
			pipe5= loadAsset("/externalassets/objects/level22/pipe5.webp", "Image")
			pipe7= loadAsset("/externalassets/objects/level22/pipe7.webp", "Image")
			pipe8= loadAsset("/externalassets/objects/level22/pipe8.webp", "Image")
			pipe9= loadAsset("/externalassets/objects/level22/pipe9.webp", "Image")
			pipe10= loadAsset("/externalassets/objects/level22/pipe10.webp", "Image")
			platform= loadAsset("/externalassets/objects/level22/platform.webp", "Image")
			platform2= loadAsset("/externalassets/objects/level22/platform2.webp", "Image")
			platform3= loadAsset("/externalassets/objects/level22/platform3.webp", "Image")
			stairs= loadAsset("/externalassets/objects/level22/stairs.webp", "Image")
			sculpture= loadAsset("/externalassets/objects/level22/sculpture.webp", "Image")
			sculpture1= loadAsset("/externalassets/objects/level22/sculpture.webp", "Image")
			sculpture2= loadAsset("/externalassets/objects/level22/sculpture.webp", "Image")
			
		elseif nLevel==23 then
			
			level23frg= loadAsset("/externalassets/levels/level23/level23frg.webp", "Image")
			level23bck= loadAsset("/externalassets/levels/level23/level23bck.webp", "Image")
			level23bck2= loadAsset("/externalassets/levels/level23/level23bck2.webp", "Image")
			level23bck3= loadAsset("/externalassets/levels/level23/level23bck3.webp", "Image")
		
			--objects
			
			column1 = loadAsset("/externalassets/objects/level23/column1.webp", "Image")
			column2 = loadAsset("/externalassets/objects/level23/column2.webp", "Image")
			column3 = loadAsset("/externalassets/objects/level23/column3.webp", "Image")
			zeusfalled=false
			zeus = loadAsset("/externalassets/objects/level23/zeus.webp", "Image")
			temple1 = loadAsset("/externalassets/objects/level23/temple1.webp", "Image")
			temple2 = loadAsset("/externalassets/objects/level23/temple2.webp", "Image")
			stairs = loadAsset("/externalassets/objects/level23/stairs.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level23/pipe.webp", "Image")
		
		elseif nLevel==24 then
		
		level24frg= loadAsset("/externalassets/levels/level24/level24frg.webp", "Image")
		level24bck= loadAsset("/externalassets/levels/level24/level24bck.webp", "Image")
		--level24bck2= loadAsset("/externalassets/levels/level24/level24bck2.webp", "Image")
		
		--objects
		
			statue= loadAsset("/externalassets/objects/level24/statue.webp", "Image")
			col = loadAsset("/externalassets/objects/level24/col.webp", "Image")
			col2 = loadAsset("/externalassets/objects/level24/col2.webp", "Image")
			col3 = loadAsset("/externalassets/objects/level24/col3.webp", "Image")
			col4 = loadAsset("/externalassets/objects/level24/col4.webp", "Image")
			col5 = loadAsset("/externalassets/objects/level24/col5.webp", "Image")
			columns = loadAsset("/externalassets/objects/level24/columns.webp", "Image")
			snail = loadAsset("/externalassets/objects/level24/snail.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level24/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/objects/level24/pipe2.webp", "Image")
			pipe3 = loadAsset("/externalassets/objects/level24/pipe3.webp", "Image")
			pipe4 = loadAsset("/externalassets/objects/level24/pipe4.webp", "Image")
			platform = loadAsset("/externalassets/objects/level24/platform.webp", "Image")
			platform2 = loadAsset("/externalassets/objects/level24/platform.webp", "Image")
			platform3 = loadAsset("/externalassets/objects/level24/platform.webp", "Image")
			platform4 = loadAsset("/externalassets/objects/level24/platform.webp", "Image")
			pedestal = loadAsset("/externalassets/objects/level24/pedestal.webp", "Image")
			octopus = loadAsset("/externalassets/objects/level24/octopus.webp", "Image")
	
		elseif nLevel==25 then
		
				level25frg = loadAsset("/externalassets/levels/level25/level25frg.webp", "Image")
				level25bck = loadAsset("/externalassets/levels/level25/level25bck.webp", "Image")
				level25bck2 = loadAsset("/externalassets/levels/level25/level25bck2.webp", "Image")
				
				--objects
				
				egyptianwoman = loadAsset("/externalassets/objects/level25/egyptianwoman.webp", "Image")
				desticka = loadAsset("/externalassets/objects/level25/desticka.webp", "Image")
				faraon = loadAsset("/externalassets/objects/level25/faraon.webp", "Image")
				mumycat = loadAsset("/externalassets/objects/level25/mumycat.webp", "Image")
				mummy = loadAsset("/externalassets/objects/level25/mumysokol.webp", "Image")
				pipe = loadAsset("/externalassets/objects/level25/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/objects/level25/pipe2.webp", "Image")
				scarab = loadAsset("/externalassets/objects/level25/scarab.webp", "Image")
				stul = loadAsset("/externalassets/objects/level25/table.webp", "Image")
		
		elseif nLevel==26 then
		
			level26frg= loadAsset("/externalassets/levels/level26/level26frg.webp", "Image")
			level26bck= loadAsset("/externalassets/levels/level26/level26bck.webp", "Image")
			--level24bck2= loadAsset("/externalassets/levels/level24/level24bck2.webp", "Image")
						
			
			--objects
			
			crab= loadAsset("/externalassets/objects/level26/crab.webp", "Image")
			craba2= loadAsset("/externalassets/objects/level26/crab.webp", "Image")
			craba3= loadAsset("/externalassets/objects/level26/crab.webp", "Image")
			speaker= loadAsset("/externalassets/objects/level26/speaker.webp", "Image")
			pipe1= loadAsset("/externalassets/objects/level26/pipe1.webp", "Image")
			pipe5= loadAsset("/externalassets/objects/level26/pipe5.webp", "Image")
			bigpipe= loadAsset("/externalassets/objects/level26/bigpipe.webp", "Image")
			column1= loadAsset("/externalassets/objects/level26/column1.webp", "Image")
			column2= loadAsset("/externalassets/objects/level26/column2.webp", "Image")
			column3= loadAsset("/externalassets/objects/level26/column3.webp", "Image")
			column4= loadAsset("/externalassets/objects/level26/column4.webp", "Image")
			skull= loadAsset("/externalassets/objects/level26/skull.webp", "Image")
			statue= loadAsset("/externalassets/objects/level26/statue.webp", "Image")
			face= loadAsset("/externalassets/objects/level26/face.webp", "Image")
			statue= loadAsset("/externalassets/objects/level26/statue.webp", "Image")
		
		elseif nLevel==27 then
	
		
		level27frg= loadAsset("/externalassets/levels/level27/level27frg.webp", "Image")
		level27bck= loadAsset("/externalassets/levels/level27/level27bck.webp", "Image")
		--level24bck2= loadAsset("/externalassets/levels/level24/level24bck2.webp", "Image")
			
		--objects
		
				anticka_hlava_00 = loadAsset("/externalassets/objects/level27/anticka_hlava.webp", "Image")
				anticka_hlava_01 = loadAsset("/externalassets/objects/level27/anticka_hlava.webp", "Image")
				anticka_hlava_02 = loadAsset("/externalassets/objects/level27/anticka_hlava.webp", "Image")
				anticka_hlava_03 = loadAsset("/externalassets/objects/level27/anticka_hlava.webp", "Image")
				anticka_hlava_04 = loadAsset("/externalassets/objects/level27/anticka_hlava.webp", "Image")
				
				balonek_00 = loadAsset("/externalassets/objects/level27/ball1.webp", "Image")
				balonek_01 = loadAsset("/externalassets/objects/level27/ball1.webp", "Image")
				balonek_02 = loadAsset("/externalassets/objects/level27/ball1.webp", "Image")
				balonek_03 = loadAsset("/externalassets/objects/level27/ball1.webp", "Image")
				
				shrimp_00 = loadAsset("/externalassets/objects/level27/shrimp.webp", "Image")
				shrimp_01 = loadAsset("/externalassets/objects/level27/shrimp.webp", "Image")
				shrimp_02 = loadAsset("/externalassets/objects/level27/shrimp.webp", "Image")
				shrimp_03 = loadAsset("/externalassets/objects/level27/shrimp.webp", "Image")
				shrimp_04 = loadAsset("/externalassets/objects/level27/shrimp.webp", "Image")
				
				krab_00  = loadAsset("externalassets/classic/level29/objects/krab_00.webp", "Image")
				krab_01  = loadAsset("externalassets/classic/level29/objects/krab_01.webp", "Image")
				krab_02  = loadAsset("externalassets/classic/level29/objects/krab_02.webp", "Image")
				krab_03  = loadAsset("externalassets/classic/level29/objects/krab_03.webp", "Image")
				krab_04  = loadAsset("externalassets/classic/level29/objects/krab_04.webp", "Image")
				krab_05  = loadAsset("externalassets/classic/level29/objects/krab_05.webp", "Image")
				krab_06  = loadAsset("externalassets/classic/level29/objects/krab_06.webp", "Image")
				krab_07  = loadAsset("externalassets/classic/level29/objects/krab_07.webp", "Image")
				krab_08  = loadAsset("externalassets/classic/level29/objects/krab_08.webp", "Image")
				krab_09  = loadAsset("externalassets/classic/level29/objects/krab_09.webp", "Image")
				
				
				kr_00 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_01 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_02 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_03 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_04 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_05 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_06 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_07 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_08 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_09 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_10 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_11 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_12 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_13 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_14 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_15 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_16 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				kr_17 = loadAsset("/externalassets/objects/level27/kr_00.webp", "Image")
				
			statuecl= loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue1 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue2 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue3 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue4 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue5 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue6 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue7 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue8 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue9 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue10 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue11 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue12 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue13 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue14 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue15 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue16 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue17 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue18 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
			statue19 = loadAsset("/externalassets/objects/level27/statue.webp", "Image")
				
				krab_00  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_01  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_02  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_03  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_04  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_05  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_06  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_07  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_08  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				krab_09  = loadAsset("externalassets/objects/level27/crab.webp", "Image")
				
				pipe = loadAsset("/externalassets/classic/level27/objects/upscaled/pipe.webp", "Image")

				stone = loadAsset("/externalassets/objects/level27/zed-big.webp", "Image")
				stonesmall = loadAsset("/externalassets/objects/level27/stonesmall.webp", "Image")
				
				pipe = loadAsset("/externalassets/classic/level27/objects/upscaled/pipe.webp", "Image")
				
				
			
			
		elseif nLevel==28 then
		
			--background
			level28frg= loadAsset("/externalassets/levels/level28/level28frg.webp", "Image")
			level28bck= loadAsset("/externalassets/levels/level28/level28bck.webp", "Image")
			--level28bck2= loadAsset("/externalassets/levels/level28/level28bck2.webp", "Image")
			--level28bck3= loadAsset("/externalassets/levels/level28/level28bck3.webp", "Image")
			
			--background second version
			--level28frg= loadAsset("/externalassets/levels/level28/level28frg.webp", "Image")
			--level28bck4= loadAsset("/externalassets/levels/level21/screensaver/level21screen2.webp", "Image")
			--level28bck= loadAsset("/externalassets/levels/level21/underwater_fantasy1.webp", "Image")
			--level28bck3= loadAsset("/externalassets/levels/level21/screensaver/level21screen1.webp", "Image")
			--level28bck2= loadAsset("/externalassets/levels/level21/screensaver/level21bck2.webp", "Image")
			
			
			--objects 
			skull= loadAsset("/externalassets/objects/level20/skull.webp", "Image")
			amphora= loadAsset("/externalassets/objects/level20/amphora.webp", "Image")
			shell= loadAsset("/externalassets/objects/level20/shell.webp", "Image")
			sculpture= loadAsset("/externalassets/objects/level20/sculpture.webp", "Image")
			elevator1= loadAsset("/externalassets/objects/level20/elevator1.webp", "Image")
			elevator2= loadAsset("/externalassets/objects/level20/elevator2.webp", "Image")
			cross= loadAsset("/externalassets/objects/level20/cross.webp", "Image")
		
			--fishs
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
				skullavatar = loadAsset("externalassets/objects/level13/skullavatar.webp", "Image")
		
		elseif nLevel==29 then
		
		--background
			level29frg= loadAsset("/externalassets/levels/level29/level29frg.webp", "Image")
			level29frg2= loadAsset("/externalassets/levels/level34/screensaver/level34frg2.webp", "Image")
			level29bck= loadAsset("/externalassets/levels/level24/level24bck.webp", "Image")
			--level29bck2= loadAsset("/externalassets/levels/level25/level25bck2.webp", "Image")
			
			--objects
		
				plug = loadAsset("externalassets/objects/level29/plug.webp", "Image")
				pipe = loadAsset("externalassets/objects/level29/pipe.webp", "Image")
				pipe2 = loadAsset("externalassets/objects/level29/pipe2.webp", "Image")
				pipe3 = loadAsset("externalassets/objects/level29/pipe3.webp", "Image")
				pipe1block = loadAsset("externalassets/objects/level29/pipe1block.webp", "Image")
				pipe2blocks = loadAsset("externalassets/objects/level29/pipe2blocks.webp", "Image")
				column = loadAsset("externalassets/objects/level29/column.webp", "Image")
				column2 = loadAsset("externalassets/objects/level29/column2.webp", "Image")
				krab_00 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_01 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_02 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_03 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_04 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_05 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_06 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_07 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_08 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				krab_09 = loadAsset("externalassets/objects/level29/crab.webp", "Image")
				snail = loadAsset("externalassets/objects/level29/snail.webp", "Image")
				snail00 = loadAsset("externalassets/objects/level29/snail.webp", "Image")
				snail01 = loadAsset("externalassets/objects/level29/snail.webp", "Image")
				snail02 = loadAsset("externalassets/objects/level29/snail.webp", "Image")
				snail03 = loadAsset("externalassets/objects/level29/snail.webp", "Image")
				
				sculpture = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_00.webp", "Image")
				sculpture1 = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_01.webp", "Image")
				sculpture2 = loadAsset("externalassets/classic/level29/objects/upscaled/hlava_m-_02.webp", "Image")
				
				ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
				fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
				fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
				
		elseif nLevel==30 then
		
			boids.img = loadAsset("/externalassets/levels/level3/boidx16.webp", "Image") 
		
			--background
			level30frg= loadAsset("/externalassets/levels/level30/level30frg.webp", "Image")
			level30bck= loadAsset("/externalassets/levels/level30/level30bck.webp", "Image")
			level30bck2= loadAsset("/externalassets/levels/level30/level30bck2.webp", "Image")
			
			--objects
			
			crab = loadAsset("/externalassets/objects/level30/crab.webp", "Image")
			piece1= loadAsset("/externalassets/objects/level30/piece1.webp", "Image")
			piece2= loadAsset("/externalassets/objects/level30/piece2.webp", "Image")
			piece3= loadAsset("/externalassets/objects/level30/piece3.webp", "Image")
			pipe= loadAsset("/externalassets/objects/level30/pipe.webp", "Image")

			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
		
		elseif nLevel==31 then
		
		--objects
			
			boids.img = loadAsset("/externalassets/levels/level3/boidx16.webp", "Image") 
			labyrinth = loadAsset("/externalassets/objects/level31/labyrinth.webp", "Image")
			pipe = loadAsset("/externalassets/objects/level31/pipe.webp", "Image")
			
				--background
			level31frg= loadAsset("/externalassets/levels/level31/level31frg.webp", "Image")
			level31bck= loadAsset("/externalassets/levels/level31/level31bck.webp", "Image")
			level31bck2= loadAsset("/externalassets/levels/level31/level31bck2.webp", "Image")
			level31bck3= loadAsset("/externalassets/levels/level31/level31bck3.webp", "Image")
		
			ffish1 = loadAsset("/externalassets/levels/level3/ffish2.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==32 then
		
			boids.img = loadAsset("/externalassets/levels/level3/boidx16.webp", "Image") 
			level32frg= loadAsset("/externalassets/levels/level32/level32frg.webp", "Image")
			level32bck= loadAsset("/externalassets/levels/level32/level32bck.webp", "Image")
			level32bck2= loadAsset("/externalassets/levels/level32/level32bck2.webp", "Image")
			
			--objects
				coral_cerv = loadAsset("/externalassets/objects/level32/coral_cerv.webp", "Image")
				seahorse00 = loadAsset("/externalassets/objects/level32/seahorse00.webp", "Image")
				seahorse01 = loadAsset("/externalassets/objects/level32/seahorse01.webp", "Image")
				seahorse02 = loadAsset("/externalassets/objects/level32/seahorse02.webp", "Image")
				seahorse03 = loadAsset("/externalassets/objects/level32/seahorse03.webp", "Image")
				korala = loadAsset("/externalassets/objects/level32/korala.webp", "Image")
				koralb = loadAsset("/externalassets/objects/level32/koralb.webp", "Image")
				koral_bily = loadAsset("/externalassets/objects/level32/koral_bily.webp", "Image")
				koralc = loadAsset("/externalassets/objects/level32/koralc.webp", "Image")
				korald = loadAsset("/externalassets/objects/level32/korald.webp", "Image")
				koral_dlouhy = loadAsset("/externalassets/objects/level32/koral_dlouhy.webp", "Image")
				koral_zel = loadAsset("/externalassets/objects/level32/koral_zel.webp", "Image")
				musle_troj = loadAsset("/externalassets/objects/level32/musle_troj.webp", "Image")
				pipe = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe.webp", "Image")
				pipe2 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe2.webp", "Image")
				pipe3 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe3.webp", "Image")
				pipe4 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe4.webp", "Image")
				pipe5 = loadAsset("/externalassets/classic/level32/objects/upscaled/pipe5.webp", "Image")
				sasanka00 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka01 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka02 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka03 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka04 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka05 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka06 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				sasanka07 = loadAsset("/externalassets/objects/level32/sasanka00.webp", "Image")
				snail00 = loadAsset("/externalassets/objects/level32/snail00.webp", "Image")
				snail01 = loadAsset("/externalassets/objects/level32/snail00.webp", "Image")
				snail02 = loadAsset("/externalassets/objects/level32/snail00.webp", "Image")
				snail03 = loadAsset("/externalassets/objects/level32/snail00.webp", "Image")
			
			ffish1 = loadAsset("/externalassets/levels/level3/ffish2.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		elseif nLevel==33 then
		
			level33frg= loadAsset("/externalassets/levels/level33/level33frg.webp", "Image")
			level33frg2= loadAsset("/externalassets/levels/level33/level33frg2.webp", "Image")
			level33bck= loadAsset("/externalassets/levels/level33/level33bck.webp", "Image")
			level33bck2= loadAsset("/externalassets/levels/level33/level33bck2.webp", "Image")
		
		boids.img = loadAsset("/externalassets/levels/level3/boidx16.webp", "Image") 
		
		--objects
		
		crab00 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab00.webp", "Image")
			crab01 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab01.webp", "Image")
			crab02 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab02.webp", "Image")
			crab03 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab03.webp", "Image")
			
			crab200 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab200.webp", "Image")
			crab201 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab201.webp", "Image")
			crab202 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab202.webp", "Image")
			crab203 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab203.webp", "Image")
			
			
			crab300 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab300.webp", "Image")
			crab301 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab301.webp", "Image")
			crab302 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab302.webp", "Image")
			crab303 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab303.webp", "Image")
			
			
			crab400 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab400.webp", "Image")
			crab401 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab401.webp", "Image")
			crab402 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab402.webp", "Image")
			crab403 = loadAsset("/externalassets/classic/level33/objects/upscaled/crab403.webp", "Image")
			
			
			pipe = loadAsset("/externalassets/classic/level33/objects/upscaled/pipe.webp", "Image")
			pipe2 = loadAsset("/externalassets/classic/level33/objects/upscaled/pipe2.webp", "Image")
			
			seahorse00 = loadAsset("/externalassets/objects/level32/seahorse00.webp", "Image")
			seahorse01 = loadAsset("/externalassets/objects/level32/seahorse01.webp", "Image")
			seahorse02 = loadAsset("/externalassets/objects/level32/seahorse02.webp", "Image")
			seahorse03 = loadAsset("/externalassets/objects/level32/seahorse03.webp", "Image")
			
			snail00 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_00.webp", "Image")
			snail01 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_01.webp", "Image")
			snail02 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_02.webp", "Image")
			snail03 = loadAsset("/externalassets/classic/level33/objects/upscaled/snail_03.webp", "Image")
			
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			
			sepia = loadAsset("/externalassets/classic/level33/objects/upscaled/sepijka_00.webp", "Image")
		
		elseif nLevel==34 then
				
		loadwhale()
		
		if res=="720p" or res=="1080p" or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
		
		--background
			level34frg= loadAsset("/externalassets/levels/level34/level34frg.webp", "Image")
			level34frg2= loadAsset("/externalassets/levels/level34/level34frg2.webp", "Image")
			level34bck= loadAsset("/externalassets/levels/level34/level34bck.webp", "Image")
			level34bck2= loadAsset("/externalassets/levels/level34/level34bck2.webp", "Image")
			
		elseif res=="8k" then	-- 8k background and foreground
		
			level34frg= loadAsset("/externalassets/levels/level34/8k/level34frg8k.webp", "Image")
			level34frg2= loadAsset("/externalassets/levels/level34/8k/level34frg28k.webp", "Image")
			level34bck= loadAsset("/externalassets/levels/level34/8k/level34bck8k.webp", "Image")
			level34bck2= loadAsset("/externalassets/levels/level34/8k/level34bck28k.webp", "Image")
		end
			
			
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
				-- objects
				ocel1 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-1.webp", "Image")
				ocel2 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-2.webp", "Image")
				ocel3 = loadAsset("/externalassets/classic/level34/objects/upscaled/ocel-3.webp", "Image")
				balal_00 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_01 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_02 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_03 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_04 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_05 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_06 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_07 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_08 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_09 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				balal_10 = loadAsset("/externalassets/objects/level34/balal_00.webp", "Image")
				
				krab_00 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_01 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_02 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_03 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_04 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_05 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_06 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_07 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_08 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_09 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				
				maly_snek_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_00.webp", "Image")
				maly_snek_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_01.webp", "Image")
				maly_snek_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_02.webp", "Image")
				maly_snek_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/maly_snek_03.webp", "Image")
				sasanka_00 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_00.webp", "Image")
				sasanka_01 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_01.webp", "Image")
				sasanka_02 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_02.webp", "Image")
				sasanka_03 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_03.webp", "Image")
				sasanka_04 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_04.webp", "Image")
				sasanka_05 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_05.webp", "Image")
				sasanka_06 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_06.webp", "Image")
				sasanka_07 = loadAsset("/externalassets/classic/level34/objects/upscaled/sasanka_07.webp", "Image")
				sepie_00 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_01 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_02 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_03 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_04 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_05 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_06 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_07 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_08 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_09 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_10 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_11 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				sepie_12 = loadAsset("/externalassets/objects/level34/sepie_00.webp", "Image")
				shell1 = loadAsset("/externalassets/classic/level34/objects/upscaled/shell1.webp", "Image")
				
			
				
		
		
		
		elseif nLevel==35 then
		
		if res=="720p" or res=="1080p" or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
		 
			level35bck= loadAsset("/externalassets/levels/level35/level35bck.webp", "Image")
			level35bck2= loadAsset("/externalassets/levels/level35/level35bck2.webp", "Image")
			level35bck3= loadAsset("/externalassets/levels/level35/level35bck3.webp", "Image")
			level35frg= loadAsset("/externalassets/levels/level35/level35frg.webp", "Image")
			
		elseif res=="8k" then	-- 8k background and foreground			
		
		level35bck= loadAsset("/externalassets/levels/level35/8k/level35bck8k.webp", "Image")
			level35bck2= loadAsset("/externalassets/levels/level35/8k/level35bck28k.webp", "Image")
			level35bck3= loadAsset("/externalassets/levels/level35/8k/level35bck38k.webp", "Image")
			level35frg= loadAsset("/externalassets/levels/level35/8k/level35frg8k.webp", "Image")
		end
			-- objects
			
			crab= loadAsset("/externalassets/objects/level34/crab.webp", "Image")
			
				krab_00 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_01 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_02 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_03 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_04 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_05 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_06 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_07 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_08 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				krab_09 = loadAsset("/externalassets/objects/level34/crab.webp", "Image")
				
			muslicka = loadAsset("/externalassets/classic//level35/objects/upscaled/muslicka.webp", "Image")
			rejnok_00 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_00.webp", "Image")
			rejnok_01 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_01.webp", "Image")
			rejnok_02 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_02.webp", "Image")
			rejnok_03 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_03.webp", "Image")
			rejnok_04 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_04.webp", "Image")
			rejnok_05 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_05.webp", "Image")
			rejnok_06 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_06.webp", "Image")
			rejnok_07 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_07.webp", "Image")
			rejnok_08 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_08.webp", "Image")
			rejnok_09 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_09.webp", "Image")
			rejnok_10 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_10.webp", "Image")
			rejnok_11 = loadAsset("/externalassets/classic//level35/objects/upscaled/rejnok_11.webp", "Image")
			
			sasanka_00 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_01 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_02 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_03 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_04 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_05 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_06 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
			sasanka_07 = loadAsset("/externalassets/objects/level35/sasanka_00.webp", "Image")
				sepie_00 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_01 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_02 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_03 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_04 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_05 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_06 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_07 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_08 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_09 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_10 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_11 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
				sepie_12 = loadAsset("/externalassets/objects/level35/sepie_00.webp", "Image")
			shell1 = loadAsset("/externalassets/objects/level35/shell1.webp", "Image")
			tecko = loadAsset("/externalassets/classic//level35/objects/upscaled/tecko.webp", "Image")
			
			
			
			kankan7= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-7.webp", "Image")
			kankan8= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-8.webp", "Image")
			kankan9= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-9.webp", "Image")
			kankan12= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-12.webp", "Image")
			kankan17= loadAsset("/externalassets/classic//level35/objects/upscaled/kankan-17.webp", "Image")
			klavir_00= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_01= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_02= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_03= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_04= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_05= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_06= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_07= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_08= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			klavir_09= loadAsset("/externalassets/objects/level35/klavir_00.webp", "Image")
			
			korals= loadAsset("/externalassets/objects/level35/koral_s.webp", "Image")
			
			--Manta ray
		mantaray23= loadAsset("/externalassets/objects/level35/mantaray/23.webp", "Image")
		mantaray24= loadAsset("/externalassets/objects/level35/mantaray/24.webp", "Image")
		mantaray25= loadAsset("/externalassets/objects/level35/mantaray/25.webp", "Image")
		mantaray26= loadAsset("/externalassets/objects/level35/mantaray/26.webp", "Image")
		mantaray27= loadAsset("/externalassets/objects/level35/mantaray/27.webp", "Image")
		mantaray28= loadAsset("/externalassets/objects/level35/mantaray/28.webp", "Image")
		mantaray29= loadAsset("/externalassets/objects/level35/mantaray/29.webp", "Image")
		mantaray30= loadAsset("/externalassets/objects/level35/mantaray/30.webp", "Image")
		mantaray31= loadAsset("/externalassets/objects/level35/mantaray/31.webp", "Image")
		mantaray32= loadAsset("/externalassets/objects/level35/mantaray/32.webp", "Image")
		mantaray33= loadAsset("/externalassets/objects/level35/mantaray/33.webp", "Image")
		mantaray34= loadAsset("/externalassets/objects/level35/mantaray/34.webp", "Image")
		mantaray35= loadAsset("/externalassets/objects/level35/mantaray/35.webp", "Image")
		mantaray36= loadAsset("/externalassets/objects/level35/mantaray/36.webp", "Image")
		mantaray37= loadAsset("/externalassets/objects/level35/mantaray/37.webp", "Image")
		mantaray38= loadAsset("/externalassets/objects/level35/mantaray/38.webp", "Image")
		mantaray39= loadAsset("/externalassets/objects/level35/mantaray/39.webp", "Image")
		mantaray40= loadAsset("/externalassets/objects/level35/mantaray/40.webp", "Image")
		mantaray41= loadAsset("/externalassets/objects/level35/mantaray/41.webp", "Image")
		mantaray42= loadAsset("/externalassets/objects/level35/mantaray/42.webp", "Image")
		mantaray43= loadAsset("/externalassets/objects/level35/mantaray/43.webp", "Image")
		mantaray44= loadAsset("/externalassets/objects/level35/mantaray/44.webp", "Image")
		mantaray45= loadAsset("/externalassets/objects/level35/mantaray/45.webp", "Image")
		mantaray46= loadAsset("/externalassets/objects/level35/mantaray/46.webp", "Image")
		mantaray47= loadAsset("/externalassets/objects/level35/mantaray/47.webp", "Image")
		mantaray48= loadAsset("/externalassets/objects/level35/mantaray/48.webp", "Image")
		mantaray49= loadAsset("/externalassets/objects/level35/mantaray/49.webp", "Image")
		mantaray50= loadAsset("/externalassets/objects/level35/mantaray/50.webp", "Image")
		mantaray51= loadAsset("/externalassets/objects/level35/mantaray/51.webp", "Image")
		mantaray52= loadAsset("/externalassets/objects/level35/mantaray/52.webp", "Image")
		mantaray53= loadAsset("/externalassets/objects/level35/mantaray/53.webp", "Image")
		mantaray54= loadAsset("/externalassets/objects/level35/mantaray/54.webp", "Image")
		mantaray55= loadAsset("/externalassets/objects/level35/mantaray/55.webp", "Image")
		mantaray56= loadAsset("/externalassets/objects/level35/mantaray/56.webp", "Image")
		mantaray57= loadAsset("/externalassets/objects/level35/mantaray/57.webp", "Image")
		mantaray58= loadAsset("/externalassets/objects/level35/mantaray/58.webp", "Image")
		mantaray59= loadAsset("/externalassets/objects/level35/mantaray/59.webp", "Image")
		mantaray60= loadAsset("/externalassets/objects/level35/mantaray/60.webp", "Image")
		mantaray61= loadAsset("/externalassets/objects/level35/mantaray/61.webp", "Image")
		
			ffish1 = loadAsset("/externalassets/levels/level3/ffish1.webp", "Image")
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
			
		elseif nLevel==36 then
			
			fishs1 = loadAsset("/externalassets/levels/level3/fishs1.webp", "Image")
			fishs2 = loadAsset("/externalassets/levels/level3/fishs2.webp", "Image")
		
		level36bck= loadAsset("/externalassets/levels/level36/level36bck.webp", "Image")
			level36bck2= loadAsset("/externalassets/levels/level36/level36bck2.webp", "Image")
			level36bck3= loadAsset("/externalassets/levels/level36/level36bck3.webp", "Image")
			level36frg= loadAsset("/externalassets/levels/level36/level36frg.webp", "Image")
		
					--objects
					ocel1 = loadAsset("/externalassets/objects/level36/1-ocel.webp", "Image")
					ocel2 = loadAsset("/externalassets/objects/level36/2-ocel.webp", "Image")
					koral = loadAsset("/externalassets/objects/level36/koral.webp", "Image")
					musla = loadAsset("/externalassets/objects/level36/musla.webp", "Image")
					musle_troj = loadAsset("/externalassets/objects/level36/musle_troj.webp", "Image")
					perla_00 = loadAsset("/externalassets/objects/level36/perla_00.webp", "Image")
					perla_01 = loadAsset("/externalassets/objects/level36/perla_00.webp", "Image")
					perla_02 = loadAsset("/externalassets/objects/level36/perla_00.webp", "Image")
					perla_03 = loadAsset("/externalassets/objects/level36/perla_00.webp", "Image")
					zeva_00 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_01 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_02 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_03 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_04 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_05 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_06 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
					zeva_07 = loadAsset("/externalassets/objects/level36/zeva_00.webp", "Image")
		
		elseif nLevel==37 then
		
		if res=="720p" or res=="1080p" or res=="1440p" or res=="4k" then	-- default background and foreground for 1080p and 4k resolution
		
			level37bck= loadAsset("/externalassets/levels/level37/4k/level37_bck1_4k.webp", "Image")
			level37bck2= loadAsset("/externalassets/levels/level37/4k/level37_bck2_4k.webp", "Image")
			level37creature= loadAsset("/externalassets/levels/level37/4k/level37_creature4k.webp", "Image")
			level37frg= loadAsset("/externalassets/levels/level37/4k/level37_frg4k.webp", "Image")
		
		elseif res=="8k" then	-- 8k background and foreground
		
			level37bck= loadAsset("/externalassets/levels/level37/8k/level37_bck1_8k.webp", "Image")
			level37bck2= loadAsset("/externalassets/levels/level37/8k/level37_bck2_8k.webp", "Image")
			level37creature= loadAsset("/externalassets/levels/level37/8k/level37_creature8k.webp", "Image")
			level37frg= loadAsset("/externalassets/levels/level37/8k/level37_frg8k.webp", "Image")
		
		end
			
			--objects
			ocel3 = loadAsset("/externalassets/classic/level37/objects/upscaled/3-ocel.webp", "Image")
			ocel5 = loadAsset("/externalassets/classic/level37/objects/upscaled/5-ocel.webp", "Image")
			ocel14 = loadAsset("/externalassets/classic/level37/objects/upscaled/14-ocel.webp", "Image")
			ocel15 = loadAsset("/externalassets/classic/level37/objects/upscaled/15-ocel.webp", "Image")
			ocel16 = loadAsset("/externalassets/classic/level37/objects/upscaled/16-ocel.webp", "Image")
			
			ocel20 = loadAsset("/externalassets/classic/level37/objects/upscaled/20-ocel.webp", "Image")
			ocel25 = loadAsset("/externalassets/classic/level37/objects/upscaled/14-ocel.webp", "Image")
			
			
			koral0 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral0.webp", "Image")
			koral1 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral1.webp", "Image")
			koral2 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral2.webp", "Image")
			koral3 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral3.webp", "Image")
			koral4 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral4.webp", "Image")
			koral5 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral5.webp", "Image")
			koral6 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral6.webp", "Image")
			koral7 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral7.webp", "Image")
			koral8 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral8.webp", "Image")
			koral9 = loadAsset("/externalassets/classic/level37/objects/upscaled/koral9.webp", "Image")
			
			musle_troj = loadAsset("/externalassets/classic/level37/objects/upscaled/musle_troj.webp", "Image")
			perla_00 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_00.webp", "Image")
			perla_01 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_01.webp", "Image")
			perla_02 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_02.webp", "Image")
			perla_03 = loadAsset("/externalassets/classic/level36/objects/upscaled/perla_03.webp", "Image")
			rybicka00 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka00.webp", "Image")
			rybicka01 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka01.webp", "Image")
			rybicka02 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka02.webp", "Image")
			rybicka03 = loadAsset("/externalassets/classic/level33/objects/upscaled/rybicka03.webp", "Image")
			z_00 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_00.webp", "Image")
			z_01 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_01.webp", "Image")
			z_02 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_02.webp", "Image")
			z_03 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_03.webp", "Image")
			z_04 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_04.webp", "Image")
			z_05 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_05.webp", "Image")
			z_06 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_06.webp", "Image")
			z_07 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_07.webp", "Image")
			z_08 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_08.webp", "Image")
			z_09 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_09.webp", "Image")
			z_10 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_10.webp", "Image")
			z_11 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_11.webp", "Image")
			z_12 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_12.webp", "Image")
			z_13 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_13.webp", "Image")
			z_14 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_14.webp", "Image")
			z_15 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_15.webp", "Image")
			z_16 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_16.webp", "Image")
			z_17 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_17.webp", "Image")
			z_18 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_18.webp", "Image")
			z_19 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_19.webp", "Image")
			z_20 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_20.webp", "Image")
			z_21 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_21.webp", "Image")
			z_22 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_22.webp", "Image")
			z_23 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_23.webp", "Image")
			z_24 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_24.webp", "Image")
			z_25 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_25.webp", "Image")
			z_26 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_26.webp", "Image")
			z_27 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_27.webp", "Image")
			z_28 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_28.webp", "Image")
			z_29 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_29.webp", "Image")
			z_30 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_30.webp", "Image")
			z_31 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_31.webp", "Image")
			z_32 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_32.webp", "Image")
			z_33 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_33.webp", "Image")
			z_34 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_34.webp", "Image")
			z_35 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_35.webp", "Image")
			z_36 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_36.webp", "Image")
			z_37 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_37.webp", "Image")
			z_38 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_38.webp", "Image")
			z_39 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_39.webp", "Image")
			z_40 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_40.webp", "Image")
			z_41 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_41.webp", "Image")
			z_42 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_42.webp", "Image")
			z_43 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_43.webp", "Image")
			z_44 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_44.webp", "Image")
			z_45 = loadAsset("/externalassets/classic/level37/objects/upscaled/z_45.webp", "Image")
			
			
		elseif nLevel==38 then
		
		level38frg= loadAsset("/externalassets/levels/level38/level38frg.webp", "Image")
		level38bck= loadAsset("/externalassets/levels/level38/level38bck.webp", "Image")
		level38bck2= loadAsset("/externalassets/levels/level44/level44bck.webp", "Image")
		
		--screen= loadAsset("/externalassets/objects/level38/screen.webp", "Image")
		--screenlight= loadAsset("/externalassets/objects/level38/screenlight.webp", "Image")
		--computerbase= loadAsset("/externalassets/objects/level38/computerbase.webp", "Image")
		
		--objects
		
				ocel4= loadAsset("/externalassets/classic/level38/objects/upscaled/4-ocel.webp", "Image")
				cola= loadAsset("/externalassets/classic/level38/objects/upscaled/cola.webp", "Image")
				kanystr= loadAsset("/externalassets/classic/level38/objects/upscaled/kanystr.webp", "Image")
				keyboard= loadAsset("/externalassets/classic/level38/objects/upscaled/klavesnice.webp", "Image")
				computerbase = loadAsset("/externalassets/objects/level38/pocitac.webp", "Image")
				screen  = loadAsset("/externalassets/objects/level38/monitor.webp", "Image")
				screenlight= loadAsset("/externalassets/objects/level38/screenlight.webp", "Image")
				roura_st= loadAsset("/externalassets/classic/level38/objects/upscaled/roura_st.webp", "Image")
				roura_st_a= loadAsset("/externalassets/classic/level38/objects/upscaled/roura_st_a.webp", "Image")
				vyvrtka= loadAsset("/externalassets/classic/level38/objects/upscaled/vyvrtka.webp", "Image")
		
		elseif nLevel==39 then
		
		level39frg= loadAsset("/externalassets/levels/level39/level39frg.webp", "Image")
		level39bck= loadAsset("/externalassets/levels/level44/level44bck.webp", "Image")
		
		--objects
		
		drak_m_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/drak_m_00.webp", "Image")
					hvezdy2 = loadAsset("/externalassets/classic/level39/objects/upscaled/hvezdy2.webp", "Image")
					krab_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/krab_00.webp", "Image")
					matrace = loadAsset("/externalassets/classic/level39/objects/upscaled/matrace.webp", "Image")
					mocel = loadAsset("/externalassets/classic/level39/objects/upscaled/mocel.webp", "Image")
					netopejr_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/netopejr_00.webp", "Image")
					perla_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/perla_00.webp", "Image")
					plz_00 = loadAsset("/externalassets/classic/level39/objects/upscaled/plz_00.webp", "Image")
					zlato3 = loadAsset("/externalassets/classic/level39/objects/upscaled/zlato3.webp", "Image")
					
					vocel = loadAsset("/externalassets/classic/level39/objects/upscaled/vocel.webp", "Image")
		
		elseif nLevel>39  	then loadlevelassets40()
		--elseif nLevel>39 and nLevel<60 	then loadlevelassetsclassic40()
		--elseif nLevel>59 				then loadlevelassetsclassic60()
		end

end



function loadwhale()
--whale animation
	
	whalespeed = 32 
	--whalepositionx=-500 
	--whalepositiony=500
	whaleframe=0 
	whaledirection="right" 
	whalescalex=0.6
	whalescaley=0.6
	level1whale1 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale2 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale3 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale4 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale5 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale6 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale7 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale8 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale9 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale10 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale11 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale12 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale13 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale14 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale15 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale16 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale17 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale18 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale19 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale20 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale21 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale22 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale23 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale24 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale25 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale26 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale27 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale28 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale29 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale30 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale31 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	level1whale32 = loadAsset("/externalassets/levels/level1/whale/whalex.webp", "Image")
	--]]
	--[[
	level1whale1 = loadAsset("/externalassets/levels/level1/whale/whale1.webp", "Image")
	level1whale2 = loadAsset("/externalassets/levels/level1/whale/whale2.webp", "Image")
	level1whale3 = loadAsset("/externalassets/levels/level1/whale/whale3.webp", "Image")
	level1whale4 = loadAsset("/externalassets/levels/level1/whale/whale4.webp", "Image")
	level1whale5 = loadAsset("/externalassets/levels/level1/whale/whale5.webp", "Image")
	level1whale6 = loadAsset("/externalassets/levels/level1/whale/whale6.webp", "Image")
	level1whale7 = loadAsset("/externalassets/levels/level1/whale/whale7.webp", "Image")
	level1whale8 = loadAsset("/externalassets/levels/level1/whale/whale8.webp", "Image")
	level1whale9 = loadAsset("/externalassets/levels/level1/whale/whale9.webp", "Image")
	level1whale10 = loadAsset("/externalassets/levels/level1/whale/whale10.webp", "Image")
	level1whale11 = loadAsset("/externalassets/levels/level1/whale/whale11.webp", "Image")
	level1whale12 = loadAsset("/externalassets/levels/level1/whale/whale12.webp", "Image")
	level1whale13 = loadAsset("/externalassets/levels/level1/whale/whale13.webp", "Image")
	level1whale14 = loadAsset("/externalassets/levels/level1/whale/whale14.webp", "Image")
	level1whale15 = loadAsset("/externalassets/levels/level1/whale/whale15.webp", "Image")
	level1whale16 = loadAsset("/externalassets/levels/level1/whale/whale16.webp", "Image")
	level1whale17 = loadAsset("/externalassets/levels/level1/whale/whale17.webp", "Image")
	level1whale18 = loadAsset("/externalassets/levels/level1/whale/whale18.webp", "Image")
	level1whale19 = loadAsset("/externalassets/levels/level1/whale/whale19.webp", "Image")
	level1whale20 = loadAsset("/externalassets/levels/level1/whale/whale20.webp", "Image")
	level1whale21 = loadAsset("/externalassets/levels/level1/whale/whale21.webp", "Image")
	level1whale22 = loadAsset("/externalassets/levels/level1/whale/whale22.webp", "Image")
	level1whale23 = loadAsset("/externalassets/levels/level1/whale/whale23.webp", "Image")
	level1whale24 = loadAsset("/externalassets/levels/level1/whale/whale24.webp", "Image")
	level1whale25 = loadAsset("/externalassets/levels/level1/whale/whale25.webp", "Image")
	level1whale26 = loadAsset("/externalassets/levels/level1/whale/whale26.webp", "Image")
	level1whale27 = loadAsset("/externalassets/levels/level1/whale/whale27.webp", "Image")
	level1whale28 = loadAsset("/externalassets/levels/level1/whale/whale28.webp", "Image")
	level1whale29 = loadAsset("/externalassets/levels/level1/whale/whale29.webp", "Image")
	level1whale30 = loadAsset("/externalassets/levels/level1/whale/whale30.webp", "Image")
	level1whale31 = loadAsset("/externalassets/levels/level1/whale/whale31.webp", "Image")
	level1whale32 = loadAsset("/externalassets/levels/level1/whale/whale32.webp", "Image")
	--]]
end

