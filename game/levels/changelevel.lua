

function changelevel()
--love.mouse.setCursor()
if mousestate==nil or mousestate==true then
	mousestate = not love.mouse.isVisible()	-- hide mouse pointer
	love.mouse.setVisible(mousestate)
end

if caustics==true and not (skin=="retro") then shader2=true end

if skin=="retro" then turnshadersoff() end	-- prevent retro mode from not rendering properly

		shaders = require "game/states/GameShaders/shaders"					-- Shadertoy shaders
		--shaders = require "shaders"					-- Shadertoy shaders
		selectshadertoy("shaders/wadingcaustic.glsl")
		--selectshadertoy("shaders/underwater.glsl")
		--selectshadertoy("shaders/waterdistortion.glsl")
		
		

if shader1type=="bluenoise" then caustics=true shader2=true end	-- if bluenoise is enabled shader2 is enabled otherwise objects will not render

blockscolor={0,0.5,1}
edgescolor={0,0.8,0}
	if nLevel==0 then
	
	blockscolor={0,0.8,0}
	edgescolor={0,0.8,0}

		threeDcellx=26.3			-- 3d edges of the map
		threeDcelly=14.35
		threeDcellz=-14.5
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.level-1')
			
		
		
	
	elseif nLevel==1 then
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.level-1')
			pipefalled=false
			
			
			
			
			if caustics==true then if caustics==true then shader2=true end end
			
			
	
	
		threeDcellx=26.3			-- 3d edges of the map
		threeDcelly=14.35
		threeDcellz=-14.5

		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
				
		elseif nLevel==2 then
			package.loaded['game.levels.level-2'] = nil
			level = require ('game.levels.level-2')
			
				require ("game/states/BriefcaseMessage/BriefcaseMessage")
		
			
			
			if caustics==true then shader2=true end

		
		threeDcellx=36.7	-- 3d edges of the map
		threeDcelly=16.3
		threeDcellz=-17
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
			loadwallborder()
		
		elseif nLevel==3 then
			
			package.loaded['game.levels.level-3'] = nil
			level = require ('game.levels.level-3')	
			
				
			--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=100 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			
			
			if caustics==true then shader2=true end
			
	

	
		
		threeDcellx=34.8	-- 3d edges of the map
		threeDcelly=19.3
		threeDcellz=-18.5

		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
		
	
		
	
		elseif nLevel==4 then
			
			package.loaded['game.levels.level-4'] = nil
			level = require ('game.levels.level-4')
					aproachcreature=false
				
				if caustics==true then shader2=true end
				
			
				if assets==2 then  end

		
		
		threeDcellx=37.1	-- 3d edges of the map
		threeDcelly=18.4
		threeDcellz=-18
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
				loadwallborder()
				
			
			
			elseif nLevel==5 then
			
			package.loaded['game.levels.level-5'] = nil
			level = require ('game.levels.level-5')
	
			
			if caustics==true then shader2=true end

		threeDcellx=30	-- 3d edges of the map
		threeDcelly=16.35
		threeDcellz=-16
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
			
			loadwallborder()

	
			
				elseif nLevel==6 then
			
			package.loaded['game.levels.level-6'] = nil
			level = require ('game.levels.level-6')
			
		
      
      
		threeDcellx=33	-- 3d edges of the map
		threeDcelly=18.35
		threeDcellz=-18
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
			
				elseif nLevel==7 then
			
			package.loaded['game.levels.level-7'] = nil
			level = require ('game.levels.level-7')
			
				--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			--dolphin
			dolphindirection="right"
			dolphinspeed="64"
			dolphinframe=0
			dolphinscalex=0.4
			dolphinscaley=0.4
			dolphinpositionx=0
			dolphinpositiony=150
   
      
		threeDcellx=27.5	-- 3d edges of the map
		threeDcelly=15
		threeDcellz=-14
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
		
	
				elseif nLevel==8 then
			
			package.loaded['game.levels.level-8'] = nil
			level = require ('game.levels.level-8')	
			
			
			
		
		threeDcellx=22.5	-- 3d edges of the map
		threeDcelly=12.4
		threeDcellz=-11.4
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
		
			elseif nLevel==9 then
			
			package.loaded['game.levels.level-9'] = nil
			level = require ('game.levels.level-9')
			

					
		threeDcellx=18.8	-- 3d edges of the map
		threeDcelly=10.1
		threeDcellz=-9.4
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
		
		loadwallborder()
		
	
			elseif nLevel==10 then
			
			package.loaded['game.levels.level-10'] = nil
			level = require ('game.levels.level-10')	
			
			
			if caustics==true then shader2=true end
			
		threeDcellx=33	-- 3d edges of the map
		threeDcelly=18.2
		threeDcellz=-18
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
			
			loadwallborder()
	
			elseif nLevel==11 then
			
			package.loaded['game.levels.level-11'] = nil
			level = require ('game.levels.level-11')
		
			

		threeDcellx=32	-- 3d edges of the map
		threeDcelly=17.5
		threeDcellz=-17
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
			
			loadwallborder()

			elseif nLevel==12 then
			
			package.loaded['game.levels.level-12'] = nil
			level = require ('game.levels.level-12')
			
			if caustics==true then shader2=true end
			
		threeDcellx=20.5	-- 3d edges of the map
		threeDcelly=11.2
		threeDcellz=-10
		
		threeDcellwidth=1
		threeDcellheight=1
		threeDcelldepth=1
			
			if musicison==true then love.audio.stop() music:stop() end
			
	
	
			elseif nLevel==13 then
			
			package.loaded['game.levels.level-13'] = nil
			level = require ('game.levels.level-13')
			
			
       
			elseif nLevel==14 then
			
			package.loaded['game.levels.level-14'] = nil
			level = require ('game.levels.level-14')


			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			elseif nLevel==15 then
			
			package.loaded['game.levels.level-15'] = nil
			level = require ('game.levels.level-15')
		
			--shark
			sharkdirection="right"
			sharkspeed="64"
			sharkframe=0
			sharkscalex=0.5
			sharkscaley=0.5
			sharkpositionx=0
			sharkpositiony=250
		

			
			elseif nLevel==16 then
			
			package.loaded['game.levels.level-16'] = nil
			level = require ('game.levels.level-16')
	
			
					--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.5
			ffish1scaley=0.5
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
	
			elseif nLevel==17 then
			
			package.loaded['game.levels.level-17'] = nil
			level = require ('game.levels.level-17')
			
				
			elseif nLevel==18 then
			
			package.loaded['game.levels.level-18'] = nil
			level = require ('game.levels.level-18')
	
	
			elseif nLevel==19 then
			
			package.loaded['game.levels.level-19'] = nil
			level = require ('game.levels.level-19')
		
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=670 
			fishs2positionx=100
			fishs2positiony=800 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			elseif nLevel==20 then
			
			package.loaded['game.levels.level-20'] = nil
			level = require ('game.levels.level-20')
	
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
			
			
			
			if caustics==true then shader2=true end
			


			elseif nLevel==21 then
			package.loaded['game.levels.level-21'] = nil
			level = require ('game.levels.level-21')
	
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
	
			
			if caustics==true then shader2=true end
	
			
		
			elseif nLevel==22 then
			package.loaded['game.levels.level-22'] = nil
			level = require ('game.levels.level-22')	
	
	
			selectshadertoy("shaders/neonpentagram.glsl")


			elseif nLevel==23 then
			package.loaded['game.levels.level-23'] = nil
			level = require ('game.levels.level-23')	


			elseif nLevel==24 then
			package.loaded['game.levels.level-24'] = nil
			level = require ('game.levels.level-24')
		
			selectshadertoy("shaders/underwater.glsl")
	
			
			
		
			elseif nLevel==25 then
			package.loaded['game.levels.level-25'] = nil
			level = require ('game.levels.level-25')	
	
			
			
	
			elseif nLevel==26 then
			package.loaded['game.levels.level-26'] = nil
			level = require ('game.levels.level-26')
		
			
			
		
			elseif nLevel==27 then
			package.loaded['game.levels.level-27'] = nil
			level = require ('game.levels.level-27')
		
			
			
	
			elseif nLevel==28 then
			package.loaded['game.levels.level-28'] = nil
			level = require ('game.levels.level-28')
		
			
			
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		
			elseif nLevel==29 then
			package.loaded['game.levels.level-29'] = nil
			level = require ('game.levels.level-29')
		
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=1
			fishs1scaley=1
			fishs2scalex=1
			fishs2scaley=1

			
		
			
			--if caustics==true then shader2=true end
			

			
			elseif nLevel==30 then
			package.loaded['game.levels.level-30'] = nil
			level = require ('game.levels.level-30')
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.7
			fishs1scaley=0.7
			fishs2scalex=0.7
			fishs2scaley=0.7

		

			
			if caustics==true then shader2=true end
			
			
			elseif nLevel==31 then
			package.loaded['game.levels.level-31'] = nil
			level = require ('game.levels.level-31')
			
			
			
				--ffish1
			ffish1speed = 32 
			ffish1positionx=100
			ffish1positiony=200 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.4
			ffish1scaley=0.4
			
							--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=300 
			fishs2positionx=100
			fishs2positiony=400 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=1
			fishs1scaley=1
			fishs2scalex=1
			fishs2scaley=1

			
				--background
		
			
			
			if caustics==true then shader2=true end
		
			
			elseif nLevel==32 then
			package.loaded['game.levels.level-32'] = nil
			level = require ('game.levels.level-32')
			
			
			--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=300 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=400 
			fishs2positionx=300
			fishs2positiony=800 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
			
			
			
			
			elseif nLevel==33 then
			package.loaded['game.levels.level-33'] = nil
			level = require ('game.levels.level-33')
			
			
			
			--fishs
			
			fishs1speed = 20
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=400 
			fishs2positionx=300
			fishs2positiony=800 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
				--whale sounds
			whalecalllow = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallLow(echo)9.ogg","static" )
			whalecallmid = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallMid(echo)8.ogg","static" )

					--vanish = love.audio.newSource("externalassets/music/hectavex/vanish.ogg","stream" )
			if musicison==true then love.audio.stop() music:stop() --love.audio.play( vanish )
			elseif musicison==false then love.audio.stop() music:stop()
			 end

			
			
	
			
			
			elseif nLevel==34 then
			package.loaded['game.levels.level-34'] = nil
			level = require ('game.levels.level-34')
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
				--whale sounds
			whalecalllow = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallLow(echo)9.ogg","static" )
			whalecallmid = love.audio.newSource( "/externalassets/sounds/whale/WhaleCallMid(echo)8.ogg","static" )
			
			loadwhale()
			whalescalex=0.3
			whalescaley=0.3
			whalepositionx=0
			--whalepositiony=300
			
			
			
			
			
			
		
			elseif nLevel==35 then
			package.loaded['game.levels.level-35'] = nil
			level = require ('game.levels.level-35')
			
						--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=370 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
			--mantaray
			mantaraydirection="right"
			mantarayspeed="64"
			mantarayframe=0
			mantarayscalex=0.8
			mantarayscaley=0.8
			mantaraypositionx=0
			mantaraypositiony=250

			
			
			
			elseif nLevel==36 then
			package.loaded['game.levels.level-36'] = nil
			level = require ('game.levels.level-36')
			
				--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishpositionx=0
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
			
					
			
			

			
			
			elseif nLevel==37 then
			package.loaded['game.levels.level-37'] = nil
			level = require ('game.levels.level-37')
			
	selectshadertoy("shaders/underwater.glsl")

			
			
			
			
			elseif nLevel==38 then
			package.loaded['game.levels.level-38'] = nil
			level = require ('game.levels.level-38')
			
			--selectshadertoy("shaders/underwater.glsl")
			
			
			
			elseif nLevel==39 then
			package.loaded['game.levels.level-39'] = nil
			level = require ('game.levels.level-39')
	
			
			shader2=false
			
			
			elseif nLevel==40 then
			package.loaded['game.levels.level-40'] = nil
			level = require ('game.levels.level-40')
			
			
			selectshadertoy("shaders/underwater.glsl")
	
			
			elseif nLevel==41 then
			package.loaded['game.levels.level-41'] = nil
			level = require ('game.levels.level-41')
			
						
			selectshadertoy("shaders/underwater.glsl")
			
			
			elseif nLevel==42 then
			package.loaded['game.levels.level-42'] = nil
			level = require ('game.levels.level-42')
			
		selectshadertoy("shaders/underwater.glsl")
			
			elseif nLevel==43 then
			package.loaded['game.levels.level-43'] = nil
			level = require ('game.levels.level-43')

			selectshadertoy("shaders/underwater.glsl")
			
		
			
			elseif nLevel==44 then
			package.loaded['game.levels.level-44'] = nil
			level = require ('game.levels.level-44')
			
			selectshadertoy("shaders/underwater.glsl")
			
		
			
			elseif nLevel==45 then
			package.loaded['game.levels.level-45'] = nil
			level = require ('game.levels.level-45')
			
			
			
			
		
			
			elseif nLevel==46 then
			package.loaded['game.levels.level-46'] = nil
			level = require ('game.levels.level-46')
			
			
			
		
			elseif nLevel==47 then
			package.loaded['game.levels.level-47'] = nil
			level = require ('game.levels.level-47')
					
			
			shader2=false
	
			
			elseif nLevel==48 then
			package.loaded['game.levels.level-48'] = nil
			level = require ('game.levels.level-48')

		
			
			elseif nLevel==49 then
			package.loaded['game.levels.level-49'] = nil
			level = require ('game.levels.level-49')
			
			
			shader2=false
			
			--nLevel=50
			
			elseif nLevel==50 then
			package.loaded['game.levels.level-50'] = nil
			level = require ('game.levels.level-50')

			
			
			
			
			
			elseif nLevel==51 then
			package.loaded['game.levels.level-51'] = nil
			level = require ('game.levels.level-51')
			
			elseif nLevel==52 then
			package.loaded['game.levels.level-52'] = nil
			level = require ('game.levels.level-52')
		
			

		--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		if res=="1440p" then
			ffish1positiony=700
			fishs1positiony=700 
			fishs2positiony=700 
		end
			elseif nLevel==53 then
			package.loaded['game.levels.level-53'] = nil
			level = require ('game.levels.level-53')
			
			enginepushed=false
			
	
			
			shader2=false
		
			elseif nLevel==54 then
			package.loaded['game.levels.level-54'] = nil
			level = require ('game.levels.level-54')
			
			--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		if res=="1440p" then
			ffish1positiony=700
			fishs1positiony=700 
			fishs2positiony=700 
		end
			
			enginepushed=false engineononce=false aproachengineonce=false

		
			
			elseif nLevel==55 then
			package.loaded['game.levels.level-55'] = nil
			level = require ('game.levels.level-55')
		
			
			shader2=false
			
			
			elseif nLevel==56 then
			package.loaded['game.levels.level-56'] = nil
			level = require ('game.levels.level-56')
	
			--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		if res=="1440p" then
			ffish1positiony=700
			fishs1positiony=700 
			fishs2positiony=700 
		end
			
			
			elseif nLevel==57 then
			package.loaded['game.levels.level-57'] = nil
			level = require ('game.levels.level-57')
			
	--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		if res=="1440p" then
			ffish1positiony=700
			fishs1positiony=700 
			fishs2positiony=700 
		end

		
			
			elseif nLevel==58 then
			package.loaded['game.levels.level-58'] = nil
			level = require ('game.levels.level-58')

	--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.3
			ffish1scaley=0.3
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6
		if res=="1440p" then
			ffish1positiony=700
			fishs1positiony=700 
			fishs2positiony=700 
		end
			
			
			elseif nLevel==59 then
			package.loaded['game.levels.level-59'] = nil
			level = require ('game.levels.level-59')
		
			--sounds
			gemsound = love.audio.newSource( "externalassets/sounds/level60/sfx100v2_glass_04.ogg","static" )
			gemsound2 = love.audio.newSource( "externalassets/sounds/level60/sfx100v2_glass_06.ogg","static" )
			
			
	
			
			
		
			
			elseif nLevel==60 then
			package.loaded['game.levels.level-60'] = nil
			level = require ('game.levels.level-60')
		
			
			shader2=false
			
			elseif nLevel==61 then
			package.loaded['game.levels.level-61'] = nil
			level = require ('game.levels.level-61')
			
			
			

			
			
			
			
			
			elseif nLevel==62 then
			package.loaded['game.levels.level-62'] = nil
			level = require ('game.levels.level-62')

			
			
			shader2=false
			
		
			
			elseif nLevel==63 then
			package.loaded['game.levels.level-63'] = nil
			level = require ('game.levels.level-63')

			
			
			shader2=false
			
			
			
			elseif nLevel==64 then
			package.loaded['game.levels.level-64'] = nil
			level = require ('game.levels.level-64')
			

			
			
			shader2=false
			
			
			elseif nLevel==65 then
			package.loaded['game.levels.level-65'] = nil
			level = require ('game.levels.level-65')

			
			
			shader2=false
		
			
			elseif nLevel==66 then
			package.loaded['game.levels.level-66'] = nil
			level = require ('game.levels.level-66')

		expx=0.5
		expy=0.5
						--set 1 of moonshine effects		"Scanlines + Vignette"
					effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("crt","scanlines","filmgrain")
						effect.crt.distortionFactor = {1.04, 1.065}
						
						effect.vignette.radius=1.5
						effect.vignette.opacity=2
						--effect.dmg.palette=3
						
						effect.scanlines.opacity=0.1
						effect.glow.min_luma = 1
						effect.filmgrain.size=1
						effect.filmgrain.opacity=1
		
			
			--shader1=true
			shader2=false
	
			elseif nLevel==67 then
			package.loaded['game.levels.level-67'] = nil
			level = require ('game.levels.level-67')
			expx=0.5
			expy=0.5
			
			shader2=false
			
			elseif nLevel==68 then
			package.loaded['game.levels.level-68'] = nil
			level = require ('game.levels.level-68')
			
			expx=1
			expy=1
			
			
			shader2=false
			
		
			
			elseif nLevel==69 then
			package.loaded['game.levels.level-69'] = nil
			level = require ('game.levels.level-69')
			expx=0.5
			expy=0.5
			
			
			shader2=false
			
			
			
			elseif nLevel==70 then
			package.loaded['game.levels.level-70'] = nil
			level = require ('game.levels.level-70')

						
			
			shader2=false
			
			
			elseif nLevel==71 then
			package.loaded['game.levels.level-71'] = nil
			level = require ('game.levels.level-71')

			shader2=false
			
			
			elseif nLevel==72 then
			package.loaded['game.levels.level-72'] = nil
			level = require ('game.levels.level-72')

			shader2=false
			
			
			elseif nLevel==73 then
			package.loaded['game.levels.level-73'] = nil
			level = require ('game.levels.level-73')

			shader2=false
			
		
			
			elseif nLevel==74 then
			package.loaded['game.levels.level-74'] = nil
			level = require ('game.levels.level-74')
			if skin=="remake" then expx=0.5 expy=0.5 end
			
			
			
		
			elseif nLevel==75 then
			package.loaded['game.levels.level-75'] = nil
			level = require ('game.levels.level-75')

						
			elseif nLevel==76 then
			package.loaded['game.levels.level-76'] = nil
			level = require ('game.levels.level-76')

					
			elseif nLevel==77 then
			package.loaded['game.levels.level-77'] = nil
			level = require ('game.levels.level-77')


			shader2=false
		
	
			
			elseif nLevel==78 then
			package.loaded['game.levels.level-78'] = nil
			level = require ('game.levels.level-78')

			
			shader2=false
		
		
			
			elseif nLevel==79 then
			package.loaded['game.levels.level-79'] = nil
			level = require ('game.levels.level-79')
		
			
			shader2=false
			
			elseif nLevel==80 then
			package.loaded['game.levels.level-80'] = nil
			level = require ('game.levels.level-80')
		
			
			
			shader2=false
			expx=1
			expy=1
			
			elseif nLevel==81 then
			package.loaded['game.levels.level-81'] = nil
			level = require ('game.levels.level-81')
		
			
			
			
			expx=0.5
			expy=0.5
			-- Screensavers

		
			elseif nLevel==101 then
			package.loaded['game.levels.screensaver.level-101'] = nil
			level = require ('game.levels.screensaver.level-101')
			
			talkies=false
			
						--ffish1
			ffish1speed = 32 
			ffish1positionx=300
			ffish1positiony=500 
			ffish1frame=0 
			ffish1direction="right" 
			ffish1scalex=0.6
			ffish1scaley=0.6
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
			
			if autoload then
				if musicison==true then love.audio.stop() music:stop()
			elseif musicison==false then love.audio.stop() music:stop()
			 end
			end
			shader1=false
			
			
	
			elseif nLevel==102 then
			package.loaded['game.levels.screensaver.level-102'] = nil
			level = require ('game.levels.screensaver.level-102')
			
			shader1=false
			
			
			talkies=false
			
			--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=100
			fishs1positiony=370 
			fishs2positionx=100
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
		
			
			elseif nLevel==103 then
			package.loaded['game.levels.screensaver.level-103'] = nil
			level = require ('game.levels.screensaver.level-103')
			
			shader1=true
			shader2=false
			
			talkies=false
			
				--fishs
			
			fishs1speed = 20 
			fishs2speed = 20 
			fishs1positionx=500
			fishs1positiony=500 
			fishs2positionx=300
			fishs2positiony=500 
			fishs1direction="right"
			fishs2direction="right"
			fishs1scalex=0.6
			fishs1scaley=0.6
			fishs2scalex=0.6
			fishs2scaley=0.6

			
									

			loadwhale()
			whalescalex=1
			whalescaley=1
			whalepositionx=0
			whalepositiony=200

			
	elseif nLevel==110 then
	
			blockscolor={0,0.8,0}
			edgescolor={0,0.8,0}

		
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.extras.level-110')
			palette=1
			
	elseif nLevel==111 then
			package.loaded['game.levels.level-1'] = nil
			level = require ('game.levels.extras.level-111')
			palette=1
			
	end
		   palettemustbeinteger()	-- prevent palette from not being assigned
		    
			timer=0
			
			
		
		--load everything
		
		loadvoices()				-- load voices		
			if musictype=="new" then loadlevelmusic() 			-- load music
		elseif musictype=="classic" then loadlevelmusicclassic()
		end
		loadlevelsounds()			-- load sounds
		loadlevelvariables()

		inputtime=0
		scalepl=1
			if skin=="remake" or skin=="retro" then
				loadlevelassets()			-- load assets
				--loadlevelmusic()			-- load music
				assignfrgbck()				-- Assign layers
		elseif skin=="classic" then	
				loadlevelassetsclassic()	-- load classic assets
				--loadlevelmusicclassic()		-- load music
				assignfrgbckclassic()		-- Assign layers
				
				if skinupscaled==false then
					assignsizenormal()
				elseif skinupscaled==true then
					assignsizeupscaled()
				end
		end

		setuppaletteforshaderfilters()
		
		loadwhale()					-- load whale animation
		loadsubtitles()				-- load subtitles
		loadleveltitle()			-- load level title
		steps=0						-- steps to count for scores
		
		boresentencestriggered=false			-- restore defaults for bore sentences
		boresentencesinterrupted=false


		
		if android==true then shader2=false touchinterfaceison=true end
			love.graphics.clear()
			pb:load (level)
			
			if not (repeatleveltriggered==true) then
				pb:loadlevel ()		-- load the last saved position of agents and blocks in case that data is saved
				

			end
			repeatleveltriggered=false
			
			--pb:drawMap ()
   
			if debugmode==true then pb:drawBackgroundGrid () end

end

function palettemustbeinteger()
 --prevent palette from not being assigned
		    if palette==1 or palette==2 or palette==3 or palette==4 or palette==5 or palette==6 or palette==7 then
		    else 
				if skin=="retro" then palette=2			-- assign a palette for the retro skin
				elseif skin=="remake" or skin=="classic" then palette=1 -- assign palette 1 for remake and classic skin
				 end
		    end
end


