local level = {}


level.name = 'level-45'

-- first cell is map[1][1], top left corner
level.map = 
{	
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,3,3,3,3,0,0,0,0,0,3,3,3,3,3,1,1,3,0,0,0},
{1,1,3,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,1,3,0,0,0,0,3,0,0,0,0,0,0,3,3,3,3,3,3,3,1,1,3,0,0,0},
{1,3,3,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{3,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,6,6,6},
{1,3,3,0,0,0,0,3,3,3,3,3,0,0,0,0,0,0,0,0,0,6,6,6,6,6,6},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6,6,6,6},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,3,6,6,6},
{1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,3,6,6,6},
{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{

{name = 'truhla',
	heavy = false,
	x = 3,
	y = 16,
	form = 
		{
			{1,1},
			{1,1},
		},
	},
	
	{name = 'papoucha',
	heavy = false,
	x = 9,
	y = 16,
	form = 
		{
			{1,1,0},
			{1,1,1},
		},
	},
	
	{name = 'lampa',
	heavy = false,
	x = 17,
	y = 3,
	form = 
		{
			{1,1,1,1,1},
			{1,0,0,0,0},
		},
	},
	
	
	{name = 'chobotnice',
	heavy = false,
	x = 4,
	y = 6,
	form = 
		{
			{1,0,0,0,1},
			{1,1,1,1,1},
		},
	},
	
	{name = 'lebzna',
	heavy = false,
	x = 14,
	y = 10,
	form = 
		{
			{1,1},
			{0,1},
		},
	},
	
	{name = 'trubka1',
	heavy = true,
	x = 15,
	y = 15,
	form = 
		{
			{1},
			{1},
		},
	},
	
	{name = 'trubka2',
	heavy = true,
	x = 11,
	y = 8,
	form = 
		{
			{1,1,1,1,1,1,1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 18,
	y = 14,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 17,
	y = 15,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
