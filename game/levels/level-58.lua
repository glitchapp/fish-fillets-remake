local level = {}


level.name = 'level-58'

-- first cell is map[1][1], top left corner
level.map = 
{	
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,3,3,0,0,0,3,3,3,0,0,0,3,3,3,0,0,0,3,3,3,0,0,0,3,3,3,0,0,0,3,3,3,0,0,0,3,3,3,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
{3,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,0,0,3,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,3,3,3,3,3,3,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,3,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3},
{3,0,0,0,0,0,3,3,0,0,0,0,3,3,3,3,3,3,3,3,3,3,3,3,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,3},
{6,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{6,6,6,6,6,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{6,6,6,6,6,6,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{6,6,6,6,6,6,3,3,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,6,6,6,6,6,3,3,0,0,0,0,0,0,0,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},
{3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	{name = 'pohon',
	heavy = false,
	x = 17,
	y = 25,
	form = 
		{
			{1,1,1,1,0},
			{1,1,1,1,1},
			{1,1,1,1,1},
			{1,1,1,1,0},
		},
	},
	{name = 'podstavec',
	heavy = false,
	x = 14,
	y = 29,
	form = 
		{
			{0,0,0,1,1,1,0,0},
			{1,1,1,1,1,1,1,1},
			{1,1,1,1,1,1,1,1},
		},
	},
	{name = 'hadice',
	heavy = false,
	x = 15,
	y = 26,
	form = 
		{
			{1,1},
			{1,0},
			{1,0},
			{1,0},
		},
	},
	{name = 'rura',
	heavy = false,
	x = 22,
	y = 23,
	form = 
		{
			{0,0,1,1,1},
			{0,0,1,0,0},
			{0,0,1,0,0},
			{1,1,1,0,0},
			{0,0,1,0,0},
		},
	},
	{name = 'cola',
	heavy = false,
	x = 24,
	y = 28,
	form = 
		{
			{1,1,1},
			{1,1,1},
			{1,1,1},
			{1,1,1},
		},
	},
	{name = 'ufo',
	heavy = false,
	x = 7,
	y = 4,
	form = 
		{
			{0,1,1,1,0},
			{1,1,1,1,1},
			{0,0,1,0,0},
			{0,0,1,0,0},
			{0,1,1,1,0},
		},
	},
	{name = 'ocel7',
	heavy = true,
	x = 3,
	y = 1,
	form = 
		{
			{1,1,1},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,1},
		},
	},
	{name = 'ocel8',
	heavy = true,
	x = 21,
	y = 1,
	form = 
		{
			{1,1,1},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
		},
	},
	{name = 'draty',
	heavy = false,
	x = 22,
	y = 30,
	form = 
		{
			{1,0},
			{1,1},
		},
	},
	{name = 'ufon',
	heavy = false,
	x = 34,
	y = 4,
	form = 
		{
			{1,1},
			{1,1},
			{1,1},
			{1,1},
			{1,1},
		},
	},
	{name = 'ocel11',
	heavy = true,
	x = 8,
	y = 30,
	form = 
		{
			{0,0,0,1,0,0,0,0},
			{0,0,0,1,0,0,0,0},
			{0,0,0,1,0,0,0,0},
			{1,1,1,1,1,1,1,1},
			{1,1,1,0,0,0,0,1},
		},
	},

	{name = 'ocel12',
	heavy = true,
	x = 28,
	y = 8,
	form = 
		{
			{0,0,0,1,1,1},
			{0,0,0,0,1,0},
			{0,0,0,0,1,0},
			{0,0,0,1,1,0},
			{0,0,0,1,0,0},
			{0,0,1,1,0,0},
			{0,0,1,0,0,0},
			{0,1,1,0,0,0},
			{0,1,0,0,0,0},
			{1,1,0,0,0,0},
		},
	},
	{name = 'kamna',
	heavy = false,
	x = 34,
	y = 34,
	form = 
		{
			{1,0,0,0,0,1,1},
			{1,1,1,1,1,1,1},
		},
	},
	{name = 'volant',
	heavy = false,
	x = 36,
	y = 36,
	form = 
		{
			{1,1},
			{0,1},
		},
	},
	{name = 'ocel15',
	heavy = true,
	x = 15,
	y = 1,
	form = 
		{
			{1,1,1},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{0,1,0},
			{1,1,0},
		},
	},
	{name = 'ocel16',
	heavy = true,
	x = 28,
	y = 23,
	form = 
		{
			{1},
			{1},
			{1},
		},
	},
	{name = 'plutonium1',
	heavy = false,
	x = 28,
	y = 26,
	form = 
		{
			{1},
		},
	},
	{name = 'plutonium4',
	heavy = false,
	x = 34,
	y = 33,
	form = 
		{
			{1,1,1,1},
		},
	},
	{name = 'matka_a',
	heavy = false,
	x = 6,
	y = 37,
	form = 
		{
			{1},
		},
	},
	{name = 'matka_a',
	heavy = false,
	x = 29,
	y = 26,
	form = 
		{
			{1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 33,
	y = 27,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 15,
	y = 13,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
