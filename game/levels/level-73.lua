local level = {}


level.name = 'level-73'

-- first cell is map[1][1], top left corner
level.map = 
{	
{3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,6,6,6,6,6,6,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,3,3,3,3,3,3,3,3,0,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,3,3,3,3,3,3,3,3,0,0,3,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,3,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,3,0,0,0,0,0,3,0,6,6,6},
{3,0,0,0,0,0,3,3,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,3,3,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,6,6},
{3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	{name = 'vocel',
	heavy = true,
	x = 8,
	y = 17,
	form = 
		{
			{1,1,1,1,1,1},
		},
	},
	
	{name = 'vocel',
	heavy = true,
	x = 26,
	y = 17,
	form = 
		{
			{1,1,1,1,1,1},
		},
	},
	
	{name = 'vocel',
	heavy = true,
	x = 44,
	y = 17,
	form = 
		{
			{1,1,1,1,1,1},
		},
	},
	
	{name = '1',
	heavy = false,
	x = 4,
	y = 4,
	form = 
		{
			{0,0,1,0,0,0,0,0,0,0,0,1,0},
		},
	},
	
	{name = '1',
	heavy = false,
	x = 4,
	y = 6,
	form = 
		{
			{0,0,1,0,0,0,0,0,0,0,1,0,0},
		},
	},
	
	{name = '1',
	heavy = false,
	x = 4,
	y = 10,
	form = 
		{
			{0,0,0,0,1,0,0,0,1,0,0,0,0},
		},
	},
		
	{name = '1',
	heavy = false,
	x = 4,
	y = 12,
	form = 
		{
			{0,0,0,0,0,1,0,1,0,0,0,0,0},
		},
	},
	
	{name = 'tyc',
	heavy = false,
	x = 10,
	y = 1,
	form = 
		{
			{0},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'tyc',
	heavy = false,
	x = 28,
	y = 1,
	form = 
		{
			
			{0},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'tyc',
	heavy = false,
	x = 47,
	y = 1,
	form = 
		{
			
			{0},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'nejvyse',
	heavy = false,
	x = 4,
	y = 4,
	form = 
		{
			
			{1,0,0,0,0,0,0,0,0,0,0,0,1},
			{0,1,1,1,1,1,0,1,1,1,1,1,0},
		},
	},
	
	{name = 'nahore',
	heavy = false,
	x = 4,
	y = 6,
	form = 
		{
			
			{1,1,0,0,0,0,0,0,0,0,0,1,1},
			{0,0,1,1,1,1,0,1,1,1,1,0,0},
		},
	},
	
	{name = 'uprostred',
	heavy = false,
	x = 4,
	y = 8,
	form = 
		{
			
			{1,1,1,0,0,0,0,0,0,0,1,1,1},
			{0,0,0,1,1,1,0,1,1,1,0,0,0},
		},
	},
	
	{name = 'dole',
	heavy = false,
	x = 4,
	y = 10,
	form = 
		{
			
			{1,1,1,1,0,0,0,0,0,1,1,1,1},
			{0,0,0,0,1,1,0,1,1,0,0,0,0},
		},
	},
	
	{name = 'nejnize',
	heavy = false,
	x = 4,
	y = 12,
	form = 
		{
			
			{1,1,1,1,1,0,0,0,1,1,1,1,1},
			{0,0,0,0,0,1,0,1,0,0,0,0,0},
		},
	},
		
	{name = 'ocel',
	heavy = true,
	x = 52,
	y = 1,
	form = 
		{
			
			{1,1,1,1,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
			{0,0,0,0,1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 44,
	y = 2,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 52,
	y = 15,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)


return level
