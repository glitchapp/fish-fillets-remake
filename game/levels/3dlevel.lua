--3DreamEngine graphics
function load3dassets()
	--load 3DreamEngine
	dream = require("3DreamEngine")
	collision = require("3DreamEngine/collision")
	
	--inits (applies settings)
	dream:init()

	--loads a object
	--yourObject = dream:loadObject("assets/3d/object")
	--bck1 = dream:loadObject("externalassets/3d/bck1")
	--monkey = dream:loadObject("assets/3d/object")
	
	--dream:loadMaterialLibrary("assets/3d/underwaterterraub")
	--underwaterraub = dream:loadObject("assets/3d/underwaterterraub")
	--woodenchair3d = dream:loadObject("externalassets/3d/level1obj/Wooden_Chair/Wooden_Chair")
	diorama = dream:loadObject("externalassets/3d/underwater_ruin_diorama")
	
	cube1 = dream:loadObject("externalassets/3d/cube1")
	
	--player1_3d = dream:loadObject("externalassets/3d/players/fish1/mm_frame")
	--player2_3d = dream:loadObject("externalassets/3d/players/fish2/mm_frame")
	--cover = dream:loadObject("assets/3d/cover")


	--creates a light
	light = dream:newLight("point", 1, 2, 3, 1.0, 0.75, 0.2,1)
	
	--add shadow
	light:addShadow()
	
	dream:setClouds(disabled)
	dream:setSky(false)
	
	dream:setFog(0.01, {0.7, 0.6, 0.5}, 0.0)
	dream:setFogHeight(-2.5, 6)
	

end
--dream:setBloom(-1,0.5,1.0,100.0)


--local r = dream:newReflection(cimg:load("assets/3d/textures/sky.cimg"))
--r:setLocal(vec3(0, 0, 0), vec3(-2, -1, -2), vec3(2, 1, 2))
--dream:setReflection(r)

function threeDdraw()
  if threeD==true then								--Draw 3d graphics if enabled
    --reset lighting to default sun
  dream:resetLight()

  --add light
  dream:addLight(light)  
  
  --prepare for rendering
  dream:prepare()  

	dream.sun = vec3(0.5, 1, 0.2)
	dream.sun_color = vec3(1, 0.7, 1)

  --rotate, draw and offset
  --diorama1:rotateY(love.timer.getDelta())
  --diorama:rotateY(0.0001)
--  cover:rotateY(0.001)
  --monkey:rotateY(0.0001)
  --dream:draw(yourObject, 0, 0, -5)
  --if nLevel<2 then
  --dream:draw(bck1, 0, 0, -5)
  --dream:draw(monkey, 0, 0, -5)
  --dream:draw(diorama, 0, 0, -100,15,15,15)
  --dream:draw(cube1, 0, 0, -5)
--  dream:draw(cover, 0, 0, -5)
   
   --draw3dborders()
  --end

  --render

	--dream:present()

  -- Draw contours in 3d
  
  end
end

function draw3dborders()


   if threeD==true then
   for y, row in ipairs(wb1) do
        for x, cell in ipairs(row) do
            if cell ~= empty then

             --dream:draw(cube1, (x - 24) , -y+13, -14)
             
			  dream:draw(cube1, ((x - threeDcellx)*threeDcellwidth+1) , (-y+threeDcelly)*threeDcellheight, threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5)
				--dream:draw(Dolphin, ffish1positionx , ffish1positiony, 0,ffish1scalex,ffish1scaley)
				--dream:draw(dolphin, ffish1positionx , ffish1positiony-300, -300,10,10)
				
                 --love.graphics.rectangle('fill',(x - 1) * cellWidth, (y - 1) * cellHeight,cellSize,cellSize-15)
                --love.graphics.rectangle('fill',(x - 1) * cellWidth, (y - 1) * cellHeight,cellSize,cellSize)
                
            end
        end
    end
  end
  
end
