local level = {}


level.name = 'level-63'

-- first cell is map[1][1], top left corner
level.map = 
{	
{6,6,3,3,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{6,6,6,0,0,3,3,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{3,0,0,0,0,0,0,3,3,3,1,1,1,1,1,1,1,3,0,3,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,3,0,3,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,0,3,3,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,3,1,1,1,1,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,3,0,0,0},
{1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,3,0,0,0},
{1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,1,3,0,0,0},
{1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0},
{1,1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0},
{1,1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,6,6},
{1,1,1,1,1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,6,6,6,6},
{1,1,1,1,1,1,1,1,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,6,6,6,6},
{1,1,1,1,1,1,1,1,3,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,3,3,6,6,6},
{1,1,1,1,1,1,1,1,3,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,1,3,0,0,0},
{1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,0,0,0},
}

level.areas = {
	{
		name = "exit", -- just in case if you want to specify the areas and conditions for it
		x = 0,
		y = 0,
		w = 1,
		h = 1,
	}
}
level.blocks = 
{
	{name = 'netopejr',
	heavy = false,
	x = 19,
	y = 20,
	form = 
		{
			{1,0},
			{1,1},
			{1,0},
		},
	},
	
	{name = 'tyc',
	heavy = true,
	x = 11,
	y = 16,
	form = 
		{
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
		},
	},
	
	{name = 'das',
	heavy = false,
	x = 18,
	y = 8,
	form = 
		{
			{1,1,1,1},
			{0,0,0,1},
		},
	},
	
	{name = 'krapnik3',
	heavy = false,
	x = 19,
	y = 4,
	form = 
		{
			{1},
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'amfora',
	heavy = false,
	x = 19,
	y = 25,
	form = 
		{
			{1},
			{1},
		},
	},
	
	{name = 'zahavec',
	heavy = false,
	x = 12,
	y = 25,
	form = 
		{
			{1,1,1,1},
			{0,1,0,0},
		},
	},
	
	{name = 'rybicka_h',
	heavy = false,
	x = 27,
	y = 19,
	form = 
		{
			{1,1},
		},
	},
	
	{name = 'muslicka',
	heavy = false,
	x = 13,
	y = 24,
	form = 
		{
			{1},
		},
	},
	
	{name = 'vazav',
	heavy = false,
	x = 19,
	y = 17,
	form = 
		{
			{1},
			{1},
			{1},
		},
	},
	
	{name = 'vaza_cervena',
	heavy = false,
	x = 19,
	y = 21,
	form = 
		{
			{1},
			{1},
		},
	},
}
level.agents = 
{
	{name = 'fish-3x1',
	fish = true,
	heavy = false,
	x = 3,
	y = 2,
	form = 
		{
			{1,1,1},
		},
	},
	
	
	{name = 'fish-4x2',
	fish = true,
	heavy = true,
	x = 3,
	y = 3,
	form = 
		{
			{1,1,1,1},
			{1,1,1,1},
		},
	},
}

prepareMap(level)

return level
