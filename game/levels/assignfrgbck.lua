--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This is a Lua function that assigns values to several variables based on the current level of the game.

The function begins by setting several variables to false and initializing the scolor4 variable. Then, based on the value of the nLevel variable (which presumably holds the current level number), different variables are assigned specific values.

For example, if nLevel equals 1, the variables currentbck, currentbck2, and currentbck3 are assigned specific background images, and the variables currentfrg and currentfrg2 are assigned specific foreground images. The scolor1, scolor2, and scolor3 variables are also set to specific values, and the bckx, bcky, bck2x, bck2y, frgx, and frgy variables are set to 0.
--]]

function assignfrgbck()

 currentbck=false currentbck2=false currentbck3=false currentbck4=false currentfrg=false currentfrg2=false 
scolor4={1,1,1,0.95}
--if res=="1440p" then expx=0.72 expy=0.72 end
	if nLevel==1 then  currentbck=level1bck   currentbck2=level1bck2 currentbck3=false currentbck4=whale 	currentfrg=level1frg currentfrg2=level1frg2		
						scolor1={1,1,1,0.95} scolor2={1,1,1,0.7}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==2 then  currentbck=level2bck	   currentbck2=level2bck2 	  currentfrg=level2frg 												scolor1={1,1,1,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==3 then  currentbck=level3bck   currentbck2=level3bck2 currentbck3=level3bck3 			currentfrg=level3frg  				scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==4 then  currentbck=level4bck1  currentbck2=level4bck2 currentfrg=level4frg1  										 		scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==5 then  currentbck=level5bck   currentbck2=level5bck2 currentfrg=level5frg   	currentbck3=level5bck3						scolor1={1,1,1,0.85} scolor2={1,1,1,0.95} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==6 then  currentbck=level6bck   currentbck2=level6bck2 currentfrg=level6frg 													scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==7 then  currentbck=level7bck   currentbck2=level7bck2 currentbck3=level7bck3 currentfrg=level7frg currentfrg2=level7frg2  	scolor1={1,1,1,0.85} scolor2={1,1,1,0.95} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==8 then  currentbck=level8bck  currentbck2=level8bck2 currentbck3=level8bck3  currentfrg=level8frg		 					scolor1={1,1,1,0.95} scolor2={1,1,1,1} scolor3={1,1,1,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==9 then  currentbck=level9bck  currentbck2=level9bck2  currentfrg=level9frg 													scolor1={1,1,1,0.95}  scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==10 then currentbck4=level10bck4 currentbck=level18bck currentbck2=level18bck2 currentbck3=level18bck3 currentfrg=level10frg 							scolor1={1,1,1,0.95} scolor2={1,1,1,0.75} scolor3={1,1,1,0.65} scolor4={1,1,1,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==11 then currentbck2=level11bck2 currentbck3=UnderwaterBGBlank currentbck=foreground11 currentfrg=false 	 currentfrg2=level11frg2			scolor1={1,1,1,1} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==12 then currentbck=level12bck 		 currentbck2=false 			 currentfrg=level12frg 											scolor1={1,1,1,0.95} 	 scolor2={1,1,1,0.60} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==13 then currentbck=false currentbck2=level17bck2	currentbck3=level17bck3		 currentfrg=level13frg 					scolor1={1,0.2,0.2,0.8} 	scolor2={1,0.2,0.2,0.60} scolor3={1,1,1,1} scolor4={1,0.2,0.2,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==14 then currentbck=level14bck currentbck2=false currentbck3=false  currentfrg=level14frg currentfrg2=level14frg2				scolor1={0.6,0.2,1,0.9}	scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==15 then currentbck=level15bck currentbck2=level15bck2 currentbck3=level15bck3 currentbck4=level15bck4 currentfrg=level15frg currentfrg2=false scolor1={1,1,1,1}	scolor2={1,1,1,0.95} scolor3={1,1,1,0.90} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==16 then currentbck=level16bck currentbck2=level16bck2 currentbck3=false currentfrg=level16frg 										scolor1={1,1,1,1}	scolor2={0.2,0.1,0.6,0.85} scolor3={1,1,1,1} scolor4={1,1,1,0.95}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==17 then currentbck=level17bck currentbck2=level17bck2 currentbck3=level17bck3 currentfrg=level17frg							scolor1={1,1,1,0.95} scolor2={1,1,1,0.85} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==18 then currentbck4=level10bck4 currentbck=level18bck currentbck2=level18bck2 currentbck3=level18bck3 currentfrg=level18frg 							scolor1={1,1,1,0.95} scolor2={1,1,1,0.75} scolor3={1,1,1,0.65} scolor4={1,1,1,0.85}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==19 then currentbck=level19bck currentbck2=level19bck2  currentbck3=level19bck3 currentbck4=level19bck4 currentfrg=level19frg	scolor1={1,0,1,0.95} scolor2={1,0,1,0.95} scolor3={1,1,1,1} scolor4={1,0,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==20 then currentbck=level20bck currentbck2=level20bck2 currentbck3=level20bck3 currentfrg=level20frg 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==21 then currentbck=underwater_fantasy1_2 currentbck2=underwater_fantasy1 currentbck3=false currentfrg=level21frg 				scolor1={1,1,1,0.95} scolor2={1,1,1,0.9}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
--elseif nLevel==21 then currentbck=underwater_fantasy1 currentbck2=level21screensaver1 currentbck3=level21screensaver2 currentfrg=level21frg scolor1={1,1,1,0.95} scolor2={1,1,1,0.5} scolor3={1,1,1,1} scolor4={1,1,1,0.9}  bckx=0 bcky=0 bck2x=0 bck2y=-200 frgx=0 frgy=0
elseif nLevel==22 then currentbck=level22bck currentbck2=level22bck2 currentbck3=level22bck3 currentfrg=level22frg 												scolor1={1,1,1,1} 	 scolor2={1,1,1,0.8}	  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==23 then currentbck=level23bck2 currentbck2=level23bck currentbck3=level23bck3 currentfrg=level23frg 							scolor1={1,1,1,0.85} scolor2={1,1,1,1} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==24 then currentbck=level24bck currentbck2=level24bck2 currentfrg=level24frg 															scolor1={1,1,1,0.85} scolor2={1,1,1,1} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==25 then currentbck=level25bck currentbck2=level25bck2 currentfrg=level25frg 															scolor1={1,1,1,0.85} scolor2={1,1,1,0.85} 		scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==26 then currentbck=level26bck currentbck2=level24bck2 currentbck3=false currentfrg=level26frg 								scolor1={1,1,1,0.7} scolor2={1,1,1,0.8}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==27 then currentbck=level27bck currentbck2=level24bck2 currentfrg=level27frg 													scolor1={1,0.5,1,0.8} scolor2={1,0.5,1,0.7}	scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==28 then currentbck=level28bck currentbck2=false currentbck3=false  currentfrg=level28frg 						scolor1={1,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==29 then currentbck=level29bck currentbck2=level29bck2 currentfrg=level29frg currentfrg2=level29frg2 							scolor1={1,1,1,0.90} scolor2={1,0,0.5,0.94} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==30 then currentbck=level30bck currentbck2=level30bck2 currentfrg=level30frg  									scolor1={1,1,1,0.95} scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==31 then currentbck=level31bck currentbck2=level31bck2 currentbck3=level31bck3 currentfrg=level31frg		  		scolor1={1,1,1,0.95} scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==32 then currentbck=level32bck currentbck2=level32bck2 currentfrg=level32frg 																									scolor1={1,1,1,0.95}scolor2={1,1,1,0.95}  scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==33 then currentbck=level33bck currentbck2=level33bck2 currentfrg=level33frg   currentfrg2=level33frg2 						scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==34 then currentbck=level34bck currentbck2=level34bck2 currentfrg=level34frg   currentfrg2=level34frg2 						scolor1={1,0.65,0.8,0.85} scolor2={1,0.65,0.8,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
--elseif nLevel==34 then currentbck=level34bck currentbck2=level34bck2 currentfrg=level34frg   currentfrg2=level34frg2 						scolor1={1,0.65,0.8,0.85} scolor2={1,0.65,0.8,0.80} scolor3={1,1,1,1} expx=0.1 expy=0.1 
elseif nLevel==35 then currentbck=level35bck currentbck2=level35bck2 currentbck3=level35bck3 currentfrg=level35frg 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==36 then currentbck=level36bck currentbck2=level36bck2 currentbck3=level36bck3 currentfrg=level36frg 							scolor1={1,1,1,0.85} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==37 then currentbck=level37creature currentbck2=level37bck currentbck3=level37bck2 currentfrg=level37frg currentfrg2=level37frg2 scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

elseif nLevel==38 then currentbck=level38bck currentbck2=level38bck2 currentbck3=false currentfrg=level38frg currentfrg2=false 				scolor1={0.6,1,0.8,0.65} scolor2={1,1,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==39 then currentbck=level39bck currentbck2=false currentbck3=false currentfrg=level39frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==40 then currentbck=level40bck currentbck2=level40bck2 currentbck3=false currentfrg=level40frg currentfrg2=false 				scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.65} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==41 then currentbck=level41bck currentbck2=false currentbck3=false currentfrg=level41frg currentfrg2=false 					scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==42 then currentbck=level42bck currentbck2=false currentbck3=false currentfrg=level42frg currentfrg2=false 					scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==43 then currentbck=level43bck currentbck2=false currentbck3=false currentfrg=level43frg currentfrg2=false 					scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==44 then currentbck=level44bck currentbck2=false currentbck3=false currentfrg=level44frg currentfrg2=false 					scolor1={1,0.65,0.8,0.65} scolor2={0,0,1,0.90} scolor3={1,1,1,0.9} scolor4={1,1,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==45 then currentbck=level45bck currentbck2=false currentbck3=false currentfrg=level45frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==46 then currentbck=level46bck currentbck2=false currentbck3=false currentfrg=level46frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==47 then currentbck=level47bck currentbck2=false currentbck3=false currentfrg=level47frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==48 then currentbck=level48bck currentbck2=false currentbck3=false currentfrg=level48frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==49 then currentbck=level49bck currentbck2=false currentbck3=false currentfrg=level49frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==50 then currentbck=level50bck currentbck2=false currentbck3=false currentfrg=level50frg currentfrg2=false 					scolor1={1,0.65,0.8,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==51 then currentbck=level51bck currentfrg=level51frg														 					scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==52 then currentbck=level52bck currentbck2=level52frg2 currentbck3=level52bck2 			currentfrg=level52frg 		scolor1={1,0.65,0.8,0.6} scolor2={1,1,1,0.90}	scolor3={1,1,1,0.8} scolor4={1,1,1,0.8}   bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==53 then currentbck=level53bck currentbck2=false currentbck3=false currentfrg=level53frg 										scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==54 then currentbck=level54bck currentbck2=level54bck2 currentbck3=level52bck3 currentbck4=level52bck4 currentfrg=level54frg currentfrg2=level54frg2 	scolor1={1,1,1,1} scolor2={1,1,1,0.90} scolor3={1,1,1,0.8} scolor4={0,1,0.5,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==55 then currentbck=level55bck currentbck2=false currentbck3=false currentfrg=level55frg 									scolor1={0,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==56 then currentbck=level56bck currentbck2=false currentbck3=false currentbck4=level52bck4 currentfrg=level56frg 				scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={0,1,0,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==57 then currentbck=level57bck currentbck3=level57bck3 currentbck4=level52bck4 currentfrg=level57frg 							scolor1={0,1,0,0.95} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={0,1,0,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==58 then currentbck=level58bck currentbck2=level57bck3 currentbck4=level52bck4 currentfrg=level58frg 							scolor1={0.8,1,0,0.9} scolor2={0,0.8,1,0.90} scolor3={1,1,1,1} scolor4={0.8,0.5,1,0.8}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==59 then currentbck=level59bck currentbck2=level59bck2 currentbck3=false currentfrg=level59frg								scolor1={1,0.65,0.8,0.90} scolor2={1,1,1,0.95} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==60 then currentbck=level60bck currentbck2=level60bck2 currentbck3=false currentfrg=level60frg 								scolor1={1,1,1,0.90} scolor2={1,1,1,0.90} scolor3={1,1,1,1} scolor4={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==61 then currentbck=level61bck currentbck2=level61bck2 currentbck3=false currentfrg2=level61frg2  currentfrg=level61frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==62 then currentbck=level62bck currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level62frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==63 then currentbck=level63bck currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level63frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==64 then currentbck=level64bck currentbck2=false currentbck3=false currentfrg2=false  currentfrg=level64frg 		scolor1={1,0.5,0.5,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==65 then currentbck=false currentbck2=false currentbck3=false currentfrg2=level61frg2  currentfrg=level65frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==66 then currentbck=level66bck currentbck2=false currentbck3=false currentfrg=level66frg 		scolor1={1,1,1,0} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==67 then currentbck=level67bck currentbck2=false currentbck3=false currentfrg=level67frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==68 then currentbck=level68bck currentbck2=false currentbck3=false currentfrg=level68frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==69 then currentbck=level69bck currentbck2=false currentbck3=false currentfrg=level69frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==70 then currentbck=level70bck currentbck2=false currentbck3=false currentfrg=level70frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==71 then currentbck=level71bck currentbck2=false currentbck3=false currentfrg=level71frg currentfrg2=false 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==72 then currentbck=level72bck currentbck2=level72bck2 currentbck3=false currentfrg=level72frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==73 then currentbck=level73bck currentbck2=level73bck2 currentbck3=false currentfrg=level73frg 		scolor1={1,1,1,1} scolor2={1,1,1,0.92} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==74 then currentbck=level74bck currentbck2=level74bck2 currentbck3=false currentfrg=level74frg 		scolor1={1,1,1,1} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==75 then currentbck=level75bck currentbck2=false currentbck3=false currentfrg=level75frg 		scolor1={1,1,1,0.95} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==76 then currentbck=level76bck currentbck2=false currentbck3=false currentfrg=level76frg 		scolor1={1,1,1,0.95} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==77 then currentbck=level77bck currentbck2=false currentbck3=false currentfrg=level77frg 		scolor1={1,1,1,1} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==78 then currentbck=level78bck currentbck2=false currentbck3=false currentfrg=level78frg 		scolor1={1,1,1,1} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==79 then currentbck=false currentbck2=false currentbck3=false currentfrg=level79frg 		scolor1={1,1,1,1} scolor2={1,0.2,0.4,0.97} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

--screensavers

elseif nLevel==101 then currentbck=level3bck currentbck2=level3bck2 currentbck3=false currentfrg=false  									scolor1={1,1,1,0.85} 	  scolor2={1,1,1,0.90} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==102 then currentbck=underwater_fantasy1 currentbck2=level21screensaver1 currentbck3=level21screensaver2 currentfrg=false 	scolor1={1,1,1,0.95} 	  scolor2={1,1,1,0.5}  scolor3={1,1,1,1} scolor4={1,1,1,0.9}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==103 then currentbck=level34bck currentbck2=level34bck2 currentfrg=false currentfrg2=level34frg2 	scolor1={1,0.65,0.8,0.85}   scolor2={1,0.65,0.8,0.80} scolor3={1,1,1,1}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0

--extra

elseif nLevel==80 then currentbck=level80bck currentbck2=false currentfrg=level80frg currentfrg2=false 	scolor1={1,1,1,1}   scolor2={1,1,1,0.80} scolor3={1,1,1,0.8} scolor4={1,1,1,0.9} bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
elseif nLevel==81 then currentbck=level81bck currentbck2=level81bck2 currentfrg=level81frg currentfrg2=level81frg2 currentbck3=false 	scolor1={1,1,1,0.85}   scolor2={1,1,1,0.90} scolor3={1,1,1,0.8} scolor4={1,1,1,0.9}  bckx=0 bcky=0 bck2x=0 bck2y=0 frgx=0 frgy=0
	

end
assignremakesize()
end

--[[
The function assignremakesize() appears to be a utility function for setting up scaling factors for the game graphics based on the current screen resolution res.

It uses a series of conditional statements to set different values for expx, expy, dividx, and dividy depending on the value of res. These values are then used elsewhere in the game code to control the size and position of various game objects in order to maintain consistent proportions across different screen resolutions.

For example, if res is "1440p", the function sets expx and expy to 0.715 and dividx and dividy to 1.42, indicating that game graphics should be scaled up to 71.5% of their original size in both X and Y dimensions, and that various elements should be divided by 1.42 to maintain the same relative size and position.
--]]

-- Function to calculate the quadratic polynomial coefficients for the x-axis
function calculateQuadraticPolynomialCoefficientsX(x1, y1, x2, y2, x3, y3, mazeWidth, mazeHeight)
    local denom = (x1 - x2) * (x1 - x3) * (x2 - x3)
    local a = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom
    local b = (x3^2 * (y1 - y2) + x2^2 * (y3 - y1) + x1^2 * (y2 - y3)) / denom
    local c = (x2 * x3 * (x2 - x3) * y1 + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom
    
    -- Adjust scaling factor based on maze size
    local scalingFactorX = a * level.w^2 + b * level.w + c
    local scalingFactorY = a * level.h^2 + b * level.h + c

    return {scalingFactorX, scalingFactorY}
end

--local cs1 = calculateQuadraticPolynomialCoefficients(720, 0.38, 1080, 0.52, 1440, 0.67)
--local cs2 = calculateQuadraticPolynomialCoefficients(720, 0.75, 1080, 1.02, 1440, 1.33)

function calculateScaling()

-- Example: Calculate scaling factor for 1920x1080 resolution (Full HD) and a maze size of 10x10
local resolutionX = game.screen_width
local resolutionY = game.screen_height
local mazeWidth = level.w
local mazeHeight = level.h

	local cs = calculateQuadraticPolynomialCoefficientsX(720, expx, 1080, expx, 1440, expx)

local scalingFactorX = cs[1]
local scalingFactorY = cs[2]

-- scalingFactorX and scalingFactorY now contain the scaling factors for both axes for the specified resolution and maze size.


--print(scalingFactorX)
end


function assignremakesize()


		if res=="720p" then expx=0.38 expy=0.38 dividx=0.75 dividy=0.75
	elseif res=="1080p" then expx=0.52 expy=0.52 dividx=1.02 dividy=1.02
	elseif res=="1440p" then expx=0.670 expy=0.670 dividx=1.33 dividy=1.33
	elseif res=="4k" then expx=1.07 expy=1.07 dividx=2.1 dividy=2.1
	end
			if nLevel==1 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.065 dividy=1.065
		elseif nLevel==2 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.05 dividy=1.05
		elseif nLevel==3 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.045 dividy=1.045
			
		elseif nLevel==4 and res=="1080p" then expx=0.532 expy=0.532 dividx=1 dividy=1
		elseif nLevel==4 and res=="1440p" then expx=0.673 expy=0.673 dividx=1.38 dividy=1.38
		elseif nLevel==4 and res=="4k" then expx=1.07 expy=1.07 dividx=2.2 dividy=2.2
		
		elseif nLevel==5 and res=="720p" then expx=0.47 expy=0.47 dividx=0.52 dividy=0.52
		elseif nLevel==5 and res=="1080p" then expx=0.532 expy=0.532 dividx=0.57 dividy=0.57
		elseif nLevel==5 and res=="1440p" then dividx=0.8 dividy=0.8 dividx=0.75 dividy=0.75
		elseif nLevel==5 and res=="4k" then expx=1.07 expy=1.07 dividx=1.15 dividy=1.15
		
		elseif nLevel==6 and res=="720p" then expx=0.47 expy=0.47 dividx=0.94 dividy=0.94
		elseif nLevel==6 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.04 dividy=1.04
		
		elseif nLevel==7 and res=="720p" then expx=0.335 expy=0.335 dividx=0.4 dividy=0.4
		elseif nLevel==7 and res=="1080p" then expx=0.5 expy=0.5 dividx=0.6 dividy=0.6
		elseif nLevel==7 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.8 dividy=0.8
		elseif nLevel==7 and res=="4k" then expx=1.005 expy=1.005 dividx=1.17 dividy=1.17
		
		elseif nLevel==8 and res=="720p" then expx=0.47 expy=0.47 dividx=0.95 dividy=0.95
		elseif nLevel==8 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.08 dividy=1.08
		elseif nLevel==8 and res=="1440p" then expx=0.668 expy=0.668 dividx=1.35 dividy=1.35
		elseif nLevel==8 and res=="4k" then expx=1.06 expy=1.06 dividx=2.12 dividy=2.12
		
		elseif nLevel==9 and res=="720p" then expx=0.335 expy=0.335 dividx=1.25 dividy=1.25
		elseif nLevel==9 and res=="1080p" then expx=0.5 expy=0.5 dividx=0.6 dividy=0.6
		elseif nLevel==9 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.8 dividy=0.8
		
		elseif nLevel==10 and res=="720p" then expx=0.402 expy=0.402 dividx=0.39 dividy=0.39
		elseif nLevel==10 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.51 dividy=0.51
		elseif nLevel==10 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.67 dividy=0.67
		
		elseif nLevel==11 and res=="720p" then expx=0.45 expy=0.45 dividx=0.9 dividy=0.9
		elseif nLevel==11 and res=="1080p" then expx=0.52 expy=0.52 dividx=1 dividy=1
		
		elseif nLevel==12 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.88 dividy=0.88
		elseif nLevel==12 and res=="1440p" then dividx=1.2 dividy=1.2 dividx=1.1 dividy=1.1
		
		--elseif nLevel==13 and res=="1080p" then expx=0.715 expy=0.715 dividx=1.02 dividy=1.02
		elseif nLevel==13 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.255 dividy=0.255
		--elseif nLevel==13 and res=="1440p" then expx=0.715 expy=0.715 dividx=1.37 dividy=1.37
		elseif nLevel==13 and res=="1440p" then expx=0.667 expy=0.667 dividx=0.32 dividy=0.32	-- 4 times resolution for the objects
		
		elseif nLevel==14 and res=="1080p" then dividx=1.48 dividy=1.48 dividx=1 dividy=1
		elseif nLevel==14 and res=="1440p" then dividx=1.8 dividy=1.8  dividx=1.3 dividy=1.3
		
		elseif nLevel==15 and res=="1080p" then expx=0.5 expy=0.5 dividx=0.98 dividy=0.98
		elseif nLevel==15 and res=="1440p" then expx=0.67 expy=0.67 dividx=1.32 dividy=1.32
		
		elseif nLevel==16 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.95 dividy=0.95
		elseif nLevel==16 and res=="1440p" then dividx=1.32 dividy=1.32
		
		--elseif nLevel==17 and res=="1080p" then expx=0.515 expy=0.515 dividx=1.02 dividy=1.02
		elseif nLevel==17 and res=="1080p" then expx=0.515 expy=0.515 dividx=0.255 dividy=0.255
		--elseif nLevel==17 and res=="1440p" then expx=0.685 expy=0.685 dividx=1.37 dividy=1.37
		elseif nLevel==17 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.325 dividy=0.325	-- 4 times resolution for the objects
		
		elseif nLevel==18 and res=="720p" then expx=0.402 expy=0.402 dividx=0.39 dividy=0.39
		elseif nLevel==18 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.51 dividy=0.51
		elseif nLevel==18 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.67 dividy=0.67
		
		elseif nLevel==19 and res=="1440p" then expx=0.67 expy=0.67 dividx=1.35 dividy=1.35
		
		elseif nLevel==21 and res=="1080p" then expx=0.5 expy=0.5 dividx=1.02 dividy=1.02
		elseif nLevel==21 and res=="1440p" then expx=0.67 expy=0.67 dividx=1.32 dividy=1.32
		
		elseif nLevel==25 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.02 dividy=1.02
		elseif nLevel==25 and res=="1440p" then expx=0.669 expy=0.669 dividx=1.37 dividy=1.37
		
		elseif nLevel==31 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.02 dividy=1.02
		elseif nLevel==31 and res=="1440p" then dividx=1.425 dividy=1.425  dividx=1.34 dividy=1.34
		
		elseif nLevel==32 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.9 dividy=0.9
		elseif nLevel==32 and res=="1440p" then dividx=1.2 dividy=1.2
		
		elseif nLevel==33 and res=="1080p" then expx=0.52 expy=0.52 dividx=2.5 dividy=2.5
		elseif nLevel==33 and res=="1440p" then dividx=3.3 dividy=3.3
		
		elseif nLevel==34 and res=="1080p" then expx=0.532 expy=0.532 dividx=1.05 dividy=1.05
		elseif nLevel==34 and res=="1440p" then dividx=1.32 dividy=1.32
		
		elseif nLevel==35 and res=="1080p" then expx=0.532 expy=0.532 dividx=1.05 dividy=1.05
		elseif nLevel==35 and res=="1440p" then expx=0.674 expy=0.674 dividx=1.36 dividy=1.36
		
		elseif nLevel==36 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.05 dividy=1.05
		elseif nLevel==36 and res=="1440p" then dividx=1.5 dividy=1.5
		
		elseif nLevel==37 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.95 dividy=0.95
		elseif nLevel==37 and res=="1440p" then dividx=1.32 dividy=1.32 dividx=1.2 dividy=1.2
		
		elseif nLevel==38 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.3 dividy=1.3
		elseif nLevel==38 and res=="1440p" then dividx=1.8 dividy=1.8
		
		elseif nLevel==39 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.4 dividy=1.4
		elseif nLevel==39 and res=="1440p" then dividx=1.8 dividy=1.8 dividx=1.8 dividy=1.8
		
		elseif nLevel==40 and res=="1080p" then expx=0.519 expy=0.519 dividx=1.1 dividy=1.1
		
		elseif nLevel==41 and res=="1080p" then expx=0.5 expy=0.5 dividx=1.6 dividy=1.6
		elseif nLevel==41 and res=="1440p" then expx=0.65 expy=0.65  dividx=2.2 dividy=2.2
		
		elseif nLevel==42 and res=="1080p" then expx=0.5 expy=0.5 dividx=0.9 dividy=0.9
		elseif nLevel==42 and res=="1440p" then expx=0.645 expy=0.645 dividx=1.2 dividy=1.2
		
		elseif nLevel==43 and res=="1080p" then expx=0.5 expy=0.5  dividx=1.1 dividy=1.1
		elseif nLevel==43 and res=="1440p" then expx=0.645 expy=0.645 dividx=1.4 dividy=1.4
		
		elseif nLevel==44 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.02 dividy=1.02
		
		elseif nLevel==45 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.6 dividy=1.6
		elseif nLevel==45 and res=="1440p" then dividx=2.2 dividy=2.2
		elseif nLevel==49 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.5 dividy=1.5
		
		elseif nLevel==49 and res=="1440p" then dividx=2 dividy=2
		elseif nLevel==50 and res=="1440p" then dividx=1.38 dividy=1.38
		
		elseif nLevel==54 and res=="1080p" then  expx=0.52 expy=0.52 dividx=1.02 dividy=1.02
		elseif nLevel==54 and res=="1440p" then  expx=0.67 expy=0.67 dividx=1.29 dividy=1.29
		
		elseif nLevel==55 and res=="1080p" then expx=0.535 expy=0.535 dividx=1 dividy=1
		elseif nLevel==55 and res=="1440p" then expx=0.67 expy=0.67 dividx=1.27 dividy=1.27
		
		elseif nLevel==56 and res=="1080p" then expx=0.522 expy=0.522 dividx=0.97 dividy=0.97
		elseif nLevel==56 and res=="1440p" then expx=0.67 expy=0.67 dividx=1.25 dividy=1.25
		
		elseif nLevel==57 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.1 dividy=1.1
		elseif nLevel==57 and res=="1440p" then dividx=1.5 dividy=1.5 dividx=1.4 dividy=1.4
		
		elseif nLevel==58 and res=="1080p" then expx=0.535 expy=0.535 dividx=1.02 dividy=1.02
		
		elseif nLevel==59 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.04 dividy=1.04
		elseif nLevel==61 and res=="1080p" then expx=0.52 expy=0.52 dividx=1.03 dividy=1.03
		
		elseif nLevel==62 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.58 dividy=0.58
		elseif nLevel==62 and res=="1440p" then dividx=0.8 dividy=0.8
		
		elseif nLevel==63 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.62 dividy=0.62
		elseif nLevel==63 and res=="1440p" then dividx=0.85 dividy=0.85
		
		elseif nLevel==64 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.5 dividy=0.5
		elseif nLevel==64 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.6 dividy=0.6
		
		elseif nLevel==65 and res=="1440p" then dividx=1.5 dividy=1.5
		
		elseif nLevel==67 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.9 dividy=0.9
		elseif nLevel==67 and res=="1440p" then dividx=1.3 dividy=1.3
		
		elseif nLevel==68 and res=="1080p" then expx=1.04 expy=1.04 dividx=0.9 dividy=0.9
		elseif nLevel==68 and res=="1440p" then expx=1.34 expy=1.34 dividx=1.3 dividy=1.3
		
		elseif nLevel==69 and res=="1080p" then expx=0.52 expy=0.52 dividx=0.65 dividy=0.65
		elseif nLevel==69 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.82 dividy=0.82
		
		elseif nLevel==70 and res=="1080p" then dividx=0.7 dividy=0.7  dividx=0.5 dividy=0.5
		elseif nLevel==70 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.64 dividy=0.64
		
		elseif nLevel==71 and res=="1080p" then expx=0.5 expy=0.5   dividx=0.65 dividy=0.65
		elseif nLevel==71 and res=="1440p" then expx=0.666 expy=0.666 dividx=0.9 dividy=0.9
		
		elseif nLevel==72 and res=="1080p" then expx=0.538 expy=0.538   dividx=0.75 dividy=0.75
		elseif nLevel==72 and res=="1440p" then expx=0.67 expy=0.67 dividx=0.98 dividy=0.98
		
		elseif nLevel==73 and res=="1080p" then expx=0.5 expy=0.5   dividx=0.55 dividy=0.55
		elseif nLevel==73 and res=="1440p" then expx=0.632 expy=0.632 dividx=0.71 dividy=0.71
		
		elseif nLevel==74 and res=="1080p" then dividx=0.6 dividy=0.6  dividx=0.45 dividy=0.45
		elseif nLevel==74 and res=="1440p" then expx=0.668 expy=0.668 dividx=0.56 dividy=0.56
		
		elseif nLevel==75 and res=="1080p" then expx=0.725 expy=0.725 dividx=0.482 dividy=0.482
		elseif nLevel==75 and res=="1440p" then expx=0.94 expy=0.94 dividx=0.65 dividy=0.65
		
		elseif nLevel==76 and res=="1080p" then expx=0.725 expy=0.725 dividx=0.8 dividy=0.8
		elseif nLevel==76 and res=="1440p" then expx=0.95 expy=0.95 dividx=1.1 dividy=1.1
		
		elseif nLevel==77 and res=="1080p" then expx=0.73 expy=0.73 dividx=0.53 dividy=0.53
		elseif nLevel==77 and res=="1440p" then expx=0.94 expy=0.94 dividx=0.7 dividy=0.7
		
		elseif nLevel==78 and res=="1440p" then expx=1 expy=1 dividx=1.52 dividy=1.52
		elseif nLevel==79 and res=="1440p" then expx=0.94 expy=0.94 dividx=0.6 dividy=0.6
		elseif nLevel==101 and res=="1440p" then expx=0.715 expy=0.715
		elseif nLevel==102 and res=="1440p" then dividx=1 dividy=0.6
		elseif nLevel==103 and res=="1440p" then dividx=0.6 dividy=0.6
		end
	
	
	--calculateScaling()
end

--[[
The code is defining the drawforshader2() function that is responsible for drawing the background animation of the game.

If the background variable is set to "yes" and the palette variable is set to 1, the function checks the nLevel variable to determine which type of background animation to draw.

If nLevel is 1, the function calls the animatewhale() function and sets the color to scolor1. If nLevel is 3, 52, 54, 56, 57, 58, or 101, the function calls the animateffish1() function.

The animatewhale(), animateffish1(), animatedolphin(), animatedshark(), animatemantaray(), and animatefishs() functions are not shown in the code provided, but it can be inferred that they are responsible for animating the different types of background elements in the game.
--]]
function drawforshader2()

if background=="yes" and palette==1 then
	
		if nLevel==1 then 	love.graphics.setColor(scolor1,scolor1[4])
							animatewhale()
							love.graphics.setColor(1,1,1,0.55) drawBoids()
	elseif nLevel==2 or nLevel==13 or nLevel==17  or nLevel==20 or nLevel==25 then love.graphics.setColor(1,1,1,0.55) drawBoids()
	elseif nLevel==33 or nLevel==34 or nLevel==38 or nLevel==40 or nLevel==41 or nLevel==42 or nLevel==43 or nLevel==44 then love.graphics.setColor(1,1,1,0.20) drawBoids()
	elseif nLevel==11 then love.graphics.setColor(1,1,1,1) drawBoids()
	 	
	elseif nLevel==52 or nLevel==54 or nLevel==56 or nLevel==57 or nLevel==58 or nLevel==101 then 	--love.graphics.setColor(scolor1,scolor1[4])
											animateffish1()
											love.graphics.setColor(1,1,1,0.55) drawBoids()
											
	elseif nLevel==7 then animatedolphin()	
	elseif nLevel==15  then animatedshark() --love.graphics.setColor(1,1,1,0.55) drawBoids()
	elseif nLevel==34 or nLevel==103 then love.graphics.setColor(scolor1,scolor1[4])
											animatewhale() 
	elseif nLevel==35 then love.graphics.setColor(scolor1,scolor1[4])
							animatemantaray() --animatefishs()
	
	elseif nLevel==14 or nLevel==102 or nLevel==103 then drawBoids() --animatefishs()
	end	-- animate the whale

--[[This is a block of code that handles the rendering of the game background based on various conditions.
 The code first checks if currentbck4 is not false, and if it is not, it applies color adjustments using the values of adjustr, adjustg, and adjustb. If these values are all equal to 1, it sets the color of the background to scolor4, otherwise it updates the value of scolor4 with the adjusted color values.
--]]
			if not currentbck4==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,scolor4[4]}
				love.graphics.setColor(scolor4)
				end
				--[[Next, the code checks if nLevel equals 15, and if it does, it applies a color effect based on the value of beat. If beat is less than 50, it sets the color of the background to a combination of red, green, and blue values that depend on beat. Otherwise, it sets the color to scolor3.--]]
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor3)
					end
				end
				--If nLevel equals 54 and enginepushed is true, it applies a color effect based on subbeat and draws the background using currentbck4
				if nLevel==54 and enginepushed then	love.graphics.setColor(1/subbeat/2,0,0,0.8)
					love.graphics.draw (currentbck4, bckx,bcky,0,expx,expy)
				end
						if nLevel==56 then
							if lightswitchon==false then  love.graphics.setColor(0.01,0.01,0.03,0.9) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,0.8) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.4,0.4,0.8)
						end
						end
				
				-- Chromatic aberration
				--Finally, the code handles chromatic aberration based on the value of chromaab.
				-- If chromaab is false, it draws the background using currentbck4 and applies a water warp shader effect if nLevel equals 15 and beat is greater than 50.
				if chromaab==false then 
				love.graphics.draw (currentbck4, bckx,bcky,0,expx,expy)
						if nLevel==15 then 
								if beat<50 then love.graphics.draw (level15bck5, bckx,bcky,0,expx,expy)
							elseif beat>50 then
								
								--water warp shader
								love.graphics.setCanvas(canvas)
								causticsAndNormals:send("time", timer)
								love.graphics.setShader(causticsAndNormals)
								love.graphics.draw (level15bck5, bckx,bcky,0,expx,expy)
								love.graphics.setCanvas()
							end
						end
			--If chromaab is true, it applies a color effect to currentbck4 by drawing three copies of the background with different color channels (red, green, and blue).
			elseif chromaab==true then
				love.graphics.setColor(1,0,0,scolor4[4]/3)
				love.graphics.draw (currentbck4, bckx,bcky,0,expx,expy)
				love.graphics.setColor(0,1,0,scolor4[4]/3)
				love.graphics.draw (currentbck4, bckx+5,bcky-5,0,expx,expy)
				love.graphics.setColor(0,0,1,scolor4[4]/3)
				love.graphics.draw (currentbck4, bckx+10,bcky-10,0,expx,expy)
			end
				
			end
	
	--[[This section of code is responsible for setting the color and graphics for the game's background.
	It starts with an if statement checking if currentbck3 is not false.
	If it's true, it proceeds to check the level number (nLevel) to determine the appropriate color and graphics adjustments.
	--]]
			if not currentbck3==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,0.85}
				love.graphics.setColor(scolor4)
				end
			
			--If nLevel is 15, it checks if beat is less than 50 and sets the color based on beat value. 
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor4)
					end
				end
			--If nLevel is 36, it sets the color to white and animates a whale.
				if nLevel==36 then love.graphics.setColor(1,1,1,0.85) animatewhale() end
			--If nLevel is 52, it checks if beat is less than 50 and sets the color based on beat value.
				if nLevel==52 then
					if beat<50 then mybeat=beat
						
						love.graphics.setColor(beat/40,beat/30,beat/40,0.85)
					else love.graphics.setColor(scolor4)
					end
				end
				--If nLevel is 54 and enginepushed is true, it sets a specific color and applies various effects such as colorgradespimple, vignette, scanlines, crt, dmg, godsray, desaturate, pixelate, posterize, boxblur, fastgaussianblur, chromasep, filmgrain, and glow.
				if nLevel==54 and enginepushed then	love.graphics.setColor(1/subbeat/2,0,0,1)
						if timer<5 then
						love.graphics.draw (currentbck3, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
						elseif timer >4 then
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("godsray")
							setflangereffect()
							--audio effect
							engineon:setEffect('myEffect')
						if isgamepad==true and vibration==true then joystickvibration = joystick:setVibration(0.05, 0.05,-1) end							
							
						--love.graphics.setShader()
						effect(function()
						love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
						end)
						end
				else
						setreverbeffect()
						
						-- Chromatic aberration
						if chromaab==false then 
							if nLevel==22 then
								--water warp shader
								love.graphics.setCanvas(canvas)
								causticsAndNormals:send("time", timer)
								love.graphics.setShader(causticsAndNormals)
								love.graphics.setCanvas()
								
							end
						love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
						--The code also sets the shader for water warp effect when nLevel is 22
						if nLevel==22 then --love.graphics.setShader()
						 end
					elseif chromaab==true then
						love.graphics.setColor(1,0,0,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x,bck2y,0,expx,expy)
						love.graphics.setColor(0,1,0,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x+5,bck2y-5,0,expx,expy)
						love.graphics.setColor(0,0,1,scolor4[4]/3)
						love.graphics.draw (currentbck3, bck2x+10,bck2y-10,0,expx,expy)
					end
				end
			end
			-- draws bubbles when nLevel is 15 and skin is "remake",
			if nLevel==15 and skin=="remake" then drawbubbles() end	--bubbles level 15
				--and sets the color and graphics when nLevel is 56 based on whether lightswitchon and lightswitchpushedafterfalling are true or false. 
				 --Finally, it draws the background with the applied adjustments and effects.
				if nLevel==56 then
							if lightswitchon==false then  love.graphics.setColor(0,0,0,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.4,0.4,1)
						end
				end
			--[[This section of the code is responsible for rendering the background graphics for the game, with additional effects depending on the current level and game state. The if statement at the beginning checks whether the current background is false, and if not, the background is drawn with appropriate adjustments.

				Next, a series of if statements check the current level and apply various effects accordingly.
			--]]
			if not currentbck2==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor2)		-- adjust colors
				else scolor2={adjustr,adjustg,adjustb,scolor2[4]}
				love.graphics.setColor(scolor2)
					
				end

				 --For example, if the current level is 15 and the beat is less than 50, the beat color is used to adjust the background color.
				if nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat/15,beat/15,beat/15,0.85)
					else --love.graphics.setColor(1,math.random(0.80,0.81),math.random(0.80,0.81),1)
					end
					-- If the current level is 5, 8, 13, 17, 59, or 102, a water warp shader is applied to the canvas.
					elseif nLevel==5 or nLevel==8 or nLevel==13 or nLevel==17 or nLevel==59 or nLevel==102 then
						--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
		
						elseif nLevel==3 then
							causticsAndNormals:send("u_normals", level3bck2Normals)
							causticsAndNormals:send("u_heightmap", level3bck2Height)
							
						elseif nLevel==5 then
							causticsAndNormals:send("u_normals", level5bck2Normals)
							causticsAndNormals:send("u_heightmap", level5bck2heightmap)
						
				end
					--If the current level is 21, a different warp shader is used instead.
					if nLevel==21 then
			
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()

					end
						
					if nLevel==26 then

						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()

					end
					--Additionally, if the current level is 54 and the engine is pushed, the background is drawn with a red tint that changes depending on the subbeat. If chromaab is true, a chromatic aberration effect is applied to the background.
					if nLevel==54 and enginepushed then
							love.graphics.setColor(1/subbeat/2,0,0,1)
						love.graphics.draw (currentbck2, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
					end
					
					-- Chromatic aberration
						if chromaab==false then love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
					elseif chromaab==true then
						love.graphics.setColor(1,0,0,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
						love.graphics.setColor(0,1,0,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x+5,bck2y-5,0,expx,expy)
						love.graphics.setColor(0,0,1,scolor2[4]/3)
						love.graphics.draw (currentbck2, bck2x+10,bck2y-10,0,expx,expy)
					end
				
		

		
				--No power animation
				--Finally, if the current level is 15, a "no power" animation is drawn over the background.
				if nLevel==15 then
					love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,1)
					love.graphics.draw (level15nopower, bck2x,bck2y,0,expx,expy)
				end
			end
			-- If the current level is 16, 29, or 32, fish animations are rendered. 
			if nLevel==16 or nLevel==32 then  --animatefishs() 
				animateffish1()
				love.graphics.setColor(1,1,1,0.15)
				drawBoids()
			end
			--If the current level is 7, 30, 31, 34, 35, or 36, fish animations are also rendered but without any color effects.
	
		if nLevel==29 then love.graphics.setColor(1,1,1,0.15) drawBoids() --animatefishs()
		 end
		
		--tetris
		--And if tetris is enabled, block graphics are drawn.
		if tetris==true then blocksdraw(dt) end
		
		--This section of the code is responsible for adjusting the color and applying shaders to the background image based on the current level.
		--The first conditional statement checks if the currentbck variable is not equal to false. 
			
			if not currentbck==false and shader1type=="bluenoise" then
				-- Set the dithering shader
				love.graphics.setShader(blueNoiseDitherShader)
				-- Pass the intensity value to the shader
				love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)	-- Render the background inside the blue noise dithering shader
				-- Reset the shader
				love.graphics.setShader()blueNoiseDitherShader:send("intensity", ditherIntensity)
			end
			
			if nLevel==1 or nLevel==2 or nLevel==3 or nLevel==4 or Level==5 or nLevel==7 or nLevel==15 or nLevel==24 or nLevel==28 or nLevel==30 or nLevel==31 or nLevel==32 or nLevel==33 or nLevel==35 or nLevel==36 or nLevel==37 or nLevel==37 or nLevel==39 or nLevel==61 or nLevel==71 or nLevel==73 or nLevel==74 or nLevel==77 or nLevel==101 or nLevel==102 then
				love.graphics.setColor(1,1,1,0.15) drawBoids()
			end
			
			if not currentbck==false and not (shader1type=="bluenoise") then
			--If true, it adjusts the color of the background image based on the values of adjustr, adjustg, and adjustb. If all values are equal to 1, the scolor1 variable is set to the color represented by these values.
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor1)		-- adjust colors
				--Otherwise, the scolor1 variable is set to a modified version of its original color, which is then applied to the image using the love.graphics.setColor() function.
				else scolor1={adjustr,adjustg,adjustb,scolor1[4]}
					love.graphics.setColor(scolor1)
				end
				
				--[[The second conditional statement checks the value of nLevel and applies a water warp shader to the background image if the value matches any of the specified levels. The causticsAndNormals and causticsAndNormals variables represent different shaders, and the timer variable is sent to each shader using the send() function. The modified image is then drawn onto a canvas using love.graphics.setCanvas(), and the shader effect is applied using love.graphics.setShader().
				--]]
				if nLevel==8  or nLevel==69 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
						
						love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
				end

				if nLevel==1 or nLevel==2 or nLevel==3 or nLevel==4 or Level==5 or nLevel==7 or nLevel==13 or nLevel==15 or nLevel==17 or nLevel==21 or nLevel==24 or nLevel==25 or nLevel==28 or nLevel==30 or nLevel==31 or nLevel==32 or nLevel==35 or nLevel==36 or nLevel==37 or nLevel==37 or nLevel==39 or nLevel==40 or nLevel==45 or nLevel==46 or nLevel==47 or nLevel==48 or nLevel==49 or nLevel==50 or nLevel==51 or nLevel==61 or nLevel==71 or nLevel==73 or nLevel==74 or nLevel==77 or nLevel==101 or nLevel==102 then
					
					--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer/5)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
						if nLevel == 1 then 
							causticsAndNormals:send("u_normals", level1bckNormal)
							causticsAndNormals:send("u_heightmap", level1bckHeight)
						elseif nLevel == 2 then 
							causticsAndNormals:send("u_normals", level2bckNormals)
							causticsAndNormals:send("u_heightmap", level2bckHeight)
						elseif nLevel == 3 then 
							causticsAndNormals:send("u_normals", level3bckNormals)
							causticsAndNormals:send("u_heightmap", level3bckHeight)
						elseif nLevel == 5 then 
							causticsAndNormals:send("u_normals", level5bckNormals)
							causticsAndNormals:send("u_heightmap", level5bckheightmap)
						else
								causticsAndNormals:send("u_normals", smask) -- empty
								causticsAndNormals:send("u_heightmap", smask) -- empty
						love.graphics.setShader(warpshader)
						warpshader:send("time", timer)
						end
						love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
						
				end
				--[[Additional conditional statements check for specific levels and adjust the color of the background image accordingly.
				 For example, if nLevel is equal to 20 and beat is less than 50, the color of the background image is adjusted based on the value of beat.
				 --]]
				
					if nLevel==20 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/80,beat/20,beat/25,0.85)
						else love.graphics.setColor(scolor1)
						end
					end
					

						
					if nLevel==26 or nLevel==27 or nLevel==53 or nLevel==81 then

						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
	
					end
						
						if nLevel==16 or nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/15,beat/25,beat*5,0.85)
						else love.graphics.setColor(scolor1)
						end
					end
					
						
					if nLevel==38 and timer>39 and timer <136 then love.graphics.setColor(1,1,subbeat*3,0.65) 
					end
					
					if nLevel==52 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(subbeat*3,subbeat*3,subbeat*3,0.2)
							love.graphics.setColor(beat/40,beat/30,beat/40,0.85)
						else love.graphics.setColor(scolor3)
							--water warp shader
								love.graphics.setCanvas(canvas)
								causticsAndNormals:send("time", timer)
								love.graphics.setShader(causticsAndNormals)
								love.graphics.setCanvas()
						end
					end
					
					if nLevel==54 and enginepushed then love.graphics.setColor(subbeat,0,0,1) end
					if nLevel==55 and pipepushed==true then love.graphics.setColor(0.3,0,0,1) end
					--Similarly, if nLevel is equal to 56 and lightswitchon is false, the color of the background image is set to black,
					if nLevel==56 then
							if lightswitchon==false then love.graphics.setColor(0,0,0,1)
						 --and if lightswitchon is true, the color is adjusted based on the value of subbeat.
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(0.6-(1/subbeat/3.2),0.4,0.4,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(0.5-(1/subbeat/3.2),0.3,0.3,1)
						end
					end
					if nLevel==66 then love.graphics.setColor(1,1,1,timer/10) end
						if nLevel==72 and timer<15 then love.graphics.setColor(timer/10,1,1,timer/10)
					elseif nLevel==72 and timer>15 then
						--if timer <15.8 or timer>74 then
							--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
					 end
				if nLevel==70 then love.graphics.setColor(1,1,0,1) end
				if nLevel==78 then love.graphics.setColor(0,1,1,1) end
				--Finally, the love.graphics.draw() function is called to draw the modified background image onto the screen,
				--and the love.graphics.setShader() function is called to disable the shader effect.
				
				love.graphics.draw (currentbck, bckx,bcky,0,expx,expy)
			
					--love.graphics.setShader()
			end
			-- If nLevel matches any of the specified levels, the animatefishs() function is called to animate fish sprites on top of the background image.
					if nLevel==19 or nLevel==20 or nLevel==21 or nLevel==101 then love.graphics.setColor(1,1,1,0.15) drawBoids() --animatefishs() 
					end
			

			--This section of code checks if the variable currentbck3 is not equal to false. If it is not false, then it proceeds to adjust the color using the RGB values specified by adjustr, adjustg, and adjustb.
			if not currentbck3==false then
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor4)		-- adjust colors
				else scolor4={adjustr,adjustg,adjustb,scolor4[4]}
				love.graphics.setColor(scolor4)
				end
				--If nLevel is equal to 20 and beat is greater than 50, a "water warp" shader effect is applied to the canvas and currentbck3 is drawn with some transformations using expx and expy.
				if nLevel==20 then
					if beat>50 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
						love.graphics.draw (currentbck3, bckx,bcky,0,expx,expy)
						--love.graphics.setShader()
					end
		
				--If nLevel is equal to 23 or 61, the same "water warp" shader effect is applied to the canvas and currentbck3 is drawn with transformations.
				elseif nLevel==23 or nLevel==61 then
				--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
						love.graphics.draw (currentbck3, bckx,bcky,0,expx,expy)
						--love.graphics.setShader()
				end
			end	
			
			
			--This section of the code contains various conditions and actions that are executed if the variable currentfrg is not false.
			if not currentfrg==false then
			causticsAndNormals:send("WaveSize", {0.0, 0.0})
			--First, it checks if adjustr, adjustg, and adjustb are all 1, and if so, sets the color to scolor3. Otherwise, it updates the scolor3 variable and sets the color to the new value.
				if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor3)		-- adjust colors
				else scolor3={adjustr,adjustg,adjustb,scolor3[4]}
					love.graphics.setColor(scolor3)
				end
					--Next, if the nLevel variable is equal to 15, 20, or 52 and beat is less than 50, the color is set to a specific value based on beat. Otherwise, the color is set to scolor3.
					if nLevel==15 or nLevel==20 or nLevel==52 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/40,beat/30,beat/40,0.85)
						else love.graphics.setColor(scolor3)
						end
					end
					
						if nLevel == 1 then 
								causticsAndNormals:send("u_normals", level1frgNormal)
								causticsAndNormals:send("u_heightmap", level1frgHeight)
					elseif nLevel == 2 then
								causticsAndNormals:send("u_normals", level2frgNormals)
								causticsAndNormals:send("u_heightmap", level2frgHeight)
					elseif nLevel == 3 then
								causticsAndNormals:send("u_normals", level3frgNormals)
								causticsAndNormals:send("u_heightmap", level3frgHeight)
					elseif nLevel == 5 then
								causticsAndNormals:send("u_normals", level5frgNormals)
								causticsAndNormals:send("u_heightmap", level5frgheightmap)
					else
								causticsAndNormals:send("u_normals", smask) -- empty
								causticsAndNormals:send("u_heightmap", smask) -- empty
					end

					--If nLevel is equal to 33, a water warp shader is applied to the canvas.
					if nLevel==33 then
					--water warp shader
						love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
					end
					
					--If nLevel is equal to 34, and beat is less than 50, the color is set to a specific value based on beat. Otherwise, the color is set to scolor1.
					if nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/10,beat/12,beat*5,0.85)
				
						else love.graphics.setColor(scolor1)
						end
					end
				
				--This first statement checks if the variable nLevel is equal to 55 and the variable pipepushed is true. If both of these conditions are true, then the graphics color is set using the love.graphics.setColor function.
				if nLevel==55 and pipepushed==true then love.graphics.setColor(1/subbeat/3+0.35,0,0,1) end
					--The second if statement checks if the variable nLevel is equal to 56. If it is, then there are three different conditions that are checked based on the variables lightswitchon and lightswitchpushedafterfalling. Depending on the combination of these variables, the graphics color is set using the love.graphics.setColor function.
					if nLevel==56 then
							if lightswitchon==false then love.graphics.setColor(0,0,0,0)
						elseif lightswitchon==true and lightswitchpushedafterfalling==false then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,1) 
						elseif lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.4,0.4,1)
						end
					end
						
				--The third if statement checks if the variable nLevel is equal to 61. If it is, then the variable mybeat is set to the current beat value if it is less than 50. The graphics color is then set based on the value of mybeat using the love.graphics.setColor function. If the beat value is greater than or equal to 50, then the graphics color is set to yellow.
				if nLevel==61 then
					if beat<50 then mybeat=beat
							love.graphics.setColor(beat/20,beat/15,0,0.6)
					else love.graphics.setColor(1,1,0,0.6)
					end
				end
				
				
			
				
				--shake screen when heavy object falls
				--The code then checks the variable screenshake. If it is true, then the currentfrg object is drawn with a randomized offset to simulate screen shake. If it is false, then the currentfrg object is drawn without any offset.
				if screenshake==true then
					if shader1type=="bluenoise" then	-- draw the blue noise shader if enabled
						love.graphics.setShader(blueNoiseDitherShader)		 		-- Set the dithering shader
						blueNoiseDitherShader:send("intensity", ditherIntensity)	-- Pass the intensity value to the shader
						love.graphics.draw (currentfrg, frgx+ math.random(0,2),frgy+ math.random(0,2),0,expx,expy)
					else
						love.graphics.draw (currentfrg, frgx+ math.random(0,2),frgy+ math.random(0,2),0,expx,expy)
					end
				elseif screenshake==false then
					if shader1type=="bluenoise" then	-- draw the blue noise shader if enabled
						love.graphics.setShader(blueNoiseDitherShader)		 		-- Set the dithering shader
						blueNoiseDitherShader:send("intensity", ditherIntensity)	-- Pass the intensity value to the shader
						love.graphics.draw (currentfrg, frgx,frgy,0,expx,expy)
					else
						love.graphics.draw (currentfrg, frgx,frgy,0,expx,expy)
					end
				end
				
				--The next if statement checks if the variable nLevel is equal to 33. If it is, then the shader is reset using the love.graphics.setShader function.
				if nLevel==33 then love.graphics.setShader() end
				
				--The next if statement checks if the variable nLevel is equal to 52. If it is, then the graphics color is set based on the value of subbeat using the love.graphics.setColor function. The currentbck2 object is then drawn with scaling using the love.graphics.draw function.
				if nLevel==52 then
							love.graphics.setColor(subbeat*0.5,subbeat*3,subbeat*0.5,0.2)
							love.graphics.draw (currentbck2, bck2x,bck2y,0,expx,expy)
				end
				
				--The last if statement checks if the variable nLevel is equal to 54 and the variable enginepushed is true. If both of these conditions are true, then the currentfrg object is drawn with a randomized offset and the graphics color is set using the love.graphics.setColor function.
					if nLevel==54 and enginepushed then
						love.graphics.setColor(1/subbeat/3,0,0,1)
						love.graphics.draw (currentfrg, frgx+math.random(0,2),frgy+math.random(0,2),0,expx,expy)
					end
			end
end

--This section of the code starts with an if statement that checks if the palette variable is not equal to 1. If that condition is true, it checks another if statement that verifies if the shader1type variable is not equal to "godsgrid".
if not (palette==1) then
				if not (shader1type=="godsgrid") then
				--Inside that block, there is another if statement that checks if the adjustr, adjustg, and adjustb variables are equal to 1. If that condition is true, the color is set to (0.146, 0.73, 0.73) to adjust the colors. Otherwise, the scolor1, scolor2, scolor3, and scolor4 variables are updated with the new values for adjustr, adjustg, and adjustb.
					if adjustr==1 and adjustg==1 and adjustb==1 then 	love.graphics.setColor(0.146, 0.73, 0.73)		-- adjust colors
					else
						scolor1={adjustr,adjustg,adjustb,scolor1[4]}
						scolor2={adjustr,adjustg,adjustb,scolor2[4]}
						scolor3={adjustr,adjustg,adjustb,scolor3[4]}
						scolor4={adjustr,adjustg,adjustb,scolor4[4]}
					end
					--Finally, there is another if statement that checks the value of the res variable and draws a rectangle using the love.graphics.rectangle() function with the appropriate width and height.	
						if res=="720p" then love.graphics.rectangle('fill', 000, 0,1366, 768)
					elseif res=="1080p" then love.graphics.rectangle('fill', 000, 0,1920, 1080)
					elseif res=="1440p" then love.graphics.rectangle('fill', 000, 0,2560, 1440)
					end
				end
--The code ends with two function calls to pb:drawMap() and pb:drawBlocks() which likely draw the map and the blocks respectively.
--pb:drawMap ()
pb:drawBlocks ()

elseif palette==1 then
	--pb:drawMap () --draw map when in remake mode to draw specific grids and blocks like exit areas
end

--The code first checks if the variable gamestatus is equal to "game", indicating that the game is currently being played. If it is, the function pb:drawBlocks() is called to draw the blocks on the screen.
if gamestatus=="game" then pb:drawBlocks () end

-- the function optionmessagesfunction() is called
--optionmessagesfunction()	-- levels/assignfrgbckclassic.lua

--this function, it checks if the variable playerscells is equal to "yes" and gamestatus is equal to "game". If both conditions are true, the function pb:drawAgents() is called to draw the player's cells on the screen.
    if playerscells=="yes" and gamestatus=="game" then
		pb:drawAgents ()
    end
    
    --After that, the function showhelp() is called, which presumably displays some kind of in-game help or instructions to the player.
    showhelp()		-- call game(interface/showhelp function
    
    --[[This section of the code contains several conditional statements that control the graphics display based on the current level, colors, and shaders being used.

	The first conditional statement checks if the variable currentfrg2 is not false. If it is not false, it proceeds to the nested conditional statements.
	--]]
		-- Render frg2 inside blue noise dithering shader
			if not currentfrg2==false and (shader1type=="bluenoise") then
					-- Set the dithering shader
					love.graphics.setShader(blueNoiseDitherShader)
					-- Pass the intensity value to the shader
					blueNoiseDitherShader:send("intensity", ditherIntensity)
					-- Render the foreground
					--love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)
					-- Reset the shader
					love.graphics.setShader()
				end
	-- Render frg2
    if not currentfrg2==false and not (shader1type=="bluenoise") then
    causticsAndNormals:send("WaveSize", {0.0, 0.0})
    		--The second conditional statement checks if the color adjustment variables adjustr, adjustg, and adjustb are set to 1. If they are, it sets the color of the graphics to the scolor3 variable. Otherwise, it updates the scolor3 variable with the adjusted values and sets the color of the graphics to the updated scolor3.
    		if adjustr==1 and adjustg==1 and adjustb==1 then love.graphics.setColor(scolor3)		-- adjust colors
				else scolor3={adjustr,adjustg,adjustb,scolor3[4]}
				love.graphics.setColor(scolor3)
				
				end
    			if not currentfrg2==false then
    						
   --The next conditional statements check the current level (nLevel) and set various colors and shaders accordingly. If nLevel is 14, 29, or 81, it sets the causticsAndNormals shader and sends the current timer variable to it. 
				if nLevel==14 or nLevel==29 or nLevel==81 then
				love.graphics.setCanvas(canvas)
						causticsAndNormals:send("time", timer)
						love.graphics.setShader(causticsAndNormals)
						love.graphics.setCanvas()
				end
				
	--If nLevel is 34 or 15, it sets the color of the graphics based on the beat variable and draws the graphics using the currentfrg2, bck2x, bck2y, expx, and expy variables. 
				if nLevel==34 then
						if beat<50 then mybeat=beat
							love.graphics.setColor(beat/20,beat/24,beat*7,0.85)
						else love.graphics.setColor(scolor3)
						end
				elseif nLevel==15 then
					if beat<50 then mybeat=beat
						love.graphics.setColor(beat*10,beat/50,beat/25,0.85)
					else love.graphics.setColor(scolor3)
					end
	-- If nLevel is 54 and enginepushed is not true, it sets the color of the graphics based on the subbeat variable and draws the graphics using the currentfrg2, bck2x, bck2y, expx, and expy variables.				
				elseif nLevel==54 and not (enginepushed) then
						love.graphics.setColor(subbeat*5,subbeat*5,subbeat*3,0.5)
						--love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)
				elseif nLevel==54 and enginepushed then
						love.graphics.setColor(subbeat*5,0,0,1)
						--love.graphics.draw (currentfrg2, bck2x,bck2y,0,expx,expy)
				--elseif nLevel==71 then
						--love.graphics.setColor(subbeat*5-0.2,subbeat-0.2,subbeat*6-0.2,0.1)
				end
			end
			
		--water warp shader
		--Finally, the code sets the water warp shader (causticsAndNormals) and sends the current timer variable to it if nLevel is 15.
			love.graphics.setCanvas(canvas)
			if nLevel==15 then
			causticsAndNormals:send("time", timer)
			love.graphics.setShader(causticsAndNormals)
		-- If nLevel is 1, 11, 34, or 103, it sets the causticsAndNormals shader and sends the current timer variable to it if the palette variable is set to 1. 
			elseif nLevel==1 or nLevel==11 or nLevel==34 or nLevel==103 then
				if palette==1 then
			
					causticsAndNormals:send("time", timer)
					love.graphics.setShader(causticsAndNormals)
				end
			end
			love.graphics.setCanvas()
			if palette==1 then
			--Then, it draws the graphics using the currentfrg2, frgx, frgy, expx, and expy variables.
				--love.graphics.draw (currentfrg2, frgx,frgy,0,expx,expy)
			end
	end
	
	
      
end

--[[This section of code is the beginning of the drawallcontent() function, which is used to draw all of the content in the game. It starts by checking if the background option is set to "yes" and the shader2 option is set to true. If these conditions are met, it calls the pb:drawMap() function to draw the game map.--]]

function drawallcontent()
		

			if background=="yes" and shader2==true then 
			--pb:drawMap () 
			
			--It then checks if the adjustr, adjustg, and adjustb variables are all set to 1. If they are, it sets the color to white. Otherwise, it sets the color to the scolor2 variable.
			if adjustr==1 and adjustg==1 and adjustb==1 then --love.graphics.setColor(1, 1, 1)		-- adjust colors
				else
				love.graphics.setColor(scolor2)
				end
			
			--The function then sets the size that the shaders should draw based on the current level and screen resolution.
			-- If the current level is 1 and the screen resolution is 1080p, it draws the smask graphic with the love.graphics.draw() function using the frgx, frgy, expx, and expy variables. If the screen resolution is 1440p or 4k, it adjusts the size of the smask graphic accordingly.
					if nLevel==1 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx/1.71,expy/1.2)
				elseif nLevel==1 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx/1.712,expy/1.2)
				elseif nLevel==1 and res=="4k" then love.graphics.draw (smask, frgx,frgy,0,expx/1.712,expy/1.2)
				
				elseif nLevel==2 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==2 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy/0.8)
				elseif nLevel==2 and res=="4k" 	  then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				
				elseif nLevel==3 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==4  and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.075/expx,0.2335/expy)
				elseif nLevel==4  and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==4  and res=="4k" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				
				elseif nLevel==5 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.175/expx,0.235/expy)
				elseif nLevel==5 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.468,expy)
				elseif nLevel==6 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.1436/expx,0.2335/expy)
				elseif nLevel==6 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx/1.75,expy)
				
				elseif nLevel==7 then love.graphics.draw (smask, frgx,frgy,0,expx*1.71,expy*1.71)
				elseif nLevel==8 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.138/expx,0.2335/expy)
				elseif nLevel==8 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx/1.82,expy/1.2)
				elseif nLevel==9 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==9 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,0.6)
				elseif nLevel==10 then love.graphics.draw (smask, frgx,frgy,0,expx,expy/1.05)
				elseif nLevel==11 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==11 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==12 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==13 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy-0.1)
				elseif nLevel==13 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==14 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==14 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==15 then love.graphics.draw (smask, frgx,frgy,0,expx,expy/1.05)
				elseif nLevel==16 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==16 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==17 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==17 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==18 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==18 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==19 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.27/expx,0.2335/expy)
				elseif nLevel==19 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==20 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==20 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==21 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==21 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.5/expx,0.4/expy)
				--elseif nLevel==21 and res=="1440p" then love.graphics.draw (smask, 2500,1000,3.15,expx,expy)
				elseif nLevel==22 and res=="1080p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				elseif nLevel==22 and res=="1440p" then love.graphics.draw (smask, 0,0,0,expx,expy)
				elseif nLevel==23 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==23 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==25 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==25 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==26 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.17125/expx,0.2335/expy)
				elseif nLevel==26 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.35/expx,expy)
				elseif nLevel==27 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.17125/expx,0.2335/expy)
				elseif nLevel==27 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.35/expx,expy)
				elseif nLevel==28 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==28 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==29 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==29 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==30 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.1462/expx,0.2335/expy)
				elseif nLevel==30 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.5/expy)
				elseif nLevel==31 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.16/expx,0.2335/expy)
				elseif nLevel==31 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,0.26/expx,0.5/expy)
				elseif nLevel==32 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.17/expx,0.2335/expy)
				elseif nLevel==32 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx/1.6,expy)
				elseif nLevel==33  and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==33  and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==34 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==35 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==36 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==37 then love.graphics.draw (smask, frgx+2800,frgy+1500,600,expx*1,expy*1.5)
				--elseif nLevel==38 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.175/expx,0.2335/expy)
				elseif nLevel==38 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==38 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx*0.96,expy*0.96)
				--elseif nLevel==39 and res=="1440p" then love.graphics.draw (smask, 2/frgx,frgy,0,expx,expy)
				elseif nLevel==39 then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				--elseif nLevel==40 or nLevel==41 or nLevel==42 or nLevel==43 or nLevel==44 then love.graphics.draw (smask, frgx+2500,frgy+1500,600.05,expx*1,expy*1.5)
				
				elseif nLevel==40 and res=="1080p" then love.graphics.draw (smask, frgx+1950,frgy+1050,600.05,expx*1,expy*1.3)
				elseif nLevel==41 and res=="1080p" then love.graphics.draw  (smask, frgx+1950,frgy+1050,600.05,expx*1,expy*1.3)
				elseif nLevel==42 and res=="1080p" then love.graphics.draw  (smask, frgx+1950,frgy+1050,600.05,expx*1,expy*1.3)
				elseif nLevel==43 and res=="1080p" then love.graphics.draw  (smask, frgx+1950,frgy+1050,600.05,expx*1,expy*1.3)
				elseif nLevel==44 and res=="1080p" then love.graphics.draw  (smask, frgx+1950,frgy+1050,600.05,expx*1,expy*1.3)
				
				
				elseif nLevel==40 and res=="1440p" then love.graphics.draw (smask, frgx+2500,frgy+1500,600,expx*1,expy*1.5)
				elseif nLevel==41 and res=="1440p" then love.graphics.draw (smask, frgx+2400,frgy+1500,600,expx*1,expy*1.5)
				elseif nLevel==42 and res=="1440p" then love.graphics.draw (smask, frgx+2400,frgy+1500,600,expx*1,expy*1.5)
				elseif nLevel==43 and res=="1440p" then love.graphics.draw (smask, frgx+2400,frgy+1500,600,expx*1,expy*1.5)
				elseif nLevel==44 and res=="1440p" then love.graphics.draw (smask, frgx+2500,frgy+1500,600,expx*1,expy*1.5)

				elseif nLevel==46 and res=="1080p"  then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==46 and res=="1440p"  then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==52 or nLevel==54 or nLevel==57 or nLevel==58 then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==56 then love.graphics.draw (smask, frgx*0.8,frgy,0,expx,expy)
				--elseif nLevel==53 then love.graphics.draw (smask, frgx,frgy,0,0.297/expx,0.235/expy)
				elseif nLevel==59 then love.graphics.draw (smask, frgx,frgy,0,0.30/expx,0.467/expy)
				elseif nLevel==60 then love.graphics.draw (smask, frgx,frgy,0,0.5/expx,0.467/expy)
				elseif nLevel==61 then love.graphics.draw (smask, frgx,frgy,0,0.35/expx,0.467/expy)
				elseif nLevel==74 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==74 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==75 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==75 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==76 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==76 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==101 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,0.2335/expy)
				elseif nLevel==101 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==102 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,0.25/expx,0.2335/expy)
				elseif nLevel==102 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==103 and res=="1080p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				elseif nLevel==103 and res=="1440p" then love.graphics.draw (smask, frgx,frgy,0,expx,expy)
				
				elseif nLevel==81 then  love.graphics.draw (smask, frgx,frgy,0,0.375/expx,0.4/expy)
				--elseif nLevel==34 then love.graphics.draw (smask, frgx,frgy,0,0.31/expx,0.1125/expy)
				end
				
			-- Draw the canvas
      end

			love.graphics.setColor(edgescolor)
--[[This code is a sequence of conditional statements that determines what to draw on the screen based on the values of certain variables.

The first if statement checks whether threeD is set to true, and if so, it calls the draw3dborders() function.--]]
	 if threeD==true then
			draw3dborders()
			end
		
		--The next if statement checks whether objectscells is set to yes and shader2 is set to false. If both conditions are true, it calls the drawBlocks() function to draw the game objects.
			if objectscells=="yes" and shader2==false then				-- if objects enabled in debug mode draw them
			
			--pb:drawBlocks ()
		
		end
		
		--The following if statement checks whether playerscells is set to yes and shader2 is set to false. If both conditions are true, it calls the drawAgents() function to draw the players.
		--if playerscells=="yes" and shader2==false then				-- if players enabled in debug mode draw them
			--pb:drawAgents ()
		--end
		
		--The fourth if statement checks whether debugmode is set to yes, and if so, it calls the drawBackgroundGrid() function to draw the background grid.
		if debugmode=="yes" then 									-- If debug mode enabled draw
		pb:drawBackgroundGrid()

			
		end	
		
    --The last if statement checks whether debugmode is set to yes and livereloaded is set to yes. If both conditions are true, it draws a message on the screen indicating that the main code has been reloaded and instructing the user to press the "r" key to close the message.
    if debugmode=="yes" and livereloaded=="yes" then				--If livemode is enabled you can update code by saving and click key -r-
			love.graphics.draw(reload,130,550)
			love.graphics.print("main.lua reloaded, press -r- to close this info",180,560)
	end
	
end

--[[The code defines a function called drawforretro() which is used to draw the game content when running in "retro" mode. The function starts by calling the drawMap() function of the pb object to draw the map.--]]
function drawforretro()
		--pb:drawMap () 

--Then, it checks if the color adjustment values (adjustr, adjustg, and adjustb) are set to 1. If so, it does not apply any color adjustment, otherwise, it sets the color to scolor2.
		if adjustr==1 and adjustg==1 and adjustb==1 then --love.graphics.setColor(1, 1, 1)		-- adjust colors
		else
		love.graphics.setColor(scolor2)
		end
		
	--The code then checks if 3D mode is enabled (threeD == true). If so, it calls the draw3dborders() function to draw the 3D borders.	
	 if threeD==true then
			draw3dborders()
			end
	--Next, it checks if objectscells is set to "yes" and if the shader is disabled (shader2 == false). If so, it calls the pb:drawBlocks() function to draw the objects in debug mode
			if objectscells=="yes" and shader2==false then				-- if objects enabled in debug mode draw them
				pb:drawBlocks ()
			end
		
		--It then checks if playerscells is set to "yes" and if the shader is disabled (shader2 == false). If so, it calls the pb:drawAgents() function to draw the players in debug mode.
		if playerscells=="yes" and shader2==false then				-- if players enabled in debug mode draw them
			pb:drawAgents ()
		end
		
		--If debugmode is enabled, the code calls the pb:drawBackgroundGrid() function to draw the background grid.
		if debugmode=="yes" then 									-- If debug mode enabled draw
		pb:drawBackgroundGrid()

			
		end	
		
    --Finally, if debugmode and livereloaded are both enabled, the code draws a reload icon and a message indicating that main.lua has been reloaded, and instructs the user to press the "r" key to close this message.
    if debugmode=="yes" and livereloaded=="yes" then				--If livemode is enabled you can update code by saving and click key -r-
			love.graphics.draw(reload,130,550)
			love.graphics.print("main.lua reloaded, press -r- to close this info",180,560)
	end
end

