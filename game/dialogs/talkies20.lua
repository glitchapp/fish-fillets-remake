function Obey.lev20()

  
  captiveavatar = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level20/380avatarcross.webp")))
    
  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  else
  loadcorrespondentfonts()
  end

 Talkies.say( "Big fish",
    {
      l20[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "small fish",
    { 
       l20[2],
       l20[3],
       l20[4],
       l20[5],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    {
      l20[6],
      l20[7],
      l20[8],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
  Talkies.say( "small fish",
    { 
       l20[9],
       l20[10],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    {
      l20[11],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    { 
       l20[12],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    Talkies.say( "Captive",
    {
      l20[13],
      l20[14],
      l20[15],
      l20[16],
      l20[17],
    },
    {
      image=captiveavatar,
      talkSound=blop,
      typedNotTalked=true,
    })

end

function Obey.lev20skull()

Talkies.say( "skull",
    {
		l20skull[1],
		l20skull[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
