function Obey.lev15()

  
  snailavatar= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level15/snailavatar.webp")))
  snailavatar2= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level15/snailavatar2.webp")))
  clouseau = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level15/clouseau.webp")))
  roundfish= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level15/roundfish.webp")))
  
  loadcorrespondentfonts()

 Talkies.say( "small fish",
    {
      	l15[1],
      	l15[2],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })


   Talkies.say( "Big fish",
    { 
    	l15[3],
      	l15[4],
      	l15[5],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })    


 Talkies.say( "small fish",
    {
		l15[6],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
 Talkies.say( "Snail",
    { 
    	l15[7],
    	l15[8],
    	l15[9],
    	l15[10],
    	l15[11],
    	l15[12],
    	l15[13],
    },
    {
      image=snailavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
 Talkies.say( "Snail Escargot",
    {
        l15[14],
		l15[15],
		l15[16],
		l15[17],
		l15[18],
		l15[19],
    },
    {
      image=snailavatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Inspector Clouseau",
    { 
    	l15[20],
    },
    {
      image=clouseau,
      talkSound=blop,
      typedNotTalked=true,
    })

	Talkies.say( "Answer machine",
    {
		l15[21],
    },
    {
      image=roundfish,
      talkSound=blop,
      typedNotTalked=true,
    })
    
       Talkies.say( "Big fish",
    { 
    	l15[22],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })   
 
end
