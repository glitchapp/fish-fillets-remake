function Obey.lev24()

  
  	  if skin=="remake" then statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level21/statue.webp")))
  elseif skin=="classic" then statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/level21/objects/upscaled/statue.webp")))
  end
  
  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  else
  loadcorrespondentfonts()
  end

 Talkies.say( "Statue",
    { 
       l24[1],
       l24[2],
       l24[3],
       l24[4],
       l24[5],
       l24[6],
       l24[7],
       l24[8],
       l24[9],
       l24[10],
       l24[11],
       l24[12],
       l24[13],
       l24[14],
    },
    {
      image=statue,
      talkSound=blop,
      typedNotTalked=true,
    })

 Talkies.say( "small fish",
    {
      l24[15],
      l24[16],
      l24[17],
      l24[18],
      l24[19],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
       l24[20],
       l24[21],
       l24[22],
       l24[23],
       l24[24],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
   
end
