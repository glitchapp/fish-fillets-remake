borescript_en={"I don’t believe we can make it…",
"We aren’t going to get out of here.",
"",
"We are shut in here forever!",
"There must be a way out!",
"There is a solution, just we cannot see it.",
"Chin up player, there must be a way.",
"Take it easy."	,
"There is something strange about it…",
"Just spare me your theories, please.",
"I’ve got it. If we push that one…"	,
"Go on!",
"Oh no, it was just an idea…",
"What if we try that…",
"What?",
"Nothing. Just an idea.",
"Not again.",
"I’m tired of this.",
"Just play, we want to get out.",
"If the player keeps doing nothing, we’ll have to solve it ourselves.",
"Shhh, don’t disturb the player. He or she might be thinking again…",
"Keep calm.",
"Take it easy.",
"Will there be some action soon?",
"Why aren’t we doing anything?",
"Why are we idle?",
"Does he expect us to solve this on our own?",
"Don’t be disruptive. The player is thinking again…",
"Is there a player there at all?",
"I don’t know. The screen glares too much from inside.",
"This little break will help.",
"We are tired of waiting!",
"Hello!",
"Did you flinch?",
"We were just afraid that you might have forgotten about us.",
"Look, why don’t you move us a little bit?",
"We are the game, you are supposed to solve the puzzle!",
"Buy yourself a fish tank for observations.",
"Hellooo! We are here! Did you forget about us?",
"Difficult, isn’t it?",
"Look player, couldn’t you bring us something to eat? Put it in the TEMP directory, we’ll eat it immediately.",
"When you stop playing, don’t turn off the computer, please. It’s so dark and scary when you do.",
"How long are we going to take to solve it?",
"As long as this chewing gum tastes good.",
"What, you are finished? But this chewing gum still tastes extraordinary.",
"What do you think, how are the others faring?",
"The other who?",
"The other players who are solving this game. Maybe they have managed to solve more levels.",
"It’s very likely.",
"Oh, yeah.",
"What happened?",
"I just feel so sorry…",
"Sorry for what?",
"Sorry for everything.",
"What everything?",
"Completely everything! And leave me alone, I beg you!"
}

borescript_es={"No creo que podamos hacerlo...",
"No vamos a salir de aquí.",
"¡Estamos encerrados aquí adentro para siempre!",
"¡Debe de haber alguna salida!",
"Hay una solución, solo que no la podemos ver.",
"Levanta la cabeza, debe de haber un camino.",
"Tómalo con calma.",
"Hay algo extraño acerca de eso...",
"Sólo pon a mi disposición tus teorías, por favor.",
"Lo tengo. Si empujamos ese...",
"¡Vamos!",
"Oh no, fué sólo una idea...",
"Que tal si intentamos con ese...",
"¿Qué?",
"Nada. Sólo una idea.",
"No de nuevo.",
"Me cansé de esto.",
"Sólo juega, queremos salir.",
"Si el jugador sigue sin hacer nada, vamos a tener que resolverlo nosotros mismos.",
"Shht, no molestes al jugador. Deberá de estar pensando de nuevo...",
"Mantén la calma.",
"Tómalo con calma.",
"¿Habrá algo de acción pronto?",
"¿Porqué no estamos haciendo nada?",
"¿Porqué estamos ociosos?",
"¿Espera el que lo resolvamos nosotros mismos?",
"No interrumpas. El jugador está pensando de nuevo...",
"¿Hay un jugador ahí después de todo?",
"No lo sé. La pantalla brilla demasiado desde adentro.",
"Esta pequeña pausa ayudará.",
"¡Estamos cansados de esperar!",
"¡Hola!",
"¿Te rendiste?",
"Sólo estamos preocupados de que te hayas olvidado de nosotros.",
"Mira, ¿Porqué no nos mueves un poquito?",
"Nosotros somos el juego, ¡Se supone que tú resuelvas el puzzle!",
"Compra un tanque para peces para tus observaciones.",
"¡Holaaa! ¡Estamos aquí! ¿Te olvidaste de nosotros?",
"Es difícil, ¿Cierto?",
"Mira jugador, ¿Podrías traernos algo para comer? Ponlo en el directorio TEMP, lo vamos a comer ahora mismo.",
"Cuando dejes de jugar, no apagues el computador, por favor. Es tan oscuro y da miedo aquí cuando lo haces.",
"¿Cuanto nos vamos a tardar en resolverlo?",
"Tanto como cuanto este chicle tenga buen sabor.",
"What, you are finished? But this chewing gum still tastes extraordinary.",
"Que piensas, ¿Cómo les está yendo a los otros?",
"¿Los otros qué?",
"Los otros jugadores que están resolviendo este juego. Quizás ellos consiguieron resolver más niveles.",
"Es muy probable.",
"Oh, si.",
"¿Qué pasó?",
"Lo siento tanto...",
"¿Lo sientes por qué?",
"Lo siento por todo.",
"¿Qué vendría siendo todo?",
"¡Completamente todo! ¡Y déjame sola, te lo ruego!",
}

borescript_fr={
"Je ne crois pas que nous y arriverons...",
"Nous ne sortirons pas d'ici.",
"Nous sommes enfermés ici pour l'éternité.",
"Il doit y avoir un moyen de sortir!",
"Il y a une solution, nous ne pouvons simplement pas la voir.",
"Ressaisis-toi, il doit y avoir une solution.",
"Détends-toi.",
"Il y a quelque chose d'étrange...",
"Épargne moi tes théories, s'il te plaît.",
"Euréka. Si nous poussons celui là...",
"Allons-y!",
"Non, c'était juste une idée...",
"Et si on essayait ça...",
"Quoi ?",
"Rien. Juste une idée.",
"Arrête.",
"Ça me fatigue.",
"Allez! Joue, nous voulons sortir.",
"Si le joueur(la joueuse) ne fait rien, nous allons devoir trouver la solution nous-même.",
"Chut, ne dérange pas le joueur(la joueuse). Il réfléchit encore...",
"Reste calme.",
"Détends-toi.",
"Est-ce qu'on va bientôt agir ?",
"Pourquoi ne faisons-nous rien ?",
"Pourquoi attendons-nous ?",
"Espère-t-il que nous trouverons nous-même la solution ?",
"Dérange pas. Le joueur réfléchit à nouveau...",
"Y-a-t-il seulement un joueur ?",
"Je ne sais pas. L'écran brille trop de l'intérieur.",
"Une petite pause peut aider.",
"Nous sommes fatigués d'attendre !",
"Salut!",
"Tu as reculé ?",
"Nous avions simplement peur que tu nous aies oubliés.",
"Écoute, pourquoi est-ce que tu ne nous bouges pas un petit peu ?",
"Nous sommes le jeu, tu es censé le résoudre.",
"Achète un bocal pour faire des observations.",
"Hellooo ! Nous sommes ici ! Nous as-tu oubliés ?",
"C'est dur, hein ?",
"Attends joueur(se), tu ne peux pas nous apporter quelque chose à manger ? Mets-le dans le répertoire temporaire, nous le mangerons vite.",
"Quand tu arrêtes de jouer, n'éteins pas l'ordinateur, s'il te plaît. C'est si noir et effrayant quand tu éteins.",
"Combien de temps il nous faudra pour trouver la solution ?",
"Aussi longtemps que ce chewing-gum a du goût.",
"Quoi, tu as fini ? Mais ce chewing-gum a encore un goût extraordinaire.",
"Dis donc, comment sont les autres fish-filleteurs ?",
"Les autres quoi ?",
"Les autres joueurs qui cogitent sur ce jeu. Peut-être qu'ils ont résolu plus de niveaux.",
"C'est très probable.",
"Oh, ouais.",
"Que se passe-t-il ?",
"Je me sens si navrée...",
"Navrée de quoi ?",
"Navrée de tout.",
"Quoi tout ?",
"Vraiment tout ! Et laisse moi seule, je t'en supplie !"
}

function resizetalkiesfonts()
		if res=="720p" or res=="1080p" 	then	Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
	elseif res=="1440p" 				then	Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 48)
	elseif res=="4k" 				then	Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 64)
	end
end

function loadborescript()
		if language=="en" or language=="chi" then
			if accent=="br" then
				idontbelieve = love.audio.newSource( "/externalassets/dialogs/share/en/bore/1idontbelieve.ogg","stream" )			-- "I don’t believe we can make it…"
				theremustbe = love.audio.newSource( "/externalassets/dialogs/share/en/bore/4theremustbe.ogg","stream" )
				thereis = love.audio.newSource( "/externalassets/dialogs/share/en/bore/5thereis.ogg","stream" )
				chinup = love.audio.newSource( "/externalassets/dialogs/share/en/bore/6chinup.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/en/bore/7takeiteasy.ogg","stream" )
				theresomething = love.audio.newSource( "/externalassets/dialogs/share/en/bore/8theresomething.ogg","stream" )
				ivegotit = love.audio.newSource( "/externalassets/dialogs/share/en/bore/10ivegotit.ogg","stream" )
				ohnoitwas = love.audio.newSource( "/externalassets/dialogs/share/en/bore/12ohno.ogg","stream" )
				whatifwetrythat = love.audio.newSource( "/externalassets/dialogs/share/en/bore/13whatifwetrythat.ogg","stream" )
				nothingjustanidea = love.audio.newSource( "/externalassets/dialogs/share/en/bore/15nothingjustanidea.ogg","stream" )
				imtired = love.audio.newSource( "/externalassets/dialogs/share/en/bore/17imtired.ogg","stream" )
				justplay = love.audio.newSource( "/externalassets/dialogs/share/en/bore/18justplay.ogg","stream" )
				iftheplayer = love.audio.newSource( "/externalassets/dialogs/share/en/bore/19iftheplayer.ogg","stream" )
				dontbedisruptive = love.audio.newSource( "/externalassets/dialogs/share/en/bore/27dontbedisruptive.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/share/en/bore/29idontknow.ogg","stream" )
				thislittlebreak = love.audio.newSource( "/externalassets/dialogs/share/en/bore/30thislittle.ogg","stream" )
				didyouflinch = love.audio.newSource( "/externalassets/dialogs/share/en/bore/33didyouflinch.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/share/en/bore/35look.ogg","stream" )
				buyyourself = love.audio.newSource( "/externalassets/dialogs/share/en/bore/37buyyourself.ogg","stream" )
				hellowearehere = love.audio.newSource( "/externalassets/dialogs/share/en/bore/38hellowearehere.ogg","stream" )
				lookplayer = love.audio.newSource( "/externalassets/dialogs/share/en/bore/40lookplayer.ogg","stream" )
				aslongas = love.audio.newSource( "/externalassets/dialogs/share/en/bore/43aslongas.ogg","stream" )
				whatdoyouthink = love.audio.newSource( "/externalassets/dialogs/share/en/bore/45whatdoyouthink.ogg","stream" )
				theotherplayers = love.audio.newSource( "/externalassets/dialogs/share/en/bore/47theotherplayers.ogg","stream" )
				whathappened = love.audio.newSource( "/externalassets/dialogs/share/en/bore/50whathappened.ogg","stream" )
				sorryforwhat = love.audio.newSource( "/externalassets/dialogs/share/en/bore/52sorryforwhat.ogg","stream" )
				whateverything = love.audio.newSource( "/externalassets/dialogs/share/en/bore/54whateverything.ogg","stream" )
			elseif accent=="us" then
				idontbelieve = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/1idontbelieve.ogg","stream" )			-- "I don’t believe we can make it…"
				theremustbe = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/4theremustbe.ogg","stream" )
				thereis = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/5thereis.ogg","stream" )
				chinup = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/6chinup.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/7takeiteasy.ogg","stream" )
				theresomething = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/8theresomething.ogg","stream" )
				ivegotit = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/10ivegotit.ogg","stream" )
				ohnoitwas = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/12ohno.ogg","stream" )
				whatifwetrythat = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/13whatifwetrythat.ogg","stream" )
				nothingjustanidea = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/15nothingjustanidea.ogg","stream" )
				imtired = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/17imtired.ogg","stream" )
				justplay = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/18justplay.ogg","stream" )
				iftheplayer = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/19iftheplayer.ogg","stream" )
				dontbedisruptive = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/27dontbedisruptive.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/29idontknow.ogg","stream" )
				thislittlebreak = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/30thislittle.ogg","stream" )
				didyouflinch = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/33didyouflinch.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/35look.ogg","stream" )
				buyyourself = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/37buyyourself.ogg","stream" )
				hellowearehere = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/38hellowearehere.ogg","stream" )
				lookplayer = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/40lookplayer.ogg","stream" )
				aslongas = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/43aslongas.ogg","stream" )
				whatdoyouthink = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/45whatdoyouthink.ogg","stream" )
				theotherplayers = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/47theotherplayers.ogg","stream" )
				whathappened = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/50whathappened.ogg","stream" )
				sorryforwhat = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/52sorryforwhat.ogg","stream" )
				whateverything = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/54whateverything.ogg","stream" )
			
			end
		
		
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
				idontbelieve = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/1idontbelieve.ogg","stream" )			-- "I don’t believe we can make it…"
				theremustbe = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/4theremustbe.ogg","stream" )
				thereis = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/5thereis.ogg","stream" )
				chinup = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/6chinup.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/7takeiteasy.ogg","stream" )
				theresomething = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/8theresomething.ogg","stream" )
				ivegotit = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/10ivegotit.ogg","stream" )
				ohnoitwas = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/12ohno.ogg","stream" )
				whatifwetrythat = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/13whatifwetrythat.ogg","stream" )
				nothingjustanidea = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/15nothingjustanidea.ogg","stream" )
				imtired = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/17imtired.ogg","stream" )
				justplay = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/18justplay.ogg","stream" )
				iftheplayer = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/19iftheplayer.ogg","stream" )
				dontbedisruptive = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/27dontbedisruptive.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/29idontknow.ogg","stream" )
				thislittlebreak = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/30thislittle.ogg","stream" )
				didyouflinch = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/33didyouflinch.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/35look.ogg","stream" )
				buyyourself = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/37buyyourself.ogg","stream" )
				hellowearehere = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/38hellowearehere.ogg","stream" )
				lookplayer = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/40lookplayer.ogg","stream" )
				aslongas = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/43aslongas.ogg","stream" )
				whatdoyouthink = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/45whatdoyouthink.ogg","stream" )
				theotherplayers = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/47theotherplayers.ogg","stream" )
				whathappened = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/50whathappened.ogg","stream" )
				sorryforwhat = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/52sorryforwhat.ogg","stream" )
				whateverything = love.audio.newSource( "/externalassets/dialogs/share/es-la/bore/54whateverything.ogg","stream" )
			end
		
		elseif language=="fr" then
				idontbelieve = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/1idontbelieve.ogg","stream" )			-- "I don’t believe we can make it…"
				theremustbe = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/4theremustbe.ogg","stream" )
				thereis = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/5thereis.ogg","stream" )
				chinup = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/6chinup.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/7takeiteasy.ogg","stream" )
				theresomething = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/8theresomething.ogg","stream" )
				ivegotit = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/10ivegotit.ogg","stream" )
				ohnoitwas = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/12ohno.ogg","stream" )
				whatifwetrythat = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/13whatifwetrythat.ogg","stream" )
				nothingjustanidea = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/15nothingjustanidea.ogg","stream" )
				imtired = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/17imtired.ogg","stream" )
				justplay = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/18justplay.ogg","stream" )
				iftheplayer = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/19iftheplayer.ogg","stream" )
				dontbedisruptive = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/27dontbedisruptive.ogg","stream" )
				idontknow = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/29idontknow.ogg","stream" )
				thislittlebreak = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/30thislittle.ogg","stream" )
				didyouflinch = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/33didyouflinch.ogg","stream" )
				look = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/35look.ogg","stream" )
				buyyourself = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/37buyyourself.ogg","stream" )
				hellowearehere = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/38hellowearehere.ogg","stream" )
				lookplayer = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/40lookplayer.ogg","stream" )
				aslongas = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/43aslongas.ogg","stream" )
				whatdoyouthink = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/45whatdoyouthink.ogg","stream" )
				theotherplayers = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/47theotherplayers.ogg","stream" )
				whathappened = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/50whathappened.ogg","stream" )
				sorryforwhat = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/52sorryforwhat.ogg","stream" )
				whateverything = love.audio.newSource( "/externalassets/dialogs/share/fr/bore/54whateverything.ogg","stream" )
		end
		if language2=="en" then
				if accent2=="br" then
			elseif accent2=="us" then
				
				wearent = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/2wearent.ogg","stream" )
				weareshut = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/3weareshut.ogg","stream" )
				justspareme = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/9justspareme.ogg","stream" )
				goon = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/11goon.ogg","stream" )
				what = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/14what.ogg","stream" )
				notagain = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/16notagain.ogg","stream" )
				shhhdont = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/20shh.ogg","stream" )
				keepcalm = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/21keepcalm.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/22takeiteasy.ogg","stream" )
				willtherebe = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/23willtherebe.ogg","stream" )
				whyarentwe = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/24whyarentwe.ogg","stream" )
				whyareweidle = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/25whyarewe.ogg","stream" )
				doesheexpect = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/26doesheexpect.ogg","stream" )
				isthereaplayer = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/28isthereaplayer.ogg","stream" )
				wearetired2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/31wearetired2.ogg","stream" )
				hello2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/32hello2.ogg","stream" )
				wewerejust = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/34wewerejust.ogg","stream" )
				wearethegame = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/36wearethegame.ogg","stream" )
				difficultisntit = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/39difficultisntit.ogg","stream" )
				whenyoustopplaying = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/41whenyoustopplaying.ogg","stream" )
				howlongare = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/42howlong.ogg","stream" )
				whatyouarefinished = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/44whatyouarefinished.ogg","stream" )
				theotherswho = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/46theotherwho.ogg","stream" )
				itsverylikely = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/48itsverylikely.ogg","stream" )
				ohyeah = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/49ohyeah.ogg","stream" )
				ijustfeelsosorry = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/51ijustfeel.ogg","stream" )
				sorryforeverything = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/53sorryforeverything.ogg","stream" )
				completelyeverything = love.audio.newSource( "/externalassets/dialogs/share/en-us/bore/55completelyeverything.ogg","stream" )
				
			
			end
		elseif language2=="pl" then
				wearent = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/2wearent.ogg","stream" )
				weareshut = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/3weareshut.ogg","stream" )
				justspareme = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/9justspareme.ogg","stream" )
				goon = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/11goon.ogg","stream" )
				what = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/14what.ogg","stream" )
				notagain = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/16notagain.ogg","stream" )
				shhhdont = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/20shh.ogg","stream" )
				keepcalm = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/21keepcalm.ogg","stream" )
				takeiteasy = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/22takeiteasy.ogg","stream" )
				willtherebe = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/23willtherebe.ogg","stream" )
				whyarentwe = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/24whyarentwe.ogg","stream" )
				whyareweidle = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/25whyarewe.ogg","stream" )
				doesheexpect = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/26doesheexpect.ogg","stream" )
				isthereaplayer = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/28isthereaplayer.ogg","stream" )
				wearetired2 = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/31wearetired2.ogg","stream" )
				hello2 = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/32hello2.ogg","stream" )
				wewerejust = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/34wewerejust.ogg","stream" )
				wearethegame = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/36wearethegame.ogg","stream" )
				difficultisntit = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/39difficultisntit.ogg","stream" )
				whenyoustopplaying = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/41whenyoustopplaying.ogg","stream" )
				howlongare = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/42howlong.ogg","stream" )
				whatyouarefinished = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/44whatyouarefinished.ogg","stream" )
				theotherswho = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/46theotherwho.ogg","stream" )
				itsverylikely = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/48itsverylikely.ogg","stream" )
				ohyeah = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/49ohyeah.ogg","stream" )
				ijustfeelsosorry = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/51ijustfeel.ogg","stream" )
				sorryforeverything = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/53sorryforeverything.ogg","stream" )
				completelyeverything = love.audio.newSource( "/externalassets/dialogs/share/pl/bore/55completelyeverything.ogg","stream" )
		end
			
				
				--fish 1
				idontbelieve:setEffect('myEffect')
				theremustbe:setEffect('myEffect')
				thereis:setEffect('myEffect')
				chinup:setEffect('myEffect')
				takeiteasy:setEffect('myEffect')
				theresomething:setEffect('myEffect')
				ivegotit:setEffect('myEffect')
				ohnoitwas:setEffect('myEffect')
				whatifwetrythat:setEffect('myEffect')
				nothingjustanidea:setEffect('myEffect')
				imtired:setEffect('myEffect')
				justplay:setEffect('myEffect')
				iftheplayer:setEffect('myEffect')
				dontbedisruptive:setEffect('myEffect')
				idontknow:setEffect('myEffect')
				thislittlebreak:setEffect('myEffect')
				didyouflinch:setEffect('myEffect')
				look:setEffect('myEffect')
				buyyourself:setEffect('myEffect')
				hellowearehere:setEffect('myEffect')
				lookplayer:setEffect('myEffect')
				aslongas:setEffect('myEffect')
				whatdoyouthink:setEffect('myEffect')
				theotherplayers:setEffect('myEffect')
				whathappened:setEffect('myEffect')
				sorryforwhat:setEffect('myEffect')
				whateverything:setEffect('myEffect')
				
				--fish 2
				wearent:setEffect('myEffect')
				weareshut:setEffect('myEffect')
				justspareme:setEffect('myEffect')
				goon:setEffect('myEffect')
				what:setEffect('myEffect')
				notagain:setEffect('myEffect')
				shhhdont:setEffect('myEffect')
				keepcalm:setEffect('myEffect')
				takeiteasy:setEffect('myEffect')
				willtherebe:setEffect('myEffect')
				whyarentwe:setEffect('myEffect')
				whyareweidle:setEffect('myEffect')
				doesheexpect:setEffect('myEffect')
				wearetired2:setEffect('myEffect')
				hello2:setEffect('myEffect')
				wewerejust:setEffect('myEffect')
				wearethegame:setEffect('myEffect')
				difficultisntit:setEffect('myEffect')
				whenyoustopplaying:setEffect('myEffect')
				howlongare:setEffect('myEffect')
				whatyouarefinished:setEffect('myEffect')
				theotherswho:setEffect('myEffect')
				itsverylikely:setEffect('myEffect')
				ohyeah:setEffect('myEffect')
				ijustfeelsosorry:setEffect('myEffect')
				sorryforeverything:setEffect('myEffect')
				completelyeverything:setEffect('myEffect')
end


function Obey.borescript()
  avatar = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("lib/talkies/example/assets/fishtalk.webp")))
  avatar2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("lib/talkies/example/assets/fishtalk2.webp")))
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	borescript[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	borescript[2],
      	borescript[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })


   Talkies.say( "Big fish",
    { 
    	borescript[4],
    	borescript[5],
    	borescript[6],
    	borescript[7],
    	borescript[8],      	
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
    
	Talkies.say( "small fish",
    {
      	borescript[9],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
   Talkies.say( "Big fish",
    { 
    	borescript[10], 	
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
    
    Talkies.say( "small fish",
    {
      	borescript[11],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[12],
    	borescript[13],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[14],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[15],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[16],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[17],
    	borescript[18],
    	borescript[19],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[20],
      	borescript[21],
      	borescript[22],
      	borescript[23],
      	borescript[24],
      	borescript[25],
      	borescript[26],
      	
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[27],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[28],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[29],
    	borescript[30],
    	borescript[31],
    	borescript[32],
    	borescript[33],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[34],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[35],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[36],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
end

