l47={
"Siehst du? Dieses Schiff hatte zwei Kanonendecks. Es muss ein Kriegsschiff gewesen sein. Der beißende Geruch von Schießpulver scheint noch in der Luft zu liegen...",
"Du meinst \"im Wasser\".",
"Wessen Schiff war das? Vielleicht diente es unter Admiral Nelson... oder Kapitän Hornblower.",
"Nein. Ich glaube, du liegst ungefähr einhundert Jahre daneben.",
"Ich hab das Gefühl, wir werden das Schwert brauchen.",
"Du und deine \"Gefühle\". Außerdem sollen wir keine Tipps geben.",
"Das war ein bisschen zu einfach, oder?",
"Kann es wirklich so einfach sein?",
"Da muss irgendwo ein Fehler sein.",
}
