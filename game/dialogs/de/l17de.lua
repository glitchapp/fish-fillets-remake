l17={
"Dieses Schiff muss vor langer Zeit gesunken sein.",
"Definitiv. Es ist eine der ersten Drakkars. Nach der Form des Kopfes, der Anzahl der Spiralen und der Farbe des Wassers nach zu urteilen, gehörte es dem Großvater von Erik dem Roten, denke ich.",
"Oder dem Schwiegervater von Olaf dem Tapferen.",
"Oder vielleicht dem Großonkel von Leif dem Geschickten.",
"Es könnte auch der Nichte von Harold dem Großen gehören.",
"Oder möglicherweise der Tante von Snorr dem Mutigen.",
"Es ist sogar möglich, dass es Thorson dem Harten gehörte.",
"Ich dachte immer ich bin das Fischerboot Eric des Unfertigen... Aber heute habe ich die Wahrheit erfahren. Ja, es ist nie zu spät, ein neues Leben anzufangen!",
"Schau dir den armen Hund an.",
"Scheint er nicht ein wenig komisch zu atmen?",
"Für einen Hund macht er sich unter Wasser nicht schlecht.",
"Er sieht gar nicht so schlecht aus, wenn man bedenkt, dass er hier schon ein paar Jahrhunderte ist.",
"Halt den Mund!",
"Wirst du wohl endlich ruhig sein!",
"So, du willst deine Klappe nicht halten, oder?",
"Aaaaargh...",
"He-he-he-he-he... Eh-eh-eh-eh...",
"Ist es vorbei?",
"Sind wir jetzt sicher?",
"Ist der Kampf vorbei?",
"Ist der Feind endlich weg?",
"Kämpfen wir noch?",
"Chef, sind wir schon in Walhalla?",
"Wann kommen wir in Walhalla an?",
"Ich lasse es euch wissen, wenn wir da sind.",
"Chef, kann ich mich mal erleichtern gehen?",
"Warte, bis wir in Walhalla sind.",
"Dauert diese Reise nach Walhalla nicht ein bisschen zu lange?",
"Ein echter Kämpfer hat Geduld.",
"Bist du sicher, dass man so nach Walhalla kommt?",
"Ein Kämpfer muss seinem Kommandanten vertrauen!",
"Ahhh, Ich habe das Kommando über ein Schiff voller Feiglinge!",
"Warum musste ich ein Schiff voller lachender Blödmänner bekommen?",
"Lachende Blödmänner, ein sabbernder Hund. Ich glaub ich geh mich ertränken.",
"Echte Wikinger haben Bärte.",
"Z-z-zöpfe sind in heu-heutzutage.",
"Was ist das denn für ein Quatsch? Zöpfe? Herrgottnochmal!",
"D-d-das Wikinger M-m-modemagazin empfiehlt ei-einen blonden Zopf a-als Ergänzung zu einem d-dunklem Helm und einem bb-blauen Schild.",
"Den Quatsch mach ich einfach nicht mit.",
"Du solltest dich lieber an die Wikinger-Traditionen halten.",
"A-a-aber ich bin c-c-cool!",
"Sogar Erik der G-g-große hatte ein Z-z-zopf!",
"Blödsinn. Er hatte einen Bart.",
"Aber er ha-hatte auch einen Z-z-zopf.",
"Hatte er nicht und basta!",
"D-d-denkst du ein B-b-bart würde mir b-b-besser stehen?",
"Definitiv.",
"Ich werd d-d-drüber nachdenken.",
"Hmmm... Ehmmm... Hahmmm...",
"N-nein, Z-z-zöpfe sind definitiv b-b-besser.",
"Die heutige Jugend - absolut unglaublich.",
"Ein Kämpfer mit Zöpfen. Das gabs noch nie.",
"Ihr jungen Burschen denkt wohl, ihr könnt Euch alles erlauben.",
"Na gut, ich sehe immer noch cool aus.",
}
