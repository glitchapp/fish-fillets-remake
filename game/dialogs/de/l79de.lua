l79={
"Der Spieler sollte wissen, dass die Zylinder mit den Autos gleicher Farbe verbunden sind.",
"DURCH EINE UNSICHTBARE KRAFT!",
"Jemand spielt mit uns -- die Autos bewegen sich. Oder bist du das? Oder ich? Ich bekomme Angst.",
"Ich frage mich gerade, ob Fish Fillets ein gutes Spiel ist, um diese Ebene zu lösen.",
"Was meinst du damit?",
"Der Spieler sollte es erst woanders lösen und dann hier wiederholen.",
"Im Gegenteil! Wenn der Spieler es nur hier löst, werde ich ihn heimlich bewundern.",
"Wir sollten das rote Auto herausbekommen.",
"Tu nicht so schlau, wenn die anderen es schon sind.",
"Was tust du? Du bist noch nicht draußen.",
}
