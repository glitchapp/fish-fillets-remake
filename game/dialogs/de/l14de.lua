l14={"Was für ein seltsames Schiff ist das denn?",
"Das ist das Wrack des Passagierfluzeuges LC-10 Lemura.",
"Das ist das Wrack des Passagierfluzeuges Atlantobus.",
"Das ist das Wrack des Passagierfluzeuges Poseidon 737.",
"Siehst du das Auge? Ein stiller Zeuge der Tragödie... Jemand traute diesem Flugzeug - und das Glasauge ist alles, was er hinterließ.",
"Das ist kein Glasauge sondern ein Kreisel. Zumindest in dieser Ebene.",
"Sitze. Warum sind hier so viele Sitze?",
"Sei dankbar. Könntest du ohne sie rauskommen?",
}
