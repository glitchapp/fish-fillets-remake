l61={
"Haben wir ihn endlich gefunden.",
"Was?",
"Na Kapitän Silvers Schatz!",
"Na den heiligen Gral!",
"Alles schön auf einem Haufen: der Schatz und der Gral.",
"Ich glaube, das ist zu einfach, um wahr zu sein.",
"Ich denke, wenn das wirklich unser Ziel wäre, würde das Programm es den Spieler wissen lassen.",
"Aber ich habe es ihn wissen lassen. Das könnte reichen.",
"Aber ich habe es angezweifelt. Das könnte ihn durcheinanderbringen.",
"Hmm, na gut. WAHRSCHEINLICH ist das nicht, wonach wir suchen.",
"So ist es besser. Das hinterlässt ein bisschen Unsicherheit.",
"Können wir die Truhe nicht zumachen?",
"Wollen wir versuchen, die Truhe zu schließen?",
"Mit der geschlossenen Truhe wäre es viel einfacher.",
"Versuch mal, den Deckel zu bewegen.",
"Wollen wir nicht etwas mitnehmen?",
"Wie wäre es, wenn wir ein paar Schätze mitnehmen?",
"Tut es dir nicht leid, die ganzen Schätze zurückzulassen?",
"Wir werden froh sein, wenn wir selbst hier rauskommen.",
"Vergiss nicht unseren Auftrag.",
"Ein Schatz würde uns nur behindern.",
"All dieses Glitzern ist eine Augenweide.",
"Hier schimmert alles so. Mir wird ganz schwindelig.",
}
