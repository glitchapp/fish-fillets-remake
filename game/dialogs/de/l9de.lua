l9={"ERSTES TORPEDOROHR GELADEN...",
"Hör auf Grimassen zu ziehen und hilf mir.",
"Hör auf, dich im Spiegel zu bewundern und mach dich mal nützlich.",
"Das ist großartig! Er macht sich einfach aus dem Staub. Und ich?",
"Wie immer. Er lässt mich links liegen.",
"Warte! Und ich? Ich will hier auch raus!",
"Hallo, ist da unten jemand?",
"Hallo, kannst du mich da drin hören?",
"Ist hier jemand?",
"Hör auf zu brüllen und hole mich hier raus.",
"Hör auf zu schreien und hilf mir hier rauszukommen.",
"Ich bin hier. Ich komme nicht raus.",
"Wessen Augen sind das?",
"Ich weiß nicht. Vielleicht ist das Periskop mit einer anderen Dimension verbunden.",
"Vorsichtig...",
"Sage nicht, ich hätte dich nicht gewarnt!",
}
