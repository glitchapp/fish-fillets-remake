l37={
"Siehst du sie? Das muss die telepatische Schildkröte sein.",
"Ja, das ist sie. Die telepatische Schildkröte.",
"Naja, ich weiß nicht. Nach den Fotos zu urteilen, könnte sie es sein.",
"Ich bin mir nicht sicher... was ist mit den telepatischen Fähigkeiten?",
"Auf jeden Fall müssen wir sie aus dem Raum bekommen.",
"Lass uns die Schildkröte rausbringen.",
"Was tust du?",
"Was hast du getan?",
"Was hat das zu bedeuten?",
"Wohin schwimmst du?",
"Was bedeutet das?",
"Was ist mit dir passiert?",
"Ich weiß nicht...",
"Ich begreife es nicht...",
"Ich weiß nicht, was mit mir passiert...",
"Ich wollte nicht...",
"Was passiert mit uns?",
"Was kann es bedeuten?",
"Das muss diese Schildkröte sein!",
"Das muss diese Schildkröte sein!",
"Dann gibt es jetzt keine Zweifel mehr - das ist sie. Die nach der wir suchen!",
"Das ist zweifellos die Schildkröte, nach der wir suchen!",
"Diese Bestie!",
"Hör auf, du Teufel!",
"Genug ist genug!",
"Macht dir das Spaß?",
"So viele bizarre Formen gibt es nur in einem Korallenriff.",
"Diese Korallen sind Wunder der Mutter Natur.",
"Woher kommen so viele bizarre Formen?",
"Das ist leicht - die Entwickler denken sich knifflige Probleme aus und überlassen das Malen den Künstlern.",
"Unser Ziel in diesem Raum ist es, diese Schildkröte herauszubekommen.",
}
