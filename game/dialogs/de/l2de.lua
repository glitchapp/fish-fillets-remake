l2={"Oh nein, nicht schon wieder...",
"Du weißt, wir können nichts dagegen tun.",
"Schieben wir ihn runter, damit wir einen Blick reinwerfen können.",
"Na dann, ran an die Arbeit!",
"So, jetzt können wir endlich anfangen.",
"Unsere erste Aufgabe ist es, aus diesem Raum rauszukommen.",
"Das ist es übrigens auch in allen anderen Räumen.",
"Wollen wir es nochmal ansehen?",
"Wie? Die Nachricht hat sich selbst zerstört.",
"Wir könnten die Ebene neustarten.",
"Das ist wahr...",
"Na los, lass uns den Spieler mit in die Werkstatt nehmen und ihm die Sicherheitsbestimmungen zeigen.",
"Gute Idee.",
"Ich kann das nicht anheben. Versuchst Du es mal?",
"Noch ein bisschen höher, so dass ich unter dir durchschwimmen kann.",
"Jetzt fass erst mal nichts an, schaue nur und lerne. Wir werden dir zeigen, was du mit uns tun und nicht tun solltest, und wozu wir noch so in der Lage sind.",
"Lass uns das Spiel abspeichern, bevor wir die Werkstatt betreten - drücke einfach die F2-Taste.",
"Erst werden wir dir zeigen, wass uns verletzen kann.",
"Ich bin freiwillig das Versuchskaninchen.",
"Vor allem sollten wir keine Dinge auf uns werfen.",
"Wenn wir einen Gegenstand tragen, können wir auch nicht nach unten schwimmen, weil er auf uns fallen würde.",
"Jetzt starten wir neu - oder wir können das gespeicherte Spiel mit der F3-Taste laden.",
"Und jetzt sind wir wieder dort, wo wir das Spiel zuletzt gespeichert haben.",
"Wir dürfen auch keine Gegenstände über unsere Rücken schieben.",
"Einige Gegenstände sind so geformt, dass wir sie gleichzeitig tragen und schieben könnten - aber das dürfen wir auch nicht.",
"Wir laden das gespeicherte Spiel wieder mit der F3-Taste.",
"Wir können auch keine zusätzlichen Gegenstände auf einen Stapel werfen, den einer von uns trägt.",
"Und im Gegensatz zu meinem größeren Partner kann ich keine Gegenstände aus Stahl tragen oder bewegen.",
"Wir haben unser gespeichertes Spiel zum letzten Mal geladen. Jetzt zeigen wir dir all das, was wir können.",
"Wir können Gegenstände immer hochheben und fallenlassen.",
"Wir können uns einen Gegenstand nur dann über den Rücken schieben, wenn der Gegenstand dabei auf einen weiteren geschoben wird...",
"... und in diesem Fall sogar auf uns fallenlassen.",
"Das kann ich sogar alleine... Ich kann drunterschwimmen... oder es auf die Kante legen.",
"Wir können einen Gegenstand auch über einen anderen Gegenstand schieben, den wir tragen, aber nicht den Gegenstand, der direkt auf unserem Rücken ist.",
"Wir können auch unter dem Gegenstand, den wir tragen, langschwimmen und ihn dem anderen Fisch übergeben.",
"Und wir können uns Gegenstände vom Rücken schieben, wenn sie sofort runterfallen.",
"Das war’s mit Regeln. Wenn du mehr wissen willst, drücke F1 und lies das Hilfe-Kapitel.",
"Zusammengefasst: Wir können Gegenstände nur anheben, fallenlassen, über und auf eine feste Oberfläche schieben und von unseren Rücken herunterschieben.",
}


l2briefcasetalkie={"Guten Morgen Fische. ",
"Dies ist eine Angelegenheit von größter Wichtigkeit.",
"Deshalb haben haben wir euch ausgewählt, unsrere fähigsten Unterwasser-Agenten.",
"Agenten der FDTO (Fisch Detektiv Training Organisation) - haben es geschafft,",
"einige Amateurfotos eines außerirdischen Objektes in die Hände zu bekommen,",
"dass irgendwo in unserer Nähe abgestürzt ist.",
"Eure Aufgabe wird sein, wenn ihr sie annehmt,",
"das UFO ausfindig zu machen und alle Informationen über die Prinzipien ",
"und die Natur des interstellaren Antriebs zu sammeln.",
"Ihr solltet auch versuchen, einige unserer ungelösten Fälle abzuschließen.",
"Wir sind sehr an den Umständen des Untergangs der sagenumwobenen Stadt interessiert,",
"die sich zufälligerweise in der gleichen Gegend befindet.",
"Dieser Fall könnte mit dem Geheimnis des Bermuda-Dreieckes zu tun haben.",
"Findet heraus, warum so viele Schiffe und Flugzeuge ",
"in den letzten Jahrzehnten dort verschwunden sind.",
"Eines der verlorenen Schiffe soll angeblich ",
"dem sagenhaften Kapitän Silver gehört haben.",
"Es gibt immer noch viele Ungewissheiten um diesen berühmten Piraten.",
"Am meisten sind wir an der Karte interessiert,",
"die den Ort seines vergrabenen Schatzes zeigt.",
"Eine eurer wichtigsten Aufgaben ist es, den Rechner zu finden,",
"den eine bestimmte kriminelle Organisation in der Tiefe versteckt hat.",
"Er enthält Daten über ein Projekt, das die ganze Welt verändern könnte.",
"Ihr solltet auch die geheimnisvolle Korallenschildkröte finden und verhaften.",
"Sie ist aus der Verhöhrstation  der FDTO entwischt. Sie ist nicht bewaffnet,",
"hat aber angeblich telepatische Fähigkeiten.",
"Wir wurden informiert, dass ein bestimmtes Atomkraftwerk ",
"seinen radioaktiven Abfall illegal entsorgt hat. Überprüft das.",
"Und vergesst nicht, den heiligen Gral zu finden.",
"Und wie üblich: Wenn einer aus eurer Mannschaft verletzt wird oder stirbt,",
"wird Altar jegliches Wissen über euch abstreiten und die Ebene wird neugestartet.",
"Diese Nachricht wird sich in 5 Sekunden selbst zerstören.",
}
