l19={
"Ich habe die Vermutung, dass wir gerade etwas Schreckliches entdecken.",
"Ich hab’s immer gewusst: Die Götter müssen verrückt sein.",
"Gott ist verrückt und die ganze Welt ist sein Spielzeug.",
"Ich denke, du hast Recht.",
"Ich fürchte, du hast Recht.",
"Ja, es ist schockierend.",
"Was können wir dagegen tun?",
"Wir können es nicht so lassen. Wir müssen sie einlochen.",
"Was müssen wir?",
"Sie ins Irren... ich meine in die Haftanstalt der FDTO stecken.",
"Du hast Recht. Sollen wir beide nehmen?",
"Natürlich. Wenn wir einen hierlassen, fällt ihm vielleicht eine verrückte Idee ein. Er könnte zum Beispiel anfangen, Golf zu spielen.",
"Wohl kaum mit diesem Schläger. Ich denke, wir werden froh sein, wenn wir einen rausbekommen. Der andere wird dann harmlos sein.",
"Gut. Und welchen?",
"Den Gelben.",
"Den Blauen.",
"Diesen Gelben.",
"Diesen Blauen.",
"Das überlassen wir dem Spieler.",
"Das sollten wir dem Spieler überlassen.",
"Schau dir den Schläger an: etwas schreckliches ist mit ihm passiert.",
"Ein rosa Monster hat wahrscheinlich an ihm gesaugt.",
"Mutter hatte Recht. Krankhafter Spieltrieb ist die Wurzel allen Übels.",
"Das ist wahr. Es wäre viel leichter, wenn der Tischtennisschläger irgendwo anders wäre.",
"Hast du übrigens bemerkt, dass der echte Golfball in der Ecke oben links und das Ding in der Ecke unten rechts der Kricktball ist?",
"Ist das ein verstecktes Zeichen?",
"Wer weiß.",
"Ich habe gedacht, da oben ist ein Tennisball.",
"Schwierig zu sagen, bei dieser Bildschirmauflösung.",
"Diesmal ist unser Ziel, einen der Götter herauszubekommen.",
"Daneben!",
"Daneben!",
"Daneben!",
"Daneben!",
"Daneben!",
"Treffer!",
"Treffer!",
"Treffer!",
"Treffer!",
"Versenkt!",
"Versenkt!",
"Versenkt!",
"Ich habe gewonnen, du Muttersöhnchen",
"Ich fange an!",
"Gut!",
"Wollen wir noch eins spielen?",
"Ich schummle nicht!",
"Es war wahrscheinlich ein Fehler...",
"Daneben!",
"Daneben!",
"Daneben!",
"Daneben!",
"Daneben!",
"Treffer!",
"Treffer!",
"Treffer!",
"Treffer!",
"Versenkt!",
"Versenkt!",
"Versenkt!",
"Ha, ha, ha... ich habe gewonnen!",
"Gut!",
"Wollen wir noch eins versuchen?",
"Das hast du schon versucht!",
"Das hast du schon gesagt!",
"Auf dem Feld kann es nicht daneben gehen!",
"Du schummelst!!!",
"Das habe ich versucht - und du hast \"Daneben\" gesagt!",
}

l19end={"Die Berufstätigkeit des Gefangenen, den wir ihnen schicken, ist Meeresgott.",
"Außer des Verschwindens von Flugzeugen und Schiffen (dem sogenannten Schiffe-Versenken-Fall)",
"ist er weiterer Verbrechen schuldig, darunter:",
"Verschieben von Kontinenten (Kennzeichen Run,Continent,Run)",
"und eines Meteoriten in Tunguska (Kennzeichen Jumping Jack).",
"Wir schafften es, im letzten Augenblick einzugreifen:",
"Wir haben eine brandneue Schachtel",
"mit dem Tischspiel \"STAR WARS\" im Haus des Gefangenen gefunden.",
"Sie finden die Aufzeichnungen seiner Seeschlachten im Anhang.",
}
