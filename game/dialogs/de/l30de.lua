l30={"Ich habe noch nie so violette Korallen gesehen.",
"Ich mag keine violetten Korallen.",
"Ich liebe violette Korallen...",
"Die gefallen mir.",
"Ich hoffe, in den nächsten Ebenen werden noch interessantere Korallen sein.",
"Bitte nicht.",
"Wir müssen das Stahlteil anheben...",
"Hallo Krabbe!",
"Wollen wir versuchen, die Krabbe ein bisschen aufzumuntern?",
"Ignoriere sie.",
"Lass die Krabbe in Ruhe.",
"Siehst du nicht wie nervös sie ist?",
"Versuch es ohne diese nervöse Krabbe.",
"Diese Krabbe könnte nützlich sein.",
"Diese Krabbe wird uns sicher helfen, hier rauszukommen.",
}

l30crab={"Wer weckt mich auf?",
"Lass mich in Ruhe, ich schlafe hier!",
"Was willst du von mir?!?",
"Fass mich nicht an!",
}

l30crabups={"Auah!",
}
