l31={
"Das ist ja ein komischer Raum.",
"Das ist ein ziemlich ungewöhnlicher Raum.",
"Die Stelle des Assistentenstellvertreters des Entwurfskoordinators wurde aufgrund dieser Ebene eingeführt. Damit wurde der Verfasser geehrt.",
"Genug zum Hintergrund des Spiels und ran an die Arbeit.",
"Das ist eine ziemlich seltsame Koralle.",
"Das ist eine sehr ungewöhnliche Koralle.",
"Wo hängt die denn dran?",
"Ich weiß nicht. Jedenfalls müssen wir sie da runterkriegen.",
"Ich weiß nicht. Müssen wir sie da runterholen?",
"Wie konnte diese Koralle solch eine bizarre Form annehmen?",
"Sie ist sorgfältig für die Logikspieler gewachsen.",
"Warum willst du die Koralle überhaupt runterholen?",
"Keine Ahnung. Frag den Spieler.",
"Schnecklein, Schnecklein komm heraus...",
"Hör auf! Du weißt doch, dass sich Gegenstände nur bewegen, wenn wir sie schieben!",
"Aber ich kann es doch versuchen, oder? Schnecklein, Schnecklein...",
"Hör auf!! Da sträuben sich mir ja die Schuppen!",
"Dann hör nicht zu. Schnecklein, Schnecklein, komm...",
"Hör auf!!! Oder ich schmeiß dir die Koralle auf den Kopf!",
}
