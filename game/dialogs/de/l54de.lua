l54={
"Ich glaube, hier können wir ein paar Informationen über den intergalaktischen Antrieb bekommen.",
"Das sieht eher wie ein gewöhnlicher Antrieb für das Landungsschiff aus.",
"So. Wir haben den Antrieb gefunden. Wir haben ein Ziel unserer Aufgabe erreicht.",
"Nicht so voreilig. Wir haben noch nicht das ganze Wrack abgesucht.",
"Schade, dass keins der technischen Wunderwerke um uns herum funktioniert.",
"Ich will mal eins dieser außerirdischen Dinger in Aktion erleben!.",
"Ich frage mich, ob dieser Motor unter Wasser funktioniert.",
"Vielleicht ist es für uns besser, wenn er es nicht tut.",
"Ich bin ziemlich froh, dass wir hier nichts anschalten können.",
"Wir sollten froh sein, dass wir hier noch nichts angeschaltet haben.",
"Vorsicht mit dem Schraubenschlüssel.",
"Hier kann ich nichts kaputt machen.",
"Was hast du gemacht? Mach das Getöse aus!",
"Das ist schrecklich! Mach das aus bevor es zu spät ist!",
"Ich kann nicht! Ich krieg’s nicht aus!",
"Ich weiß nicht wie! Ich krieg’s nicht aus!",
"Mayday! Mayday!",
"Endlich.",
"Was für eine Erleichterung.",
"Was machst du? Wohin gehen wir?",
"Was hast du angeschaltet? Wohin bringt es uns?",
"Wie kann ich es ausschalten?!",
"Ich kann es nicht ausschalten!",
"Danke.",
"Endlich.",
"Ich fürchte nur, wir müssen es nochmal anschalten.",
}
