

function Obey.lev30()
  
  
  avatarcrab = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level30/crabavatar.webp")))
  
  loadcorrespondentfonts()

   Talkies.say( "Big fish",
    { 
		  l30[1],
		  l30[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })


 Talkies.say( "small fish",
    {
  l30[3],
  l30[4],
  l30[5],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l30[6],
  l30[7],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l30[8],
  l30[9],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l30[10],
  l30[11],
  l30[12],
  l30[13],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  
  Talkies.say( "small fish",
    {
  l30[14],
  l30[15],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      
  Talkies.say( "crab",
    {
  l30[16],
  l30[17],
  l30[18],
  l30[19],
  l30[20],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev30crab()


  Talkies.say( "Crab",
    { 
		  l30crab[1],
		  l30crab[2],
		  l30crab[3],
		  l30crab[4],
    },
    {
      image=avatarcrab,
      talkSound=blop,
      typedNotTalked=true,
    })
end
function Obey.lev30crabups()
  Talkies.say( "Crab",
    { 
		  l30crabups[1],
    },
    {
      image=avatarcrab,
      talkSound=blop,
      typedNotTalked=true,
    })
end
