l23={"Ik denk dat we eindelijk die mysterieuze stad op het spoor zijn.",
"Het ziet er heel indrukwekkend uit.",
"Nou moeten we er alleen achter zien te komen waarom de stad is gezonken.",
"Maar dat gaat misschien wel wat tijd kosten.",
"Laat me met rust. Ik bewonder de klassieke schoonheid van deze tempel.",
"Wie is dat op die stoel?",
"Dat is een kopie van de Zeus van Feidios, een van de zeven wereldwonderen.",
"Hmm. Dat was een ander tijdperk.",
}

l23zeus={"Voorzichtig! Probeer het heel te houden!",
"Wat zonde!",
"En zet het nou voorzichtig weer neer!",
"Barbaar! Kun je niet een beetje voorzichtiger zijn?!",
"Ik vond dat beeld toch maar niks.",
}
