l3={"Je kent de regels al.",
"Nu kun je wat oefenen.",
"We geven je af en toe wat aanwijzingen.",
"Ik mag nu niet naar beneden zwemmen. ",
"En ik kan ook niet naar boven.",
"Hee, regel dat daar boven eens.",
"Maar wees wel voorzichtig!",
""
}

l3icantmove={"Dat kan ik niet verplaatsen.  Het is van staal."}

l3comeback={"Kom terug, je kunt niet verder!"}

l3thisistricky={"Hmm... Dit is lastig. Als we de honing opzij duwen worden we verpletterd door die pot abrikozenjam.",
"Kunnen we die niet opzij duwen met de bijl?",
"Dat is een goed idee.",
"Nu kan ik niet meer bij de bijl. De pot zit in de weg.",
"Je moet niet tegen de bijl duwen!",
"Als je die pot mijn kant op duwt, dan schuif ik de bijl erop en dan komt alles goed."
}

l3thislooksrealbad={"Dit ziet er slecht uit. Je moet het veld herstarten."}

l3ifyousolvesomespecific={"If you solve some specific part you can save the position.",
"But only if you are sure of your solution.",
"Remember what we have said about saving positions."}

l3youarestandinginmyway={"Je zwemt in de weg. Probeer je ergens anders te verstoppen.",
"Hier misschien? Ik vind dit een beetje eng. Hoe moet ik hier straks weer uit komen?",
"Wacht maar."}

l3ishoulndtbe={"I shouldn’t be thinking about this."}

l3wewillgiveyouahint={"We geven je hier wel een aanwijzing, speler. Je moet dat boek daar naar links duwen.",
"Zodat die stalen cylinder erop terecht komt als ik die een duwtje geef.",
"Maar we vertellen je lekker niet hoe het moet.",
"Vergeet niet dat we misschien geen dingen over elkaars rug kunnen duwen, maar wel van elkaar af of op een vast punt.",
"Wat als ik het bovenste pad kies?",
"Sst. Je zegt te veel voor."}

l3justremember={"Just remember that although we cannot push objects along each other’s back, we can push them down or onto some solid structure."}

l3thingslikethat={"Things like that happen sometimes."}

l3sometimes={"Soms moet je heeeeeeel ver vooruit denken.",
"En dan moet je vaak het veld opnieuw doen."}

l3weadmitwedid={"We moeten toegeven dat het expres was.",
"Zodat je het nog een keertje kunt proberen, maar dan zonder aanwijzingen."}
