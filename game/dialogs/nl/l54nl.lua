l54={"Ik denk dat we hier misschien wat aanwijzingen kunnen vinden over ruimtereizen.",
"Dit ziet er meer uit als een conventionele aandrijving voor het landingsvoertuigje.",
"Zo. We hebben de aandrijving gevonden. We hebben een van onze doelen van deze missie bereikt!",
"Niet zo vlug. We hebben de rest van het wrak nog helemaal niet doorzocht.",
"Jammer dat al die technische hoogstandjes om ons heen het niet meer doen.",
"Ik zou deze buitenaardse apparaatjes wel eens in actie willen zien.",
"Ik vraag me af of deze motor onderwater zou kunnen werken.",
"Misschien is het maar goed dat hij het niet doet.",
"Ik ben blij dat we hier niks per ongeluk aan kunnen zetten.",
"We moeten blij zijn dat we hier niks per ongeluk aan kunnen zetten.",
}
l54engineon={"What have you done? Turn off that roar!",
"What have you activated? Where is it taking us?",
"This is terrible! Turn it off before it’s too late!",
"Ik krijg hem niet uit!",
"I don’t know how! I can’t take it out!",
"Mayday! Mayday!",
"How can I turn it off?!",
"I can’t turn it off!"
}

l54engineonfish1={"Wat heb je gedaan? Zet die herrie uit!",
"Aaarggg! Zet het snel weer uit!",
"Het lukt niet. Ik krijg het er niet uit!",
"Hoe dan? Ik krijg het er niet meer uit!",
"Mayday! Mayday!",
"",
}
--"I can’t turn it off!"
l54engineoff={"Eindelijk.",
"Wat een opluchting.",
"Bedankt.",
"Eindelijk.",
"Ik ben alleen bang dat we hem weer aan moeten zetten.",
""
}

l54aproachengine={"Wat doe je nou? Waar gaan we heen?",
"Voorzichtig met die steeksleutel.",
"Ik kan hier toch niks kapot maken.",
}

