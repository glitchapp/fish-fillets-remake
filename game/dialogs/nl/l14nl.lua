l14={"Wat is dit voor raar schip?",
"Dat is het wrak van het passagiersvliegtuig LC-10 Lemura.",
"Dat is het wrak van het passagiersvliegtuig Atlantobus.",
"Dat is het wrak van het passagiersvliegtuig Poseidon 737.",
"Zie je dat oog? De stille getuige van deze tragedie... Iemand vertrouwde op dit vliegtuig - en dat glazen oog is alles wat er van hem over is.",
"Dit is geen glazen oog, maar een gyroscoop. In ieder geval in dit veld.",
"Stoelen. Waarom zijn hier zoveel stoelen?",
"Wees blij. Zou je zonder die dingen hier weg komen?",
}
