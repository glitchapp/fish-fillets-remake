l21={"Welkom in de mooiste stad onder de zon.",
"Welkom in onze stad - de stad van onbeperkte mogelijkheden.",
"In naam van de burgers van deze stad heet ik u welkom.",
"Welkom in onze stad.  Vergeleke bij haar schoonheid verbleekt zelfs de schoonheid van de goden zelf.",
"Waarde burgers, blijft alstublieft rustig.",
"Er is geen direct gevaar.",
"De overstroming van het noordelijke gedeelte van ons eiland is slechts tijdelijk.",
"We hebben de situatie volledig onder controle.",
"Het schip uit Lemuria heeft zojuist aangelegd bij steiger 5. Dit schip gaat verder naar Utopia, Mu en Atlantis.",
"Met een Vissers Verzekering is uw huis verzekerd tegen schade als gevolg van brand, tornado's, wervelwinden, goddelijk interventie en drakenaanvallen. Omdat vissers overal aan denken, verzeker uw huis bij Vissers.",
"Waarom heb je zo'n haast?",
"Ik ga naar Maanstraat. Ik moet acht zwaarden bestellen.",
"Waarom zoveel zwaarden?",
"Heb je het niet gehoord? Bij elke acht zwaarden krijg je helemaal GRATIS een paar bronzen oorbellen!",
"Wacht op mij. Ik ga mee!",
--"Het Gerechtshof van de stad nodigt u allen uit voor het jaarlijkse festival van ondervragingen en executies. Gepaste kleding verplicht.",
"De gemeenteraad heeft besloten een aquaduct te bouwen om direct stromend water naar elk huis mogelijk te maken.",
--"Zou de moeder van het kind dat verdwaald was in de stadsschatkamer alstublieft zijn stoffelijk overschot kunnen ophalen bij de receptie.",
"Waarde burgers, onze werknemers komen morgen na middennacht bij alle huizen langs om demonen en geesten uit te drijven. We vragen vriendelijk om uw medewerking.",
"Onze hoofdketter Demikuls zal vanavond in het kleine auditorium een seminarium geven getiteld “Bestaan de goden echt? of Wees gerust dat ze u geen kwaad kunnen doen!”",
"Bezoek ons visrestaurant aan het Marktplein. Heerlijke visspecialiteiten en...",
"O, pardon.",
"Wat?! Een visrestaurant?",
"Meen je dat nou echt?",
"Wat afgrijselijk!",
"Dit beeld heeft duidelijk niet door dat de stad al een hele tijd geleden onderwater gelopen is.",
"Het is waarschijnlijk geautomatiseerd ofzo. Het denkt dat de stad nog leeft.",
"Of het kan de waarheid niet aan.",
"Ik krijg hoofdpijn van dat hoofd.",
"Dat hoofd doet mijn hoofd pijn.",
"Laten we proberen dat hoofd het zwijgen op te leggen.",
"Hoezo? We krijgen allemaal waardevolle informatie over het dagelijkse leven van onze voorouders.",
"Als we al deze krabben daarboven bij dat hoofd konden krijgen...",
"...dan...?",
"Wacht, ik moet nog even nadenken...",
"Stel je voor - dat hoofd is al die tijd zo bezig geweest...",
"Nou en?",
"Moet je je eens voorstellen hoe die arme krabben zich voelen!",
"",
}
