l7={"Lieve hemel, door de inslag is het hier ingestort!",
"Ik kom hier nooit doorheen. We moeten een andere uitgang zoeken.",
"Dit gaat niet.",
"Dit lukt me niet in m'n eentje.",
"Ik had misschien beter die schelp eerst op kunnen tillen.",
"Misschien moeten we deze dingen omwisselen.",
"Een matras. Het allerhandigste om onderwater bij je te hebben.",
}

l7part2={"Nu kunnen we de slak op de matras laten vallen.",
"Het gaat lastig worden de slak van daaraf op te tillen.",
"Eindelijk is hij er.",
"Arm slakje...",
"We moeten het koraalrif systematisch doorzoeken.",
"Er zullen daar vast veel interessante wezens te onderzoeken zijn.",
"Hebben we geen microscoop nodig, als we koralen willen bekijken?",
"Ja, die zijn inderdaad klein, maar er kunnen ook andere levensvormen tussen wonen.",
"Bij voorbeeld koraalschildpadden.",
"En bovendien heb ik een donkerbruin vermoeden dat er een microscoop in een bathyscaaf ligt ergens.",--]]
}
