l2={"O nee, niet weer...",
"Je weet dat we er niks tegen kunnen beginnen.",
"Laten we dat ding naar beneden schuiven, zodat we kunnen zien wat erin zit.",
"Nou, aan de slag dan maar!",
}

l2thediskself={"Hoe? Dat ding heeft zichzelf opgeblazen.",
"Zo, nu kunnen we eindelijk beginnen.",
"Eerst moeten we uit deze ruimte zien te ontsnappen.",
"Dat is trouwens ook in bijna alle andere ruimtes ons doel.",
"Zullen we het nog een keertje bekijken?",
"Nou, laten we de speler meenemen naar de werkplaats en de veiligheidsvoorschriften uitleggen.",
"Goed idee.",
"Blijf nu maar even overal af. Let op en leer. We laten je zien wat je wel en niet met ons moet doen en wat we kunnen.",
"Voordat we de werkplaats binnen gaan kunnen we beter het spel opslaan - druk maar op de F2 toets.",
"Eerst demonstreren we wat niet goed voor ons is.",
"Ik werp me wel op als slachtoffer.",
"We moeten in ieder geval geen dingen op elkaar laten vallen.",
"We kunnen ook niet naar beneden zwemmen als we iets dragen, omdat het dan op ons valt.",
"Iets anders wat we beter ook niet kunnen doen is dingen over elkaars rug duwen.",
"Sommige dingen hebben zo'n vorm dat we ze tegelijk omhoog zouden kunnen houden en duwen - maar dat kan ook niet.",
"In tegenstelling tot mijn grotere partner kan ik geen stalen objecten dragen of verplaatsen.",
"We hebben ons opgeslagen spel van de vorige keer geladen. Nu laten we je zien wat we allemaal kunnen.",
"We kunnen altijd dingen oppakken en dan weer laten vallen.",
"We kunnen wel iets langs elkaars rug duwen als het daarna gelijk weer op een ander steunpunt terecht komt...",
"... en in zo'n situatie kunnen we zelfs dingen op elkaar laten vallen.",
"Dit kan ik zelfs in mijn eentje... Ik kan eronder zwemmen... of het op dit richeltje leggen.",
"We kunnen ook iets over een ding duwen dat door de ander wordt ondersteund, zolang we niet het object proberen te verschuiven dat direct op zijn of haar rug ligt.",
"We kunnen ook onder iets dat we dragen zwemmen en het doorgeven aan de andere vis.",
"En we kunnen dingen van elkaars ruggen afduwen, als het maar gelijk naar beneden valt.",
"Dat is het wel zo'n beetje met de regels. Als je meer wilt weten, druk dan op F1 en lees de help.",
"Kort samengevat: we kunnen dingen optillen, laten vallen, langs vaste oppervlakken verschuiven, op dingen duwen, en van elkaar af duwen.",
}

l2wecouldrestart={"We zouden het veld kunnen herstarten.",
"Dat is waar...",
}

l2icantliftit={"Ik kan dat niet optillen. Kun jij het even proberen?",
"Nog een klein beetje hoger, zodat ik onder je door kan zwemmen.",
}

l2lethisbytheway={
"Should we watch it again?",
}

l2nowell={"We laden het opgeslagen spel weer met de F3 toets.",
"We kunnen ook beter geen extra dingen bovenop andere dingen gooien die door de ander gedragen worden.",
}

l2again={"Nu beginnen we opnieuw - of je kunt het opgeslagen spel laden door op de F3 toets te drukken.",
"En nu zijn we weer terug waar we eerder het spel hadden opgeslagen.",
}

l2wehaveloaded={"We have loaded our saved game for the last time. Now we’ll show you all the things we’re capable of.",
}

l2briefcasetalkie={"Goede morgen, vissen. ",
"Dit is een zeer belangrijke zaak en daarom benaderen we jullie,",
"onze beste onderwateragenten.",
"Agenten van de VDTO - Vissen Detective Training Organisatie",
" - zijn erin geslaagd om een aantal amateuropnamen",
"te bemachtigen van een buitenaards object",
"dat ergens bij jullie in de buurt is neergestort.",
"Jullie missie, als jullie die accepteren,",
"is om de vliegende schotel te bemachtigen en zoveel mogelijk informatie",
"te vinden over de interstellaire aandrijving.",
"Het zou fijn zijn als jullie ook even kunnen kijken naar wat andere onopgeloste zaken.",
"We zijn vooral geïnteresseerd in de omstandigheden",
"omtrend het zinken van de legendarische stad, toevallig ook daar in de buurt.",
"Deze zaak houdt wellicht verband met het mysterie van de Bermuda Driehoek.",
"Achterhaal waarom er de afgelopen tientallen jaren",
"zo veel schepen en vliegtuigen zijn verdwenen in deze omgeving.",
"Een van de verloren schepen zou het schip van de legendarische kapitein Silver zijn geweest.",
"Er is nog steeds een hoop onduidelijkheid omtrend deze beruchte piraat.",
"We zijn vooral geïnteresseerd in de kaart waar de verstopplaats van zijn schat op is aangegeven.",
"Een van jullie belangrijkste opdrachten is het vinden van de computer",
"die door een bepaalde criminele organisatie in de diepte is verstopt.",
"Er staat informatie op over een project dat de hele wereld zou kunnen veranderen.",
"Jullie moeten ook de mysterieuze koraalschildpad zien te vinden en arresteren.",
"Die is aan de VDTO ondervragingsafdeling ontsnapt.",
"Hij is niet bewapend, maar is waarschijnlijk wel telepatisch.",
"We hebben bericht gekregen dat een bepaalde kernreactor",
"illegaal radioactief afval heeft gedumpt. Zoek het uit.",
"En vergeet niet de Heilige Graal te vinden, als je toch bezig bent.",
"En, zoals altijd, als een van jullie team gewond raakt of omkomt,",
"dan zal Altar ontkennen iets met jullie te maken te hebben en het veld zal worden herstart.",
"Dit bericht zal zichzelf over vijf seconden vernietigen.",
}
--"five, four, three, two, one"

