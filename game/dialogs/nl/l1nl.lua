l1={"Wat was dat?",
"Ik heb geen idee.",
"We kunnen beter even buiten gaan kijken.",
"Wacht even! Ik ga met je mee.",
"Waarom gebeurt er niks?",
"Hee, speler!",
"Je kunt ons besturen met de pijltjestoetsen en met de spatiebalk wissel je van vis.",
"Je kunt ons ook met de muis besturen -- klik met de linker knop ergens en...",
"...als we daarheen kunnen gaan zonder iets te verplaatsen...",
"...volgen we je geklik.",
"En als je ergens met de rechter knop klikt, dan proberen we daar heen te gaan en alle obstakels die we onderweg tegen komen opzij te duwen.",
"De bedoeling is dat je ons allebei naar buiten leidt.",
"En wees wel voorzichtig met ons. Als je iets op ons laat vallen...",
"Oh, hou op. Dat vertellen we in het volgende veld wel.",
"Goed.",
"Oeps! Dat was niet goed. We moeten allebei ontsnappen om verder te kunnen.",
"Hier kom ik nooit meer uit. Je zult het veld opnieuw moeten spelen.",
"Je moet op die lange pijl op je toetsenbord drukken.",
"Mensen noemen dat de backspace toets.",
"Laat maar. In het tjechisch was het grappiger.",
"De optie om te herstarten zit ook op het controlepaneel.",
"Als je denkt dat je liever opnieuw begint met een veld, dan is dat geen probleem.",
"Druk op de knop met de lange pijl.",
"Ik ben een zware stalen cylinder. Het is niet makkelijk om mij te verplaatsen. Een klein visje als dat oranje visje daar heeft gewoon geen kans. Ik zou haar gewoon verpletten.",
}


l1icantgetthrough={"Ik kom hier niet doorheen. Deze stalen cylinder zit in de weg.",
"Ik kan die cylinder niet verplaatsen. Kun je me helpen?",
"Geen probleem...",
}

l1wowyoumovedit={
"Wow, je hebt hem verplaatst. Dat zou ik nooit kunnen!",
"Bedankt, nu kan ik mee naar buiten.",
}

l1damnit={"Verdomme, ik kan er niet door.",
"Misschien ben ik wat aangekomen.",
"Kun je die stoel een beetje naar links schuiven?",
}
