l71={"Siamo bloccati qui.",
	"Troveremo un modo per uscirne.",
	"Perché ne sei così sicuro?",
	"Abbiamo sempre trovato un modo per uscire.",
	"Siamo arrivati fin qui, non falliremo qui.",
	"Il tema di questo ramo è: 'UFO-Exit', il che significa che riusciremo ad uscire di qui.",
	"Qual sarebbe il punto di un livello insolubile?",
	"Non so se sia un argomento valido.",
}

l71random={"Dovremmo spegnere l'elettromagnete.",
	"Come potremmo farlo? Siamo in grado solo di spostare oggetti.",
	"Quindi spostiamo la leva laggiù.",
	"Pensi che la leva abbia un impatto sui magneti?",
	"Almeno dovrebbe essere collegata al campo magnetico in alto a destra.",
	"Potrebbe essere.",
	"Forse dovremmo capire qual è lo scopo di tutti questi magneti in questo livello?",
	"Non penso. Nel briefing diceva che dovevamo scoprire come funziona la propulsione interstellare.",
	"E se i magneti fanno parte della propulsione interstellare?",
	"Allora dovremmo scoprirlo. Ma secondo me sono solo qui per ostacolarci.",
	"Guarda, spazzatura aliena.",
	"Dove?",
	"Beh, la lattina piccola laggiù.",
	"Forse è ancora piena.",
	"È un artefatto e non spazzatura.",
	"Non penso che sia davvero una lattina. Probabilmente è un'unità di controllo aliena camuffata.",
	"In ogni caso, per me è spazzatura e ostacola.",
	"Guarda il cristallo.",
	"Bello. Di che materiale pensi che sia fatto?",
	"Dilicio.",
	"Diidrogenomonossido.",
	"Cloruro di sodio.",
	"Metallo non ferroso.",
	"Vetro con carta colorata incollata sopra.",
	"Una lega aliena.",
	"Non penso.",
"Perché i magneti rimangono a distanze fisse e non si attraggono reciprocamente in modo più forte?",
"Sono magneti alieni, funzionano in modo diverso.",
"Capisco.",
"Un'altra asta di combustibile.",
"Spero che siamo abbastanza lontani dal livello precedente nel caso in cui il reattore esploda.",
"Forse non avremmo dovuto spostare tutte le aste di combustibile nel reattore.",
"Allora saremmo ancora bloccati lì.",
"Forse non era un reattore.",
"Quella cosa solitaria lassù, che cos'è?",
"Un artefatto alieno non identificato. Quale intendi esattamente?",
"Quella angolare che ci ostacola, quella che copre metà del livello.",
"Come ho detto, è un artefatto alieno non identificato.",
"Sì. Ma qual è il suo scopo?",
"Se lo sapessi, non sarebbe più un artefatto alieno non identificato ma un semplice artefatto alieno.",
}

l71staybythemagnet={"Ahi.",
"Ouch.",
"Ahi.",
"Mi fa solletico.",
"Mi pizzica.",
"È caldo.",
}

l71shootingthelaser={"Stai attento!",
"Quell'arma è carica.",
"Guarda il buco che il laser ha fatto.",
"Quasi mi hai colpito.",
"Oops. Scusa.",
"Non è sorprendente che l'arma spari se usi il grilletto per spostare qualcosa.",
"Non dovresti usare un'arma da fuoco come sostituto di un piede di porco!",
"Non è colpa mia. Mi hai dato quella cosa lì.",
"Cercherò di ricordarmelo.",
"La cosa positiva è che a me non è successo nulla.",
"Perché gli alieni non sono così pacifici da non aver bisogno di armi?",
"Mi chiedo la stessa cosa degli esseri umani.",
}
