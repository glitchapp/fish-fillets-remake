l23={"Penso che siamo finalmente sulla pista di quella misteriosa città.",
"Sembra magnifica.",
"Ora dobbiamo solo scoprire perché è affondata e abbiamo finito.",
"Ma potrebbe richiedere un po' di tempo.",
"Lasciami solo. Adoro la bellezza classica di questo tempio.",
"Chi è seduto su quella sedia?",
"È una copia del Zeus di Feidios. Una delle sette meraviglie del mondo.",
"Hmm. Era un'altra epoca.",
}

l23zeus={"Stai attento! Cerca di non danneggiarlo!",
"Un tale spreco!",
"E ora abbassalo, con attenzione.",
"Barbaro! Non puoi essere un po' più cauto?!",
"Non mi piaceva questa scultura comunque.",
}
