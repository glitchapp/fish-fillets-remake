l2={"Oh no, di nuovo...",
"Sai che non c'è niente che possiamo fare al riguardo.",
"Spingiamolo giù così possiamo darci un'occhiata.",
"Bene, mettiamoci al lavoro.",
}

l2thediskself={"Come? Il disco si è autodistrutto.",
"Quindi, adesso possiamo finalmente cominciare.",
"La nostra prima sfida è uscire da questa stanza.",
"E questa, a proposito, sarà la nostra sfida anche in tutte le altre stanze.",
"Dai, portiamo il giocatore nel laboratorio e mostriamogli le norme di sicurezza sul lavoro.",
"Bella idea.",
"Per ora, non toccare niente, guarda solo e impara. Ti mostreremo cosa dovresti e non dovresti fare con noi e cosa siamo capaci di fare.",
"Prima di entrare nel laboratorio, salviamo il gioco - premi semplicemente il tasto F2.",
"Prima ti mostreremo cosa può ferirci.",
"Mi offro volontario come cavia.",
"Prima di tutto, non dovremmo far cadere le cose l'uno sull'altro.",
"Non possiamo nemmeno nuotare verso il basso quando portiamo un oggetto, perché cadrebbe su di noi.",
"Un'altra cosa che non possiamo fare è spingere gli oggetti lungo la nostra schiena.",
"Alcuni oggetti hanno una forma tale che potremmo tenerli e spingerli contemporaneamente, ma non ci è permesso farlo.",
"Non possiamo nemmeno far cadere oggetti aggiuntivi su una pila che uno di noi sta trasportando.",
"E a differenza del mio partner più grande, io non posso spostare né portare oggetti in acciaio.",
"Possiamo sempre raccogliere gli oggetti e poi farli cadere.",
"Possiamo spingere un oggetto lungo la schiena dell'altro solo se l'oggetto verrà poi spinto su un altro supporto...",
"...e persino farlo cadere l'uno sull'altro in questo caso.",
"Posso farlo anche da solo... Posso nuotare verso il basso sotto di esso... o metterlo su questa sporgenza.",
"Possiamo anche spingere un oggetto lungo la parte superiore di un altro oggetto che stiamo sostenendo, ma non possiamo spostare l'oggetto che si trova direttamente sulla nostra schiena.",
"Possiamo anche nuotare liberamente sotto un oggetto che stiamo sostenendo e passarlo all'altro pesce.",
"E possiamo far cadere gli oggetti dalla schiena dell'altro, purché l'oggetto cada immediatamente.",
"In sintesi, possiamo sollevare gli oggetti, farli cadere, spingerli lungo una superficie solida, spingerli su una superficie e farli cadere l'uno sull'altro.",
"Queste sono circa le regole. Se vuoi saperne di più, premi F1 e leggi la sezione di aiuto.",
}

l2wecouldrestart={"Potremmo ricominciare il livello.",
"È vero...",
}

l2icantliftit={"Non riesco a sollevarlo. Vuoi provare tu?",
"Un po' più in alto così posso nuotare sotto di te.",
}

l2lethisbytheway={
"Dovremmo guardarlo di nuovo?",
}

l2nowell={"Adesso ricominceremo - o possiamo caricare il gioco salvato premendo il tasto F3.",
"E adesso siamo tornati dove abbiamo salvato l'ultima volta il gioco.",
}

l2again={"Ancora una volta, carichiamo un gioco salvato premendo il tasto F3.",
}

l2wehaveloaded={"Abbiamo caricato il nostro gioco salvato per l'ultima volta. Adesso ti mostreremo tutte le cose di cui siamo capaci.",
}

l2briefcasetalkie={"Buongiorno, pesci.",
"Questa è un'affare di estrema importanza e quindi abbiamo scelto voi,",
"i nostri agenti subacquei più abili.",
"Agenti del FDTO - Fish Detective Training Organization -",
"siamo riusciti ad ottenere diverse foto amatoriali di un oggetto extraterrestre",
"che è precipitato da qualche parte nelle vostre vicinanze.",
"La vostra missione, se decidete di accettarla,",
"sarà recuperare l'UFO e acquisire tutte le informazioni possibili sui principi e la natura della propulsione interstellare.",
"Dovrete anche cercare di risolvere alcuni dei nostri casi ancora irrisolti.",
"Siamo particolarmente interessati alle circostanze che hanno portato all'affondamento della mitica città, per caso nella stessa area.",
"Questo caso potrebbe essere collegato al mistero del Triangolo delle Bermuda.",
"Scoprite perché così tante navi e aerei sono scomparsi in questa zona nel corso degli ultimi decenni.",
"Si dice che una delle navi perdute appartenesse al leggendario Capitan Silver.",
"Ci sono ancora molti interrogativi su questo famoso pirata.",
"Soprattutto siamo interessati alla mappa che mostra la posizione del suo tesoro sepolto.",
"Una delle vostre compiti più importanti è trovare il computer nascosto in profondità da una certa organizzazione criminale.",
"Contiene dati su un progetto che potrebbe cambiare l'intero mondo.",
"Dovrete anche trovare e detenere la misteriosa tartaruga di corallo.",
"È fuggita dalla struttura di interrogatorio del FDTO.",
"Non è armata, ma si dice abbia abilità telepatiche.",
"Siamo stati informati che una certa centrale nucleare sta scaricando illegalmente i suoi rifiuti radioattivi.",
"Andate a controllare. E non dimenticate di trovare il Santo Graal.",
"E come al solito: se qualcuno del vostro team viene ferito o ucciso,",
"Altar negherà ogni conoscenza della vostra esistenza e il livello sarà ricominciato.",
"Questo disco si autodistruggerà tra cinque secondi.",
}
--"cinque, quattro, tre, due, uno"
