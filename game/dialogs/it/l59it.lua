l59={"Vedi quel cavalluccio marino?",
	"È bloccato tra le anfore.",
	"Si è ubriacato tra le anfore.",
	"Mi chiedo se ci sia qualcosa dentro di loro.,"
	"Probabilmente devi andare a controllare tu stesso.",
	"Finalmente, posso vedere un nuovo tipo di teschio.",
	"Hai notato quel totem? È il dio messicano Shelloxuatl.",
	"Sembra proprio lui.",
	"Queste anfore cadono insopportabilmente lentamente.",
	"Beh, tu non sei un orso, dopotutto.",
	"Non dimenticare che siamo sott'acqua.",
	"Acciderba. Gli autori avrebbero potuto risparmiarci quell'animazione.",
	"Questo totem mi sembra interessante.,"
	"Questo teschio sembra emettere qualcosa di strano.,"
	"È vivo o è qualche tipo di incantesimo?",
	"",
	"",
	"",
}

l59skull_canyouall={"Potreste occuparvi dei fatti vostri? Ci piace dormire.", ""}

l59skull_wellanother={"Bene, un'altra notte di sonno agitato a causa di persone curiose.", ""}

l59skull_welliwas={"Beh, stavo dormendo finché non sono stato così scortesemente svegliato.", ""}

l59skull_anothernight={"Un'altra notte di sonno tranquillo rovinata.", ""}
