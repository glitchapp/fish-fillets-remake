l20={"Che gloriosa civiltà. Tanti secoli sott'acqua e i loro ascensori funzionano ancora.",
"Soprattutto quando li spingiamo.",
"Ma solo verso il basso.",
"Vedi quella conchiglia?",
"Non ti sembra familiare quella conchiglia?",
"È un antico talismano per i costruttori di ascensori che dicevano sempre 'Costruisci bene. Costruisci con la Shell'.",
"Dovrebbero vergognarsi. Questa è pubblicità subliminale.",
"Quello scheletro sembra familiare.",
"Tutti gli scheletri umani sono uguali... A differenza degli scheletri dei pesci.",
"Questa è una strada difficile.",
"Mi sento come un topo in una ruota.",
"Una ruota per i pesci? Ma noi abbiamo solo le pinne.",
"Hoo, hooooo, hoooo... Sono quiii...",
"Hoooo, se vuoi ti dirò come risolvere tutti i livelli...",
"Hooo, ho, hooo, so perché la città è affondata!",
"Hoooo, perché nessuno mi sta dando attenzione?",
"Hoooo ho hooo! Perché mi state ignorando?",
"Hooooo, hooooo, pesci! Fate attenzione a me!",
}

l20skull={"Potreste occuparvi dei vostri affari? A noi piace dormire, in effetti.",
""
}
