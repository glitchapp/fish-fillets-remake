l14={"Che tipo di strana nave è quella?",
"Questo è il relitto dell'aereo civile LC-10 Lemura.",
"Questo è il relitto dell'aereo civile Atlantobus.",
"Questo è il relitto dell'aereo civile Poseidon 737.",
"Vedi quell'occhio? Il silenzioso testimone della tragedia... Qualcuno ha confidato in questo aereo - e l'occhio di vetro è tutto ciò che ha lasciato indietro.",
"Questo non è un occhio di vetro, ma un giroscopio. Almeno in questo livello.",
"Posti a sedere. Perché ci sono così tanti posti qui?",
"Sii grato. Potresti uscire senza di loro?",
}
