l3={"Conosci già le regole.",
"Ora puoi esercitarti con esse.",
"Di tanto in tanto ti daremo degli indizi.",
"Non posso nuotare verso il basso adesso.",
"E non posso andare su, neanche.",
"Ehi, sistemalo lassù in qualche modo.",
"Ma fai attenzione!",
""}

l3icantmove={"Non posso spostare questo. È di acciaio."}

l3comeback={"Torna indietro, non puoi andare oltre!"}

l3thisistricky={"Hmm... Questo è difficile.",
"Se spingiamo da parte il miele, saremo schiacciati da quella lattina di marmellata di albicocche.",
"Non potremmo spingerla da parte con quell'ascia?",
"È un'ottima idea.",
"Adesso non posso prendere l'ascia. La lattina è sulla strada.",
"Non devi spingere l'ascia!",
"Se riesci a spingere quella lattina da me, io spingerò l'ascia su di essa e tutto andrà bene."}

l3thislooksrealbad={"Questo sembra davvero brutto. Dovrai ricominciare il livello."}

l3ifyousolvesomespecific={"Se risolvi una parte specifica, puoi salvare la posizione.",
"Ma solo se sei sicuro della tua soluzione.",
"Ricorda quello che abbiamo detto sul salvataggio delle posizioni."}

l3youarestandinginmyway={"Sei sulla mia strada lì. Cerca di nasconderti da qualche altra parte.",
"Se lasci che cada e io rimango qui, non riuscirò mai a uscire.",
"Qui? Sono un po' spaventato. Riuscirò a uscire?",
"Aspetta e vedrai."}

l3ishoulndtbe={"Non dovrei pensare a questo."}

l3wewillgiveyouahint={"Ti daremo un suggerimento qui, giocatore. Devi mettere quel libro là, a sinistra.",
"In modo che prenda quel cilindro di acciaio quando lo spingo.",
"Ma non ti diremo come farlo.",
"E se prendo la strada superiore?",
"Shhhh. Questi sono troppi suggerimenti."}

l3justremember={"Ricorda solo che anche se non possiamo spingere gli oggetti lungo la schiena dell'altro, possiamo spingerli verso il basso o su una struttura solida."}

l3thingslikethat={"Cose del genere succedono a volte."}

l3sometimes={"A volte devi pensare molto in anticipo.",
"E spesso devi ricominciare il livello."}

l3weadmitwedid={"Ammettiamo che abbiamo fatto questo apposta.",
"In modo che tu possa provarci di nuovo, questa volta senza suggerimenti."}
