l44={"Qualcuno di Greenpeace dovrebbe vedere questo.",
"Qualcuno di Three Mile Island dovrebbe vedere questo.",
"Cosa dobbiamo farci?",
"Dobbiamo sterminarli tutti. Tali abominii non hanno il diritto di respirare la stessa aria... eh, acqua, che respiriamo noi.",
"Penso che dobbiamo eliminare la causa e non i risultati.",
"Bene, potresti avere ragione.",
"E la causa è indubbiamente questa grande botte. Dobbiamo toglierla in qualche modo.",
"Sì. Mettiamoci al lavoro. Possiamo lasciarla cadere nel cortile del signor B. dopo.",
"Inizio a simpatizzarci.",
"Se solo non ostacolassero così tanto il nostro cammino.",
"Se solo fossero dove li vogliamo.",
"Una così bella collezione di mostruosità. Sarà un peccato lasciarla.",
"Questo non succederà presto.",
"Avrai abbondante tempo per goderne.",
"Se le portiamo con noi e le conserviamo nell'alcol, potremmo fondare uno spettacolo circense.",
"E FDTO non ti basta?",
"Sento tutto!",
"Mi scuso, capo. Non volevo offendere.",
"Sai, ho finalmente capito che anche i mutanti hanno un'anima.",
"Forse anche gli UFO possono avere un'anima.",
"Credo che questo livello ci cambierà per sempre.",
"Questo piede non è solo ripugnante ma anche spudorato. Guardalo.",
"Guarda quella cosa lì. Sembra la stessa eppure diventa sempre più ripugnante.",
"Ma anche quella nasconde qualche istinto materno. Guarda come si prende cura del piccolino.",
"Sì. Quasi l'ha ucciso con una trave di ferro.",
"Questo pesce mi sembra familiare.",
"Forse era in quella foto della centrale elettrica.",
"Povero granchio. Tante chele e nessuna testa. Anzi... forse sta meglio di noi.",
"Nessun uomo è solo con un piccolo anatroccolo giallo. Nemmeno i pesci.",
"Questa volta, il nostro obiettivo è spingere fuori la botte con tutta quella schifezza.",
"Ma dobbiamo lasciare tutte quelle creature qui. Non devono danneggiare il pool genetico della popolazione sana.",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
}

l44end={"Saluti dalla discarica al nostro amato capo.",
"Cordiali saluti",
"Agenti",
"PS: È fantastico essere qui. Ci irradiamo e nuotiamo tanto nel mare. Abbiamo molti nuovi amici.",
}
