l12={"Potrebbe essere la nave del Capitano Silver?",
"Cosa ti fa pensare così?",
"Il titolo di questo livello.",
"Cosa hai sulla testa?",
"Ti sta bene.",
"Grazie, anche a te.",
"Un cappello davvero bello.",
"Anche il tuo Stetson è molto buono.",
"Le mie orecchie stavano gelando.",
"È un cappello fantastico! Posso prenderlo in prestito?",
"Se mi presti il tuo.",
"Sicuramente è un grande Stetson.",
"E anche bellissimo.",
"Credo che dovremo entrare in quella nave.",
"Sembra che dobbiamo entrare dentro.",
"L'aria qui dentro odora di stantio.",
"Sembra che non ci sia nessuno.",
"Non mi piace essere qui.",
"Smhetti di parlare e nuota. Gli interni delle vecchie navi sono luoghi molto interessanti.",
"Riesci a percepire quell'atmosfera di destino e decadenza?",
"No, solo la muffa.",
"Cosa vedi?",
"Vedo molti livelli interessanti che dovremo risolvere.",
}

l12cylinder={"Non riesco a muoverlo.",
"Non riesco a spostare quel cilindro."}
