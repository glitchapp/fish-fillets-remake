l16={"Questa è un'evoluzione. Da batiscafo a carro armato, è come passare da una linea a una rete.",
"Almeno qui non ci sono lumache tanto gentili.",
"Ma questo è un piccolo conforto.",
"Pensandoci, ho sempre voluto vedere l'interno di un carro armato.",
"Sembra che avrai molte opportunità per dare un'occhiata.",
"Mi chiedo come questo carro armato sia finito in mare.",
"Forse era un carro armato anfibio.",
"Un carro armato anfibio? Immagina: nella notte senza luna, i difensori increduli guardano confusi la massa di tubi snorkel emergere dall'onda, cercando invano le imbarcazioni da sbarco...",
"Allora perché chiedi, se sei così maledettamente furbo?!",
"Forse c'è una nave da sbarco affondata nelle vicinanze.",
"È possibile.",
"Pensi che queste munizioni possano farci del male?",
"Non lo so, ma cercherò di tenermi a distanza da esse.",
"-Credo che avremo bisogno di quella scala.",
"-Per arrampicarci fuori? Ma abbiamo solo le pinne.",
"-No. Per tappare quel buco.",}
