l30={"Non ho mai visto coralli viola del genere.",
"Non mi piacciono i coralli viola.",
"Amo i coralli viola...",
"Mi sembrano belli.",
"Spero che ci saranno coralli ancora più interessanti nei prossimi livelli.",
"Che Dio ce ne scampi.",
"Dovremo sollevare questo acciaio...",
"Ciao, granchio!",
"Non cercheremo di tirare su un po' il morale a quel granchio?",
"Ignoralo.",
"Lascia in pace quel granchio.",
"Non vedi quanto è nervoso?",
"Cerchiamo di farcela senza quel granchio nervoso.",
"Quel granchio potrebbe essere utile.",
"Questo granchio ci aiuterà sicuramente a risolvere la situazione.",
}

l30crab={"Chi mi ha svegliato?",
"Lasciatemi in pace, sto dormendo qui!",
"Cosa volete da me?!?",
"Non mi toccare!",
}

l30crabups={
"Ops.",
}
