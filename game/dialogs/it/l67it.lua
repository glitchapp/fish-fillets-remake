l67={"Sono imprigionato sotto il castello.",
	"Come è finito dell'acciaio in questo castello?",
	"Aspetta un attimo, porterò alcuni contadini per aiutarti.",
	"Questi non sono contadini ma peoni.",
	"Pensi che queste miniere contengano il tesoro del Capitano Silver?",
	"Considero ciò altamente improbabile.",
	"Dove siamo finiti di nuovo?",
	"Trovo stranamente familiare questo posto.",
	"Quando MS Word o un'altra noiosa applicazione viene eseguita su questa macchina, noi, i personaggi del gioco per computer, ci riuniamo nella directory C:\\WINDOWS\\CONFIG e parliamo.",
	"Quando non sappiamo cosa fare, giochiamo a nascondino nel registro di sistema. Ecco perché il tuo Windows talvolta si blocca se non giochi a nessun gioco per un lungo periodo di tempo.",
	"Non dovresti rivelarlo. Volevo solo dire che assomiglia molto a questo livello. Così tanti personaggi di videogiochi.",
	"Ma non è così difficile uscirne.",
	"Gli sviluppatori di questo gioco vogliono esprimere la loro ammirazione per l'azienda Blizzard Entertainment e augurargli molti giochi di successo.",
	"E vogliono ringraziarli anche per le tante ore di tempo trascorso in modo divertente.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
}
