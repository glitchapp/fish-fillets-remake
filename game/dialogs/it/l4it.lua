l4={"Questi relitti diventano sempre più arroganti.",
"Queste navi affondate mi irritano davvero.",
"Oh, cavolo, un'altra nave è affondata. Mi prude davvero.",
"Guarda, ha rotto la nostra libreria!",
"Guarda, ha distrutto i nostri scaffali.",
"Guarda, ha invaso la nostra biblioteca.",
"Vedi? Non avresti dovuto comprare così tanti libri.",
"Tu compri ogni libro che riesci a trovare e ora hai dei problemi.",
"Perché devi averne così tanti?",
"ESP",
"Vita dopo vita.",
"Ricordi del futuro.",
"Metodi olistici.",
"Insegnati la telepatia.",
"Veggente controvoglia.",
"Daniken che va oltre.",
"Butterei via questi, ad esempio.",
"I Tre Piccoli Gamberetti",
"Il Sottomarino di Pan di Zenzero",
"Riccioli d'oro e i Tre Astici",
"Squalerella",
"Calamaretta e i Sette Nani",
"Mai! Le mie amate fiabe.",
"Non posso nascondermi qui.",
"Non ci entro qui.",
"Perché abbiamo così tanti libri se non possiamo tirarli fuori in ogni caso?",
"Se facessi qualche esercizio invece di leggere di faccende sciocche, potresti adattarti.",
"Ti ho detto di non comprare quel Dizionario Completo delle Anomalie.",
"Pensiamo. Non possiamo togliere questi libri dallo scaffale.",
"Ci sono solo due oggetti qui che possiamo spostare con risultati.",
}

--[[

"Come è arrivato questo pungiglione qui?",
"Sei sulla strada, strana forma di vita!",
"Sei un ostacolo, strana creatura!",
"Non riesci a spingere quel lumaca verso di me?",
"Come faccio ad arrivare lì attraverso quel metallo?!",
"E se ricominciassi e provassi da capo?",
]]
