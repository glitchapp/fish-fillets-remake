l13={"Questa deve essere una nave vichinga!",
"Sicuro, è un drakkar.",
"Deve essere affondata velocemente. Gli vogatori sono ancora ai loro posti.",
"Forse l'hanno affondata apposta. I guerrieri avrebbero sempre accompagnato il loro capo nel suo viaggio verso Valhalla.",
"È orribile. Sembrano ancora vivi.",
"La musica è sparita!",
"Hanno smesso di suonare!",
"La musica si è fermata di nuovo!",
"Oh no!",
"Di nuovo?",
"Oh no, non di nuovo!",
"Cosa è successo?",
"Di nuovo?",
"Oh no!",
"La maledetta musica si è fermata!",
"Cosa stanno facendo lassù?",
"Come è possibile?",
"Rilassatevi, ragazzi. Solo per Olaf!",
"Non importa, ragazzi. Possiamo cantare da soli!",
"Non importa, ragazzi. Possiamo cantare da soli, come ai vecchi tempi!"
}


l13skull={"Potete non interagire con noi? Stiamo cercando di riposare un po' qui.",
""
}
