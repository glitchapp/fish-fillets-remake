l62={"Dalla tesoreria all'emporio di candele. Questo sì che è progresso.",
	"Devi guardare il lato positivo. Qui non c'è niente che assomigli a un Graal.",
	"Sembra che dovrò attraversare quel terribile labirinto.",
	"Il tuo corpo grasso. Ci ostacoli sempre.",
	"Lasciami in pace, rospo scheletrico.",
	"Basta con le sciocchezze, mucchio malformato di muscoli e squame, va bene?",
	"Mi sento come se fossi in un magazzino di ceramiche.",
	"Intendi dire un magazzino di anfore.",
	"No, dovrebbe essere un magazzino di anfore.",
	"Sai una cosa? Penso che questa missione mi abbia davvero aiutato.",
	"E perché?",
	"Sono arrivato a capire che gioielli e oro non sono che cianfrusaglie economiche.",
	"Non sopporto più queste dannate cose!",
}

--"Potresti riavviare il livello, per favore? Non sembra che ci stiamo capendo molto bene adesso."
