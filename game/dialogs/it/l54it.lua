l54={"Penso che possiamo ottenere informazioni sul propulsore interstellare qui.",
"Questo sembra più un propulsore convenzionale per l'astronave di atterraggio.",
"Allora. Abbiamo trovato il motore. Abbiamo raggiunto uno degli obiettivi della nostra missione.",
"Non essere così frettoloso. Non abbiamo ancora cercato tutto il relitto.",
"Mi dispiace che nessuna di queste meraviglie tecniche intorno a noi funzioni.",
"Mi piacerebbe vedere alcuni di questi aggeggi extraterrestri in azione.",
"Mi chiedo se questo motore potrebbe funzionare sott'acqua.",
"Forse è meglio per noi che non funzioni.",
"Sono piuttosto contento che non possiamo accendere niente qui.",
"Dovremmo essere felici di non aver ancora acceso niente.",
}
l54engineon={"Cosa hai fatto? Spegni quel ruggito!",
"Cosa hai attivato? Dove ci sta portando?",
"È terribile! Spegnilo prima che sia troppo tardi!",
"Non posso! Non riesco a toglierlo!",
"Non so come! Non riesco a toglierlo!",
"Mayday! Mayday!",
"Come posso spegnerlo?!",
"Non riesco a spegnerlo!"
}

l54engineonfish1={"Cosa stai facendo? Dove stiamo andando?",
"Cosa hai attivato? Dove ci sta portando?",
"Non posso! Non riesco a toglierlo!",
"Non so come! Non riesco a toglierlo!",
"Mayday! Mayday!",
"Come posso spegnerlo?!",

}
--"Non riesco a spegnerlo!"
l54engineoff={"Finalmente.",
"Che sollievo.",
"Grazie.",
"Finalmente.",
"Mi preoccupa solo che dovremo riaccenderlo.",
""
}

l54aproachengine={"Cosa stai facendo? Dove stai andando?",
"Attento con quella chiave inglese.",
"Qui non posso fare del male a niente."
}
