l29={"Ecco a voi!",
"Immaginate un po'!",
"Chi avrebbe potuto indovinare?!",
"Che negligenza.",
"Migliaia sono morti: l'intera città è scomparsa tra le onde a causa di una tale incompetenza.",
"Spina scollegata.",
"Un errore imbarazzante.",
"Cosa dobbiamo farci?",
"Possiamo provare a rimetterla al suo posto.",
"E poi? Beviamo l'acqua che è entrata o cosa?",
"Possiamo rimetterla al suo posto come segno del nostro apprezzamento per l'eroico sforzo dei cittadini nel tenerla a galla. Come memoria della loro natura industriosa, abile e... persistente.",
"Di cosa? Dove stai correndo? Devo ordinare otto spade. La Provvidenza stessa ha staccato quella spina. Immagina di sentire cose del genere a casa tua. Giorno dopo giorno.",
"E inoltre: è probabile che qualcuno venga mai qui? Solo le seppie la mordicchieranno occasionalmente.",
"Ci sentiremo bene a riguardo.",
"Ci sentiremo giù a riguardo. Credi che il capo ci crederà? Una spina gigante scollegata? E tu, cosa ne hai fatto? L'abbiamo rimessa a posto. Ehehehe.",
"Potresti avere ragione. Forse sarebbe meglio portarla con noi.",
"Penso anche io.",
"Metiamoci al lavoro.",
"E se lasciassimo quella spina qui?",
"Cosa direbbe l'Agenzia?",
"Non credo che mi interessi ciò che il capo pensa di me.",
"Aspetta. Siamo sicuri che risolveremo.",
"La dannata spina. E se inventassimo qualcosa.",
"Come cosa, ad esempio?",
"Beh, potremmo dire che la città in realtà non è mai esistita.",
"Beh, potremmo dire che la città è affondata perché il ghiaccio artico si è sciolto.",
"Un terremoto potrebbe aver fatto affondare la città, ad esempio.",
"Un tsunami potrebbe aver fatto affondare la città, ad esempio.",
"Potremmo cercare di dire che un vulcano è eruttato al centro della città.",
"È solo una sciocchezza.",
"Questa volta il nostro obiettivo è rimuovere quella spina.",
}

l29end={"Siamo riusciti a scoprire la vera causa del naufragio della città.
Si trattava di un noto manufatto chiamato rilievo atlantico,
finora considerato per errore come una rappresentazione di una visita extraterrestre,
che ci ha dato un indizio.
Allo stesso tempo, raccomandiamo di aumentare la sorveglianza
delle spine su tutti i continenti e sulle isole più grandi
per evitare di ripetere una simile catastrofe imbarazzante in futuro.
}
