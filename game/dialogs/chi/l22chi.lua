l22={"我们一定接近任务的结束了。",
"这肯定是摧毁我们房子的UFO。",
"你确定吗？",
"这才是第八关而已。",
"这些钢缸真是讨厌。",
"这里的钢材太多了。",
"情况可能更糟。",
"我可以想象一个只有钢材的房间。",
"天哪！在这样的关卡里我该怎么办？",
"你可以换着让我走在前面。",
"有趣的是，这些交织在一起的钢材比任何安全系统都更有效地阻止我们进入那个UFO。",
"我一直告诉你，我们的生活受到某种更高、奇怪的意识的控制。",
"我也看到了，但我不太确定它是更高的意识。",
"你没看到吗？一切都发生在这里。",
"我也不知道为什么UFO会撞到我们的房子。",
"一旦我们成功进入，我们能在里面找到什么呢？",
"另一个文明的秘密、新技术、新能源...",
"最重要的是，我们应该试着弄清楚他们为什么来这里。",
"你知道，最让我好奇的是为什么他们偏偏在这里坠毁。",

}
