l16={"這真是一個進步。從深海潛水艇變成坦克，就像從一條線變成了一個網。",
"至少這裡沒有那些那麼殷勤的蝸牛。",
"這只是些微不足道的安慰。",
"想想看，我一直想看看坦克的內部。",
"看來你有很多機會四處觀察。",
"我不知道這輛坦克是怎麼進入海裡的。",
"也許它是一輛兩棲坦克。",
"兩棲坦克？想象一下：在月黑風高的夜晚，困惑不解的防守者們驚愕地看著一大群水上呼吸管從海浪中浮現，徒勞無功地尋找著登陸艇...",
"那你問幹嘛，如果你這麼聰明？！",
"也許附近有一艘沉沒的登陸艇。",
"有可能。",
"你認為這些彈藥會對我們造成傷害嗎？",
"我不知道，但我會試著保持距離。",
"我覺得我們需要那架梯子。",
"爬出來嗎？但我們只有鰭。",
"不是，是用來堵住那個洞的。",
}
