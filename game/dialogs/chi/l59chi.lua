l59={"你看到那只海马吗？",
"它被古瓶阻挡住了。",
"它在古瓶中喝醉了。",
"我想知道里面还有没有什么东西。",
"你可能得自己去检查一下。",
"终于，我能看到一种新的头骨。",
"你注意到那个图腾了吗？那是墨西哥神祇Shelloxuatl。",
"看起来像是。",
"这些古瓶下落得太慢了。",
"嗯，毕竟你不是熊。",
"别忘了我们是在水下。",
"呸。作者本可以省略这个动画。",
"这个图腾对我来说很不错。",
"那个头骨似乎散发着奇怪的东西。",
"它是活的还是某种咒语？",
"",
"",
"",
}

l59skull_canyouall={"你们能不能管好自己的事？我们实际上享受睡觉的时光。",""}

l59skull_wellanother={"嗯，又是一个夜晚因为好奇心而无法安稳入睡。",""}

l59skull_welliwas={"嗯，我正在睡觉，直到被粗鲁地唤醒。",""}

l59skull_anothernight={"又是一个宁静的夜晚被毁了。",""}
