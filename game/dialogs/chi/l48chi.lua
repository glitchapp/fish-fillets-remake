l48={"哦，天啊，这是一个漂亮的厨房。",
"是的，我们发现了船上的厨房。",
"该死的厨房。",
"现在我们在银船长的厨房里。",
"我不认为他自己是个厨师。",
"但这不是我们的问题。",
"这里没有太多问题。我可以立刻游出去。",
"但你看看我。",
"我不明白这些餐桌是怎么来的。",
"这些餐桌挡在路上。",
"这个小房间让我毛骨悚然。",
"我出不去了！",
"我正在思考怎么帮你。",
"以某种方式帮帮我！",
"我在思考中...",
"我正在赶来。",
"呸，这锅里还有些东西！",
"这锅连洗都没洗。",
"这里没人洗碗。",
"如果没有椅子，我们会省下很多功夫。",
"尤其是我自己。",
"一把椅子在厨房里是怎么回事？",
"厨房里不应该有椅子。",
"每个人都想偶尔休息一下。",
"我在某个地方见过这些餐桌。",
"你知道它们是大规模生产的。",
"但它们很漂亮。我们也应该订购一些。",
"恐怕只有我能从这个空间中出去。",
"看来甚至这艘船都有一些棘手的问题。",
"这把剑在这里做什么？",
"也许他们用它来切肉。",
"当然。但通常不会用剑在厨房里切肉。",
"幸运的是这里没有厨师。他们可能会试着把我们煮熟。",
"那张羊皮纸上一定有一些食谱。",
"那张羊皮纸上一定有一些烹饪的秘密。",
"可能是关于如何烹饪鱼的。",
}
