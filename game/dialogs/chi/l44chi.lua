l44={"绿色和平组织的人应该看到这个。",
"三英里岛的人应该看到这个。",
"我们应该怎么处理这个？",
"我们必须消灭它们所有。这些可憎之物没有权利和我们呼吸同样的空气...嗯，水。",
"我认为我们应该消除原因而不是结果。",
"嗯，你可能是对的。",
"而原因无疑是这个巨大的桶。我们得想办法把它弄出去。",
"是的。让我们开始工作吧。之后我们可以把它扔在B先生的前院。",
"我想我开始喜欢它们了。",
"只要它们不挡我们的路就好了。",
"只要它们在我们需要它们的地方就好了。",
"如此可爱的怪物收藏。我很舍不得离开。",
"那不会很快发生。",
"你会有足够的时间来欣赏它们。",
"如果我们带着它们并用酒精保存，我们可以开一个怪物秀。",
"而FDTO对你来说还不够吗？",
"我听到了一切！",
"对不起，老板。我不是故意冒犯的。",
"你知道，我终于意识到，即使是突变体也有灵魂。",
"也许甚至是不明飞行物也有灵魂。",
"我觉得这个层级将永远改变我们。",
"那只脚不仅令人厌恶，还无耻。你看看它。",
"看那个鳞片。它看起来一样，但越来越令人厌恶。",
"但它甚至还怀有一些母性本能。看它如何照顾小家伙。",
"是的。它几乎用铁梁将它砸死。",
"这条鱼对我来说很熟悉。",
"也许它出现在那个核电厂的照片中。",
"可怜的螃蟹。那么多钳子，却没有头。虽然...也许它比我们过得更好。",
"没有人会独自一人和一只小黄鸭在一起。即使是鱼。",
"这一次，我们的目标是用那个肮脏的东西推出桶。",
"但我们必须把所有这些生物留在这里。它们不能伤害健康人口的基因库。",
}

l44end={"向我们亲爱的老板致以垃圾堆的问候。",
"诚挚地",
"特工们",
"附言：在这里感觉很棒。我们辐射自己，经常在海里",

游泳。我们结交了许多新朋友。",
}
