
function Obey.lev57()

  loadcorrespondentfonts()

 Talkies.say( "small fish",
    {
  l57[1],
  l57[2],
  l57[3],
  },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

  Talkies.say( "Big fish",
    { 
  l57[4],
  l57[5],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 Talkies.say( "small fish",
    {
  l57[6],
  },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l57[7],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l57[8],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l57[9],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l57[10],
  l57[11],
  l57[12]
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
      Talkies.say( "Big fish",
    { 
  l57[13],
  l57[14],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l57[15],
  l57[16],
  l57[17],
  l57[18],
  l57[19],
  l57[20],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Big fish",
    { 
  l57[21],
  l57[22],
  l57[23],
  l57[24],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l57[25],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    
     Talkies.say( "Big fish",
    { 
  l57[26],
  l57[27],
  l57[28],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l57[29],
  l57[30],
  l57[31],
  l57[32],
  l57[33],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
end

function Obey.lev57aproach3eyes()
Talkies.say( "small fish",
    {
  l57aproach3eyes[1],
  l57aproach3eyes[2],
  },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

  Talkies.say( "Big fish",
    { 
  l57aproach3eyes[3],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 Talkies.say( "small fish",
    {
    l57aproach3eyes[4], 
  },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  Talkies.say( "Big fish",
    { 
  l57aproach3eyes[5],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
