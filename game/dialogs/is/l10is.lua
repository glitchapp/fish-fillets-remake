l10={"Líttu. Partíbáturinn.",
"Ég hef tilfinningu að þetta verði ekki venjulegt veisluver.",
"Þú og þínar tilfinningar. Vissulega er hann fullur af góðhjartaðum og vingjarnlegum fólki.",
"Kannski hafðirðu rétt fyrir þér. Þetta er undarleg veisla.",
"Trúirðu að þeir geti komist út úr þessum báti?",
"Ég vona að þeir geti ekki komist út úr þeim báti.",
"Hvað ef þeir hefja eldri við okkur?",
"Ég tel að þeir geti ekki náð okkur. Beinagrindur geta ekki notið vel á svæði.",
"Hættu að hugsa um það og hreyf fjöðrunum þínum svo við getum komist hérna út.",
"Viltu styðja þennan stálhnoðra með glasinu?",
"Ég get ekki gert neitt annað fyrir þig.",
"Varúð við að skvetta því!",

}
