l3={"Þú þekkir reglurnar þegar þær eru nú þegar.",
"Nú getur þú æft þær.",
"Við munum gefa þér einhverar vísbendingar stundum.",
"Ég má ekki synda niður núna.",
"Og ég get ekki farið upp heldur.",
"Hey, skipuleggja það upp þarna á einhvern hátt.",
"En var þú varkár!",
""
}

l3icantmove={"Ég get ekki fært þetta. Þetta er úr stáli."}

l3comeback={"Kom aftur, þú getur ekki farið lengra!"}

l3thisistricky={"Hmm... Þetta er erfitt.",
"Ef við færum hunanginn út úr leiðinni, munum við verða brotnir af þeirri krukku af berjamos.",
"Gætum við ekki fært hann út úr leiðinni með þeim öxi?",
"Það er góð hugmynd.",
"Nú get ég ekki tekið öxina. Krukkan er í vegi.",
"Þú darfst ekki ýta á öxina!",
"Ef þú getur ytt þeirri krukku til mín, mun ég skella öxinni á hana og allt mun vera í góðu lagi."}

l3thislooksrealbad={"Þetta lítur alvarlegt út. Þú þarft að byrja upp á nýtt."}


l3ifyousolvesomespecific={"Ef þú leystir einhvern tiltekinn hluta getur þú vistað staðinn.",
"En bara ef þú ert viss um lausn þína.",
"Mundu það sem við höfum sagt um að vista staði."}

l3youarestandinginmyway={"Þú stendur í vegi mínum þarna. Reyndu að falast einhvers staðar annars staðar.",
"Ef þú dettur þvína og ég verð hér, mun ég aldrei komast út.",
"Hérna? Ég er lítið hrædd. Mun ég komast út?",
"Bíddu bara og sjáðu."}

l3ishoulndtbe={"Ég ætti ekki að hugsa um þetta."}

l3wewillgiveyouahint={"Við munum gefa þér vísbendingu hér, leikmaður. Þú verður að setja það bók þarna til vinstri.",
"Svo að hún grípi þennan stálhylki þegar ég hrifsa það.",
"En við munum ekki segja þér hvernig þú gerir það.",
"Hvað ef ég færi yfir með það?",
"Shhhh. Það er of margar vísbendingar."}

l3justremember={"Mundu bara að þótt við getum ekki hrifsa hluti á bak við annan, getum við hrifsa þá niður eða á einhvern fastann byggingarhlut."}

l3thingslikethat={"Svona hlutir gerast stundum."}

l3sometimes={"Stundum þarft þú að hugsa langt fram í tímann.",
"Og oft þarft þú að byrja upp á nýtt þá."}

l3weadmitwedid={"Við viðurkennum að við gerðum þetta með vilja.",
"Þannig að þú getir reynt það aftur - þetta skipti án nokkurra vísbendinga."}
