l27={"Ég er fanginn hér.",
"Ég vil komast út.",
"Hvernig get ég komist út héðan?",
"Ég er lokaður í herbergi án hurða!",
"Reyndu að leysa upp einhver stein í veggnum.",
}

l27door={
"Sjáðu, nú komstu út.",
"Svo, við erum saman aftur.",
"Er það ekki lítið undarlegt krabbið?",
"Hvaða þaðan talar þú um?",
"Hinum ofari, auðvitað.",
"Hinum neðra, auðvitað.",
"Ég tel það vera venjulegt.",
"Já, það er smá skrítið.",
"Við eigum að gleðjast yfir að hafa allar þessar boltar.",
"Þessi borg líkist áhugaverðu vel varðveitt...",
"Mér finnst ég vera að missa eitthvað.",
"Þínar tilfinningar... hugsaðu um hvernig við komumst út.",
"Nei, bíddu, horfðu á þessa krabbamein... horfðu í augun þeirra...",
"Augun þeirra hreyfast samhæft. Eins og þau séu stjórnuð af sameiginlegri yfirmeðvitund.",
"En auðvitað, þau eru stjórnuð af tölvuverkefni. Eins og bylgjandi vatnsins og fávitarlegt orðspor þitt.",
"Þáttumst við nokkra skulptúr sem hafa verið þögn í svo langan tíma?",
"Ég held að þau bíði bara eftir bestu tækifærinu.",
}
