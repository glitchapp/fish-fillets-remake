l2={"Jæja, ekki aftur...",
"Þú veist að við getum ekki gert neitt í því.",
"Látum okkur færa það niður svo við getum skoðað það.",
"Allt í lagi, komumst að verki.",
}

l2thediskself={"Hvernig? Diskurinn eyðileggst af sjálfum sér.",
"Svo, nú getum við loksins byrjað.",
"Fyrsta verkefnið okkar er að komast út úr þessu herbergi.",
"Þetta verður fyrsta verkefnið okkar í öllum hinum herbergjum einnig.",
"Komdu nú, tökum leikmanninn með okkur í verkstæðið og sýnum honum reglur um öryggi í vinnunni.",
"Góð hugmynd.",
"Í fyrsta skiptið, ekki snertu neitt, horfðu bara og lærdómstaktu. Við munum sýna þér hvað þú átt og átt ekki að gera með okkur og hvaða hluti við getum.",
"Fyrir innganginn í verkstæðið, látum okkur vista leikinn - þú þarft bara að ýta á F2 hnappinn.",
"Fyrst munum við sýna þér hvað getur skaðað okkur.",
"Ég frivillast til að vera dúmpillinn.",
"Fyrst og fremst, við eigum ekki að dettast á hver annan hlut.",
"Við getum ekki útsverð til hafs með hlut á baki, því hann myndi detta á okkur.",
"Annað sem við eigum ekki að gera er að ýta hlutum á baki okkar.",
"Sumir hlutir eru þó lögun þannig að við gætum haft þá og ýtt þeim á sama tíma - en það er okkur ekki heimilt.",
"Við getum ekki hengt viðbótarmyndum á hrúgu sem einhver af okkur ber.",
"Og ólíkt stærri samstarfsmanni mínum, get ég ekki fært eða jafnvel borið járnhluti.",
"Við getum alltaf tekið hluti upp og látið þá detta.",
"Við getum ýtt hlut á bak hvers annars aðeins ef hluturinn verður síðan þrýstur á annan stuðning...",
"... og jafnvel látið það detta á annan okkar í þessu tilfelli.",
"Ég get jafnvel gert þetta einn... Ég get farið undir hann... eða sett hann á þennan leðurhól.",
"Við getum einnig ýtt einum hlut á topp annars hlutar sem við styðjum, en við getum ekki fært þann hlut sem er beint á bakið okkar.",
"Við getum einnig sundið frjálst undir hlut sem við styðjum og flutt það yfir til hinna fisksins.",
"Og við getum ýtt hlutum af bak hvers annars, svo framarlega sem hluturinn dettur niður strax.",
"Að tilraunarlöngu máli, við getum einungis lyft hlutum, látið þá detta, ýtt þeim á þverið yfirborð, ýtt þeim á yfirborð og ýtt þeim af bak hvers annars.",
"Það er það um reglurnar. Ef þú vilt vita meira, ýttu á F1 og lesaðu hjálpargreinina.",
}

l2wecouldrestart={"Við gætum byrjað um borð.",
"Það er rétt...",
}

l2icantliftit={"Ég get ekki lyft því. Getur þú reynt?",
"Bara smá hærra svo ég geti sundið undir þér.",
}

l2lethisbytheway={
"Áttum við að horfa á það aftur?",
}

l2nowell={"Nú byrjum við aftur - eða við getum hlaðið vistaða leiknum með því að ýta á F3 hnappinn.",
"Og nú erum við aftur þar sem við vistaðum leikinn.",
}

l2again={"Aftur, við hleðjum vistaða leik með því að ýta á F3 hnappinn.",
}

l2wehaveloaded={"Við höfum hlaðið inn vistaða leiksins fyrir síðasta sinn. Nú munum við sýna þér allt það sem við getum.",
}


l2briefcasetalkie={"Góðan dag, fiskur.",
"Þetta er mál af mestri mikilvægi og því höfum við valið þig,",
"fæmstu undirvatsáttmála okkar.",
"Þú ert meðlimur í FDTO - Fiskadeild leynidetektífa -",
"þau komu í eign á nokkrum amatør myndum af framandi fyrirbæri",
"sem hefur steypt af sér á einhverjum stað í nágrenninu þínu.",
"Það verkefni sem þú færð að taka á þér,",
"verður að endurheimta flaugina og afla þér allrar upplýsingar sem þú getur um stefnu og eiginleika milligalaksíustöðvunar.",
"Þú átt einnig að reyna að leysa nokkrar af verkefnum okkar sem eru enn óleyst.",
"Okkur langar aðallega að vita umstæðurnar við söfnun frásagna um sökkun mýtuðu borgarinnar, sem tilviljun býður í sömu svæðinu.",
"Þetta mál gæti tengst dularfullu þjóðhverfinu Bermuda-hringnum.",
"Finndu út af því hvers vegna svo margar skip og flugvélar hafa horfið í þessu svæði í mörg áratugi.",
"Eitt af þeim tapuðu skipum er sögð að hafa tilheyrt legendarísku Herra Silfurkapteini.",
"Það eru enn mörg óvissanatriði sem umhverfa þennan fræga hræðilega.",
"Aðallega langar okkur að fá aðgang að kortinu sem sýnir staðsetningu jarðfæranna hans.",
"Eitt af þínum mikilvægustu verkefnum er að finna tölvuna sem falin er í djúpinu af ákveðinni glæpdreifingarstofu.",
"Hún geymir upplýsingar um verkefni sem gætu breytt heiminum alveg.",
"Þú átt einnig að finna og handtaka dularfulla kóralltúrtla.",
"Hún sleppur úr skurðgöngunum hjá FDTO.",
"Hún er ekki bewaffnuð, en hún á að hafa talandi hæfileika.",
"Okkur hefur berast tilkynning um að tiltekinn kjarnorkuvirkjanir hafi haft ólöglega útskot á því úrgangi sem hann framleiðir.",
"Kannaðu það. Og gleym ekki að finna helga graal.",
"Og eins og venjulega: Ef einhver í liðnum þínum er særður eða látinn",
"mun Altar neita þekkingu á tilvist þinni og verkefnið mun byrja upp á nýtt.",
"Þessi diskur mun sprengjast í fimm sekúndum.",
}


