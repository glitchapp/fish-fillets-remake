l1={"Hvað var það?",
"Ég hef enga hugmynd.",
"Við ættum að fara út og skoða.",
"Bíddu! Ég fer með þig.",
"Hvers vegna er ekkert að gerast?",
"Halló, leikmaður!",
"Þú getur stjórnað okkur með örhnappunum og notað bilaborð til að skipta um fisk.",
"Eða þú getur stjórnað okkur með músinni -- smellt á einhver stað og...",
"...ef við getum komist þangað án þess að færa neitt...",
"...þá munum við fylgja smelli þínum.",
"Og þegar þú smellar með hægri músartakkast, munum við reyna að komast þangað og koma í veg fyrir öll hindrunar sem erum á leiðinni.",
"Mörkin þín ættu að vera að fá okkur báða út.",
"Og passaðu að ekki meiða okkur. Ef þú dettur einhverju á okkur eða...",
"Æ! Hættu það. Við segjum honum það í næsta þrepi.",
"Allt í lagi.",

"Jæja, þú færð það að hreyfa! Ég gæti aldrei gert það!",
"Takk, nú get ég farið með þig.",
"Leiðinlegt, ég komst ekki í gegnum.",
"Ég verð að hafa orðið þyngri.",
"Gætirðu ýtt stólnum smá til vinstri?",
"Úps! Það var mistök. Báðir okkar þurfa að geta farið út til að halda áfram.",
"Það er engin leið sem ég get farið út núna. Þú verður að endurræsa þrepið.",
"Þú verður að ýta á langa örhnappinn á lyklaborðinu þínu.",
"Menn kalla það aftur-takkann.",
"Óháð því. Það var fyndið á tæksku.",
"Endurræsingarvalmyndin er einnig á stýriborðinu.",
"Ef þú heldur að þú viljir frekar endurræsa þrepið, engin mál.",
"Þú þarft bara að ýta á langa örhnappinn á lyklaborðinu þínu.",
"Ég er þungt járnhylki. Það er ekki auðvelt að færa mig um. Slíkur litill fiskur, eins og sá appelsínugula þar, hefur einfaldlega enga möguleika. Ég gæti knúið hann án nokkurs vandkvæðis.",
}

l1icantgetthrough={"Ég komst ekki í gegnum hérna. Það er járnhylkurinn í vegi.",
"Ég get ekki fært þennan hylki. Getur þú hjálpað mér?",
"Engin mál...",
}

l1wowyoumovedit={
"Jæja, þú færð það að hreyfa! Ég gæti aldrei gert það!",
"Takk, nú get ég farið með þig.",
}

l1damnit={"Leiðinlegt, ég komst ekki í gegnum.",
"Ég verð að hafa orðið þyngri.",
"Gætirðu ýtt stólnum smá til vinstri?"
}
