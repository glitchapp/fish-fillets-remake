l30={"Ég hef aldrei séð slíkar fjólubláar korallar.",
"Mér líkar ekki við fjólublaðar korallar.",
"Ég elska fjólublaða korallana...",
"Þeir líkjast mér.",
"Ég vona að það muni koma enn áhugaverðari korallar í komandi stigum.",
"Guð fyrbyrði.",
"Við munum þurfa að lyfta þessu járni...",
"Halló, krabbinn!",
"Viljum við ekki reyna að gleðja þessa krabbu lítið?",
"Hunsum því.",
"Skiljaðu þessa krabbu í frið.",
"Sérðu ekki hversu taugaveik krabbinn er?",
"Reyndu að komast án þessar taugaveiku krabbu.",
"Þessi krabbi getur verið gagnleg.",
"Þessi krabbi mun vissulega hjálpa okkur að leysa það.",
}

l30crab={"Hver vöknaði mér?",
"Sleppu mér, ég er að sofa hér!",
"Hvað viltu frá mér?!?",
"Snertu mig ekki!",
}

l30crabups={
"Úps.",
}
