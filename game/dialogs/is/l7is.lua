l7 = {
    "Djöfull, hræðslan valdi steinhrúgu!",
    "Ég get ekki farið í gegnum þessa steinhrúgu,",
    "við þurfum að finna annan útgang.",
    "Þetta er ekki leiðin.",
    "Ég mun ekki klára þetta einn.",
    "Ég ætti kannski að hafa lyft þeirri skel fyrst.",
    "Kannski þurfum við að skipta um þessi hlutun.",
    "Eitt rúm. Besta sem þú getur fengið í undir vatni.",
}

l7part2 = {
    "Nú getum við lét snigilinn detta á rúminu.",
    "Það verður erfitt að taka snigilinn upp þaðan.",
    "Loksins, hann er þar.",
    "Bekkjarfíflinn...",
    "Við ættum að leita í koralrífum.",
    "Þar verða margir spennandi verur að skoða.",
    "Þurfum við ekki smásjá til að skoða kórall?",
    "Já, þau eru smá. En þar geta verið önnur lífefni.",
    "Kórallaskjaldbökur til dæmis.",
    "Og að auki hef ég tilfinningu að það sé smásjá í kafaldsfari.",
}
