l22={"Við þurfum að vera nálægt enda á verkefni okkar.",
"Þetta er vissulega flaug sem eyddi húsinu okkar.",
"Ertu viss?",
"Þetta er bara áttunda stig.",
"Þessir járnssúlur eru frekar óþægilegar.",
"Hér er of mikið af járni.",
"Það gæti verið verri.",
"Ég get t.d. ímyndað mér herbergi þar sem er ekkert nema járn.",
"Jæja! Hvað á ég að gera í slíku stigi?",
"Þú myndir láta mig fara fremst, fyrir breytingu.",
"Það er áhugavert að þetta flæðistíg hindrar aðgang okkar í þá flaug skynsamlega betur en hvaða öryggismálamiðlun sem er.",
"Ég segi þér alltaf að lífið okkar sé stýrt af einhverri hærra, undarlegri meðvitund.",
"Ég sé það líka. En ég er ekki viss hvort hún sé hærra.",
"Sáttirðu það ekki? Allt gerist bara hérna.",
"Ég veit ekki af hverju flaugarnar halda áfram að brotna inn í hús okkar.",
"Hvað getum við fundið þar inni, þegar við náumst þangað?",
"Leyni annarar menningar, nýjar tækni, ný orkulindir...",
"Mest allt að því leyti ættum við að reyna að komast að því hvers vegna þær komu yfirhöfuð.",
"Veistu, ég undrast mest af öllu af hverju þær brotnaðu bara inn akkurat hérna.",
}
