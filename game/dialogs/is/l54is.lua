l54={"Ég tel að við getum aflað upplýsinga um millistjarnarflugvélina hér.",
"Þetta líkist meira venjulegri framdrift fyrir lendingarförina.",
"Svo. Við fundum það sem getur hratt flugvélina. Við höfum náð einu af markmiðum okkar.",
"Ekki vera svo fljóttur. Við höfum ekki leitað í gegnum heilan ruslið ennþá.",
"Mér þykir það leitt að enginn af þessum tæknifundum í kringum okkur virki.",
"Mig langar að sjá sumar þessara geimveru-dulmagnanna í hreyfingu.",
"Ég undrar hvort þessi hreyfill gæti virkað undir vatni.",
"Kannski er betra fyrir okkur að hann virki ekki.",
"Ég er frekar ánægður með að við getum ekki skotið neinu á hér.",
"Við ættum að gleðjast yfir að við getum ekki kveikt á neinu ennþá."
}
l54engineon={"Hvað ert þú að gera? Slökkva á þessari hávaða!",
"Hvað hefur þú virkjað? Hvert er það að berast okkur?",
"Þetta er hættulegt! Slökkva á því áður en það er of seint!",
"Ég get ekki! Ég get ekki fengið það úr!",
"Ég veit ekki hvernig! Ég get ekki fengið það úr!",
"Mayday! Mayday!",
"Hvernig get ég slökkt á því?!",
"Ég get ekki slökkt á því!"
}

l54engineonfish1={"Hvað ert þú að gera? Hvert er þú að fara?",
"Hvað hefur þú virkjað? Hvert er það að berast okkur?",
"Ég get ekki! Ég get ekki fengið það úr!",
"Ég veit ekki hvernig! Ég get ekki fengið það úr!",
"Mayday! Mayday!",
"Hvernig get ég slökkt á því?!",

}
--"Ég get ekki slökkt á því!"
l54engineoff={"Loksins.",
"Hvað er þetta góð tilfinning.",
"Takk.",
"Loksins.",
"Ég er bara hræddur við að við þurfum að kveikja aftur.",
""
}

l54aproachengine={"Hvað ert þú að gera? Hvert ert þú að fara?",
"Varúð með þessa röskunartólur.",
"Ég get ekki skemmt neinu hér."
}
