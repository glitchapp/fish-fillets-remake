l8 = {
    "Þvottahúsin eru uppáhaldsstaður Davíðs.",
    "Hvað?",
    "Ertu ekki kunnugur við Davíð? Hann er einn af listamönnum sem unnu að þessu leik.",
    "Davíð var engin grafískur listamaður. Hann var höggmyndari. Höggmyndirnar hans eru einnar af þekktustu miðaldalistaverkum.",
    "Þú ert alveg að ruglast.",
    "Sagði ég þér ekki að VIÐ þurfum ekki þvottahús sem skolpar? Og með því að vera skítugt, að því gefnu!",
    "Þvottahúsin eru hreinari og þegar þau eru undir vatni eru þau jafnframt umhverfisvænni.",
    "Sem betur fer, ég þarf ekki að koma mér inn í þau.",
    "Ég finnst ekki alveg eins og hér. Ég finnst eins og ég sé á kirkjugarði.",
    "Hvað þýðir það?",
    "Veistu ekki hvar svo margar akvaríumfiskar enda líf sitt?",
}
