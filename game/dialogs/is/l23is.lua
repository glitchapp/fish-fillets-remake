l23={"Ég held að við séum loksins á sporinu af þessari dularfullu borg.",
"Hún virðist dásamleg.",
"Nú þurfum við bara að finna út af því hvers vegna hún sökk og við erum komnir.",
"En það gæti tekið okkur smá stund.",
"Slepptu mér. Ég dýrka klassísku fegurð þessa hofs.",
"Hver situr á þeirri stóli?",
"Það er afrit af Zeus af höndum Feidíos. Eitt af sjöundu undrum heimsins.",
"Hmm. Það voru önnur öld.",
}

l23zeus={"Varðandi það! Reynið að skemma það ekki!",
"Slík eyðsla!",
"Og nú lágstu það mjög rólega niður.",
"Þú berjinn! Geturðu ekki verið smá varkár?!",
"Mér fannst ekki vel við þessa skúlptúr heldur.",
}
