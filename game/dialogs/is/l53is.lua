l53={"Sjáðu. Annar orkuheimild.",
"Ekki kvíða. Þessi segull er hér til að hindra okkur aðeins.",
"Ég tel að við þurfum það.",
"Þetta er mjög undarlegur hlutur.",
"Ég tel að fljúgaþilin verði full af slíkum undrum.",
"Hættu að þróa kenningarnar þínar og byrjaðu að leysa.",
"Þetta líkist útvarpi.",
"Hvernig gæti útvarp komist hingað?",
"Láttu það vera.",
"Þetta er undarlegt útvarp.",
"Hvað höfum við hér?",
"Mér líst þetta vera skammbyssa.",
"Mögulega er þetta ljósbyssa.",
"Það skiptir engu máli. Við þurfum hana ekki.",
"Hættu að vísa á mig með henni!",
"Sem þú vissir ekki að þú gætir ekki snúið hlutum í þessu leiknum.",
"Hér er of mikið af járnförum.",
"Ekki gleyma því að við erum innan fljúgaþilsins!",
"Mögulega gæti ég lært að stjórna þessum járnhlutum.",
"Nei, ég get ekki fært það.",
"Það er mjög þröngt hér.",
"Þessir segull eru að gera mig hræddan.",
"Þessi segullgæði hafa sennilega einhver áhrif á líkamsorku.",
"Það gagnar mér ekki.",
}
