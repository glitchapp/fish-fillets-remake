
function Obey.lev59()

  
  loadcorrespondentfonts()


 Talkies.say( "small fish",
    {
  l59[1],
  },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l59[2],
  l59[3],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l59[4],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
  l59[5],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l59[6],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
      Talkies.say( "Big fish",
    { 
  l59[7],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
  Talkies.say( "small fish",
    {
  l59[8],
  l59[9],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Big fish",
    { 
  l59[10],
  l59[11],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l59[12],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
   
    Talkies.say( "Big fish",
    { 
  l59[13],
  l59[14],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l59[15],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      
     Talkies.say( "bot-x",
    { 
  l59[16],
  l59[17],
  l59[18],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
   
end

function Obey.lev59skulll_canyouall()

Talkies.say( "skull",
    {
		l59skull_canyouall[1],
		l59skull_canyouall[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev59skulll_restlesssleep()

Talkies.say( "skull",
    {
		l59skull_wellanother[1],
		l59skull_wellanother[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev59skulll_welliwas()

Talkies.say( "skull",
    {
		l59skull_welliwas[1],
		l59skull_welliwas[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev59skulll_anothernight()

Talkies.say( "skull",
    {
		l59skull_anothernight[1],
		l59skull_anothernight[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
