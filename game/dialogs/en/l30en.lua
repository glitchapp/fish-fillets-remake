l30={"I have never seen such violet corals.",
"I don’t like violet corals.",
"I love violet corals...",
"They look nice to me.",
"I hope there will be even more interesting corals in the levels to come.",
"God forbid.",
"We will have to lift this steel...",
"Hi, crab!",
"Won’t we try to cheer that crab up a little?",
"Ignore it.",
"Leave that crab alone.",
"Can’t you see how nervous it is?",
"Try to do without that nervous crab.",
"That crab could be useful.",
"This crab will surely help us to solve it.",
}

l30crab={"Who woke me up?",
"Leave me alone, I am sleeping here!",
"What do you want from me?!?",
"Don’t touch me!",
}

l30crabups={
"Ooops.",
}
