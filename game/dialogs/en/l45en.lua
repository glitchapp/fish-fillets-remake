l1={"Crrruel, crrruel, crrruel Captain Silverrrr...",
"Ay... caramba!!!",
"Crrruel Captain Silverrrr...",
"Trrreasurre, you foools...",
"Good grrrief!",
"That’s rrrrrridiculous! Polly is his masterrr’s darrrling.",
"Don’t ppppush me!",
"That’s betterrr!",
"And it’s currrtains forrr you!",
"I’m going to be in yourrr way herrre.",
"Trrricky prrroblem, isn’t it?",
"Dang it!",
"You’rrre too fat, you can’t make it thrrrrough.",
"Polly wants a crrracker!",
"Beat it, sirrr...",
"Leave me alone and get lost!",
"What could be inside?",
"Who knows...",
"Could this be the legendary Silver’s treasure?",
"In such a small chest? The boss surely wouldn’t contact us for such a little case.",
"Yuck!",
"Thanks.",
"No problem.",
"What are you doing?!",
"I’m sorry. I’ll be more careful next time.",
"Be careful! Don’t hurt it!",
"It’s more the other way around!",
"That’s some octopus! I wonder who’s her owner?",
"Maybe it belonged to the person who lived in this cabin.",
"Whoever lived here sure was weird.",
"Why?",
"Look, he had a human skull here!",
"Well, maybe it’s his own skull.",
"You think so?",
"Can you sense the atmosphere of rot and decay? The sunken wreck, the parrot’s skeleton, the human skull, and the broken cradle...",
"Where do you see the cradle?",
"Oh, you don’t have any feeling for poetry at all...",
}

l45skull={"Well I was asleep, until I was so rudely awakened.",""}
