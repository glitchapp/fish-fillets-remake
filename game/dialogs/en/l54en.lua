l54={"I think we can gain some information about the interstellar propulsion here.",
"This looks more like conventional propulsion for the landing craft.",
"So. We found the drive. We have achieved one of the objectives of our mission.",
"Don’t be so hasty. We haven’t searched the whole wreck yet.",
"I am sorry that none of these technical marvels around us work.",
"I’d like to see some of these extraterrestrial gizmos in action.",
"I wonder if this motor could work under water.",
"Maybe it’s better for us if it doesn’t work.",
"I am rather glad we cannot turn anything on here.",
"We should be happy that we could not switch anything on yet.",
}
l54engineon={"What have you done? Turn off that roar!",
"What have you activated? Where is it taking us?",
"This is terrible! Turn it off before it’s too late!",
"I can’t! I can’t take it out!",
"I don’t know how! I can’t take it out!",
"Mayday! Mayday!",
"How can I turn it off?!",
"I can’t turn it off!"
}

l54engineonfish1={"What are you doing? Where are we going?",
"What have you activated? Where is it taking us?",
"I can’t! I can’t take it out!",
"I don’t know how! I can’t take it out!",
"Mayday! Mayday!",
"How can I turn it off?!",

}
--"I can’t turn it off!"
l54engineoff={"Finally.",
"What a relief.",
"Thanks.",
"Finally.",
"I am only afraid we’ll have to turn it on again.",
""
}

l54aproachengine={"What are you doing? Where are you going?",
"Careful with that wrench.",
"I can’t harm anything here."
}

