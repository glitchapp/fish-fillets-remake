l2={"Oh no, not again...",
"You know there’s nothing we can do about it.",
"Let’s push it down so we can take a look at it.",
"Well, let’s get to work",
}

l2thediskself={"How? The disk self-destructed.",
"So, now we can finally get started.",
"Our first task is to get out of this room.",
"This, by the way, is going to be our task in all the other rooms as well.",
"Come on, let’s take the player into the workshop and show him the work safety regulations.",
"Good idea.",
"For now, don’t touch anything, just watch and learn. We’ll show you what you should and shouldn’t do with us as well as what things we’re capable of.",
"Before entering the workshop, let’s save the game - just press the F2 key.",
"First we’ll show you what can hurt us.",
"I’ll volunteer to be the dummy.",
"First of all, we shouldn’t drop things on each other.",
"We also can’t swim downwards when carrying an object, because it would fall on us.",
"Another thing we must not do is push objects along our back.",
"Some objects are shaped in such a way that we could hold them and push them at the same time - but we’re not allowed to do this either.",
"We also can’t drop additional objects on a pile that one of us is carrying.",
"And unlike my bigger partner, I can’t move or even carry steel objects.",
"We can always pick up objects and then let them fall.",
"We can push an object along each other’s back only if the object will then be pushed onto another support...",
"... and even let it drop on each other in this case.",
"I can even do this alone... I can swim down under it... or put it on this ledge.",
"We can also push one object along the top of another object which we are supporting, but we can’t move the object that is directly on our back.",
"We can also swim freely under an object which we are supporting and pass it to the other fish.",
"And we can push objects off of each other’s back, as long as the object falls down immediately.",
"In summary, we can only lift objects, let them drop, push them along a solid surface, push them onto a surface, and push them off of each other.",
"That’s about it for the rules. If you want to know more, press F1 and read the help section.",
}

l2wecouldrestart={"We could restart the level.",
"That’s true...",
}

l2icantliftit={"I can’t lift it. Can you give it a try?",
"Just a little bit higher so I can swim underneath you.",
}

l2lethisbytheway={
"Should we watch it again?",
}

l2nowell={"Now we’ll start again - or we can load the saved game by pressing the F3 key.",
"And now we’re back where we last saved the game.",
}

l2again={"Again, we load a saved game by pressing the F3 key.",
}

l2wehaveloaded={"We have loaded our saved game for the last time. Now we’ll show you all the things we’re capable of.",
}



l2briefcasetalkie={"Good morning, fish.",
"This is an affair of the gravest importance and so we have chosen you,",
"our ablest underwater agents.",
"Agents of FDTO - Fish Detective Training Organization -",
"managed to get hold of several amateur snapshots of an extraterrestrial object",
"which has crashed somewhere in your vicinity.",
"Your mission if you choose to accept it,",
"will be to recover the UFO and acquire all the information you can about the principles and nature of interstellar propulsion.",
"You should also try to close some of our as yet unsolved cases.",
"We are mostly interested in the circumstances surrounding the sinking of the mythical city, by chance in the same area.",
"This case may be connected to the mystery of the Bermuda Triangle.",
"Find out why so many ships and planes have disappeared in this area over the past several decades.",
"One of the lost ships is rumored to have belonged to the legendary Captain Silver.",
"There are still many uncertainties surrounding this famous pirate.",
"Most of all we are interested in the map which shows the location of his buried treasure.",
"One of your most important tasks is to find the computer hidden in the deep by a certain criminal organization.",
"It contains data about a project which could change the entire world.",
"You should also find and detain the mysterious coral turtle.",
"It escaped from the FDTO interrogation facility.",
"It is not armed, but it supposedly has telepathic abilities.",
"We have been notified that a certain nuclear power plant has been dumping its radioactive waste illegally.",
"Check it out. And don’t forget to find the holy grail.",
"And as usual: If anyone from your team is injured or killed,",
"Altar will deny any knowledge of your existence and the level will be restarted.",
"This disk will self-destruct in five seconds.",
}
--"five, four, three, two, one"

