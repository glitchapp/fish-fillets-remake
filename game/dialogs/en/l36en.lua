l36={"It is lovely here.",
"So many pearls.",
"There are so many pearls here.",
"Listen... Don’t forget our mission!",
"We are no pearl-hunters.",
"Remember! We have a mission to complete.",
"You are just sitting up there and I have to do all the work myself!",
"Didn’t you say that you like the pearls?",
"Aren’t you happy with so many pearls?",
"I need one more pearl.",
"Nope.",
"One more pearl, please.",
"My dearest friend, couldn’t you possibly lend me just one little pearl?",
"I wonder if it wouldn’t be easier to shorten that steel pipe.",
"Cleopatra is rumored to dissolve pearls in vinegar.",
"Do you think we have too many of them?",
"Well, I just wanted to educate you a little.",
"Watching you, I have to think about how I played marbles with tadpoles.",
"You haven’t got a spare marble in your pocket, do you?",
}
