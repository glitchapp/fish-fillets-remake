l10={"Look. The party boat.",
"I have a feeling that this is not going to be a normal picnic.",
"You and your feelings. Surely it’s full of kind and friendly people.",
"Maybe, you were right. This is a strange picnic.",
"Do you think they could get out of this boat?",
"I hope they can’t get out of that boat.",
"What if they start to pursue us?",
"I don’t think they could get us. Skeletons can’t swim very well.",
"Stop thinking about it and move your fins so that we can get out of here.",
"You want to support that steel tube with a glass?",
"I can’t do anything else for you.",
"Be careful not to spill it!",

}
