l59={"Can you see that seahorse?",
"It is blocked by amphoras.",
"It got itself drunk among the amphoras.",
"I wonder if there is anything left in them.",
"You probably have to go check for yourself.",
"Finally, I can see some new kind of skull.",
"Did you notice that totem? It is the Mexican god Shelloxuatl.",
"It looks like it.",
"These amphores fall unbearably slow.",
"Well, you are not a bear, after all.",
"Don’t forget that we are under water.",
"Yuck. The authors could have spared us that animation.",
"This totem looks good to me.",
"That skull seems to radiate something strange.",
"Is it alive or is it some kind of spell?",
"",
"",
"",
}

l59skull_canyouall={"Can you all mind your own business? We actually enjoy sleeping.",""}

l59skull_wellanother={"Well another night of restless sleep due to curious people",""}

l59skull_welliwas={"Well I was asleep, until I was so rudely awakened.",""}

l59skull_anothernight={"Another night of peaceful sleep, ruined.",""}
