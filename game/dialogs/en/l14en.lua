l14={"What kind of strange ship is that?",
"This is the wreck of the civilian airplane LC-10 Lemura.",
"This is the wreck of the civilian airplane Atlantobus.",
"This is the wreck of the civilian airplane Poseidon 737.",
"Can you see that eye? The silent witness to the tragedy... Somebody trusted this airplane - and the glass eye is everything he left behind.",
"This is not a glass eye but a gyroscope. At least in this level.",
"Seats. Why are there so many seats here?",
"Be thankful. Could you get out without them?",
}
