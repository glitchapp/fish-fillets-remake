l77={"Father was right. The linux users must be mad.",
"Who else would make so many barriers.",
"To top it all off they did it for free.",
"It is clear now, they were trying to protect themselves from us.",
"They were building mountains, valleys, ...",
"The linux users!",
"There is one last task: to put them away.",
"We must put them away.",
"I think we should be happy if we manage to get one.",
"But if we manage both we would be happier.",
"Do you think that gettin one linux user out is enough?",
"No I don’t. The remaining one could fork himself.",
"Our goal is to reconcile the linux users.",
"Or at least to get them out.",
"In my opinion it would be better to give up on the linux users. Anyway, they weren’t in the assignment.",
"But they hacked this game and we can’t win while they’re here.",
"Wow, you had to try pretty hard to put only one linux user away. You got over the steel construction.",
"But it isn’t enough to win.",
"I would like to tell the player to not take this room too seriously. After all a linux user made it.",
"He was probably mad.",
"Player, if you are a linux user, don’t take this madness personally.",
"We aren’t talking about you, we are talking about the others.",
"It’s interesting, they are quarreling about the same problems.",
"And they are using the same arguments.",
"What have you got over there on the bottom?",
"Frozen bubbles.",
"If you join three bubbles of the same color they will drop away.",
"If only the linux users would be funny.",
"I know one super joke, do you know the difference between chattr and chmod?",
"man chattr > 1; man chmod > 2; diff 1 2",
"It’s more embarrassing than the last comic strip on userfriendly.org.",
"And do you know what eight trilobites is? It’s one trilobyte.",
"Ha ha ha",
"Look, this room is trying not to be opinionated. There is a penguin in the background, but if there would be logo of Debian, Fedora, ...",
"Why did you say Debian first?",
"I am reciting in alphabetical order.",
"So you should start with Arch Linux.",
"Sorry, I forgot about it. So Arch Linux, Debian, ...",
"Why did you say Debian twice?",
"This game was programmed on Debian.",
"Why wasn’t it done on Ubuntu?",
"Ubuntu is just a bad copy of Debian.",
"Have you tried Ubuntu?",
"No, but I’ve heard plenty. Can you log in as the root on it?",
"Ubuntu doesn’t need antiquities like a root user.",
"But it is based on them.",
"An interface between the user and Unix exists in every distribution.",
"What about Slackware?",
"Oh yes, but its packaging system is lousy.",
"At least it’s owned by Slackware.",
"Do you know why there are snakes?",
"Of course, it’s the logo of the best programming language.",
"When did C get a logo with snakes?",
"C is ancient and outdated. Now there are C++ and Java.",
"Java is an interpreted programming language so it’s very slow. Your Python has the same problem.",
"I am not interested in speed but in programming comfort.",
"Yes, you are.",
"Look, Wilber!",
"It is the mascot of a great graphics application.",
"Which is trying to catch up to Photoshop.",
"The GIMP is not Photoshop. It works completely differently.",
"The GIMP developers say that to conceal their inability.",
"I am bored with this music.",
"Couldn’t you write a script for this?",
"And I am bored with the music switching.",
}

l77end={"When we saw the linux users we didn’t know what we should do with them.",
"We didn’t understand why we got them and it seemed they would never agree with each other.",
"But finally we got them to calm down, by confronting them with a windows user.",
"From then on they kept the motto: Gentoo or Mandriva, we’re all one family.",
"Although you didn’t solve any problem which we assigned to you ",
"I wouldn’t have left them in one game together either and they are quite useful here.",
}
