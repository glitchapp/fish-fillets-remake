l16={"This is some improvement. From bathyscaph to tank, this is like from a line into a net.",
"At least there are no oh-so-obliging snails here.",
"That is but little consolation.",
"Thinking about it, I always wanted to see the inside of a tank.",
"It seems you will have plenty of opportunity to look around.",
"I wonder how this tank got into the sea.",
"Maybe it was an amphibious tank.",
"An amphibious tank? Just imagine: On the moonless night, the uncomprehending defenders bewilderedly watch the mass of snorkels emerging from the surf, searching in vain for the landing craft...",
"So why are you asking, if you are so darned clever?!",
"Maybe there is a sunken landing craft nearby.",
"It is possible.",
"Do you think that this ammunition could harm us?",
"I don’t know, but I’ll try to keep my distance from it.",
"I think we are going to need that ladder.",
"To climb out? But we have only fins.",
"No. To plug that hole.",
}
