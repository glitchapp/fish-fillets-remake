l7={"My goodness, the impact created cave-in!",
"I can’t go through this cave-in,", 
"we have to find another exit.",
"This is not the way.",
"I won’t make it alone.",
"I probably should have had lifted that shell first.",
"Maybe we have to switch these objects.",
"A mattress. The best thing you can get under water.",
}

l7part2={"Now we can drop the snail on the mattress.",
"It will be difficult to pick up that snail from there.",
"Finally, it’s there.",
"The poor snail...",
"We should search the coral reefs.",
"There are going to be many interesting beings to investigate there.",
"Don’t we need a microscope to investigate corals?",
"Yes, they are small. But there can be other life forms.",
"Coral turtles, for example.",
"And moreover I have a suspicion that there’s a microscope in a bathyscaph.",--]]
}
