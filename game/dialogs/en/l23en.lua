l23={"I think that we are finally on the trail of that mysterious city.",
"It looks magnificent.",
"Now we only have to find out why it sank and we are done.",
"But it may take us a little while.",
"Leave me alone. I adore the classic beauty of this temple.",
"Who is sitting on that chair?",
"It’s a copy of Feidios’ Zeus. One of the seven wonders of the world.",
"Hmm. It was another age.",
}

l23zeus={"Be careful! Try not to damage it!",
"Such a waste!",
"And now ease it down, carefully.",
"You barbarian! Can’t you be a little bit cautious?!",
"I didn’t like this sculpture anyway.",
}
