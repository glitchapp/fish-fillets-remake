l1={"What was that?",
"I have no idea.",
"We should go and have a look outside.",
"Wait! I’m going with you.",
"Why isn’t anything happening?",
"Hey, player!",
"You can control us with arrow keys and use the space bar to switch fish.",
"Or you can control us with the mouse -- left-click somewhere and...",
"...if we can get there without moving anything...",
"...we’ll follow your clicks.",
"And when you right-click somewhere, we’ll try to get there, pushing aside all obstacles in our way.",
"Your goal should be to get both of us outside.",
"And be careful not to hurt us. If you drop something on us or...",
"Oh, stop it. We’ll tell him in the next level.",
"Okay.",

"Wow, you moved it! I could never do that!",
"Thanks, now I can go with you.",
"Darn it, I can’t get through.",
"I must have gained some weight.",
"Could you push the chair a little bit to the left?",
"Ooops! That was a mistake. Both of us have to be able to leave in order to continue.",
"There’s no way I can get out now. You’ll have to restart the level.",
"You have to press the long arrow on your keyboard.",
"Humans call it a backspace key.",
"Whatever. It was funnier in Czech.",
"The restart option is also located on the control panel.",
"If you think you would rather restart the level, no problem.",
"You just have to press the long arrow on your keyboard.",
"I’m a heavy steel cylinder. It’s not easy to move me around. Such a tiny fish, like the orange one over there, simply has no chance. I could smash her without any problem.",
}

l1icantgetthrough={"I can’t get through here. That steel cylinder is in the way.",
"I can’t move that cylinder. Can you help me?",
"No problem...",
}

l1wowyoumovedit={
"Wow, you moved it! I could never do that!",
"Thanks, now I can go with you.",
}

l1damnit={"Darn it, I can’t get through.",
"I must have gained some weight.",
"Could you push the chair a little bit to the left?"
}

--lipsync
lipsiter=1			-- iterations through mouth positions
lipstime 	 		=  {0.00,0.10,0.22,0.29,0.57,0.71,1.06,									--what was that?	"duration": 1.13						--7
						0.00,0.04,0.16,0.23,0.58,0.65,0.86,0.93,1.00,1.14,1.56,1.77,		-- I have no idea "duration": 1.87							--12
						0.00,0.05,0.11,0.24,0.52,0.59,0.66,0.87,0.94,1.08,1.43,1.57,1.78,	-- we should go and have a look outside    "duration": 1.93	--13
						0.00,0.04,0.09,0.28,0.63,1.04,1.11,1.18,1.32,1.46,1.95,				-- wait I'm going with you "duration": 2.12	
						0.07,0.18,0.25,0.39,0.60,0.88,1.09,1.18,1.27,1.55,1.77}				-- why isnt anything happening?	"duration": 1.77
						
lipsvalue	 		=  {"X" ,"B" ,"F" ,"B" ,"C" ,"B" ,"X",									-- what was that?
						"X", "D", "B", "C", "G", "C", "E", "F", "D", "B", "D", "X",			-- I have no idea
						"X", "B", "F", "B", "E", "F", "C", "B", "F", "B", "D", "B", "X",	-- we should go and have a look outside
						"X", "F", "C", "B", "X", "C", "B", "E", "B", "F", "X",				-- wait I'm going with you "duration" 2.12
						"X", "F", "C", "B", "C", "B", "C", "A", "C", "B", "X"}				-- why isnt anything happening?	"duration": 1.77

lippositions={7,19,32,43,54}	-- number of positions per line
lipslines=1					-- total number of lines

--[[
lipwhat= {
	{ "start", 0.00, "end", 0.10, "value", "X" },
    { "start", 0.10, "end", 0.22, "value", "B" },
    { "start", 0.22, "end", 0.29, "value", "F" },
    { "start", 0.29, "end", 0.57, "value", "B" },
    { "start", 0.57, "end", 0.71, "value", "C" },
    { "start", 0.71, "end", 1.06, "value", "B" },
    { "start", 1.06, "end", 1.13, "value", "X" }
  }
  --]]
--[[
-- I have no idea "duration": 1.87

lipihavenoidea= {
    { "start": 0.00, "end": 0.04, "value": "X" },
    { "start": 0.04, "end": 0.16, "value": "D" },
    { "start": 0.16, "end": 0.23, "value": "B" },
    { "start": 0.23, "end": 0.58, "value": "C" },
    { "start": 0.58, "end": 0.65, "value": "G" },
    { "start": 0.65, "end": 0.86, "value": "C" },
    { "start": 0.86, "end": 0.93, "value": "E" },
    { "start": 0.93, "end": 1.00, "value": "F" },
    { "start": 1.00, "end": 1.14, "value": "D" },
    { "start": 1.14, "end": 1.56, "value": "B" },
    { "start": 1.56, "end": 1.77, "value": "D" },
    { "start": 1.77, "end": 1.87, "value": "X" }
  }
  
-- we should go and have a look outside    "duration": 1.93

lipweshouldgo= {
    { "start": 0.00, "end": 0.05, "value": "X" },
    { "start": 0.05, "end": 0.11, "value": "B" },
    { "start": 0.11, "end": 0.24, "value": "F" },
    { "start": 0.24, "end": 0.52, "value": "B" },
    { "start": 0.52, "end": 0.59, "value": "E" },
    { "start": 0.59, "end": 0.66, "value": "F" },
    { "start": 0.66, "end": 0.87, "value": "C" },
    { "start": 0.87, "end": 0.94, "value": "B" },
    { "start": 0.94, "end": 1.08, "value": "F" },
    { "start": 1.08, "end": 1.43, "value": "B" },
    { "start": 1.43, "end": 1.57, "value": "D" },
    { "start": 1.57, "end": 1.78, "value": "B" },
    { "start": 1.78, "end": 1.93, "value": "X" }
}
-- wait I'm going with you "duration": 2.12
lipwait= {
    { "start": 0.00, "end": 0.04, "value": "X" },
    { "start": 0.04, "end": 0.09, "value": "F" },
    { "start": 0.09, "end": 0.28, "value": "C" },
    { "start": 0.28, "end": 0.63, "value": "B" },
    { "start": 0.63, "end": 1.04, "value": "X" },
    { "start": 1.04, "end": 1.11, "value": "C" },
    { "start": 1.11, "end": 1.18, "value": "B" },
    { "start": 1.18, "end": 1.32, "value": "E" },
    { "start": 1.32, "end": 1.46, "value": "B" },
    { "start": 1.46, "end": 1.95, "value": "F" },
    { "start": 1.95, "end": 2.12, "value": "X" }
}
--]]
