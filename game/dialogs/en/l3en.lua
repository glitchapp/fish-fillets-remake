l3={"You know the rules already.", 
"Now you can practice them.",
"We will give you some hints occasionally.",
"I must not swim down now.",
"And I cannot go up, either.",
"Hey, arrange it up there somehow.",
"But be careful!",
""
}

l3icantmove={"I can’t move this. This is steel."}

l3comeback={"Come back, you can’t go further!"}

l3thisistricky={"Hmm... This is tricky.",
"If we push aside the honey, we will be smashed by that can of apricot jam.",
"Couldn’t we push it aside with that axe?",
"That’s a good idea.",
"Now I cannot take the axe. The can is in the way.",
"You mustn’t push the axe!",
"If you can push that can to me, I’ll shove the axe on it and everything will be okay."}

l3thislooksrealbad={"This looks real bad. You are going to have to restart the level."}


l3ifyousolvesomespecific={"If you solve some specific part you can save the position.",
"But only if you are sure of your solution.",
"Remember what we have said about saving positions."}

l3youarestandinginmyway={"You are standing in my way there. Try to hide somewhere else.",
"If you drop that and I stay here, I will never get out.",
"Here? I’m little bit afraid. Will I get out?",
"Just wait and see."}

l3ishoulndtbe={"I shouldn’t be thinking about this."}

l3wewillgiveyouahint={"We will give you a hint here, player. You have to put that book over there, to the left.",
"So that it will catch that steel cylinder when I push it.",
"But we will not tell you how to do it.",
"What if I go the upper way?",
"Shhhh. That’s too many hints."}

l3justremember={"Just remember that although we cannot push objects along each other’s back, we can push them down or onto some solid structure."}

l3thingslikethat={"Things like that happen sometimes."}

l3sometimes={"Sometimes you have to think faaar ahead.",
"And often you have to restart the level then."}

l3weadmitwedid={"We admit that we did this on purpose.",
"So that you could try it again - this time without any hints."}
