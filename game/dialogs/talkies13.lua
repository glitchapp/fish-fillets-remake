

function Obey.lev13()

  
  vikingavatar1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level13/vikingavatar1.webp")))
  vikingavatar2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level13/vikingavatar2.webp")))
  vikingavatar3 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level13/vikingavatar3.webp")))
  vikingavatar4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level13/vikingavatar4.webp")))
  vikingavatar5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level13/vikingavatar5.webp")))
  
  loadcorrespondentfonts()


       
 Talkies.say( "small fish",
    {
		l13[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Big fish",
    { 
    	l13[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
	
	Talkies.say( "small fish",
    {
		l13[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	l13[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "small fish",
    {
		l13[5],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
 Talkies.say( "Viking 1",
    { 
    	l13[6],
    },
    {
      image=vikingavatar1,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 2",
    { 
    	l13[7],
    },
    {
      image=vikingavatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
     
     Talkies.say( "Viking 3",
    { 
    	l13[8],
    },
    {
      image=vikingavatar3,
      talkSound=blop,
      typedNotTalked=true,
    })
    
 Talkies.say( "Viking 4",
    {
		l13[9],
    },
    {
      image=vikingavatar4,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 5",
    {
		l13[10],
    },
    {
      image=vikingavatar5,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 1",
    {
		l13[11],
    },
    {
      image=vikingavatar1,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Vinking 2",
    {
		l13[12],
    },
    {
      image=vikingavatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Viking 3",
    {
		l13[13],
    },
    {
      image=vikingavatar3,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Viking 4",
    {
		l13[14],
    },
    {
      image=vikingavatar4,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Viking 5",
    {
		l13[15],
    },
    {
      image=vikingavatar5,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Viking 1",
    {
		l13[16],
    },
    {
      image=vikingavatar1,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 2",
    {
		l13[17],
    },
    {
      image=vikingavatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Viking 3",
    {
		l13[18],
    },
    {
      image=vikingavatar3,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 4",
    {
		l13[19],
    },
    {
      image=vikingavatar4,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "Viking 5",
    {
		l13[20],
    },
    {
      image=vikingavatar5,
      talkSound=blop,
      typedNotTalked=true,
    })
 
end

function Obey.lev13skull()

Talkies.say( "skull",
    {
		l13skull[1],
		l13skull[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
