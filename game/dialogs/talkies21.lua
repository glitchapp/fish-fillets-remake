function Obey.lev21()

  
  --statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level21/statue.webp")))
	  if skin=="remake" then statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level21/statue.webp")))
  elseif skin=="classic" then statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/classic/level21/objects/upscaled/statue.webp")))
  end
  
  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  else
  loadcorrespondentfonts()
  end

 Talkies.say( "Statue",
    { 
       l21[1],
       l21[2],
       l21[3],
       l21[4],
       l21[5],
       l21[6],
       l21[7],
       l21[8],
       l21[9],
       l21[10],
       l21[11],
       l21[12],
       l21[13],
       l21[14],
       l21[15],
       l21[16],
       l21[17],
       l21[18],
       l21[19],
       l21[20],
       l21[21],
       l21[22],
       l21[23],
       l21[24],
    },
    {
      image=statue,
      talkSound=blop,
      typedNotTalked=true,
    })
  

 Talkies.say( "small fish",
    {
      l21[25],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
       l21[26],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "small fish",
    {
      l21[27],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
  Talkies.say( "Big fish",
    { 
       l21[28],
       l21[29],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "small fish",
    {
      l21[30],
      l21[31],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
       l21[32],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
   Talkies.say( "small fish",
    {
      l21[33],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
      Talkies.say( "Big fish",
    { 
       l21[34],
       l21[35],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
      Talkies.say( "small fish",
    {
      l21[36],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    Talkies.say( "Big fish",
    { 
       l21[37],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    Talkies.say( "small fish",
    {
      l21[38],
       },
       {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
        Talkies.say( "Big fish",
    { 
       l21[39],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    Talkies.say( "small fish",
    {
      l21[40],
      },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
     Talkies.say( "Big fish",
    { 
       l21[41],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
end

function Obey.lev21advertising1()
statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level21/statue.webp")))
Talkies.say( "Statue",
    { 
       l21[12],
       l21[13],
       l21[14],
       l21[15],
       l21[16],
    },
    {
      image=statue,
      typedNotTalked=true,
    })
end


function Obey.lev21advertising2()
statue = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level21/statue.webp")))
Talkies.say( "Statue",
    { 
       l21[10],
       l21[11],
    },
    {
      image=statue,
      typedNotTalked=true,
    })
end
