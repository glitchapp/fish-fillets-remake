l9 = {
  "ПЕРВЫЙ ТОРПЕДНЫЙ БАРХАН ЗАГРУЖЕН...",
  "Перестань строить гримасы и приди помоги мне.",
  "Перестань смотреть на себя в зеркало и стань полезным.",
  "Вот так, прекрасно! Он просто продолжает свой веселый путь, а что со мной?",
  "Как обычно. Он просто оставляет меня позади.",
  "Подожди! Что же со мной? Я тоже хочу выбраться отсюда!",
  "Эй, кто-нибудь там внизу?",
  "Привет, вы меня слышите?",
  "Кто-нибудь здесь?",
  "Перестань кричать и просто вытащи меня отсюда.",
  "Перестань кричать и помоги мне выбраться отсюда.",
  "Вот я, я не могу выбраться.",
  "Чьи это глаза?",
  "Я не знаю. Может быть, перископ связан с другим измерением.",
  "Будь осторожен...",
  "Не говори, что я не предупреждал тебя!"
}
