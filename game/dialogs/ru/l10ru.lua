l10 = {
  "Посмотри. Праздничная лодка.",
  "У меня есть ощущение, что это не будет обычный пикник.",
  "Ты и твои ощущения. Наверняка там полны добрых и дружелюбных людей.",
  "Может быть, ты был прав. Это странный пикник.",
  "Ты думаешь, что они смогут выбраться из этой лодки?",
  "Я надеюсь, что они не смогут выбраться из этой лодки.",
  "А если они начнут нас преследовать?",
  "Не думаю, что они смогут нас достать. Скелеты не очень хорошо плавают.",
  "Перестань думать об этом и двигай своими плавниками, чтобы мы выбрались отсюда.",
  "Ты хочешь поддержать эту стальную трубку стаканом?",
  "Я не могу ничего другого для тебя сделать.",
  "Будь осторожен, чтобы не пролить его!"
}
