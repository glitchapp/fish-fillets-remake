l1 = {
"Что это было?",
"Я не имею ни малейшего понятия.",
"Мы должны выйти и посмотреть наружу.",
"Подожди! Я пойду с тобой.",
"Почему ничего не происходит?",
"Привет, игрок!",
"Вы можете управлять нами с помощью стрелок на клавиатуре и использовать пробел для переключения рыбы.",
"Или вы можете управлять нами с помощью мыши - щелкните где-нибудь левой кнопкой и...",
"...если мы можем туда попасть без движения чего-либо...",
"...мы будем следовать вашим кликам.",
"А когда вы щелкнете правой кнопкой где-нибудь, мы постараемся добраться туда, отодвигая все препятствия на своем пути.",
"Вашей целью должно быть выведение нас обоих наружу.",
"И будьте осторожны, чтобы не причинить нам вреда. Если вы что-нибудь уроните на нас или...",
"Остановитесь. Мы расскажем ему на следующем уровне.",
"Хорошо.",

"Вау, ты сдвинул его! Я бы никогда не смог это сделать!",
"Спасибо, теперь я могу пойти с тобой.",
"Черт, я не могу пройти.",
"Наверное, я набрал вес.",
"Можешь немного подвинуть стул влево?",
"Упс! Это была ошибка. Нам обоим нужно быть способными уйти, чтобы продолжить.",
"Сейчас я не смогу выбраться. Тебе придется перезапустить уровень.",
"Тебе нужно нажать длинную стрелку на клавиатуре.",
"Люди называют ее клавишей Backspace.",
"Как скажешь. В чешском это было смешнее.",
"Опция перезапуска также находится на панели управления.",
"Если вы хотите перезапустить уровень, никаких проблем.",
"Вам просто нужно нажать длинную стрелку на клавиатуре.",
"Я тяжелый стальной цилиндр. Мне не так просто двигаться. Маленькая рыбка, как оранжевая там, просто не имеет шансов. Я мог бы раздавить ее без проблем.",
}

l1icantgetthrough = {
"Я не могу пройти здесь. Передо мной стальной цилиндр.",
"Я не могу переместить этот цилиндр. Ты можешь мне помочь?",
"Нет проблем...",
}

l1wowyoumovedit = {
"Вау, ты сдвинул его! Я бы никогда не смог это сделать!",
"Спасибо, теперь я могу пойти с тобой.",
}

l1damnit = {
"Черт, я не могу пройти.",
"Наверное, я набрал вес.",
"Можешь немного подвинуть стул влево?",
}
