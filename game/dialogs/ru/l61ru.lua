l61 = {
  "Итак, мы наконец-то нашли это.",
  "Что?",
  "Сокровища капитана Сильвера, конечно.",
  "Святой Грааль, конечно.",
  "Они аккуратно в одном месте: сокровища и Грааль.",
  "Мне кажется, это слишком легко, чтобы быть правдой.",
  "Я думаю, что если бы это действительно была наша цель, игра дала бы знать игроку.",
  "Но я дал им знать. Этого может быть достаточно.",
  "Но я вызвал сомнения. Это может их запутать.",
  "Хорошо, ладно. ВОЗМОЖНО, это не то, что мы ищем.",
  "Так лучше. Остается некоторое неопределенность.",
  "Разве мы не можем закрыть этот сундук?",
  "Что, если мы попробуем закрыть этот сундук?",
  "С закрытым сундуком было бы намного проще.",
  "Попробуй придавить его крышку.",
  "А не взять ли нам что-нибудь с собой?",
  "А что, если мы возьмем с собой немного сокровищ?",
  "Тебе не жаль оставить все эти сокровища позади?",
  "Мы будем рады, если сможем выбраться сами.",
  "Не забывай нашу миссию.",
  "Сокровища только будут мешать нам.",
  "Вся эта блестящая красота просто захлестывает глаза.",
  "Здесь все сверкает. Это меня пьянит.",
}
