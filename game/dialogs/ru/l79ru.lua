l79 = {
  "Игрок должен знать, что цилиндры связаны с автомобилями того же цвета",
  "НЕВИДИМОЙ СИЛОЙ!!",
  "Кто-то играет с нами — автомобили двигаются. Или это делаешь ты? Или я? Я начинаю бояться.",
  "Интересно, рыбные филе — хорошая игра для решения этой комнаты.",
  "Что ты имеешь в виду?",
  "Игрок должен научиться решать это где-то еще и только повторять здесь.",
  "Наоборот, если игрок решит это только здесь, я буду тайно восхищаться им.",
  "Мы должны вытащить красную машину.",
  "Не строй из себя умного, когда кто-то уже им является.",
  "Что ты делаешь? Ты еще не вышел.",
  "Что ты делаешь? Ты еще не вышел.",
}

l79end = {
  "Доброе утро, рыбка!",
  "Снова ты нас не разочаровал.",
  "Генеральный комитет решил наградить тебя высшими орденами.",
  "Они изготовлены из молочного шоколада. Из-за конфиденциальности, съешь их немедленно.",
  "БОСС",
  "P.S. Я понимаю эту маленькую проблему, но в следующий раз предупреди заранее,",
  "чтобы мы могли предоставить разрешение на усыновление.",
  "P.P.S. Скажи мне, где ты нашел такого хорошего игрока, который все справился?",
  "Хотелось бы, чтобы он выиграл компьютер или хотя бы какие-то другие призы.",
}
