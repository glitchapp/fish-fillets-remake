l15 = {
  "Так вот это исследовательский батискаф.",
  "Хороший микроскоп.",
  "Не должен ли я это поднять?",
  "Хочешь, чтобы я это поднял?",
  "Не должен ли я это выключить?",
  "Мы не можем добраться до приемника отсюда.",
  "Это будет для меня...",
  "Это будет моим братом...",
  "Мог бы ты ответить на этот звонок за меня, пожалуйста?",
  "Надеюсь, я не мешаю здесь.",
  "Действительно, я могу легко убраться. Просто скажи слово.",
  "Я действительно не хочу быть препятствием.",
  "Просто скажи мне, если ты хочешь, чтобы я ушел.",
  "Привет.",
  "Кто-нибудь там?",
  "Ты там, брат?",
  "Улитка Эскарго говорит.",
  "Я не слышу тебя.",
  "Привет, привет!",
  "Привет, главный инспектор Клузо говорит. Я ищу опасных моллюсков, которые... О нет, не сейчас, Като, ты желтый обезьянка!",
  "Привет, вы позвонили по номеру 4202-21913271. В настоящее время никто из семьи Рыбки не находится здесь. Пожалуйста, оставьте сообщение после сигнала. Что случилось?",
  "Теперь здесь тихо.",
}
