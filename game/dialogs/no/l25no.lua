l25 = {
"Hvor er vi nå?",
"Pyramidene... Legg merke til hvordan de klassiske motivene blander seg i denne byen.",
"Hva er det som kravler rundt der borte?",
"Du kan ikke se det herfra. Det er på den andre siden av veggen.",
"Se, kvinnen kjeder seg!",
"Tror du dette tar for lang tid?",
"Prøv det selv, hvis du er så smart.",
"Hva skal vi si?",
"Du trenger ikke å bære noe.",
"Ikke vær redd.",
"Hva står det på disse tavlene?",
"Ha det bra og takk for all fisken.",
}
