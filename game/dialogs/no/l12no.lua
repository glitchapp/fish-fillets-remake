l12={"Kan dette være Kaptein Silver's skip?",
"Hva får deg til å tro det?",
"Tittelen på dette nivået.",
"Hva er det på hodet ditt?",
"Det kler deg.",
"Takk, det gjør det samme med deg.",
"En veldig fin hatt.",
"Din stetson er også veldig god.",
"Ørene mine holdt på å fryse.",
"Det er en flott hatt! Kan jeg låne den?",
"Hvis du låner meg din.",
"Dette er virkelig en stor stetson.",
"Og en vakker en også.",
"Jeg tror vi må gå inn i det skipet.",
"Det ser ut som om vi må gå inn.",
"Luften her inne lukter veldig muggen.",
"Det virker som om det ikke er noen her.",
"Jeg liker det ikke her.",
"Slutt å snakke og svøm. Gamle skipsinteriører er svært interessante steder.",
"Kan du kjenne den dystre og forfallende atmosfæren?",
"Nei, bare muggen.",
"Hva ser du?",
"Jeg ser mange interessante nivåer som vi må løse.",
}

l12cylinder={"Jeg kan ikke flytte denne.",
"Jeg kan ikke flytte den sylinderen."}
