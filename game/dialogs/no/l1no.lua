l1={"Hva var det?",
"Jeg har ingen anelse.",
"Vi bør gå ut og se.",
"Vent! Jeg følger med deg.",
"Hvorfor skjer det ingenting?",
"Hei, spiller!",
"Du kan kontrollere oss med piltastene og bruke mellomromstasten for å bytte fisk.",
"Eller du kan kontrollere oss med musen - venstreklikk et sted og...",
"...hvis vi kan komme dit uten å flytte på noe...",
"...vil vi følge dine klikk.",
"Og når du høyreklikker et sted, vil vi prøve å komme dit ved å skyve bort alle hindringer på veien.",
"Målet ditt bør være å få oss begge ut.",
"Og vær forsiktig så du ikke skader oss. Hvis du slipper noe på oss eller...",
"Oh, slutt. Vi vil fortelle ham det i neste nivå.",
"Greit.",

"Wow, du flyttet det! Jeg kunne aldri gjort det!",
"Takk, nå kan jeg bli med deg.",
"Herregud, jeg kommer ikke gjennom.",
"Jeg må ha lagt på meg.",
"Kunne du dytte stolen litt til venstre?",
"Oops! Det var en feil. Begge av oss må kunne dra herfra for å fortsette.",
"Det er ingen måte jeg kan komme ut nå. Du må starte nivået på nytt.",
"Du må trykke på den lange pilen på tastaturet ditt.",
"Mennesker kaller det en backspace-tast.",
"Hva som helst. Det var morsommere på tsjekkisk.",
"Startalternativet finner du også på kontrollpanelet.",
"Hvis du heller vil starte nivået på nytt, er det ikke noe problem.",
"Du trenger bare å trykke på den lange pilen på tastaturet ditt.",
"Jeg er en tung stålsylinder. Det er ikke lett å flytte meg. En så liten fisk, som den oransje der borte, har rett og slett ingen sjanse. Jeg kunne knuse henne uten problemer.",
}

l1icantgetthrough={"Jeg kommer ikke gjennom her. Den stålsylinderen er i veien.",
"Jeg kan ikke flytte den sylinderen. Kan du hjelpe meg?",
"Ingen problem...",
}

l1wowyoumovedit={
"Wow, du flyttet det! Jeg kunne aldri gjort det!",
"Takk, nå kan jeg bli med deg.",
}

l1damnit={"Forbanna, jeg kommer ikke gjennom.",
"Jeg må ha lagt på meg.",
"Kunne du dytte stolen litt til venstre?"
}
