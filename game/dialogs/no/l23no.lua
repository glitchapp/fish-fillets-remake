l23 = {
"Jeg tror vi er endelig på sporet av den mystiske byen.",
"Den ser storslått ut.",
"Nå må vi bare finne ut hvorfor den sank, så er vi ferdige.",
"Men det kan ta litt tid.",
"La meg være i fred. Jeg beundrer den klassiske skjønnheten til dette tempelet.",
"Hvem sitter på den stolen?",
"Det er en kopi av Feidios' Zeus. En av verdens syv underverker.",
"Hmm. Det var en annen tid.",
}

l23zeus = {
"Vær forsiktig! Prøv å ikke skade den!",
"Så synd!",
"Og nå senk den forsiktig ned.",
"Du barbar! Kan du ikke være litt forsiktig?!",
"Jeg likte ikke denne skulpturen uansett.",
}
