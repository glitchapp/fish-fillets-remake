l3={"Du kjenner allerede reglene.",
"Nå kan du øve på dem.",
"Vi vil gi deg noen hint av og til.",
"Nå må jeg ikke svømme ned.",
"Og jeg kan heller ikke gå opp.",
"Hei, ordne det der oppe på en eller annen måte.",
"Men vær forsiktig!",
""
}

l3icantmove={"Jeg kan ikke flytte dette. Dette er stål."}

l3comeback={"Kom tilbake, du kan ikke gå lenger!"}

l3thisistricky={"Hmm... Dette er vanskelig.",
"Hvis vi skyver honningen til side, vil vi bli knust av den boksen med aprikos syltetøy.",
"Kunne vi dytte den til side med den øksen?",
"Det er en god idé.",
"Nå kan jeg ikke ta øksen. Boksen er i veien.",
"Du må ikke dytte øksen!",
"Hvis du kan dytte den boksen til meg, skal jeg skyve øksen på den og alt vil være i orden."}

l3thislooksrealbad={"Dette ser virkelig dårlig ut. Du må starte nivået på nytt."}


l3ifyousolvesomespecific={"Hvis du løser en spesifikk del, kan du lagre posisjonen.",
"Men bare hvis du er sikker på løsningen din.",
"Husk det vi har sagt om å lagre posisjoner."}

l3youarestandinginmyway={"Du står i veien min der. Prøv å gjemme deg et annet sted.",
"Hvis du slipper det der og jeg blir her, kommer jeg aldri ut.",
"Her? Jeg er litt redd. Vil jeg komme ut?",
"Bare vent og se."}

l3ishoulndtbe={"Jeg burde ikke tenke på dette."}

l3wewillgiveyouahint={"Vi vil gi deg et hint her, spiller. Du må plassere den boken der, til venstre.",
"Slik at den vil fange den stål-sylinderen når jeg dytter den.",
"Men vi vil ikke fortelle deg hvordan du gjør det.",
"Hva om jeg går den øvre veien?",
"Shhhh. Det er for mange hint."}

l3justremember={"Husk bare at selv om vi ikke kan dytte objekter langs hverandres rygg, kan vi dytte dem ned eller oppå en solid struktur."}

l3thingslikethat={"Slike ting skjer av og til."}

l3sometimes={"Noen ganger må du tenke laaangt fremover.",
"Og ofte må du starte nivået på nytt da."}

l3weadmitwedid={"Vi innrømmer at vi gjorde dette med vilje.",
"Slik at du kan prøve det igjen - denne gangen uten noen hint."}
