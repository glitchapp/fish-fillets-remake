l54={"Jeg tror vi kan få litt informasjon om interstellar framdrift her.",
"Dette ser mer ut som konvensjonell framdrift for landingsskipet.",
"Så. Vi fant drivverket. Vi har oppnådd ett av målene i oppdraget vårt.",
"Ikke vær så hastig. Vi har ennå ikke søkt hele vraket.",
"Jeg beklager at ingen av disse tekniske miraklene rundt oss fungerer.",
"Jeg skulle ønske jeg kunne se noen av disse utenomjordiske duppedittene i aksjon.",
"Jeg lurer på om denne motoren kunne fungere under vann.",
"Kanskje det er bedre for oss hvis den ikke fungerer.",
"Jeg er ganske glad for at vi ikke kan slå på noe her.",
"Vi bør være glade for at vi ennå ikke har klart å slå på noe.",
}
l54engineon={"Hva har du gjort? Slå av den bråket!",
"Hva har du aktivert? Hvor tar det oss?",
"Dette er forferdelig! Slå det av før det er for sent!",
"Jeg kan ikke! Jeg får det ikke ut!",
"Jeg vet ikke hvordan! Jeg får det ikke ut!",
"Mayday! Mayday!",
"Hvordan slår jeg det av?!",
"Jeg kan ikke slå det av!"
}

l54engineonfish1={"Hva holder du på med? Hvor drar vi?",
"Hva har du aktivert? Hvor tar det oss?",
"Jeg kan ikke! Jeg får det ikke ut!",
"Jeg vet ikke hvordan! Jeg får det ikke ut!",
"Mayday! Mayday!",
"Hvordan slår jeg det av?!",
}
--"Jeg kan ikke slå det av!"
l54engineoff={"Endelig.",
"Hva en lettelse.",
"Takk.",
"Endelig.",
"Jeg er bare redd for at vi må slå det på igjen.",
""
}

l54aproachengine={"Hva holder du på med? Hvor drar du?",
"Vær forsiktig med den skiftenøkkelen.",
"Jeg kan ikke skade noe her."
}
