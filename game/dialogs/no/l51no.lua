l51={"Jaja. Så det kartet eksisterer virkelig likevel!",
"Greit. Her er kartet.",
"Så det er klart nå at målet vårt er å få ut det kartet på en eller annen måte.",
"Selvfølgelig. Det ville vært for lett hvis vi bare kunne svømme bort.",
"Slutt å snakke tull og prøv heller å tenke.",
"Målet vårt er å få ut det kartet på en eller annen måte.",
"Hva kan det være på det kartet?",
"Tror du virkelig det viser Silver's skatt?",
"Vi får se når vi klarer å få det forhatte kartet ut.",
"Vi trenger flere av disse sneglene.",
"Mener du Escargots, ikke sant?",
"Vi trenger flere glassøyne.",
"Den enkleste måten å gjøre det på ville vært å komme til øvre del av dette nivået. La oss prøve å starte på nytt, kanskje vi dukker opp der.",
"Late som om du ikke forsto reglene etter så mange løste nivåer.",
"Så vi klarte å flytte det.",
"La oss fortsette det gode arbeidet.",
"Ja, det er nesten ferdig.",
}

l51end={"Etter mye strev lyktes vi i å finne kaptein Silver's kart.",
"Innledende entusiasme ble senere til bitter skuffelse,",
"når vi fant ut at det nevnte kartet ikke indikerer plasseringen av skatten,",
"men bostedet til den siste overlevende papegøyen til Silver som,",
"dessverre lider av sklerose og ikke kan huske hvor skatten er.",
}
