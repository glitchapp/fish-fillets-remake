l30={"Jeg har aldri sett så violette koraller før.",
"Jeg liker ikke violette koraller.",
"Jeg elsker violette koraller...",
"De ser fine ut for meg.",
"Jeg håper det vil være enda mer interessante koraller i de kommende nivåene.",
"Måtte Gud forby det.",
"Vi må løfte denne stålen...",
"Hei, krabbe!",
"Skal vi ikke prøve å glede den krabben litt?",
"Ignorer den.",
"La den krabben være i fred.",
"Ser du ikke hvor nervøs den er?",
"Prøv å klare deg uten den nervøse krabben.",
"Den krabben kunne vært nyttig.",
"Denne krabben vil sikkert hjelpe oss å løse det.",
}

l30crab={"Hvem vekket meg?",
"La meg være i fred, jeg sover her!",
"Hva vil du fra meg?!?",
"Rør meg ikke!",
}

l30crabups={
"Ups.",
}
