l36={"Det er herlig her.",
"Så mange perler.",
"Det er så mange perler her.",
"Hør... Glem ikke oppdraget vårt!",
"Vi er ikke perlejegere.",
"Husk! Vi har en oppgave å fullføre.",
"Du sitter bare der oppe, og jeg må gjøre alt arbeidet selv!",
"Sa du ikke at du liker perlene?",
"Er du ikke glad for så mange perler?",
"Jeg trenger bare én til perle.",
"Nei.",
"Bare én til perle, vær så snill.",
"Kjæreste venn, kan du ikke låne meg bare én liten perle?",
"Jeg lurer på om det ikke ville være lettere å forkorte den stålrøret.",
"Det sies at Kleopatra løste opp perler i eddik.",
"Tror du vi har for mange av dem?",
"Vel, jeg ville bare lære deg litt.",
"Idet jeg ser på deg, må jeg tenke på hvordan jeg lekte med marmor og småtroll.",
"Har du tilfeldigvis en reservekule i lommen?",
}
