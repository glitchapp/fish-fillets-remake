l14={"Hva slags merkelig skip er det der?",
"Dette er vraket av det sivile flyet LC-10 Lemura.",
"Dette er vraket av det sivile flyet Atlantobus.",
"Dette er vraket av det sivile flyet Poseidon 737.",
"Ser du det øyet? Den stille vitnet til tragedien... Noen stolte på dette flyet - og det glassøyet er alt han etterlot seg.",
"Dette er ikke et glassøye, men en gyroskop. I hvert fall i dette nivået.",
"Seter. Hvorfor er det så mange seter her?",
"Vær takknemlig. Kunne du kommet deg ut uten dem?",
}
