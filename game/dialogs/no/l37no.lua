l37={"Ser du det? Dette må være den telepatiske skilpadden.",
"Ja, det er den. Den telepatiske skilpadden.",
"Vel, jeg vet ikke. Ut fra bildene kan det være den.",
"Jeg er ikke sikker... hva med de telepatiske evnene?",
"Uansett må vi få den ut av rommet.",
"La oss ta ut den skilpadden.",
"Hva gjør du?",
"Hva har du gjort?",
"Hva skal det bety?",
"Hvor svømmer du hen?",
"Hva skal det bety?",
"Hva har skjedd med deg?",
"Jeg vet ikke...",
"Jeg kan ikke fatte det...",
"Jeg vet ikke hva som skjer med meg...",
"Jeg ville ikke...",
"Hva skjer med oss?",
"Hva kan det bety?",
"Dette må være den skilpadden!",
"Dette må være den skilpadden!",
"Så det kan ikke være tvil nå - dette er den vi leter etter!",
"Utvilsomt, dette er skilpadden vi leter etter!",
"Denne skapningen!",
"Stopp, djevelen!",
"Nok er nok!",
"Koser du deg?",
"Så mange bisarre former finnes bare i et korallrev.",
"Disse korallene er Moder Jord's underverker.",
"Hvor kommer så mange bisarre former fra?",
"Det er enkelt - designerne skaper intrikate problemer og overlater tegningen til grafikerne.",
"Målet vårt i dette rommet er å få ut den skilpadden.",
}

l37end={"Vi lyktes i å fange den farlige korallturtlen som egentlig er helt ufarlig.",
"Vi anbefaler maksimal sikkerhetstiltak, selv om det ikke er nødvendig, da hun er veldig snill.",
"Den kan ikke bare lese snille tanker, men også påvirke dem - dette er tull.",
"Og hun har bitt oss, også - hva så?",
"For all del, fjern munnkurven hennes, men bare hvis hun ønsker det - ikke fjern munnkurven.",
}
