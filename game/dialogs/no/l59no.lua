l59={"Ser du den sjøhesten?",
"Den er blokkert av amforer.",
"Den har satt seg selv i beruselse blant amforene.",
"Jeg lurer på om det er noe igjen i dem.",
"Du må nok sjekke selv.",
"Endelig kan jeg se en ny type hodeskalle.",
"La du merke til den totempælen? Det er den meksikanske guden Shelloxuatl.",
"Slik ser det ut.",
"Disse amforene faller uutholdelig sakte.",
"Vel, du er tross alt ikke en bjørn.",
"Glem ikke at vi er under vann.",
"Ekkelt. Forfatterne kunne spart oss den animasjonen.",
"Den totempælen ser bra ut for meg.",
"Den hodeskallen virker å utstråle noe merkelig.",
"Er den levende eller er det noen form for besvergelse?",
"",
"",
"",
}

l59skull_canyouall={"Kan dere alle passe deres eget? Vi nyter faktisk å sove.",""}

l59skull_wellanother={"Vel, enda en natt med urolig søvn på grunn av nysgjerrige mennesker",""}

l59skull_welliwas={"Vel, jeg sov, til jeg ble vekket så uhøflig.",""}

l59skull_anothernight={"En annen natt med fredelig søvn, ødelagt.",""}
