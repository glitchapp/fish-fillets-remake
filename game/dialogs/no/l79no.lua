l79={"Spilleren bør vite at sylinderne er koblet til bilene med samme farge",
"AV EN USYNLIG KRAFT!!",
"Noen leker spill med oss - bilene flytter seg. Eller gjør du det? Eller gjør jeg det? Jeg begynner å bli redd.",
"Jeg lurer på om fiskefileter er et godt spill for å løse dette rommet.",
"Hva mener du med det?",
"Spilleren bør lære å løse det et annet sted og bare gjenta det her.",
"Tvert imot, hvis spilleren løser det bare her, vil jeg beundre ham i hemmelighet.",
"Vi må få ut den røde bilen.",
"Ikke vær smart når noen andre allerede er det.",
"Hva gjør du? Du er ikke ute ennå.",
"Hva gjør du? Du er ikke ute ennå.",
}

l79end={"God morgen, fisk!",
"Igjen skuffet du oss ikke.",
"Generalkomiteen har bestemt seg for å dekorere deg med de høyeste ordenene.",
"De er laget av melkesjokolade. Av hensyn til konfidensialitet, spis dem umiddelbart.",
"SJEFEN",
"PS: Jeg forstår denne lille, lille saken, men neste gang, vær så snill å fortell meg på forhånd,",
"slik at vi kan gi en adopsjonstillatelse.",
"PPS: Fortell meg, hvor fant du en så god spiller som klarte alt dette?",
"Jeg ønsker at han vant datamaskinen eller i det minste noen av de andre premiene.",
}
