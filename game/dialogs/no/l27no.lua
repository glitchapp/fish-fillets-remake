l27 = {
"Jeg er fanget her.",
"Jeg vil komme meg ut.",
"Hvordan kan jeg komme meg ut herfra?",
"Jeg er lukket inne i et rom uten dører!",
"Prøv å løsne en stein i veggen.",
}

l27door = {
"Se, nå har du kommet deg ut.",
"Så, vi er sammen igjen.",
"Er ikke den krabben litt merkelig?",
"Hvilken mener du?",
"Den øverste, selvfølgelig.",
"Den nederste, selvfølgelig.",
"Jeg synes det er normalt.",
"Ja, den er litt merkelig.",
"Vi bør være glade for at vi har alle disse ballene.",
"Denne byen ser mistenkelig godt bevart ut...",
"Jeg har en følelse av at jeg går glipp av noe.",
"Følelsene dine... tenk på hvordan vi kan komme oss ut.",
"Nei, vent, se på alle disse krabbene et øyeblikk... se inn i øynene deres...",
"Øynene deres beveger seg i takt med hverandre. Som om de blir kontrollert av en felles overbevissthet.",
"Men selvfølgelig, de blir kontrollert av et dataprogram. Akkurat som bølgene i vannet og dine tullete uttalelser.",
"Har vi noensinne sett en skulptur som har vært stille så lenge?",
"Jeg tror den bare venter på den beste muligheten.",
}
