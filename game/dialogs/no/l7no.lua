l7={"Kjære vene, sammenstøtet skapte en sammenraset grotte!",
"Jeg kan ikke gå gjennom denne sammenraset,",
"vi må finne en annen utgang.",
"Dette er ikke veien.",
"Jeg vil ikke klare det alene.",
"Jeg burde nok ha løftet den skjell først.",
"Kanskje vi må bytte disse objektene.",
"En madrass. Det beste du kan få under vann.",
}

l7part2={"Nå kan vi slippe sneglen på madrassen.",
"Det vil være vanskelig å plukke opp den sneglen derfra.",
"Endelig er den der.",
"Den stakkars sneglen...",
"Vi bør undersøke korallrevene.",
"Det vil være mange interessante skapninger å undersøke der.",
"Trenger vi ikke et mikroskop for å undersøke korallene?",
"Ja, de er små. Men det kan være andre livsformer.",
"Korallskilpadder, for eksempel.",
"Og dessuten har jeg en mistanke om at det er et mikroskop i en bathyscaph.",--]]
}
