l2={"Å nei, ikke igjen...",
"Du vet det er ingenting vi kan gjøre med det.",
"La oss dytte det ned så vi kan se på det.",
"Vel, la oss komme i gang",
}

l2thediskself={"Hvordan? Disken selvdestruerte.",
"Så nå kan vi endelig begynne.",
"Vår første oppgave er å komme oss ut av dette rommet.",
"Dette kommer forresten til å være oppgaven vår i alle de andre rommene også.",
"Kom igjen, la oss ta spilleren med oss inn i verkstedet og vise ham arbeidssikkerhetsforskriftene.",
"God idé.",
"Foreløpig må du ikke røre noe, bare se og lær. Vi skal vise deg hva du skal og ikke skal gjøre med oss, samt hva vi er i stand til.",
"Før vi går inn i verkstedet, la oss lagre spillet - trykk bare på F2-tasten.",
"Først skal vi vise deg hva som kan skade oss.",
"Jeg melder meg frivillig som dummysubjekt.",
"Først og fremst bør vi ikke slippe ting på hverandre.",
"Vi kan heller ikke svømme nedover mens vi bærer en gjenstand, fordi den ville falle på oss.",
"En annen ting vi ikke må gjøre er å dytte gjenstander langs ryggen vår.",
"Noen gjenstander er formet slik at vi kunne holde dem og dytte dem samtidig - men vi har ikke lov til å gjøre dette heller.",
"Vi kan heller ikke slippe ekstra gjenstander oppå en haug som en av oss bærer.",
"Og i motsetning til min større partner kan jeg ikke flytte eller bære stål gjenstander engang.",
"Vi kan alltid plukke opp gjenstander og la dem falle.",
"Vi kan dytte en gjenstand langs hverandres rygg bare hvis gjenstanden deretter blir dyttet opp på en annen støtte...",
"... og til og med la den falle på hverandre i dette tilfellet.",
"Jeg kan til og med gjøre dette alene... Jeg kan svømme ned under den... eller legge den på denne hylle.",
"Vi kan også dytte en gjenstand langs toppen av en annen gjenstand som vi støtter, men vi kan ikke flytte gjenstanden som ligger direkte på ryggen vår.",
"Vi kan også svømme fritt under en gjenstand som vi støtter og gi den til den andre fisken.",
"Og vi kan dytte gjenstander av hverandres rygg, så lenge gjenstanden faller ned umiddelbart.",
"Oppsummert kan vi bare løfte gjenstander, la dem falle, dytte dem langs en fast overflate, dytte dem oppå en overflate og dytte dem av hverandre.",
"Det er omtrent reglene. Hvis du vil vite mer, trykk på F1 og les hjelpeseksjonen.",
}
l2wecouldrestart={"Vi kan starte nivået på nytt.",
"Det er sant...",
}

l2icantliftit={"Jeg klarer ikke å løfte det. Kan du prøve?",
"Bare litt høyere så jeg kan svømme under deg.",
}

l2lethisbytheway={
"Skal vi se det igjen?",
}

l2nowell={"Nå starter vi på nytt - eller så kan vi laste inn det lagrede spillet ved å trykke på F3-tasten.",
"Nå er vi tilbake der vi sist lagret spillet.",
}

l2again={"På nytt, laster vi inn det lagrede spillet ved å trykke på F3-tasten.",
}

l2wehaveloaded={"Vi har lastet inn det lagrede spillet for siste gang. Nå skal vi vise deg alt vi er i stand til.",
}

l2briefcasetalkie={"God morgen, fisk.",
"Dette er en sak av aller største viktighet, og derfor har vi valgt deg,",
"våre mest dyktige undervannsagenter.",
"Agenter for FDTO - Fiske Detektiv Trening Organisasjonen -",
"klarte å få tak i flere amatørbilder av et utenomjordisk objekt",
"som har krasjet et sted i nærheten av deg.",
"Din oppgave, hvis du velger å godta den,",
"vil være å hente UFO-en og skaffe all informasjon du kan om prinsippene og naturen til interstellær fremdrift.",
"Du bør også prøve å løse noen av våre fortsatt uløste saker.",
"Vi er særlig interessert i omstendighetene rundt forliset av den mytiske byen, som tilfeldigvis er i samme område.",
"Denne saken kan være knyttet til mysteriet med Bermuda-trekanten.",
"Finn ut hvorfor så mange skip og fly har forsvunnet i dette området de siste tiårene.",
"Det sies at ett av de forsvunne skipene tilhørte den legendariske Kaptein Silver.",
"Det er fortsatt mange usikkerheter rundt denne berømte piraten.",
"Framfor alt er vi interessert i kartet som viser plasseringen av hans begravde skatt.",
"En av dine viktigste oppgaver er å finne datamaskinen som er skjult i dypet av en bestemt kriminell organisasjon.",
"Den inneholder data om et prosjekt som kan forandre hele verden.",
"Du bør også finne og arrestere den mystiske koralturtelen.",
"Den rømte fra FDTOs forhørsfasilitet.",
"Den er ikke bevæpnet, men den skal visstnok ha telepatiske evner.",
"Vi har fått beskjed om at en visst kjernekraftverk har dumpet radioaktivt avfall ulovlig.",
"Sjekk det ut. Og glem ikke å finne den hellige gral.",
"Og som vanlig: Hvis noen fra teamet ditt blir skadet eller drept,",
"vil Altar nekte enhver kjennskap til deres eksistens, og nivået vil bli startet på nytt.",
"Denne disken vil selvdestruere om fem sekunder.",
}


