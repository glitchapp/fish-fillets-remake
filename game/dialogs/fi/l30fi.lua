l30={"En ole koskaan nähnyt sellaisia violetteja koralleja.",
"En pidä violeteista koralleista.",
"Rakastan violetteja koralleja...",
"Ne näyttävät mukavilta minusta.",
"Toivottavasti tulevissa tasoissa on vielä mielenkiintoisempia koralleja.",
"Jumala varjelkoon.",
"Meidän täytyy nostaa tämä teräs...",
"Hei, rapu!",
"Eikö meidän kannattaisi yrittää piristää tuota rapua hieman?",
"Älä välitä siitä.",
"Jätä se rapu rauhaan.",
"Etkö näe, kuinka hermostunut se on?",
"Yritä pärjätä ilman sitä hermostunutta rapua.",
"Tuosta rapusta voi olla hyötyä.",
"Tämä rapu auttaa meitä varmasti ratkaisemaan sen.",
}

l30crab={"Kuka herätti minut?",
"Jätä minut rauhaan, nukun täällä!",
"Mitä haluat minusta?!?",
"Älä koske minuun!",
}

l30crabups={
"Ups.",
}
