l18={"Taas se kummitusvene.",
"Näetkö sen? Jotain on muuttunut sillä aikaa kun olimme poissa.",
"Sanon sinulle, että joku tarkkailee meitä.",
"Miksi tuijotat meitä koko ajan? Etkö näe, että se hermostuttaa meitä?",
"Näyttää siltä, että heidän piknikinsä päättyy.",
"Piknik on ohi.",
"Siitä jäi jäljelle vain tämä piknik.",
"He ovat pitäneet jonkinlaista piknikkiä pohjalla.",
"Luulen, että juhlijat ovat hieman väsyneitä.",
"Kyllä, he ovat saaneet tarpeekseen.",
"Luulen, etteivät he voi enää vahingoittaa meitä.",
"Luulen, että he ovat melko vaarattomia nyt.",
"Ei tule olemaan yhtä helppoa kuin viime kerralla.",
"Näyttää siltä, että tällä kertaa tulee olemaan vaikeampaa.",
}
