l29={"Katsos kummaa!",
"Kuvittelehan!",
"Kuka olisi arvannut?!",
"Moista huolimattomuutta.",
"Tuhanneet menehtyivät - koko kaupunki katosi aaltojen alle tällaisen osaamattomuuden takia.",
"Irrotettu pistoke.",
"Nolo kömmähdys.",
"Mitä meidän pitäisi tehdä sen kanssa?",
"Voimme yrittää laittaa sen takaisin paikalleen.",
"Ja sitten? Juommeko sen sisään tulvivan veden vai mitä?",
"Voimme laittaa sen takaisin paikalleen osoituksena kunnioituksestamme kansalaisten sankarilliselle ponnistukselle pitää se kelluvana. Muistomerkkinä heidän ahkerasta, taitavasta ja... sinnikkäästä luonteestaan.",
"Mistä? 'Minne olet menossa? Tarvitsen tilata kahdeksan miekkaa.' Itse Kohtalo irrotti sen aukon. Kuvittelehan, kuuletko tuollaisia asioita kotona. Päivä päivältä.",
"Ja lisäksi: Onko todennäköistä, että kukaan tulisi tänne? Ainoastaan sepiat pureskelevat sitä satunnaisesti.",
"Tulemme tuntemaan olomme hyväksi siitä.",
"Tulemme tuntemaan olomme huonoksi siitä. Luuletko pomojen uskovan sitä? Jättimäinen irrotettu pistoke? Ja mitä sinä olet tehnyt sen kanssa? Laitoimme sen takaisin. Heh, heh, heh.",
"Voi olla että olet oikeassa. Ehkä olisi parempi ottaa se mukaamme.",
"Niin minustakin.",
"Ryhdytään töihin.",
"Entä jos jätämme tuon pistokkeen tähän?",
"Mitä viranomainen sanoo?",
"En usko, että välitän siitä, mitä pomot ajattelevat minusta.",
"Odota. Olemme varmasti ratkaisee sen.",
"Perkeleen pistoke. Mitä jos keksisimme jotain.",
"Kuten mitä, esimerkiksi?",
"No, voisimme sanoa, että kaupunki ei tosiasiassa koskaan ollut olemassa.",
"No, voisimme sanoa, että kaupunki upposi, koska Arktisen jää suli.",
"Maanjäristys olisi voinut upottaa kaupungin, esimerkiksi.",
"Tsunami olisi voinut upottaa kaupungin, esimerkiksi.",
"Voisimme yrittää sanoa, että tulivuori purkautui kaupungin keskellä.",
"Se on pelkkää hölynpölyä.",
"Tällä kertaa tavoitteenamme on saada se pistoke pois.",
}

l29end={"Onnistuimme selvittämään kaupungin uppoamisen oikean syyn.",
"Kyseessä oli tunnettu esine nimeltään atl",
"anttinen reliefi,",
"joka aiemmin virheellisesti luultiin kuvastavan avaruusolentojen vierailua,",
"ja se antoi meille vihjeen.",
"Samalla suosittelemme lisäämään valvontaa",
"kaikkien mantereiden ja suurempien saarten pistokkeiden suhteen,",
"jotta vastaavilta kömpelöiltä katastrofeilta vältyttäisiin tulevaisuudessa.",
}
