l23={"Luulen, että olemme vihdoin löytäneet jäljet tuohon salaperäiseen kaupunkiin.",
"Se näyttää upealta.",
"Nyt meidän täytyy vain selvittää, miksi se upposi, ja olemme valmiit.",
"Mutta se voi viedä hieman aikaa.",
"Jätä minut rauhaan. Ihailen tämän temppelin klassista kauneutta.",
"Kuka istuu tuossa tuolissa?",
"Se on kopio Feidioksen Zeuksesta. Yksi maailman seitsemästä ihmeestä.",
"Hmm. Se oli toinen aikakausi.",
}

l23zeus={"Varo! Yritä olla vahingoittamatta sitä!",
"Mikä tuhlausta!",
"Ja nyt laske se varovasti alas.",
"Sinä barbaari! Etkö voisi olla hieman varovaisempi?!",
"En pitänyt tästä patsaasta kuitenkaan.",
}
