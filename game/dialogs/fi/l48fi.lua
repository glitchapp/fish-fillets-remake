l48={"Voi juku, tämä on hieno keittiö.",
"Kyllä, löysimme laivan keittiön.",
"Pirskatin keittiö.",
"Ja nyt olemme kapteeni Silverin keittiössä.",
"En usko, että hän itse oli kokki.",
"Mutta tämä ei ole meidän ongelmamme.",
"Täällä ei ole paljon ongelmia. Voin uida heti ulos.",
"Mutta katsohan minua.",
"En ymmärrä, miten nämä tarjoilupöydät ovat päätyneet tänne.",
"Nämä pöydät ovat tiellä.",
"Tämä pieni tila saa minut kauhun valtaan.",
"En pääse täältä ulos!",
"Mietin vain, miten voisin auttaa sinua.",
"Auta minua jotenkin!",
"Mietin...",
"Olen matkalla.",
"Yäk, tässä kattilassa on jotain jäänyt!",
"Tätä kattilaa ei ole edes pesty.",
"Kukaan ei pese astioita täällä.",
"Jos täällä ei olisi tuolia, säästäisi paljon vaivaa.",
"Etenkin minulle.",
"Mitä tuoli tekee keittiössä?",
"Keittiössä ei pitäisi olla tuoleja.",
"Kaikki haluavat joskus levätä.",
"Olen nähnyt näitä pöytiä jossain.",
"Ne ovat massatuotantoa, tiedäthän.",
"Mutta ne ovat hienoja. Pitäisi tilata muutama myös meille.",
"Pelkään, että vain minä pääsen tästä tilasta ulos.",
"Vaikuttaa siltä, että tämäkin laiva sisältää joitain vaativia ongelmia.",
"Miksi tämä miekka on täällä?",
"Mahdollisesti sitä käytettiin lihan leikkaamiseen.",
"Totta. Mutta yleensä keittiössä ei leikata lihaa miekalla.",
"Onneksi täällä ei ole kokkeja. He saattaisivat yrittää keittää meidät.",
"Tuolla pergamentilla täytyy olla jokin resepti.",
"Tuolla pergamentilla täytyy olla jokin kulinaarinen mysteeri.",
"Mahdollisesti, miten valmistaa kala.",
}
