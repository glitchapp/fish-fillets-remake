l20={"Mikä komea sivilisaatio. Niin monta vuosisataa veden alla ja heidän hissinsä toimivat yhä.",
"Erityisesti, kun työnnämme niitä.",
"Mutta vain alaspäin.",
"Näetkö sen simpukan?",
"Eikö tuo simpukka ole hieman tuttu?",
"Se on ikivanha talismaani hissien rakentajille, jotka aina sanoivat 'Rakenna hyvin. Rakenna Shellillä'.",
"Heidän pitäisi hävetä. Tämä on alitajuinen mainonta.",
"Tuo pääkallo näyttää tutulta.",
"Kaikki ihmisten pääkallot näyttävät samalta... Toisin kuin kalan luurangot.",
"Tämä on vaikea polku.",
"Tunnen itseni hiireksi juoksupyörässä.",
"Juoksupyörä kaloille? Mutta meillä on vain evät.",
"Hoo, hooooo, hoooo... Olen täälläääää...",
"Hooo, jos haluat, voin kertoa sinulle kuinka ratkaista kaikki tasot...",
"Hooo, ho, hooo, tiedän miksi kaupunki upposi!",
"Hoooo, miksei kukaan kiinnitä huomiota minuun?",
"Hoooo ho hooo! Miksi te ohitatte minut?",
"Hooooo, hooooo, kala! Kiinnitä huomiota minuun!",
}

l20skull={"Voisitteko kaikki pitää omasta hommastanne? Nautimme itse asiassa nukkumisesta.",
""
}
