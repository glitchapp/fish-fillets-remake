l5 = {
    "Jotain putosi taas tänne.",
    "Se on jonkinlainen kuori.",
    "Meidän täytyy luultavasti työntää se yläkertaan uudelleen.",
    "Näen sen jo: 'Kuori kohtaa kukkaruukun'.",
    "Se on melkein kuin silitysrauta ja ompelukone tapaisivat leikkauspöydällä.",
    "Anteeksi, innostuin vähän liikaa.",
    "Anteeksi, olin hetken häiriintynyt.",
    "Minne olen menossa tämän kuoren kanssa?",
    "",
}
