l1 = {
  "Mikä se oli?",
  "Ei hajuakaan.",
  "Meidän pitäisi mennä katsomaan ulos.",
  "Odota! Menen mukaasi.",
  "Miksi mikään ei tapahdu?",
  "Hei, pelaaja!",
  "Voit ohjata meitä nuolinäppäimillä ja käyttää välilyöntiä vaihtaaksesi kalaa.",
  "Tai voit ohjata meitä hiirellä -- klikkaa jonnekin vasemmalla ja...",
  "...jos pääsemme sinne liikuttamatta mitään...",
  "...seuraamme sinun klikkauksiasi.",
  "Ja kun napsautat jonnekin hiiren oikealla painikkeella, yritämme päästä sinne työntämällä kaikki esteet tieltämme.",
  "Tavoitteesi tulisi olla saada meidät molemmat ulos.",
  "Ja ole varovainen, ettet satuta meitä. Jos pudotat jotain päällemme tai...",
  "Lopeta jo. Kerromme hänelle seuraavalla tasolla.",
  "Selvä.",

  "Vau, siirrätpä sitä! En ikinä pystyisi siihen!",
  "Kiitos, nyt voin lähteä mukaasi.",
  "Hitto, en pääse läpi.",
  "Olen varmaan lihonut.",
  "Voisitko työntää tuolia hieman vasemmalle?",
  "Oho! Se oli virhe. Meidän molempien on pystyttävä poistumaan jatkaaksemme.",
  "Ei ole mitään tapaa, että pääsisin ulos nyt. Sinun on käynnistettävä taso uudelleen.",
  "Sinun täytyy painaa pitkää nuolta näppäimistössäsi.",
  "Ihmiset kutsuvat sitä backspace-näppäimeksi.",
  "Mitä väliä. Se oli hauskempaa tšekiksi.",
  "Uudelleenkäynnistysvaihtoehto sijaitsee myös hallintapaneelissa.",
  "Jos haluat mieluummin käynnistää tason uudelleen, ei ongelmaa.",
  "Sinun tarvitsee vain painaa pitkää nuolta näppäimistössäsi.",
  "Olen raskas teräsputki. Minun on vaikea liikkua. Sellainen pieni kala, kuten tuo oranssi tuolla, ei yksinkertaisesti ole mahdollisuutta. Voisin murskata hänet ongelmitta.",
}

l1icantgetthrough = {
  "En pääse tästä läpi. Tuo teräsputki on tiellä.",
  "En voi liikuttaa sitä putkea. Voitko auttaa minua?",
  "Ei ongelmaa...",
}

l1wowyoumovedit = {
  "Vau, siirrätpä sitä! En ikinä pystyisi siihen!",
  "Kiitos, nyt voin lähteä mukaasi.",
}

l1damnit = {
  "Hitto, en pääse läpi.",
  "Olen varmaan lihonut.",
  "Voisitko työntää tuolia hieman vasemmalle?",
}
