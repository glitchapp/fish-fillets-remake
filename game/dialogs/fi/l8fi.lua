l8 = {
    "WC on Davidin suosikkipaikka.",
    "Mitä?",
    "Et tunne Davidia? Hän on yksi tämän pelin taiteilijoista.",
    "David ei ollut graafinen taiteilija. Hän oli kuvanveistäjä. Hänen veistoksensa ovat yksi tunnetuimmista renessanssin artefakteista.",
    "Olet aivan väärässä.",
    "Enkö sanonut sinulle, ettemme tarvitse vetävää wc-istuinta? Ja vielä koristeltuna sinun likaisuudellasi!",
    "Vetävä wc-istuin on hygieenisempi ja veden alla vielä ekologisempi.",
    "Onneksi minun ei tarvitse kiivetä sisään.",
    "En tunne oloani kovin hyväksi täällä. Tuntuu kuin olisin hautausmaalla.",
    "Mitä tarkoitat?",
    "Et kai tiedä, mihin niin monet akvaarioissa elävät kalat päätyvät?",
}
