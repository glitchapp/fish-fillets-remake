l3={"Tiedät jo säännöt.",
"Nyt voit harjoitella niitä.",
"Annamme sinulle satunnaisesti joitakin vihjeitä.",
"En saa uida alaspäin nyt.",
"Enkä voi mennä ylöspäin myöskään.",
"Hei, järjestä se jotenkin tuonne ylös.",
"Mutta ole varovainen!",
""
}

l3icantmove={"En voi liikuttaa tätä. Tämä on terästä."}

l3comeback={"Palaa takaisin, et voi mennä pidemmälle!"}

l3thisistricky={"Hmm... Tämä on hankalaa.",
"Jos työnnämme hunajaa sivuun, meitä murskataan aprikoosihillon tölkiltä.",
"Emmekö voisi työntää sitä pois tieltä tuolla kirveellä?",
"Se on hyvä idea.",
"Nyt en voi ottaa kirvestä. Tölkki on tiellä.",
"Sinun ei saa työntää kirvestä!",
"Jos voit työntää sen tölkin minulle, työnnän kirveen siihen ja kaikki on kunnossa."}

l3thislooksrealbad={"Tämä näyttää todella huonolta. Sinun täytyy aloittaa taso alusta."}


l3ifyousolvesomespecific={"Jos ratkaiset jonkin tietyn osan, voit tallentaa sijainnin.",
"Mutta vain, jos olet varma ratkaisustasi.",
"Muista mitä olemme sanoneet sijaintien tallentamisesta."}

l3youarestandinginmyway={"Seisoessasi siinä, olet tielläni. Yritä piiloutua jonnekin muualle.",
"Jos pudotat sen ja jään tänne, en pääse koskaan pois.",
"Täällä? Olen vähän peloissani. Pääsenkö pois?",
"Odota vain ja katso."}

l3ishoulndtbe={"En saisi ajatella tätä."}

l3wewillgiveyouahint={"Annamme sinulle vinkin täällä, pelaaja. Sinun täytyy laittaa se kirja tuonne vasemmalle.",
"Niin että se tarttuu siihen terässylinteriin, kun työnnän sitä.",
"Mutta emme kerro sinulle miten tehdä se.",
"Entä jos menen yläreittiä?",
"Shhhh. Se on liian monta vinkkiä."}

l3justremember={"Muista vain, että vaikka emme voi työntää esineitä toistemme selkiä vasten, voimme työntää niitä alas tai johonkin kiinteään rakenteeseen."}

l3thingslikethat={"Tällaisia asioita tapahtuu joskus."}

l3sometimes={"Joskus sinun on ajateltava todella pitkälle.",
"Ja usein sinun täytyy aloittaa taso alusta sitten."}

l3weadmitwedid={"Tunnustamme, että teimme tämän tarkoituksella.",
"Jotta voisit kokeilla uudelleen - tällä kertaa ilman mitään vihjeitä."}
