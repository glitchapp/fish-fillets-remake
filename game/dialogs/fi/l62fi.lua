l62={"Valtiokansliasta kynttilävarastoon. Sitä minä kutsun edistykseksi.",
"Sinun täytyy katsoa positiiviselta puolelta. Täällä ei näytä olevan mitään Graalia muistuttavaa.",
"Näyttää siltä, että minun täytyy mennä läpi tuon kauhean sokkelon.",
"Lihava vartalosi. Se vain estää meitä aina.",
"Jätä minut rauhaan, sinä laiha vesilisko.",
"Lopeta se jaarittelu, sinä muodoton lihas- ja suomukasa, kuuletko?",
"Tuntuu kuin olisin keramiikkavarastossa.",
"Tarkoitatko amforavarastoa.",
"Ei, sen pitäisi olla amforavarasto.",
"Tiedätkö mitä? Luulen, että tämä tehtävä auttoi minua todella.",
"Ja miksi?",
"Tajusin, että jalokivet ja kulta ovat vain halpoja koristeita.",
"En enää kestä tuota kirottua kamaa!",
}

--"Voisitko käynnistää tason uudelleen, kiitos? Meillä ei tunnu sujuvan kovin hyvin nyt.",
}
