l13={"Tämä täytyy olla viikinkilaiva!",
"Totta, se on drakkaari.",
"Hän on varmasti uponnut nopeasti. Soutajat ovat yhä paikoillaan.",
"Mahdollisesti he upottivat sen tarkoituksella. Soturit seurasivat aina johtajaansa matkalla Valhallaan.",
"Kamalaa. He näyttävät siltä, kuin olisivat yhä elossa.",
"Musiikki lakkasi!",
"He lopettivat soittamisen!",
"Musiikki lakkasi taas!",
"Oh, ei!",
"Taasko?",
"Oh, ei taas!",
"Mitä tapahtui?",
"Uudelleen?",
"Oh, ei!",
"Pirun musiikki loppui!",
"Mitä he tekevät tuolla ylhäällä?",
"Kuinka se voi olla mahdollista?",
"Rauhoittukaa, pojat. Olaf soittaa soolon!",
"Ei se haittaa, pojat. Me osaamme laulaa itse!",
"Ei se haittaa, pojat. Me osaamme laulaa itse, kuten ennen vanhaan!"
}

l13skull={"Ettekö voi jättää meitä rauhaan? Yritämme hieman levätä täällä.",
""
}
