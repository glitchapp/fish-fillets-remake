l59={"Näetkö sen merihevosen?",
"Se on estetty amphoroiden keskellä.",
"Se on juonut itsensä humalaan amphoroiden joukossa.",
"Ihmettelen, onko niissä vielä jotain jäljellä.",
"Sinun täytyy varmaan mennä tarkistamaan itse.",
"Vihdoinkin näen jonkin uudenlaisen kalloksen.",
"Huomasitko sen totemin? Se on meksikolainen jumala Shelloxuatl.",
"Näyttää siltä.",
"Nämä amphorat putoavat sietämättömän hitaasti.",
"No, et ole lopulta karhu.",
"Älä unohda, että olemme veden alla.",
"Yäk. Kirjoittajat olisivat voineet säästää meidät tuolta animaatiolta.",
"Tämä totemi näyttää hyvältä minusta.",
"Se kallo tuntuu säteilevän jotain outoa.",
"Onko se elossa vai onko se jonkinlainen loitsu?",
"",
"",
"",
}

l59skull_canyouall={"Voitteko kaikki pitää omista asioistanne? Nautimme itse asiassa nukkumisesta.",""}

l59skull_wellanother={"No, taas yksi levottoman unen yö uteliaiden ihmisten takia",""}

l59skull_welliwas={"No, olin nukkumassa, kun minut niin töykeästi herätettiin.",""}

l59skull_anothernight={"Taas yksi rauhallisen unen yö pilalla.",""}
