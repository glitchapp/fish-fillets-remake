l2 = {
  "Voi ei, taas näin...",
  "Tiedät kyllä, ettei meillä ole mitään keinoa estää sitä.",
  "Työntäkäämme se alas, jotta voimme tarkastella sitä.",
  "No, ryhdytään töihin.",
}

l2thediskself = {
  "Miten? Levy tuhoutui itsestään.",
  "Joten nyt voimme vihdoin aloittaa.",
  "Ensimmäinen tehtävämme on päästä ulos tästä huoneesta.",
  "Tämä tulee olemaan tehtävämme myös kaikissa muissa huoneissa.",
  "Tulkaa, viemme pelaajan työpajaan ja näytämme hänelle työturvallisuusmääräykset.",
  "Hyvä idea.",
  "Älä toistaiseksi koske mihinkään, vain katso ja opi. Näytämme sinulle, mitä sinun pitäisi ja ei pitäisi tehdä meidän kanssamme, sekä mitä me pystymme tekemään.",
  "Ennen työpajaan menoa tallennetaan peli - paina vain F2-näppäintä.",
  "Ensinnäkin näytämme sinulle, mikä voi satuttaa meitä.",
  "Minä voin olla koejänis.",
  "Emme saa pudottaa esineitä toistemme päälle.",
  "Emme myöskään voi uida alaspäin, kun kannamme esinettä, koska se pudottaisi sen päällemme.",
  "Emme myöskään saa työntää esineitä selkämme päällä.",
  "Jotkut esineet on muotoiltu niin, että voisimme pitää niitä ja työntää niitä samanaikaisesti - mutta sitäkään emme saa tehdä.",
  "Emme myöskään saa pudottaa lisäesineitä pinon päälle, jonka jompikumpi meistä kantaa.",
  "Ja toisin kuin isompi kumppanini, en voi liikuttaa tai edes kantaa teräsesineitä.",
  "Voimme aina nostaa esineitä ja sitten päästää ne tippumaan.",
  "Voimme työntää esinettä toistemme selkää vasten vain, jos esine työntyy sitten toisen tuen päälle...",
  "... ja voimme jopa päästää sen putoamaan toisemme päälle tässä tapauksessa.",
  "Voin jopa tehdä tämän yksin... Voin uida sen alapuolelle... tai laittaa sen tälle tasanteelle.",
  "Voimme myös työntää yhtä esinettä toisen esineen päällä, jota tuemme, mutta emme voi liikuttaa esinettä suoraan selkämme päällä.",
  "Voimme myös vapaasti uida esineen alla, jota tuemme, ja siirtää sen toiselle kalalle.",
  "Voimme työntää esineitä toistemme päältä, kunhan esine putoaa heti alas.",
  "Yhteenvetona voimme vain nostaa esineitä, päästää ne tippumaan, työntää niitä kiinteällä pinnalla, työntää niitä pinnalle ja työntää niitä pois toistemme päältä.",
  "Siinäpä ne säännöt. Jos haluat tietää enemmän, paina F1-näppäintä ja lue ohjesivu.",
}

l2wecouldrestart = {
  "Voisimme aloittaa tason alusta.",
  "Se on totta...",
}

l2icantliftit = {
  "En pysty nostamaan sitä. Voitko kokeilla?",
  "Vielä vähän korkeammalle, jotta voin uida sinun alitse.",
}

l2lethisbytheway = {
  "Katsotaanko se uudelleen?",
}

l2nowell = {
  "Aloitetaanpa uudelleen - tai voimme ladata tallennetun pelin painamalla F3-näppäintä.",
  "Ja nyt olemme takaisin siihen kohtaan, jossa tallensimme pelin viimeksi.",
}

l2again = {
  "Uudelleen, lataamme tallennetun pelin painamalla F3-näppäintä.",
}

l2wehaveloaded = {
  "Olemme ladanneet tallennetun pelin viimeisen kerran. Nyt näytämme sinulle kaiken sen, mihin me pystymme.",
}


l2briefcasetalkie = {
  "Hyvää huomenta, kala.",
  "Tämä on vakavimmasta päästä oleva tapaus, ja siksi olemme valinneet teidät,",
  "kyvykkäimmät vedenalaiset agenttimme.",
  "FDTO:n - Fish Detective Training Organization - agentit",
  "ovat onnistuneet saamaan haltuunsa useita amatöörivalokuvia avaruusolentojen esineestä,",
  "joka on törmännyt jonnekin lähialueillenne.",
  "Teidän tehtävänne, mikäli päätätte sen ottaa vastaan,",
  "on palauttaa UFO ja hankkia kaikki mahdolliset tiedot tähtien välisestä voimanlähteestä ja sen ominaisuuksista.",
  "Teidän tulee myös yrittää selvittää joitain meidän vielä ratkaisemattomia tapauksiamme.",
  "Olemme eniten kiinnostuneita myyttisen kaupungin uppoamiseen liittyvistä olosuhteista, sattumalta samalla alueella.",
  "Tämä tapaus saattaa liittyä Bermuda-kolmion mysteeriin.",
  "Selvittäkää, miksi niin monet laivat ja lentokoneet ovat kadonneet tällä alueella viimeisten vuosikymmenten aikana.",
  "Erään kadonneen laivan väitetään kuuluneen legendaariselle Kapteeni Silverille.",
  "Tämän kuuluisan merirosvoon liittyy yhä paljon epävarmuutta.",
  "Eniten meitä kiinnostaa kartta, joka osoittaa hänen kätketyn aarteensa sijainnin.",
  "Yksi tärkeimmistä tehtävistänne on löytää syvyyksiin piilotettu tietokone erään rikollisjärjestön toimesta.",
  "Siinä on tietoja projektista, joka voisi muuttaa koko maailman.",
  "Teidän tulee myös löytää ja pidättää salaperäinen korallikilpikonna.",
  "Se pakeni FDTO:n kuulustelutiloista.",
  "Se ei ole aseistautunut, mutta sillä väitetään olevan telepaattisia kykyjä.",
  "Olemme saaneet tiedon, että eräs ydinvoimala on laittomasti dumpannut radioaktiivista jätettä.",
  "Tarkistakaa se. Älkääkä unohtako löytää pyhää Graalia.",
  "Ja kuten tavallista: Mikäli joku tiimistänne loukkaantuu tai kuolee,",
  "Altar kiistää tietävänsä teidän olemassaolostanne, ja taso aloitetaan alusta.",
  "Tämä levy tuhoutuu itsestään viiden sekunnin sisällä.",
}
