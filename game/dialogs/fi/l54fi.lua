l54={"Luulen, että voimme saada täältä tietoa tähtien välisestä työntövoimasta.",
"Tämä näyttää enemmän perinteiseltä työntövoimalta laskeutumisalukselle.",
"No niin. Löysimme työntövoiman. Olemme saavuttaneet yhden tehtävämme tavoitteista.",
"Älä ole niin kiireinen. Emme ole vielä tutkineet koko hylyä.",
"On harmi, että nämä tekniset ihmeet ympärillämme eivät toimi.",
"Haluaisin nähdä jotain näistä avaruuden ulkopuolisista vempaimista toiminnassa.",
"Ihmettelen, voisiko tämä moottori toimia veden alla.",
"Kenties on parempi, että se ei toimi.",
"Olen pikemminkin iloinen siitä, ettemme voi kytkeä mitään päälle täällä.",
"Meidän tulisi olla iloisia, että emme ole vielä saaneet mitään päälle."
}

l54engineon={"Mitä olet tehnyt? Sammuta se jylinä!",
"Mitä olet aktivoinut? Minne se vie meidät?",
"Tämä on kauheaa! Sammuta se ennen kuin on liian myöhäistä!",
"En pysty! En pysty ottamaan sitä pois!",
"En tiedä miten! En pysty ottamaan sitä pois!",
"Mayday! Mayday!",
"Kuinka voin sammuttaa sen?!",
"En pysty sammuttamaan sitä!"
}

l54engineonfish1={"Mitä olet tekemässä? Minne olemme menossa?",
"Mitä olet aktivoinut? Minne se vie meidät?",
"En pysty! En pysty ottamaan sitä pois!",
"En tiedä miten! En pysty ottamaan sitä pois!",
"Mayday! Mayday!",
"Kuinka voin sammuttaa sen?!"
}

l54engineoff={"Vihdoin.",
"Mikä helpotus.",
"Kiitos.",
"Vihdoin.",
"Pelkään vain, että meidän täytyy kytkeä se taas päälle.",
""
}

l54aproachengine={"Mitä sinä teet? Minne olet menossa?",
"Varovasti sen jakoavaimen kanssa.",
"En voi vahingoittaa mitään täällä."
}
