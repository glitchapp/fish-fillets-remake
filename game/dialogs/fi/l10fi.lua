l10={"Katso. Juhlavene.",
"Mulla on tunne, että tästä ei tule normaali piknik.",
"Sinun ja sun tunteidesi kanssa. Varmasti siellä on ystävällistä ja mukavaa porukkaa.",
"Saattaa olla, että olit oikeassa. Tämä on outo piknik.",
"Luuletko, että he pääsevät pois tuosta veneestä?",
"Toivottavasti he eivät pääse pois tuosta veneestä.",
"Entä jos he alkavat seurata meitä?",
"En usko, että he saavat meidät kiinni. Luurangot eivät osaa uida kovin hyvin.",
"Lopeta murehtiminen ja liikuta eviäsi, jotta pääsemme täältä pois.",
"Haluat tukea tuota teräksistä putkea lasilla?",
"En voi tehdä muuta sinun puolestasi.",
"Varo, ettet kaada sitä!",
}
