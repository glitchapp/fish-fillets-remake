l7 = {
    "Voi hyvänen aika, törmäys aiheutti sortuman!",
    "En pääse tämän sortuman läpi,",
    "meidän täytyy löytää toinen uloskäynti.",
    "Tämä ei ole oikea reitti.",
    "En selviä yksin.",
    "Olisin ehkä ensin pitänyt nostaa sen kuoren.",
    "Ehkä meidän täytyy vaihtaa näitä esineitä.",
    "Patja. Parasta, mitä voi saada veden alla.",
}

l7part2 = {
    "Nyt voimme pudottaa etanan patjalle.",
    "Etanan nostaminen sieltä voi olla vaikeaa.",
    "Vihdoinkin, se on siellä.",
    "Köyhä etana...",
    "Meidän pitäisi etsiä koralliriuttoja.",
    "Siellä on paljon mielenkiintoisia olentoja tutkittavaksi.",
    "Tarvitsemmeko mikroskooppia korallien tutkimiseen?",
    "Kyllä, ne ovat pieniä. Mutta siellä voi olla muita elämänmuotoja.",
    "Esimerkiksi korallikilpikonnia.",
    "Ja lisäksi minulla on epäilys, että sukellusveneessä on mikroskooppi.",
}
