l3={"Pravidla už znáš.",
"Teď si je můžeš vyzkoušet v praxi.",
"Budeme ti trochu radit.",
"Já například teď nesmím podklesnout dolů. A nahoru to také moc nejde.",
"Hej, urovnej mi to tam nahoře nějak. Ale opatrně!",
""
}

l3icantmove={"S tímhle já nepohnu, to je ocel."}

l3comeback={"Pojeď zpátky, dál nemůžeš!"}

l3thisistricky={"Hmmm... zapeklitá věc. Jakmile odstrčíme med, spadne na nás meruňková marmeláda.",
"Co takhle to vyrazit tou sekyrou?",
"Dobrý nápad.",
"Teď už tu sekeru nemůžu chytit, když jsme tam dali sklenici.",
"Tu sekeru nemůžeš vzít!",
"Když mi sem přisuneš sklenici, hodím ji na ni, to bude v pohodě."}

l3thislooksrealbad={"Tohle vypadá špatně. Asi budeš muset místnost restartovat."}


l3ifyousolvesomespecific={"If you solve some specific part you can save the position.",
"But only if you are sure of your solution.",
"Remember what we have said about saving positions."}

l3youarestandinginmyway={"Jestli to pustíš a já zůstanu tady, nedostanu se už nikdy ven.",
"Tam mi zavazíš, zkus se schovat jinde.",
"Tady? Mám trochu strach. Dostanu se pak ven?",
"Nech se překvapit."}

l3ishoulndtbe={"O tomhle bych ani neměla uvažovat.",}

l3wewillgiveyouahint={"Tady ti, hráči, poradíme - tu knihu musíš dostat támhle vlevo.",
"Aby se na ní zachytil ten ocelový válec, až do něj strčím.",
"Ale jak to udělat, v tom ti nebudeme radit.",
"Jen nezapomeň, že i když po sobě nemůžeme předměty posouvat, můžeme je na něco nasouvat a nebo je ze sebe shazovat.",
"Co kdybych to objel vrchem.",
"Pssst. Řekli jsme neradit."}

l3justremember={"Just remember that although we cannot push objects along each other’s back, we can push them down or onto some solid structure."}

l3thingslikethat={"Things like that happen sometimes."}

l3sometimes={"Sometimes you have to think faaar ahead.",
"And often you have to restart the level then."}

l3weadmitwedid={"We admit that we did this on purpose.",
"So that you could try it again - this time without any hints."}
