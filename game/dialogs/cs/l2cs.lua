l2={"Jé, už zase...",
"No, co naděláme.",
"Hodíme to dolů a podíváme se na to.",
"Tak, pusťme se do práce.",
"Tak, a teď už se do toho konečně pustíme.",
}

l2thediskself={"Naším prvním úkolem bude dostat se ven z místnosti.",
"To bude ostatně naším úkolem ve všech místnostech.",
"Nepodíváme se na to ještě jednou?",
"Jak? Disk se zničil.",
"Mohli bychom místnost restartovat.",
"To je pravda...",
"Pojď, vezmeme hráče do dílny a ukážeme mu pravidla bezpečnosti práce.",
"Dobrý nápad.",
"Já to nenadzvednu, zkus to ty.",
"Ještě kousek, ať pod tebou můžu projet.",
"Teď na nic nesahej, jen se dívej. Ukážeme ti, co bys s námi neměl dělat a co dělat umíme.",
"Než vstoupíme do dílny, uložíme si pozici - dělá se to klávesou F2.",
"Nejprve ti ukážeme, co nám ubližuje.",
"Já budu dělat figuranta.",
"Především bychom na sebe neměli házet předměty.",
"Stejně tak nesmíme podklesnout pod předmětem, který neseme - spadnul by na nás.",
"Nyní začínáme znovu - můžeme však nahrát uloženou pozici klávesou F3.",
"Tak, a jsme na místě, kde jsme hru ukládali.",
"Další věc, kterou nesmíme dělat, je posunovat si předmět po hřbetě.",
"Některé předměty mají takový tvar, že bych je mohla zároveň držet i posouvat - ale to také	25 nesmím.",
"Znovu nahrajeme pozici klávesou F3.",
"Nesmíme ani přihazovat předměty na hromádku, kterou někdo z nás drží.",
"A já, na rozdíl od většího kamaráda, nejen že nepohnu s ocelovými předměty, ale nesmím je ani držet.",
"Naposledy jsme nahráli pozici. Teď ti ukážeme, co dělat umíme.",
"Předmět můžeme vždy zvedat a zase pouštět.",
"Pokud se těsně před tím, než by nás měl zamáčknout, někde zachytí, můžeme ho i posouvat jedna po druhé...",
"... a dokonce ho na sebe pouštět.",
"To mohu udělat i sama... podklesnu pod ním... a nebo jej nasunu támhle na tu polici.",
"Také můžeme posouvat předmět po jiném předmětu, který držíme, ne však přímo po zádech.",
"Můžeme také pod předmětem volně proplouvat a předávat si jej.",
"A pokud předmět hned spadne, můžeme ho jedna z druhé shodit.",
"Tak, to by asi bylo z pravidel všechno. Chceš-li vědět více, stiskni F1 a přečti si návod.",
"Já bych to ještě shrnula. Předměty můžeme beztrestně pouze zvedat, pouštět, posouvat po pevné podložce, na něco je nasouvat nebo je ze sebe shazovat.",
}

l2wecouldrestart={"We could restart the level.",
"That’s true...",
}

l2icantliftit={"I can’t lift it. Can you give it a try?",
"Just a little bit higher so I can swim underneath you.",
}

l2lethisbytheway={
"Should we watch it again?",
}

l2nowell={"Now we’ll start again - or we can load the saved game by pressing the F3 key.",
"And now we’re back where we last saved the game.",
}

l2again={"Again, we load a saved game by pressing the F3 key.",
}

l2wehaveloaded={"We have loaded our saved game for the last time. Now we’ll show you all the things we’re capable of.",
}



l2briefcasetalkie={"Good morning, fish.",
"This is an affair of the gravest importance and so we have chosen you,",
"our ablest underwater agents.",
"Agents of FDTO - Fish Detective Training Organization -",
"managed to get hold of several amateur snapshots of an extraterrestrial object",
"which has crashed somewhere in your vicinity.",
"Your mission if you choose to accept it,",
"will be to recover the UFO and acquire all the information you can about the principles and nature of interstellar propulsion.",
"You should also try to close some of our as yet unsolved cases.",
"We are mostly interested in the circumstances surrounding the sinking of the mythical city, by chance in the same area.",
"This case may be connected to the mystery of the Bermuda Triangle.",
"Find out why so many ships and planes have disappeared in this area over the past several decades.",
"One of the lost ships is rumored to have belonged to the legendary Captain Silver.",
"There are still many uncertainties surrounding this famous pirate.",
"Most of all we are interested in the map which shows the location of his buried treasure.",
"One of your most important tasks is to find the computer hidden in the deep by a certain criminal organization.",
"It contains data about a project which could change the entire world.",
"You should also find and detain the mysterious coral turtle.",
"It escaped from the FDTO interrogation facility.",
"It is not armed, but it supposedly has telepathic abilities.",
"We have been notified that a certain nuclear power plant has been dumping its radioactive waste illegally.",
"Check it out. And don’t forget to find the holy grail.",
"And as usual: If anyone from your team is injured or killed,",
"Altar will deny any knowledge of your existence and the level will be restarted.",
"This disk will self-destruct in five seconds.",
}
--"five, four, three, two, one"

