l54={"Myslím, že bychom tu mohli získat nějaké informace o mezihvězdném pohonu.",
"Tohle vypadá spíš na konvenční pohon pro výsadková vozidla.",
"Tak. Našli jsme motor. Mohli bychom prohlásit jeden z našich úkolů za splněný.",
"Nedělej ukvapené závěry. Ještě jsme neprohlédli celý vrak.",
"To je škoda, že všechna ta technika, kterou tu vidíme, už nefunguje.",
"Já bych tak chtěla vidět trochu té mimozemské techniky v akci.",
"To by mě zajímalo, jestli by tenhle motor fungoval i pod vodou.",
"Možná je pro nás lepší, že už tu nic nepracuje.",
"Já jsem spíš rád, že tu nemůžeme nic zapnout.",
"Naše štěstí je, že tu nemůžeme nic zapnout.",
}
l54engineon={"Opatrně s tím klíčem.",
"Tady přece nemůžu ničemu ublížit.",
"Co jsi to udělal? Vypni ten randál!",
"To jsou strašné zvuky! Vypni to dřív, než bude pozdě.",
"Nemůžu! Jak to mám vytáhnout?!",
"Nevím jak! Nedá se to vyndat!",
"Mayday! Mayday!",
"",
}

l54engineonfish1={"Co to děláš? Kam to jedeme?",
"Co jsi to aktivovala? Kam nás to veze?",
"Kde se to vypíná?!",
"Nedá se to vypnout!",
"",
"",
}
--"I can’t turn it off!"
l54engineoff={"Konečně.",
"To se mi ulevilo",
"Díky.",
"Konečně.",
"Bojím se, že to budeme muset zapnout znovu.",
"",
"",
}

l54aproachengine={"What are you doing? Where are you going?",
"Careful with that wrench.",
"I can’t harm anything here."
}

