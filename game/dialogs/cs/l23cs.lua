l23={"Myslím, že jsme skutečně na stopě tomu tajemnému městu.",
"Vypadá to velkolepě.",
"Teď už musíme jen zjistit, proč se vlastně potopilo a máme to v kapse.",
"Ale možná nám to bude ještě chvíli trvat.",
"Nech mě. Kochám se antickou dokonalostí tohoto chrámu.",
"Kdo to tam sedí na židli?",
"To je kopie Feidiova Dia. Jeden ze sedmi divů světa.",
"No. Tehdy byla jiná doba.",
}

l23zeus={"Opatrně! Ať to nepoškodíš.",
"Taková škoda!",
"Teď to opatrně pusť.",
"Barbarko! Nemůžeš si dát trochu pozor?!",
"Stejně se mi ta socha nelíbila.",
}
