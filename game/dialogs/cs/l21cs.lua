l21={"Vítejte v nejkrásnějším městě pod sluncem.",
"Vítejte v našem městě, městě neomezených možností.",
"Dovoluji si vás přivítat jménem občanů našeho města.",
"Vítejte v našem městě, před jehož krásou by mohli blednout i bohové.",
"Občané. Zachovejte klid a rozvahu.",
"Nehrozí žádné nebezpečí.",
"Potopení severní části ostrova je jen dočasné.",
"Situaci máme plně pod kontrolou.",
"K pátému molu připlula zpožděná loď z Lemurie. Loď dále pokračuje na Utopii, Mu a Atlantidu.",
"První rybářská pojišťovna pojistí váš dům proti požárům, větrným smrštím, zřícení nebeské klenby a náletům draků. Rybáři myslí na vše.",
"Kam běžíš?",
"Do Měsíční ulice, nechám si tam udělat osm mečů.",
"A proč tolik mečů?",
"Ty to ještě nevíš? Ke každým osmi mečům ti tam dají bronzové náušnice zdarma.",
"Tak počkej na mě, jdu s tebou!",
"Městský soud si vás dovoluje pozvat na veselé pásmo výslechů a poprav. Společenský oděv podmínkou.",
"Město se rozhodlo pro vaše pohodlí vybudovat vodovod. Zavedeme každému vodu až do domu.",
"Prosíme maminku, jejíž dítě se zaběhlo ke střeženému objektu městské pokladny, aby si jeho ostatky vyzvedla na vrátnici.",
"Občané. Zítra v půlnočních hodinách se k vám dostaví náš pracovník za účelem zaříkávání proti temným duchům a démonům. Umožněte mu prosím přístup do domu.",
"Dnes večer přednese v malém přednáškovém sále náš přední kacíř Demilukes přednášku na téma: ’Existují bohové? aneb buďte v klidu, nic vám nemohou!’",
"Navštivte rybí jídelnu na horním náměstí. Vybrané speciality z mořských...",
"Och, promiňte.",

"Jakže, rybí jídelnu?",
"To myslíte vážně?",
"Jak nechutné!",
"Ta socha si asi nevšimla, že se tohle město dávno potopilo.",
"Je to asi nějakej automat, pořád si myslí, že město žije.",
"Nebo si to zkrátka nechce připustit.",
"Už mě z té hlavy bolí hlava.",

"Nezkusíme tu hlavu nějak umlčet?",
"Proč? Dozvídáme se zde cenné poznatky z všedního života našich předků.",
"Kdybychom všechny ty kraby dostali nahoru...",
"...tak...?",
"Počkej, ještě to nemám domyšlené...",
"Představ si, že ta hlava tohle dělá celou věčnost...",
"No a?",
"Jak asi musí být těm chudákům krabům!",
"",
}
