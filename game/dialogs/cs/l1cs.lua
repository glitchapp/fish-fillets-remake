l1={"Co to bylo?",
"Nemám tušení.",
"Měli bychom se podívat ven.",
"Počkej na mě, jdu s tebou!",
"Proč se nic neděje?",
"Hej, hráči!",
"Můžeš nás ovládat kurzorovými šipkami a mezerníkem mezi námi přepínat.",
"A nebo myškou - někam ukaž levým tlačítkem a když tam můžeme dojet...",
"...aniž bychom něčím hýbali...",
"...tak tam jedeme.",
"A když někam klikneš pravým, tak tím směrem zkrátka zkusíme jet, překážky nepřekážky...",
"No a tvým cílem by mělo být dostat nás oba ven.",
"A dávej pozor, ať nám neublížíš. Když nám něco hodíš na hlavu nebo...",
"Už toho nech, to mu řekneme v příští místnosti.",
"Tak dobře.",
"Tudy neprojedu. Zavazí mi ten ocelový válec.",
"S tou ocelí nepohnu. Nechceš mi pomoct?",
"Tak ukaž...",
"Hmmm... Pohneš s tím. To já bych nedokázala!",
"Díky, už můžu jít s tebou.",
"Zatraceně, nemůžu tudy prolézt.",
"Nějak jsem asi přibral.",
"Nemůžeš posunout tu židli kousek vlevo?",
"Tak to byla chyba. Správné řešení musí být takové, abychom odjeli oba.",
"Já už se teď ven nedostanu. Musíš celou místnost hrát znovu.",
"Stiskni tlačítko zpětného výmazu.",
"On myslí backspace.",
"To je jedno.",
"Můžeš si to najít i na ovládacím panelu.",
"Pokud máš pocit, že jsi už řešení zkazil, nevadí.",
"Chceš-li to zkustit znovu, stiskni tlačítko zpětného výmazu.",
"Jsem těžký ocelový válec. Pohnout se mnou není nic jednoduchého. Taková malá rybka, jako je támhleta oranžová, vůbec nemá šanci. Rozmáčknul bych ji jedna dvě.",
}


l1icantgetthrough={"Tudy neprojedu. Zavazí mi ten ocelový válec.",
"S tou ocelí nepohnu. Nechceš mi pomoct?",
"Tak ukaž...",
}

l1wowyoumovedit={"Hmmm... Pohneš s tím. To já bych nedokázala!",
"Díky, už můžu jít s tebou.",				
}

l1damnit={"Zatraceně, nemůžu tudy prolézt.",
"Nějak jsem asi přibral.",
"Nemůžeš posunout tu židli kousek vlevo?",
}

