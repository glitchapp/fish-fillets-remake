
function Obey.lev54()

  
  loadcorrespondentfonts()

 
      Talkies.say( "small fish",
    {
  l54[1],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
         Talkies.say( "Big fish",
    { 
  l54[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    Talkies.say( "small fish",
    {
  l54[3],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })   
     
      Talkies.say( "Big fish",
    { 
  l54[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l54[5],
  l54[6],
  l54[7],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
          Talkies.say( "Big fish",
    { 
  l54[8],
  l54[9],
  l54[10],
  l54[11],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
        Talkies.say( "small fish",
    {
  l54[12],
  l54[13],
  l54[14],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })  
    
          Talkies.say( "Big fish",
    { 
  l54[15],
  l54[16],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l54[17],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
          Talkies.say( "Big fish",
    { 
  l54[18],
  l54[19],
  l54[20],
  l54[21],
  
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
        Talkies.say( "small fish",
    {
  l54[22],
  l54[23],
  l54[24],
  l54[25],
  
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })            
    
    
      Talkies.say( "Big fish",
    { 
  l54[26],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
     
end


function Obey.lev54engineon()
   
   
      Talkies.say( "small fish",
    {
  l54engineon[1],
  
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "big fish",
    {
  l54engineon[2],  
     },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "small fish",
    {
  l54engineon[3],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "big fish",
    {
  l54engineon[4],
  l54engineon[5],
     },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
  l54engineon[6],
  l54engineon[7],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    

end

function Obey.lev54engineoff()
   
    
      Talkies.say( "big fish",
    {
  l54engineoff[1],
  l54engineoff[2],
     },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( "small fish",
    {
  l54engineoff[3],
  l54engineoff[4],
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
   Talkies.say( "big fish",
    {
  l54engineoff[5],
     },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })

end


function Obey.lev54aproach()
   
   
      Talkies.say( "big fish",
    {
  l54aproachengine[1],
  l54aproachengine[2],
  
     },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
   Talkies.say( "small fish",
    {
  l54aproachengine[3],
 
     },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })


end
