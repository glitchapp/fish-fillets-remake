


function Obey.lev3()

  
  loadcorrespondentfonts()

 Talkies.say( "small fish",
    {
      l3[1],
      l3[2],
      l3[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
       l3[4],
       l3[5],
       l3[6],
       l3[7],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })

end

function Obey.lev3uarestanding()
  
   Talkies.say( "Big fish",
    { 
       l3youarestandinginmyway[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      l3youarestandinginmyway[2],
      l3youarestandinginmyway[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
        l3youarestandinginmyway[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
end

function Obey.lev3wewillgiveyouahint()

  Talkies.say( "small fish",
    {
      l3wewillgiveyouahint[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
        l3wewillgiveyouahint[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
     
     Talkies.say( "small fish",
    {
      l3wewillgiveyouahint[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

Talkies.say( "Big fish",
    { 
        l3wewillgiveyouahint[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
     
     Talkies.say( "small fish",
    {
      l3wewillgiveyouahint[5],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev3comeback()

  Talkies.say( "small fish",
    {
      l3comeback[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev3aproachaxe()


Talkies.say( "Big fish",
    { 
      l3thisistricky[1],
      l3thisistricky[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })

  Talkies.say( "small fish",
    {
      
      
      l3thisistricky[3],
     
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

Talkies.say( "Big fish",
    { 
       l3thisistricky[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })

   Talkies.say( "small fish",
    {
      
      
        l3thisistricky[5],
     
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

   Talkies.say( "Big fish",
    { 
       l3thisistricky[6],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })   
 
  
   Talkies.say( "small fish",
    {
      
       l3thisistricky[7],
     
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })    


end

function Obey.lev3savingpositions()


Talkies.say( "Big fish",
    { 
      l3ifyousolvesomespecific[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })

  Talkies.say( "small fish",
    {
      
      
      l3ifyousolvesomespecific[2],
     
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })

Talkies.say( "Big fish",
    { 
       l3ifyousolvesomespecific[3],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
