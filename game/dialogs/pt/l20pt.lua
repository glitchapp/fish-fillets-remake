l20={"Que civilização gloriosa. Tantos séculos debaixo d'água e seus elevadores ainda funcionam.",
"Especialmente quando nós os empurramos.",
"Mas apenas para baixo.",
"Você vê essa concha?",
"Essa concha não parece um pouco familiar?",
"É um antigo talismã para construtores de elevadores que sempre diziam 'Construa bem. Construa com a Shell'.",
"Deveriam ter vergonha. Isso é publicidade subliminar.",
"Aquele crânio parece familiar.",
"Todos os crânios humanos são iguais... Ao contrário dos esqueletos de peixe.",
"Este é um caminho difícil.",
"Sinto-me como um rato em uma esteira.",
"Uma esteira para peixes? Mas só temos barbatanas.",
"Hoo, hooooo, hoooo... Estou aquiíííí...",
"Hoooo, se quiser, vou te contar como resolver todos os níveis...",
"Hooo, ho, hooo, eu sei por que a cidade afundou!",
"Hoooo, por que ninguém está me dando atenção?",
"Hoooo ho hooo! Por que vocês estão me ignorando?",
"Hooooo, hooooo, peixes! Prestem atenção em mim!",
}

l20skull={"Vocês poderiam cuidar da própria vida? Na verdade, estamos aproveitando o sono.",
""
}
