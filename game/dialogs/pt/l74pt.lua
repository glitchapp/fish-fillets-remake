l74={"É um quarto assombrado.",
"Receio que será difícil mover as coisas sobre esse buraco.",
"Aqui está, como você deseja.",
"Tem alguém aí?",
"Quem está aí?",
"Acho que o receptor não está funcionando.",
"Coloque de volta imediatamente, a pena de índio é minha.",
"Coloque de volta imediatamente, é minha!",
"O O O O, eu sou índio. Coloque de volta imediatamente!",
"O O O O, coloque de volta imediatamente!",
"Para onde você está me empurrando? Eu sou o famoso Vinnetou!",
"Desculpe, precisamos disso necessariamente.",
"Como pode ser seu, se você não é índio?",
"Você não pode ser um índio porque não é vermelho.",
"Não posso discordar de nada.",
"Jogador, se você for um índio, não interprete isso como uma discriminação, por favor.",
"Pode ser um receptor mágico.",
"Tente.",
"Olá, gostaria de fazer o aço próximo à saída desaparecer?",
"Coloque de volta, isso deve ser resolvido pelo jogador, não por nós mesmos.",
}
