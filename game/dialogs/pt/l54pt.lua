l54={"Eu acho que podemos obter algumas informações sobre a propulsão interplanetária aqui.",
"Isto parece mais uma propulsão convencional para a nave de pouso.",
"Então. Encontramos a propulsão. Alcançamos um dos objetivos da nossa missão.",
"Não seja tão apressado. Ainda não exploramos todo o destroço.",
"Sinto muito que nenhuma dessas maravilhas técnicas ao nosso redor funcione.",
"Gostaria de ver alguns desses gadgets extraterrestres em ação.",
"Eu me pergunto se esse motor poderia funcionar debaixo d'água.",
"Talvez seja melhor para nós se ele não funcionar.",
"Estou bastante contente que não conseguimos ligar nada aqui.",
"Devemos ficar felizes por ainda não termos ligado nada.",
}
l54engineon={"O que você fez? Desligue esse barulho!",
"O que você ativou? Para onde está nos levando?",
"Isso é terrível! Desligue antes que seja tarde demais!",
"Não consigo! Não consigo tirar!",
"Não sei como! Não consigo tirar!",
"Mayday! Mayday!",
"Como posso desligar isso?!",
"Não consigo desligar!"
}

l54engineonfish1={"O que você está fazendo? Para onde estamos indo?",
"O que você ativou? Para onde está nos levando?",
"Não consigo! Não consigo tirar!",
"Não sei como! Não consigo tirar!",
"Mayday! Mayday!",
"Como posso desligar isso?!",

}
--"Não consigo desligar!"
l54engineoff={"Finalmente.",
"Que alívio.",
"Obrigado.",
"Finalmente.",
"Só tenho medo de ter que ligá-lo novamente.",
""
}

l54aproachengine={"O que você está fazendo? Para onde você está indo?",
"Cuidado com a chave inglesa.",
"Aqui não posso causar nenhum dano."
}
