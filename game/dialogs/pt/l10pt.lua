l10={"Olhe. O barco de festa.",
"Eu tenho a sensação de que isso não vai ser um piquenique normal.",
"Você e suas sensações. Com certeza está cheio de pessoas gentis e amigáveis.",
"Talvez você estivesse certo. Este é um piquenique estranho.",
"Você acha que eles poderiam sair desse barco?",
"Espero que eles não consigam sair desse barco.",
"E se eles começarem a nos perseguir?",
"Não acho que eles possam nos pegar. Esqueletos não nadam muito bem.",
"Pare de pensar nisso e mexa suas nadadeiras para que possamos sair daqui.",
"Você quer segurar esse tubo de aço com um copo?",
"Não posso fazer mais nada por você.",
"Tome cuidado para não derramar!",
}
