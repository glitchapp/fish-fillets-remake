l14={"Que tipo de navio estranho é esse?",
"Este é o destroço do avião civil LC-10 Lemura.",
"Este é o destroço do avião civil Atlantobus.",
"Este é o destroço do avião civil Poseidon 737.",
"Você vê esse olho? A testemunha silenciosa da tragédia... Alguém confiou nesse avião - e o olho de vidro é tudo o que ele deixou para trás.",
"Isso não é um olho de vidro, mas um giroscópio. Pelo menos neste nível.",
"Assentos. Por que há tantos assentos aqui?",
"Agradeça. Você conseguiria sair sem eles?"
}
