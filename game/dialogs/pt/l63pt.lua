l63={"Aquele morcego deve ser terrivelmente forte.",
 "Bem, sim.",
 "Aquela criatura vermelha é um pouco estranha.",
 "Isso pode ser verdade, mas acho que vamos precisar dela.",
 "É muito estreito para mim. Você terá que fazer isso sozinho aí embaixo.",
 "Aquele peixe branco é um terrível obstáculo.",
 "Peixe? Eu pensei que fosse apenas uma pedra.",
 "Acho que devemos procurar pelo Graal em outro lugar...",
 "Não seja deprimente. Vamos resolver isso rapidinho.",
 "Não tenho tanta certeza.",
 "Viu? Você está aqui.",
 "Que tipo de monstro que muda de cor é esse?",
 "Isso é uma abominação.",
 "Isso é uma ofensa à Natureza.",
 "Ele é chamado de Cabeça de martelo chamativa.",
 "Ele é chamado de Repulsor brilhante.",
 "Ele é chamado de Peixe político.",
 "Esse morcego é meio estranho.",
 "Esse morcego é muito fleumático.",
 "É um morcego empalhado.",
 "É uma escultura de morcego.",
 "É uma estalagmite em forma de morcego.",
 "É apenas uma estalagmite comum.",
 "Acho que você vai precisar daquele 'monstro'.",
 "Acho que você terá que superar seu desgosto por aquela 'abominação'.",
}
