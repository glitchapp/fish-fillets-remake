l3={"Você já conhece as regras.",
"Agora você pode praticá-las.",
"Vamos dar algumas dicas de vez em quando.",
"Eu não posso nadar para baixo agora.",
"E também não posso subir.",
"Ei, organize lá em cima de alguma forma.",
"Mas tenha cuidado!",
""
}

l3icantmove={"Eu não consigo mover isso. Isso é aço."}

l3comeback={"Volte, você não pode ir mais longe!"}

l3thisistricky={"Hmm... Isso é complicado.",
"Se empurrarmos o mel para o lado, seremos esmagados por aquela lata de geleia de damasco.",
"Não poderíamos empurrá-la para o lado com aquela machadinha?",
"Essa é uma boa ideia.",
"Agora não consigo pegar a machadinha. A lata está no caminho.",
"Você não deve empurrar a machadinha!",
"Se você conseguir empurrar aquela lata para mim, eu vou empurrar a machadinha nela e tudo ficará bem."}

l3thislooksrealbad={"Isso parece realmente ruim. Você terá que reiniciar o nível."}


l3ifyousolvesomespecific={"Se você resolver uma parte específica, você pode salvar a posição.",
"Mas apenas se tiver certeza da sua solução.",
"Lembre-se do que dissemos sobre salvar posições."}

l3youarestandinginmyway={"Você está bloqueando o meu caminho. Tente se esconder em outro lugar.",
"Se você soltar isso e eu ficar aqui, nunca conseguirei sair.",
"Aqui? Estou um pouco com medo. Vou conseguir sair?",
"Espere e veja."}

l3ishoulndtbe={"Eu não deveria estar pensando nisso."}

l3wewillgiveyouahint={"Vamos te dar uma dica aqui, jogador. Você precisa colocar aquele livro ali, à esquerda.",
"Para que ele pegue aquele cilindro de aço quando eu empurrar.",
"Mas não vamos te dizer como fazer isso.",
"E se eu seguir pelo caminho de cima?",
"Shhhh. Isso são dicas demais."}

l3justremember={"Apenas lembre-se de que, embora não possamos empurrar objetos um ao longo do outro, podemos empurrá-los para baixo ou sobre alguma estrutura sólida."}

l3thingslikethat={"Coisas assim acontecem às vezes."}

l3sometimes={"Às vezes você precisa pensar muuuito adiante.",
"E frequentemente você precisa reiniciar o nível então."}

l3weadmitwedid={"Admitimos que fizemos isso de propósito.",
"Para que você possa tentar novamente - desta vez sem nenhuma dica."}
