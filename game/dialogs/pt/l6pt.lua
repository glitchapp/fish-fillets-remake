l6={"Já faz muito tempo desde que estivemos aqui...",
"Ninguém limpou aqui há muito tempo.",
"Eu sabia que teríamos que limpar este quarto.",
"Pelo menos podemos arrumar as coisas.",
"Pelo menos as coisas finalmente estarão em ordem aqui.",
"Vou ter que varrer.",
"É bom que tenhamos essa vassoura.",
"Você poderia me passar aquela vassoura, por favor?",
"Você poderia me passar aquela vassoura, por favor? E tente não deixá-la cair na minha cabeça.",
"Acho que vai dar certo se você simplesmente varrer tudo para lá.",
"Deve caber tudo.",
"Só varra para baixo e sairemos daqui.",
"Esqueça aquela lixeira e apenas varra o lixo para baixo.",
}
