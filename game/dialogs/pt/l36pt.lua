l36={"É adorável aqui.",
"Tantas pérolas.",
"Há tantas pérolas aqui.",
"Escute... Não se esqueça da nossa missão!",
"Não somos caçadores de pérolas.",
"Lembre-se! Temos uma missão a cumprir.",
"Você está apenas sentado aí em cima e eu tenho que fazer todo o trabalho sozinho!",
"Você não disse que gosta das pérolas?",
"Não está feliz com tantas pérolas?",
"Eu preciso de mais uma pérola.",
"Não.",
"Uma pérola a mais, por favor.",
"Meu querido amigo, você não poderia me emprestar apenas uma pequena pérola?",
"Eu me pergunto se não seria mais fácil encurtar esse tubo de aço.",
"Dizem que Cleópatra dissolvia pérolas em vinagre.",
"Você acha que temos muitas delas?",
"Bem, eu só queria te educar um pouco.",
"Observando você, eu tenho que pensar em como eu brincava de bolas de gude com girinos.",
"Você não tem uma bola de gude sobressalente no bolso, tem?",
}
