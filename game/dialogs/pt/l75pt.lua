l75={"Olha, alguém construiu uma fechadura na pedra.",
"Onde você vê uma fechadura? Eu só vejo que não consigo sair pela porta.",
"Tem uma fechadura aqui em algum lugar. A chave está lá embaixo.",
"De qualquer forma, eu não consigo sair. Tudo está bloqueado ali.",
"Por isso precisamos da chave.",
"Você poderia me pegar a chave, eu gostaria de sair.",
"Eu pegaria se pudesse, mas acho que está presa.",
"Olha, uma vassoura.",
"Eu não quero limpar aqui. Já foi ruim o suficiente na sala da caldeira.",
"Por que está reclamando? Eu tirei todas as coisas pesadas de lá.",
"Sem mim, você ainda estaria preso lá.",
"Nós poderíamos simplesmente nadar para sair. Você vê o quão grandes são os buracos na rede?",
"Sim, mas são muito pequenos para mim.",
"Acho que realmente devemos usar a chave para abrir a fechadura.",
"Então vá pegá-la, espertinho.",
"Seja legal comigo, ou posso sair nadando sozinho.",
"OK, não vou mais dizer essas coisas para você.",
"Não pense nem nelas.",
"Hmm... OK.",
"Você acha que a chave vai se soltar se a movermos um pouco?",
"Eu não acho, mas podemos tentar.",
"Nunca conseguiremos tirar a chave. Ela está presa há tanto tempo que cresceu com corais.",
"Não desista.",
"Será que o pingente de gelo ali pode ajudar?",
"De qualquer forma, não podemos tirá-lo.",
"Só precisamos aplicar força suficiente.",
"Talvez se você usar o martelo?",
"Ai, esse cacto tem espinhos.",
"Claro, é um cacto.",
"O que ele está fazendo aqui?",
"Ai, esse cacto tem espinhos.",
"Claro, é um cacto.",
"O que ele está fazendo aqui?",
"Conseguimos. Agora segure a chave até eu sair.",
"Sem problema.",
"Assim que eu aprender a carregar aço.",
"É melhor você aprender rápido.",
"Por favor, segure a chave desta vez.",
"Vou tentar.",
"Não! A porta fechou de novo.",
"Então abra de novo.",
"Grmbl...",
"Mantenha a calma. Vamos conseguir em breve.",
}
