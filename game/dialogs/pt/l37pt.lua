l37={"Você consegue ver? Isso deve ser aquela tartaruga telepática.",
"Sim, é isso. A tartaruga telepática.",
"Bem, não sei. Pelas fotos, poderia ser isso.",
"Não tenho certeza... e quanto aos poderes telepáticos?",
"De qualquer forma, temos que tirá-la da sala.",
"Tiremos essa tartaruga.",
"O que você está fazendo?",
"O que você fez?",
"O que isso deve significar?",
"Para onde você está nadando?",
"O que isso deveria significar?",
"O que aconteceu com você?",
"Eu não sei...",
"Não consigo entender...",
"Não sei o que está acontecendo comigo...",
"Eu não queria...",
"O que está acontecendo conosco?",
"O que pode significar?",
"Isso deve ser aquela tartaruga!",
"Isso deve ser aquela tartaruga!",
"Então não pode haver dúvidas agora - é isso. A que estamos procurando!",
"Sem dúvida, essa é a tartaruga que estamos procurando!",
"Essa fera!",
"Pare com isso, seu diabo!",
"Chega!",
"Você está se divertindo?",
"Tantas formas bizarras só podem ser encontradas em um recife de coral.",
"Esses corais são maravilhas da Mãe Natureza.",
"De onde vêm tantas formas bizarras?",
"É fácil - os designers criam problemas complicados e deixam o desenho para os artistas gráficos.",
"Nosso objetivo nesta sala é tirar essa tartaruga.",
}

l37end={"Conseguimos capturar uma perigosa tartaruga de coral completamente inofensiva.",
"Recomendamos medidas máximas de segurança desnecessárias, ela é realmente gentil.",
"Ela não só consegue ler pensamentos muito gentis, mas também influenciá-los - isso é besteira.",
"E ela também nos mordeu, e daí?",
"Por todos os meios, tire a mordaça dela, apenas se ela quiser - não tire a mordaça dela.",
}
