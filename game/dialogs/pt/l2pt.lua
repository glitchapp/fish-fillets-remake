l2={"Ah não, de novo...",
"Você sabe que não há nada que possamos fazer a respeito disso.",
"Vamos empurrá-lo para baixo para darmos uma olhada nele.",
"Bem, vamos começar o trabalho.",
}

l2thediskself={"Como? O disco se autodestruiu.",
"Então, agora podemos finalmente começar.",
"Nossa primeira tarefa é sair desta sala.",
"Isso, aliás, será nossa tarefa em todas as outras salas também.",
"Vamos levar o jogador para a oficina e mostrar a ele as regras de segurança do trabalho.",
"Boa ideia.",
"Por enquanto, não toque em nada, apenas observe e aprenda. Vamos mostrar o que você deve e não deve fazer conosco, além do que somos capazes.",
"Antes de entrar na oficina, vamos salvar o jogo - basta pressionar a tecla F2.",
"Primeiro vamos mostrar o que pode nos machucar.",
"Eu me voluntario para ser o manequim.",
"Antes de tudo, não devemos derrubar coisas uns nos outros.",
"Também não podemos nadar para baixo quando estamos carregando um objeto, porque ele cairia em cima de nós.",
"Outra coisa que não devemos fazer é empurrar objetos em nossas costas.",
"Alguns objetos têm formato que poderíamos segurar e empurrar ao mesmo tempo - mas também não nos é permitido fazer isso.",
"Também não podemos soltar objetos adicionais em uma pilha que um de nós está carregando.",
"E, ao contrário do meu parceiro maior, eu não consigo mover ou até mesmo carregar objetos de aço.",
"Podemos sempre pegar objetos e depois deixá-los cair.",
"Podemos empurrar um objeto ao longo das costas um do outro apenas se o objeto for então empurrado para outro suporte...",
"...e até deixá-lo cair um sobre o outro nesse caso.",
"Eu posso até fazer isso sozinho... posso nadar por baixo dele... ou colocá-lo nesta saliência.",
"Também podemos empurrar um objeto ao longo do topo de outro objeto que estamos apoiando, mas não podemos mover o objeto que está diretamente em nossas costas.",
"Também podemos nadar livremente sob um objeto que estamos apoiando e passá-lo para o outro peixe.",
"E podemos empurrar objetos um do outro, contanto que o objeto caia imediatamente.",

"Em resumo, só podemos levantar objetos, deixá-los cair, empurrá-los ao longo de uma superfície sólida, empurrá-los para uma superfície e empurrá-los um do outro.",
"Isso é basicamente tudo para as regras. Se você quiser saber mais, pressione F1 e leia a seção de ajuda.",
}

l2wecouldrestart={"Nós poderíamos reiniciar o nível.",
"Isso é verdade...",
}

l2icantliftit={"Eu não consigo levantá-lo. Você pode tentar?",
"Um pouco mais alto para que eu possa nadar por baixo de você.",
}

l2lethisbytheway={
"Devemos assistir isso novamente?",
}

l2nowell={"Agora vamos começar novamente - ou podemos carregar o jogo salvo pressionando a tecla F3.",
"E agora estamos de volta onde salvamos o jogo pela última vez.",
}

l2again={"Novamente, carregamos um jogo salvo pressionando a tecla F3.",
}

l2wehaveloaded={"Carregamos nosso jogo salvo pela última vez. Agora vamos mostrar a você todas as coisas que somos capazes de fazer.",
}



l2briefcasetalkie={"Bom dia, peixe.",
"Este é um assunto de extrema importância e, por isso, escolhemos você,",
"nossos agentes subaquáticos mais capazes.",
"Agentes da FDTO - Organização de Treinamento de Detetives de Peixes -",

    "conseguiram obter várias fotos amadoras de um objeto extraterrestre",
    "que caiu em algum lugar nas suas proximidades.",
    "Sua missão, caso decida aceitá-la,",
    "será recuperar o OVNI e adquirir todas as informações possíveis sobre os princípios e a natureza da propulsão interestelar.",
    "Você também deve tentar resolver alguns dos nossos casos ainda não resolvidos.",
    "Estamos especialmente interessados nas circunstâncias que cercam o afundamento da cidade mítica, por acaso na mesma área.",
    "Esse caso pode estar relacionado ao mistério do Triângulo das Bermudas.",
    "Descubra por que tantos navios e aviões desapareceram nessa área ao longo das últimas décadas.",
    "Sabe-se que um dos navios perdidos pertencia ao lendário Capitão Silver.",
    "Ainda existem muitas incertezas em torno desse famoso pirata.",
    "Acima de tudo, estamos interessados no mapa que mostra a localização do tesouro enterrado por ele.",
    "Uma das suas tarefas mais importantes é encontrar o computador escondido nas profundezas por uma certa organização criminosa.",
    "Ele contém dados sobre um projeto que pode mudar o mundo inteiro.",
    "Você também deve encontrar e deter a misteriosa tartaruga de coral.",
    "Ela escapou das instalações de interrogatório da FDTO.",
    "Ela não está armada, mas supostamente possui habilidades telepáticas.",
    "Fomos informados de que uma certa usina nuclear está despejando ilegalmente seus resíduos radioativos.",
    "Verifique isso. E não se esqueça de encontrar o Santo Graal.",
    "E como de costume: Se alguém da sua equipe ficar ferido ou morrer,",
    "A Altar negará qualquer conhecimento de sua existência e o nível será reiniciado.",
    "Este disco se autodestruirá em cinco segundos.",
    }



