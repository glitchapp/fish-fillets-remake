l30={"Nunca vi corais violetas assim.",
"Não gosto de corais violetas.",
"Eu amo corais violetas...",
"Para mim, eles parecem bonitos.",
"Espero que haja corais ainda mais interessantes nos próximos níveis.",
"Deus me livre.",
"Teremos que levantar esse aço...",
"Oi, caranguejo!",
"Não vamos tentar animar um pouco esse caranguejo?",
"Ignore-o.",
"Deixe esse caranguejo em paz.",
"Você não vê como ele está nervoso?",
"Tente resolver isso sem o caranguejo nervoso.",
"Esse caranguejo pode ser útil.",
"Esse caranguejo certamente nos ajudará a resolver isso.",
}

l30crab={"Quem me acordou?",
"Me deixem em paz, estou dormindo aqui!",
"O que vocês querem de mim?!?",
"Não me toquem!",
}

l30crabups={
"Ooops.",
}
