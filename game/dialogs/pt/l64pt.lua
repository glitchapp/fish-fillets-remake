l64={"Aquilo ali deve ser o Santo Graal!",
 "Como você pode ter tanta certeza?",
 "Você não vê aquele halo?",
 "Você não vê como ele brilha entre os outros?",
 "Você não vê como ele está colocado de forma tão conspicua no centro da caverna?",
 "O halo não significa nada. O Santo Graal pode ter uma aparência bem comum.",
 "Nem tudo que reluz é ouro. Ninguém nunca disse que o Santo Graal precisa brilhar.",
 "Hmm... Colocar um Graal comum em um lugar tão evidente não o transformará em um santo.",
 "Sinto que as coisas não serão como parecem.",
 "Você e seus sentimentos.",
 "Vamos embora, vamos levar aquele Graal central que brilha com a gente.",
 "Eu prefiro levar todos eles.",
 "Hmm... Parece que você estava certo. Vamos levar todos eles.",
 "Nosso objetivo neste nível é empurrar para fora o Santo Graal.",
 "Não, empurrar todos os Graais.",
 "O Santo já é o suficiente.",
 "Nosso objetivo é empurrar todos os Graais para fora desta sala.",
 "Quase todos eles se foram agora!",
 "Mais um Graal e teremos terminado!",
}

l64end={"Recomendamos submeter o material anexo a testes rigorosos de santidade.",
 "Devido à falta de equipamentos, realizamos apenas testes básicos e os resultados são mais do que promissores.",
 "Ao bombardear alguns dos graais com palavras ultragrossas, medimos até mesmo 3 Papais Noéis,",
 "alguns deles resistiram a uma pressão de circunstâncias de 0,8 trabalhos por um minuto!",
 "Também é possível que tenhamos encontrado toda a Santa Missa.",
}
