l59={"Você consegue ver esse cavalo-marinho?",
"Ele está bloqueado pelas ânforas.",
"Ele se embriagou entre as âforas.",
"Eu me pergunto se tem algo sobrando nelas.",
"Provavelmente você terá que verificar por si mesmo.",
"Finalmente, consigo ver um novo tipo de crânio.",
"Você notou esse totem? É o deus mexicano Shelloxuatl.",
"Parece ser isso.",
"Essas ânforas caem terrivelmente devagar.",
"Bem, você não é um urso, afinal.",
"Não se esqueça de que estamos debaixo d'água.",
"Eca. Os autores poderiam ter poupado essa animação para nós.",
"Esse totem parece bom para mim.",
"Aquele crânio parece irradiar algo estranho.",
"Está vivo ou é algum tipo de feitiço?",
"",
"",
"",
}

l59skull_canyouall={"Vocês podem cuidar da sua própria vida? Nós realmente gostamos de dormir.",""}

l59skull_wellanother={"Bem, mais uma noite de sono agitado por causa de pessoas curiosas.",""}

l59skull_welliwas={"Bem, eu estava dormindo até ser tão rude despertado.",""}

l59skull_anothernight={"Mais uma noite de sono tranquilo, arruinada.",""}
