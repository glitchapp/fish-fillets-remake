l23={"Acho que finalmente estamos no rastro dessa cidade misteriosa.",
"Parece magnífico.",
"Agora só precisamos descobrir por que ela afundou e estamos prontos.",
"Mas pode levar um pouco de tempo.",
"Me deixe em paz. Eu adoro a beleza clássica desse templo.",
"Quem está sentado naquela cadeira?",
"É uma cópia do Zeus de Feídias. Uma das sete maravilhas do mundo.",
"Hmm. Era outro tempo.",
}

l23zeus={"Tenha cuidado! Tente não danificá-lo!",
"Que desperdício!",
"Agora abaixe-o com cuidado.",
"Bárbaro! Você não consegue ser um pouco mais cauteloso?!",
"De qualquer forma, eu não gostava dessa escultura.",
}
