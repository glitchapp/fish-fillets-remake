l1={"O que foi isso?",
"Não faço ideia.",
"Devíamos dar uma olhada lá fora.",
"Espera! Eu vou com você.",
"Por que nada está acontecendo?",
"Oi, jogador!",
"Você pode nos controlar com as setas do teclado e usar a barra de espaço para trocar de peixe.",
"Ou você pode nos controlar com o mouse - clique esquerdo em algum lugar e...",
"...se pudermos chegar lá sem mover nada...",
"...vamos seguir seus cliques.",
"E quando você clicar com o botão direito em algum lugar, vamos tentar chegar lá, empurrando todos os obstáculos em nosso caminho.",
"Seu objetivo deve ser nos levar para fora.",
"E tenha cuidado para não nos machucar. Se você deixar algo cair em cima de nós ou...",
"Ah, pare com isso. Vamos contar para ele no próximo nível.",
"Ok.",

"Uau, você moveu isso! Eu nunca conseguiria fazer isso!",
"Obrigado, agora posso ir com você.",
"Ah droga, não consigo passar.",
"Deve ser porque ganhei peso.",
"Você poderia empurrar a cadeira um pouco para a esquerda?",
"Ops! Isso foi um erro. Ambos precisam conseguir sair para continuar.",
"Não tem como eu sair agora. Você terá que reiniciar o nível.",
"Você precisa pressionar a seta longa no seu teclado.",
"Os humanos chamam isso de tecla de retrocesso.",
"Enfim, era mais engraçado em tcheco.",
"A opção de reiniciar também está no painel de controle.",
"Se você preferir reiniciar o nível, sem problemas.",
"Basta pressionar a seta longa no seu teclado.",
"Sou um cilindro de aço pesado. Não é fácil me mover. Um peixinho tão pequeno, como o laranja ali, simplesmente não tem chance. Eu poderia esmagá-lo sem problema algum.",
}

l1icantgetthrough={"Não consigo passar por aqui. Esse cilindro de aço está no caminho.",
"Não consigo mover esse cilindro. Você pode me ajudar?",
"Sem problemas...",
}

l1wowyoumovedit={
"Uau, você moveu isso! Eu nunca conseguiria fazer isso!",
"Obrigado, agora posso ir com você.",
}

l1damnit={"Ah, droga, não consigo passar.",
"Devo ter ganhado peso.",
"Você poderia empurrar a cadeira um pouco para a esquerda?",
}
