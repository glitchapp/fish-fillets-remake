l7={"Meu Deus, o impacto causou um desmoronamento!",
"Não consigo passar por esse desmoronamento,",
"temos que encontrar outra saída.",
"Este não é o caminho.",
"Não vou conseguir sozinho.",
"Provavelmente deveria ter levantado aquela concha primeiro.",
"Talvez tenhamos que trocar esses objetos.",
"Um colchão. A melhor coisa que se pode ter debaixo d'água.",
}

l7part2={"Agora podemos soltar o caracol no colchão.",
"Será difícil pegar aquele caracol de lá.",
"Finalmente, está lá.",
"O pobre caracol...",
"Devemos procurar nos recifes de coral.",
"Haverá muitos seres interessantes para investigar lá.",
"Não precisamos de um microscópio para investigar os corais?",
"Sim, eles são pequenos. Mas pode haver outras formas de vida.",
"Tartarugas de coral, por exemplo.",
"E além disso, suspeito que haja um microscópio em um banatista.",--]]
}
