l75={"Se, nogen har bygget en lås ind i stenen.",
"Hvor ser du en lås? Jeg kan kun se, at jeg ikke kan gå ud gennem døren.",
"Der er en lås her et eller andet sted. Nøglen er der nede.",
"Jeg kan alligevel ikke gå ud. Alt er blokeret derovre.",
"Derfor har vi brug for nøglen.",
"Kan du ikke hente nøglen til mig? Jeg vil gerne gå.",
"Det ville jeg gerne, hvis jeg kunne, men jeg tror, den sidder fast.",
"Se, en kost.",
"Jeg vil ikke gøre rent her. Det var slemt nok i kedelrummet.",
"Hvorfor beklager du dig? Jeg ryddede alt det tunge derinde.",
"Uden mig ville du stadig sidde fast derinde.",
"Vi kunne bare svømme ud. Kan du se, hvor store hullerne i nettet er?",
"Ja, for små til mig.",
"Jeg tror, vi virkelig skal bruge nøglen til at åbne låsen.",
"Så gå og hent den, klogskab.",
"Vær rar mod mig, ellers svømmer jeg ud alene.",
"Okay, jeg vil ikke sige sådanne ting til dig mere.",
"Tænk dem ikke engang.",
"Hmm... Okay.",
"Tror du, nøglen vil løsne sig, hvis vi bevæger den lidt rundt?",
"Jeg tror det ikke, men vi kan prøve.",
"Vi får aldrig nøglen ud. Den har siddet fast så længe, at den er vokset fast med koraller.",
"Giv ikke op.",
"Kunne isen derovre hjælpe?",
"Vi kan alligevel ikke få den ud.",
"Vi skal bare bruge tilstrækkelig kraft.",
"Måske hvis du bruger hammeren?",
"Au, den kaktus har pigge.",
"Selvfølgelig, det er en kaktus.",
"Hvad laver den her?",
"Au, den kaktus har pigge.",
"Selvfølgelig, det er en kaktus.",
"Hvad laver den her?",
"Vi har den. Hold nøglen, indtil jeg er gået ud.",
"Ingen problem.",
"Så snart jeg har lært at bære stål.",
"Du skal lære det hurtigt.",
"Vær sød at holde nøglen denne gang.",
"Jeg vil prøve.",
"Nej! Døren lukkede sig igen.",
"Så åbn den igen.",
"Grmbl...",
"Bliv rolig. Vi får den snart åbnet.",
}
