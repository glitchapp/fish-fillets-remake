l56robodog={"Se! Det må være... ROBODOG!",
"Hvad? DEN robodog?!?",
"Vær forsigtig. Den bider!",
"Jeg håber, den ikke sparker mig...",
"Nå, nu giver det hele mening.",
"Inklusive Cubakrisen og Nostradamus' profetier.",
"Inklusive mordet på John Lennon og Tunguzian-meteoren.",
"Inklusive Challenger-eksplosionen og slaget ved Bull Run.",
"Inklusive Tjernobyl-ulykken og forliset af Noahs ark.",
"Nu kan jeg se logikken bag dette spil.",
"Og også meningen med livet, universet og alt.",
"Lyt ikke til dem, spiller. De driller dig.",
"Lyt ikke til dem, spiller. De forsøger at være sjove.",
"Lyt ikke til dem, spiller. De forsøger at narre dig.",
"Jeg er blot et ydmygt cybernetisk legetøj.",
"Selvom jeg oprindeligt skulle være en scooter.",
"Selvom jeg oprindeligt skulle være en græsslåmaskine.",
"Selvom jeg oprindeligt skulle være en rumstationskreds.",
"Selvom jeg oprindeligt skulle være en stykke kage.",
"Og jeg besøgte Cuba kun én gang.",
"Og alle ved, at en anden myrdede ham, de kan ikke bevise noget.",
"Og hvem bekymrer sig om et par soldater.",
"Og de fortalte mig, at reguleringstangen ikke var vigtig.",
}

l56switchoff={
"Tænd lyset, NU!",
"Hvad tror du, du laver?",
"Vent, jeg vil prøve at finde kontakten.",
"Sluk det og tænd lyset.",
"Det kan jeg ikke. Den dumme kontakt faldt ned.",
"Tænd selv lyset, hvis du er så klog.",
"Hvor er du henne?",
"Hej-o-o.",
"Sig noget.",
"Her.",
"Her er jeg.",
"Jeg er lige her.",
"Hvor er du, jeg er bange.",
"Jeg er bange for mørket.",
"Vær sød ikke at gå. Det er så mørkt herinde.",
"Vær ikke bange. Jeg er her.",
"Stop med at jamre. Vi klarer det.",
"Der er intet at være bange for.",
}

l56switchon={"Endelig, lyset er tilbage...",
"Jeg troede aldrig, jeg ville blive så glad for at se dig igen.",
"Det blinkende lys begynder at gøre ondt i mine øjne.",
}
