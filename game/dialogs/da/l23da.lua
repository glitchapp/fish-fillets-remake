l23 = {"Jeg tror, vi er endelig på sporet af den mystiske by.",
"Den ser fantastisk ud.",
"Nu skal vi bare finde ud af, hvorfor den sank, og så er vi færdige.",
"Men det kan tage lidt tid.",
"Lad mig være i fred. Jeg beundrer den klassiske skønhed i dette tempel.",
"Hvem sidder der på den stol?",
"Det er en kopi af Feidios' Zeus. Et af verdens syv vidundere.",
"Hmm. Det var en anden tid.",
}

l23zeus = {"Vær forsigtig! Prøv ikke at beskadige den!",
"Sådan et spild!",
"Og nu sænk den forsigtigt ned.",
"Du barbar! Kan du ikke være lidt forsigtig?!",
"Jeg kunne alligevel ikke lide denne skulptur.",
}
