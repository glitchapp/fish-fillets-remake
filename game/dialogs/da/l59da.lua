l59={"Kan du se den søhest der?",
"Den er blokeret af amforaer.",
"Den er blevet fuld blandt amforaerne.",
"Jeg spekulerer på, om der er noget tilbage i dem.",
"Du skal nok selv gå og tjekke.",
"Endelig kan jeg se en ny slags kranium.",
"Lagde du mærke til den totem? Det er den mexicanske gud Shelloxuatl.",
"Det ser sådan ud.",
"Disse amforaer falder ubærligt langsomt.",
"Nå, du er trods alt ikke en bjørn.",
"Glem ikke, at vi er under vandet.",
"Øv. Forfatterne kunne have sparet os for den animation.",
"Den totem ser godt ud for mig.",
"Det kranium ser ud til at udstråle noget mærkeligt.",
"Er det levende eller er det nogen form for trylleformular?",
"",
"",
"",
}

l59skull_canyouall={"Kan I alle sammen passe jeres eget? Vi nyder faktisk at sove.",""}

l59skull_wellanother={"Nå endnu en nat med urolig søvn på grund af nysgerrige mennesker",""}

l59skull_welliwas={"Nå, jeg sov, indtil jeg blev så uhøfligt vækket.",""}

l59skull_anothernight={"Endnu en nat med fredelig søvn, ødelagt.",""}
