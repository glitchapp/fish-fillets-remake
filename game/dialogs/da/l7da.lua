l7 = {
    "Kors i hytten, sammenstødet skabte et sammenbrud!",
    "Jeg kan ikke komme igennem dette sammenbrud,",
    "vi er nødt til at finde en anden udgang.",
    "Dette er ikke vejen.",
    "Jeg klarer det ikke alene.",
    "Jeg burde nok have løftet den skal først.",
    "Måske er vi nødt til at bytte disse genstande.",
    "En madras. Det bedste, du kan få under vand.",
}

l7part2 = {
    "Nu kan vi lægge sneglen på madrassen.",
    "Det vil være svært at samle sneglen op derfra.",
    "Endelig er den der.",
    "Den stakkels snegl...",
    "Vi burde søge i koralrevene.",
    "Der vil være mange interessante væsner at undersøge der.",
    "Skal vi ikke bruge et mikroskop til at undersøge koraller?",
    "Ja, de er små. Men der kan være andre former for liv.",
    "Koralskildpadder, for eksempel.",
    "Og desuden har jeg en mistanke om, at der er et mikroskop i en batiscaf.",
}
