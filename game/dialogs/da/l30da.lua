l30 = {"Jeg har aldrig set sådanne lilla koraller.",
"Jeg kan ikke lide lilla koraller.",
"Jeg elsker lilla koraller...",
"De ser pæne ud for mig.",
"Jeg håber, at der vil være endnu mere interessante koraller i de kommende niveauer.",
"Gud forbyde det.",
"Vi bliver nødt til at løfte dette stål...",
"Hej, krabbe!",
"Skal vi ikke prøve at opmuntre den krabbe lidt?",
"Ignorer den.",
"Lad den krabbe være i fred.",
"Kan du ikke se, hvor nervøs den er?",
"Prøv at klare dig uden den nervøse krabbe.",
"Den krabbe kunne være nyttig.",
"Denne krabbe vil helt sikkert hjælpe os med at løse det.",
}

l30crab = {"Hvem vækkede mig?",
"Lad mig være i fred, jeg sover her!",
"Hvad vil du have fra mig?!?",
"Rør mig ikke!",
}

l30crabups = {
"Ups.",
}
