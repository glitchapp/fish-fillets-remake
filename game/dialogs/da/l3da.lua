l3={"Du kender allerede reglerne.",
"Nu kan du øve dem.",
"Vi vil give dig nogle hints lejlighedsvis.",
"Jeg må ikke svømme ned nu.",
"Og jeg kan heller ikke gå op.",
"Hej, arranger det deroppe på en eller anden måde.",
"Men vær forsigtig!",
""
}

l3icantmove={"Jeg kan ikke flytte dette. Det er lavet af stål."}

l3comeback={"Kom tilbage, du kan ikke gå længere!"}

l3thisistricky={"Hmm... Dette er svært.",
"Hvis vi skubber honningen til side, vil vi blive mast af den dåse med abrikosmarmelade.",
"Kunne vi skubbe den til side med den økse?",
"Det er en god idé.",
"Nu kan jeg ikke tage øksen. Dåsen er i vejen.",
"Du må ikke skubbe øksen!",
"Hvis du kan skubbe den dåse hen til mig, vil jeg skubbe øksen på den, og alt bliver okay."}

l3thislooksrealbad={"Dette ser virkelig dårligt ud. Du bliver nødt til at genstarte niveauet."}


l3ifyousolvesomespecific={"Hvis du løser en bestemt del, kan du gemme positionen.",
"Men kun hvis du er sikker på din løsning.",
"Husk, hvad vi har sagt om at gemme positioner."}

l3youarestandinginmyway={"Du står i vejen der. Prøv at gemme dig et andet sted.",
"Hvis du taber det der, og jeg bliver her, kommer jeg aldrig ud.",
"Her? Jeg er lidt bange. Kommer jeg ud?",
"Bare vent og se."}

l3ishoulndtbe={"Jeg burde ikke tænke på dette."}

l3wewillgiveyouahint={"Vi vil give dig et hint her, spiller. Du skal sætte den bog derovre, til venstre.",
"Så den vil fange den stålcylinder, når jeg skubber den.",
"Men vi vil ikke fortælle dig, hvordan du gør det.",
"Hvad hvis jeg går den øverste vej?",
"Shhhh. Det er for mange hints."}

l3justremember={"Husk bare, at selvom vi ikke kan skubbe genstande langs hinandens bagside, kan vi skubbe dem ned eller på en solid struktur."}

l3thingslikethat={"Sådanne ting sker nogle gange."}

l3sometimes={"Nogle gange skal du tænke langt frem.",
"Og ofte bliver du nødt til at genstarte niveauet."}

l3weadmitwedid={"Vi indrømmer, at vi gjorde det med vilje.",
"Så du kunne prøve det igen - denne gang uden nogen hints."}
