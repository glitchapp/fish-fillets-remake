l13={"Dette må være et vikingeskib!",
"Selvfølgelig, det er en drakkar.",
"Hun må være sunket hurtigt. Årerne er stadig i deres pladser.",
"Måske sænkede de hende med vilje. Krigere ville altid følge deres leder på hans vej til Valhal.",
"Det er forfærdeligt. De ser ud som om, de stadig er i live.",
"Musikken er forsvundet!",
"De stoppede med at spille!",
"Musikken stoppede igen!",
"Oh nej!",
"Igen?",
"Oh nej, ikke igen!",
"Hvad skete der?",
"Igen?",
"Oh nej!",
"Den forbandende musik stoppede!",
"Hvad laver de deroppe?",
"Hvordan kunne det ske?",
"Tag det roligt, drenge. Solo for Olaf!",
"Ligegyldigt med det, drenge. Vi kan synge selv!",
"Ligegyldigt, drenge. Vi kan synge selv, som i gamle dage!"
}

l13skull={"Kan I ikke interagere med os? Vi prøver at få lidt hvile her.",
""
}
