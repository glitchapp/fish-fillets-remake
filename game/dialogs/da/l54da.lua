l54={"Jeg tror, vi kan få nogle oplysninger om interstellar fremdrift her.",
"Dette ligner mere konventionel fremdrift for landingsfartøjer.",
"Så. Vi fandt drivkraften. Vi har opnået et af vores missionens mål.",
"Vær ikke så hurtig. Vi har endnu ikke søgt hele vraget.",
"Jeg beklager, at ingen af disse tekniske vidundere omkring os virker.",
"Jeg ville gerne se nogle af disse udenjordiske dimser i aktion.",
"Jeg undrer mig over, om denne motor kan fungere under vand.",
"Måske er det bedre for os, hvis den ikke virker.",
"Jeg er faktisk glad for, at vi ikke kan tænde noget her.",
"Vi bør være glade for, at vi endnu ikke har kunnet tænde noget.",
}
l54engineon={"Hvad har du gjort? Sluk for den larm!",
"Hvad har du aktiveret? Hvor fører det os hen?",
"Dette er forfærdeligt! Sluk det, inden det er for sent!",
"Jeg kan ikke! Jeg kan ikke få det til at stoppe!",
"Jeg ved ikke hvordan! Jeg kan ikke få det til at stoppe!",
"Mayday! Mayday!",
"Hvordan slukker jeg det?!",
"Jeg kan ikke få det til at stoppe!"
}

l54engineonfish1={"Hvad laver du? Hvor tager vi hen?",
"Hvad har du aktiveret? Hvor fører det os hen?",
"Jeg kan ikke! Jeg kan ikke få det til at stoppe!",
"Jeg ved ikke hvordan! Jeg kan ikke få det til at stoppe!",
"Mayday! Mayday!",
"Hvordan slukker jeg det?!",

}
--"Jeg kan ikke få det til at stoppe!"
l54engineoff={"Endelig.",
"Hvad en lettelse.",
"Tak.",
"Endelig.",
"Jeg er bare bange for, at vi bliver nødt til at tænde det igen.",
""
}

l54aproachengine={"Hvad laver du? Hvor er du på vej hen?",
"Vær forsigtig med den skruenøgle.",
"Jeg kan ikke skade noget her."
}
