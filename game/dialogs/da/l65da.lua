l65={"Så dette er, hvordan det mest succesrige spil nogensinde oprindeligt så ud.",
"Det er kun dyb respekt for dets minde, der forhindrer mig i at sige, hvordan det ser ud...",
"Men alle andre kan se det selv.",
"Ved du, hvad jeg har i tankerne?",
"Nå?",
"Jeg tror, vi kunne lave en bedre tetris end dette rum.",
"Hvad mener du med bedre?",
"I det mindste behøver du ikke flytte alle brikkerne til højre.",
"Sandheden er, at vi har rigeligt med egnede objekter her.",
"Jeg tror, at en lille kodeopdatering kunne give spilleren muligheden for at nyde noget originalt tetris.",
"Okay, prøv at programmere det!",
"Vær forsigtig med ikke at skade os.",
"Vi bliver nødt til at stable disse brikker mere effektivt, måske.",
"Hmm... Jeg burde have arrangeret det bedre.",
}
