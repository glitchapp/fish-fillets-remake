l2={"Åh nej, ikke igen...",
"Du ved, der er ikke noget, vi kan gøre ved det.",
"Lad os skubbe det ned, så vi kan se på det.",
"Nå, lad os gå i gang",
}

l2thediskself={"Hvordan? Disken ødelagde sig selv.",
"Så kan vi endelig komme i gang.",
"Vores første opgave er at komme ud af dette rum.",
"Dette bliver i øvrigt vores opgave i alle de andre rum også.",
"Kom, lad os tage spilleren med ind på værkstedet og vise ham arbejdssikkerhedsforskrifterne.",
"God idé.",
"For nu skal du ikke røre ved noget, bare se og lær. Vi vil vise dig, hvad du skal og ikke skal gøre med os samt hvilke ting, vi er i stand til.",
"Før vi går ind på værkstedet, lad os gemme spillet - tryk bare på F2-tasten.",
"Først vil vi vise dig, hvad der kan skade os.",
"Jeg melder mig frivilligt som forsøgsdukke.",
"Først og fremmest må vi ikke tabe ting på hinanden.",
"Vi kan heller ikke svømme nedad, når vi bærer en genstand, fordi den ville falde ned på os.",
"Noget andet, vi ikke må gøre, er at skubbe genstande langs vores ryg.",
"Nogle genstande er formet på en måde, så vi kunne holde dem og skubbe dem på samme tid - men det må vi heller ikke gøre.",
"Vi må heller ikke tabe yderligere genstande på en bunke, som en af os bærer.",
"Og i modsætning til min større partner kan jeg ikke flytte eller bære stålobjekter.",
"Vi kan altid samle genstande op og derefter lade dem falde.",
"Vi kan skubbe en genstand langs hinandens ryg, kun hvis genstanden derefter vil blive skubbet over på en anden støtte...",
"... og endda lade den falde ned på hinanden i dette tilfælde.",
"Jeg kan endda gøre det alene... Jeg kan svømme nedenunder den... eller lægge den på denne afsats.",
"Vi kan også skubbe en genstand hen over toppen af en anden genstand, som vi støtter, men vi kan ikke flytte genstanden, der er direkte på vores ryg.",
"Vi kan også frit svømme under en genstand, som vi støtter, og give den videre til den anden fisk.",
"Og vi kan skubbe genstande væk fra hinandens ryg, så længe genstanden falder ned med det samme.",
"Alt i alt kan vi kun løfte genstande, lade dem falde, skubbe dem langs en solid overflade, skubbe dem på en overflade og skubbe dem væk fra hinanden.",
"Det er cirka reglerne. Hvis du vil vide mere, skal du trykke på F1 og læse hjælpesektionen.",
}

l2wecouldrestart={"Vi kunne genstarte niveauet.",
"Det er rigtigt...",
}

l2icantliftit={"Jeg kan ikke løfte den. Kan du prøve?",
"Bare en smule højere, så jeg kan svømme under dig.",
}

l2lethisbytheway={
"Skal vi se det igen?",
}

l2nowell={"Nu starter vi igen - eller vi kan indlæse det gemte spil ved at trykke på F3-tasten.",
"Og nu er vi tilbage, hvor vi sidst gemte spillet.",
}

l2again={"Igen indlæser vi et gemt spil ved at trykke på F3-tasten.",
}

l2wehaveloaded={"Vi har indlæst vores gemte spil for sidste gang. Nu vil vi vise dig alle de ting, vi er i stand til.",
}

l2briefcasetalkie={"Godmorgen, fisk.",
"Dette er en sag af allerstørste vigtighed, og derfor har vi valgt jer,",
"vores dygtigste undervandsagenter.",
"Agenter fra FDTO - Fiskedetektivtræningsorganisationen -",
"har fået fat i flere amatør-snapshots af et udenjordisk objekt,",
"som er styrtet ned et sted i jeres nærhed.",
"Jeres mission, hvis I vælger at acceptere den,",
"vil være at bjerge UFO'en og indhente al den information, I kan, om principperne og naturen ved interstellar fremdrift.",
"I skal også forsøge at løse nogle af vores endnu uløste sager.",
"Vi er især interesseret i omstændighederne omkring sankningen af den mytiske by, tilfældigvis i samme område.",
"Denne sag kan være forbundet med mysteriet om Bermuda-trekanten.",
"Find ud af, hvorfor så mange skibe og fly er forsvundet i dette område i løbet af de seneste årtier.",
"Det siges, at et af de forsvundne skibe tilhørte den legendariske kaptajn Silver.",
"Der er stadig mange usikkerheder omkring denne berømte pirat.",
"Vigtigst af alt er vi interesseret i kortet, der viser placeringen af hans begravne skat.",
"En af jeres vigtigste opgaver er at finde computeren, der er skjult i dybet af en bestemt kriminel organisation.",
"Den indeholder data om et projekt, der kan ændre hele verden.",
"I skal også finde og tilbageholde den mystiske koralskildpadde.",
"Den undslap fra FDTO's forhørsfacilitet.",
"Den er ikke bevæbnet, men siges at have telepatiske evner.",
"Vi har fået besked om, at et visst atomkraftværk ulovligt har dumpet radioaktivt affald.",
"Tjek det ud. Og glem ikke at finde Den Hellige Gral.",
"Og som altid: Hvis nogen i jeres team bliver såret eller dræbt,",
"vil Altar benægte ethvert kendskab til jeres eksistens, og niveauet vil blive genstartet.",
"Denne disk vil selvdestruere om fem sekunder.",
}
