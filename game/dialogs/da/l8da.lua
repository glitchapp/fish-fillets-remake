l8 = {
    "Toilettet er Davids yndlingssted.",
    "Hvad?",
    "Kender du ikke David? Han er en af kunstnerne, der arbejdede på dette spil.",
    "David var ikke grafisk kunstner. Han var en billedhugger. Hans skulpturer er nogle af de mest kendte renæssanceartefakter.",
    "Du har helt misforstået det.",
    "Sagde jeg ikke, at VI ikke har brug for et skylletoilet? Og dekoreret med din skid, endda!",
    "Det skylletoilet er mere hygiejnisk, og når det er under vand, endnu mere økologisk.",
    "Lykkeligvis behøver jeg ikke kravle derind.",
    "Jeg har det ikke særlig godt her. Jeg føler, at jeg er på kirkegården.",
    "Hvad mener du?",
    "Ved du ikke, hvor mange akvariefisk der ender deres liv?",
}
