l68={"Sådan ser det ud, når du ikke holder dit skrivebord ryddeligt.",
"Ville det ikke hjælpe, hvis vi vælger Skrivebord/Justér ikoner?",
"Hvad nu hvis vi prøver at minimere de største vinduer?",
"Hvad nu hvis vi prøver at lukke nogle af disse vinduer?",
"Hvad med at slukke det hele og skifte til kommandoprompten?",
"Hvad med at give helt op og i stedet svømme til pubben?",
"Talte du disse vinduer omkring os?",
"Jeg behøver ikke tælle dem. Der er femoghalvfems af dem.",
"Kan du se den Notesblok? Nu er det et godt tidspunkt at sende en besked til spilleren!",
"Stop den narrestreg og prøv at tænke i stedet.",
"Denne system skal have et hul et sted.",
"Selvfølgelig. Det er i det nedre højre hjørne.",
"Kan du se det store vindue til højre?!",
"Oh nej! Det må være originalversionen af dette spil.",
"Det kørte på de gamle maskiner, i standard VGA-opløsning...",
"Ingen animation...",
"Ingen tale...",
"Ingen musik...",
"Kun en bip lyd fra tid til anden...",
"Men heldigvis vendte forfatterne tilbage til det og gav det dette moderne ansigtsløft.",
"Men jeg vil stadig gerne spille det en dag!",
"Jeg kan ikke flytte dette vindue. Nede er det en stålcylinder, selvom det kun er i seksten farver i VGA.",
"Vent et øjeblik, spiller. Vi skal have noget klart. Disse to fisk, de er vores yngre selv.",
"Du har ret. De er fanget der, stakkels sjæle.",
"Vi kan ikke efterlade dem der. Vi skal få dem ud!",
"Ja, men spilleren skal få os ud.",
"Så vi går i strejke. Personligt har jeg fuld sympati for den større fisk indeni.",
"Du har ret. Jeg kan forestille mig, hvordan den mindre føler det.",
"Vi vil ikke spille, før du redder de to, spiller.",
"Åh gud, det er et realistisk spil!",
"Åh nej, han tog spillet for alvorligt!",
}
