l64={"Det der ovre må være den Hellige Gral!",
"Hvordan kan du være så sikker?",
"Kan du ikke se den hellige glorie?",
"Kan du ikke se, hvordan den lyser blandt de andre?",
"Kan du ikke se, hvor påfaldende den er placeret i midten af hulen?",
"Glorien betyder ingenting. Den Hellige Gral kan godt se helt almindelig ud.",
"Alt hvad der glimter er ikke guld. Ingen har nogensinde sagt, at den Hellige Gral skal lyse.",
"Hmm... At placere en almindelig Gral på et påfaldende sted vil ikke gøre den hellig.",
"Jeg har en fornemmelse af, at det ikke bliver, som det ser ud til.",
"Dig og dine fornemmelser.",
"Lad os gå, vi skal tage den centrale, lysende Gral med os.",
"Jeg vil hellere tage dem alle sammen.",
"Hmm... Det ser ud til, at du havde ret. Vi tager dem alle sammen.",
"Vores mål i dette niveau er at skubbe den Hellige Gral ud.",
"Nej, at skubbe alle Grals ud.",
"Den Hellige er nok.",
"Vores mål er at skubbe alle Grals ud af dette rum.",
"Næsten alle er væk nu!",
"Én mere Gral, og vi er færdige!",
}

l64end={"Vi anbefaler at underkaste det vedlagte materiale en grundig test af hellighed.",
"På grund af manglende udstyr udførte vi kun de basale tests, og resultaterne er mere end lovende.",
"Ved at bombardere nogle af gralerne med ultrasnavsede ord målte vi endda 3 julemænd nogle gange,",
"nogle af dem kunne modstå forholdstrykket på 0,8 jobs i en hel minut!",
"Det er også muligt, at vi har stødt på hele den Hellige Service.",
}
