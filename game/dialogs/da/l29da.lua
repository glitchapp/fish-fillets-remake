l29 = {"Se og skue!",
"Forestil dig det!",
"Hvem kunne have gættet det?!",
"Sådan en uagtsomhed.",
"Tusinder omkom - hele byen forsvandt i bølgerne på grund af sådan inkompetence.",
"En frakoblet prop.",
"En pinlig brøler.",
"Hvad skal vi gøre med den?",
"Vi kan prøve at sætte den tilbage på plads.",
"Og så? Skal vi drikke det vand, der hældte ind, eller hvad?",
"Vi kan sætte den tilbage som et tegn på vores anerkendelse af borgernes heroiske indsats for at holde den flydende. Som et minde om deres flittige, dygtige og... vedholdende natur.",
"Af hvad? 'Hvor løber du hen? Jeg skal bestille otte sværd.' Skæbnen selv afbrød den prop. Forestil dig, at du skulle høre sådan noget derhjemme. Dag for dag.",
"Og desuden: Er det sandsynligt, at nogen nogensinde vil komme her? Kun sepiaer vil lejlighedsvis tygge på den.",
"Vi kommer til at føle os godt tilpas med det.",
"Vi kommer til at føle os skuffede over det. Tror du, chefen kommer til at tro det? En gigantisk prop, der blev afbrudt? Og hvad har du gjort ved det? Vi satte den i igen. He, he, he.",
"Du har måske ret. Det ville måske være bedre at tage den med.",
"Det synes jeg også.",
"Lad os gå i gang.",
"Hvad nu hvis vi bare lader den prop være her?",
"Hvad ville Agenturet sige?",
"Jeg tror ikke, det betyder noget, hvad chefen kommer til at tænke om mig.",
"Vent lidt. Vi skal nok finde en løsning.",
"Den forbandende prop. Hvad nu hvis vi bare finder på noget.",
"Såsom hvad, for eksempel?",
"Nå, vi kunne sige, at byen faktisk aldrig har eksisteret.",
"Nå, vi kunne sige, at byen sank, fordi det arktiske is smeltede.",
"Et jordskælv kunne for eksempel have sunket byen.",
"En tsunami kunne for eksempel have sunket byen.",
"Vi kunne forsøge at sige, at en vulkan brød ud i byens centrum.",
"Det er bare vrøvl.",
"Denne gang er vores mål at få den prop ud.",
}

l29end = {"Vi lykkedes med at opdage den rigtige årsag til byens forlis.",
"Det var et velkendt artefakt kaldet atlantisk relief,",
"der hidtil fejlagtigt blev betragtet som en fremstilling af udenjordisk besøg,",
"der gav os et hint.",
"På samme tid anbefaler vi at øge overvågningen",
"af propperne på alle kontinenter og større øer",
"for at undgå at gentage en lignende pinlig katastrofe igen.",
}
