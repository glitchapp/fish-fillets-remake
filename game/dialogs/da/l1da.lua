l1={"Hvad var det?",
"Jeg har ingen idé.",
"Vi burde gå ud og se efter.",
"Vent! Jeg går med dig.",
"Hvorfor sker der ikke noget?",
"Hey, spiller!",
"Du kan styre os med pilene og bruge mellemrumstasten til at skifte fisk.",
"Eller du kan styre os med musen - venstreklik et sted og...",
"...hvis vi kan komme derhen uden at flytte noget...",
"...vil vi følge dine klik.",
"Og når du højreklikker et sted, vil vi forsøge at komme derhen ved at skubbe alle forhindringer væk.",
"Dit mål bør være at få os begge ud.",
"Og pas på ikke at skade os. Hvis du taber noget på os eller...",
"Oh, stop det. Vi fortæller ham det i næste niveau.",
"Okay.",

"Wow, du flyttede den! Det kunne jeg aldrig have gjort!",
"Tak, nu kan jeg tage med dig.",
"For søren, jeg kan ikke komme igennem.",
"Jeg må være blevet lidt tykkere.",
"Kan du skubbe stolen lidt til venstre?",
"Ups! Det var en fejl. Vi skal begge kunne komme ud for at fortsætte.",
"Jeg kan ikke komme ud nu. Du bliver nødt til at genstarte niveauet.",
"Du skal trykke på den lange pil på dit tastatur.",
"Mennesker kalder det en backspace-tast.",
"Uanset hvad. Det var sjovere på tjekkisk.",
"Genstartindstillingen findes også på kontrolpanelet.",
"Hvis du foretrækker at genstarte niveauet, er det ikke noget problem.",
"Du skal bare trykke på den lange pil på dit tastatur.",
"Jeg er en tung stålcylinder. Det er ikke nemt at flytte mig rundt. En så lille fisk som den orange derovre har simpelthen ingen chance. Jeg kunne knuse hende uden nogen problemer.",
}

l1icantgetthrough={"Jeg kan ikke komme igennem her. Den stålcylinder er i vejen.",
"Jeg kan ikke flytte den cylinder. Kan du hjælpe mig?",
"Ingen problem...",
}

l1wowyoumovedit={
"Wow, du flyttede den! Det kunne jeg aldrig have gjort!",
"Tak, nu kan jeg tage med dig.",
}

l1damnit={"For søren, jeg kan ikke komme igennem.",
"Jeg må være blevet lidt tykkere.",
"Kan du skubbe stolen lidt til venstre?"
}
