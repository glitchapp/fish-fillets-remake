l3={
"Tu ya conoces las reglas.",
"Ahora puedes ponerlas en práctica.",
"Te daremos algunas pistas ocasionalmente.",
"No debo nadar hacia abajo ahora." ,
"Y tampoco puedo ir hacia arriba.",
"Hey, ordena un poco ahi de alguna forma.",
"¡Pero sé cuidadoso!"
}

l3icantmove={"No puedo mover esto. Esto es acero.",}

l3comeback={"Regresa, ¡No puedes ir más lejos!",}

l3thisistricky={"Hmm... esto es difícil.",
"Si empujamos a un lado la miel, seremos aplastados por ese tarro de jugo de albaricoque.",
"¿No podríamos empujarlo a un lado con esa hacha?",
"Esa es una buena idea.",
"Ahora no puedo tomar el hacha. El tarro me bloquea el camino.",
"¡No debes empujar el hacha!",
"Si tu puedes empujar ese tarro hacia mí, empujaré el hacha sobre el tarro y todo estará bien."
}

l3thislooksrealbad={"Esto luce realmente mal. Vas a tener que reiniciar el nivel.",}

l3ifyousolvesomespecific={"Si tu resuelves alguna parte en específico puedes guardar la posición.",
"Pero sólo si estás seguro de tu solución.",
"Recuerda lo que hemos dicho acerca de guardar posiciones.",}

l3youarestandinginmyway={"Estás en mi camino ahí. Trata de esconderte en alguna otra parte.",
"Si sueltas esto y yo me quedo aquí, nunca podré salir.",
"¿Aquí? Estoy un poquito preocupado. ¿Conseguiré salir?",
"Sólo espera y mira.",}

l3ishoulndtbe={"No debería de estar pensando acerca de esto.",}

l3wewillgiveyouahint={"Te daremos una pista aquí, jugador. Tendrás que poner ese libro ahí, hacia la izquierda.",
"Así que atrapará ese cilindro de acero cuando lo empuje.",
"Pero no te diremos cómo hacerlo.",
"¿Qué pasa si voy por arriba?",
"Shhht. Esas son muchas pistas."
}

l3justremember={"Sólo recuerda que aunque no podemos empujar objetos a lo largo de nuestras espaldas, podemos empujarlas para que caigan o en alguna estructura sólida."}

l3thingslikethat={"Cosas como esa algunas veces ocurren."}

l3sometimes={"Algunas veces tienes que pensar muuuy hacia adelante.",
"Y entonces tienes que reiniciar el nivel frecuentemente."}

l3weadmitwedid={"Admitimos que hicimos esto a propósito.",
"Así que podrías intentarlo nuevamente - esta vez sin ninguna pista."}
