l7={
"¡Mi dios, el impacto creó una caverna!",
"No puedo avanzar por esta caverna, tenemos que encontrar otra salida.",
"Este no es el camino.",
"No lo haré solo.",
"Yo probablemente debería haber levantado esa concha primero.",
"Quizás tenemos que intercambiar esos objetos.",
"Un colchón. Lo mejor que puedes conseguir bajo el agua.",

}

l7part2={"Ahora podemos hacer caer el caracol en el colchón.",
"Será difícil recoger el caracol desde ahí.",
"Finalmente, está allí.",
"El pobre caracol...",
"Deberíamos de buscar en los arrecifes de coral.",
"Habrán muchos seres interesantes para investigar aquí.",
"¿No necesitamos un microscopio para investigar corales?",
"Si, son pequeños. Pero pueden haber otras formas de vida.",
"Tortugas de coral, por ejemplo.",
"Y además sospecho que hay un microscopio en un batiscafo.",
"",
"",
}
