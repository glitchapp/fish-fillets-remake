l17={
"Esta nave se debe de haber hundido desde hace mucho tiempo.",
"Definitivamente. Es una de las drakkars más antiguas. Juzgando por la forma de la proa, el número de espirales y el color del agua pienso que perteneció al abuelo de Eric el Rojo.",
"O al suegro de Olafo el Bravo.",
"O quizás al tío abuelo de Leif el Hábil.",
"También podría pertenecer a la sobrina de Harold el Grande.",
"O posiblemente a la tía de Snorr el Intrépido.",
"Es incluso posible que perteneciera a Thorson el Duro.",
"Yo siempre pensé que era el barco pesquero de Eric el Incapaz... Pero hoy supe la verdad. ¡Si, nunca es demasiado tarde para comenzar una nueva vida!",
"Mira ese pobre perro.",
"¿No parece estar respirando en forma extraña?",
"Siendo un perro bajo el agua creo que lo está haciendo bien.",
"El no se ve tan mal considerando que ha estado aquí por unas pocas centurias.",
"¡Silencio!",
"¡Te vas a callar!",
"Entonces, ¿No te vas a callar, o si?",
"Aaaaahhh...",
"He-he-he-he-he... Eh-eh-eh-eh...",
"¿Terminó?",
"¿Estamos a salvo ahora?",
"¿Terminó la pelea?",
"¿Se fué el enemigo al final?",
"¿Todavía estamos peleando?",
"¿Todavía estamos en el Valhalla, jefe?",
"¿Cuando vamos a llegar al Valhalla?",
"Te lo haré saber cuando lleguemos allí.",
"¿Me puede relevar, señor?",
"Espera a que lleguemos al Valhalla.",
"¿No nos está tomando mucho esta jornada al Valhalla?",
"Paciencia es la señal de un guerrero verdadero.",
"¿Estás seguro que de esta forma se llega al Valhalla?",
"¡Un guerrero debe de confiar en su comandante!",
"Ahhh, estoy a cargo de una nave llena de cobardes.",
"¿Porqué tuve que conseguir una nave llena de bufones?",
"Bufones, un perro baboso, creo que me voy a ahogar yo mismo.",
"Los Vikingos de verdad tienen barbas.",
"B-b-barbas están de moda ho-hoy en d-d-día.",
"¿Qué clase de capricho es ese, trenzas? Por favor...",
"D-d-das Wikinger M-m-modemagazin empfiehlt ei-einen blonden Zopf a-als Ergänzung zu einem d-dunklem Helm und einem bb-blauen Schild.",
"M-m-moda Vikinga M-m-mensual r-recomienda una barba rubia para complementar un c-casco os-os-oscuro y una coraza a-a-azul.",
"Simplemente no estoy de acuerdo con ese capricho.",
"En lugar de eso deberías apegarte a las tradiciones Vikingas.",
"¡P-p-pero soy lo m-m-máximo!",
"¡Incluso Erik El G-g-grande llevaba t-t-trenzas!",
"Ridículo. El tenía barba.",
"Pero e-él también u-u-usaba t-trenzas.",
"El no usaba. Y punto final.",
"¿T-t-tu crees que una b-b-barba me quedaría m-m-mejor?",
"Definitivamente.",
"Lo v-v-voy a p-p-pensar.",
"Hmmm... Ehmmm... Hahmmm...",
"N-no, t-t-trenzas son d-definitivamente m-m-mejor.",
"La juventud de hoy - absolutamente increíble.",
"Un guerrero con trenzas. No había escuchado eso.",
"Ustedes jóvenes nada más piensan que pueden salirse con la suya en todo hoy en día.",
"Bueno, yo todavía me veo bien.",
}
