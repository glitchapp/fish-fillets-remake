l31={
"Esta si que es una habitación extraña.",
"Esta es una habitación muy inusual.",
"El puesto de asistente de coordinación de diseño fué introducido debido a este nivel. Así que su autor obtuvo su propio crédito.",
"Ya basta de hablar sobre el juego. Vamos a trabajar.",
"Este es un coral muy extraño.",
"Este es un coral muy peculiar.",
"¿Y de donde cuelga?",
"No lo sé. Pero tenemos que bajarlo de todas maneras.",
"No lo sé. ¿Tenemos que ponerlo abajo?",
"¿Cómo es que ese coral adquirió una forma tan bizarra?",
"Está cuidadosamente crecido para los juegos de lógica.",
"¿Porqué quieres poner ese coral abajo, de cualquier forma?",
"No tengo idea. Pregúntale al jugador.",
"Caracol, caracol, saca tu cachito al sol...",
"¡Para! ¡Como si tu no supieras que ningún objeto se moverá si nosotros no lo empujamos primero!",
"Pero puedo intentarlo, ¿Cierto? Caracol, caracol...",
"¡Detente! Que se me erizan las escamas.",
"No escuches entonces. Caracol, caracol, saca tu...",
"¡Basta! ¡O te voy a tirar ese coral en la cabeza!",
}
