l20={"Que gloriosa civilización. Tantas centurias bajo el agua y sus ascensores siguen funcionando.",
"Especialmente, cuando los empujamos.",
"Pero sólo hacia abajo.",
"¿Ves esa concha?",
"Es un antiguo talismán para constructores de ascensores quienes siempre dicen 'Construya bien. Construya con Shell'.",
"¿No te parece esa concha un poco familiar?",
"Deberían de avergonzarse de si mismos. Este es un comercial subliminal.",
"Esa calavera luce familiar.",
"Todas las calaveras humanas lucen igual... No como los esqueletos de peces.",
"Este es un duro camino.",
"Me siento como un ratón en una rueda de molino.",
"¿Una rueda de molino para peces? Pero si sólo tenemos aletas.",
"Hoo, hooooo, hoooo... Estoy aquiiiii... ",
"Hooo, si quieren les diré como resolver todos los niveles...",
"Hooo, ho hooo, ¡Yo sé por que la ciudad se hunde!",
"Hoooo, ¿Porqué nadie me presta atención?",
"¡Hoooo ho hooo! ¿Porqué me ignoran?",
"Hooooo, hooooo, ¡pez! ¡Póneme atención!",
}
