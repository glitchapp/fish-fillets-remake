l1={"¿Qué fué eso?",
"No tengo idea.",
"Deberíamos ir a mirar afuera.",
"¡Espera! Voy contigo.",
"¿Porqué no está ocurriendo nada?",
"¡Oye, jugador!",
"Puedes controlarnos con las teclas de flecha y usar la barra espaciadora para intercambiar de pez.",
"O puedes controlarnos con el ratón -- click-izquierdo en alguna parte y...",
"...si podemos llegar ahí sin mover nada...",
"...seguiremos tus clicks.",
"Y cuando hagas click-derecho en alguna parte, trataremos de llegar ahí, empujando a un lado todos los obstáculos en nuestro camino.",
"Tu meta debería ser sacarnos a los dos afuera.",
"Y sé cuidadoso de no herirnos. Si dejas caer algo en nosotros o...",
"Oh, basta. Se lo diremos en el nivel que sigue.",
"Estoy de acuerdo.",
"¡Oh no! Ese fué un error. Ambos tendremos que ser capaces de salir en orden para continuar.",
"No hay forma de que pueda salir ahora. Tendrás que reiniciar el nivel.",
"Tienes que presionar la flecha grande en tu teclado.",
"Los humanos la llaman la tecla retroceder.",
"Lo que sea. Era más divertido en checo.",
"La opción de reiniciar también está localizada en el panel de control.",
"Si crees que deberías de reiniciar el nivel, no hay problema.",
"Sólo tienes que presionar la flecha grande en tu teclado.",
"Soy un pesado cilindro de acero. No es fácil moverme. Un pequeño pez, como el naranja que está allí, simplemente no puede. Yo lo podría aplastar sin ningún problema.",
}

l1icantgetthrough={"No puedo pasar por aquí. Ese cilindro de acero está en el camino.",
"No puedo mover ese cilindro. ¿Me puedes ayudar?",
"Ningún problema...",
}
l1wowyoumovedit={"¡Ooh, lo moviste! ¡Yo nunca podría hacer eso!",
"Gracias, ahora puedo ir contigo.",
}


l1damnit={"Esto está mal. No puedo pasar.",
"Debo de haber subido algo de peso.",
"¿Podrías empujar la silla un poquito hacia la izquierda?",
}
