l45={
"Crrueeel, crrueeel, crueeel Capitán Silverrrr...",
"Ay... ¡¡¡caramba!!!",
"Crrueeel Capitán Silverrr...",
"Teeesooooroooo, tooontooos...",
"¡Buen prrrrobleeema!",
"¡Eso es rrrrridículooo! Polly es la prrrreferiiiida del maeeestrrrroo.",
"¡No me emppppuuujeeennn!",
"¡Eso es meeejoooorrrr!",
"¡Y la muerrrrte está sobrre usteeeedeeesss!",
"Estarreeeee en su camiiiiino aquiiiiii.",
"Complicaaaado prrrrobleeeema, ¿cierrrrto?",
"¡Hazlo bailar!",
"Erres muuuuuuy gooorrrdooo, no pueeedesss cruzaaar.",
"¡Polly quiere un petarrrdo!",
"Golpéelo, señorrr...",
"¡Déjame solo y piérrrdete!",
"¿Que podría haber dentro?",
"Quién sabe...",
"¿Podrá ser esto el legendario tesoro de Silver?",
"¿En un cofre tan pequeño? El jefe seguramente no nos contactaría por una caja tan pequeña.",
"¡Puaj!",
"Gracias.",
"No hay problema.",
"¡¿Qué estás haciendo?!",
"Lo siento. Seré más cuidadoso la próxima vez.",
"¡Ten cuidado! ¡No le hagas daño!",
"¡Hay otro camino!",
"¡Ese es una clase de octopus! ¿Me pregunto, quien es el dueño?",
"Quizás perteneció a la persona que vivía en esta cabina.",
"Quien fuera que viviera ahí seguro que era raro.",
"¿Por qué?",
"Mira, ¡él tiene una calavera humana ahí!",
"Bueno, quizás es su propia calavera.",
"¿Tu lo crees?",
"¿Puedes sentir la atmósfera de podredumbre y decadencia? El fregadero en ruinas, el esqueleto del loro, la calavera humana, y la cuna quebrada...",
"¿Donde puedes ver la cuna?",
"Oh, no tienes ningún sentimiento poético... para nada.",
}
