l25={
"¿Donde estamos ahora?",
"Las pirámides... Nota como los adornos clásicos se mezclan en esta ciudad.",
"¿Qué es eso que se arrastra por ahí?",
"No puedes verlo desde aquí. Está en el otro lado de la pared.",
"¡Mira, la mujer está aburrida!",
"¿Crees que esto nos está tomando demasiado?",
"Inténtalo tu mismo, si eres tan listo.",
"¿Qué deberíamos decir?",
"Tu no tienes que llevar nada.",
"No tengas miedo.",
"¿Qué está escrito en esas tablas?",
"Gracias por todos los peces.",
}
