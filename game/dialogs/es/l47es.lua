l47={
"¿Ves? Este barco tuvo dos cañones. Este debió de haber sido un buque de guerra. El olor ocre de la pólvora todavía parece flotar en el aire...",
"Tu quieres decir, en el agua.",
"¿De cuál barco era ella? Quizás ella sirvió bajo el admirante Nelson... o el capitán Hornblower.",
"No, yo creo que llevas fuera cerca de un siglo.",
"Presiento que vamos a necesitar esa espada.",
"Tu y tus \"presentimientos\". Además, se supone que nosotros no demos pistas.",
"Eso fué un poquitín muy simple, ¿No crees?",
"¿Será posible que haya sido tan simple?",
"Debe de haber alguna clase de error.",
}
