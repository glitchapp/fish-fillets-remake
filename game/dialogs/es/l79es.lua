l79={"El jugador debe saber que los cilindros están vinculados con los autos del mismo color",
"¡¡POR UN PODER INVISIBLE!!",
"Alguien está jugando con nosotros: los autos se mueven. ¿O lo haces tú? ¿O lo hago yo? Estoy empezando a tener miedo".
"Me pregunto si los filetes de pescado son un buen juego para resolver esta sala.",
"¿A que te refieres?",
"El jugador debe aprender a resolverlo en otro lugar y solo debe repetirlo aquí.",
"Por el contrario, si el jugador lo resuelve solo aquí, lo admiraré en secreto.",
"Deberíamos salir del auto rojo.",
"No seas listo cuando alguien más ya lo es.",
"¿Qué estás haciendo? Todavía no has salido.",

}

l79end={"¡Buenos días, peces!",
"De nuevo, no nos decepcionaste.",
"El Comité General decidió condecorarte con las más altas órdenes.",
"Están hechos de chocolate con leche. Debido a la confidencialidad, cómelos de inmediato.",
"JEFE",
"PD: entiendo este pequeño problema de pld, pero la próxima vez dímelo con anticipación",
"para que podamos proporcionar un permiso de adopción.",
"PPS: Dime, ¿dónde encontraste un jugador tan bueno que lo manejara todo?",
"Desearía que ganara la computadora o al menos algunos de los otros premios.",
}
