l10={
"Mira, el bote fiesta.",
"Presiento que esto no va a ser un picnic normal.",
"Tu y tus teorías. Seguramente que está lleno de gente buena y amistosa.",
"Quizás, tengas razón. Este es un picnic extraño.",
"¿Crees que ellos podrían salir de este bote?",
"Espero que no puedan salir de ese bote.",
"¿Y que pasa si ellos comienzan a perseguirnos?",
"No creo que nos puedan alcanzar. Los esqueletos no pueden nadar muy bien.",
"Deja de pensar en eso y mueve tus aletas para que podamos salir de aquí.",
"¿Quieres sostener ese tubo de acero con un vidrio?",
"No puedo hacer nada más por ti.",
"¡Ten cuidado de no derramar!",
}
