l2={
"Oh no, no otra vez...",
"Tu sabes que no hay nada que podamos hacer al respecto.",
"Empujémoslo abajo para poder hecharle un vistazo.",
"Bueno, ¡A trabajar!",
}
l2thediskself={
"¿Cómo? El disco se autodestruyó.",
"Bien, ahora podemos comenzar finalmente.",
"Nuestra primera tarea será salir de esta habitación.",
"Esa, por cierto, será nuestra tarea en todas las otras habitaciones también.",
"Vamos, llevemos al jugador al taller y mostrémosle las medidas de seguridad.",
"Buena idea.",
"Por ahora, no toques nada, sólo mira y aprende. Te mostraremos que puedes y lo que no debes hacer con nosotros como también las cosas que somos capaces de hacer.",
"Antes de entrar al taller, guardemos el juego - sólo presiona la tecla F2.",
"Primero te mostraremos lo que nos puede hacer daño.",
"Me ofrezco como voluntario para ser el muñeco.",
"Primero que nada, no deberíamos de tirarnos cosas sobre nosotros.",
"Tampoco podemos nadar hacia abajo cuando llevamos un objeto, porque nos caería encima.",
"Otra cosa que no debemos hacer es empujar objetos a lo largo de nuestra espalda.",
"Algunos objetos son de una forma que podemos sostenerlos y empujarlos al mismo tiempo - pero no podemos hacer eso tampoco.",
"Tampoco podemos soltar otros objetos en una pila que uno de nosotros esté sosteniendo.",
"Y a diferencia de mi gran compañero, yo no puedo mover o incluso cargar objetos de acero.",
"Siempre podemos recoger objetos y luego dejarlos caer.",
"Podemos empujar un objeto sobre la espalda del otro sólo si el objeto será puesto sobre otro soporte...",
"... e incluso dejarlo caer sobre el otro en este caso.",
"Yo incluso puedo hacer esto solo... puedo nadar por abajo.... o ponerlo en su borde.",
"También podemos empujar un objeto sobre otro objeto que estemos soportando, pero no podemos mover el objeto que está diréctamente en nuestras espaldas.",
"También podemos nadar libremente bajo un objeto que estemos soportando y pasarlo al otro pez.",
"Y podemos empujar objetos desde la espalda de cada uno, siempre que el objeto caiga abajo inmediatamente.",
"Esas son las reglas. Si quieres saber más, presiona F1 y lee la sección de ayuda.",
"En resumen, podemos sólo levantar objetos, dejarlos caer, empujarlos sobre una superficie sólida, empujarlos hacia una superficie, y empujarlos desde cada uno de nosotros.",
}

l2wecouldrestart={"¿Veámoslo de nuevo?",
"Podríamos reiniciar el nivel.",
"Eso es cierto...",
}

l2icantliftit={"No puedo levantarlo. ¿Quieres intentarlo?",
"Sólo un poco más alto para que yo pueda nadar por abajo tuyo.",
}

l2lethisbytheway={
"Should we watch it again?",
}

l2nowell={"Ahora comenzaremos nuevamente - o podemos cargar el juego guardado presionando la tecla F3.",
"Ahora regresamos donde guardamos el juego por última vez.",
}

l2again={"Nuevamente, cargamos un juego guardado presionando la tecla F3.",
}

l2wehaveloaded={"Hemos cargado nuestro juego guardado por última vez. Ahora te mostraremos todas las cosas que podemos hacer.",
}


l2briefcasetalkie={"Buenos días, peces. Este es un asunto de grave importancia",
"así que los hemos escogido a ustedes, ",
"nuestros agentes submarinos más hábiles.",
"Agentes de la FDTO - Federación de Detectives peces Tomando Observaciones",
"pudieron conseguir varias fotos aficionadas de un objeto extraterrestre",
"que cayó en algún lugar en su vecindad. Su misión, si decide aceptarla,",
"será recuperar el OVNI y adquirir toda la información que puedan acerca",
"de los principios y naturaleza de la propulsión interestelar.",
"También deberían de tratar de solucionar los casos aún no resueltos.",
"Estamos muy interesados  en las circunstancias que rodean",
"el hundimiento de una ciudad mítica, de casualidad en la misma área.",
"Este caso podría estar conectado al misterio del Triángulo de las Bermudas.",
"Encuentren la razón de porqué tantos barcos y aviones han desaparecido ",
"en esta área en varias décadas pasadas.",
"Se rumorea que una de las naves perdidas perteneció al legendario Capitán Silver.",
"Hay todavía muchas dudas que rodean a este famoso pirata.",
"La mayoría de nosotros estamos interesados en el mapa que muestra",
"la ubicación de su tesoro enterrado.",
"Una de sus tareas más importantes es encontrar el computador escondido",
"en las profundidades por cierta organización criminal.",
"Este contiene datos acerca de un proyecto que podría cambiar el mundo entero.",
"Deberías encontrar y detener también la misteriosa tortuga de coral.",
"Escapó desde el lugar de interrogación de la FTDO. No está armada,",
"pero supuestamente tiene habilidades telepáticas.",
"Hemos sido notificados que una cierta planta de energía nuclear",
"ha estado arrojando sus desechos radioactivos en forma ilegal. Revísenlo.",
"Y no olviden encontrar el Santo Grial.",
"Y lo usual: Si cualquiera de su equipo es herido o muerto, Altar negará cualquier conocimiento de su existencia y el nivel será reiniciado.",
"Este disco se autodestruirá en cinco segundos.",
""
}

