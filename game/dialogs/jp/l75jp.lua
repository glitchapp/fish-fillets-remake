l75={"見てください、誰かが石に鍵を作りました。",
"どこに鍵があるのですか？私にはただドアから出ることができないだけです。",
"どこかに鍵があるはずです。鍵は下にあります。",
"とにかく出られないんです。あそこには何もかもが塞がっています。",
"だから私たちは鍵が必要なんです。",
"鍵を取ってもらえませんか？私は出たいんです。",
"できれば取りますが、詰まっていると思います。",
"見てください、ほうきがあります。",
"ここを掃除したくありません。ボイラールームでも十分大変でした。",
"なぜ不平を言うんですか？私が重い物を全て片付けましたよ。",
"私がいなかったら、まだここに閉じ込められているでしょう。",
"私たちは泳いで出られるかもしれません。ネットの穴がどれだけ大きいか見てください。",
"はい、私には小さすぎます。",
"本当に鍵を使って鍵を開けるべきだと思います。",
"なら取ってきてください、賢い人。",
"私に優しくしていてください。さもないと、私が一人で泳いで出てしまいますよ。",
"わかりました、もうそんなことは言いません。",
"それを考えるなんて。",
"うーん...わかりました。",
"鍵を少し動かせば取れるかしら？",
"そうは思いませんが、試してみましょう。",
"鍵を取ることはできません。長い間詰まっているので、サンゴに覆われています。",
"あきらめないでください。",
"あそこのつららは役に立つかもしれませんか？",
"どうせ取れないですよ。",
"ただ十分な力を加える必要があります。",
"もしかしたら、ハンマーを使ったらどうでしょうか？",
"痛っ、そのサボテンにはとげがあります。",
"もちろん、それはサボテンです。",
"ここで何をしているんでしょうか？",
"痛っ、そのサボテンにはとげがあります。",
"もちろん、それはサボテンです。",
"ここで何をしているんでしょうか？",
"取りました。今、私が出るまで鍵を持っていてください。",
"問題ありません。",
"私が鋼を運べるようになるまで。",
"早く覚えた方がいいですね。",
"今度こそ鍵を持っていてください。",
"頑張ります。",
"いや！ドアが再び",
"閉まりました。",
"では再び開けましょう。",
"ぐずぐず...",
"落ち着いてください。すぐに開けますよ。",
}
