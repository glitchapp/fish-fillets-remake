l44={"グリーンピースの誰かにこれを見せるべきだね。",
"スリーマイル島の誰かにこれを見せるべきだね。",
"これをどうすればいいのかな？",
"私たちは全ての生物を根絶やしにしなければならない。そのような忌まわしい存在は、私たちと同じ空気...いや、水を吸う権利はない。",
"私は原因を排除するべきだと思います。",
"まあ、おそらく君の言うことは正しい。",
"そして原因は間違いなくこの巨大な樽です。なんとか取り除かなければなりません。",
"そうですね。では作業に取り掛かりましょう。その後、それをミスターBの庭にドロップしましょう。",
"私は彼らが好きになってきている気がします。",
"彼らが私たちの邪魔をしなければならないことさえなければなあ。",
"私たちが必要な場所にいればいいのに。",
"こんなにも美しい変異体のコレクション。私たちが去るのは残念だ。",
"それはすぐには起こりません。",
"あなたは十分な時間を持って、それらを楽しむことができます。",
"もし私たちがそれらを持ち帰り、アルコールに保存したら、私たちはフリークショーを始めることができるかもしれません。",
"それとFDTOだけでは満足できないの？",
"私はすべてを聞こえているよ！",
"ごめんなさい、ボス。冒瀆するつもりはありませんでした。",
"わかったんだ。私はついに変異体にも魂があることに気付いたんだ。",
"UFOにも魂があるかもしれない。",
"私たちにとって、このレベルは永遠に私たちを変えることになると思います。",
"その足は単に嫌悪感を引き起こすだけでなく、無恥です。それを見てください。",
"あの魚、私にはなじみがあります。",
"おそらく原子力発電所の写真に写っていたのかもしれません。",
"かわいそうなカニ。たくさんのはさみを持っていますが、頭がありません。でも...おそらく私たちよりも幸せかもしれません。",
"一人ぼっちの男性は小さな黄色いアヒルと一緒にいることはありません。魚でさえも。",
"今回の目標は、その汚物を使って樽を押し出すことです。",
"しかし、これらの生物はここに残しておかなければなりません。彼らは健康な個体群の遺伝子プールを損なってはなりません。",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
}

l44end={"ゴミ捨て場から愛するボスへのご挨拶。",
"敬具",
"エージェント",
"PS：ここにいるのは素晴らしいことです。私たちは自らを放射能で汚染し、海で泳いでいます。新しい友達もたくさんいます。",
}
