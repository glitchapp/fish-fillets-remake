l7 = {
    "まあまあ、衝撃で崩落が起きました！",
    "私はこの崩落を通ることができません。",
    "別の出口を見つけなければなりません。",
    "こちらが正しい道ではありません。",
    "一人では無理です。",
    "おそらく、最初にあの貝殻を持ち上げるべきでした。",
    "おそらく、これらのオブジェクトを入れ替える必要があります。",
    "マットレス。水中で手に入る最高のものです。",
}

l7part2 = {
    "今、そのカタツムリをマットレスに落とすことができます。",
    "そこからそのカタツムリを取るのは難しいでしょう。",
    "やっとそこに着きました。",
    "かわいそうなカタツムリ...",
    "私たちはサンゴ礁を探索すべきです。",
    "そこには多くの興味深い生物がいるでしょう。",
    "サンゴを調査するには顕微鏡が必要ではありませんか？",
    "はい、それらは小さいです。しかし、他の生物も存在するかもしれません。",
    "例えば、サンゴの亀です。",
    "そしてさらに、バチスカーフに顕微鏡があるという疑いがあります。",
}
