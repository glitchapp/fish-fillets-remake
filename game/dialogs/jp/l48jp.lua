l48={"おやおや、これは素敵な台所ですね。",
"そうです、私たちは船の台所を発見しました。",
"くそったれの台所だ。",
"そして、私たちはシルバー船長の台所にいます。",
"彼自身が料理人だったとは思えません。",
"でもこれは私たちの問題ではありません。",
"ここにはあまり問題はありません。私はすぐに泳いで出られます。",
"でも私を見てください。",
"これらの仕切りテーブルがどうやってここに来たのか理解できません。",
"これらのテーブルが邪魔です。",
"この小さな部屋は私をゾッとさせます。",
"ここから出られません！",
"あなたをどう助けるか考えているだけです。",
"どうか私を助けてください！",
"考えています...",
"私は行動に移ります。",
"うわっ、この鍋には何か残っています！",
"この鍋は洗われていません。",
"ここでは誰も食器を洗いません。",
"椅子がなければ、私たちの作業がずっと楽になるのに。",
"特に私にとっては。",
"台所に椅子があるのはどうしてですか？",
"台所には椅子はありませんね。",
"誰でも時には休みたくなるものです。",
"私はどこかでこれらのテーブルを見たことがあります。",
"大量生産されているんですよ。",
"でもそれらは素敵ですね。私たちもいくつか注文すべきです。",
"おそらく、私だけがこの空間から出られるでしょう。",
"この船にもいくつかの難解な問題があるようです。",
"なぜその剣がここにあるのでしょうか？",
"おそらくそれを肉を切るために使ったのでしょう。",
"確かに。でも通常、台所では剣で肉を切りません。",
"ラッキーなことに、ここには料理人がいないようです。彼らは私たちを調理しようとするかもしれません。",
"その羊皮紙には何かレシピがあるはずです。",
"その羊皮紙には何か料理の謎が隠されているはずです。",
"おそらく、魚を調理する方法などです。",
}
