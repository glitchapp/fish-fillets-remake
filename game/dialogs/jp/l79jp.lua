l79 = {
"プレーヤーは、シリンダーが同じ色の車とリンクしていることを知るべきです。",
"見えない力によって！！",
"誰かが私たちと遊んでいるのです - 車が動いています。それともあなたがやっているのですか？それとも私が？怖くなってきました。",
"この部屋を解くのに、フィッシュフィレがいいゲームかなと思います。",
"それはどういう意味ですか？",
"プレーヤーは別の場所で解決方法を学び、ここではただ繰り返すべきです。",
"それどころか、プレーヤーがここでだけ解決する場合、私は彼を密かに賞賛します。",
"赤い車を出さなければなりません。",
"誰かがすでに賢いことをしているときに、自分も賢くならないでください。",
"何をしているのですか？まだ外に出ていないのですよ。",
}

l79end = {
"おはよう、フィッシュ！",
"また、がっかりさせないでくれましたね。",
"総務委員会は、最高位の勲章であなたを装飾することを決定しました。",
"それらはミルクチョコレートで作られています。秘密保持のため、すぐに食べてください。",
"ボス",
"PS: この小さな問題は理解していますが、次回は事前に教えてください。",
"それによって、採用許可を提供することができます。",
"PPS: 教えてください、どこでそんなに優れたプレーヤーを見つけたのですか、彼がすべてを解決しましたか？",
"彼がコンピューターまたは少なくとも他のいくつかの賞を獲得することを願っています。",
}
