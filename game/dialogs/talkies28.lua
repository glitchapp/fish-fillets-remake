function Obey.lev28()
 
  
  
  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  else
  loadcorrespondentfonts()
  end

 Talkies.say( "small fish",
    {
      "...",
      l1[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "Big fish",
    { 
       l1[2],
       l1[3],
       l1[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "small fish",
    {
      l1[5],
      l1[6],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
  Talkies.say( "Big fish",
    { 
       l1[7],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( "small fish",
    {
      l1[8],
      l1[9],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
 
 Talkies.say( "Big fish",
    { 
       l1[10],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Captive",
    { 
       l1[11],
       l1[12],
       l1[13],
       l1[14],
       l1[15],
       l1[16],
       l1[17],
       l1[18],
       l1[19],
       l1[20],
       l1[21],
       l1[22],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
 
end

function Obey.lev28skull()

Talkies.say( "skull",
    {
		l28skull[1],
		l28skull[2],
    },
    {
      image=skullavatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end
