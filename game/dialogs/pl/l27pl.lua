l27={
"Jestem tu uwięziony!",
"Ja chcę wyjść!",
"Jak mam się stąd wydostać?",
}

l27door={
"No widzisz, wydostałeś się.",
"No, to jesteśmy znowu razem.",
"Jestem w pokoju bez klamek!",
"Spróbuj poluzować jakiś kamień w ścianie.",
"Nie wydaje ci się, że ten krab jest jakiś dziwny?",
"Który?",
"No przecież ten na górze.",
"No ten na dole.",
"Mnie się wydaje całkiem normalny.",
"Może i trochę nietypowy...",
"Powinniśmy się cieszyć, że mamy tu te wszystkie piłki.",
"To miasto jest podejrzanie dobrze utrzymane...",
"Mam przeczucie, że coś nam umyka.",
"Te twoje przeczucia... lepiej myśl, jak się stąd wydostać.",
"Nie, czekaj, przyjrzyj się tym krabom... Spójrz im w oczy...",
"Ich oczy poruszają się jednocześnie... tak jakby były kontrolowane przez tę samą superświadomość.",
"Oczywiście, są kontrolowane przez program komputerowy. Podobnie jak to falowanie za nami, czy twoje głupie gadanie.",
"To niebywałe, że ta rzeźba tak długo milczy.",
"Wydaje mi się, że tylko czeka na sposobność.",
}
