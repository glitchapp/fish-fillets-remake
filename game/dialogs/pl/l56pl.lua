l56={"",
}

l56robodog={"No, wreszcie...",
"Uważaj, może cię pogryźć!",
"To jest... chyba... ROBOPIES!",
"Co? TEN robopies?!",
"No to wszystko jasne.",
"Włączając Kryzys Kubański i przepowiednie Nostradamusa.",
"Włączając morderstwo Johna Lennona i Meteoryt Tunguski.",
"Włączając katastrofę Challengera i bitwę pod Legnicą.",
"Włączając katastrofę w Czarnobylu i rozbicie się Arki Noego.",
"To ja już wiem, o co chodzi w tej grze.",
"A także sens życia, wszechświata i w ogóle.",
"KLIK",
"KLIK",
"Nie słuchaj ich, graczu, robią cię w konia.",
"Nie słuchaj ich, graczu, żarty sobie robią.",
"Nie słuchaj ich, graczu, nabijają się z ciebie.",
"Jestem zwykłą elektroniczną zabawką.",
"Choć w załóżeniu miałem być skuterem.",
"Choć w załóżeniu miałem być kosiarką do trawy.",
"Choć w załóżeniu miałem być stacją orbitalną.",
"Choć w załóżeniu miałem być rękawiczką jednopalczatką.",
"I tylko raz byłem na Kubie.",
"I wszyscy wiedzą, że kto inny go zamordował. Niczego mi nie udowodnią.",
"I kogo obchodzi paru żołnierzy?",
"Zresztą, powiedzieli mi, że woda chłodnicza nie jest konieczna.",
}

l56switchoff={"Włącz to światło!",
"Co ty wyprawiasz?!",
"Odbiło ci? Włącz światło!",
"Poczekaj, muszę znaleźć ten wyłącznik.",
"Nie mogę, ten cholerny wyłacznik spadł na dół.",
"Sama sobie włącz, jak jesteś taka mądra.",
"Gdzie jesteś?",
"Halo!",
"Powiedz coś.",
"Tutaj.",
"Tu jestem.",
"Jestem tutaj.",
"Gdzie jesteś? Boję się...",
"Boję się ciemności.",
"Nie odpływaj, strasznie tu ciemno...",
"Nie bój się, jestem przy tobie.",
"Nie marudź, damy radę.",
"Nie bój nic.",
}

l56switchon={"Mam nadzieję, że mnie nie kopnie...",
"Oczy mnie już bolą od tego migotania.",
"W życiu nie pomyślałam, że kiedyś się tak ucieszę na twój widok.",
}
