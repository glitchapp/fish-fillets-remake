l65={"So this is what the most successful game of all time originally looked like.",
"Only deep respect for its memory prevents me from saying what it looks like...",
"But everybody else can see for themselves.",
"Do you know what I have in mind?",
"Well?",
"I think we could do a better tetris than this room.",
"What do you mean, better?",
"At least, you won’t have to move all the pieces to the right.",
"The truth is we have an ample supply of suitable objects here.",
"I think a little patch of code could give the player the possibility to enjoy some original tetris.",
"Okay, try to program it!",
"Be careful not to hurt us.",
"We will have to stack these pieces more efficiently, perhaps.",
"Hmm... I should have arranged it better.",
}
