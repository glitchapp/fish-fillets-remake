l1={"Co to było?",
"Nie mam pojęcia.",
"Powinniśmy chyba wyjść i się rozejrzeć.",
"Czekaj, płynę z tobą.",
"Czemu nic się nie dzieje?",
"Hej, graczu!",
"Możesz nas kontrolować strzałkami, a przełączać między nami używając spacji.",
"Albo możesz używać myszki - kilkasz lewym przyciskiem i...",
"...jeżeli możemy tam swobodnie dopłynąć...",
"...to tam popłyniemy.",
"A gdy klikniesz gdzieś prawym przyciskiem, popłyniemy tam spychając z drogi wszelkie przeszkody.",
"Twoim celem jest wydostanie nas obojga z pomieszczenia.",
"Ale uważaj, żeby nie zrobić nam krzywdy. Jeśli coś na nas upuścisz, albo...",
"No, już wystarczy. Powiemy mu w następnym poziomie.",
"No dobra.",


"Pies to trącał, nie mogę przepłynąć.",
"Chyba trochę przytyłem.",
"Mogłabyś popchnąć to krzesło trochę w lewo?",
"To był błąd. Aby ukończyć poziom, oboje musimy opuścić pomieszczenie.",
"Ja już się stąd nie wydostanę. Musisz zrestartować poziom.",
"Naciśnij ten klawisz z długą strzałką.",
"Ludzie mówią na to backspace.",
"Wszystko jedno.",
"Opcja restartu znajduje się także na panelu kontrolnym.",
"Jeśli wolisz zrestartować poziom, nie krępuj się.",
"Po prostu naciśnij ten klawisz z długą strzałką.",
"Jestem ciężką, stalową rurą. Niełatwo mnie przesunąć. Taka mała rybka jak ta pomarańczowa nie ma najmniejszych szans. Mogłabym ją zmiażdżyć bez mrugnięcia okiem.",
}

l1icantgetthrough={"Tędy się nie przecisnę. Ta stalowa rura blokuje mi drogę.",
"Nie dam rady przesunąć tej rury. Pomożesz mi?",
"Nie ma sprawy...",
}
l1wowyoumovedit={"O, przesunąłeś! Ja bym tak nigdy nie mogła.",
"Dzięki, teraz już mogę płynąć z tobą.",
}

l1damnit={"Darn it, I can’t get through.",
"I must have gained some weight.",
"Could you push the chair a little bit to the left?"
}
