l54={"Wydaje mi się, że możemy tu uzyskać informacje o napędzie międzygwiezdnym.",
"To mi raczej wygląda na konwencjonalny napęd lądownika.",
"No to znaleźliśmy silnik. Jeden z naszych celów został osiągnięty.",
"Nie bądź taka do przodu. Nie przeszukaliśmy jeszcze całego wraku.",
"Szkoda, że te wszystkie cuda techniki już nie działają.",
"Chciałabym zobaczyć wszystkie te pozaziemskie wynalazki w akcji.",
"Ciekawe, czy ten napęd działa pod wodą.",
"Moze lepiej byłoby dla nas, gdyby nie działał.",
"Ja tam się cieszę, że niczego tu nie możemy włączyć."
}
l54engineon={"Cóżeś narobił? Wyłącz ten ryk!",
"To jest straszne! Wyłącz to, póki nie jest za późno.",
"Nie mogę! Jak mam to wyciągnąć?!",
"Nie wiem jak! Nie da się tego wyciągnąć!",
"Mayday! Mayday!",
"Jak to wyłaczyć?!",
"Nie mogę tego wyłączyć!"
}

l54engineonfish1={"Nie mogę! Jak mam to wyciągnąć?!",
"Nie wiem jak! Nie da się tego wyciągnąć!",
"Jak to wyłaczyć?!",
"Nie mogę tego wyłączyć!",
"Mayday! Mayday!",
"Jak to wyłaczyć?!"
}
--"I can’t turn it off!"
l54engineoff={"Finally.",
"W końcu.",
"Dzięki.",
"Wreszcie.",
"Obawiam się, że będziemy musieli znowu to włączyć.",
""
}

l54aproachengine={"Powinniśmy się cieszyć, że niczego tu nie mozna uruchomić.",
"Ostrożnie z tym kluczem.",
"Niczego tu przecież nie uszkodzę."
}

