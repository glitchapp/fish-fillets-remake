l18={"Znów ten upiorny statek.",
"Widzisz? Coś się zmieniło, gdy nas nie było. Mówię ci, ktoś nas obserwuje.",
"Czemu się tak na nas patrzysz? Nie widzisz, że nas to stresuje?",
"Coś mi się zdaje, że ich impreza dobiega końca.",
"No to po imprezie.",
"Tyle im zostało z tej ich imprezy.",
"Coś denna ta impreza.",
"Zdaje się, że ci imprezowicze są już nieco zmęczeni.",
"Z pewnością mają już dość.",
"Chyba już nawet nie mają siły nas skrzywdzić.",
"Myślę, że są raczej nieszkodliwi.",
"To nie będzie takie łatwe, jak ostatnim razem.",
"Zdaje się, że tym razem będzie nieco trudniej."
}
