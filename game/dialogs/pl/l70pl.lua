l70={"It looks as if we finally got there.",
"Do you think this is it?",
"How could it all fit on one three and a half inch floppy?",
"Look how much bigger than me it is. It is a three and a half meter floppy.",
"That’s better. Big plan, big disk. That makes sense.",
"Do you think we can lift it up?",
"It will be difficult.",
"I don’t think it can be done. Why would they be hiding it here, if we could lift it?",
"Shut up, or I will break off your pins!",
"Can you see that steel cross? This is the grave of our hopes.",
"Don’t be always so gloomy. I can’t bear it.",
"Wouldn’t it be enough to download that plan from the floppy to our notebook?",
"Why not. Take it out.",
"Oh, darn! I left it at home.",
"Shall we return for it?",
"Never. It may seem funny to you, but my fins are sore.",
"Suit yourself. I am curious how are you going to do it now.",
"What if there is no plan on this floppy? What if it is here only to confound us?",
"I don’t believe that. Is there some other object that would be more difficult to get out?",
"What kind of little creatures are those?",
"These must be computer viruses.",
"Yuck. Some monsters.",
"Above all, we must be careful not to take them with the disk. The player wouldn’t be happy if we pour them on their desktop.",
"Our goal is to push out that disk.",
"You won’t move me easily!",
"That was smart!",
"Ooops!",
"Darn!",
"Oh.",
"Ouch.",
"Yup.",
"Leave me alone!",
"1060D0A2A050B508C34D590923763F6B5B466C0E6033A9",
"033455D394F938E624A68CBCA301923784F35892",
"1A566837C0323B0413239DD2374E9F0036C56B23074",
}

l70end={"THE GREATEST EARTHQUAKE SINCE 1990",
"when Dragon’s Lair was issued, Rules for beginners, version 1.0.",
"DRAGON’S LAIR PLUS",
"The world of role-playing games will soon change forever.",
"Dragon’s Lair Plus is not only a new version.",
"It’s a new game.",
"Plenty of new possibilities.",
"New occupations.",
"New system of character creation, where,",
"instead of your lives, your skills grow. New elegant mechanism of playing.",
"A game for 21. century.",
"GAMES HAVE CHANGED",
"This is what we get from the computer after inserting the waterproof diskette that was found during our investigation of secret criminal organisation.",
"We are shocked. Do not let journalists know it.",
"You must prevent the panic.",
"And subscribe two copies for us.",
}
