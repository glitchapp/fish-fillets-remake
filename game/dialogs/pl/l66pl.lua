l66={"So this is a level for real connoisseurs.",
"Yeah, I feel like I can feel the eight bit wind of history.",
"Can you see that pixel graphic all around us? I must confess I feel nostalgic.",
"This is an opportunity to play the merry old games of ZX Spectrum.",
"What are you thinking about now?",
"Well, I just realized that the animations of us two alone would fill up the RAM of a ZX Spectrum.",
"Do you know that your question alone in WAV format would overfill the RAM of ZX Spectrum? And imagine how many such sounds are in this game.",
"But still, these games had something to them, didn’t they?",
"Can you see that Knight Lorer?",
"And that must be the Manic Miner.",
"I really didn’t know I’d ever be putting the Jetpack together again.",
"This line of robots looks familiar.",
"Oh sure, they are from the Highway Encounter.",
"I wonder what is loading right now.",
"The single biggest advantage of this room is that it contains no steel.",
}
