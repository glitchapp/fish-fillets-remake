l2={"O nie, tylko nie to...",
"Nic nie poradzisz.",
"Zepchnijmy to i przyjrzyjmy się.",
"Dobra, bierzmy się do roboty.",
"No, to możemy zaczynać.",
}

l2thediskself={
"Jak? Dysk wybuchł.",
"Naszym pierwszym zadaniem jest wydostanie się z tego pomieszczenia.",
"To będzie też naszym zadaniem we wszystkich innych pomieszczeniach.",
"Chodź, weźmiemy gracza do warsztatu i pokażemy mu zasady BHP.",
"Dobry pomysł.",
"Na razie nic nie rób, tylko patrz i ucz się. Pokażemy ci, co można, a czego nie należy z nami robić.",
"Zanim wpłyniemy do warsztatu, zapiszmy grę - naciśnij klawisz F2.",
"Najpierw pokażemy ci, co nam może zrobić krzywdę.",
"Ja mogę być królikiem doświadczalnym.",
"Przede wszystkim nie powinniśmy zrzucać na siebie przedmiotów.",
"Nie powinniśmy też płynąć w dół, gdy niesiemy coś na grzbiecie. Nie chcemy przecież, żeby na nas spadło.",
"Kolejną rzeczą, której nie powinniśmy robić, to pchanie przedmiotów po naszych grzbietach.",
"Niektóre przedmioty mają taki kształt, że możemy je jednocześnie trzymać na grzbiecie i popychać... ale tego też nie powinniśmy robić.",
"Nie możemy też zrzucać dodatkowych przedmiotów na te, które jedno z nas niesie.",
"A ja, w przeciwieństwie do kolegi Dużego, nie mogę popychać ani dźwigać rzeczy ze stali.",
"Ostatni raz wczytujemy grę. Teraz pokażemy ci, co potrafimy robić.",
"Jedno z nas może popychać przedmioty po grzbiecie drugiego, ale pod warunkiem, że tym ruchem przedmiot zostanie wepchnięty na jakąś podporę.",
"... a nawet możemy w takim przypadku zrzucić coś na siebie.",
"Mogę nawet zrobić to sama... mogę popłynąć pod tym w dół... albo postawić to na tej półce.",
"Możemy także popychać przedmiot po powierzchni innego przedmiotu, który znajduje się na grzbiecie jednego z nas... ale tego spoczywającego bezpośrednio na grzbiecie nie możemy poruszyć.",
"Możemy również swobodnie poruszać się pod przedmiotem, który niesiemy, a także przekazać go drugiej rybce.",
"I możemy spychać przedmioty z grzbietu drugiego, pod warunkiem, że od razu pójdą na dno.",
"To by było na tyle w kwestii zasad. Więcej przydatnych informacji znajdziesz pod klawiszem F1.",
"Podsumowując: możemy podnosić przedmioty, upuszczać je, popychać po trwałym podłożu, wpychać je na stabilne podłoże i spychać je z siebie nawzajem.",
}

l2wecouldrestart={"Moglibyśmy zrestartować poziom.",
"To prawda...",
}

l2icantliftit={"Nie udźwignę tego. Pomożesz?",
"Ciutkę wyżej, żebym mogła pod toba przepłynąć.",
}

l2lethisbytheway={
"Obejrzymy jeszcze raz?",
}

l2nowell={"Zacznijmy więc od początku... lub wczytajmy zapisaną grę wciskając klawisz F3.",
"No i jesteśmy tu, gdzie zapisywaliśmy grę.",
}

l2again={"Wczytajmy ponownie grę przy pomocy klawisza F3.",
}

l2wehaveloaded={"Możemy na przykład podnosić i upuszczać przedmioty.",
}

l2briefcasetalkie={"Dzień dobry, ryby. To jest sprawa wielkiej wagi, więc wybraliśmy was,",
"naszych najzdolniejszych podwodnych agentów.",
"Informatorom FDTO - Faktycznie Działającej Tajnej Organizacji - ",
"udało się przechwycić amatorskie zdjęcia obiektu pozaziemskiego,",
"który rozbił się w waszej okolicy. Waszym zadaniem, jeśli się go podejmiecie,",
"będzie odnalezienie UFO i pozyskanie wszelkich możliwych informacji",
"o zasadach funkcjonowania i konstrukcji napędu międzygwiezdnego.",
"Przy okazji możecie spróbować rozwiązać kilka z naszych starych spraw.",
"Jesteśmy żywo zainteresowani okolicznościami zatonięcia mitycznego miasta,",
"zbiegiem okoliczności w tej samej okolicy.",
"Sprawa ta może być związana z tajemnica Trójkąta Bermudzkiego.",
"Dowiedzcie się, dlaczego tak wiele statków",
"i samolotów przez wieki zniknęło na tym obszarze.",
"Wieść gminna niesie, że jeden z zaginionych",
"statków należał do legendarnego kapitana Silvera.",
"Postać tego pirata wciąż owiana jest mgiełką tajemnicy.",
"Najbardziej interesuje nas mapa miejsca zakopania jego skarbu.",
"Jednym z najważniejszych zadań stojących przed wami jest odnalezienie komputera,",
"ukrytego w głębinach przez pewną organizację terrorystyczną.",
"Zawiera on dane o projekcie, który może zmienić oblicze Ziemi.",
"Powinniście również odnaleźć i pojmać tjemniczego żółwia koralowego,",
"który zbiegł z pokoju przesłuchań FDTO.",
"Nie jest uzbrojony, ale przypuszczalnie ma zdolności telepatyczne.",
"Doniesiono nam, że pewna elektrownia atomowa składuje odpady radioaktywne na dnie oceanu.",
"Sprawdźcie to.",
"I nie zapomnijcie odszukać Świętego Graala.",
"I jak zawsze: jeśli ktoś z waszej drużyny zginie lub zostanie ranny,",
"Altar wyprze się jakichkolwiek z wami powiązań i poziom zostanie zrestartowany.",
"Ten dysk ulegnie samozniszczeniu w ciągu pięciu sekund.",
}
