l3={"Zasady już znasz. ",
"Teraz możesz je wypróbować w praktyce.",
"Będziemy ci trochę podpowiadać.",
"Nie mogę połynąć w dół. ",
"Do góry też nie mogę.",
"Hej, poukładaj to jakoś na górze.",
" Tylko uważaj!"
}

l3youarestandinginmyway={"Jeśli to upuścisz, a ja tu zostanę, nigdy się stąd nie wydostanę.",
"Przeszkadzasz mi tam, spróbuj schować się gdzie indziej.",
"Tutaj? Trochę się boję. Jesteś pewien, że będę mogła stąd wypłynąć?",
"Siedź i patrz."}

l3wewillgiveyouahint={"Tak, aby spadła na nią ta stalowa rurka, gdy ją popchnę.",
"Ale jak to zrobić, tego już ci nie powiemy.",
"Pamiętaj tylko, że nie możemy wprawdzie popychać przedmiotów po swoich grzebietach, ale możemy je z nich spychać w dół albo na jakąś stałą powierzchnię.",
"A może górą?",
"Ćśśś, nie podpowiadaj tyle."}


--[[
"Nie mogę tego poruszyć, to stal.",
"Wracaj, dalej nie możesz!",
"Hmmm... tu mamy zagwozdkę. Jeśli popchniemy miód, zmiażdży nas ten słoik konfitury morelowej.",
"Może moglibyśmy popchnąć go tym toporkiem?",
"Dobry pomysł.",
"Teraz nie mogę podnieść toporka, dżem mi przeszkadza.",
"Nie popychaj tej siekiery!",
"Jeśli podepchniesz mi ten słoik, oprę na nim toporek i wszystko będzie dobrze.",
"Po rozwiązaniu jakiegoś problemu możesz zapisać stan gry.",
"Ale tylko jeśli jesteś pewien swojego rozwiązania.",
"Pamiętaj, co mówiliśmy na temat zapisywania gry.",
"Nie powinnam nawet o tym myśleć.",
"Nie wygląda to najlepiej. Chyba będzie trzeba zrestartować poziom.",
"Jeśli to upuścisz, a ja tu zostanę, nigdy się stąd nie wydostanę.",
"Przeszkadzasz mi tam, spróbuj schować się gdzie indziej.",
"Tutaj? Trochę się boję. Jesteś pewien, że będę mogła stąd wypłynąć?",
"Siedź i patrz.",
"Tu ci, graczu, podpowiemy - tę ksiązkę należy włożyć tam, po lewej.",
"Tak, aby spadła na nią ta stalowa rurka, gdy ją popchnę.",
"Ale jak to zrobić, tego już ci nie powiemy.",
"Pamiętaj tylko, że nie możemy wprawdzie popychać przedmiotów po swoich grzebietach, ale możemy je z nich spychać w dół albo na jakąś stałą powierzchnię.",
"A może górą?",
"Ćśśś, nie podpowiadaj tyle.",
"Tak bywa.",
"Czasem musisz myśleć perspektywicznie.",
"I wtedy wychodzi na to, że musisz zrestartować poziom.",
"Przyznajemy, że zrobiliśmy to celowo.",
"Abyś mógł przećwiczyć wszystko jeszcze raz - tym razem już bez podpowiedzi.",
"",
--]]
