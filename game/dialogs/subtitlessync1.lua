--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

function loadcorrespondentfonts()
    local fontPaths = {
        de = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        en = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        fr = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        es = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        nl = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        pl = "externalassets/fonts/PoorFish/PoorFish-Regular.otf",
        ru = "externalassets/fonts/alanesiana/AlanesianaRegular.ttf",
        sv = "externalassets/fonts/alanesiana/AlanesianaRegular.ttf",
        sl = "externalassets/fonts/alanesiana/AlanesianaRegular.ttf",
        chi = "externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf",
        gr = "externalassets/fonts/pecita/Pecita.otf",
        jp = "externalassets/fonts/MT_Tare/MT_TARE.ttf",
        thai = "externalassets/fonts/thsarabun-new/THSarabunNew001.ttf"
    }
    local defaultFont = "externalassets/fonts/default_font.ttf"

    local fontPath = fontPaths[language]
    Talkies.font = fontPath and love.graphics.newFont(fontPath, 32) or love.graphics.newFont(defaultFont, 32)
    resizetalkiesfonts()
end

--[[
function playSubtitle(subtitle)
    if timer > subtitle.timer and stepdone == subtitle.step then
        Talkies.onAction()
        subtitle.sound:play()
        fish1talk()
        stepdone = stepdone + 1
    end
end

function subtitlesScript()
    for _, subtitle in ipairs(subtitles) do
        playSubtitle(subtitle)
    end
end
--]]

function subtitlesborescript()
		if timer>1    and stepdone==0 then stepdone=1 Talkies.onAction()    idontbelieve:play()		fish1talk() end		-- "I don’t believe we can make it…"
		if timer >4.5 and stepdone==1 then stepdone=2 Talkies.onAction() 	wearent:play()		 	fish2talk() end		-- "We aren’t going to get out of here.",
		if timer >6.5 and stepdone==2 then stepdone=3 Talkies.onAction()	weareshut:play()		fish2talk() end		-- "We are shut in here forever!",
		if timer >8.5 and stepdone==3 then stepdone=4 Talkies.onAction()	theremustbe:play() 		fish1talk() end		-- "There must be a way out!",
		if timer >11 and stepdone==4  then stepdone=5 Talkies.onAction() 	thereis:play()			fish1talk() end		-- "There is a solution, just we cannot see it.",
		if timer >14 and stepdone==5  then stepdone=6 Talkies.onAction() 	chinup:play() 			fish1talk() end		-- "Chin up player, there must be a way."
		if timer >17 and stepdone==6  then stepdone=7 Talkies.onAction() 	takeiteasy:play()		fish1talk() end		-- "Take it easy."	,
		if timer >21 and stepdone==7  then stepdone=8 Talkies.onAction() 	theresomething:play() 	fish1talk() end		-- "There is something strange about it…",
		if timer >26 and stepdone==8  then stepdone=9 Talkies.onAction() 	justspareme:play()		fish2talk() end		-- "Just spare me your theories, please.",
		if timer >29 and stepdone==9  then stepdone=10 Talkies.onAction()	ivegotit:play() 		fish1talk() end		-- "I’ve got it. If we push that one…"	,
		if timer >31 and stepdone==10 then stepdone=11 Talkies.onAction()	goon:play()		 		fish2talk() end		-- "Go on!",
		if timer >37 and stepdone==11 then stepdone=12 Talkies.onAction() 	ohnoitwas:play()		fish1talk() end		-- "Oh no, it was just an idea…",
		if timer >40 and stepdone==12 then stepdone=13 Talkies.onAction() 	whatifwetrythat:play()	fish1talk() end		-- "What if we try that…",
		if timer >44 and stepdone==13 then stepdone=14 Talkies.onAction() 	what:play()		 		fish2talk() end		-- "What?",
		if timer >47 and stepdone==14 then stepdone=15 Talkies.onAction() 	nothingjustanidea:play() fish1talk() end	-- "Nothing. Just an idea.",
		if timer>70   and stepdone==15 then stepdone=16 Talkies.onAction() 	notagain:play()			fish2talk() end		-- "Not again.",
		if timer>78   and stepdone==16 then stepdone=17 Talkies.onAction() imtired:play()			fish1talk() end		-- "I’m tired of this.",
		if timer>84   and stepdone==17 then stepdone=18 Talkies.onAction() justplay:play()			fish1talk() end		-- "Just play, we want to get out.",
		if timer>88   and stepdone==18 then stepdone=19 Talkies.onAction() iftheplayer:play()		fish1talk() end		-- "If the player keeps doing nothing, we’ll have to solve it ourselves.",
		if timer>91   and stepdone==19 then stepdone=20 Talkies.onAction() shhhdont:play()			fish2talk() end		-- "Shhh, don’t disturb the player. He or she might be thinking again…",
		if timer>96   and stepdone==20 then stepdone=21 Talkies.onAction() 	keepcalm:play()			fish2talk() end		-- "Keep calm.",
		if timer>102   and stepdone==21 then stepdone=22 Talkies.onAction() takeiteasy:play()		fish2talk() end		-- "Take it easy.",
		if timer>110   and stepdone==22 then stepdone=23 Talkies.onAction()	willtherebe:play()		fish2talk() end		-- "Will there be some action soon?",
		if timer>115   and stepdone==23 then stepdone=24 Talkies.onAction()	whyarentwe:play()		fish2talk() end		-- "Why aren’t we doing anything?",
		if timer >130 	and stepdone==24 then stepdone=25 Talkies.onAction() whyareweidle:play()	fish2talk() end		-- "Why are we idle?",
		if timer >135 	and stepdone==25 then stepdone=26 Talkies.onAction() doesheexpect:play()	fish2talk() end		-- "Does he expect us to solve this on our own?",
		if timer >140 	and stepdone==26 then stepdone=27 Talkies.onAction() dontbedisruptive:play() fish1talk() end		-- "Don’t be disruptive. The player is thinking again…",
		if timer >145 	and stepdone==27 then stepdone=28 Talkies.onAction() isthereaplayer:play() fish2talk() end		-- "Is there a player there at all?",
		if timer >150 	and stepdone==28 then stepdone=29 Talkies.onAction() idontknow:play()		fish1talk() end		-- "I don’t know. The screen glares too much from inside.",
		if timer >155 	and stepdone==29 then stepdone=30 Talkies.onAction() thislittlebreak:play()	fish1talk() end		-- "This little break will help.",
		if timer >160 	and stepdone==30 then stepdone=31 Talkies.onAction() wearetired2:play()		 fish2talk() end		-- "We are tired of waiting!",
		if timer >165 	and stepdone==31 then stepdone=32 Talkies.onAction() hello2:play()			 fish2talk() end		-- "Hello!",
		if timer >170 	and stepdone==32 then stepdone=33 Talkies.onAction() didyouflinch:play()	fish1talk() end		-- "Did you flinch?",
		if timer >175 	and stepdone==33 then stepdone=34 Talkies.onAction() wewerejust:play() 		fish2talk() end		-- "We were just afraid that you might have forgotten about us.", 
		if timer >180 	and stepdone==34 then stepdone=35 Talkies.onAction() look:play()			fish1talk() end		-- "Look, why don’t you move us a little bit?",
		if timer >185 	and stepdone==35 then stepdone=36 Talkies.onAction() wearethegame:play()	fish2talk() end		-- "We are the game, you are supposed to solve the puzzle!"
		if timer >190 	and stepdone==36 then stepdone=37 Talkies.onAction() buyyourself:play()		fish2talk() end		-- "Buy yourself a fish tank for observations."
		if timer >195 	and stepdone==37 then stepdone=38 Talkies.onAction() hellowearehere:play()	fish1talk() end		-- "Hellooo! We are here! Did you forget about us?"
		if timer >200 	and stepdone==38 then stepdone=39 Talkies.onAction() difficultisntit:play()	fish2talk() end		-- "Difficult, isn’t it?"
		if timer >205 	and stepdone==39 then stepdone=40 Talkies.onAction() lookplayer:play()		fish1talk() end		-- "Look player, couldn’t you bring us something to eat? Put it in the TEMP directory, we’ll eat it immediately."
		if timer >210 	and stepdone==40 then stepdone=41 Talkies.onAction() whenyoustopplaying:play()	fish2talk() end		-- "When you stop playing, don’t turn off the computer, please. It’s so dark and scary when you do."
		if timer >215 	and stepdone==41 then stepdone=42 Talkies.onAction() howlongare:play()		fish2talk() end		-- 	"How long are we going to take to solve it?"	
		if timer >220 	and stepdone==42 then stepdone=43 Talkies.onAction() aslongas:play()		fish1talk() end		-- 	"As long as this chewing gum tastes good."	
		if timer >225 	and stepdone==43 then stepdone=44 Talkies.onAction() whatyouarefinished:play() fish2talk() end		-- 	"What, you are finished? But this chewing gum still tastes extraordinary."	
		if timer >230 	and stepdone==44 then stepdone=45 Talkies.onAction() whatdoyouthink:play()	fish1talk() end		-- 	"What do you think, how are the others faring?"
		if timer >235 	and stepdone==45 then stepdone=46 Talkies.onAction() theotherswho:play()	fish2talk() end		-- "The other who?"
		if timer >240 	and stepdone==46 then stepdone=47 Talkies.onAction() theotherplayers:play()	fish1talk() end		-- "The other players who are solving this game. Maybe they have managed to solve more levels."	
		if timer >245 	and stepdone==47 then stepdone=48 Talkies.onAction() 	itsverylikely:play() fish2talk() end		-- "It’s very likely."
		if timer >250 	and stepdone==48 then stepdone=49 Talkies.onAction() ohyeah:play()			fish2talk() end		-- 	"Oh, yeah."
		if timer >255 	and stepdone==49 then stepdone=50 Talkies.onAction() whathappened:play()	fish1talk() end		-- 	"What happened?"
		if timer >260 	and stepdone==50 then stepdone=51 Talkies.onAction() ijustfeelsosorry:play() fish2talk() end		-- "I just feel so sorry…"
		if timer >265 	and stepdone==51 then stepdone=52 Talkies.onAction() sorryforwhat:play()	fish1talk() end		-- "Sorry for what?"	
		if timer >270 	and stepdone==52 then stepdone=53 Talkies.onAction() sorryforeverything:play() fish2talk() end		-- "Sorry for everything."
		if timer >275 	and stepdone==53 then stepdone=54 Talkies.onAction() whateverything:play()	fish1talk() end		-- "What everything?"
		if timer >280 	and stepdone==54 then stepdone=55 Talkies.onAction() completelyeverything:play() fish2talk() end		-- "Completely everything! And leave me alone, I beg you!"
	
end

function subtitlegameoverdarn()
		if timer>1    and stepdone==0 then stepdone=1 Talkies.onAction()    darngo:play()			fish1talk() end		-- "Darn."
		if timer >4.5 and stepdone==1 then stepdone=2 Talkies.onAction() 	shhchildren:play()		fish2talk() end		-- "Shhh, children can be playing this game!"
end

function subtitlesgameoverparrot()
		if timer>1    and stepdone==0 then stepdone=1 Talkies.onAction()    aycarambago:play()			fish1talk() end		-- "Aye, caramba!"
		if timer >4.5 and stepdone==1 then stepdone=2 Talkies.onAction() 	buthereisnoparrot:play()	fish2talk() end		-- "But there is no parrot here!"
		if timer >6.5 and stepdone==2 then stepdone=3 Talkies.onAction()	shutupyouaredead:play()		fish2talk() end		-- "Shut up, you are dead."
end

function subtitlegameoverhereafter()
		if timer>1    and stepdone==0 then stepdone=1 Talkies.onAction()    canyouhearmehere:play()			fish1talk() end		-- "Hellooo? Can you hear me hereafter? Is there life after death?"
		if timer >4.5 and stepdone==1 then stepdone=2 Talkies.onAction() 	idontknowidontthinkso:play()	fish2talk() end		-- "I don’t know. I don’t think so."
end


--[[The function subtitles1() appears to control the subtitles and dialogue of a video game or interactive experience.
 The code contains a series of conditional statements that check if a certain amount of time has elapsed since the last line of dialogue was spoken (timer).
 If the timer has reached a certain value and the previous step has been completed (stepdone),
 the function advances to the next step and triggers an audio clip (Talkies.onAction())
 and displays the appropriate dialogue for the character (fish1talk() or fish2talk()) with the appropriate lip sync animation (lipsline and lipstimer).
 The dialogue seems to involve characters discussing the player's ability to control them and the player's goal in the game.
 The function ends by calling fishstop() when the final line of dialogue has been displayed.--]]

--]]


function subtitles1()
	
--level 1
		if timer>1    and stepdone==0 then stepdone=1 Talkies.onAction()    whatwasthat:play()			fish2talk()	lipsline=1 lipstimer=0 end	-- what was that
		if timer >4.5 and stepdone==1 then stepdone=2 Talkies.onAction() 	ihavenoidea:play() 			fish1talk()	lipsline=2 lipstimer=0 end	-- I have no idea
		if timer >6.5 and stepdone==2 then stepdone=3 Talkies.onAction()	weshouldgoandhave:play() 	fish1talk()	lipsline=3 lipstimer=0 end	-- we should go and have a look outside
		if timer >8.5 and stepdone==3 then stepdone=4 Talkies.onAction()	waitimgoingwithyou:play()	fish2talk()	lipsline=4 lipstimer=0 end	-- wait! I'm going with you
		if timer >11 and stepdone==4  then stepdone=5 Talkies.onAction() 	whyisntanything:play()		fish2talk()	lipsline=5 lipstimer=0 end	-- why isn't anything happening?
		if timer >14 and stepdone==5  then stepdone=6 Talkies.onAction() 	heiplayer:play() 			fish2talk()	end-- Hei! Player!
		if timer >17 and stepdone==6  then stepdone=7 Talkies.onAction() 	youcancontrolus:play()		fish1talk()	end-- you can control us
		if timer >21 and stepdone==7  then stepdone=8 Talkies.onAction() 	oryoucancontrol:play() 		fish2talk()	end-- or you can control us with the mouse
		if timer >26 and stepdone==8  then stepdone=9 Talkies.onAction() 	ifwecangetthere:play()		fish1talk()	end-- if we can get there without moving anything
		if timer >29 and stepdone==9  then stepdone=10 Talkies.onAction()	wellfollow:play() 			fish2talk()	end-- we will folow your clicks
		if timer >31 and stepdone==10 then stepdone=11 Talkies.onAction()	andwhenyourightclick:play() fish1talk()	end-- and when you right click somewhere
		if timer >37 and stepdone==11 then stepdone=12 Talkies.onAction() 	yourgoalshouldbe:play()		fish2talk()	end-- your goal should be to get us
		if timer >40 and stepdone==12 then stepdone=13 Talkies.onAction() 	andbecareful:play()			fish1talk()	end-- and be careful not to hurt us
		if timer >44 and stepdone==13 then stepdone=14 Talkies.onAction() 	ohstopit:play() 			fish2talk()	end-- oh stop it!
		if timer >47 and stepdone==14 then stepdone=15 Talkies.onAction() 	okay:play() 				fish1talk()	end-- Okay
		if timer >48 and stepdone==15 then stepdone=16 fishstop() end
end

function subtitles1Icantgetthrough()
--level 1
		if timer>0    	and stepdone==0 then stepdone=1 twotalkiesactions() icantgetthrough:play() 	fish2talk()	end		-- 	"I can’t get through here. That steel cylinder is in the way.",
		if timer >3.5	and stepdone==1 then stepdone=2 Talkies.onAction() 	icantmovethatcylinder:play() 					fish2talk()	end		-- "I can’t move that cylinder. Can you help me?",
		if timer >7.5   and stepdone==2 then stepdone=3 Talkies.onAction()	noproblem:play() 								fish2talk()	end		-- "No problem...",
		if timer >8.5   and stepdone==3  then stepdone=4 Talkies.onAction()	icantgetsaidonce=true end	
end

function subtitles1wowyoumovedit()
--level 1
		if timer>0    	and stepdone==0 then stepdone=1 twotalkiesactions()	wowyoumovedit:play() 					fish2talk()		end		-- "Wow, you moved it! I could never do that!",	
		if timer >3.5	and stepdone==1 then stepdone=2 Talkies.onAction() 	thanksnowicango:play() 					fish2talk()		end		-- "Thanks, now I can go with you.",
		if timer >7.5   and stepdone==2 then stepdone=3 Talkies.onAction()	wowsaidonce=true	 					fishstop()		end		--
end

function subtitles1damnit()
--level 1
		if timer>0    	and stepdone==0 then stepdone=1 twotalkiesactions()		  	damnit:play()		fish1talk()			end		-- "Darn it, I can’t get through.",
		if timer >3.5	and stepdone==1 then stepdone=2 Talkies.onAction() 						 							end		-- "I must have gained some weight.",
		if timer >7.5   and stepdone==2 then stepdone=3 Talkies.onAction()						 							end		-- "Could you push the chair a little bit to the left?",
end

function subtitlessync2()
		--level 2
		if timer>0    and stepdone==0 then stepdone=1 					 	ohnonotagain:play()	fish2talk()	end	-- oh no not again
		if timer >3	  and stepdone==1 then stepdone=2 Talkies.onAction() 	youknow:play()		fish1talk()	end	-- you know there's nothing
		if timer >6   and stepdone==2 then stepdone=3 Talkies.onAction()	letspushitdown:play() fish1talk()	end	-- let's push it down
		if timer >9.5 and stepdone==3 then stepdone=4 Talkies.onAction() 	wellletsgettowork:play() fish1talk() end	-- let's get to work
		if timer >11.5 and stepdone==4 then stepdone=5 Talkies.onAction()  	end	-- 
end

function subtitle2FDTOmessage()
	
		--Briefcase message
		if timer>0    and stepdone==0 and goodmorningsaidonce==false then stepdone=1 	goodmorning:play()		end	-- "Good morning, fish.",
		if timer>2    and stepdone==1 then stepdone=2 Talkies.onAction() 	thisisanaffair:play()	end	-- "This is an affair of the gravest importance and so we have chosen you,",
		if timer>6    and stepdone==2 then stepdone=3 Talkies.onAction() 	ourablest:play()		end	-- "our ablest underwater agents.",
		if timer>9    and stepdone==3 then stepdone=4 Talkies.onAction() 	agentsoffdto:play()		end	-- "Agents of FDTO - Fish Detective Training Organization -",
		if timer>13.5    and stepdone==4 then stepdone=5 Talkies.onAction() managed:play()			end	-- "managed to get hold of several amateur snapshots of an extraterrestrial object",
		if timer>18.5   and stepdone==5 then stepdone=6 Talkies.onAction() 	whichhascrashed:play()	end	-- "which has crashed somewhere in your vicinity.",
		if timer>22   and stepdone==6 then stepdone=7 Talkies.onAction() 	yourmission:play()		end	-- "Your mission if you choose to accept it,",
		if timer>24.5   and stepdone==7 then stepdone=8 Talkies.onAction() 	willbe:play()			end	-- "will be to recover the UFO and acquire all the information you can about the principles and nature of interstellar propulsion.",
		if timer>33   and stepdone==8 then stepdone=9 Talkies.onAction() 	youshouldalsotry:play()	end	-- "You should also try to close some of our as yet unsolved cases.",
		if timer>37   and stepdone==9 then stepdone=10 Talkies.onAction() 	wearemostly:play()		end	-- "We are mostly interested in the circumstances surrounding the sinking of the mythical city, by chance in the same area.",
		if timer>44   and stepdone==10 then stepdone=11 Talkies.onAction() thiscase:play()			end	-- "This case may be connected to the mystery of the Bermuda Triangle.",
		if timer>48   and stepdone==11 then stepdone=12 Talkies.onAction() findout:play()			end	-- "Find out why so many ships and planes have disappeared in this area over the past several decades.",
		if timer>54   and stepdone==12 then stepdone=13 Talkies.onAction() oneofthelost:play()		end	-- "One of the lost ships is rumored to have belonged to the legendary Captain Silver.",
		if timer>60   and stepdone==13 then stepdone=14 Talkies.onAction() therearestill:play()	end	-- "There are still many uncertainties surrounding this famous pirate.",
		if timer>64   and stepdone==14 then stepdone=15 Talkies.onAction() mostofall:play()		end	-- "Most of all we are interested in the map which shows the location of his buried treasure.",
		if timer>70   and stepdone==15 then stepdone=16 Talkies.onAction() oneofyour:play()		end	-- "One of your most important tasks is to find the computer hidden in the deep by a certain criminal organization.",
		if timer>78   and stepdone==16 then stepdone=17 Talkies.onAction() itcontains:play()		end	-- "It contains data about a project which could change the entire world.",
		if timer>84   and stepdone==17 then stepdone=18 Talkies.onAction() youshouldalso:play()	end	-- "You should also find and detain the mysterious coral turtle.",
		if timer>88   and stepdone==18 then stepdone=19 Talkies.onAction() itescaped:play()		end	-- "It escaped from the FDTO interrogation facility.",
		if timer>91   and stepdone==19 then stepdone=20 Talkies.onAction() itisnotarmed:play()		end	-- "It is not armed, but it supposedly has telepathic abilities.",
		if timer>96   and stepdone==20 then stepdone=21 Talkies.onAction() wehavebeen:play()		end	-- "We have been notified that a certain nuclear power plant has been dumping its radioactive waste illegally.",
		if timer>102   and stepdone==21 then stepdone=22 Talkies.onAction() checkitout:play()		end	-- "Check it out. And don’t forget to find the holy grail.",
		if timer>110   and stepdone==22 then stepdone=23 Talkies.onAction()	andasusual:play()		end	-- "And as usual: If anyone from your team is injured or killed,",
		if timer>115   and stepdone==23 then stepdone=24 Talkies.onAction()	altar:play()			end	-- "Altar will deny any knowledge of your existence and the level will be restarted.",
		if timer>120   and stepdone==24 then stepdone=25 Talkies.onAction() thisdisk:play()			end	-- "This disk will self-destruct in five seconds.",
		--if timer>83   and stepdone==25 then stepdone=26 Talkies.onAction() five			end	-- 
		

end

function subtitle2thedisk()

	--level 2 the disk self desctructed
	
		if timer >11 and stepdone==0 then stepdone=1 Talkies.onAction() 	how:play()			fish2talk()	end		-- 		how? the disk self destructed
		if timer >17 and stepdone==1 then stepdone=2 Talkies.onAction() 	sonowwecan:play()	fish1talk()	end		--  	so, now we can finally get started
		if timer >21 and stepdone==2 then stepdone=3 Talkies.onAction()  	ourfirsttask:play()	fish2talk()	end		-- 		our first task is to get
		if timer >24 and stepdone==3 then stepdone=4 Talkies.onAction()	thisbytheway:play()		fish1talk()	end		-- 		this by the way
		if timer >30 and stepdone==4 then stepdone=5 Talkies.onAction() 	comeon:play()		fish2talk()	end		-- 		come on, let's take the player
		if timer >35 and stepdone==5 then stepdone=6 Talkies.onAction()  	goodidea:play()		fish1talk()	end		-- 		good idea
		if timer >37 and stepdone==6 then stepdone=7 Talkies.onAction()  	fornow:play()		fish2talk()	end		-- 		for now don't touch anything
		if timer >46 and stepdone==7 then stepdone=8 Talkies.onAction()  	before:play()		fish1talk()	end		-- 		before entering the workshop
		if timer >53 and stepdone==8 then stepdone=9 Talkies.onAction()   first:play()			fish2talk()	end		-- 		first we'll show yoo can hurt us
		if timer >56 and stepdone==9 then stepdone=10 Talkies.onAction()	illvolunteer:play()	fish1talk()	end		-- 		I'll volunteer to be
		if timer >58 and stepdone==10 then stepdone=11 Talkies.onAction() firstofall:play()		fish2talk()	end		-- 	 	first of all we shouldn't
		if timer >61 and stepdone==11 then stepdone=12  Talkies.onAction() wealsocant:play()	fish2talk()	end		-- 	 	We also can’t swim downwards
		if timer >66 and stepdone==12 then stepdone=13  Talkies.onAction()	anotherthing:play()	fish2talk()	end		-- 	 	Another thing
		if timer >70 and stepdone==13 then stepdone=14  Talkies.onAction() someobjects:play()	fish2talk()	end		-- 	 	Some objects are shaped
		if timer >78 and stepdone==14 then stepdone=15  Talkies.onAction() wealsocantdrop:play() fish2talk() end	--  	We also can’t drop additional
		if timer >83 and stepdone==15 then stepdone=16  Talkies.onAction()	andunlike:play()	fish2talk()	end		-- 	 	And unlike my bigger partner,
		if timer >89 and stepdone==16 then stepdone=17  Talkies.onAction()	wecanalways:play()	fish2talk()	end		--  	We can always pick up
		if timer >93 and stepdone==17 then stepdone=18  Talkies.onAction() wecanpush:play()  	fish2talk()	end		--  	We can push an object
		if timer >100 and stepdone==18 then stepdone=19  Talkies.onAction() andeven:play()		fish2talk()	end		--  	and even let it drop on
		if timer >102 and stepdone==19 then stepdone=20  Talkies.onAction() icaneven:play()		fish2talk()	end		--  	I can even do this alone...
		
		if timer >107 and stepdone==20 then stepdone=21  Talkies.onAction() wecan:play()		fish2talk()	end		--  	We can also push one object along
		if timer >112 and stepdone==21 then stepdone=22  Talkies.onAction() wecanalsoswim:play() fish2talk() end	--  	We can also swim freely under
		if timer >123 and stepdone==22 then stepdone=23  Talkies.onAction() andwe:play()		fish2talk()	end		--  	And we can push objects off of
		if timer >129 and stepdone==23 then stepdone=24  Talkies.onAction() insummary:play()	fish2talk()	end		--  	In summary, we can only
		if timer >139 and stepdone==24 then stepdone=25  Talkies.onAction() thatsaboutit:play()	fish1talk()	end		--  	That’s about it for the rules.

end

function subtitle3()

		--level 3 Rehersal in cellar

		if timer >0 	and stepdone==0 then stepdone=1 			 		youknowtherules:play()	fish2talk()		end		-- 		You know the rules already
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	nowyoucanpractice:play() fish2talk()	end		-- 		Now you can practice them
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction() 	wewillgiveyou:play()	fish2talk()		end		-- 		We will give you some hints
		if timer >7		and stepdone==3 then stepdone=4 Talkies.onAction() 	imustnotswimdown:play() fish1talk()		end		-- 		I must not swim down now.
		if timer >9 	and stepdone==4 then stepdone=5 Talkies.onAction() 	andicannotgo:play()		fish1talk()		end		-- 		and I can not go up either
		if timer >11 	and stepdone==5 then stepdone=6 Talkies.onAction() 	heiarrange:play()		fish1talk()		end		-- 		Hey, arrange it up there somehow.
		if timer >15 	and stepdone==6 then stepdone=7 Talkies.onAction() 	butbecareful:play()		fish1talk()		end		-- 		but be carefull!
		if timer >18.5 	and stepdone==7 then stepdone=8 Talkies.onAction()	fishstop()				fishstop()		end		-- 		

end

function subtitle3youarestanding()

		--level 3 Rehersal in cellar

		if timer >0 	and stepdone==0 then stepdone=1 			 		youarestandingm:play()	fish1talk()	end		--	"You are standing in my way there. Try to hide somewhere else.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	ifyoudropthat:play()	fish2talk()	end		--	"If you drop that and I stay here, I will never get out.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	here:play()				fish2talk()	end		--	"Here? I’m little bit afraid. Will I get out?",
		if timer >14	and stepdone==3 then stepdone=4 Talkies.onAction() 	justwaitandsee:play() 	fish1talk()	end		--	"Just wait and see."
		if timer >17 	and stepdone==4 then stepdone=5 Talkies.onAction() 							fishstop()	end		--
		
end

function subtitle3wewillgiveyouahint()

		--level 3 Rehersal in cellar

		if timer >0 	and stepdone==0 then stepdone=1 			 		wewillgiveyouahint:play() 		fish2talk()	end		--	"We will give you a hint here, player. You have to put that book over there, to the left.",
		if timer >7 	and stepdone==1 then stepdone=2 Talkies.onAction() 	sothatwillcatchthesteel:play() fish1talk()	end		--	"So that it will catch that steel cylinder when I push it.",
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction() 	butwewillnottellyou:play()		fish2talk() end		--	"But we will not tell you how to do it.",
		if timer >16	and stepdone==3 then stepdone=4 Talkies.onAction() 	whatifigotheupperway:play() 	fish1talk() end		--	"What if I go the upper way?",
		if timer >19 	and stepdone==4 then stepdone=5 Talkies.onAction() 	shh:play()						fish2talk() end		--	"Shhhh. That’s too many hints."
		
end

function subtitle3aproachaxe()
		--level 3 Rehersal in cellar

		if timer >0 	and stepdone==0 then stepdone=1 			 		thisistricky:play() 		fish1talk() end		--	"Hmm... This is tricky."
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	ifwepush:play()				fish1talk() end		--	"If we push aside the honey, we will be smashed by that can of apricot jam.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() 	couldntwe:play()			fish2talk() end		--	"Couldn’t we push it aside with that axe?",
		if timer >12	and stepdone==3 then stepdone=4 Talkies.onAction() 	thatsagood:play() 			fish1talk() end		--	"That’s a good idea."
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() 	nowicannot:play()			fish2talk() end		--	"Now I cannot take the axe. The can is in the way."
		if timer >18 	and stepdone==5 then stepdone=6 Talkies.onAction() 	youmusnt:play()				fish1talk() end		--	"You mustn’t push the axe!"
		if timer >22 	and stepdone==6 then stepdone=7 Talkies.onAction() 	ifyoucanpush:play()			fish2talk() end		--	"If you can push that can to me, I’ll shove the axe on it and everything will be okay."
		
end

function subtitle3savingpositions()
		--level 3 Rehersal in cellar

		if timer >0 	and stepdone==0 then stepdone=1 			 		ifyousolve:play() 		fish1talk() end		--	"If you solve some specific part you can save the position.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	butonlyif:play()		fish2talk() end		--	"But only if you are sure of your solution.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	remember:play()			fish1talk() end		--	"Remember what we have said about saving positions."
		
end

function subtitle4()

		--level 4 Library flotsam

		if timer >0 	and stepdone==0 then stepdone=1 				 	these:play()			fish1talk() end		-- 		"These shipwrecks grow more and more arrogant.",
		if timer >6.5 	and stepdone==1 then stepdone=2 Talkies.onAction() thesesunken:play()		fish1talk() end		-- 		"These sunken ships really irritate me.",
		if timer >11 	and stepdone==2 then stepdone=3 Talkies.onAction() ohmy:play()				fish1talk() end		-- 		"Oh my, another ship sank. It really makes me itchy.",
		if timer >16	and stepdone==3 then stepdone=4 Talkies.onAction() look1:play()				fish2talk() end		-- 		"Look, it broke our bookcase!",
		if timer >18.5 	and stepdone==4 then stepdone=5 Talkies.onAction() look2:play()				fish2talk() end		-- 		"Look, it smashed our shelves.",
		if timer >21 	and stepdone==5 then stepdone=6 Talkies.onAction() look3:play()				fish2talk() end		-- 		"Look, it invaded our library.",
		if timer >24 	and stepdone==6 then stepdone=7 Talkies.onAction()	see:play()				fish2talk() end		-- 		"See? You shouldn’t have bought so many books.",
		if timer >29 	and stepdone==7 then stepdone=8 Talkies.onAction() youbuy:play()			fish2talk() end		-- 		"You buy every book you can lay your hand on and now you have problems.",
		if timer >34 	and stepdone==8 then stepdone=9 Talkies.onAction()	why:play() 				fish2talk() end		-- 		"Why do you have to have so many of them?",
		if timer >39 	and stepdone==9 then stepdone=10 Talkies.onAction() esp:play() 				fish2talk() end		-- 		"ESP",
		if timer >42 	and stepdone==10 then stepdone=11 Talkies.onAction() life:play() 			fish2talk() end		-- 		"Life after life.",
		if timer >44 	and stepdone==11 then stepdone=12 Talkies.onAction() memories:play() 		fish2talk() end		-- 		"Memories of the future.",
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction() holistic:play() 		fish2talk() end		-- 		"Holistic methods.",
		if timer >49 	and stepdone==13 then stepdone=14 Talkies.onAction() teach:play() 			fish2talk() end		-- 		"Teach yourself telepathy.",
		if timer >52 	and stepdone==14 then stepdone=15 Talkies.onAction() unwilling:play() 		fish2talk() end		-- 		"Unwilling clairvoyant.",
		if timer >55 	and stepdone==15 then stepdone=16 Talkies.onAction() outreaching:play() 	fish2talk() end		-- 		"Outreaching Daniken.",
		if timer >58 	and stepdone==16 then stepdone=17 Talkies.onAction() throwout:play() 		fish1talk() end		-- 		"I’d throw out these, for example.",
		if timer >61 	and stepdone==17 then stepdone=18 Talkies.onAction() thethree:play()		fish1talk() end		-- 		"The Three Little Shrimp",
		if timer >64 	and stepdone==18 then stepdone=19 Talkies.onAction() thegingerbread:play()	fish1talk() end		-- 		"The Gingerbread Submarine",
		if timer >67 	and stepdone==19 then stepdone=20 Talkies.onAction() goldilocks:play() 		fish1talk() end		-- 		"Goldilocks and the Three Lobsters",
		if timer >70 	and stepdone==20 then stepdone=21 Talkies.onAction() sharkerella:play()		fish1talk() end		-- 		"Sharkerella",
		if timer >73 	and stepdone==21 then stepdone=22 Talkies.onAction() slimmy:play()			fish1talk() end		-- 		"Slimy Squid and the Seven Dwarfs",
		if timer >78 	and stepdone==22 then stepdone=23 														end		--
		if timer >82 	and stepdone==23 then stepdone=24 Talkies.onAction() never:play() 			fish2talk() end		-- 		"Never! My lovely fairy-tales.",
		if timer >85 	and stepdone==24 then stepdone=25 Talkies.onAction() icant:play() 			fish1talk() end		-- 		"I can’t hide myself here.",
		if timer >89 	and stepdone==25 then stepdone=26 Talkies.onAction() icantfit:play()		fish1talk() end		-- 		"I can’t fit in here.",
		if timer >92 	and stepdone==26 then stepdone=27 Talkies.onAction() why:play() 			fish2talk() end		-- 		"Why do we have so many books if we cannot pull them out anyway?",
		if timer >100 	and stepdone==27 then stepdone=28 Talkies.onAction() ifyoudid:play()		fish2talk() end		-- 		"If you did some work-outs instead of reading about silly affairs you could fit yourself in.",
		if timer >106 	and stepdone==28 then stepdone=29 Talkies.onAction() itoldyou:play()		fish2talk() end		-- 		"I told you not to buy that Complete Dictionary of the Abnormal.",
		if timer >110 	and stepdone==29 then stepdone=30 Talkies.onAction() letsthink:play()		fish1talk() end		-- 		"Let’s think. We can’t get these books off the shelf.",
		if timer >115 	and stepdone==30 then stepdone=31 Talkies.onAction() there:play()			fish2talk() end		-- 		"There are only two objects here which we can move with any results.",
end

function subtitle5()
		if timer >1 	and stepdone==0 then stepdone=1 					something:play()		fish2talk() end		-- 		"Something fell here again.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() itsome:play()			fish1talk() end		-- 		"It’s some kind of shell. 
		if timer >6 	and stepdone==2 then stepdone=3 Talkies.onAction() weprobably:play()		fish1talk() end		-- 		We’ll probably have to push it upstairs again.",
		if timer >9 	and stepdone==3 then stepdone=4 Talkies.onAction() ican:play()				fish1talk() end		-- 		"I can see it now: ‘Shell Meets Flowerpot’.
		if timer >13 	and stepdone==4 then stepdone=5 Talkies.onAction() thats:play()				fish1talk() end		-- 		That’s almost like an iron and a sewing machine meeting on an operating table.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction() imsorry:play()			fish2talk() end		-- 		"I’m sorry, I got a bit carried away.",
		if timer >21 	and stepdone==6 then stepdone=7 Talkies.onAction() sorry:play()				fish2talk() end		-- 		"Sorry, I was momentarily distracted.",
		if timer >24 	and stepdone==7 then stepdone=8 Talkies.onAction() where:play()				fish2talk() end		-- 		"Where I am going with this shell?",
		if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction() 										end		-- 		
end

function subtitle6()
		if timer >1 	and stepdone==0 then stepdone=1 					itsbeen:play()			fish2talk() end		-- 	"It’s been a long time since we’ve been here...",	
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() 	nobody:play()			fish2talk() end		-- 	"Nobody has cleaned up in here for a long time.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction() 	iknew:play()			fish2talk() end		-- 	"I knew we’d have to clean this room up.",	
		if timer >9 	and stepdone==3 then stepdone=4 Talkies.onAction() 	atleast:play()			fish1talk() end		-- 	"At least we can tidy things up.",	
		if timer >12 	and stepdone==4 then stepdone=5 Talkies.onAction() 	atleast2:play()			fish1talk() end		-- 	"At least things will finally be in order in here.",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction() 	illhave:play()			fish1talk() end		-- 	"I’ll have to do the sweeping.",	
		if timer >18 	and stepdone==6 then stepdone=7 Talkies.onAction() 	itsagood:play()			fish1talk() end		-- 	"It’s a good thing we have that broom.",	
		if timer >21 	and stepdone==7 then stepdone=8 Talkies.onAction() 	couldyou:play()			fish1talk() end		-- 	"Could you please hand me that broom?",	
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction() 	couldyou2:play()		fish1talk() end		-- 	"Could you please hand me that broom? And try not to drop it on my head.",	
		if timer >28 	and stepdone==9 then stepdone=10 Talkies.onAction()	ithink:play()	 		fish2talk() end		--	"I think it’ll be fine if you just sweep it all down there.",
		if timer >32 	and stepdone==10 then stepdone=11 Talkies.onAction()	itshould:play()		fish2talk() end		--	"It should all fit.",
		if timer >35 	and stepdone==11 then stepdone=12 Talkies.onAction()	justsweep:play()	fish2talk() end		--	"Just sweep it down and we’ll be out of here.",
		if timer >37 	and stepdone==12 then stepdone=13 Talkies.onAction()	forget:play()		fish2talk() end		--	"Forget that bin and just sweep the garbage down.",
		if timer >41 	and stepdone==13 then stepdone=14 Talkies.onAction()	 								 end		--

end

function subtitle7()
		if timer >1 	and stepdone==0 then stepdone=1 				mygod:play()						fish2talk() end		-- 	"My goodness, the impact created cave-in!",
		if timer >6 	and stepdone==1 then stepdone=2 Talkies.onAction() icant:play()						fish1talk() end		-- 	"I can’t go through this cave-in, we have to find another exit.",
		if timer >11 	and stepdone==2 then stepdone=3 Talkies.onAction() thisisnot:play()					fish1talk() end		-- 	"This is not the way.",
		if timer >14 	and stepdone==3 then stepdone=4 Talkies.onAction() iwont:play()						fish1talk() end		-- 	"I won’t make it alone.",
		if timer >18 	and stepdone==4 then stepdone=5 Talkies.onAction() iprobablyshould:play()			fish2talk() end		-- 	"I probably should have had lifted that shell first.",
		if timer >23 	and stepdone==5 then stepdone=6 Talkies.onAction() maybewehavetoswitch:play()		fish1talk() end		--  "Maybe we have to switch these objects.",
		if timer >26 	and stepdone==6 then stepdone=7 Talkies.onAction() amatress:play()					fish2talk() end		-- 	"A mattress. The best thing you can get under water.",
end

function subtitle7part2()

		if timer >1 	and stepdone==0 then stepdone=1 					 nowwecandropthesnail:play()	fish2talk() end		-- 	"Now we can drop the snail on the mattress.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	finallyitsthere:play()			fish1talk() end		-- 	"Finally, it’s there.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction()	thepoorsnail:play()	 			fish2talk() end		--	"The poor snail...",
		if timer >12 	and stepdone==3 then stepdone=4 Talkies.onAction() theerearegoingtobe:play()		fish2talk() end		-- "There are going to be many interesting beings to investigate there.",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction()	 weshouldsearchthecoral:play()	fish1talk() end		--	"We should search the coral reefs.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	 dontweneedamicroscope:play()	fish1talk() end		--	"Don’t we need a microscope to investigate corals?",
		if timer >23 	and stepdone==6 then stepdone=7 Talkies.onAction()	 yestheyaresmall:play()			fish2talk() end		--	"Yes, they are small. But there can be other life forms.",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction()	 	coralturtles:play()			fish2talk() end		--  "Coral turtles, for example.",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()	 andmoreover:play()				fish2talk() end		-- 	"And moreover I have a suspicion that there’s a microscope in a bathyscaph.",	
		if timer >34 	and stepdone==9 then stepdone=10 Talkies.onAction()	 								fish1talk() end		-- 		
end

function subtitle8()
		if timer >1 	and stepdone==0 then stepdone=1 				 thetoilet:play()		fish1talk() end		-- 	"The toilet is David’s favorite place.",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() what:play()			fish2talk() end		-- 	"What?",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction() youdont:play()		fish1talk() end		-- 	"You don’t know David? It’s one of the artists who worked on this game.",
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction() david:play()			fish2talk() end		-- 	"David was no graphic artist. He was a sculpturer. His sculptures are one of the best-known Renaissance artifacts.",
		if timer >19 	and stepdone==4 then stepdone=5 Talkies.onAction() youve:play()			fish1talk() end		-- 	"You’ve got it all wrong.",
		if timer >22 	and stepdone==5 then stepdone=6 Talkies.onAction() didntitell:play()	fish2talk() end		--  "Didn’t I tell you, that WE don’t need a flushing toilet? And decorated with your smut, too!",
		if timer >31 	and stepdone==6 then stepdone=7 Talkies.onAction() theflushing:play()	fish1talk() end		-- 	"The flushing toilet is more hygienic and when under water even more ecological.",
		if timer >37 	and stepdone==7 then stepdone=8 Talkies.onAction() luckily:play()		fish2talk() end		-- 	"Luckily, I needn’t climb inside.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() idont:play()			fish2talk() end		-- 	"I don’t feel very well here. I feel like I’m in the cemetery.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() whatdo:play()		fish1talk() end		--	"What do you mean?",
		if timer >47 	and stepdone==10 then stepdone=11 Talkies.onAction() dontyou:play()		fish2talk() end		--	"Don’t you know where so many aquarium fish end their life?",
		if timer >54 	and stepdone==11 then stepdone=12 Talkies.onAction()	 				fishstop()	end		--	
end

function subtitle9()
		if timer >0 	and stepdone==0 then stepdone=1  			 										fish2talk() end		-- 	"Stop making faces and come help me.",
		if timer >0.5 	and stepdone==1 then stepdone=2 stopmakingfaces:play() Talkies.onAction() 			fish2talk() end		-- 	"Stop looking at yourself in the mirror and make yourself useful.",
		if timer >4 	and stepdone==2 then stepdone=3 stoplookingatyourself:play() Talkies.onAction() 	fish2talk() end		-- 	"That’s great! He just goes on his merry little way, but what about me?",
		if timer >7.5 	and stepdone==3 then stepdone=4 thatsgreat:play() Talkies.onAction() 				fish2talk() end		-- 	"As usual. He just leaves me behind.",
		if timer >13 	and stepdone==4 then stepdone=5 asusual:play() Talkies.onAction() 					fish2talk() end		-- 	"Wait! What about me? I want to get out of here, too!",
		if timer >17 	and stepdone==5 then stepdone=6 wait:play() Talkies.onAction() 						fish2talk() end		--  "Hey, is anybody down there?",
		if timer >21 	and stepdone==6 then stepdone=7 heyisanybody:play() Talkies.onAction() 				fish1talk() end		-- 	"Hello, can you hear me in there?",
		if timer >24 	and stepdone==7 then stepdone=8 hello:play() Talkies.onAction() 					fish1talk() end		-- 	"Is anybody here?",
		if timer >28 	and stepdone==8 then stepdone=9 isanybodyhere:play() Talkies.onAction() 			fish2talk() end		-- 	"Stop shouting and just get me out of here.",
		if timer >29 	and stepdone==9 then stepdone=10 stopshouting:play() Talkies.onAction()		 		fish2talk() end		--	"Stop your screaming and help me get out of here.",
		if timer >33 	and stepdone==10 then stepdone=11 stopyourscreaming:play() Talkies.onAction()	 	fish2talk() end		--	"Here I am. I can’t get out.",
		if timer >36 	and stepdone==11 then stepdone=12 hereiam:play() Talkies.onAction()	 				fish2talk() end		--	"Whose eyes are those?",
		if timer >39 	and stepdone==12 then stepdone=13 whoseeyes:play() Talkies.onAction()	 			fish1talk() end		--	"I don’t know. Maybe the periscope is connected to some other dimension.",
		if timer >42 	and stepdone==13 then stepdone=14 idontknow:play() Talkies.onAction()	 			fish1talk() end		--	"Careful...",
		if timer >48 	and stepdone==14 then stepdone=15 careful:play() Talkies.onAction()	 				fish1talk() end		--	"Don’t say I didn’t warn you!",
		if timer >50 	and stepdone==15 then stepdone=16 dontsay:play() Talkies.onAction()	 				fishstop() end		--	
end

function subtitle10()
		if timer >0 	and stepdone==0 then stepdone=1  					 	lookthepartyboat:play()		fish2talk() end		-- 	"Look. The party boat.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	ihaveafeeling:play()			fish1talk() end		-- 	"I have a feeling that this is not going to be a normal picnic.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() 	youandyourfeelings:play()		fish2talk() end		-- 	"You and your feelings. Surely it’s full of kind and friendly people.",
		if timer >14 	and stepdone==3 then stepdone=4 Talkies.onAction() 	maybeyouareright:play()			fish2talk() end		-- 	"Maybe, you were right. This is a strange picnic.",
		if timer >19 	and stepdone==4 then stepdone=5 Talkies.onAction() 	doyouthink:play()				fish2talk() end		-- 	"Do you think they could get out of this boat?",
		if timer >23 	and stepdone==5 then stepdone=6 Talkies.onAction() 	ihope:play()					fish2talk() end		--  "I hope they can’t get out of that boat.",
		if timer >26 	and stepdone==6 then stepdone=7 Talkies.onAction() 	whatiftheystart:play()			fish2talk() end		-- 	"What if they start to pursue us?",
		if timer >30 	and stepdone==7 then stepdone=8 Talkies.onAction() 	idontthink:play()				fish1talk() end		-- 	"I don’t think they could get us. Skeletons can’t swim very well.",
		if timer >36 	and stepdone==8 then stepdone=9 Talkies.onAction() 	stopthinking:play()				fish1talk() end		-- 	"Stop thinking about it and move your fins so that we can get out of here.",
		if timer >41 	and stepdone==9 then stepdone=10 Talkies.onAction()	youwant:play()					fish1talk() end		--	"You want to support that steel tube with a glass?",
		if timer >45 	and stepdone==10 then stepdone=11 Talkies.onAction()	icantdo:play()				fish2talk() end		--	"I can’t do anything else for you.",
		if timer >48 	and stepdone==11 then stepdone=12 Talkies.onAction()	careful:play()				fish1talk() end		--	"Be careful not to spill it!",
		
end

function subtitle11()
		if timer >0 	and stepdone==0 then stepdone=1  					thewar:play()		fish2talk() end		-- 	"The war goes on even here, on the ocean floor."
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	thehostility:play()	fish1talk() end		-- 	"The hostility will never ebb.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() 	theywill:play()		fish2talk() end		-- 	"They will fight to the last... parrot.",
		if timer >12 	and stepdone==3 then stepdone=4 Talkies.onAction() 	andelk:play()		fish1talk() end		-- 	"And elk.",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() 	nowthis:play()		fish2talk() end		-- 	"Now, this is strange. I thought that elks only live in Sweden.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction() 	yesitoo:play()		fish1talk() end		--  "Yes. I too have heard that they bite tourists there.",
		if timer >23 	and stepdone==6 then stepdone=7 Talkies.onAction() 						fish1talk() end		--  "Ya rusky los.",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction() 	idlike:play()		fish2talk() end		-- 	"I’d like this elk much more if it wasn’t blocking our way so much.",
end

function subtitle12()
		if timer >0 	and stepdone==0 then stepdone=1  					 	could:play()		fish1talk() end		-- 	"Could this be Captain Silver’s ship?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatmakesyou:play()				fish2talk() end		-- 	"What makes you think so?",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() 	thetitle:play()			fish1talk() end		-- 	"The title of this level.",
		if timer >10.5 	and stepdone==3 then stepdone=4 Talkies.onAction() 	whatisthat:play()		fish1talk() end		-- 	"What is that on your head?",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() 	itsuitsyou:play()		fish1talk() end		-- 	"It suits you.",
		if timer >16 	and stepdone==5 then stepdone=6 Talkies.onAction() 	thanks:play()			fish2talk() end		-- 	"Thanks, you too.",
		if timer >19 	and stepdone==6 then stepdone=7 Talkies.onAction() 	averynice:play()		fish1talk() end		--  "A very nice hat.",
		if timer >23 	and stepdone==7 then stepdone=8 Talkies.onAction() yourstetson:play()		fish2talk() end		--  "Your stetson is quite good, too.", 	
		if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction()	myears:play() 			fish2talk() end		-- "My ears were freezing.",
		if timer >31 	and stepdone==9 then stepdone=10 Talkies.onAction() thatssome:play() 		fish2talk() end		-- "That’s some hat! Can I borrow it?",
		if timer >42 	and stepdone==10 then stepdone=11 Talkies.onAction() ifyou:play() 			fish1talk() end		-- "If you lend me yours.",
		if timer >44 	and stepdone==11 then stepdone=12 Talkies.onAction() thissure:play() 		fish2talk() end		-- "This sure is a big stetson.",
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction() andabeautiful:play() 	fish1talk() end		-- "And a beatiful one, too.",
		if timer >49 	and stepdone==13 then stepdone=14 Talkies.onAction() ithink:play() 			fish2talk() end		-- "I think we’re going to have to go inside that ship.",
		if timer >52 	and stepdone==14 then stepdone=15 Talkies.onAction() itlooks:play() 		fish2talk() end		-- "It looks like we have to go inside.",
		if timer >55 	and stepdone==15 then stepdone=16 Talkies.onAction() theair:play() 			fish2talk() end		-- "The air in here smells so stale.",
		if timer >58 	and stepdone==16 then stepdone=17 Talkies.onAction() itseems:play() 		fish1talk() end		-- "It seems that nobody’s here.",
		if timer >61 	and stepdone==17 then stepdone=18 Talkies.onAction() idont:play()			fish2talk() end		-- "I don’t like it here.",
		if timer >64 	and stepdone==18 then stepdone=19 Talkies.onAction() stoptalking:play()		fish1talk() end		-- "Stop talking and swim. Old ship interiors are highly interesting places.",
		if timer >67 	and stepdone==19 then stepdone=20 Talkies.onAction()  						fish1talk() end		-- 	"Can you sense that atmosphere of doom and decay?",
		if timer >70 	and stepdone==20 then stepdone=21 Talkies.onAction() nojust:play()			fish2talk() end		-- "No, just the mold.",
		if timer >73 	and stepdone==21 then stepdone=22 Talkies.onAction() whatdo:play()			fish2talk() end		-- "What do you see?",
		if timer >78 	and stepdone==22 then stepdone=23 Talkies.onAction() icansee:play() 		fish1talk() end		-- "I can see many interesting levels which we’ll have to solve.",		
		
		
end

function subtitle13()
		if timer >0 	and stepdone==0 then stepdone=1  					thismust:play()			fish2talk() end		-- 	"This must be a Viking ship!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()	sure:play()				fish1talk() end		-- 	"Sure, it’s a drakkar.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() shemust:play()			fish2talk() end		-- 	"She must have sunk fast. The oarsmen are still in their places.",
		if timer >13 	and stepdone==3 then stepdone=4 Talkies.onAction() maybe:play()				fish1talk() end		-- 	"Maybe they sank her on purpose. Warriors would always accompany their leader on his way to Valhalla.",
		if timer >21 	and stepdone==4 then stepdone=5 Talkies.onAction() thatsawful:play()		fish2talk() end		-- 	"That’s awful. They look like they’re still alive.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction() themusic:play()			 fishstop() end		-- 	"The music is gone!",
		if timer >28 	and stepdone==6 then stepdone=7 Talkies.onAction() theystopped:play()		 end		--  "They stopped playing!",
		if timer >31 	and stepdone==7 then stepdone=8 Talkies.onAction() themusicstopped:play()	 end		--  "The music stopped again!",
		if timer >34 	and stepdone==8 then stepdone=9 Talkies.onAction()	ohno:play()				 end		--  "Oh, no!",
		if timer >37 	and stepdone==9 then stepdone=10 Talkies.onAction() again:play() 			 end		--  "Again?",
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() notagain:play() 		 end		-- "Oh, not again!",
		if timer >43 	and stepdone==11 then stepdone=12 Talkies.onAction() whathappened:play()	 end		-- "What happened?",
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction() again:play()	 		 end		-- "Again?",
		if timer >51 	and stepdone==13 then stepdone=14 Talkies.onAction() ohno2:play()			 end		-- "Oh, no!",
		if timer >54 	and stepdone==14 then stepdone=15 Talkies.onAction() thebloody:play() 		 end		-- "The bloody music stopped!",
		if timer >57 	and stepdone==15 then stepdone=16 Talkies.onAction() whatare:play() 		 end		-- "What are they doing up there?",
		if timer >60 	and stepdone==16 then stepdone=17 Talkies.onAction() howcould:play()		 end		-- "How could it be?",
		if timer >63 	and stepdone==17 then stepdone=18 Talkies.onAction() solo:play() 			 end		-- "Take it easy, boys. Solo for Olaf!",
		if timer >66 	and stepdone==18 then stepdone=19 Talkies.onAction() nevermind:play()		 end		-- "Never mind that, boys. We can sing ourself!",
		if timer >69 	and stepdone==19 then stepdone=20 Talkies.onAction() nevermind2:play()		 end		-- "Never mind, boys. We can sing ourself, like in the old times!"
--		if timer >70 	and stepdone==20 then stepdone=21 Talkies.onAction() 		end		-- 
end

function subtitle13skull()
		if timer >0 	and stepdone==0 then stepdone=1  					end		-- 	
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			fish1talk() end		-- 	
end

function subtitle14()
		if timer >0 	and stepdone==0 then stepdone=1  				 	whatkind:play()		fish2talk() end		--  "What kind of strange ship is that?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	thisis1:play()		fish1talk() end		-- 	"This is the wreck of the civilian airplane LC-10 Lemura.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	thisis2:play()		fish1talk() end		-- 	"This is the wreck of the civilian airplane Atlantobus.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	thisis3:play()		fish1talk() end		-- 	"This is the wreck of the civilian airplane Poseidon 737.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	canyousee:play()	fish1talk() end		-- 	"Can you see that eye? The silent witness to the tragedy... Somebody trusted this airplane - and the glass eye is everything he left behind.",
		if timer >35 	and stepdone==5 then stepdone=6 Talkies.onAction() 	thisisnot:play()	fish2talk() end		-- 	"This is not a glass eye but a gyroscope. At least in this level.",
		if timer >40 	and stepdone==6 then stepdone=7 Talkies.onAction() 	seats:play()		fish2talk() end		--  "Seats. Why are there so many seats here?",
		if timer >45 	and stepdone==7 then stepdone=8 Talkies.onAction() bethankful:play()	fish1talk() end		--  "Be thankful. Could you get out without them?",	
end

function subtitle15()

		--level 15 bathyscaph

		if timer >0 	and stepdone==0 then stepdone=1 				 sothis:play()					fish2talk() end		-- 		"So this is an exploratory bathyscaph.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() anice:play()					fish2talk() end		-- 		"A nice microscope.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() shouldnt:play()				fish1talk() end		-- 		"Shouldn’t I lift this?",
		if timer >11	and stepdone==3 then stepdone=4 Talkies.onAction() doyou:play()					fish1talk() end		-- 		"Do you want me to lift this up?",
		if timer >13 	and stepdone==4 then stepdone=5 Talkies.onAction() shouldi:play()				fish1talk() end		-- 		"Shouldn’t I turn that off?",
		if timer >14 	and stepdone==5 then stepdone=6 					 phonebeep1:play()	phoneringing=true timer2=0 end		-- 		beep
		if timer >18 	and stepdone==6 then stepdone=7 Talkies.onAction() wecant:play()		phoneringing=false fish2talk()  end		-- 		"We can’t get to the receiver from here.", 		
		if timer >22 	and stepdone==7 then stepdone=8 Talkies.onAction()	phonebeep1_2:play() thatisgoing:play()			phoneringing=true fishstop() end		--		"That is going to be for me...",
		if timer >24 	and stepdone==8 then stepdone=9 Talkies.onAction() thatisgoing2:play()	phoneringing=false  end		-- 		"That is going to be my brother...",
		if timer >27 	and stepdone==9 then stepdone=10 Talkies.onAction() phonebeep1_3:play() phoneringing=true  couldyou:play()	 			 end		-- 		"Could you handle that call for me, please?",
		if timer >30 	and stepdone==10 then stepdone=11 Talkies.onAction() ihope:play() 		phoneringing=false		 end		-- 		"I hope I am not in the way here.",
		if timer >32 	and stepdone==11 then stepdone=12 Talkies.onAction() really:play() 				 end		-- 		"Really, I can easily crawl away. Just say the word.",
		if timer >36 	and stepdone==12 then stepdone=13 Talkies.onAction() phonebeep1:play() ireallydont:play()	phoneringing=true		 end		-- 		"I really don’t want to be an obstacle.",
		if timer >38 	and stepdone==13 then stepdone=14 Talkies.onAction() phonebeep2:play() justtellme:play()	phoneringing=false		 end		-- 		"Just tell me if you want me to move away.",
		if timer >41 	and stepdone==14 then stepdone=15 Talkies.onAction() pickupphone:play() hello:play() 				 end		-- 		"Hello.",
		if timer >43 	and stepdone==15 then stepdone=16 Talkies.onAction() isanybody:play() 			 end		-- 		"Is anybody there?",
		if timer >45 	and stepdone==16 then stepdone=17 Talkies.onAction() areyou:play() 				 end		-- 		"Are you there, brother?",
		if timer >47 	and stepdone==17 then stepdone=18 Talkies.onAction() thesnail:play() 			 end		-- 		"The snail Escargot speaking.",
		if timer >51 	and stepdone==18 then stepdone=19 Talkies.onAction() icanthearyou:play()		 end		-- 		"I can’t hear you.",
		if timer >53 	and stepdone==19 then stepdone=20 Talkies.onAction() hello2:play()				 end		-- 		"Hello, hello!",
		if timer >55 	and stepdone==20 then stepdone=21 					  phonebeep1:play() phoneringing=true 		 end		-- 		beep2
		if timer >57.7 	and stepdone==21 then stepdone=22 					  					phoneringing=false		 end		--
		if timer >63 	and stepdone==22 then stepdone=23 Talkies.onAction() pickupphone:play() chiefinspector:play()		phoneringing=false end		-- 		"Hello, chief inspector Clouseau speaking. I am looking for the dangerous shellfish, who... Oh no, not now, Kato, you yellow ape!",
		if timer >68 	and stepdone==23 then stepdone=24 					 phonebeep1:play() 			phoneringing=true timer2=0 end		-- 		beep 3
		if timer >75 	and stepdone==24 then stepdone=25 Talkies.onAction() pickupphone:play() answermachine:play()		phoneringing=false end		-- 		"Hello, you have reached the number 4202-21913271. Nobody from the family of Roundfish is presently present. Please, leave the message after the beep. What happened?",
		if timer >92 	and stepdone==25 then stepdone=26 Talkies.onAction() nowisquite:play() 			fish1talk() end		-- 		"Now it is quiet here.",
		--if timer >80 	and stepdone==25 then stepdone=26 Talkies.onAction()	 						end		-- 		


end

function subtitle16()

		--level 16 Tank

		if timer >0 	and stepdone==0 then stepdone=1 					 thisissome:play()			fish2talk() end		-- 		"This is some improvement. From bathyscaph to tank, this is like from a line into a net.",
		if timer >7 	and stepdone==1 then stepdone=2 Talkies.onAction() atleast:play()				fish1talk() end		-- 		"At least there are no oh-so-obliging snails here.",
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction() thatisbut:play()				fish2talk() end		-- 		"That is but little consolation.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() ialways:play()				fish1talk() end		-- 		"Thinking about it, I always wanted to see the inside of a tank.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() itseems:play()				fish2talk() end		-- 		"It seems you will have plenty of opportunity to look around.",
		if timer >24 	and stepdone==5 then stepdone=6 Talkies.onAction()	iwonder:play()				fish2talk() end		-- 		"I wonder how this tank got into the sea.",
		if timer >28 	and stepdone==6 then stepdone=7 Talkies.onAction() maybe:play()					fish1talk() end		-- 		"Maybe it was an amphibious tank.",
		if timer >31 	and stepdone==7 then stepdone=8 Talkies.onAction()	anamphibious:play()			fish2talk() end		--		"An amphibious tank? Just imagine: On the moonless night, the uncomprehending defenders bewilderedly watch the mass of snorkels emerging from the surf, searching in vain for the landing craft...",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction() sowhy:play()					fish1talk() end		-- 		"So why are you asking, if you are so darned clever?!",
		if timer >48 	and stepdone==9 then stepdone=10 Talkies.onAction() maybethereis:play()			fish1talk() end		-- 		"Maybe there is a sunken landing craft nearby.",
		if timer >53 	and stepdone==10 then stepdone=11 Talkies.onAction() itispossible:play() 		fish2talk() end		--		"It is possible.",
		if timer >56 	and stepdone==11 then stepdone=12 Talkies.onAction() doyouthink:play() 			fish2talk() end		-- 		"Do you think that this ammunition could harm us?",
		if timer >59 	and stepdone==12 then stepdone=13 Talkies.onAction() idontknow:play()			fish1talk() end		-- 		"I don’t know, but I’ll try to keep my distance from it.",
		if timer >62 	and stepdone==13 then stepdone=14 Talkies.onAction() ithink:play()				fish1talk() end		-- 		"I think we are going to need that ladder.",
		if timer >65 	and stepdone==14 then stepdone=15 Talkies.onAction() toclimbout:play()			fish2talk() end		--		"To climb out? But we have only fins.",
		if timer >68 	and stepdone==15 then stepdone=16 Talkies.onAction() notoplug:play() 			fish1talk() end		-- 		"No. To plug that hole.",
			

end

function subtitle17()

		--level 17 Eight vikings in a Boat

		if timer >0 	and stepdone==0 then stepdone=1 					 thisship:play()			fish2talk() end		-- 		"This ship must have sunk long time ago.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() definitely:play()			fish1talk() end		-- 	Definitely. It’s one of the earliest drakkars. Judging by the shape of the head, number of spirals and color of the water I think it belonged to the grandfather of Eric the Red.",
		if timer >20 	and stepdone==2 then stepdone=3 Talkies.onAction() ortothefather:play()			fish1talk() end		-- 		"Or to the father-in-law of Olaf the Brave.",
		if timer >25	and stepdone==3 then stepdone=4 Talkies.onAction() ormaybeto:play()				fish1talk() end		-- 	"Or maybe to the great-uncle of Leif the Skillful.",
		if timer >30 	and stepdone==4 then stepdone=5 Talkies.onAction() itcould:play()				fish1talk() end		-- 	"It could also belong to the niece of Harold the Great.",
		if timer >35 	and stepdone==5 then stepdone=6 Talkies.onAction()	orpossibly:play()			fish1talk() end		-- 	"Or possibly to the aunt of Snorr the Courageous.",
		if timer >39 	and stepdone==6 then stepdone=7 Talkies.onAction() itseven:play()				fish1talk() end		-- 	"It is even possible it belonged to Thorson the Hard.",
		if timer >44 	and stepdone==7 then stepdone=8 Talkies.onAction()								fish1talk() end		--		"I always thought I was the fishing boat of Eric the Unready... But today I learned the truth. Yes, it’s never too late to start a new life!",
		if timer >46 	and stepdone==8 then stepdone=9 Talkies.onAction() lookatthat:play()			fish2talk() end		-- 	 "Look at that poor dog.",
		if timer >49 	and stepdone==9 then stepdone=10 Talkies.onAction() doesnthe:play()				fish2talk() end		-- 		"Doesn’t he seem to be breathing kind of strangely?",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() foradog:play() 			fish1talk() end		--		"For a dog under water I think he’s doing rather well.",
		if timer >65 	and stepdone==11 then stepdone=12 Talkies.onAction() hedoesntlook:play() 		fish1talk() end		-- 		"He doesn’t look all that bad considering that he’s been here for a few centuries.",
		if timer >69 	and stepdone==12 then stepdone=13 Talkies.onAction() shutup:play()				fishstop() end		-- 	"Shut up!
		if timer >73 	and stepdone==13 then stepdone=14 Talkies.onAction() willyou:play()				 end		-- "Will you shut up already!"
		if timer >80 	and stepdone==14 then stepdone=15 Talkies.onAction() soyouarenot:play()			 end		--		"So, you’re not going to shut up, are you?"
		if timer >85 	and stepdone==15 then stepdone=16 Talkies.onAction() argh:play() 				 end		-- 		"Aaaaargh..."
		if timer >90 	and stepdone==16 then stepdone=17 Talkies.onAction() hehehe:play() 				 end		-- 		"He-he-he-he-he... Eh-eh-eh-eh..."
		if timer >95 	and stepdone==17 then stepdone=18 Talkies.onAction() isitover:play() 			 end		-- 		"Is it over?"
		if timer >100 	and stepdone==18 then stepdone=19 Talkies.onAction() arewesafe:play()			 end		-- 		"Are we safe now?"
		if timer >105 	and stepdone==19 then stepdone=20 Talkies.onAction() isthefight:play()			 end		-- 		"Is the fight over?"
		if timer >110 	and stepdone==20 then stepdone=21 Talkies.onAction() arewestill:play() 			 end		-- 		"Is the enemy finally gone?"
		if timer >115 	and stepdone==21 then stepdone=22 Talkies.onAction() areweinvalhalla:play()		 end		-- 		"Are we still fighting?"
		if timer >120 	and stepdone==22 then stepdone=23 Talkies.onAction() whenare:play() 			 end		-- 		"Are we in Valhalla yet, chief?"
		if timer >125 	and stepdone==23 then stepdone=24 Talkies.onAction() illletyou:play()			 end		-- 		"When are we going to get to Valhalla?"
		if timer >130 	and stepdone==24 then stepdone=25 Talkies.onAction() canigo:play() 				 end		-- 		"Can I go relieve myself, sir?"
		if timer >135 	and stepdone==25 then stepdone=26 Talkies.onAction() isntthis:play()			 end		-- 		"Wait till we get to Valhalla."
		if timer >140 	and stepdone==26 then stepdone=27 Talkies.onAction() patience:play() 			 end		-- 		"Isn’t this journey to Valhalla taking a bit too long?"
		if timer >145 	and stepdone==27 then stepdone=28 Talkies.onAction() areyousure:play()			 end		-- 		"Patience is the sign of a true warrior."
		if timer >150 	and stepdone==28 then stepdone=29 Talkies.onAction() awarrior:play()			 end		-- 		"Are you sure this is how to get to Valhalla?"
		if timer >155 	and stepdone==29 then stepdone=30 Talkies.onAction() imincharge:play()			 end		-- 		"A warrior has to trust his commander!"
		if timer >160 	and stepdone==30 then stepdone=31 Talkies.onAction() whydid:play()				 end		-- 		"Ahhh, I’m in charge of a ship full of cowards."
		if timer >165 	and stepdone==31 then stepdone=32 Talkies.onAction() laughing:play()			 end		-- 		"Why did I have to get a ship full of laughing buffoons?"
		if timer >170 	and stepdone==32 then stepdone=33 Talkies.onAction() realvikings:play()			 end		-- 		"Laughing buffoons, a slobbering dog. I think I’ll go drown myself."
		if timer >175 	and stepdone==33 then stepdone=34 Talkies.onAction() braids:play()				 end		-- 		"Real Vikings have beards."
		if timer >180 	and stepdone==34 then stepdone=35 Talkies.onAction() whatkind:play()			 end		-- 		"B-b-braids are in n-nowadays."
		if timer >185 	and stepdone==35 then stepdone=36 Talkies.onAction() vikingfashion:play()		 end		-- 		"What kind of fad is that? Braids? For crying out loud."
		if timer >190 	and stepdone==36 then stepdone=37 Talkies.onAction() isimply:play()				 end		-- 		"V-v-viking Fashion M-m-monthly r-recommends a blonde braid to complement a d-d-dark helmet and b-blue shield."
		if timer >195 	and stepdone==37 then stepdone=38 Talkies.onAction() youshould:play()			 end		-- 		"I simply don’t agree with such a fad."
		if timer >200 	and stepdone==38 then stepdone=39 Talkies.onAction() butimcool:play()			 end		-- 		"You should stick to the Viking traditions instead."
		if timer >205 	and stepdone==39 then stepdone=40 Talkies.onAction() evenerik:play()			 end		-- 		"B-b-but I’m c-c-cool!"
		if timer >210 	and stepdone==40 then stepdone=41 Talkies.onAction() nonsense:play()			 end		-- 		"Even Erik the G-g-great Eric had a b-b-braid!"
		if timer >215 	and stepdone==41 then stepdone=42 Talkies.onAction() buthealso:play()			 end		-- 		"Nonsense. He had a beard."
		if timer >220 	and stepdone==42 then stepdone=43 Talkies.onAction() hedidnot:play()			 end		-- 		"But h-he also h-had a b-b-braid."
		if timer >225 	and stepdone==43 then stepdone=44 Talkies.onAction() doyouthink:play()			 end		-- 		"He did not. And that’s final."
		if timer >230 	and stepdone==44 then stepdone=45 Talkies.onAction() definitely:play()			 end		-- 		"D-d-do you think that a b-b-beard would suit me b-b-better?"
		if timer >235 	and stepdone==45 then stepdone=46 Talkies.onAction() illthink:play()			 end		-- 		"Definitely."
		if timer >240 	and stepdone==46 then stepdone=47 Talkies.onAction() hmmm:play()				 end		-- 		"I’ll th-th-think it o-over."
		if timer >245 	and stepdone==47 then stepdone=48 Talkies.onAction() breads:play()				 end		-- 		"Hmmm... Ehmmm... Hahmmm..."
		if timer >250 	and stepdone==48 then stepdone=49 Talkies.onAction() todays:play()				 end		-- 		"N-no, b-b-braids are d-definitely b-b-better."
		if timer >255 	and stepdone==49 then stepdone=50 Talkies.onAction() todaysyouth:play()			 end		-- 		"Today’s youth - absolutely unbelievable."
		if timer >260 	and stepdone==50 then stepdone=51 Talkies.onAction() awarriorwith:play()		 end		-- 		"A warrior with braids. It’s unheard of."
		if timer >265 	and stepdone==51 then stepdone=52 Talkies.onAction() youyoungsters:play()		 end		-- 		"You youngsters just think you can get away with anything today."
		if timer >270 	and stepdone==52 then stepdone=53 Talkies.onAction() wellistill:play()			 end		-- 		"Well, I still look cool."
	
		

end

function subtitle18()

		--level 18 Return from the party

		if timer >0 	and stepdone==0 then stepdone=1 					 thatghostly:play()			fish2talk() end		--	"That ghostly boat, again.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() canyouseeit:play()			fish1talk() end		--	"Can you see it? Something has changed while we were away.
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction() itellyou:play()				fish1talk() end		--	"I tell you, somebody is watching us.",
		if timer >18 	and stepdone==3 then stepdone=4 Talkies.onAction() whydoyou:play()				fish2talk() end		--	"Why do you keep looking at us? Can’t you see it makes us nervous?",
		if timer >22	and stepdone==4 then stepdone=5 Talkies.onAction() itlooks:play()				fish2talk() end		--	"It looks like their picnic is coming to an end.",
		if timer >26 	and stepdone==5 then stepdone=6 Talkies.onAction() 								fish2talk() end		--	"The picnic is over.",
		if timer >29 	and stepdone==6 then stepdone=7 Talkies.onAction()	thatsall:play()				fish2talk() end		--	"That’s all that is left from the picnic.",
		if timer >33 	and stepdone==7 then stepdone=8 Talkies.onAction() theyhavehad:play()			fish2talk() end		--	"They have had a kind of picnic by the bottom.",
		if timer >39 	and stepdone==8 then stepdone=9 Talkies.onAction()	ithink:play()				fish1talk() end		--	"I think that those party-goers are little bit tired.",
		if timer >44 	and stepdone==9 then stepdone=10 Talkies.onAction() yes:play()					fish1talk() end		--	"Yes, they have had enough.",
		if timer >46 	and stepdone==10 then stepdone=11 Talkies.onAction() ithinktheycannot:play()	fish1talk() end		--	"I think they cannot think about harming us now.",
		if timer >49 	and stepdone==11 then stepdone=12 Talkies.onAction() ithinktheyare:play() 		fish1talk() end		--	"I think they are quite harmless now.",
		if timer >55 	and stepdone==12 then stepdone=13 Talkies.onAction() itwontbethat:play() 		fish1talk() end		--	"It won’t be that easy like the last time.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() itseemsitsgoing:play()		fish1talk() end		--	"It seems it’s going to be more difficult this time.",
		if timer >69 	and stepdone==14 then stepdone=15 Talkies.onAction() 							end		--
		if timer >73 	and stepdone==15 then stepdone=16 Talkies.onAction() 					end		--
		if timer >98 	and stepdone==16 then stepdone=17 Talkies.onAction() 					end		--
		if timer >95 	and stepdone==17 then stepdone=18 Talkies.onAction()  					end		--
		if timer >97 	and stepdone==18 then stepdone=19 Talkies.onAction()  					end		--
		if timer >91 	and stepdone==19 then stepdone=20 Talkies.onAction() 					end		--
		if timer >93 	and stepdone==20 then stepdone=21 Talkies.onAction() 					end		--
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction()  					end		--
		if timer >93 	and stepdone==22 then stepdone=23 Talkies.onAction() 					end		--
		if timer >98 	and stepdone==23 then stepdone=24 Talkies.onAction()  					end		--
		if timer >95 	and stepdone==24 then stepdone=25 Talkies.onAction() 					end		--
		if timer >85 	and stepdone==25 then stepdone=26 Talkies.onAction() 	 				end		--
		
end

function subtitle19()
		-- level 19 The gods must be mad
		
		if timer >0 	and stepdone==0 then stepdone=1 				 ihaveasuspicion:play()			fish1talk() end		--	"I have a suspicion that we are about to discover something terrible.",
		if timer >7 	and stepdone==1 then stepdone=2 Talkies.onAction() ialwaysknewthat:play()		fish1talk() end		--	"I always knew that: the gods must be mad.",
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction() godismad:play()				fish1talk() end		--	"God is mad and the whole world is his plaything.",
		if timer >16	and stepdone==3 then stepdone=4 Talkies.onAction() ithinkthat:play()			fish2talk() end		--	"I think that you are right.",
		if timer >18.5 	and stepdone==4 then stepdone=5 Talkies.onAction() imafraid:play() 				fish2talk() end		--	"I am afraid that you are right.",
		if timer >21 	and stepdone==5 then stepdone=6 Talkies.onAction()	yesitsschocking:play()		fish2talk() end		--	"Yes, it is shocking.",
		if timer >24 	and stepdone==6 then stepdone=7 Talkies.onAction() whatarewe:play()				fish2talk() end		--	"What are we to do about it?",
		if timer >27 	and stepdone==7 then stepdone=8 Talkies.onAction()	wecantleave:play()			fish1talk() end		--	"We can’t leave it this way. We must incarcerate them.",
		if timer >32 	and stepdone==8 then stepdone=9 Talkies.onAction() whatmustwedo:play()			fish2talk() end		--	"What must we do?",
		if timer >34 	and stepdone==9 then stepdone=10 Talkies.onAction() puttheminto:play()			fish1talk() end		--	"Put them into the mad... I mean into the detainment facility of FDTO.",
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() youareright:play() 		fish2talk() end		--	"You are right. Shall we take both of them?",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() ofcourse:play() 			fish1talk() end		--	"Of course. If we leave one of them here, any mad idea could occur to him. He could try to start playing golf, for example.",
		if timer >55 	and stepdone==12 then stepdone=13 Talkies.onAction() hardlywith:play()			fish2talk() end		--	"Hardly with this club. I think we should be happy if we manage to get one. The other one will be harmless then.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() wellwhichone:play()		fish1talk() end		--  "Well. Which one?",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() theyellowone:play()		fish2talk() end		--  "The yellow one.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 							end		--  "The blue one.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() thatyellowone:play() 		fish1talk() end		--  "That yellow one.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction()  							end		--  "That blue one.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() wewillleavethis:play()		fish2talk() end		--	"We will leave this to the player.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() weshallleave:play()		fish1talk() end		--  "We shall leave this to the player.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() lookatthatclub:play()		fish1talk() end		--  "Look at that club: something terrible happened to it.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 							end		--  "A pld probably sucked upon it.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() motherwasright:play() 		fish1talk() end		--  "Mother was right. Abnormal playfulness is the root of all evil.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() thatstrue:play()			fish2talk() end		--  "That’s true. It would be much easier if that table tennis bat was somewhere else.",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() bytheway:play()			fish1talk() end		--  "By the way, did you notice that the real golf ball is in the upper left corner while that thing in the lower right corner is a cricket ball?",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() isthere:play()	 			fish2talk() end		--  "Is there some hidden symbolism?",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() whoknows:play()			fish1talk() end		--  "Who knows.",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() ithought:play() 			fish2talk() end		--  "I thought it was a table tennis ball up there.",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() itshardtosay:play() 		fish1talk() end		--"It’s hard to say with this screen resolution.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() thistime:play()	 		fish2talk() end		--"This time our goal is to get out one of those gods.",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() miss:play()	 			end		--	"Miss!",
		if timer >152 	and stepdone==31 then stepdone=32 Talkies.onAction() miss2:play()	 			end		--	"Miss!",
		if timer >154 	and stepdone==32 then stepdone=33 Talkies.onAction() miss3:play()	 			end		--	"Miss!",
		if timer >156 	and stepdone==33 then stepdone=34 Talkies.onAction() miss4:play()	 			end		--	"Miss!",
		if timer >158 	and stepdone==34 then stepdone=35 Talkies.onAction() 				 			end		--	"Miss!",
		if timer >160 	and stepdone==35 then stepdone=36 Talkies.onAction() hit:play()		 			end		--	"Hit!",
		if timer >162 	and stepdone==36 then stepdone=37 Talkies.onAction() hit2:play()	 			end		--	"Hit!",
		if timer >164 	and stepdone==37 then stepdone=38 Talkies.onAction() hit3:play()	 			end		--	"Hit!",
		if timer >166 	and stepdone==38 then stepdone=39 Talkies.onAction() hit4:play()	 			end		--	"Hit!",
		if timer >168 	and stepdone==39 then stepdone=40 Talkies.onAction() sank:play()	 			end		--	"Sank!",
		if timer >170 	and stepdone==40 then stepdone=41 Talkies.onAction() sank2:play()	 			end		--	"Sank!",
		if timer >172 	and stepdone==41 then stepdone=42 Talkies.onAction() sank3:play()	 			end		--	"Sank!",
		if timer >174 	and stepdone==42 then stepdone=43 Talkies.onAction()							end		--iwonyouseaslug:play()	 			--	"I won you seaslug!",
		if timer >180 	and stepdone==43 then stepdone=44 Talkies.onAction() istart:play()	 			end		--	"I start!",
		if timer >182 	and stepdone==44 then stepdone=45 Talkies.onAction() well:play()	 			end		--	"Well!",
		if timer >184 	and stepdone==45 then stepdone=46 Talkies.onAction() shallwe:play()	 			end		--	"Shall we play another one?",
		if timer >186 	and stepdone==46 then stepdone=47 Talkies.onAction() idontcheat:play()	 		end		--	"I don’t cheat.",
		if timer >188 	and stepdone==47 then stepdone=48 Talkies.onAction() itwasamistake:play()	 	end		--	"It was a mistake, probably...",
		if timer >190 	and stepdone==48 then stepdone=49 Talkies.onAction() miss:play()	 			end		--	"Miss!",
		if timer >192 	and stepdone==49 then stepdone=50 Talkies.onAction() miss2:play()	 			end		--	"Miss!",
		if timer >194 	and stepdone==50 then stepdone=51 Talkies.onAction() miss3:play()	 			end		--	"Miss!",
		if timer >196 	and stepdone==51 then stepdone=52 Talkies.onAction() miss4:play()	 			end		--	"Miss!",
		if timer >198	and stepdone==52 then stepdone=53 Talkies.onAction() 				 			end		--	"Miss!",
		if timer >200 	and stepdone==53 then stepdone=54 Talkies.onAction() hit:play()		 			end		--	"Hit!",
		if timer >202 	and stepdone==54 then stepdone=55 Talkies.onAction() hit1:play()	 			end		--	"Hit!",
		if timer >204 	and stepdone==55 then stepdone=56 Talkies.onAction() hit2:play()	 			end		--	"Hit!",
		if timer >206 	and stepdone==56 then stepdone=57 Talkies.onAction() hit3:play()	 			end		--	"Hit!",
		if timer >208 	and stepdone==57 then stepdone=58 Talkies.onAction() sank:play()	 			end		--	"Sank!",
		if timer >210 	and stepdone==58 then stepdone=59 Talkies.onAction() sank2:play()	 			end		--	"Sank!",
		if timer >212 	and stepdone==59 then stepdone=60 Talkies.onAction() sank3:play()	 			end		--	"Sank!",
		if timer >214 	and stepdone==60 then stepdone=61 Talkies.onAction() hehehe:play()	 			end		--	"He, he, he... I won!",
		if timer >216 	and stepdone==61 then stepdone=62 Talkies.onAction() shallwe:play()	 			end		--	"Shall we try it again?",
		if timer >218 	and stepdone==62 then stepdone=63 Talkies.onAction() youalreadytried:play()		end		--	"You already tried this!",
		if timer >220 	and stepdone==63 then stepdone=64 Talkies.onAction() youalreadysaid:play()	 	end		--	"You already said that!",
		if timer >222 	and stepdone==64 then stepdone=65 Talkies.onAction() itcantbe:play()	 		end		--	"It can’t be a miss on this space.",
		if timer >224 	and stepdone==65 then stepdone=66 Talkies.onAction() youcheat:play()	 		end		--	"You cheat!!!",
		if timer >226 	and stepdone==66 then stepdone=67 Talkies.onAction() ihavetried:play()	 		end		--	"I have tried that - and you said ‘miss’!",
		if timer >228 	and stepdone==67 then stepdone=68 Talkies.onAction() 	 						end		--	"",
		if timer >230 	and stepdone==68 then stepdone=69 Talkies.onAction() a1:play()	 				end		--	"A%1.",
		if timer >232 	and stepdone==69 then stepdone=70 Talkies.onAction() b1:play()	 				end		--	"B%1.",
		if timer >234 	and stepdone==70 then stepdone=71 Talkies.onAction() c1:play()	 				end		--	"C%1.",
		if timer >236 	and stepdone==71 then stepdone=72 Talkies.onAction() d1:play()	 				end		--	"D%1.",
		if timer >238 	and stepdone==72 then stepdone=73 Talkies.onAction() e1:play()	 				end		--	"E%1.",
		if timer >240 	and stepdone==73 then stepdone=74 Talkies.onAction() f1:play()	 				end		--	"F%1.",
		if timer >242 	and stepdone==74 then stepdone=75 Talkies.onAction() g1:play()	 				end		--	"G%1.",
		if timer >244 	and stepdone==75 then stepdone=76 Talkies.onAction() h1:play()	 				end		--	"H%1.",
		if timer >246 	and stepdone==76 then stepdone=77 Talkies.onAction() i1:play()	 				end		--	"I%1.",
		if timer >248 	and stepdone==77 then stepdone=78 Talkies.onAction() j1:play()	 				end		--	"J%1.",
		if timer >250 	and stepdone==78 then stepdone=79 Talkies.onAction() 	 						end		--	"",
		if timer >252 	and stepdone==79 then stepdone=80 Talkies.onAction() a12:play()	 				end		--	"A%1.",
		if timer >256 	and stepdone==80 then stepdone=81 Talkies.onAction() b12:play()	 				end		--	"B%1.",
		if timer >258 	and stepdone==81 then stepdone=82 Talkies.onAction() c12:play()	 				end		--	"C%1.",
		if timer >260 	and stepdone==82 then stepdone=83 Talkies.onAction() d12:play()	 				end		--	"D%1.",
		if timer >262 	and stepdone==83 then stepdone=84 Talkies.onAction() e12:play()	 				end		--	"E%1.",
		if timer >264 	and stepdone==84 then stepdone=85 Talkies.onAction() f12:play()	 				end		--	"F%1.",
		if timer >266 	and stepdone==85 then stepdone=86 Talkies.onAction() g12:play()	 				end		--	"G%1.",
		if timer >268 	and stepdone==86 then stepdone=87 Talkies.onAction() h12:play()	 				end		--	"H%1.",
		if timer >270 	and stepdone==87 then stepdone=88 Talkies.onAction() i12:play()	 				end		--	"I%1.",
		if timer >272 	and stepdone==88 then stepdone=89 Talkies.onAction() j12:play()	 				end		--	"J%1.",
	
		
end

function subtitle19end()
		if timer >0 	and stepdone==0 then stepdone=1 					occupation:play()			end		-- 		Occupation of the captive we are sending to you is an ocean god.
		if timer >6 	and stepdone==1 then stepdone=2 Talkies.onAction() exceptofthe:play()			end		-- 		Except of the plane and ship disappearings (so called Sea Battle case)
		if timer >11.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	heisresponsible:play()		end		-- 		he is responsible for the other crimes as well,
		if timer >18	and stepdone==3 then stepdone=4 Talkies.onAction() movingthecontinents:play()	end		-- 		moving the continents (code name Run, continent, run)
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() andmeteorite:play()			end		-- 		and meteorite in Tunguzka (code Jumping Jack) are among them.
		if timer >26.5 	and stepdone==5 then stepdone=6 Talkies.onAction()	wemanagedto:play()			end		-- 		We managed to intervene just in the nick of time:
		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()	wehavefound:play()			end		-- 		we have found a brand new unwrapped box with a table game called STAR WARS in the captive’s house.
		if timer >38 	and stepdone==7 then stepdone=8 Talkies.onAction()	youcanfindtherecords:play()	end		-- 		You can find the records of his sea battles in the attachement.
		
end

function subtitle20()

		-- level 20 House with an elevator

		if timer >0 	and stepdone==0 then stepdone=1 				 		whata:play()			fish1talk() end		-- 		"What a glorious civilization. So many centuries under water and their elevators still work.",
		if timer >10 	and stepdone==1 then stepdone=2 Talkies.onAction()		especially:play() 		fish2talk() end		-- 		"Especially, when we push them.",
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction()		butonly:play()			fish2talk() end		-- 		"But only down.",
		if timer >14	and stepdone==3 then stepdone=4 Talkies.onAction() 	doyousee:play()				fish2talk() end		-- 		"Do you see that shell?",
		if timer >16 	and stepdone==4 then stepdone=5 Talkies.onAction() 	isntthatshell:play()		fish1talk() end		-- 		"It’s an age-old talisman for elevator builders who always said ’Build well. Build with Shell’.",
		if timer >18.5 	and stepdone==5 then stepdone=6 Talkies.onAction()		itsanage:play() 		fish2talk() end		-- 		"Isn’t that shell a little bit familiar?",
		if timer >27 	and stepdone==6 then stepdone=7 Talkies.onAction()		theyshould:play()		fish1talk() end		-- 		"They should be ashamed of themselves. This is subliminal advertising.",
		if timer >33 	and stepdone==7 then stepdone=8 Talkies.onAction() 	thatskull:play()			fish1talk() end		-- 		"That skull looks familiar.",
		if timer >35 	and stepdone==8 then stepdone=9 Talkies.onAction()		allhuman:play() 		fish2talk() end		-- 		"All human skulls look the same... Unlike fish skeletons.",
		if timer >39.5 	and stepdone==9 then stepdone=10 Talkies.onAction()	thisisathough:play() 		fish2talk() end		-- 		"This is a tough path.",
		if timer >42 	and stepdone==10 then stepdone=11 Talkies.onAction()	ifeel:play() 			fish1talk() end		-- 		"I feel like a mouse on a treadmill.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction()	atreadmill:play() 		fish2talk() end		-- 		"A treadmill for fish? But we only have fins.",
		if timer >49 	and stepdone==12 then stepdone=13 Talkies.onAction()	imoverhere:play() 		fish1talk() end		-- 		"Hoo, hooooo, hoooo... I’m over heeeere...",
		if timer >53 	and stepdone==13 then stepdone=14 Talkies.onAction()	ifyouwant:play() 		fish1talk() end		-- 		"Hoooo, if you want I’ll tell you how to solve all the levels...",
		if timer >58 	and stepdone==14 then stepdone=15 Talkies.onAction()	iknow:play()			fish1talk() end		-- 		"Hooo, ho, hooo, I know why the city sank!",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction()	whyisnt:play() 			fish1talk() end		-- 		"Hoooo, why isn’t anyone paying attention to me?",
		if timer >68 	and stepdone==16 then stepdone=17 Talkies.onAction()	whyare:play() 			fish1talk() end		-- 		"Hoooo ho hooo! Why are you ignoring me?",
		if timer >73 	and stepdone==17 then stepdone=18 Talkies.onAction()	fish:play() 			fish1talk() end		-- 		"Hooooo, hooooo, fish! Pay attention to me!",
		if timer >78 	and stepdone==18 then stepdone=19 Talkies.onAction()					 		fish1talk() end		-- 		

end

function subtitle21()
		if not (language=="es") and not (language=="fr") and not (language=="de") and not (language=="nl") then
		-- level 21 Welcome to our city

		if timer >0 	and stepdone==0 then stepdone=1  	welcome:play()		statuetalk()		end		-- 		"Welcome to the most beautiful city under the sun.",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction()	welcometo:play() 		end		-- 		"Welcome to our city - the city of unlimited opportunities.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction()	onbehalf:play()			end		-- 		"On behalf of the citizens of this town I welcome you.",
		if timer >12	and stepdone==3 then stepdone=4 Talkies.onAction() welcometoour:play()		end		-- 		"Welcome to our city, whose beauty eclipses that of the gods themselves.",
		if timer >17 	and stepdone==4 then stepdone=5 Talkies.onAction() citizens:play()			end		-- 		"Citizens, please remain calm.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	thereisno:play()		end		-- 		"There is no imminent danger.",
		if timer >21 	and stepdone==6 then stepdone=7 Talkies.onAction()	thesinking:play()		end		-- 		"The sinking of the northern part of our island is only temporary.",
		if timer >24.5 	and stepdone==7 then stepdone=8 Talkies.onAction()	 wehave:play()			end		-- 		"We have the situation completely under control.",
		if timer >27 	and stepdone==8 then stepdone=9 Talkies.onAction()	theshipfrom:play() 		end		-- 		"The ship from Lemury has now docked at pier #5. This ship continues on to Utopia, Mu and Atlantis.",
		if timer >30 	and stepdone==9 then stepdone=10 			theshipcontinues:play() 		end		-- 		"The ship from Lemury has now docked at pier #5. This ship continues on to Utopia, Mu and Atlantis.",		
		if timer >34 	and stepdone==10 then stepdone=11 Talkies.onAction() withfirst:play()		end		-- 		"With First Fishermen Insurance, your house is covered against damage caused by fire, tornadoes, hurricanes, acts of God and dragons’ raids. 
		if timer >42 	and stepdone==11 then stepdone=12 Talkies.onAction()  because:play()		end		--		"Because fishermen think of everything, choose First Fisherman.",
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction() where:play()			end		-- 		"Where are you running?",
		if timer >51 	and stepdone==13 then stepdone=14 Talkies.onAction()  tomoon:play()			end		-- 		"To Moon Street. I need to order eight swords.",
		if timer >53 	and stepdone==14 then stepdone=15 Talkies.onAction()  whyso:play() 	 		end		-- 		"Why so many swords?",
		if timer >55 	and stepdone==15 then stepdone=16 Talkies.onAction() youhavent:play()		end		-- 		"You haven’t heard?!? For every eight swords you purchase, you get a pair of bronze earrings absolutely FREE!",
		if timer >58.5 	and stepdone==16 then stepdone=17 Talkies.onAction() wait:play() 			end		-- 		"Wait for me. I’m coming with you!",
		if timer >60 	and stepdone==17 then stepdone=18 Talkies.onAction() thecity:play() 		end		-- 		"The City Council has decided to build an aqueduct, thereby providing direct water service to each home.",
		
		--if timer >62 	and stepdone==16 then stepdone=1 Talkies.onAction()  						end		-- 		"The City Court kindly invites you to the annual town festival of interrogations and executions. Proper attire required.",
		
		if timer >67 	and stepdone==18 then stepdone=19 Talkies.onAction() citizens2:play() 		end		-- 		"Citizens, our employees will be stopping by individual homes tomorrow after midnight in order to rid your home of demons and ghosts. We ask that you kindly let them in.",
		if timer >77 	and stepdone==19 then stepdone=20 Talkies.onAction() ourchief:play()		end		-- 		"Our chief heretic Demikuls will present a lecture this evening in the small auditorium entitled ’Do the gods really exist? or Rest assured they cannot harm you!’",
		if timer >86 	and stepdone==20 then stepdone=21 Talkies.onAction() comeonvisit:play() 	end		-- 		"Come visit our seafood restaurant located at the Main Square. Delicious fish specialties and...",
		if timer >90 	and stepdone==21 then stepdone=22 Talkies.onAction() 	statuestop()		end		-- 		"Oh, excuse me.",
		if timer >91 	and stepdone==22 then stepdone=23 Talkies.onAction() what:play()			fish2talk() end		-- 		"What?! A fish restaurant?",
		if timer >93 	and stepdone==23 then stepdone=24 Talkies.onAction() areyouserious:play()	fish1talk() end		-- 		"Are you serious?",
		if timer >96 	and stepdone==24 then stepdone=25 Talkies.onAction() howdisgusting:play()	fish2talk() end		-- 		"How disgusting!",
		if timer >100 	and stepdone==25 then stepdone=26 Talkies.onAction() thatsculpture:play()	fish1talk() end		-- 		"That sculpture obviously hasn’t noticed that the city sank a long time ago.",
		if timer >104 	and stepdone==26 then stepdone=27 Talkies.onAction() itsprobably:play()		fish2talk() end		-- 		"It’s probably some kind of automaton. It still thinks that the city is alive.",
		if timer >106 	and stepdone==27 then stepdone=28 Talkies.onAction() orit:play()			fish2talk() end		-- 		"Or it just can’t face the truth.",
		if timer >110 	and stepdone==28 then stepdone=29 Talkies.onAction() imgetting:play()		fish1talk() end		-- 		"I’m getting a headache from that head.",
		if timer >112 	and stepdone==29 then stepdone=30 Talkies.onAction() thathead:play()		fish1talk() end		-- 		"That head is giving me a headache.",
		if timer >116 	and stepdone==30 then stepdone=31 Talkies.onAction() letstry:play()			fish2talk() end		-- 		"Let’s try to shut that head up somehow.",
		if timer >118 	and stepdone==31 then stepdone=32 Talkies.onAction() why:play()				fish1talk() end		-- 		"Why? We’re getting invaluable information about the everyday life of our ancestors.",
		if timer >178 	and stepdone==32 then stepdone=33 Talkies.onAction() ifwecould:play()		fish1talk() end		-- 		"If we could get all these crabs up near the head...",
		if timer >122 	and stepdone==33 then stepdone=34 Talkies.onAction() thenwhat:play()		fish2talk() end		-- 		"...then...?",
		if timer >126 	and stepdone==34 then stepdone=35 Talkies.onAction() wait:play()			fish1talk() end		-- 		"Wait, I have to think about it for a moment...",
		if timer >129 	and stepdone==35 then stepdone=36 Talkies.onAction() justimagine:play()		fish2talk() end		-- 		"Just imagine - that head has been like doing this all these years...",
		if timer >132 	and stepdone==36 then stepdone=37 Talkies.onAction() sowhat:play()			fish1talk() end		-- 		"So what?",
		if timer >136 	and stepdone==37 then stepdone=38 Talkies.onAction() think:play()	 		fish2talk() end		-- 		"Well think about how these poor crabs must feel!",
		if timer >138 	and stepdone==38 then stepdone=39 Talkies.onAction() 		 				end		-- 		
		if timer >140 	and stepdone==39 then stepdone=30 Talkies.onAction()	 					end		-- 		

	elseif language=="es" or language=="fr" or language=="de" or language=="nl" then
	
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction() 	welcome:play()			end		-- 		"Welcome to the most beautiful city under the sun.",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction()	welcometo:play() 		end		-- 		"Welcome to our city - the city of unlimited opportunities.",
		if timer >9 	and stepdone==2 then stepdone=3 Talkies.onAction()	onbehalf:play()			end		-- 		"On behalf of the citizens of this town I welcome you.",
		if timer >13	and stepdone==3 then stepdone=4 Talkies.onAction() welcometoour:play()		end		-- 		"Welcome to our city, whose beauty eclipses that of the gods themselves.",
		if timer >18 	and stepdone==4 then stepdone=5 Talkies.onAction() citizens:play()			end		-- 		"Citizens, please remain calm.",
		if timer >21 	and stepdone==5 then stepdone=6 Talkies.onAction()	thereis:play()			end		-- 		"There is no imminent danger.",
		if timer >24 	and stepdone==6 then stepdone=7 Talkies.onAction()	thesinking:play()		end		-- 		"The sinking of the northern part of our island is only temporary.",
		if timer >28 	and stepdone==7 then stepdone=8 Talkies.onAction() wehave:play()			end		-- 		"We have the situation completely under control.",
		if timer >32 	and stepdone==8 then stepdone=9 Talkies.onAction()	theshipfrom:play() 		end		-- 		"The ship from Lemury has now docked at pier #5. This ship continues on to Utopia, Mu and Atlantis.",
		if timer >36 	and stepdone==9 then stepdone=10 					 theshipcontinues:play()end		-- 		"With First Fishermen Insurance, your house is covered against damage caused by fire, tornadoes, hurricanes, acts of God and dragons’ raids. 
		if timer >40.5 	and stepdone==10 then stepdone=11 Talkies.onAction() withfirst:play()	 	end		-- 		"Because fishermen think of everything, choose First Fisherman.",
		if timer >54 	and stepdone==11 then stepdone=12 Talkies.onAction() because:play() 		end		-- 		"Where are you running?",
		if timer >57 	and stepdone==12 then stepdone=13 Talkies.onAction() where:play()	 		end		-- 		"To Moon Street. I need to order eight swords.",
		if timer >62 	and stepdone==13 then stepdone=14 Talkies.onAction() tomoon:play() 			end		-- 		"Why so many swords?",
		if timer >68 	and stepdone==14 then stepdone=15 Talkies.onAction() whyso:play() 			end		-- 		"You haven’t heard?!? For every eight swords you purchase, you get a pair of bronze earrings absolutely FREE!",
		if timer >72 	and stepdone==15 then stepdone=16 Talkies.onAction() youhavent:play() 		end		-- 		"Wait for me. I’m coming with you!",
		if timer >74 	and stepdone==16 then stepdone=17 Talkies.onAction() wait:play() 			end		-- 		"The City Court kindly invites you to the annual town festival of interrogations and executions. Proper attire required.",
		if timer >84 	and stepdone==17 then stepdone=18 Talkies.onAction() thecity:play() 		end		-- 		"The City Council has decided to build an aqueduct, thereby providing direct water service to each home.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() citizens2:play()		end		-- 		"Citizens, our employees will be stopping by individual homes tomorrow after midnight in order to rid your home of demons and ghosts. We ask that you kindly let them in.",
		if timer >100 	and stepdone==19 then stepdone=20 Talkies.onAction() ourchief:play() 		end		-- 		"Our chief heretic Demikuls will present a lecture this evening in the small auditorium entitled ’Do the gods really exist? or Rest assured they cannot harm you!’",
		if timer >105 	and stepdone==20 then stepdone=21 Talkies.onAction() comeonvisit:play() 	end		-- 		"Come visit our seafood restaurant located at the Main Square. Delicious fish specialties and...",
		if timer >110 	and stepdone==21 then stepdone=22 Talkies.onAction()	 					end		-- 		"Oh, excuse me.",
		if timer >115 	and stepdone==22 then stepdone=23 Talkies.onAction() what:play()			fish2talk() end		-- 		"What?! A fish restaurant?",
		if timer >120 	and stepdone==23 then stepdone=24 Talkies.onAction() areyouserious:play()	fish1talk() end		-- 		"Are you serious?",
		if timer >125 	and stepdone==24 then stepdone=25 Talkies.onAction() howdisgusting:play()	fish2talk() end		-- 		"How disgusting!",
		if timer >130 	and stepdone==25 then stepdone=26 Talkies.onAction() thatsculpture:play()	fish1talk() end		-- 		"That sculpture obviously hasn’t noticed that the city sank a long time ago.",
		if timer >135 	and stepdone==26 then stepdone=27 Talkies.onAction() itsprobably:play()		fish1talk() end		-- 		"It’s probably some kind of automaton. It still thinks that the city is alive.",
		if timer >140 	and stepdone==27 then stepdone=28 Talkies.onAction() orit:play()			fish2talk() end		-- 		"Or it just can’t face the truth.",
		if timer >145 	and stepdone==28 then stepdone=29 Talkies.onAction() imgetting:play()		fish2talk() end		-- 		"I’m getting a headache from that head.",
		if timer >150 	and stepdone==29 then stepdone=30 Talkies.onAction() thathead:play()		fish1talk() end		-- 		"That head is giving me a headache.",
		if timer >155 	and stepdone==30 then stepdone=31 Talkies.onAction() letstry:play()			fish2talk() end		-- 		"Let’s try to shut that head up somehow.",
		if timer >160 	and stepdone==31 then stepdone=32 Talkies.onAction() why:play()				fish1talk() end		-- 		"Why? We’re getting invaluable information about the everyday life of our ancestors.",
		if timer >165 	and stepdone==32 then stepdone=33 Talkies.onAction() ifwecould:play()		fish1talk() end		-- 		"If we could get all these crabs up near the head...",
		if timer >170 	and stepdone==33 then stepdone=34 Talkies.onAction() thenwhat:play()		fish2talk() end		-- 		"...then...?",
		if timer >175 	and stepdone==34 then stepdone=35 Talkies.onAction() waitihave:play()		fish1talk() end		-- 		"Wait, I have to think about it for a moment...",
		if timer >180 	and stepdone==35 then stepdone=36 Talkies.onAction() justimagine:play()		fish2talk() end		-- 		"Just imagine - that head has been like doing this all these years...",
		if timer >185 	and stepdone==36 then stepdone=37 Talkies.onAction() sowhat:play()	 		fish1talk() end		-- 		"So what?",
		if timer >190 	and stepdone==36 then stepdone=38 Talkies.onAction() think:play() 			fish2talk() end		-- 		"Well think about how these poor crabs must feel!",
	end

end

function subtitle21advertising1()
		if timer >2 	and stepdone==0 then stepdone=1 					 where:play()			end		-- 		"Where are you running?",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction()  tomoon:play()			end		-- 		"To Moon Street. I need to order eight swords.",
		if timer >7.5 	and stepdone==2 then stepdone=3 Talkies.onAction()  whyso:play() 	 		end		-- 		"Why so many swords?",
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction() youhavent:play()		end		-- 			"You haven’t heard?!? For every eight swords you purchase, you get a pair of bronze earrings absolutely FREE!",
		if timer >16 	and stepdone==4 then stepdone=5 Talkies.onAction() wait:play() 			end		-- 			"Wait for me. I’m coming with you!",
end

function subtitle21advertising2()
		if timer >2 	and stepdone==0 then stepdone=1 					 withfirst:play()	end		-- 		"With First Fishermen Insurance, your house is covered against damage caused by fire, tornadoes, hurricanes, acts of God and dragons’ raids. 
		if timer >15 	and stepdone==1 then stepdone=2 Talkies.onAction() because:play() 		end		-- 		"Because fishermen think of everything, choose First Fisherman.",
end

function subtitle20skull()
		if timer >0 	and stepdone==0 then stepdone=1  								end		-- 	
		if timer >9 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
end

function subtitle22()

		--level 22 U.F.O.

		if timer >0 	and stepdone==0 then stepdone=1 				 	wemustbe:play()				fish1talk() end		-- 		"We must be getting near the end of our mission. 
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 											end		-- 		"This is surely the UFO that destroyed our house.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() areyousure:play()			fish2talk() end		-- 		"Are you sure? 
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction() 											end		-- 		"This is only the eighth level.",		
		if timer >13 	and stepdone==4 then stepdone=5 Talkies.onAction()	thesesteel:play()			fish2talk() end		-- 		"These steel cylinders are pretty obnoxious.",
		if timer >16	and stepdone==5 then stepdone=6 twotalkiesactions() thereisjust:play()			fish2talk() end		-- 		"There is just too much steel here.",
		if timer >18 	and stepdone==6 then stepdone=7 twotalkiesactions() imyself:play()				fish1talk() end		-- 		"It could be worse. I for one can imagine myself a room where there is nothing but steel.",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction()	mygod:play()				fish2talk() end		-- 		"My goodness! What should I do in such a level?",
		if timer >29 	and stepdone==8 then stepdone=9 Talkies.onAction()	youwould:play()				fish1talk() end		-- 		"You would let me go in front, for a change.",
		if timer >32 	and stepdone==9 then stepdone=10 Talkies.onAction() itsinteresting:play()		fish2talk() end		-- 		"It’s interesting that this tangled steel forestalls our entry into that UFO more effectively than any security system.",
		if timer >35 	and stepdone==10 then stepdone=11 Talkies.onAction()	 ikeeptelling:play()	fish1talk() end		-- 		"I keep telling you that our life is controlled by some higher, strange consciousness.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() icansee:play()	 			fish2talk() end		-- 		"I can see that, too. But I am not that sure it’s higher.",
		if timer >55 	and stepdone==12 then stepdone=13 Talkies.onAction() whatcanwe:play()			fish1talk() end		-- 		"What can we find inside, once we manage to get there?",
		if timer >60 	and stepdone==13 then stepdone=14 Talkies.onAction() secrets:play()	 			fish2talk() end		-- 		"Secrets of another civilization, new technologies, new sources of energy...",
		if timer >65 	and stepdone==14 then stepdone=15 Talkies.onAction() mostimportantly:play()		fish1talk() end		-- 		"Most importantly, we should try to find out why they came at all.",
		if timer >70 	and stepdone==15 then stepdone=16 Talkies.onAction() youknow:play()	 			fish1talk() end		-- 		"You know, most of all I wonder why they crashed just here.",
		if timer >75 	and stepdone==16 then stepdone=17 Talkies.onAction() didntyousee:play()	 		fish2talk() end		-- 		"Didn’t you see it? Everything happens just here.",
		if timer >80 	and stepdone==17 then stepdone=18 Talkies.onAction() idontknow:play()			fish2talk() end		-- 		"I don’t know why the UFOs keep crashing at our house, either.",
		if timer >85 	and stepdone==18 then stepdone=19 Talkies.onAction()	 						end		-- 		
		if timer >90 	and stepdone==19 then stepdone=20 Talkies.onAction()	 						end		-- 		
		if timer >95 	and stepdone==20 then stepdone=21 Talkies.onAction()	 						end		-- 		

end

function subtitle23()

		--level 23 The columns

		if timer >0 	and stepdone==0 then stepdone=1 				 	ithinkthat:play()		fish1talk() end		-- 		"I think that we are finally on the trail of that mysterious city.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() itlooksmagnificent:play()	fish2talk() end		-- 		"It looks magnificent.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	nowweonly:play()		fish1talk() end		-- 		"Now we only have to find out why it sank and we are done.",
		if timer >12	and stepdone==3 then stepdone=4 Talkies.onAction() butitmaytake:play()		fish2talk() end		-- 		"But it may take us a little while.",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() leavemealone:play()		fish1talk() end		-- 		"Leave me alone. I adore the classic beauty of this temple.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	whoissitting:play()		fish2talk() end		-- 		"Who is sitting on that chair?",
		if timer >22 	and stepdone==6 then stepdone=7 Talkies.onAction()	itsacopyoffeidios:play() fish1talk() end		-- 		"It’s a copy of Feidios’ Zeus. One of the seven wonders of the world.",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction() mmitwasanotherage:play() fish2talk() end		-- 		"Hmm. It was another age.",
end

function subtitle23Zeus()

		if timer >0 	and stepdone==0 then stepdone=1						becareful:play() 		fish1talk() end		-- 		"Be careful! Try not to damage it!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() andnoweaseit:play()		fish1talk() end		-- 		"And now ease it down, carefully.", 	
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction() suchawaste:play()		fish1talk() end		-- 		"Such a waste!",
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction() youbarbarian:play()		fish1talk() end		-- 		"You barbarian! Can’t you be a little bit cautious?!",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() ididntlikethis:play()	fish1talk() end		-- 		"I didn’t like this sculpture anyway.",

end

function subtitle24()

		--level 24 Unevent pavement
		if timer >0 	and stepdone==0 then stepdone=1 itwouldbeeasier:play() 		statuetalk()		end	-- "It would be easiest if you",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() itwouldbebetter:play() 		end	-- "It would be better off if you",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()	itwouldhelpifyou:play() 	end -- "It would help you if you",
		if timer >6.5	and stepdone==3 then stepdone=4 Talkies.onAction() 	ihaveanidea:play() 			end -- "I have an idea. What if you",
		if timer >9.5 	and stepdone==4 then stepdone=5 Talkies.onAction() 	sorrytointerrupt:play()	    end -- "Sorry to interrupt, but what if you",
		if timer >12 	and stepdone==5 then stepdone=6 Talkies.onAction()	demolishsomewall:play() 	end -- "demolish some wall.",
		if timer >16 	and stepdone==6 then stepdone=7 Talkies.onAction()	breakoff:play()			    end --	"break off a few stones from the wall and plug the holes with them.",
		if timer >19 	and stepdone==7 then stepdone=8 Talkies.onAction() plugthose:play() 			end --	"plug those holes with something so that that hooked steel wouldn’t get stuck there.",
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction()	smashthatpillar:play()		end --	"smash that pillar by the exit.",
		if timer >28 	and stepdone==9 then stepdone=10 Talkies.onAction() sawoff:play() 				end --	"saw off some of that hooked steel.",
		if timer >34 	and stepdone==10 then stepdone=11 Talkies.onAction() rearrange:play()	 		end --"rearrange the objects so that you can swim out.",
		if timer >38 	and stepdone==11 then stepdone=12 Talkies.onAction() solveit:play()				end --"solve it somehow and get to the next level.",
		if timer >42 	and stepdone==12 then stepdone=13 Talkies.onAction() start:play()			 	end --"start to think about it really hard.",
		if timer >48 	and stepdone==13 then stepdone=14 Talkies.onAction() swim:play() 				end --"swim out through that hole on the left.",
		if timer >52 	and stepdone==14 then stepdone=15 Talkies.onAction() stuffyourself:play() statuestop()		fish2talk() end		-- 		"Stuff yourself with your advice",
		if timer >58 	and stepdone==15 then stepdone=16 Talkies.onAction() weknowthat:play()			fish2talk() end		-- 		"We know that very well.",
		if timer >62 	and stepdone==16 then stepdone=17 Talkies.onAction() getlost:play()				fish2talk() end		-- 		"Get lost.",
		if timer >68 	and stepdone==17 then stepdone=18 Talkies.onAction() mypatience:play()			fish2talk() end		--	 	"My patience is running out.",
		if timer >74 	and stepdone==18 then stepdone=19 Talkies.onAction() illsmashit:play()			fish2talk() end		-- 		"Arrgh... I’ll smash it to pieces.",
		if timer >80 	and stepdone==19 then stepdone=20 Talkies.onAction() ignoreit:play()			fish1talk() end		-- 		"Ignore it, let it talk.",
		if timer >84 	and stepdone==20 then stepdone=21 Talkies.onAction() letusthink:play()			fish1talk() end		-- 		"Let us think.",
		if timer >88 	and stepdone==21 then stepdone=22 Talkies.onAction() thiswassomeadvice:play()	fish1talk() end		-- 		"This was some advice, again.",
		if timer >92 	and stepdone==22 then stepdone=23 Talkies.onAction() itsashame:play()			fish1talk() end		-- 		"It’s a shame I have no ears. I could plug them.",
		if timer >96 	and stepdone==23 then stepdone=24 Talkies.onAction() iamfeduptomygills:play()	fish1talk() end		-- 		"I am fed up to my gills with its nonsense.",
		if timer >100 	and stepdone==24 then stepdone=25 Talkies.onAction()							fishstop() end
		

		
		
		
		
			
			
			
			
			
	
		
		
		
end

function subtitle25()

		--level 25 the pyramids

		if timer >0 	and stepdone==0 then stepdone=1 					 wherearewe:play()			fish2talk() end		-- 		"Where are we now?",
		if timer >2.5 	and stepdone==1 then stepdone=2 Talkies.onAction() thepyramids:play()			fish1talk() end		-- 		"The Pyramids... Notice how the classical motifs mix in this city.",
		if timer >8.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	whatisthat:play()			fish2talk() end		-- 		"What is that crawling around over there?",
		if timer >11	and stepdone==3 then stepdone=4 Talkies.onAction() youcantseeit:play()			fish1talk() end		--	 	"You can’t see it from here. It’s on the other side of wall.",
		if timer >14.5 	and stepdone==4 then stepdone=5 Talkies.onAction() lookthewoman:play()			fish2talk() end		-- 		"Look, the woman is bored!",
		if timer >16.5 	and stepdone==5 then stepdone=6 Talkies.onAction()	doyouthink:play()			fish1talk() end		-- 		"Do you think that this is taking us too long?",
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()	tryityourself:play()		fish2talk() end		-- 		"Try it yourself, if you’re so clever.",
		if timer >22 	and stepdone==7 then stepdone=8 Talkies.onAction() whatwouldyou:play()			fish2talk() end		-- 		"What should we say?",
		if timer >24 	and stepdone==8 then stepdone=9 Talkies.onAction()	youdonthave:play() 			fish2talk() end		-- 		"You don’t have to carry anything.",
		if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction()	dontbeafraid:play() 		fish1talk() end		-- 		"Don’t be afraid.",
		if timer >27 	and stepdone==9 then stepdone=10 Talkies.onAction() whatisitwritten:play()	 	fish2talk() end		-- 		"What is it written on these tablets?",
		if timer >33 	and stepdone==10 then stepdone=11 Talkies.onAction() solongandthanks:play() 	fish1talk() end		-- 		"So long and thanks for all the fish.",
		if timer >32 	and stepdone==11 then stepdone=12 Talkies.onAction() 							fishstop() end		-- 		
end

function subtitle26()

		--level 26 a bit of music

		if timer >0 	and stepdone==0 then stepdone=1 				 therearemostinteresting:play() fish1talk()	end		-- 		"There are most interesting things in this city!",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() thepoorcrab:play()			fish2talk()	end		-- 		"The poor crab!",
		if timer >6.5 	and stepdone==2 then stepdone=3 Talkies.onAction() weshouldturnitoff:play()		fish1talk()	end		-- 		"We should turn it off!",
		if timer >8		and stepdone==3 then stepdone=4 Talkies.onAction() ohno:play()					fish2talk()	end		--	 	"Oh, noo...",
		--if timer >12.5 	and stepdone==4 then stepdone=5 Talkies.onAction() 								end		-- 		"And it’s over.",
		--if timer >16.5 	and stepdone==5 then stepdone=6 Talkies.onAction()								end		-- 		"Thank you player. We couldn’t bear it any longer.",
		--if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()									end		-- 		"But we have to push it down anyway.",
		--if timer >22 	and stepdone==7 then stepdone=8 Talkies.onAction() 									end		-- 		"Now listen to the bit of music!",
		--if timer >24 	and stepdone==8 then stepdone=9 Talkies.onAction()	 								end		-- 		"",
		--if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction()	 								end		-- 		"Pa pada pa paaapa pa pada pa paaa...",
		--if timer >27 	and stepdone==9 then stepdone=10 Talkies.onAction()	 								end		-- 		"",
		--if timer >33 	and stepdone==10 then stepdone=11 Talkies.onAction()	 							end		-- 		
end

function subtitle27()

		--level 27 Crab freak show

		if timer >0 	and stepdone==0 then stepdone=1 				 	imtrapped:play()			fish1talk() end		-- 		"I am trapped here.",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() iwanttoget:play()			fish1talk() end		-- 		"I want to get out.",
		if timer >6.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	howcaniget:play()			fish1talk() end		-- 		"How I can get out of here?",
		if timer >8.5 	and stepdone==3 then stepdone=4 Talkies.onAction()	imclosed:play()				fish1talk() end		-- 		"I am closed in a room without doors!",
		if timer >12 	and stepdone==4 then stepdone=5 Talkies.onAction()	trytoloosen:play()			fish1talk() end		-- 		"Try to loosen some stone in the wall.",
end

function subtitle27door()

		if timer >0		and stepdone==0 then stepdone=1  					 seenow:play()				fish2talk() end		--	 	"See, now you got out.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() soweare:play()				fish2talk() end		-- 		"So, we are together again.",
		if timer >6 	and stepdone==2 then stepdone=3 Talkies.onAction() isntthatcrab:play()			fish1talk() end		-- 		"Isn’t that crab a little bit strange?",
		if timer >9 	and stepdone==3 then stepdone=4 Talkies.onAction()	 whichone:play()			fish1talk() end		-- 		"Which one do you mean?",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction()	 theupper:play()			fish2talk() end		-- 		"The upper one, of course.",
		if timer >13 	and stepdone==5 then stepdone=6 Talkies.onAction() thelower:play()	 			fish2talk() end		-- 		"The lower one, of course.",
		if timer >15 	and stepdone==6 then stepdone=7 Talkies.onAction()	ithinkits:play() 			fish1talk() end		-- 		"I think it’s normal.",
		if timer >17 	and stepdone==7 then stepdone=8 Talkies.onAction() yesitsalittle:play()			fish1talk() end		-- 		"Yes, it’s a little weird.",
		if timer >21 	and stepdone==8 then stepdone=9 Talkies.onAction() weshouldbe:play()			fish2talk() end		-- 		"We should be glad we have all these balls.",
		if timer >24 	and stepdone==9 then stepdone=10 Talkies.onAction() thiscity:play()	 			fish1talk() end		-- 		"This city looks suspiciously well preserved...",
		if timer >27 	and stepdone==10 then stepdone=11 Talkies.onAction() ihaveafeeling:play()		fish1talk() end		-- 		"I have a feeling that I am missing something.",
		if timer >30 	and stepdone==11 then stepdone=12 Talkies.onAction() yourfeelings:play()		fish2talk() end		-- 		"Your feelings... think about how to get us out.",
		if timer >34.5 	and stepdone==12 then stepdone=13 Talkies.onAction() nowait:play()	 			fish1talk() end		-- 		"No, wait, watch all these crabs for a moment... look into their eyes...",
		if timer >40 	and stepdone==13 then stepdone=14 Talkies.onAction() theireyes:play()			fish1talk() end		-- 		"Their eyes move in accord with each other. As if they are controlled by some common superconsciousness.",
		if timer >48 	and stepdone==14 then stepdone=15 Talkies.onAction()	butofcourse:play() 		fish2talk() end		-- 		"But of course, they are controlled by computer program. Just like the undulating of water and your silly talk.",
		if timer >56 	and stepdone==15 then stepdone=16 Talkies.onAction()	 didweever:play()		fish1talk() end		-- 		"Did we ever see a sculpture that would be silent for so long?",
		if timer >60 	and stepdone==16 then stepdone=17 Talkies.onAction()	 ithink:play()			fish2talk() end		-- 		"I think it’s only waiting for the best opportunity.",
		
end		
		
		

function subtitle28()

		--level 28 Another elevator

		if timer >0 	and stepdone==0 then stepdone=1 				 	another:play()					fish2talk() end		-- 		"Another elevator? How many more are there to come?",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() 	icanseesome:play()				fish1talk() end		-- 		"I can see some differences here... And somehow I don’t think it’s going to be easier.",
		if timer >9 	and stepdone==2 then stepdone=3 Talkies.onAction()		thelastone:play()			fish1talk() end		-- 		"The last one had a little bit more advantageous architecture, I think.",
		if timer >13	and stepdone==3 then stepdone=4 Talkies.onAction() 	canyousee:play()				fish1talk() end		--	 	"Can you see that thing in the lower left? It must be some ancient religious symbol.",
		if timer >18 	and stepdone==4 then stepdone=5 Talkies.onAction() 	fortunately:play()				fish2talk() end		-- 		"Fortunately, I can’t see much of it.",
		if timer >20 	and stepdone==5 then stepdone=6 Talkies.onAction()		ifyouareright:play()		fish2talk() end		-- 		"If you are right, I’d rather not know what kind of religion it was.",
		if timer >25 	and stepdone==6 then stepdone=7 Talkies.onAction()		imafraid:play()				fish1talk() end		-- 		"I am afraid that if we use that strange symbol we could unwillingly initiate some dark ritual.",
		if timer >30 	and stepdone==7 then stepdone=8 Talkies.onAction() 	ijusthope:play()				fish2talk() end		-- 		"I just hope it won’t drop on my head.",
		if timer >33 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	anotherskull:play()			fish2talk() end		-- 		"Another skull. It’s worse than a morgue.",
		if timer >36 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	itsworsethanhamlet:play()	fish1talk() end		-- 		"It’s worse than Hamlet.",
		if timer >37 	and stepdone==9 then stepdone=10 Talkies.onAction()	 								fish1talk() end		-- 		
		--if timer >33 	and stepdone==10 then stepdone=11 Talkies.onAction()	 							end		-- 		
end

function subtitle28skull()
		if timer >0 	and stepdone==0 then stepdone=1   								end		-- 	
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
		
end

function subtitle29()

		--level 29 and how it was

		if timer >0 	and stepdone==0 then stepdone=1							loandbehold:play()			fish1talk() end		-- 		"Lo and behold!",
		if timer >3 	and stepdone==1 then stepdone=2 threetalkiesactions() 	justimagine:play()			fish2talk() end		-- 		"Just imagine!",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()		whocouldhave:play()			fish1talk() end		-- 		"Who could have guessed?!",
		if timer >7		and stepdone==3 then stepdone=4 Talkies.onAction() 		suchcarelessness:play()		fish2talk() end		--	 	"Such carelessness.",
		if timer >9 	and stepdone==4 then stepdone=5 Talkies.onAction()	 	thousands:play()			fish1talk() end		-- 		"Thousands perished - the whole city disappeared in the waves because of such ineptness.",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()		unpluggedplug:play()		fish2talk() end		-- 		"Unplugged plug.",
		if timer >17 	and stepdone==6 then stepdone=7 Talkies.onAction()		anembarrasing:play()		fish1talk() end		-- 		"An embarrassing blunder.",
		if timer >22 	and stepdone==7 then stepdone=8 Talkies.onAction() 		whatarewetodo:play()		fish2talk() end		-- 		"What are we to do with it?",
		if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	wecantryto:play()			fish1talk() end		-- 		"We can try to put it back in place.",
		if timer >30 	and stepdone==9 then stepdone=10 Talkies.onAction()	 	andthen:play()				fish2talk() end		-- 		"And then? Shall we drink the water that poured in, or what?",
		if timer >35 	and stepdone==10 then stepdone=11 Talkies.onAction()	wecanputitback:play() 		fish1talk() end		-- 		"We can put it back in place as a token of our esteem of the citizens’ heroic effort to keep it afloat. As a memorial to their industrious, adroit and... persistent nature.",
		if timer >50 	and stepdone==11 then stepdone=12 Talkies.onAction()	ofwhat:play() 				fish2talk() end		-- 		"Of what? ‘Where are you running? I need to order eight swords.’ The Providence itself unplugged that hole. Just imagine you’d hear such things at home. Day by day.",
		
		
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() andmoreover:play()				fish2talk() end		-- 		"And moreover: is it likely that anybody would ever come here? Only sepias will nibble it occasionally.",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() weargoingtofeel:play()	 		fish1talk() end		-- 		"We are going to feel good about it.",
		if timer >80 	and stepdone==14 then stepdone=15 Talkies.onAction() wearegoingtofeeldown:play()	fish2talk() end		-- 		"We are going to feel down about it. Do you think the boss is going to believe it? A giant unplugged plug? And what have you done with it? We plugged it again. He, he, he.",
		if timer >85 	and stepdone==15 then stepdone=16 Talkies.onAction() youmightberight:play()	 		fish1talk() end		-- 		"You might be right. Maybe it would be better to take it along.",
		if timer >90 	and stepdone==16 then stepdone=17 Talkies.onAction() ithinkso:play()				fish2talk() end		-- 		"I think so, too.",
		if timer >105 	and stepdone==17 then stepdone=18 Talkies.onAction() letsgettowork:play()	 		fish1talk() end		-- 		"Let ’s get to work.",
		if timer >110 	and stepdone==18 then stepdone=19 Talkies.onAction() whatifwejust:play()	 		fish2talk() end		-- 		"What if we just leave that plug here?",
		if timer >115 	and stepdone==19 then stepdone=20 Talkies.onAction() whatwouldtheagency:play()		fish1talk() end		-- 		"What would the Agency say?",
		if timer >120 	and stepdone==20 then stepdone=21 Talkies.onAction() idontthink:play() 				fish1talk() end		-- 		"I don’t think I mind what the boss is going to think about me.",
		if timer >125 	and stepdone==21 then stepdone=22 Talkies.onAction() holdon:play()					fish2talk() end		-- 		"Hold on. We are sure to solve it.",
		if timer >130 	and stepdone==22 then stepdone=23 Talkies.onAction() thedarnedplug:play()			fish2talk() end		-- 		"The darned plug. What if we just make up something.",
		if timer >135 	and stepdone==23 then stepdone=24 Talkies.onAction() likewhat:play()				fish1talk() end		-- 		"Like what, for example?",
		if timer >140 	and stepdone==24 then stepdone=25 Talkies.onAction() wellwecouldsay:play()			fish2talk() end		-- 		"Well, we could say that the city in fact never existed.",
		if timer >145 	and stepdone==25 then stepdone=26 Talkies.onAction() wellwecouldsay2:play()			fish2talk() end		-- 		"Well, we could say that the city sank because the Arctic ice had melted.",
		if timer >150 	and stepdone==26 then stepdone=27 Talkies.onAction() anearthquake:play()			fish2talk() end		-- 		"An earthquake could have sunk the city, for example.",
		if timer >155 	and stepdone==27 then stepdone=28 Talkies.onAction() atsunami:play()				fish2talk() end		-- 		"A tsunami might have sunk the city, for example.",
		if timer >160 	and stepdone==28 then stepdone=29 Talkies.onAction() wecouldtrytosay:play()			fish2talk() end		-- 		"We could try to say that a volcano erupted in the center of the city.",
		if timer >165 	and stepdone==29 then stepdone=30 Talkies.onAction() thatisjustnonsense:play()		fish1talk() end		-- 		"That is just nonsense.",
		if timer >170 	and stepdone==30 then stepdone=31 Talkies.onAction() thistimeourgoalis:play()		fish1talk() end		-- 		"This time our goal is to get that plug out.",
		if timer >175 	and stepdone==31 then stepdone=32 Talkies.onAction() 								fish1talk() end		-- 		
		
		
end

function subtitle29end()
		if timer >0 	and stepdone==0 then stepdone=1 					wesucceed:play()		end		--	"We succeeded in discovering the right cause of sinking the city.
		if timer >6 	and stepdone==1 then stepdone=2 Talkies.onAction() itwasawellknown:play()	end		--  It was a well known artifact called atlantic relief, 
		if timer >11.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	uptonow:play()			end		-- 	up to now considered by mistake to be a depiction of extraterrestrial visit,
		if timer >19.5	and stepdone==3 then stepdone=4 Talkies.onAction() thatgaveusahint:play()	end		-- 		 that gave us a hint. 
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() atthesametime:play()		end		--  At the same time, we recommend increasing the surveillance
		if timer >32 	and stepdone==5 then stepdone=6 Talkies.onAction()	inorder:play()			end		--of the plugs on all continents and bigger islands in order not to repeat similar awkward catastrophe again."
--		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()	:play()			end		--
		--if timer >38 	and stepdone==7 then stepdone=8 Talkies.onAction()	:play()	end		-- 		
		
end

function subtitle30crab()
		if timer >0 	and stepdone==0 then stepdone=1 					 whowokemeup:play()		end		-- 		"Who woke me up?",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() leavemealone:play()		end		-- 		"Leave me alone, I am sleeping here!",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()	 whatdoyouwant:play()	end		-- 		"What do you want from me?!?",
		if timer >7		and stepdone==3 then stepdone=4 Talkies.onAction()  donttouchme:play()		end		--	 	"Don’t touch me!",
end

function subtitle30crabups()
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction()	 ups:play()				end		-- 		"Ooops.",
end

function subtitle30()

		--level 30	

		if timer >0 	and stepdone==0 then stepdone=1 				 	ihavenever:play()	fish1talk() end		-- 	"I have never seen such violet corals.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() idontlike:play()		fish1talk() end		-- 	"I don’t like violet corals.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	iloveviolet:play()	fish2talk() end		--	"I love violet corals...",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	theylooknice:play()	fish2talk() end		--	"They look nice to me.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	ihope:play()		fish2talk() end		-- 	"I hope there will be even more interesting corals in the levels to come.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	godforbid:play()	fish1talk() end		-- 	"God forbid.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	wewillhave:play()	fish1talk() end		-- 	"We will have to lift this steel...",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() hicrab:play()		fish2talk() end		-- 	"Hi, crab!",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() wontwe:play()		fish2talk() end		-- 	"Won’t we try to cheer that crab up a little?",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	 ignoreit:play()	fish1talk() end		-- 	"Ignore it.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() leavethatcrab:play() fish1talk() end		-- 	"Leave that crab alone.",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() cantyousee:play()	fish1talk() end		-- 	"Can’t you see how nervous it is?",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() try:play()			fish1talk() end		-- 	"Try to do without that nervous crab.",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() thatcrab:play()	fish2talk() end		-- 	"That crab could be useful.",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() thiscrab:play()	fish2talk() end		-- 	"This crab will surely help us to solve it.",
		if timer >75 	and stepdone==14 then stepdone=15 Talkies.onAction()					fishstop()	end		-- 	
		if timer >80 	and stepdone==15 then stepdone=16 Talkies.onAction()	 				end		-- 	
		if timer >86 	and stepdone==16 then stepdone=17 Talkies.onAction()	 				end		-- 	
		if timer >90 	and stepdone==17 then stepdone=18 Talkies.onAction()	 				end		-- 		
		if timer >95 	and stepdone==18 then stepdone=19 Talkies.onAction()					end		-- 		
end

function subtitle31()

		--level 31 Labyrinthus

		if timer >0 	and stepdone==0 then stepdone=1 				   nowthis:play()				fish2talk() end		-- 		"Now this is a strange room.",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() thisisavery:play()			fish1talk() end		-- 		"This is a very unusual room.",
		if timer >9 	and stepdone==2 then stepdone=3 Talkies.onAction() theposition:play()			fish2talk() end		-- 		"The position of assistant deputy design coordinator was introduced because of this level. So that its author got his own credit.",
		if timer >17	and stepdone==3 then stepdone=4 Talkies.onAction() enoughaboutthegame:play()	fish1talk() end		--	 	"Enough about the game background. Let’s get to work.",
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() thisisavery2:play()			fish2talk() end		-- 		"This is a very strange coral.",
		if timer >26 	and stepdone==5 then stepdone=6 Talkies.onAction() thisisaverypeculiar:play()	fish1talk() end		-- 		"This is a very peculiar coral.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction() andwhatdoes:play()			fish2talk() end		-- 		"And what does it hang on?",
		if timer >34 	and stepdone==7 then stepdone=8 Talkies.onAction() idontknowcoral:play()				fish1talk() end		-- 		"I don’t know. But we have to get it down anyway.",
		if timer >38 	and stepdone==8 then stepdone=9 Talkies.onAction()	 										end		-- 		"I don’t know. Do we have to put it down?",
		if timer >44 	and stepdone==8 then stepdone=9 Talkies.onAction()	 howcould:play()			fish2talk() end		-- 		"How could that coral acquire such a bizarre shape?",
		if timer >47 	and stepdone==9 then stepdone=10 Talkies.onAction()  itiscarefully:play()	 	fish1talk() end		-- 		"It is carefully grown for the logical games players.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() why:play()	 				fish2talk() end		-- 		"Why do you want to put that coral down, anyway?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() ihavenoidea:play()			fish2talk() end		-- 		"I have no idea. Ask the player.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() shalimuddy:play() 			fish1talk() end		-- 		"Shalimuddy, shalimuddy, put your horns out...",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() stopit:play()				fish1talk() end		-- 		"Stop it! As if you don’t know that no object will move if we don’t push it first!",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() buticantry:play()			fish2talk() end		-- 		"But I can try, can’t I? Shalimuddy, Shalimuddy...",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction()	 									end		-- 		"Stop it! It makes my scales itch!",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() dontlisten:play()			fish2talk() end		-- 		"Don’t listen then. Shalimuddy, shalimuddy, put your...",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction()	 						end		-- 		
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction()							end		-- 		
end

function subtitle32()

		--level 32	

		if timer >0 	and stepdone==0 then stepdone=1 				 	lookimprisoned:play()				fish2talk() end		--  "Look, how I am supposed to get out of here?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() itsquitetight:play()		fish2talk() end		--	"It’s quite tight in here.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	howdid:play()			fish1talk() end		--	"How did you manage to get there?",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 	stopplaying:play()	fish1talk() end		--	"Stop playing with anemones and help me to get us out of here.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	dontbesurprised:play()	fish2talk() end		--	"Don’t be surprised.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	thereareso:play()		fish2talk() end		-- 	"There are so many corals here...",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		what:play()			fish1talk() end		-- 	"What?",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() somanycorals:play()		fish2talk() end		--	"So many corals. Are you deaf or what?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() ohmy:play()				fish2talk() end		--	"Oh my, this is a hard coral.",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	ohmy:play()			fish2talk() end		--	"Oh my, this is a soft coral.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() 	so:play()			fish1talk() end		-- 	"So!",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() nowyoucan:play()		fish1talk() end		--	"Now you can get out, can’t you?",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() 	hmm:play()			fish1talk() end		--	"Hmm, I can’t get through here...",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() ilovecolors:play()		fish2talk() end		-- 	"I love colors...",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() 						end		-- 	
		
end

function subtitle33()

		--level 33	

		if timer >0 	and stepdone==0 then stepdone=1 				 	dang:play()				fish1talk() end		--  "Dang, that snail is blocking the way.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	icant:play()			fish1talk() end		--	"I can’t swim through.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	thosecrabs:play()		fish2talk() end		--	"Those crabs are too talkative.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	theydont:play()			fish2talk() end		--	"They don’t look like hermits.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	ihadnoidea:play()		fish2talk() end		--	"I had no idea that hermit crabs could be so talkative.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	shutup:play()			fish1talk() end		-- 	"Shut up!",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	silence:play()			fish1talk() end		-- 	"Silence!",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	whatis:play()			fish1talk() end		--	"What is it now?!",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	thatsbetter:play()		fish1talk() end		--	"That’s better.",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	 yes:play()				fish1talk() end		--	"Yes.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() 	dontbemad:play()	fish2talk() end		-- 	"Don’t be mad at them.",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() 	nowwecan:play()		fish2talk() end		--	"Now we can calmly ponder how to get out of here.",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() 	whitefish:play()	fish2talk() end		--	"White fish, why do you have such big eyes?",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() 									end		-- 	"All the better to see you with.",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() 	iveseenthis:play()	fish2talk() end		-- 	"I’ve seen this seahorse somewhere before.",
		
end

function subtitle34()

		--level 34	

		if timer >0 	and stepdone==0 then stepdone=1 				 	lookat:play()				fish2talk() end		--1  "Look at all these lobsters...",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	theysleep:play()			fish1talk() end		--2	"They sleep for thousands of years... in eternal stillness...",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	andthegoverment:play()		fish2talk() end		--3	"...and the government tries to cover it up.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	nothingcan:play()			fish1talk() end		--4	"...nothing can wake them up...",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() unlessweactivate:play()		fish2talk() end		--5	"Unless we activate that octopus.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	look:play()					fish1talk() end		--6 	"Look, everything here is so still and quiet.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	buttheanemones:play()		fish2talk() end		--7 	"But the anemones are moving.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	maybe:play()				fish1talk() end		--8	"Maybe they’re in the throes of death.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	ifind:play()				fish1talk() end		--9	"I find a certain symbolism in that.",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	 thatsinteresting:play()	fish1talk() end		--10	"That’s interesting. This type of coral normally grows only in the Bermuda Triangle.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() didyouknow:play()			fish1talk() end		--11	"Did you know that some types of coral are almost as intelligent as fish?",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() becareful:play()			fish1talk() end		--12	"Be careful, most types of coral are poisonous.",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() whatdoyouthink:play()		fish2talk() end		--13	"What do you think, is there such a thing as a ‘coral-soul’?",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() doyouhave:play()			fish2talk() end		--14 	"Do you have the feeling that those coral are watching us?",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() 	iwonder:play()			fish2talk() end		--15 	"I wonder why the coral have such a bizarre shape.",
		if timer >75 	and stepdone==14 then stepdone=15 Talkies.onAction() 	thatwas:play()			fish1talk() end		--16 	"That was some number, wasn’t it?",	
		if timer >80 	and stepdone==15 then stepdone=16 Talkies.onAction() yeah:play()	 			fish2talk() end		--17 	"Yeah, he’s a great player.",
		if timer >85 	and stepdone==16 then stepdone=17 Talkies.onAction() 	 thatscool:play()		fish1talk() end		--18 	"That’s cool. What if we pushed him again?",	
		if timer >90 	and stepdone==17 then stepdone=18 Talkies.onAction() didyousee:play()			fish2talk() end		--19 	"Did you see what the lobsters were doing?",
		if timer >95 	and stepdone==18 then stepdone=19 Talkies.onAction()	 andhow:play()			fish1talk() end		--20 	"And how those anemones were dancing?",	
		if timer >100 	and stepdone==19 then stepdone=20 Talkies.onAction() wellyouknow:play()			fish2talk() end		--21 	"Well, you know, it’s not all that much fun down here at the bottom of the ocean.",
		if timer >105 	and stepdone==20 then stepdone=21 Talkies.onAction() hesgood:play()				fish1talk() end		--22 	"He’s good, isn’t he?",
		if timer >110 	and stepdone==21 then stepdone=22 Talkies.onAction() notbadfor:play()			fish2talk() end		--23 	"Not bad for an octopus...",
		if timer >115 	and stepdone==22 then stepdone=23 Talkies.onAction() doyouthink:play()			fish2talk() end		--24 	"Do you think he knows any other tune?",
		if timer >120 	and stepdone==23 then stepdone=24 Talkies.onAction() dontyouthink:play()		fish2talk() end		--25 	"Don’t you think we’ve had enough already?",
		if timer >125 	and stepdone==24 then stepdone=25 Talkies.onAction() imgettingsick:play()		fish2talk() end		--26 	"I’m getting sick of it...",
		if timer >130 	and stepdone==25 then stepdone=26 Talkies.onAction() imsick:play()				fish1talk() end		--27 	"I’m sick and tired of it...",
		if timer >135 	and stepdone==26 then stepdone=27 Talkies.onAction() imfed:play()				fish1talk() end		--28 	"I’m fed up to the gills with this.",
		if timer >140 	and stepdone==27 then stepdone=28 Talkies.onAction() icantbudge:play()			fish1talk() end		--29 	"I can’t budge this from where I am. I’d have to try to move it from the left side.",
		if timer >145 	and stepdone==28 then stepdone=29 Talkies.onAction() wellyou:play()				fish2talk() end		--30 	"Well you’ll have to go that way through those anemones.",
		if timer >150 	and stepdone==29 then stepdone=30 Talkies.onAction() thatsnot:play()			fish2talk() end		--31 	"That’s not what I had in mind...",
		if timer >155 	and stepdone==30 then stepdone=31 Talkies.onAction() weshould:play()			fish2talk() end		--32 	"We should have plugged that hole beforehand.",
		if timer >160 	and stepdone==31 then stepdone=32 Talkies.onAction() 							end		-- 	
end

function subtitle35()

		--level 35	

		if timer >0 	and stepdone==0 then stepdone=1 				 		why:play()			fish1talk() end		--  "Why isn’t he playing?",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 							end		--		
		
end

function subtitle36()

		--level 36	

		if timer >0 	and stepdone==0 then stepdone=1 				 		itislovely:play()		fish2talk() end		--1  "It is lovely here.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 		somanypearls:play()		fish2talk() end		--2	"So many pearls.",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()		thereare:play()			fish2talk() end		--3	"There are so many pearls here.",
		if timer >7		and stepdone==3 then stepdone=4 Talkies.onAction() 		listen:play()			fish1talk() end		--4	"Listen... Don’t forget our mission!",
		if timer >9 	and stepdone==4 then stepdone=5 Talkies.onAction() 		wearenot:play()			fish1talk() end		--5	"We are no pearl-hunters.",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()		remember:play()			fish1talk() end		--6 	"Remember! We have a mission to complete.",
		if timer >17 	and stepdone==6 then stepdone=7 Talkies.onAction()		youarejust:play()		fish2talk() end		--7 	"You are just sitting up there and I have to do all the work myself!",
		if timer >19 	and stepdone==7 then stepdone=8 Talkies.onAction() 		didntyousay:play()		fish1talk() end		--8	"Didn’t you say that you like the pearls?",
		if timer >20 	and stepdone==8 then stepdone=9 Talkies.onAction() 		arentyou:play()			fish1talk() end		--9	"Aren’t you happy with so many pearls?",
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	ineedone:play()			fish1talk() end		--10	"I need one more pearl.",
		if timer >27 	and stepdone==9 then stepdone=10 Talkies.onAction() 	nope:play()				fish2talk() end		--11 	"Nope.",
		if timer >33 	and stepdone==10 then stepdone=11 Talkies.onAction() 	onemorepearl:play()		fish2talk() end		--12	"One more pearl, please.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 	mydearest:play()		fish2talk() end		--13	"My dearest friend, couldn’t you possibly lend me just one little pearl?",
		if timer >49 	and stepdone==12 then stepdone=13 Talkies.onAction() 	iwonder:play()			fish2talk() end		--14 	"I wonder if it wouldn’t be easier to shorten that steel pipe.",
		if timer >53 	and stepdone==13 then stepdone=14 Talkies.onAction() 	cleopatra:play()		fish1talk() end		--15 	"Cleopatra is rumored to dissolve pearls in vinegar.",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() 	doyouthink:play()		fish2talk() end		--16 	"Do you think we have too many of them?",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() 	well:play()				fish1talk() end		--17 	"Well, I just wanted to educate you a little.",
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 	watching:play()			fish1talk() end		--18 	"Watching you, I have to think about how I played marbles with tadpoles.",
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 	youhaventgot:play()		fish2talk() end		--19	"You haven’t got a spare marble in your pocket, do you?",
		
end

function subtitle37()

		--level 37 Telepathic turtle

		if timer >0 	and stepdone==0 then stepdone=1 			 		canyouseeit:play()												fish1talk() end		--	"Can you see it? This must be that telepathic turtle.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	yesthisisit:play()												fish1talk() end		--	"Yes, this is it. The telepathic turtle.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	wellidont:play()												fish2talk() end		--	"Well, I don’t know. Judging from those photos, it could be it.",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction() 	iamnotsure:play()		turtlestatus="lookaround"	zframe=0	fish2talk() end		--	"I am not sure... what about those telepathic powers?",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction()		inanycase:play() 											fish1talk() end		--	"In any case we have to get it out of the room.",
		if timer >25	and stepdone==5 then stepdone=6 Talkies.onAction() 	letstakeout:play()		turtlestatus="telepatic" zframe=0 telepaticsound:play() fish1talk() end		--	 "Let’s take out that turtle.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction() 	whatareyou:play()												fish2talk() end		--	"What are you doing?",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	whathaveyou:play()												fish2talk() end		--	"What have you done?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	whatisit:play()													fish2talk() end		--	"What is it supposed to mean?",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	whereareyouswimming:play()									fish1talk() end		--	"Where are you swimming?",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()		whatshoulditmean:play()									fish1talk() end		-- 		"What should it mean?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction()		whathappening:play()									fish1talk() end		-- 		"What happened to you?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	idontknow:play()											fish2talk() end		--	"I don’t know...",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	icantrealize:play()											fish2talk() end		--	"I can’t realize...",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	idontknow2:play()											fish1talk() end		-- 		"I don’t know what’s happening to me...",	
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction()	 	ididntwantto:play()										fish1talk() end		-- 	"I didn’t want to...",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction()	 	whatshappening:play()									fish1talk() end		-- "What’s happening to us?",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() 	whatcanitmean:play()	turtlestatus="hide"		zframe=0	fish2talk() end		--	"What can it mean?",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() 	thismustbe:play()											fish2talk() end		--	"This must be that turtle!",
		if timer >95 	and stepdone==19 then stepdone=20 threetalkiesactions() 	sotherecan:play()											fish2talk() end		--	"So there can be no doubts now - this is it. The one we are looking for"
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction()	doubtless:play() 											fish1talk() end		--	"Doubtless, this is the turtle we are looking for!",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	thatbeast:play()											fish2talk() end		--	"That beast!",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() 	stopit:play()			turtlestatus="lookunder" zframe=0	fish2talk() end		--	"Stop it, you devil!",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() enough:play()													fish1talk() end	--	"Enough is enough!",	
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() areyouenyoing:play()	 										fish1talk() end		--	"Are you enjoying yourself?",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() somanybizarre:play()											fish1talk() end		--	"So many bizarre shapes can only be found in a coral reef."
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() wheredoso:play()	 											fish1talk() end		--
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() 	thesecorals:play()											fish2talk() end		--	"These corals are Mother Nature’s wonders.",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() ourgoal:play()													fish1talk() end		--	"Our goal in this room is to get that turtle out.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() 																fish2talk() end		--	"Where do so many bizarre shapes come from?",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() 	itseasy:play()												fish1talk() end		--	"It’s easy - designers create tricky problems and leave the drawing to the graphic artists.",
		
end

		 
function subtitle37end()
		if timer >0 	and stepdone==0 then stepdone=1 					wesucceeded:play()				end		--	
		if timer >7 	and stepdone==1 then stepdone=2 Talkies.onAction() werecommend:play()				end		--  
		if timer >13.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	itcannotonly:play()				end		-- 	
		if timer >22	and stepdone==3 then stepdone=4 Talkies.onAction() andshehasbitten:play()			end		-- 	
		if timer >26 	and stepdone==4 then stepdone=5 Talkies.onAction() byallmeansunmuzzleher:play()	end		--  
		if timer >32 	and stepdone==5 then stepdone=6 Talkies.onAction()									end		--
--		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()	:play()			end		--
		--if timer >38 	and stepdone==7 then stepdone=8 Talkies.onAction()	:play()	end		-- 		
		
end

function subtitle38()

		--level 38 The deep server
	if language=="en" then
		if timer >0 	and stepdone==0 then stepdone=1 			 		thatsanother:play()			fish2talk() end		--	"That’s another fine mess.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatkindofplace:play()		fish2talk() end		--	"What kind of place is this?",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()	wowwhataplace:play() 		fish2talk() end		--	"Wow, what a place!",
		if timer >7 	and stepdone==3 then stepdone=4 Talkies.onAction() 	wherecanman:play()			fish1talk() end		--	"Where can man - I mean fish - get by climbing into a toilet?",
		if timer >13 	and stepdone==4 then stepdone=5 Talkies.onAction() 	itlookslike:play()			fish1talk() end		--	"It looks like a cozy little dump.",
		if timer >18 	and stepdone==5 then stepdone=6 Talkies.onAction()	lookwhatkindof:play()		fish1talk() end		--	"Look, what kind of things people have flushed down the toilet.",
		if timer >22	and stepdone==6 then stepdone=7 Talkies.onAction() 	ithoughtitwould:play()		fish1talk() end		--	"I thought it would be worse when I was climbing in.",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction() 	youcanfind:play()			fish1talk() end		--	"You can find many strange things in such a dump.",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction() 	doyouthink:play()			fish2talk() end		--	"Do you think this is the computer that’s at stake in this game?",
		if timer >40 	and stepdone==9 then stepdone=10 Talkies.onAction() 	surelynot:play()		fish1talk() end		--	"Surely not! This is no powerful multimedia computer. This is but an XT machine with a twelve inch display.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()	theone:play()		 	fish1talk() end		--	"The one who solves, I mean who has solved this, will receive, I mean will have received, MMX based machine with a 3Dfx card, plenty of RAM, a huge hard disk...",
		if timer >65 	and stepdone==11 then stepdone=12 Talkies.onAction()	andwhatisitfor:play()	fish2talk() end		-- 	"And what is it for? I for one know plenty of games almost as good as this one that could easily run on this XT machine.",
		if timer >75 	and stepdone==12 then stepdone=13 Talkies.onAction()	shh:play()				fish1talk() end		-- 	"Shhhh!",
		if timer >80 	and stepdone==13 then stepdone=14 Talkies.onAction() 	heylook:play()			fish1talk() end		--	"Hey, look, just an idea: Could this be the computer we are looking for?",
		if timer >95 	and stepdone==14 then stepdone=15 Talkies.onAction() 	thiscouldbeit:play()	fish2talk() end		--	"This could be it.",
		if timer >10 	and stepdone==15 then stepdone=16 Talkies.onAction() 	sowhatare:play()		fish1talk() end		-- 	"So what are we waiting for? Put that suspicious data on the diskette and off we go.",
		if timer >105 	and stepdone==16 then stepdone=17 Talkies.onAction()	itwontbeaseasy:play()	fish2talk() end		-- 	"It won’t be as easy as that. The data is surely going to be hidden somewhere. We have to go in.",
		if timer >115 	and stepdone==17 then stepdone=18 Talkies.onAction()	andwhatabout:play()		fish1talk() end		-- 	"And what about getting in through that opening?",
		if timer >120 	and stepdone==18 then stepdone=19 Talkies.onAction() 	thatisafloppy:play()	fish2talk() end		--	"That is a floppy disk drive.",
		if timer >125 	and stepdone==19 then stepdone=20 Talkies.onAction() 	ihavetoget:play()		fish2talk() end		--	"I have to get to the back of it.",
		if timer >130 	and stepdone==20 then stepdone=21 Talkies.onAction()	 putdownthat:play()		fish2talk() end		-- 	"Put down that corkscrew and come to help me.",
		if timer >135 	and stepdone==21 then stepdone=22 Talkies.onAction() 	ohmywhatasloppy:play()	fish2talk() end		--	"Oh my, what a sloppy machine. So big, so heavy and surely so slow.",
		if timer >140 	and stepdone==22 then stepdone=23 Talkies.onAction() 	andmoreover:play()			fish1talk() end		--	"And moreover only mono.",
		if timer >145 	and stepdone==23 then stepdone=24 Talkies.onAction() 							fish1talk() end		--	
	else
		if timer >0 	and stepdone==0 then stepdone=1 			 		thatsanother:play()			fish2talk() end		--	"That’s another fine mess.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatkindofplace:play()		fish2talk() end		--	"What kind of place is this?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	wowwhataplace:play() 		fish2talk() end		--	"Wow, what a place!",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction() 	wherecanman:play()			fish1talk() end		--	"Where can man - I mean fish - get by climbing into a toilet?",
		if timer >25 	and stepdone==4 then stepdone=5 Talkies.onAction() 	itlookslike:play()			fish1talk() end		--	"It looks like a cozy little dump.",
		if timer >30 	and stepdone==5 then stepdone=6 Talkies.onAction()	lookwhatkindof:play()		fish1talk() end		--	"Look, what kind of things people have flushed down the toilet.",
		if timer >40	and stepdone==6 then stepdone=7 Talkies.onAction() 	ithoughtitwould:play()		fish1talk() end		--	"I thought it would be worse when I was climbing in.",
		if timer >50 	and stepdone==7 then stepdone=8 Talkies.onAction() 	youcanfind:play()			fish1talk() end		--	"You can find many strange things in such a dump.",
		if timer >60 	and stepdone==8 then stepdone=9 Talkies.onAction() 	doyouthink:play()			fish2talk() end		--	"Do you think this is the computer that’s at stake in this game?",
		if timer >70 	and stepdone==9 then stepdone=10 Talkies.onAction() 	surelynot:play()		fish1talk() end		--	"Surely not! This is no powerful multimedia computer. This is but an XT machine with a twelve inch display.",
		if timer >80 	and stepdone==10 then stepdone=11 Talkies.onAction()	theone:play()		 	fish1talk() end		--	"The one who solves, I mean who has solved this, will receive, I mean will have received, MMX based machine with a 3Dfx card, plenty of RAM, a huge hard disk...",
		if timer >90 	and stepdone==11 then stepdone=12 Talkies.onAction()	andwhatisitfor:play()	fish2talk() end		-- 	"And what is it for? I for one know plenty of games almost as good as this one that could easily run on this XT machine.",
		if timer >100 	and stepdone==12 then stepdone=13 Talkies.onAction()	shh:play()				fish1talk() end		-- 	"Shhhh!",
		if timer >105 	and stepdone==13 then stepdone=14 Talkies.onAction() 	heylook:play()			fish1talk() end		--	"Hey, look, just an idea: Could this be the computer we are looking for?",
		if timer >115 	and stepdone==14 then stepdone=15 Talkies.onAction() 	thiscouldbeit:play()	fish2talk() end		--	"This could be it.",
		if timer >120 	and stepdone==15 then stepdone=16 Talkies.onAction() 	sowhatare:play()		fish1talk() end		-- 	"So what are we waiting for? Put that suspicious data on the diskette and off we go.",
		if timer >130 	and stepdone==16 then stepdone=17 Talkies.onAction()	itwontbeaseasy:play()	fish2talk() end		-- 	"It won’t be as easy as that. The data is surely going to be hidden somewhere. We have to go in.",
		if timer >140 	and stepdone==17 then stepdone=18 Talkies.onAction()	andwhatabout:play()		fish1talk() end		-- 	"And what about getting in through that opening?",
		if timer >150 	and stepdone==18 then stepdone=19 Talkies.onAction() 	thatisafloppy:play()	fish2talk() end		--	"That is a floppy disk drive.",
		if timer >155 	and stepdone==19 then stepdone=20 Talkies.onAction() 	ihavetoget:play()		fish2talk() end		--	"I have to get to the back of it.",
		if timer >165 	and stepdone==20 then stepdone=21 Talkies.onAction()	 putdownthat:play()		fish2talk() end		-- 	"Put down that corkscrew and come to help me.",
		if timer >175 	and stepdone==21 then stepdone=22 Talkies.onAction() 	ohmywhatasloppy:play()	fish2talk() end		--	"Oh my, what a sloppy machine. So big, so heavy and surely so slow.",
		if timer >185 	and stepdone==22 then stepdone=23 Talkies.onAction() 	andmoreover:play()			fish1talk() end		--	"And moreover only mono.",
		if timer >190 	and stepdone==23 then stepdone=24 Talkies.onAction() 							fish1talk() end		--	
	end
end

function subtitle39()

		--level 39 Almost no wall

		if timer >0 	and stepdone==0 then stepdone=1 			 		thisisavery:play()			fish2talk() end		--	"This is a very strange room.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	therearenosquares:play()	fish1talk() end		--	"There are no squares of walls here.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	rather:play()				fish1talk() end		--	"Or rather squares of the Earth.",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction() 	youneedto:play()			fish2talk() end		--	"You need to realize that the steel cylinder surrounding us",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	orrathersquares:play()		fish1talk() end		--	"... rather above me ...",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	isonlyanobject:play()		fish2talk() end		--	"is only an object."
		if timer >30	and stepdone==6 then stepdone=7 Talkies.onAction() 	therefore:play()			fish1talk() end		--	"Therefore I am more tender then usualy.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	amiinufo:play()				fish2talk() end		--	"Am I in UFO?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	sowhyis:play()				fish2talk() end		--	"So why is starlit sky on the background?",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 								end		--	"And why are the stars moving?",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()	itisrotating:play()		 	fish2talk() end		--	"It is rotating very quick.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction()	ithoughtweare:play()		fish2talk() end		-- 	"I thought we are in the space.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction()	youcantbethere:play()		fish1talk() end		-- 	"You can’t be there, it is just another elevator.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	because:play()				fish1talk() end		--	"Because it’s night now.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 								end		--	"Because the globe is rotating around its axis.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() itdoesnot:play()				fish1talk() end		-- 	"It does not matter to us. We are in the water.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction()								end		-- 	


end

function subtitle40()

		--level 40 Plumbman's refutes

		if timer >0 	and stepdone==0 then stepdone=1 			 		itsquitecomfortable:play()		fish1talk() end		--	"It’s quite comfortable for a wasteyard.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatdoyouthinkplumbman:play()			fish2talk() end		--	"What do you think, does anybody live here?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	someunderwater:play()			fish1talk() end		--	"Some underwater homeless.",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction() 	wecallthem:play()				fish2talk() end		--	"We call them shell-less.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction()	didyounotice:play()				fish2talk() end		--	"Did you notice that...",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	dontyouthink:play()				fish2talk() end		--	"Don’t you think that...",
		if timer >30	and stepdone==6 then stepdone=7 Talkies.onAction() 	isntitstrange:play()			fish2talk() end		--	"Isn’t it strange that...",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	donttalk:play()					fish1talk() end		--	"Don’t talk about it.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	pleasenojokes:play()			fish1talk() end		--	"Please, no jokes about defecation.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	yesiknow:play()				fish1talk() end		--	"Yes, I know about that. We won’t comment on it.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()	yeahbutwhy:play()			fish2talk() end		--	"Yeah, but why TWO?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction()	stopit:play()				fish1talk() end		-- 	"Stop it, will you?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction()	iveseenmany:play()			fish2talk() end		-- 	"I’ve seen many useless things, but an underwater shower...",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	maybeitspraysair:play()		fish1talk() end		--	"Maybe it sprays air, like in the bubble bath."
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	doyouthink:play()			fish2talk() end		--	"Do you think we could turn it on?",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 	hardly:play()				fish1talk() end		-- 	"Hardly. But we could push it down.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction()	nowthisisreal:play()		fish2talk() end		-- 	"Now this is real treasure.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction()	weshould:play()				fish1talk() end		-- "We should have left it alone.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() 	anitdoesnt:play()			fish2talk() end		--	"And it doesn’t work anyway.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() 	helloinside:play()			fish2talk() end		--	"Hello, inside there.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction()	thismustbe:play() 			fish1talk() end		-- 	"This must be better than a rollercoaster.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	iwouldlove:play()			fish2talk() end		--	"I would love to try it myself.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() heseems:play()					fish1talk() end		--	"He seems to be enjoying himself.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 								end		--	

end

function subtitle41()

		--level 41

		if timer >0 	and stepdone==0 then stepdone=1 			 		doesntthis:play()				fish1talk() end		--	"Doesn’t this duck look suspicious to you?",	
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() 	ithinkitlooks:play()			fish2talk() end		--	"I think it looks dumb.",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction() 	ithinkitlooksinflatable:play()	fish2talk() end		--	"I think it looks inflatable.",
		if timer >7 	and stepdone==3 then stepdone=4 Talkies.onAction() 	canyousee:play()				fish1talk() end		--	"Can you see that snowman?",
		if timer >9 	and stepdone==4 then stepdone=5 Talkies.onAction()	whatdoyoumean:play()			fish2talk() end		--	"What do you mean? Snowmen belong to the Mess Hall."
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()	trytoopenthat:play()			fish2talk() end		--	"Try to open that tap!",
		if timer >17	and stepdone==6 then stepdone=7 Talkies.onAction() 	nowthiswasajoke:play()			fish1talk() end		--	"Now, this was a joke. Heh, heh.",
		if timer >19 	and stepdone==7 then stepdone=8 Talkies.onAction() 	andthereisnteven:play()			fish2talk() end		--	"And there isn’t even a funny animated object here.",
		if timer >21 	and stepdone==8 then stepdone=9 Talkies.onAction() 	butabeautifullywavy:play()		fish1talk() end		--	"But a beautifully wavy background.",
		if timer >23 	and stepdone==9 then stepdone=10 Talkies.onAction() 	moveon:play()				fish1talk() end		--	"Move on, so that we can get going.",
		if timer >27 	and stepdone==10 then stepdone=11 Talkies.onAction()	andwhatabout:play()			fish1talk() end		--	"And what about me?",
		if timer >33 	and stepdone==11 then stepdone=12 Talkies.onAction()	aminotenough:play()			fish1talk() end		-- 	"Am I not enough for you now, or what?",
		if timer >35 	and stepdone==12 then stepdone=13 Talkies.onAction()								fish1talk() end		-- 	
		
end

function subtitle42()

		--level 42

		if timer >0 	and stepdone==0 then stepdone=1 			 		lookwhatwe:play()					fish2talk() end		--	"Look what we can find on such a dump.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()  itsinteresting:play()				fish1talk() end		--	"It’s interesting what kind of stuff somebody will throw out.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 	imsure:play()						fish1talk() end		--	"I am sure I have seen this woman somewhere.",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction() 	youareprobablywrong:play()			fish2talk() end		--	"You are probably wrong. It’s a completely plain face.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction()	lookatthatthing:play()				fish2talk() end		--	"Look at that thing.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	lookatthatpld:play()				fish2talk() end		--	"Look at that pld. What a monster.",
		if timer >30	and stepdone==6 then stepdone=7 Talkies.onAction() 	ohmythisis:play()					fish2talk() end		--	"Oh, my, this is repulsive.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	repulsive:play()					fish2talk() end		--	"Repulsive, slimy, dirty and in the way.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	itstaxing:play()					fish1talk() end		--	"It’s taxing.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	itsquite:play()					fish1talk() end		--	"It’s quite demanding.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()	itwasalot:play()				fish1talk() end		--	"It was a lot of work.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction()	butitpaidoff:play()				fish2talk() end		-- 	"But it paid off, didn’t it?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction()	justimagineitwas:play()			fish2talk() end		-- 	"Just imagine it was some precious painting and we have saved it!",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction()	whatnonsense:play()				fish1talk() end		-- 	"What  nonsense!",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction()									fish1talk() end		-- 	"MUAHHH... UAHH... UUUH...",
		
end

function subtitle43()

		--level 43 real chaos
		
		if timer >0 	and stepdone==0 then stepdone=1 			 		wehavebeen:play()		fish2talk() end		-- "We have been crawling through wasteyard for six levels now.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() thisone:play()			fish1talk() end		-- "This one looks really very waste-like.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction() butno:play()				fish1talk() end		-- "But no, I think that there is something strange about it.",
		if timer >13 	and stepdone==3 then stepdone=4 Talkies.onAction() noithink:play()			fish1talk() end		-- "No, I think there is something here, that cannot be found anywhere else.",
		if timer >19 	and stepdone==4 then stepdone=5 Talkies.onAction() look:play()				fish1talk() end		-- "Look at that clock. Doesn’t it resemble a child-comforter a little?",
		if timer >24.5 	and stepdone==5 then stepdone=6 Talkies.onAction() doyoufeelallright:play()	fish2talk() end		-- "Do you feel all right?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction() thisisapile:play()		fish2talk() end		-- "This is a pile of garbage.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() thismustbeawreck:play()	fish2talk() end		-- "This must be a wreck of a flea shop.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() itsworse:play() 			fish1talk() end		-- "It’s worse here than in an oil spill.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() thereissomuch:play() 	fish1talk() end		-- "There is so much filth here.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() canyousee:play()		fish1talk() end		-- "Can you see that boat?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() theonewiththat:play() 	fish2talk() end		-- "The one with that old man?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() itsold:play()			fish1talk() end		-- "It’s old Charon, ferryman of the dead.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() andwhatdoeshedo:play()	fish2talk() end		-- "And what does he do in the wasteyard?",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() hesprobably:play()		fish1talk() end		-- "He’s probably retired.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() thecorpses:play()		fish1talk() end		-- "The corpses were no longer profitable and so they started a recycling business.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() canyousee:play()		fish1talk() end		-- "Can you see that collection of useless stuff, weird objects and refuse... All of them were useful things, carefully and skillfully manufactured.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() orpainted:play()		fish2talk() end		-- "Or painted.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() everything:play()		fish1talk() end		-- "Everything is in vain, like that medusa over there.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() why:play()				fish2talk() end		-- "Why?",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() justlookatit:play()	fish1talk() end		-- "Just look at it. It keeps running forward but stays in one place. Isn’t it the most fitting example of what I was talking about?",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() imjogging:play()		fish1talk() end		-- "I am jogging to lose weight, you fools!",

end		

function subtitle44()

		--level 44

		if timer >0 	and stepdone==0 then stepdone=1 				 	someonefrom:play()			fish1talk() end		-- 		"Somebody from Greenpeace should see this.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	somebodyfrom:play()			fish2talk() end		-- 		"Somebody from Three Mile Island should see this.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	whatarewe:play()			fish1talk() end		-- 		"What are we supposed to do with it?",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 	wehavetoexterminate:play()	fish2talk() end		--	"We have to exterminate all of them. Such abominations have no right to breath the same air... er, water, as we do.",
		if timer >25 	and stepdone==4 then stepdone=5 Talkies.onAction()  ithink:play()				fish1talk() end		-- 		"I think we have to eliminate the cause and not the results.",
		if timer >30 	and stepdone==5 then stepdone=6 Talkies.onAction()	wellyoumaybe:play()			fish2talk() end		-- 		"Well, you may be right.",
		if timer >35 	and stepdone==6 then stepdone=7 Talkies.onAction()	andthecause:play()			fish1talk() end		-- 		"And the cause is doubtless this giant barrel. We have to get it out somehow.",
		if timer >40 	and stepdone==7 then stepdone=8 Talkies.onAction() 	yesletsgettowork:play()		fish2talk() end		-- 		"Yes. Let’s get to work. We can drop it on the Mr. B.’s front yard afterwards.",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	ithinkimgrowing:play()		fish2talk() end		-- 		"I think I am growing to like them.",
		if timer >50 	and stepdone==8 then stepdone=9 Talkies.onAction()	ifonly:play()				fish1talk() end		-- 		"If only they wouldn’t stand in our way so much.",
		if timer >55 	and stepdone==9 then stepdone=10 Talkies.onAction() ifonlythey:play()			fish1talk() end		-- 		"If only they were where we need them.",
		if timer >60 	and stepdone==10 then stepdone=11 Talkies.onAction() suchalovely:play() 		fish1talk() end		-- 		"Such a lovely collection of monstrosities. I’ll be sorry to leave.",
		if timer >65 	and stepdone==11 then stepdone=12 Talkies.onAction() thatwonthappenany:play()	fish2talk() end		-- 		"That won’t happen any time soon.",
		if timer >70 	and stepdone==12 then stepdone=13 Talkies.onAction() youaregoing:play()			fish2talk() end		-- 		"You are going to have plenty of time to enjoy them.",
		if timer >75 	and stepdone==13 then stepdone=14 Talkies.onAction() ifwetake:play() 			fish1talk() end		-- 		"If we take them with us and store them in alcohol, we could found ourselves a freak show.",
		if timer >80 	and stepdone==14 then stepdone=15 Talkies.onAction() andfdto:play()	 			fish2talk() end		-- 		"And FDTO isn’t enough for you?",
		if timer >85 	and stepdone==15 then stepdone=16 Talkies.onAction()	 						end		-- 		
		if timer >90 	and stepdone==16 then stepdone=17 Talkies.onAction() 				 			end		-- 	"I can hear everything!",
		if timer >95 	and stepdone==17 then stepdone=18 Talkies.onAction() sorryboss:play()			fish2talk() end		--	"Sorry, boss. I didn’t mean to offend.",
		if timer >100 	and stepdone==18 then stepdone=19 Talkies.onAction() youknowifinally:play()		fish2talk() end		-- 	"You know, I finally realized that even mutants have a soul.",	
		if timer >105 	and stepdone==19 then stepdone=20 Talkies.onAction() maybeeven:play()			fish1talk() end		-- 	"Maybe even UFOs can have a soul.",
		if timer >110 	and stepdone==20 then stepdone=21 Talkies.onAction() ithinkthathis:play()	 	fish2talk() end		-- 	"I think that this level is going to change us forever.",
		if timer >115 	and stepdone==21 then stepdone=22 Talkies.onAction() thatfootis:play()			fish2talk() end		-- 	"That foot is not only repulsive but also shameless. Just look at it.",
		if timer >120 	and stepdone==22 then stepdone=23 Talkies.onAction() lookatthat:play()			fish1talk() end		-- 	"Look at that pld. It looks the same and still it grows more and more repulsive.",
		if timer >125 	and stepdone==23 then stepdone=24 Talkies.onAction() butevenitharbors:play()	fish2talk() end		-- 	"But even it harbors some motherly instincts. Look how it cares about the little one.",
		if timer >130 	and stepdone==24 then stepdone=25 Talkies.onAction() yes:play()					fish1talk() end		-- 	"Yes. It almost killed it with iron beam.",
		if timer >135 	and stepdone==25 then stepdone=26 Talkies.onAction() thisfishlooks:play()		fish2talk() end		-- 	"This fish looks familiar to me.",
		if timer >140 	and stepdone==26 then stepdone=27 Talkies.onAction() maybeitwas:play()			fish1talk() end		-- 	"Maybe it was in that photo from the power plant.",
		if timer >145 	and stepdone==27 then stepdone=28 Talkies.onAction() thatpoorcrab:play()		fish1talk() end		-- 	"The poor crab. So many pincers and no head. Although... maybe he is better off than us.",
		if timer >150 	and stepdone==28 then stepdone=29 Talkies.onAction() nomanisalone:play()		fish2talk() end		-- 	"No man is alone with a little yellow duck. Not even fish.",
		if timer >155 	and stepdone==29 then stepdone=30 Talkies.onAction() thistimeourgoal:play()		fish2talk() end		-- 	"This time, our goal is to push out the barrel with that filth.",
		if timer >160 	and stepdone==30 then stepdone=31 Talkies.onAction() butwehave:play()			fish1talk() end		-- 	"But we have to leave all those creatures here. They must not harm the genepool of the healthy population.",
		if timer >165 	and stepdone==31 then stepdone=32 Talkies.onAction() look:play()	 			end		-- 		
	
end

function subtitle44end()
		if timer >0 	and stepdone==0 then stepdone=1 				 	greetings:play()			end		-- 	"Greetings from the rubbish heap to our beloved boss."
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	yourssincerely:play()		end		-- 	"Yours sincerely"
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	agents:play()				end		-- 	"Agents"
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() itsgreattobehere:play()		end		--	"PS: It’s great to be here. We irradiate ourselves and swim in the sea a lot. We have many new friends."
		
end


function subtitle45()

		--level 45 first mate cabin

		if timer >0 	and stepdone==0 then stepdone=1 				 	cruecruel:play()			end		-- 		"Crrruel, crrruel, crrruel Captain Silverrrr...",
		if timer >4.5 	and stepdone==1 then stepdone=2 Talkies.onAction() aycaramba:play()				end		-- 		"Ay... caramba!!!",
		if timer >6.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	cruelcaptain:play()			end		-- 		"Crrruel Captain Silverrrr...",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() treasureyoufools:play()		end		--	 	"Trrreasurre, you foools...",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() goodgrief:play()				end		-- 		"Good grrrief!",
		if timer >13 	and stepdone==5 then stepdone=6 Talkies.onAction()	thatsridiulous:play()		end		-- 		"That’s rrrrrridiculous! Polly is his masterrr’s darrrling.",
		if timer >18 	and stepdone==6 then stepdone=7 Talkies.onAction()	dontpushme:play()			end		-- 		"Don’t ppppush me!",
		if timer >20 	and stepdone==7 then stepdone=8 Talkies.onAction() thastbetter:play()			end		-- 		"That’s betterrr!",
		if timer >22 	and stepdone==8 then stepdone=9 Talkies.onAction()	anditscurtainsforyou:play()	end		-- 		"And it’s currrtains forrr you!",
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction()	imgoingtobeinyourway:play()	end		-- 		"I’m going to be in yourrr way herrre.",
		if timer >25 	and stepdone==9 then stepdone=10 Talkies.onAction() 							end		-- 		"Trrricky prrroblem, isn’t it?",
		if timer >26 	and stepdone==10 then stepdone=11 Talkies.onAction() trickyproblem:play() 		end		-- 		"Dang it!",
		if timer >29 	and stepdone==11 then stepdone=12 Talkies.onAction() dangit:play() 				end		-- 		"You’rrre too fat, you can’t make it thrrrrough.",
		if timer >33 	and stepdone==12 then stepdone=13 Talkies.onAction() youaretoofat:play() 		end		-- 		"Polly wants a crrracker!",
		if timer >36 	and stepdone==13 then stepdone=14 Talkies.onAction() pollywantsacracker:play() end		-- 		"Beat it, sirrr...",
		if timer >38 	and stepdone==14 then stepdone=15 Talkies.onAction() beatitsir:play()	 		end		-- 		
		if timer >42 	and stepdone==15 then stepdone=16 Talkies.onAction()	 						end		-- 		
		if timer >44 	and stepdone==16 then stepdone=17 Talkies.onAction() whatcouldbe:play() 		fish2talk() end	-- 	"What could be inside?",
		if timer >47 	and stepdone==17 then stepdone=18 Talkies.onAction() whoknows:play() 			fish1talk() end		-- 		"Who knows...",
		if timer >49 	and stepdone==18 then stepdone=19 Talkies.onAction() couldthisbe:play() 		fish2talk() end	-- 		"Could this be the legendary Silver’s treasure?"
		if timer >52 	and stepdone==19 then stepdone=20 Talkies.onAction() insuchasmall:play()		fish1talk() end	-- 		"In such a small chest? The boss surely wouldn’t contact us for such a little case.",
		if timer >58 	and stepdone==20 then stepdone=21 Talkies.onAction() yuck:play()	 			fish2talk() end		-- 		"Yuck!",
		if timer >60 	and stepdone==21 then stepdone=22 Talkies.onAction() thanks:play()			 	fish2talk() end		-- 		"Thanks.",
		if timer >63 	and stepdone==22 then stepdone=23 Talkies.onAction() noproblem:play()			fish1talk() end		-- 		"No problem.",
		if timer >66 	and stepdone==23 then stepdone=24 Talkies.onAction() whatareyoudoing:play() 	fish2talk() end		-- 		"What are you doing?!",
		if timer >70 	and stepdone==24 then stepdone=25 Talkies.onAction() imsorry:play()				fish1talk() end		-- 		"I’m sorry. I’ll be more careful next time.",
		if timer >74 	and stepdone==25 then stepdone=26 Talkies.onAction() becareful:play() 			fish1talk() end		-- 		"Be careful! Don’t hurt it!",
		if timer >78 	and stepdone==26 then stepdone=27 Talkies.onAction() itsmoretheotherway:play()	fish2talk() end		-- 		"It’s more the other way around!",
		if timer >82 	and stepdone==27 then stepdone=28 Talkies.onAction() thatssomeoctopus:play()	fish2talk() end		-- 		"That’s some octopus! I wonder who’s her owner?",
		if timer >86 	and stepdone==28 then stepdone=29 Talkies.onAction() maybeitbelonged:play()		fish1talk() end		-- 		"Maybe it belonged to the person who lived in this cabin.",
		if timer >90 	and stepdone==29 then stepdone=30 Talkies.onAction() whoever:play()				fish2talk() end		-- 		"Whoever lived here sure was weird.",
		if timer >94 	and stepdone==30 then stepdone=31 Talkies.onAction() why:play()					fish1talk() end		-- 		"Why?",
		if timer >96 	and stepdone==31 then stepdone=32 Talkies.onAction() look:play()	 			fish2talk() end		-- 		"Look, he had a human skull here!",
		if timer >100 	and stepdone==32 then stepdone=33 Talkies.onAction() wellmaybe:play()			fish1talk() end		-- 		"Well, maybe it’s his own skull.",
		if timer >104 	and stepdone==33 then stepdone=34 Talkies.onAction() youthinkso:play()			fish2talk() end		-- 		"You think so?",
		if timer >108 	and stepdone==34 then stepdone=35 Talkies.onAction() canyousense:play()			fish1talk() end		-- 		"Can you sense the atmosphere of rot and decay? The sunken wreck, the parrot’s skeleton, the human skull, and the broken cradle...",
		if timer >114	and stepdone==35 then stepdone=36 Talkies.onAction() wheredoyousee:play()		fish2talk() end		-- 		"Where do you see the cradle?",
		if timer >117 	and stepdone==36 then stepdone=37 Talkies.onAction() ohyoudonthave:play()		fish1talk() end		-- 		"Oh, you don’t have any feeling for poetry at all...",
	
end

function subtitle45skull()
		if timer >0 	and stepdone==0 then stepdone=1   Talkies.onAction() 				end		-- 	
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
		
end

function subtitle46()

		--level 46 The winter mess hall

		if timer >0 	and stepdone==0 then stepdone=1 				 	imkindof:play() 			fish2talk() end		-- 		"I’m kind of cold.",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() nowonder:play() 				fish1talk() end		-- 		"No wonder. This is the Winter Mess Hall.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	imcold:play() 				fish2talk() end		-- 		"I’m cold.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() wellthats:play() 			fish1talk() end		--	 	"Well that’s what you would expect in a Winter Mess Hall, right?",
		if timer >13 	and stepdone==4 then stepdone=5 Talkies.onAction() atleastthesnowman:play() 	fish1talk() end		-- 		"At least the snowman has survived.",
		if timer >17 	and stepdone==5 then stepdone=6 Talkies.onAction()	maybeyoushould:play() 		fish2talk() end		-- 		"Maybe you should start thinking about how to manipulate the tables.",
		if timer >21 	and stepdone==6 then stepdone=7 Talkies.onAction()	everything:play() 			fish2talk() end		-- 		"Everything is so frozen here...",
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() ow:play() 					fish2talk() end		-- 		"Ow!",
		if timer >27 	and stepdone==8 then stepdone=9 Talkies.onAction()	ouch:play()  				fish2talk() end		-- 		"Ouch!",
		if timer >29 	and stepdone==8 then stepdone=9 Talkies.onAction()	ivenever:play()  			fish1talk() end		-- 		"I’ve never seen such an aggressive snowman.",
		if timer >33 	and stepdone==9 then stepdone=10 Talkies.onAction()	 										end		-- 		
		
end

function subtitle47()
		
		--level 47 Fire!
		
		if timer >0		and stepdone==0 then stepdone=1 					 see:play() 				fish1talk() end		-- 		"See? This ship had two gun decks. It must have been a man-of-war. The acrid smell of gunpowder still seems to linger in the air...",
		if timer >11 	and stepdone==1 then stepdone=2 Talkies.onAction()	youmeaninthewater:play() 	fish2talk() end		-- 		"You mean, in the water.",
		if timer >14	and stepdone==2 then stepdone=3 Talkies.onAction() whoseship:play() 			fish2talk() end		--	 	"Whose ship was she? Maybe she served under admiral Nelson... or captain Hornblower.",
		if timer >20 	and stepdone==3 then stepdone=4 Talkies.onAction() noithink:play() 				fish1talk() end		-- 		"No, I think you’re about a hundred years off.",
		if timer >24 	and stepdone==4 then stepdone=5 Talkies.onAction()	ihaveafeeling:play() 		fish1talk() end		-- 		"I have a feeling we’re going to need that sword.",
		if timer >27 	and stepdone==5 then stepdone=6 Talkies.onAction()	youandyourfeelings:play() 	fish2talk() end		-- 		"You and your ‘feelings’. Besides, we’re not supposed to be giving any hints.",
		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction() thatwasalittle:play() 		fish2talk() end		-- 		"That was a little too simple, wasn’t it?",
		if timer >36 	and stepdone==7 then stepdone=8 Talkies.onAction()	coulditpossibly:play()  	fish2talk() end		-- 		"Could it possibly be so simple?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	theremustbesomekind:play()  fish2talk() end		-- 		"There must be some kind of mistake.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()	end


end

function subtitle48()

		--level 45 Kitchen

		if timer >0 	and stepdone==0 then stepdone=1 				 	ohmy:play()					fish2talk() end		-- 		Oh my, this is a nice kitchen.",	
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() yeahwe:play()				fish2talk() end		-- 		"Yeah, we discovered the ship kitchen.",
		if timer >6.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	darnedkitchen:play()		fish2talk() end		-- 		"Darned kitchen.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() andnowweare:play()			fish2talk() end		--	 	"And now we are in the captain Silver’s kitchen.",
		if timer >12 	and stepdone==4 then stepdone=5 Talkies.onAction() idontthinkhe:play()			fish1talk() end		-- 		"I don’t think he was a cook himself.",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()	butthisisnot:play()			fish1talk() end		-- 		"But this is not our problem.",
		if timer >17 	and stepdone==6 then stepdone=7 Talkies.onAction()	therearenotmany:play()		fish2talk() end		-- 		"There are not many problems here. I can swim out at once.",
		if timer >20 	and stepdone==7 then stepdone=8 Talkies.onAction() butjusthavealook:play()		fish1talk() end		-- 		"But just have a look at me.",
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction()	 icantunderstand:play()		fish1talk() end		-- 		"I can’t understand how these serving tables got there.",
		if timer >26 	and stepdone==8 then stepdone=9 Talkies.onAction()	 thesetables:play()			fish1talk() end		-- 		"These tables are in the way.",
		if timer >29 	and stepdone==9 then stepdone=10 Talkies.onAction() theselittle:play()	 		fish1talk() end		-- 		"This little chamber gives me the creeps.",
		if timer >32 	and stepdone==10 then stepdone=11 Talkies.onAction() icantgetoutof:play()		fish1talk() end		-- 		"I can’t get out of here!",
		if timer >36 	and stepdone==11 then stepdone=12 Talkies.onAction() imjustthinking:play()		fish2talk() end		-- 		"I’m just thinking how to help you.",
		if timer >41 	and stepdone==12 then stepdone=13 Talkies.onAction() helpmesomehow:play()		fish1talk() end		-- 		"Help me somehow!",
		if timer >52 	and stepdone==13 then stepdone=14 Talkies.onAction() imjustthinking:play()	 	fish2talk() end		-- 		"I am thinking...",
		if timer >54 	and stepdone==14 then stepdone=15 Talkies.onAction() imonmyway:play()	 		fish2talk() end		-- 		"I am on my way.",
		if timer >56 	and stepdone==15 then stepdone=16 Talkies.onAction() yuck:play()	 			fish2talk() end		-- 		"Yuck, there is something left in this pot!",
		if timer >50 	and stepdone==16 then stepdone=17 Talkies.onAction() thispotwasnt:play() 		fish2talk() end		-- 		"This pot wasn’t even washed up.",
		if timer >53 	and stepdone==17 then stepdone=18 Talkies.onAction() nobody:play() 				fish2talk() end		--		"Nobody washes the dishes here.",
		if timer >56 	and stepdone==18 then stepdone=19 Talkies.onAction() iftherewasnochair:play() 	fish1talk() end		-- 		"If there was no chair, it would save us a lot of work.",
		if timer >52 	and stepdone==19 then stepdone=20 Talkies.onAction() especiallymyself:play()	fish1talk() end		-- 		"Especially myself.",
		if timer >54 	and stepdone==20 then stepdone=21 Talkies.onAction() whatisachair:play()	 	fish2talk() end		-- 		"What is a chair doing in a kitchen?",
		if timer >56 	and stepdone==21 then stepdone=22 Talkies.onAction() thereshouldbe:play()		fish1talk() end		-- 		"There should be no chairs in the kitchen.",
		if timer >58 	and stepdone==22 then stepdone=23 Talkies.onAction() everybodywant:play()		fish2talk() end		-- 		"Everybody wants to rest sometimes.",
		if timer >66 	and stepdone==23 then stepdone=24 Talkies.onAction() ihaveseenthesetables:play() fish2talk() end	-- 		"I have seen these tables somewhere.",
		if timer >70 	and stepdone==24 then stepdone=25 Talkies.onAction() theyaremass:play()			fish1talk() end			-- 		"They are mass-produced, you know.",
		if timer >74 	and stepdone==25 then stepdone=26 Talkies.onAction() butheyarenice:play() 		fish2talk() end		-- 		"But they are nice. We should order a few too.",
		if timer >78 	and stepdone==26 then stepdone=27 Talkies.onAction() imafraidthatonly:play()	fish1talk() end		-- 		"I am afraid that only I can get out of this space.",
		if timer >82 	and stepdone==27 then stepdone=28 Talkies.onAction() itseemsthateven:play()		fish2talk() end		-- 		"It seems that even this ship contains some demanding problems.",
		if timer >86 	and stepdone==28 then stepdone=29 Talkies.onAction() whyisthatsword:play()		fish1talk() end		-- 		"Why is that sword here?",
		if timer >90 	and stepdone==29 then stepdone=30 Talkies.onAction() maybetheyusedit:play()		fish2talk() end		-- 		"Maybe they used it to cut the meat.",
		if timer >94 	and stepdone==30 then stepdone=31 Talkies.onAction() certainly:play()			fish1talk() end		-- 		"Certainly. But you don’t cut meat with a sword in the kitchen, usually.",
		if timer >98 	and stepdone==31 then stepdone=32 Talkies.onAction() wearelucky:play()	 		fish2talk() end		-- 		"We are lucky there are no cooks here. They could try to cook us.",
		if timer >102 	and stepdone==32 then stepdone=33 Talkies.onAction() theremustbesome:play()			fish1talk() end		-- 		"There must be some recipe on that parchment.",
		if timer >106 	and stepdone==33 then stepdone=34 Talkies.onAction() theremustbesomeculinary:play()	fish1talk() end		-- 		"There must be some culinary mystery on that parchment.",
		if timer >110 	and stepdone==34 then stepdone=35 Talkies.onAction() possibly:play()			fish2talk() end		-- 		"Possibly, how to cook the fish.",
		if timer >114	and stepdone==35 then stepdone=36 Talkies.onAction() 							end		-- 		
		if timer >118 	and stepdone==36 then stepdone=37 Talkies.onAction() 							end		-- 		
	

end

function subtitle49()

		--level 49 Second mate's cabin

		if timer >0 	and stepdone==0 then stepdone=1 				 	cruecruel:play()			end		-- 		"Crrruel, crrruel, crrruel Captain Silverrrr...",
		if timer >4 	and stepdone==1 then stepdone=2 Talkies.onAction() cruelcaptain:play()			end		-- 		"Crrruel Captain Silverrrr...",
		if timer >6.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	goodgrief:play()			end		-- 		"Good grrrief!",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() trickyproblem:play()			end		--	 	"Trrricky prrroblem, isn’t it?",
		if timer >12 	and stepdone==4 then stepdone=5 Talkies.onAction() 								end		-- 		"Darrrn!",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()	pollywantsacracker:play()	end		-- 		"Polly wants a crrracker!",
		if timer >17 	and stepdone==6 then stepdone=7 Talkies.onAction()	beatitsir:play()			end		-- 		"Beat it, sirrr...",
		if timer >20 	and stepdone==7 then stepdone=8 Talkies.onAction() didntwesee:play()			fish1talk() end		-- 		"Didn’t we see this somewhere already?",
		if timer >23 	and stepdone==8 then stepdone=9 Talkies.onAction()	 noyouarewrong:play()		fish2talk() end		-- 		"No, you’re wrong. This cabin is one hundred and forty four squares larger...",
		if timer >29 	and stepdone==9 then stepdone=10 Talkies.onAction()	 lookatthatparrot:play() 	fish1talk() end		-- 		"Look at that parrot!!!",
		if timer >32 	and stepdone==10 then stepdone=11 Talkies.onAction() sowhat:play()				fish2talk() end		-- 		"So what? It’s a normal skeleton.",
		if timer >36 	and stepdone==11 then stepdone=12 Talkies.onAction() canyouseeit:play()			fish2talk() end		-- 		"Can you see it? This room is almost the last in the row. Could this really be that treasure?",
		if timer >44 	and stepdone==12 then stepdone=13 Talkies.onAction() andwhatwould:play()		fish1talk() end		-- 		"And what would be in the other two?",
		if timer >49 	and stepdone==13 then stepdone=14 Talkies.onAction() 										end		-- 		"They told us clearly to find the map. Nobody mentioned treasure.",
		if timer >52 	and stepdone==14 then stepdone=15 Talkies.onAction() dontbesoannoying:play()	fish1talk() end		-- 		"Don’t be so annoying. The authors of the game would surely let the player know.",
		if timer >56 	and stepdone==15 then stepdone=16 Talkies.onAction() 	 thisoctopus:play()		fish2talk() end		-- 		"This octopus has unpleasantly knotted tentacles. I can’t fit between them.",
		if timer >60 	and stepdone==16 then stepdone=17 Talkies.onAction() sowhatifwe:play()		 	fish1talk() end		-- 		"... so what if we cut one off?!",
		if timer >64 	and stepdone==17 then stepdone=18 Talkies.onAction() imsorry:play() 			fish1talk() end		-- 		"I am sorry, it was just an idea.",
		if timer >68 	and stepdone==18 then stepdone=19 Talkies.onAction() littleoctopus:play()		fish2talk() end		--		"Little octopus, couldn’t you move away a little bit, please?",
		if timer >72 	and stepdone==19 then stepdone=20 Talkies.onAction()  hmmthanks:play()			fish2talk() end		-- 		"Hmm... thanks a lot.",
		if timer >76 	and stepdone==20 then stepdone=21 Talkies.onAction() skeletons:play()			fish1talk() end		-- 		"Skeletons seem to be a favorite motif of this game authors.",
		if not (language=="pl") then
		if timer >80 	and stepdone==21 then stepdone=22 Talkies.onAction() 	whoareprobably:play()	fish2talk() end		-- 		"Who are probably living on land. These skeletons are visible proof of their effort to stuff the ocean with all the things that don’t belong there.",
		if timer >84 	and stepdone==22 then stepdone=23 Talkies.onAction() letstrytothink:play()		fish1talk() end		-- 		"Let’s try to think. Can’t we take our solution of that previous cabin and modify it a little?",
		if timer >88 	and stepdone==23 then stepdone=24 Talkies.onAction() 	hmmidont:play()			fish2talk() end		-- 		"Hmm... I don’t think so.",
		if timer >92 	and stepdone==24 then stepdone=25 Talkies.onAction()  	solesshed:play()		fish2talk() end		-- 		"So, let’s shed some light on it.",
		if timer >96 	and stepdone==25 then stepdone=26 Talkies.onAction() mybackaches:play()			fish1talk() end		-- 		"My back aches.",
		if timer >100 	and stepdone==26 then stepdone=27 Talkies.onAction()  										end		-- 		
		end
end

function subtitle50()

		--level 50 Captain's cabin
		
		if timer >0 	and stepdone==0 then stepdone=1 					thereisalotofgarbage:play()			fish2talk() end		-- 		"There is a lot of garbage here!",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() thiswassurely:play()					fish1talk() end		-- 		"This was surely a captain’s cabin.",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()		whatwouldyouexpect:play()		fish1talk() end		-- 		"What would you expect after so many years?",
		if timer >8.5	and stepdone==3 then stepdone=4 Talkies.onAction()		doyouthinkthatsilver:play()		fish2talk() end		-- 		"Do you think that Silver had this hook in place of his hand?",
		if timer >12 	and stepdone==4 then stepdone=5 Talkies.onAction() 	thisisashiphook:play()				fish1talk() end		-- 		"This is a ship hook. It’s used to pull up boats...",
		if timer >15 	and stepdone==5 then stepdone=6 Talkies.onAction()										fish1talk() end		-- 		"... and nets!",
		if timer >16 	and stepdone==6 then stepdone=7 Talkies.onAction()		itsastrangelookingeye:play()	fish2talk() end		-- 		"It’s a strange looking eye.",
		if timer >19 	and stepdone==7 then stepdone=8 Talkies.onAction() 	thiseyesquint:play()				fish1talk() end		-- 		"This eye squints kind of sneakily.",
		if timer >25.5 	and stepdone==8 then stepdone=9 Talkies.onAction()		haventyouseenmyeye:play()		fishstop()  end		-- 		"Haven’t you seen my eye somewhere?",
		if timer >30 	and stepdone==9 then stepdone=10 Talkies.onAction() 	thisscarfisveryimportant:play()				end		-- 		"This scarf is very important."
		if timer >33 	and stepdone==10 then stepdone=11 Talkies.onAction() 	thehumanskull:play()						end		-- 		The human skull with an empty eye socket looks really disgusting, you know.",
		if timer >39 	and stepdone==11 then stepdone=12 Talkies.onAction() 	afterthatunfortunateaccident:play()			end		-- 		"After that unfortunate accident with a teaspoon I have a completely different viewpoint of the world.",
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction()	whyamihereafterall:play()					end		-- 		"Why am I here, after all?
		if timer >49 	and stepdone==13 then stepdone=14 Talkies.onAction()	asiftheycantputsomechest:play() 			end		--		As if they can’t put some chest here... or a chamber pot.",
		if timer >54 	and stepdone==14 then stepdone=15 Talkies.onAction()	doyouappreciatemyfacial:play()  			end		-- 		"Do you appreciate my facial expressions?"
		if timer >59 	and stepdone==15 then stepdone=16 Talkies.onAction()	notbadforaskeleton:play()					end		-- 		Not bad for a skeleton, is it?",
		--if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction()		end		-- 		
		
		
end

function subtitle51()

		--level 51 Silver's Hideout
				
		if timer >0 	and stepdone==0 then stepdone=1 					ohwell:play()				fish1talk() end		-- 		"Oh well. So that map really exists after all!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() okhereisthemap:play()		fish2talk() end		-- 		"Okay. Here is the map.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	soitsclearnow:play()		fish2talk() end		-- 		"So it’s clear now that our goal is to get that map out somehow.",
		if timer >11	and stepdone==3 then stepdone=4 Talkies.onAction() ofcourse:play()				fish1talk() end		-- 		"Of course. It would be too easy, if we should just swim away.",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() donttalknonsense:play()		fish2talk() end		-- 		"Don’t talk nonsense and try to think instead.",
		if timer >20 	and stepdone==5 then stepdone=6 Talkies.onAction()	ourgoal:play()				fish1talk() end		-- 		"Our goal is to get out that map somehow.",
		if timer >26 	and stepdone==6 then stepdone=7 Talkies.onAction()	whatcanbe:play()			fish1talk() end		-- 		"What can be on that map?",
		if timer >32 	and stepdone==7 then stepdone=8 Talkies.onAction() doyouthink:play()			fish1talk() end		-- 		"Do you think that it really shows Silver’s treasure?",
		if timer >34 	and stepdone==8 then stepdone=9 Talkies.onAction()	weshall:play() 				fish2talk() end		-- 		"We shall see when we manage to get the darned map out.",
		if timer >39 	and stepdone==9 then stepdone=10 Talkies.onAction() wedneedmore:play()	 		fish2talk() end		-- 		"We’d need more of these snails.",
		if timer >41 	and stepdone==10 then stepdone=11 Talkies.onAction()	youmeanescargots:play() fish1talk() end		-- 		"You mean Escargots, don’t you?",
		if timer >47 	and stepdone==11 then stepdone=12 Talkies.onAction()	weneedmore:play()	 	fish1talk() end		-- 		"We’d need more glass eyes.",
		if timer >51 	and stepdone==12 then stepdone=13 Talkies.onAction()	theeasiestway:play()	fish1talk() end		-- 		"The easiest way to do it would be to get to the upper part of this level. Let’s try to restart it, maybe we will appear there.",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction()	dontpretend:play() 		fish2talk() end		-- 		"Don’t pretend that you didn’t understand the rules after so many solved levels.",
		if timer >62 	and stepdone==14 then stepdone=15 Talkies.onAction()	sowemanaged:play() 		fish2talk() end		-- 		"So we managed to move it.",
		if timer >63 	and stepdone==15 then stepdone=16 Talkies.onAction()	letskeepup:play() 		fish1talk() end		-- 		"Let’s keep up the good work.",
		if timer >67 	and stepdone==16 then stepdone=17 Talkies.onAction()	yesitsalmostdone:play()	fish2talk() end		-- 		"Yes, it’s almost done.",
		--if timer >68 	and stepdone==17 then stepdone=18 Talkies.onAction()	 						end		-- 		
end

function subtitle51end()
		if timer >0 	and stepdone==0 then stepdone=1 					aftermuchhardship:play()			end		-- 		"After much hardship we succeeded in finding captain Silver’s map.",
		if timer >6 	and stepdone==1 then stepdone=2 Talkies.onAction() initialenthusiasm:play()			end		-- 		"Initial enthusiasm afterwards changed into bitter disappointment,",
		if timer >11.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	whenwefound:play()					end		-- 		"when we found out that the above mentioned map does not indicate the location of the treasure,",
		if timer >18	and stepdone==3 then stepdone=4 Talkies.onAction() buttheplaceofresidence:play()		end		-- 		"but the place of residence of the last surviving Silver’s parrot that,",
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() thatunfortunately:play()			end		-- 		"unfortunately, suffers from sclerosis and cannot remember, where the treasure is.",
		if timer >26.5 	and stepdone==5 then stepdone=6 					andcannotremember:play()			end		-- 		
		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()										end		-- 		
		--[[
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction()	equalizerload("/externalassets/ends/2silversship/audios/en/1aftermuchhardship.ogg") end		-- 		"After much hardship we succeeded in finding captain Silver’s map.",
		if timer >6 	and stepdone==1 then equalizerupdate(dt) stepdone=2 Talkies.onAction() equalizerload("/externalassets/ends/2silversship/audios/en/2initialenthusiasm.ogg") end		-- 		"Initial enthusiasm afterwards changed into bitter disappointment,",
		if timer >11.5 	and stepdone==2 then stepdone=3 Talkies.onAction()	equalizerload("/externalassets/ends/2silversship/audios/en/3whenwefound")			end		-- 		"when we found out that the above mentioned map does not indicate the location of the treasure,",
		if timer >18	and stepdone==3 then stepdone=4 Talkies.onAction() equalizerload("/externalassets/ends/2silversship/audios/en/4buttheplaceofresidence") end		-- 		"but the place of residence of the last surviving Silver’s parrot that,",
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() equalizerload("/externalassets/ends/2silversship/audios/en/5thatunfortunately")		end		-- 		"unfortunately, suffers from sclerosis and cannot remember, where the treasure is.",
		if timer >26.5 	and stepdone==5 then stepdone=6 					equalizerload("/externalassets/ends/2silversship/audios/en/6andcannotremember")		end		-- 		
		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()								end		-- 		
			
			--equalizerupdate(dt)
		--]]
end

function subtitle52()

		--level 52 Power plant
	if language=="en" then
		if timer >0 	and stepdone==0 then stepdone=1 					mygod:play()				fish2talk() end		-- 		"My God, can you see that pink thing over there?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() whatcoulditbe:play()			fish1talk() end		-- 		"What could it be?",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	ihavenoidea:play()			fish2talk() end		-- 		"I have no idea, but it’s disgusting.",
		if timer >11	and stepdone==3 then stepdone=4 Talkies.onAction() whatdoyouthinkufo:play()	fish2talk() end		-- 		"What do you think about these rods?",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() theylooklike:play()			fish1talk() end		-- 		"They look like they’re some kind of fuel rods or maybe regulators for the nuclear reactor.",
		if timer >20 	and stepdone==5 then stepdone=6 Talkies.onAction()	unless:play()				fish1talk() end		-- 		"Unless, of course, this reactor works on some completely different principle.",
		if timer >26 	and stepdone==6 then stepdone=7 Talkies.onAction()	itcouldofcourse:play()		fish2talk() end		-- 		"It could, of course, be a nucleo-particle megadisruptor with ultramezon colliders.",
		if timer >32 	and stepdone==7 then stepdone=8 Talkies.onAction() thatsounds:play()			fish1talk() end		-- 		"That sounds reasonable...",
		if timer >34 	and stepdone==8 then stepdone=9 Talkies.onAction()	thenagain:play() 			fish2talk() end		-- 		"Then again, maybe this isn’t a nuclear reactor.",
		if timer >39 	and stepdone==9 then stepdone=10 Talkies.onAction() sowhatisit:play()	 		fish1talk() end		-- 		"So what is it, then?",
		if timer >41 	and stepdone==10 then stepdone=11 Talkies.onAction()	howshouldiknow:play() 	fish2talk() end		-- 		"How should I know? A cinematoscope? A child’s toy? A shooting gallery?",
		if timer >47 	and stepdone==11 then stepdone=12 Talkies.onAction()	idrather:play() 		fish1talk() end		-- 		"I’d rather stick to a more rational explanation.",
		if timer >51 	and stepdone==12 then stepdone=13 Talkies.onAction()	buthowdo:play()			fish2talk() end		-- 		"But how do you know what’s a rational explanation and what isn’t?",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction()	welljusthowmany:play()	fish1talk() end		-- 		"Well just how many flying saucers, in your opinion, have a shooting gallery on board? And how many have a power plant?",
		if timer >62 	and stepdone==14 then stepdone=15 Talkies.onAction()	yousee:play() 			fish1talk() end		-- 		"You see?",
		if timer >63 	and stepdone==15 then stepdone=16 Talkies.onAction()	ihope:play() 			fish2talk() end		-- 		"I hope we don’t start some unpleasant reaction.",
		if timer >67 	and stepdone==16 then stepdone=17 Talkies.onAction()	dontthink:play() 		fish1talk() end		-- 		"Don’t think. Just push the rods.",
		--if timer >68 	and stepdone==17 then stepdone=18 Talkies.onAction()	 						end		-- 		
		--if timer >70 	and stepdone==17 then stepdone=18 Talkies.onAction()	 						end		-- 		
	elseif not (language=="en") then
		if timer >0 	and stepdone==0 then stepdone=1 					mygod:play()				fish2talk() end		-- 		"My God, can you see that pink thing over there?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() whatcoulditbe:play()			fish1talk() end		-- 		"What could it be?",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction()	ihavenoidea:play()			fish2talk() end		-- 		"I have no idea, but it’s disgusting.",
		if timer >12	and stepdone==3 then stepdone=4 Talkies.onAction() whatdoyouthinkufo:play()	fish2talk() end		-- 		"What do you think about these rods?",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() theylooklike:play()			fish1talk() end		-- 		"They look like they’re some kind of fuel rods or maybe regulators for the nuclear reactor.",
		if timer >24 	and stepdone==5 then stepdone=6 Talkies.onAction()	unless:play()				fish1talk() end		-- 		"Unless, of course, this reactor works on some completely different principle.",
		if timer >33 	and stepdone==6 then stepdone=7 Talkies.onAction()	itcouldofcourse:play()		fish2talk() end		-- 		"It could, of course, be a nucleo-particle megadisruptor with ultramezon colliders.",
		if timer >40 	and stepdone==7 then stepdone=8 Talkies.onAction() thatsounds:play()			fish1talk() end		-- 		"That sounds reasonable...",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	thenagain:play() 			fish2talk() end		-- 		"Then again, maybe this isn’t a nuclear reactor.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() sowhatisit:play()	 		fish1talk() end		-- 		"So what is it, then?",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction()	howshouldiknow:play() 	fish2talk() end		-- 		"How should I know? A cinematoscope? A child’s toy? A shooting gallery?",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction()	idrather:play() 		fish1talk() end		-- 		"I’d rather stick to a more rational explanation.",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction()	buthowdo:play()			fish2talk() end		-- 		"But how do you know what’s a rational explanation and what isn’t?",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction()	welljusthowmany:play()	fish1talk() end		-- 		"Well just how many flying saucers, in your opinion, have a shooting gallery on board? And how many have a power plant?",
		if timer >80 	and stepdone==14 then stepdone=15 Talkies.onAction()	yousee:play() 			fish1talk() end		-- 		"You see?",
		if timer >85 	and stepdone==15 then stepdone=16 Talkies.onAction()	ihope:play() 			fish2talk() end		-- 		"I hope we don’t start some unpleasant reaction.",
		if timer >90 	and stepdone==16 then stepdone=17 Talkies.onAction()	dontthink:play() 		fish1talk() end		-- 		"Don’t think. Just push the rods.",
	end
end


function subtitle53()

		--level 53 Strange forces

		if timer >0 	and stepdone==0 then stepdone=1 			 		lookufo:play()					fish1talk() end		-- 		"Look. Another energy source.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	dontworry:play()			fish2talk() end		-- 		"Don’t worry. This magnet is here only to hinder us.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()		ithink:play()			fish1talk() end		-- 		"I think we are going to need it.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 	thisis:play()				fish2talk() end		-- 		"This is a very strange object.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 	ithinkthatufo:play()		fish1talk() end		-- 		"I think that UFO is going to be full of such surprises.",
		if timer >17 	and stepdone==5 then stepdone=6 Talkies.onAction() 	stop:play()					fish2talk() end		-- 		"Stop developing your theories and start solving.",
		if timer >24 	and stepdone==6 then stepdone=7 Talkies.onAction()		thislooks:play()		fish2talk() end		-- 		"This looks like a radio.",
		if timer >28 	and stepdone==7 then stepdone=8 Talkies.onAction()		howcould:play()			fish1talk() end		-- 		"How could a radio get here?",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction() 	letitbe:play()				fish2talk() end		-- 		"Let it be.",
		if timer >34 	and stepdone==9 then stepdone=10 Talkies.onAction()	thisis:play()				fish1talk() end		-- 		"This is a strange radio.",
		if timer >37 	and stepdone==10 then stepdone=11 Talkies.onAction()	what:play() 			fish2talk() end		-- 		"What have we here?",
		if timer >40 	and stepdone==11 then stepdone=12 Talkies.onAction()	itlooks:play() 			fish2talk() end		-- 		"It looks like a pistol to me.",
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction()	maybe:play() 			fish1talk() end		-- 		"Maybe it’s a laser weapon.",
		if timer >56 	and stepdone==13 then stepdone=14 Talkies.onAction()	itdoesnt:play() 		fish2talk() end		-- 		"It doesn’t matter. We don’t need it.",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction()	stoppointing:play() 	fish1talk() end		-- 		"Stop pointing at me with it!",
		if timer >62 	and stepdone==15 then stepdone=16 Talkies.onAction()	asifyoudont:play() 		fish2talk() end		-- 		"As if you don’t know that you can’t turn objects in this game.",
		if timer >64 	and stepdone==16 then stepdone=17 Talkies.onAction()	thereare:play() 		fish2talk() end		-- 		"There are too many steel constructions here.",
		if timer >68 	and stepdone==17 then stepdone=18 Talkies.onAction()	dontforget:play() 		fish1talk() end		-- 		"Don’t forget that we are inside of the flying saucer!",
		if timer >70 	and stepdone==18 then stepdone=19 Talkies.onAction()	maybe:play() 			fish2talk() end		-- 		"Maybe I could learn how to manipulate these steel tubes.",
		if timer >72 	and stepdone==19 then stepdone=20 Talkies.onAction()	noicant:play() 			fish2talk() end		-- 		"No, I can’t move it.",
		if timer >74 	and stepdone==20 then stepdone=21 Talkies.onAction()	isquiet:play() 			fish1talk() end		-- 		"It’s quite tight here.",
		if timer >74 	and stepdone==21 then stepdone=22 Talkies.onAction()	thesemagnets:play() 	fish2talk() end		-- 		"These magnets are giving me the creeps.",
		if timer >80 	and stepdone==22 then stepdone=23 Talkies.onAction()	thismagnetic:play() 	fish1talk() end		--		"This magnetic field probably has some invigorating effects.",
		if timer >84 	and stepdone==23 then stepdone=24 Talkies.onAction()	itsdoing:play() 		fish2talk() end		--	 	"It’s doing me no good.",
end

function subtitle54()

		--level 54 Brm Brm
		if timer >0 	and stepdone==0 then stepdone=1 					 ithinkwecangain:play() 	fish2talk() end		-- 		"I think we can gain some information about the interstellar propulsion here."
		if timer >7 	and stepdone==1 then stepdone=2 Talkies.onAction() thislooks:play()				fish1talk() end		-- 		"This looks more like conventional propulsion for the landing craft."
		if timer >11 	and stepdone==2 then stepdone=3 Talkies.onAction()	sowefoundthedrive:play() 	fish2talk() end		--		"So. We found the drive. We have achieved one of the objectives of our mission."
		if timer >16		and stepdone==3 then stepdone=4 Talkies.onAction() dontbesohasty:play()		fish1talk() end		-- 		"Don’t be so hasty. We haven’t searched the whole wreck yet."
		if timer >21 	and stepdone==4 then stepdone=5 Talkies.onAction() imsorry:play()				fish2talk() end		-- 		"I am sorry that none of these technical marvels around us work.",
		if timer >26 	and stepdone==5 then stepdone=6 Talkies.onAction()	iwould:play()				fish2talk() end		-- 		"I’d like to see some of these extraterrestrial gizmos in action.",
		if timer >31 	and stepdone==6 then stepdone=7 Talkies.onAction()	iwonder:play()				fish2talk() end		-- 		"I wonder if this motor could work under water.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() maybeits:play()				fish1talk() end		-- 		"Maybe it’s better for us if it doesn’t work.",
		if timer >38 	and stepdone==8 then stepdone=9 Talkies.onAction()	 imrather:play()			fish1talk() end		-- 		"I am rather glad we cannot turn anything on here.",
		if timer >42 	and stepdone==9 then stepdone=10 Talkies.onAction() weshould:play()				fish1talk() end		-- 		"We should be happy that we could not switch anything on yet.",
		
end

function subtitle54engineon()

		if timer >1 	and stepdone==0 then stepdone=1 				 	whathaveyoudone:play()			fish1talk() end		-- 		"What have you done? Turn off that roar!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()	whathaveyouactivated:play()		fish1talk() end		-- 		"What have you activated? Where is it taking us?",
		if timer >9 	and stepdone==2 then stepdone=3 Talkies.onAction() thisisterrible:play()			fish2talk() end		-- 		"This is terrible! Turn it off before it’s too late!",
		if timer >13 	and stepdone==3 then stepdone=4 Talkies.onAction()	icant:play()					fish1talk() end		-- 		"I can’t! I can’t take it out!",
		if timer >16	and stepdone==4 then stepdone=5 Talkies.onAction() idontknowhow:play()					fish1talk() end		-- 		"I don’t know how! I can’t take it out!",
		if timer >20 	and stepdone==5 then stepdone=6 Talkies.onAction() mayday:play()					fish2talk() end		-- 		"Mayday! Mayday!",
		if timer >22 	and stepdone==6 then stepdone=7 Talkies.onAction()	howcani:play()					fish2talk() end		-- 		"How can I turn it off?!",
		--if timer >22 	and stepdone==8 then stepdone=9 Talkies.onAction()	icant2:play()					end		-- 		"I can’t turn it off!"

end

function subtitle54engineoff()

		if timer >0 	and stepdone==0 then stepdone=1 				 	ohfinally:play()			fish1talk() end		-- 		"Finally.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() whatarelief:play()			fish1talk() end		-- 		"What a relief.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	thanks:play()				fish2talk() end		-- 		"Thanks.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() finally:play()				fish2talk() end		-- 		"Finally.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() imonlyafraid:play()			fish1talk() end		-- 		"I am only afraid we’ll have to turn it on again.",
		--if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()								end		-- 		

end

function subtitle54aproachengine()
		if timer >0 	and stepdone==0 then stepdone=1 				 	whatareyoudoing:play()	aproachengineonce=true	fish1talk() end		-- 		"What are you doing? Where are you going?",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() careful:play()								fish1talk() end		-- 		"Careful with that wrench.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	icantharm:play()	 aproachengineonce=true	fish2talk() end		--		"I can’t harm anything here."
		
end

function subtitle55()

		if timer >0 	and stepdone==0 then stepdone=1 				 	itso:play()			fish2talk() end		-- 		"It’s so quiet here...",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() boy:play()			fish2talk() end		-- 		"Boy is it quiet...",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()						end		-- 		

end

function subtitle56lightswitchon()

		if timer >0 	and stepdone==0 then stepdone=1 				 	finallythelights:play()			fish2talk() end		-- 		"Finally, the lights are back on...",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() ineverthought:play()				fish2talk() end		-- 		"I never thought I’d be so glad to see you again.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	thisflickering:play()			fish2talk() end		-- 		"This flickering light is beginning to make my eyes hurt.",

end

function subtitle56lightswitchoff()

		if timer >2 	and stepdone==0 then stepdone=1 			 		turnonthelights:play()		end		-- 		"Turn on the lights, NOW!",
		if timer >6 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatdoyouthinkyou:play()		end		-- 		"What do you think you are doing?",
		if timer >8		and stepdone==2 then stepdone=3 Talkies.onAction() 	wait:play()					end		-- 		"Wait, I’ll try to find the switch.",
		if timer >12 	and stepdone==3 then stepdone=4 Talkies.onAction()	cutitout:play()			end		-- 		"Cut it out and turn on the lights.",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() 	icant:play()				end		-- 		"I can’t. The dang switch fell down.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	turnonyourself:play()	end		-- 		"Turn on the lights yourself, if you’re so smart.",
		if timer >23 	and stepdone==6 then stepdone=7 Talkies.onAction()	whereareyou1:play()		end		-- 		"Where are you?",
		if timer >27 	and stepdone==7 then stepdone=8 Talkies.onAction() 	hello:play()				end		-- 		"Hello-o-o.",
		if timer >35 	and stepdone==8 then stepdone=9 Talkies.onAction()	saysomething:play()			end		-- 		"Say something.",
		if timer >40 	and stepdone==9 then stepdone=10 Talkies.onAction()	here:play()					end		-- 		"Here.",
		if timer >45 	and stepdone==10 then stepdone=11 Talkies.onAction()	hereiam:play()				end		--	 	"Here I am.",
		if timer >50 	and stepdone==11 then stepdone=12 Talkies.onAction()	iamrightover:play()			end		-- 		"I am right over here.",
		if timer >55 	and stepdone==12 then stepdone=13 Talkies.onAction()	whereareyou:play()			end		-- 		"Where are you, I am afraid.",
		if timer >60 	and stepdone==13 then stepdone=14 Talkies.onAction()	imafraidof:play()			end		-- 		"I’m afraid of the dark.",
		if timer >65 	and stepdone==14 then stepdone=15 Talkies.onAction()	pleasedontleave:play()		end		-- 		"Please don’t leave. It’s so dark in here."
		if timer >70 	and stepdone==15 then stepdone=16 Talkies.onAction()	dontbeafraid:play() 		end		-- 		"Don’t be afraid. I’m here.",
		if timer >75 	and stepdone==16 then stepdone=17 Talkies.onAction()	stopwhining:play() 			end		-- 		"Stop whining. We’ll manage.",
		if timer >80 	and stepdone==17 then stepdone=18 Talkies.onAction()	theresnothing:play() 		end		-- 		"There’s nothing to be afraid of.",
		--if timer >77 	and stepdone==18 then stepdone=19 Talkies.onAction()	 		end		-- 					"I hope it doesn’t kick me...",
		
end

function subtitle56aproachrobodog()

		if timer >0 	and stepdone==0 then stepdone=1 			 			lookthat:play()					fish1talk() end		-- 		"Look! That must be... ROBODOG!",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		whatthatrobodog:play()		fish2talk() end		-- 		"What? THAT robodog?!?",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()		becareful:play()			fish1talk() end		-- 		"Be careful. It bites!",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 		ihope:play()				fish2talk() end		-- 		"I hope it doesn’t kick me...",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 		wellnow:play()				fish1talk() end		-- 		"Well now it all makes sense.",
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()		includingthecuban:play()	fish2talk() end		-- 		"Including the Cuban missile crisis and Nostradamus’ prophecies.",
		if timer >18 	and stepdone==6 then stepdone=7 Talkies.onAction()		includingjohnlehnon:play()	fish2talk() end		-- 		"Including the murder of John Lennon and the Tunguzian meteor.",
		if timer >23 	and stepdone==7 then stepdone=8 Talkies.onAction() 		includingbull:play()		fish2talk() end		-- 		"Including the Challenger explosion and the Battle of Bull Run.",
		if timer >28 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	includingchernobyl:play()	fish2talk() end		-- 		"Including the Chernobyl meltdown and the wreck of Noah’s Ark.",
		if timer >32 	and stepdone==9 then stepdone=10 Talkies.onAction()		nowicansee:play()			fish1talk() end		-- 		"Now I can see the rationale behind this game.",
		if timer >36 	and stepdone==10 then stepdone=11 Talkies.onAction()	andalsothemeaning:play()	fish2talk() end		-- 		"And also the meaning of life, the universe and everything.",

		if timer >40 	and stepdone==11 then stepdone=12 Talkies.onAction()	dontlisten:play() 		end		-- 		"Don’t listen to them, player. They are kidding you.",
		if timer >43 	and stepdone==12 then stepdone=13 Talkies.onAction()	dontlisten2:play() 		end		-- 		"Don’t listen to them, player. They are trying to be funny.",
		if timer >47 	and stepdone==13 then stepdone=14 Talkies.onAction()	dontlisten3:play() 		end		-- 		"Don’t listen to them, player. They are trying to fool you.",
		if timer >50 	and stepdone==14 then stepdone=15 Talkies.onAction()	iambut:play()			end		-- 		"I am but a humble cybernetic toy.",
		if timer >53 	and stepdone==15 then stepdone=16 Talkies.onAction()	though:play() 			end		-- 		"Though originally I was to be a scooter.",
		if timer >55 	and stepdone==16 then stepdone=17 Talkies.onAction()	though2:play() 			end		-- 		"Though originally I was to be a lawn-mower.",
		if timer >58 	and stepdone==17 then stepdone=18 Talkies.onAction()	though3:play() 			end		-- 		"Though originally I was to be a space orbital station.",
		if timer >62 	and stepdone==18 then stepdone=19 Talkies.onAction()	though4:play() 			end		-- 		"Though orginally I was to be a piece of cake.",
		if timer >65 	and stepdone==19 then stepdone=20 Talkies.onAction()	andivisited:play() 		end		-- 		"And I visited Cuba only once.",
		if timer >67 	and stepdone==20 then stepdone=21 Talkies.onAction()	andeverybody:play()		end		-- 		"And everybody knows somebody else murdered him, they can’t prove anything.",
		if timer >72 	and stepdone==21 then stepdone=22 Talkies.onAction()	andwhocares:play()		end		--		"And who cares about a few soldiers.",
		if timer >75 	and stepdone==22 then stepdone=23 Talkies.onAction()	andtheytoldme:play()	end		--		"And they told me that the regulatory rod wasn’t important.",
end

function subtitle57()

		--level 57 Experiments

		if timer >0 	and stepdone==0 then stepdone=1 love.audio.play( spacehorn )									end		-- 		"This must be some kind of laboratory.",
		if timer >3 	and stepdone==1 then stepdone=2 			 	this:play()							fish2talk() end		-- 		
		
		if timer >6 	and stepdone==2 then stepdone=3 Talkies.onAction() itseems:play()					fish2talk() end		-- 		"It seems that even UFOs can be equipped with an on-board lab.",
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction()	--this2:play()
																											fishstop()	end		-- 		"This looks like a laboratory.",
		if timer >12	and stepdone==4 then stepdone=5 Talkies.onAction() theseexperiments:play()			fish1talk() end		-- 		"These experiments seem quite suspicious to me.",
		if timer >17 	and stepdone==5 then stepdone=6 Talkies.onAction() ithink:play()					fish1talk() end		-- 		"I think that someone’s carrying out some rather suspicious experiments here.",
		if timer >23 	and stepdone==6 then stepdone=7 Talkies.onAction()	mygod:play()					fish2talk() end		-- 		"My gosh, what’s this?!?",
		if timer >26 	and stepdone==7 then stepdone=8 Talkies.onAction()	everything:play()				fish1talk() end		-- 		"Everything’s alive here.",
		if timer >28 	and stepdone==8 then stepdone=9 Talkies.onAction() shh:play()						fish2talk() end		-- 		"Shhhhh, I need to examine this laboratory.",
		-- Events
		
		if timer >52 	and stepdone==14 then stepdone=15 Talkies.onAction() mygoodness:play()				fish1talk() end		-- 		"My goodness, look at those things reproduce!",
		if timer >54 	and stepdone==15 then stepdone=16 Talkies.onAction() ivenever2:play()	 			fish2talk() end		-- 		"I’ve never seen this kind of organism before.",
		if timer >58 	and stepdone==16 then stepdone=17 Talkies.onAction() help:play()		 			fish2talk() end		-- 		"Help! It’s looking at me!",
		if timer >62 	and stepdone==17 then stepdone=18 Talkies.onAction() yuck:play()					fish2talk() end		-- 		"Yuck.",
		if timer >68 	and stepdone==18 then stepdone=19 Talkies.onAction() thathand:play()				fish2talk() end		-- 		"That hand gives me the creeps...",
		if timer >70 	and stepdone==19 then stepdone=20 Talkies.onAction() theres:play()					fish2talk() end		-- 		"There’s a strange odor coming from this test-tube...",
		if timer >73 	and stepdone==20 then stepdone=21 Talkies.onAction() all:play()						fish2talk() end		-- 		"All of this is making my head spin...",
		if timer >76 	and stepdone==21 then stepdone=22 Talkies.onAction() icant:play()					fish1talk() end		-- 		"I can’t swim through here. We’re going to have to push this aside.",
		if timer >79 	and stepdone==22 then stepdone=23 Talkies.onAction() icant2:play()					fish1talk() end		-- 		"I can’t swim through here. We’re going to have to push this down.",
		if timer >82 	and stepdone==23 then stepdone=24 Talkies.onAction() icant3:play()					fish1talk() end		-- 		"We’re going to need to throw on a lot more of these things.",
		if timer >84 	and stepdone==24 then stepdone=25 Talkies.onAction() 											end		-- 		"More.",
		if timer >88 	and stepdone==25 then stepdone=26 Talkies.onAction() wehave:play()					fish2talk() end		-- 		"We have to look everything here over very carefully.",
		if timer >92 	and stepdone==26 then stepdone=27 Talkies.onAction() idrather:	play()				fish1talk() end		-- 		"I’d rather get out of here.",
		if timer >94 	and stepdone==27 then stepdone=28 Talkies.onAction() youknow:play()					fish1talk() end		-- 		"You know, I’d really rather get out of here.",
		if timer >96 	and stepdone==28 then stepdone=29 Talkies.onAction() theodor:play()					fish1talk() end		-- 		"The odor from this bottle is making me sick.",
		if timer >100 	and stepdone==29 then stepdone=30 Talkies.onAction() 								fish2talk() end		-- 		ups:play() "Ooops, it broke...",
		if timer >102 	and stepdone==30 then stepdone=31 Talkies.onAction() ithink2:play()		 			fish2talk() end		-- 		"I think that some of these creatures here are a result of genetic engineering.",
		if timer >104 	and stepdone==31 then stepdone=32 Talkies.onAction() thisis:play()		 			fish2talk() end		-- 		"This is just too much!",
		if timer >106 	and stepdone==32 then stepdone=33 Talkies.onAction() howmorbid:play()			 	fish2talk() end		-- 		"How morbid!",
		if timer >108 	and stepdone==33 then stepdone=34 Talkies.onAction() thiscorpse:play()				fish2talk() end		-- 		"This corpse looks familiar to me.",
		if timer >110 	and stepdone==34 then stepdone=35 Talkies.onAction()			 					fish1talk() end		-- 		
--[[--]]
end

function subtitle57_3eyes()
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction() im:play()					fish2talk() end		-- 		"I’m little bit afraid of that thing behind the door...",
		if timer >3 	and stepdone==1 then stepdone=2 Talkies.onAction() why:play()					fish2talk() end		-- 		"Why does it have three eyes?",
		if timer >6 	and stepdone==2 then stepdone=3 Talkies.onAction() stop:play() 					fish1talk() end		-- 		"Stop what you’re doing down there and come help me!",
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction() ivenever:play()		 		fish2talk() end		-- 		"I would never have thought that we’d find so many living things inside a UFO...",
		if timer >18 	and stepdone==4 then stepdone=5 Talkies.onAction() there:play()					fish1talk() end		-- 		"There are an incredible number of different creatures here!",
end

function subtitle58()

		--level 58 Real Propulsion

		if timer >0 	and stepdone==0 then stepdone=1 				 	sothatshow:play()			fish1talk() end		-- 		"So that’s how it is!",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() ohno:play()					fish2talk() end		-- 		"Oh, no!",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()	biologicalpower:play()		fish1talk() end		-- 		"Biological power. We should have thought about it earlier. We had every hint.",
		if timer >11		and stepdone==3 then stepdone=4 Talkies.onAction() andwhatwas:play()		fish2talk() end		-- 		"And what was that nuclear power plant for?",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() cantyousee:play()			fish1talk() end		-- 		"Can’t you see? It powers that Coca-Cola vending machine over there.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	whataboutthatmotor:play()	fish2talk() end		-- 		"And what about that motor?",
		if timer >21 	and stepdone==6 then stepdone=7 Talkies.onAction()	itmakes:play()				fish1talk() end		-- 		"It makes the level run around the display.",
		if timer >24 	and stepdone==7 then stepdone=8 Talkies.onAction() nobodyis:play()				fish1talk() end		-- 		"Nobody is going to believe us.",
		
		if timer >27 	and stepdone==8 then stepdone=9 Talkies.onAction() maybeyes:play()	 			fish2talk() end		-- 		"Maybe yes. FDTO works on a project to make use of bioenergy.",
		if timer >32 	and stepdone==9 then stepdone=10 Talkies.onAction() wehave:play()	 			fish1talk() end		-- 		"We have to get out this device.",
		if timer >34 	and stepdone==10 then stepdone=11 Talkies.onAction() ifcanseeit:play()	 		fish2talk() end		-- 		"If I can see it correctly, it is composed of six parts.",
		if timer >38 	and stepdone==11 then stepdone=12 Talkies.onAction() thecagein:play()	 		fish1talk() end		-- 		"The cage in the middle is probably the most important.",
		if timer >42.5 	and stepdone==12 then stepdone=13 Talkies.onAction() okay:play() 				fish2talk() end		-- 		"Okay, we shall take this one.",
		if timer >45 	and stepdone==13 then stepdone=14 Talkies.onAction() look:play()	 			fish1talk() end		-- 		"Look, there is some strange life form, over there. Shouldn’t we try to contact them?",
		if timer >50.5 	and stepdone==14 then stepdone=15 Talkies.onAction() ourmission:play()	 		fish2talk() end		-- 		"Our mission was clear - principles and construction of the interstellar propulsion. Not a single word about contact.",
		if timer >59.5 	and stepdone==15 then stepdone=16 Talkies.onAction() well:play() 				fish1talk() end		-- 		"Well. But they could at least help us.",
		if timer >63 	and stepdone==16 then stepdone=17 Talkies.onAction() goodafternoon:play() 		fish2talk() end		-- 		"Good afternoon, excuse me, please, couldn’t I loot your drive?",
		if timer >70 	and stepdone==17 then stepdone=18 Talkies.onAction() goodafternoon2:play() 		fish2talk() end		-- 		"Hmm, good afternoon sir, could I ask you to help us to dismantle your propulsion unit, please?",
		if timer >78 	and stepdone==18 then stepdone=19 Talkies.onAction() ourgoal:play() 			fish1talk() end		-- 		"Our goal in this level is to get out the cage with that animal.",
		if timer >84 	and stepdone==19 then stepdone=20 Talkies.onAction() ihaveafeeling:play()		fish2talk() end		-- 		"I have a feeling there was an argument between these two.",
		if timer >88 	and stepdone==20 then stepdone=21 Talkies.onAction() inever:play()				fish1talk() end		-- 		"I never imagined that meeting aliens would look this way.",
		if timer >94 	and stepdone==21 then stepdone=22 Talkies.onAction() andwhatdidyou:play() 		fish2talk() end		-- 		"And what did you expect? That they will kidnap your sister?",

end

function subtitle58alternative()

		if timer >0 	and stepdone==0 then stepdone=1 				 	sothatshow:play()			fish1talk() end		-- 		"So that’s how it is!",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() ohno:play()					fish2talk() end		-- 		"Oh, no!",
		if timer >5 	and stepdone==2 then stepdone=3 Talkies.onAction()	biologicalpower:play()		fish1talk() end		-- 		"Biological power. We should have thought about it earlier. We had every hint.",
		if timer >11		and stepdone==3 then stepdone=4 Talkies.onAction() andwhatwas:play()		fish2talk() end		-- 		"And what was that nuclear power plant for?",
		if timer >14 	and stepdone==4 then stepdone=5 Talkies.onAction() cantyousee:play()			fish1talk() end		-- 		"Can’t you see? It powers that Coca-Cola vending machine over there.",
		if timer >19 	and stepdone==5 then stepdone=6 Talkies.onAction()	whataboutthatmotor:play()	fish2talk() end		-- 		"And what about that motor?",
		if timer >21 	and stepdone==6 then stepdone=7 Talkies.onAction()	itmakes:play()				fish1talk() end		-- 		"It makes the level run around the display.",
		if timer >24 	and stepdone==7 then stepdone=8 Talkies.onAction() nobodyis:play()				fish1talk() end		-- 		"Nobody is going to believe us.",
		
		if timer >27 	and stepdone==8 then stepdone=9 Talkies.onAction() maybeyes:play()	 			fish2talk() end		-- 		"Maybe yes. FDTO works on a project to make use of bioenergy.",
		if timer >32 	and stepdone==9 then stepdone=10 Talkies.onAction() wehave:play()	 			fish1talk() end		-- 		"We have to get out this device.",
		if timer >34 	and stepdone==10 then stepdone=11 Talkies.onAction() ifcanseeit:play()	 		fish2talk() end		-- 		"If I can see it correctly, it is composed of six parts.",
		if timer >38 	and stepdone==11 then stepdone=12 Talkies.onAction() thecagein:play()	 		fish1talk() end		-- 		"The cage in the middle is probably the most important.",
		if timer >42.5 	and stepdone==12 then stepdone=13 Talkies.onAction() okay:play() 				fish2talk() end		-- 		"Okay, we shall take this one.",
		if timer >45 	and stepdone==13 then stepdone=14 Talkies.onAction() look:play()	 			fish1talk() end		-- 		"Look, there is some strange life form, over there. Shouldn’t we try to contact them?",
		if timer >50.5 	and stepdone==14 then stepdone=15 Talkies.onAction() ourmission:play()	 		fish2talk() end		-- 		"Our mission was clear - principles and construction of the interstellar propulsion. Not a single word about contact.",
		if timer >59.5 	and stepdone==15 then stepdone=16 Talkies.onAction() well:play() 				fish1talk() end		-- 		"Well. But they could at least help us.",
		if timer >63 	and stepdone==16 then stepdone=17 Talkies.onAction() goodafternoon:play() 		fish2talk() end		-- 		"Good afternoon, excuse me, please, couldn’t I loot your drive?",
		if timer >70 	and stepdone==17 then stepdone=18 Talkies.onAction() goodafternoon2:play() 		fish2talk() end		-- 		"Hmm, good afternoon sir, could I ask you to help us to dismantle your propulsion unit, please?",
		if timer >78 	and stepdone==18 then stepdone=19 Talkies.onAction() ourgoal:play() 			fish1talk() end		-- 		"Our goal in this level is to get out the cage with that animal.",
		if timer >84 	and stepdone==19 then stepdone=20 Talkies.onAction() ihaveafeeling:play()		fish2talk() end		-- 		"I have a feeling there was an argument between these two.",
		if timer >88 	and stepdone==20 then stepdone=21 Talkies.onAction() inever:play()				fish1talk() end		-- 		"I never imagined that meeting aliens would look this way.",
		if timer >94 	and stepdone==21 then stepdone=22 Talkies.onAction() andwhatdidyou:play() 		fish2talk() end		-- 		"And what did you expect? That they will kidnap your sister?",
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction()	indeed:play()			fish1talk() end		-- 		"Indeed, this is the most embarrasing script I‘ve ever done, I‘m done with it.",
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction()	 whatpleasedont:play()	fish1talk() end		-- 		"What!? Please don‘t! I need you for my game!",
		if timer >110 	and stepdone==24 then stepdone=25 Talkies.onAction()	 sheisright:play()		fish1talk() end		-- 		"She is right, it is completely unbelieavable,",
		if timer >115 	and stepdone==25 then stepdone=26 Talkies.onAction()	 itcannot:play()		fish1talk() end		-- 		"it cannot be more ridiculous.",
		
		if timer >120 	and stepdone==26 then stepdone=27 Talkies.onAction()	Ipromise:play() 		fish1talk() end		-- 		"I promise I will improve the script, please don‘t go!",
		if timer >125 	and stepdone==27 then stepdone=28 Talkies.onAction()	 Illbeback:play()		fish1talk() end		-- 		"I‘ll be back with a better story, please don‘t leave this project,",
		if timer >130 	and stepdone==28 then stepdone=29 Talkies.onAction()	 						end		-- 		" we are almost done!",
		if timer >135 	and stepdone==29 then stepdone=30 Talkies.onAction()	 						end		-- 		"...",
		if timer >140 	and stepdone==30 then stepdone=31 Talkies.onAction()	 sohereitis:play()		fish1talk() end		-- 		"So here it is! Thank you so much for your patience, this is a real great story now!",
		if timer >145 	and stepdone==31 then stepdone=32 Talkies.onAction()	 westart:play()			fish1talk() end		-- 		"We start recording again! Scene 'Real propulsion', Take 2, action!",
		if timer >150 	and stepdone==32 then stepdone=33 Talkies.onAction()	 sothatshow:play()		fish1talk() end		-- 		"So that’s how it is!",
		if timer >155 	and stepdone==33 then stepdone=34 Talkies.onAction()	 ohno:play()			fish2talk() end		-- 		"Oh, no!",
		if timer >160 	and stepdone==34 then stepdone=35 Talkies.onAction()	 biologicalpower:play()	fish1talk() end		-- 		"Biological power. We should have thought about it earlier. We had every hint.",
		if timer >165 	and stepdone==35 then stepdone=36 Talkies.onAction()	 andwhatwas:play()		fish2talk() end		-- 		"And what was that nuclear power plant for?",
		if timer >170 	and stepdone==36 then stepdone=37 Talkies.onAction()	 cantyousee:play()		fish1talk() end		-- 		"Can’t you see? It powers that washing machine over there.",
		if timer >175 	and stepdone==37 then stepdone=38 Talkies.onAction()	 						end		-- 		"what‘s in the washing machine drum? I can see something spinning",
		if timer >180 	and stepdone==38 then stepdone=39 Talkies.onAction()	 itsaportal:play()		fish1talk() end		-- 		"It is a portal that leads to their dimension, they can travel to their home from that drum",
		if timer >185 	and stepdone==39 then stepdone=40 Talkies.onAction()	 nobodyis:play()		fish1talk() end		-- 		"Nobody is going to believe us.",
		if timer >190 	and stepdone==40 then stepdone=41 Talkies.onAction()	 maybeyes:play()		fish2talk() end		-- 		"Maybe yes. FDTO works on a project to make use of bioenergy.",
		if timer >195 	and stepdone==41 then stepdone=42 Talkies.onAction()	 wehave:play()			fish1talk() end		-- 		"We have to get out this device.",
		if timer >200 	and stepdone==42 then stepdone=43 Talkies.onAction()	 ifcanseeit:play()		fish2talk() end		-- 		"If I can see it correctly, it is composed of six parts.",
		if timer >205 	and stepdone==43 then stepdone=44 Talkies.onAction()	 thecagein:play()		fish1talk() end		-- 		"The cage in the middle is probably the most important.",
		if timer >210 	and stepdone==44 then stepdone=45 Talkies.onAction()	 okay:play() 			fish2talk() end		-- 		"Okay, we shall take this one.",
		if timer >215 	and stepdone==45 then stepdone=46 Talkies.onAction()	 look:play()			fish1talk() end		-- 		"Look, there is some strange life form, over there. Shouldn’t we try to contact them?",
		if timer >220 	and stepdone==46 then stepdone=47 Talkies.onAction()	 ourmission:play()		fish2talk() end		-- 		"Our mission was clear - principles and construction of the interstellar propulsion. Not a single word about contact.",
		if timer >225 	and stepdone==47 then stepdone=48 Talkies.onAction()	 well:play()			fish1talk() end		-- 		"Well. But they could at least help us.",
		if timer >230 	and stepdone==48 then stepdone=49 Talkies.onAction()	 goodafternoon:play()	fish2talk() end		-- 		"Good afternoon, excuse me, please, couldn’t I loot your drive?",
		if timer >235 	and stepdone==49 then stepdone=50 Talkies.onAction()	 goodafternoon2:play()	fish2talk() end		-- 		"Hmm, good afternoon sir, could I ask you to help us to dismantle your propulsion unit, please?",
		if timer >240 	and stepdone==50 then stepdone=51 Talkies.onAction()	 ourgoal:play()			fish1talk() end		-- 		"Our goal in this level is to get out the cage with that animal.",
		if timer >245 	and stepdone==51 then stepdone=52 Talkies.onAction()	 ihaveafeeling:play()	fish2talk() end		-- 		"I have a feeling there was an argument between these two.",
		if timer >250 	and stepdone==52 then stepdone=53 Talkies.onAction()	 inever:play()			fish1talk() end		-- 		"I never imagined that meeting aliens would look this way.",
		if timer >255 	and stepdone==53 then stepdone=54 Talkies.onAction()	 andwhatdidyou:play()	fish2talk() end		-- 		"And what did you expect? That they will kidnap your sister?",
		
end

function subtitle58end()
		if timer >0 	and stepdone==0 then stepdone=1 					squirrel:play()			fish1talk() end		--	Squirrel,
		if timer >2		and stepdone==1 then stepdone=2 Talkies.onAction()							end		--	this is a notion that will be effective today widely talked about in the next few years in connection with interspace propulsion.
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction() thisisalso:play()		fish1talk() end		--  This is also possible reason of UFO visits of the Earth.
		if timer >17 	and stepdone==3 then stepdone=4 Talkies.onAction()	yes:play()				fish1talk() end		-- 	Yes, a few cases of squirrel kidnaps were reported in past 5 years, 
		if timer >25	and stepdone==4 then stepdone=5 Talkies.onAction() specially:play()			fish1talk() end		-- 	specially from Arizona, Utah and Southern Moravia,
		if timer >30 	and stepdone==5 then stepdone=6 Talkies.onAction() butnoone:play()			fish1talk() end		--  but no one really took them seriously. Now we know the whole appalling truth.
		if timer >34 	and stepdone==6 then stepdone=7 Talkies.onAction()	nowweknow:play()		fish1talk() end		--  Technical description is included."
--		if timer >32 	and stepdone==6 then stepdone=7 Talkies.onAction()	:play()			end		--
		
		
end

function subtitle59skull()
		if timer >0 	and stepdone==0 then stepdone=1  								end		-- 	"Can you all mind your own business? We actually enjoy sleeping."
		if timer >8 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
end

function subtitle59skull2()
		if timer >0 	and stepdone==0 then stepdone=1  								end		-- 	"Well another night of restless sleep due to curious people"
		if timer >8 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
end

function subtitle59skull3()
		if timer >0 	and stepdone==0 then stepdone=1  								end		-- 	"Well I was asleep, until I was so rudely awakened."
		if timer >8 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
end

function subtitle59skull4()
		if timer >0 	and stepdone==0 then stepdone=1  								end		-- 	"Another night of peaceful sleep, ruined."
		if timer >8 	and stepdone==1 then stepdone=2 Talkies.onAction()	 			end		-- 	
end

function subtitle59()

		--level 59	

		if timer >0 	and stepdone==0 then stepdone=1 				 		canyousee:play()	fish2talk() end		--1  "Can you see that seahorse?",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		itisblocked:play()	fish1talk() end		--2	"It is blocked by amphoras.",
		if timer >8 	and stepdone==2 then stepdone=3 Talkies.onAction()		itgot:play()		fish1talk() end		--3	"It got itself drunk among the amphoras.",
		if timer >10		and stepdone==3 then stepdone=4 Talkies.onAction() 		iwonder:play()	fish2talk() end		--4	"I wonder if there is anything left in them.",
		if timer >15 	and stepdone==4 then stepdone=5 Talkies.onAction() 		youprobably:play()	fish1talk() end		--5	"You probably have to go check for yourself.",
		if timer >20 	and stepdone==5 then stepdone=6 Talkies.onAction()		finally:play()		fish2talk() end		--6 	"Finally, I can see some new kind of skull.",
		if timer >25 	and stepdone==6 then stepdone=7 Talkies.onAction()		didyounoticed:play() fish1talk() end		--7 	"Did you notice that totem? It is the Mexican god Shelloxuatl.",
		if timer >30 	and stepdone==7 then stepdone=8 Talkies.onAction() 		itlookslike:play()	fish2talk() end		--8	"It looks like it.",
		if timer >35 	and stepdone==8 then stepdone=9 Talkies.onAction() 										end		--9	"These amphores fall unbearably slow.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	well:play()			fish1talk() end		--10	"Well, you are not a bear, after all.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	dontforget:play()	fish1talk() end		--11 	"Don’t forget that we are under water.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() 	theauthors:play()	fish2talk() end		--12	"Yuck. The authors could have spared us that animation.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	thistotem:play()	fish1talk() end		--13	"This totem looks good to me.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	thatskull:play()	fish1talk() end		--14 	"That skull seems to radiate something strange.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	isitalive:play()	fish2talk() end		--15 	"Is it alive or is it some kind of spell?",
		
end

function subtitle60()

		--level 60	

		if timer >0 	and stepdone==0 then stepdone=1 				 	pleasenote:play()		fish1talk() end		--1  "Please note, that this level contains one hundred and nine objects.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	andallofthem:play()		fish1talk() end		--2	"And all of them gleam beautifully.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	pleasenote2:play()		fish2talk() end		--3	"Please note, that I can’t move a fin here.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	itsbeautiful:play()		fish2talk() end		--4	"It’s beautiful! Everything gleams so much...",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	itwould:play()			fish1talk() end		--5	"It’ll be much nicer when we get out.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	thisis:play()			fish1talk() end		--6 	"This is very dangerous cave-in. We are lucky to be alive.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	dontspeak:play()		fish2talk() end		--7 	"Don’t speak too soon. We’re still in here.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	dontyouthink:play()		fish2talk() end		--8	"Don’t you think that these stones are arranged in a different way?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 	youareright:play()		fish1talk() end		--9	"You are right. That green one was surely over there!",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	ithink:play()			fish1talk() end		--10	"I think that these jewels arrange themselves in a different way after each restart.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() yeahyouareright:play()	fish1talk() end		--11 	"Yeah, you’re right. Maybe they form some sensible pattern sometimes.",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() look:play()			fish1talk() end		--12	"Look, our player obviously satiated his need for colors.",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() maybe:play()			fish1talk() end		--13	"Maybe he could try to save us instead now.",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() 						end		-- 	
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() 						end		-- 	
		
end

function subtitle61()

		--level 61 Giant's chest

		if timer >0 	and stepdone==0 then stepdone=1 				 	wefinally:play()			fish1talk() end		-- 		"So we have finally found it.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	what:play()					fish2talk() end		-- 		"What?",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	captainssilver:play()		fish1talk() end		-- 		"Captain Silver’s treasure, of course.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() theholygrail:play()			fish1talk() end		-- 		"The Holy Grail, of course.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() itsneatly:play()				fish1talk() end		-- 		"It’s neatly in one place: the treasure and the Grail.",
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()	ithink:play()				fish2talk() end		-- 		"I think it’s much too easy to be true.",
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()	ithinkthat:play()			fish2talk() end		-- 		"I think that if it really was our goal, the game would let the player know.",
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() butihave:play()				fish1talk() end		-- 		"But I have let them know. That could be enough.",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()	butihavecasted :play()		fish2talk() end		-- 		"But I have casted doubts on it. This could confuse them.",
		if timer >35 	and stepdone==9 then stepdone=10 Talkies.onAction() wellallright:play() 		fish1talk() end		-- 		"Well, all right. PROBABLY this is not what we are looking for.",
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() thatsbetter:play()			fish2talk() end		-- 		"That’s better. There remains a bit of uncertainty.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 	couldntwe:play()		fish2talk() end		-- 		"Couldn’t we close that chest?",
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() whatifwe:play()			fish2talk() end		-- 		"What if we try to close that chest?",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() itwouldbe:play()			fish1talk() end		-- 		"It would be much easier with the chest closed.",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() trytopush:play()			fish1talk() end		-- 		"Try to push its lid.",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() wontwe:play()				fish2talk() end		-- 		"Won’t we take something with us?",
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() whatabout:play()			fish2talk() end		-- 		"What about taking some treasure with us?",
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() arentyousorry:play()		fish2talk() end		-- 		"Aren’t you sorry to leave all this treasure behind?",
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() wewillbeglad:play()		fish1talk() end		-- 		"We will be glad if we manage to get out ourselves.",
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() dontforget:play()			fish1talk() end		-- 		"Don’t forget our mission.",
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() thetreasurewould:play()	fish1talk() end		-- 		"The treasure would only hinder us.",
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction() allthisglinting:play()		fish1talk() end		-- 		"All this glinting is a sight for the eyes."
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction() everything:play()			fish2talk() end		-- 		"Everything here gleams so. It makes me dizzy.",
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction() 							end		-- 		

end

function subtitle62()

		--level 62 

		if timer >0 	and stepdone==0 then stepdone=1 				 	fromthe:play()				fish2talk() end		-- 		"From the treasury to the candlestick warehouse. Now, that is what I call progress.",
		if timer >8 	and stepdone==1 then stepdone=2 Talkies.onAction()	youhavetolook:play() 		fish1talk() end		-- 		"You have to look on the bright side. There is nothing that looks like a grail here.",
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction()	itseemsthat:play()			fish1talk() end		-- 		"It seems that I’ll have to go through that horrible maze.",
		if timer >17	and stepdone==3 then stepdone=4 Talkies.onAction() 	yourfatbody:play()			fish2talk() end		-- 		"Your fat body. It always hinders us.",
		if timer >22 	and stepdone==4 then stepdone=5 Talkies.onAction() justleavemealone:play()		fish1talk() end		-- 		"Just leave me alone, you emaciated pollywog.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	cutthecrap:play()			fish2talk() end		-- 		"Cut the crap, you misshaped heap of muscles and scales, will you?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		ifeellike:play()		fish2talk() end		-- 		"I feel like I am in the pottery warehouse.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 		youmean:play()			fish1talk() end		-- 		"You mean, amphory warehouse.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	noitshould:play()		fish2talk() end		-- 		"No, it should be amphora warehouse.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	youknowwhat:play()		fish2talk() end		-- 		"You know what? I think that this mission really helped me.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() 	andwhy:play()			fish1talk() end		-- 		"And why?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	igrew:play()			fish2talk() end		-- 		"I grew to understand that jewels and gold are but cheap trinkets.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	icantbear:play()		fish2talk() end		-- 		"I can’t bear the darned stuff any longer!",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 							end		-- 		
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 							end		-- 		

end

function subtitle63()

		--level 62 

		if timer >0 	and stepdone==0 then stepdone=1 				 	thatbat:play()					fish2talk() end		-- 		"That bat must be terribly strong.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction()	wellyes:play() 					fish1talk() end		-- 		"Well, yes.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	thatredcreature:play()			fish2talk() end		-- 		"That red creature is a little bit strange.",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 	thatmaybetrue:play()		fish1talk() end		-- 		"That may be true, but I think we are going to need it.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() itstoonarrow:play()				fish1talk() end		-- 		"It’s too narrow for me. You have to do it on your own down there.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()		thatwhitefish:play()		fish2talk() end		-- 		"That white fish is a terrible obstacle.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		fish:play()					fish1talk() end		-- 		"Fish? I thought it was only a stone.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 		ithink:play()				fish1talk() end		-- 		"I think that we should be looking for the Grail somewhere else...",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	 	dontbedepressive:play()		fish2talk() end		-- 		"Don’t be depressive. We will solve this in no time.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() 	imnotsosure:play() 			fish1talk() end		-- 		"I am not so sure.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() 	seeyouare:play()			fish2talk() end		-- 		"See? You are here.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	whatkind:play()				fish2talk() end		-- 		"What kind of color-shifting monster is this?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	thisis:play()				fish2talk() end		-- 		"This is an abomination.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	thisisan:play()				fish2talk() end		-- 		"This is an offense of Nature.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	itscalledflashy:play()		fish1talk() end		-- 		"It is called the Flashy hammerhead.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 	itscalledglossy:play()		fish1talk() end		-- 		"It is called the Glossy repulsor.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() 	itscalledpolitician:play()	fish1talk() end		-- 		"It is called the Politician fish.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() 	thisbat:play()				fish1talk() end		-- 		"This bat is kind of strange.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() thisisveryphlegmatic:play()	fish1talk() end		-- 		"This is very phlegmatic bat.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() 	thisisa:play()				fish2talk() end		-- 		"This is a stuffed bat.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() itsasculpture:play()			fish1talk() end		-- 		"It’s a sculpture of bat.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 		itsastalagmite:play()	fish2talk() end		-- 		"It’s a stalagmite shaped as a bat.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() itsaplainstalagmite:play()		fish1talk() end		-- 		"It’s a plain stalagmite.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 	itsjustan:play()			fish2talk() end		-- 		"It’s just an ordinary piece of rock.",
		if timer >120	and stepdone==24 then stepdone=25 Talkies.onAction() ithinkyouare:play()			fish1talk() end		-- 		"I think you are going to need that ‘monster’.",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() ithink:play()					fish1talk() end		-- 		"I think you will have to overcome your dislike of that ‘abomination’.",

end

function subtitle64()

		--level 64	

		if timer >0 	and stepdone==0 then stepdone=1 				 		thatthing:play()		fish2talk() end		--  "That thing over there must be Holy Grail!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		howcanyou:play()		fish1talk() end		--	"How can you be so sure?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()		cantyousee:play()		fish2talk() end		--	"Can’t you see that halo?",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 		cantyouseehow:play()	fish2talk() end		--	"Can’t you see how it glows among the others?",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 		cantyouseehow2:play()	fish2talk() end		--	"Can’t you see how conspicuously is it placed in the center of the cave?",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()		thehalomeans:play()		fish1talk() end		-- 	"The halo means nothing. The Holy Grail could look quite plain.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		allthatglitters:play()	fish1talk() end		-- 	"All that glitters is not gold. No one ever said that Holy Grail must glow.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 		hmm:play()				fish1talk() end		--	"Hmm... Placing a plain Grail in a conspicuous place will not turn it into a Holy one.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 		ifeel:play()			fish1talk() end		--	"I feel it will not be as it seems.",
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction()	 		youandyour:play()	fish2talk() end		--	"You and your feelings.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction() 		letsgo:play()		fish2talk() end		-- 	"Let’s go, we shall take that central glowing Grail with us.",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() 		hmm:play()			fish1talk() end		--	"I’d rather take all of them.",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() 	idrather:play()			fish2talk() end		--	"Hmm... It seems you were right. We’ll take them all.",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() 		ourgoal:play()		fish2talk() end		-- 	"Our goal in this level is to push out the Holy Grail.",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() 	notopush:play()			fish1talk() end		-- 	"No, to push out all the Grails.",
		if timer >75 	and stepdone==14 then stepdone=15 Talkies.onAction() 		theholy:play()		fish2talk() end		-- 	"The Holy one is enough.",
		if timer >80 	and stepdone==15 then stepdone=16 Talkies.onAction() 		ourgoal2:play()		fish2talk() end		-- 	"Our goal is to push all the Grails out of this room.",
		if timer >85 	and stepdone==16 then stepdone=17 Talkies.onAction() 	almostall:play()		fish1talk() end		-- 	"Almost all of them are gone, now!",
		if timer >90 	and stepdone==17 then stepdone=18 Talkies.onAction() 	onemoregrail:play()		fish1talk() end		-- 	"One more Grail and we are done!",
		
		
end

function subtitle64end()
		--level 64	end

		if timer >0 	and stepdone==0 then stepdone=1 				 								end		--  "We recommend subject the enclosed material to thorough tests of holiness.",
		if timer >1 	and stepdone==1 then stepdone=2 Talkies.onAction() 		werecommend:play()		end		--	"Due to the lack of equipment we performed only the basic tests and the results are more than promising.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()		duetothelack:play()		end		--	"Bombing some of the grails with ultradirty words, we measured sometimes even 3 Santa Clauses,",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 		bombing:play()	 		end		--	"some of them withstanded the circumstances pressure of 0.8 jobs for the period of one minute!",
		if timer >23 	and stepdone==4 then stepdone=5 Talkies.onAction() 		someofthem:play()	 	end		--	"It is also possible that we have encountered the whole Holy Service.",
		if timer >32 	and stepdone==5 then stepdone=6 Talkies.onAction()		itisalsopossible:play()							 end		-- 	
	
end

function subtitle65()

		--level 61 Tetris
		
		if timer >0 	and stepdone==0 then stepdone=1 				 	sothisis:play()				end		--	"So this is what the most successful game of all time originally looked like.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() onlydeeprespect:play()		end		--	"Only deep respect for its memory prevents me from saying what it looks like...",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	buteverybody:play()			end		--	"But everybody else can see for themselves.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() doyouknow:play()				end		--	"Do you know what I have in mind?",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	well:play()					end		--	"Well?",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()								end		--	"I think we could do a better tetris than this room.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	whatdoyoumean:play()		end		--	"What do you mean, better?",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() atleast:play()				end		--	"At least, you won’t have to move all the pieces to the right."
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	 thetruthis:play()			end		--	"The truth is we have an ample supply of suitable objects here.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() ithinkalittle:play() 		end		--	"I think a little patch of code could give the player the possibility to enjoy some original tetris.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() okaytrytoprogram:play()	end		--	"Okay, try to program it!",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() wewillhave:play() 			end		--	"Be careful not to hurt us.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() becareful:play()			end		--	"We will have to stack these pieces more efficiently, perhaps.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() hmm:play()					end		--	"Hmm... I should have arranged it better.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 							end		--

end

function subtitle66()

		--level 66 Emulator
		
		if timer >0 	and stepdone==0 then stepdone=1 				 	sothisisalevel:play()		end		--	"So this is a level for real connoisseurs.",	
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() yeah:play()					end		--	"Yeah, I feel like I can feel the eight bit wind of history.",
		if timer >9		and stepdone==2 then stepdone=3 transform:play()								end
		if timer >10 	and stepdone==3 then stepdone=4 Talkies.onAction()	canyouseethat:play()		end		--	"Can you see that pixel graphic all around us? I must confess I feel nostalgic.",
		if timer >18	and stepdone==4 then stepdone=5 Talkies.onAction() thisisanopportunity:play()	end		--	"This is an opportunity to play the merry old games of ZX Spectrum.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction() 	whatareyouthinking:play()	end		--	"What are you thinking about now?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	wellijust:play()			end		--	"Well, I just realized that the animations of us two alone would fill up the RAM of a ZX Spectrum.",
		if timer >40 	and stepdone==7 then stepdone=8 Talkies.onAction()	doyouknow:play()			end		--	"Do you know that your question alone in WAV format would overfill the RAM of ZX Spectrum?
		if timer >45 	and stepdone==8 then stepdone=9 Talkies.onAction() andimagine:play()			end		--	"And imagine how many such sounds are in this game.",
		if timer >50 	and stepdone==9 then stepdone=10 Talkies.onAction()	 canyouseethatknight:play()	end		--	"Can you see that Knight Lorer?",
		if timer >55 	and stepdone==10 then stepdone=11 Talkies.onAction() andthatmustbe:play()		end		--	"And that must be the Manic Miner.",
		if timer >60 	and stepdone==11 then stepdone=12 Talkies.onAction() ireallydidntknow:play()	end		--	"I really didn’t know I’d ever be putting the Jetpack together again.",
		if timer >65 	and stepdone==12 then stepdone=13 Talkies.onAction() thislineof:play() 			end		--	"This line of robots looks familiar.",
		if timer >70 	and stepdone==13 then stepdone=14 Talkies.onAction() ohsurethey:play()			end		--	"Oh sure, they are from the Highway Encounter.",
		if timer >75 	and stepdone==14 then stepdone=15 Talkies.onAction() iwonder:play()				end		--	"I wonder what is loading right now.",
		if timer >80 	and stepdone==15 then stepdone=16 Talkies.onAction() thesinglebiggest:play()	end		--	"The single biggest advantage of this room is that it contains no steel.",

end

function subtitle67()

		--level 66 Emulator
		
		if timer >0 	and stepdone==0 then stepdone=1 				 	iamimprisoned:play()		fish2talk() end		--	"I am imprisoned under the castle.",	
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	howdidthesteel:play()		fish2talk() end		--	"How did the steel get into this castle?",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	holdon:play()				fish1talk() end		--	"Hold on, I’ll bring some peasants to help you.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 	thesearenotpeasants:play()	fish2talk() end		--	"These are not peasants but peons.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() doyouthink:play()			fish1talk() end		--	Do you think that these mines contain captain Silver’s treasure?",	
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()	iconsiderthis:play()		fish2talk() end		--	"I consider this highly unlikely.",
		if timer >17 	and stepdone==6 then stepdone=7 Talkies.onAction()	wheredidweget:play()		fish2talk() end		--	"Where did we get us, again?",
		if timer >19 	and stepdone==7 then stepdone=8 Talkies.onAction() ifindit:play()				fish1talk() end		--	"I find it oddly familiar here."
		if timer >24 	and stepdone==8 then stepdone=9 Talkies.onAction()	 whenms:play()				fish1talk() end		--	"When MS Word or another boring application is running on this machine, we, the computer game characters, get together in the C:\\WINDOWS\\CONFIG directory and talk.",
		if timer >28 	and stepdone==9 then stepdone=10 Talkies.onAction() whenwedontknow:play()		fish2talk() end		--	"When we don’t know what to do, we play hide-and-seek in the registry. That’s why your Windows sometimes crashes if you don’t play any games for a long period of time.",
		if timer >31 	and stepdone==10 then stepdone=11 Talkies.onAction() 							fish1talk() end		--	"You shouldn’t give that away. I only wanted to say that it looks very similar to this level. So many computer game characters.",	
		if timer >39 	and stepdone==11 then stepdone=12 Talkies.onAction() 	butitsnotso:play()		fish2talk() end		--	"But it’s not so difficult to get out of there.",	
		if timer >46 	and stepdone==12 then stepdone=13 Talkies.onAction() themakers:play()			fish1talk() end		--	"The makers of this game would like to express their admiration for the Blizzard Entertainment company and wish them many successful games.",
		if timer >50 	and stepdone==13 then stepdone=14 Talkies.onAction() 	andtheywould:play()		fish2talk() end		--	"And they would also like to thank them for so many hours of neatly killed time.",	
		if timer >54 	and stepdone==14 then stepdone=15 Talkies.onAction() 							end		--	

end

function subtitle68()

		--level 68

		if timer >0 	and stepdone==0 then stepdone=1 				 	thisishow:play()			fish1talk() end		-- 		"This is how it looks when you don’t keep your desktop tidy.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	wouldntithelp:play()		fish2talk() end		-- 		"Wouldn’t it help us if we select Desktop/Line up Icons?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	whatifwetryto:play()		fish2talk() end		-- 		"What if we try to minimize the biggest windows?",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 								fish2talk() end		-- 		"What if we try to close some of these windows?",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	whataboutshutting:play()	fish2talk() end		-- 		"What about shutting down the whole thing and switching to command prompt only?",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	whataboutgiving:play()		fish2talk() end		-- 		"What about giving up altogether and swimming to the pub instead?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	didyoucount:play()			fish1talk() end		-- 		"Did you count these windows around us?",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	idontneedtocount:play()		fish2talk() end		-- 		"I don’t need to count them. There are ninety five of them.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	canyouseethat:play()		fish2talk() end		-- 		"Can you see that Notepad? Now is a good time to send a message to the player!",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()  stop:play()				fish1talk() end		-- 		"Stop that folly and try to think instead.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() thissystemhas:play()		fish2talk() end		-- 		"This system has to have a hole somewhere.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() sure:play()				fish1talk() end		-- 		"Sure. It’s in the lower right corner.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() canyouseethatbig:play()	fish2talk() end		-- 		"Can you see that big window on the right?!",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() ohno:play()				fish1talk() end		-- 		"Oh, no! That must be the original version of this game.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() itranonthe:play()			fish2talk() end		-- 		"It ran on the antediluvial machines, in standard VGA resolution...",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() noanimation:play()			fish1talk() end		-- 		"No animation...",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() notalking:play()			fish2talk() end		-- 		"No talking...",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() nomusic:play()				fish1talk() end		-- 		"No music...",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() onlyabeep:play()			fish2talk() end		-- 		"Only a beep from time to time...",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() butfortunately:play()		fish1talk() end		-- 		"But fortunately the authors got back to it and gave it this modern facelift.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() butidloketoplayit:play()	fish2talk() end		-- 		"But I’d like to play it sometime, anyway!",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() icantmovethis:play()		fish2talk() end		-- 		"I can’t move this window. Down there, it’s a steel cylinder, though it’s only in sixteen color VGA.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() waita:play()				fish1talk() end		-- 		"Wait a moment, player. We have to make something clear. These two fish, they are our younger selves.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() youareright:play()			fish2talk() end		-- 		"You are right. They are trapped there, poor souls.",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() wecant:play()				fish1talk() end		-- 		"We can’t leave them there. We have to get them out!",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() yesbutttheplayer:play()	fish2talk() end		-- 		"Yes, but the player has to get us out.",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() sowellgo:play()			fish1talk() end		-- 		"So we’ll go on strike. Personally, I fully sympathize with the bigger fish inside.",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() youareright:play()			fish2talk() end		-- 		"You’re right. I can imagine what the smaller one feels.",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() wearenot:play()			fish1talk() end		-- 		"We are not going to play, until you save those two, player.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() goodness:play()			fish1talk() end		-- 		"Goodness, that is a realistic game!",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() ohmyhetook:play()			fish2talk() end		-- 		"Oh my, he took the game too seriously!",
		if timer >155 	and stepdone==31 then stepdone=32 Talkies.onAction() 							end		-- 		

end

function subtitle69()

		--level 69

		if timer >0 	and stepdone==0 then stepdone=1 				 	imcompletely:play()		fish1talk() end		-- 		"I’m completely blocked in here.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whatareyoudoing:play()	fish2talk() end		-- 		"What are you doing up there?",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	theremustbe:play()		fish1talk() end		-- 		"These must be some hardware keys.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 	thereare:play()			fish1talk() end		-- 		"There are many hardware keys here.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 	whatdo:play()			fish1talk() end		-- 		"What do you have down there?",
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()	whatsdown:play()		fish1talk() end		-- 		"What’s down there?",
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()	therearesomeprinted:play()	fish2talk() end		-- 		"There are some printed circuits here.",
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() 	therearesomeintegrated:play() fish2talk() end		-- 		"There are some integrated circuits here.",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()	iamsurroundedby:play()	fish2talk() end		-- 		"I am surrounded by electronics here.",
		if timer >35 	and stepdone==9 then stepdone=10 Talkies.onAction()  computerstheyve:play()	fish2talk() end		-- 		"Computers. They’ve always fascinated me. But now I’m sick of them.",
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() 						end		-- 		"Fish. I’ve always hated them. And now my circuits are overloaded with them.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() hey:play()				fish1talk() end		-- 		"Hey, how are you? It’s quite boring here.",
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() canitwillyou:play()	fish2talk() end		-- 		"Can it, will you?",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() getout:play()			fish2talk() end		-- 		"Get out!",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() wehave:play()			fish1talk() end		-- 		"We have to put these keys somewhere.",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() itdoesntseem:play()	fish2talk() end		-- 		"It doesn’t seem to fit.",
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 						end		-- 		
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 						end		-- 		
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() 						end		-- 		
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() 						end		--
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() 						end		-- 		
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction() 						end		-- 		
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction() 						end		-- 		
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction() 						end		-- 		
		if timer >110 	and stepdone==24 then stepdone=25 Talkies.onAction() 						end		-- 		
		if timer >115 	and stepdone==25 then stepdone=26 Talkies.onAction() 						end		-- 		
		if timer >120 	and stepdone==26 then stepdone=27 Talkies.onAction() 						end		-- 		
		if timer >125 	and stepdone==27 then stepdone=28 Talkies.onAction() 						end		-- 		
		if timer >130 	and stepdone==28 then stepdone=29 Talkies.onAction() 						end		-- 	
		if timer >135 	and stepdone==29 then stepdone=30 Talkies.onAction() 						end		-- 		
		if timer >140 	and stepdone==30 then stepdone=31 Talkies.onAction() 						end		-- 		
		if timer >145 	and stepdone==31 then stepdone=32 Talkies.onAction() 						end		-- 		

end

function subtitle70()

		--level 70

		if timer >0 	and stepdone==0 then stepdone=1 				 		itlooks:play()			fish1talk() end		-- 		"It looks as if we finally got there.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		doyouthinkthisisit:play()	fish2talk() end		-- 		"Do you think this is it?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()		howcoulditall:play()	fish2talk() end		-- 		"How could it all fit on one three and a half inch floppy?",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 		lookhow:play()			fish1talk() end		-- 		"Look how much bigger than me it is. It is a three and a half meter floppy.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 		thatsbetter:play()		fish2talk() end		-- 		"That’s better. Big plan, big disk. That makes sense.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()		doyouthinkwe:play()		fish2talk() end		-- 		"Do you think we can lift it up?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		itwillbe:play()			fish1talk() end		-- 		"It will be difficult.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 		idontthink:play()		fish1talk() end		-- 		"I don’t think it can be done. Why would they be hiding it here, if we could lift it?",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()		shutup:play()			fish1talk() end		-- 		"Shut up, or I will break off your pins!",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()  	canyousee:play()		fish1talk() end		-- 		"Can you see that steel cross? This is the grave of our hopes.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() 	dontbealways:play()		fish2talk() end		-- 		"Don’t be always so gloomy. I can’t bear it.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	wouldntitbe:play()		fish2talk() end		-- 		"Wouldn’t it be enough to download that plan from the floppy to our notebook?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	whynot:play()			fish1talk() end		-- 		"Why not. Take it out.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	ohdarn:play()			fish2talk() end		-- 		"Oh, darn! I left it at home.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	shallwe:play()			fish1talk() end		-- 		"Shall we return for it?",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 	never:play()			fish1talk() end		-- 		"Never. It may seem funny to you, but my fins are sore.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() 	suit:play()				fish1talk() end		-- 		"Suit yourself. I am curious how are you going to do it now.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() 	whatifthereis:play()	fish2talk() end		-- 		"What if there is no plan on this floppy? What if it is here only to confound us?",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() 	idont:play()			fish1talk() end		-- 		"I don’t believe that. Is there some other object that would be more difficult to get out?",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() 	whatkindoflittle:play()	fish2talk() end		--		"What kind of little creatures are those?",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() 	thesemust:play()		fish1talk() end		-- 		"These must be computer viruses.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	yucksome:play()			fish2talk() end	-- 		"Yuck. Some monsters.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() 	aboveall:play()			fish1talk() end		-- 		"Above all, we must be careful not to take them with the disk. The player wouldn’t be happy if we pour them on their desktop.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 	ourgoalistopush:play()	fish2talk() end		-- 		"Our goal is to push out that disk.",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() 							end		-- 		"You won’t move me easily!",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() 							end		-- 		"That was smart!",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() 							end		-- 		"Ooops!",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() 							end		-- 		"Darn!",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() 							end		-- 		"Oh.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() 							end		-- 		"Ouch.",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() 							end		-- 		"Yup.",
		if timer >155 	and stepdone==31 then stepdone=32 Talkies.onAction() 							end		-- 		"Leave me alone!",
		if timer >160 	and stepdone==32 then stepdone=33 Talkies.onAction() 							end		-- 		"1060D0A2A050B508C34D590923763F6B5B466C0E6033A9",
		if timer >165 	and stepdone==33 then stepdone=34 Talkies.onAction() 							end		-- 		"033455D394F938E624A68CBCA301923784F35892",
		if timer >170 	and stepdone==34 then stepdone=35 Talkies.onAction() 							end		-- 		"1A566837C0323B0413239DD2374E9F0036C56B23074",


end

function subtitle70end()
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction() 		thegreatestearthquake:play()	end		-- 		"THE GREATEST EARTHQUAKE SINCE 1990"
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		whendragonslair:play()	 		end		-- 		"when Dragon’s Lair was issued, Rules for beginners, version 1.0."
		if timer >12 	and stepdone==2 then stepdone=3 Talkies.onAction()		dragonslairplus:play()	 		end		-- 		"DRAGON’S LAIR PLUS"
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 		theworldof:play()				end		-- 		"The world of role-playing games will soon change forever."
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 		dragonslairplus2:play()		 	end		-- 		"Dragon’s Lair Plus is not only a new version."
		if timer >24 	and stepdone==5 then stepdone=6 Talkies.onAction()		itsanewgame:play()		 		end		-- 		"It’s a new game."
		if timer >26 	and stepdone==6 then stepdone=7 Talkies.onAction()		plentyofnew:play()				end		-- 		"Plenty of new possibilities."
		if timer >28 	and stepdone==7 then stepdone=8 Talkies.onAction() 		newoccupations:play()			end		-- 		"New occupations."
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()		newsystemofcharacter:play()		end		-- 		"New system of character creation,"
		if timer >33 	and stepdone==9 then stepdone=10 Talkies.onAction()  	whereinstead:play()				end		-- 		"where, instead of your lives, your skills grow."
		if timer >37 	and stepdone==10 then stepdone=11 Talkies.onAction() 	newelegant:play()				end		-- 		"New elegant mechanism of playing."
		if timer >40 	and stepdone==11 then stepdone=12 Talkies.onAction() 	agamefor:play()					end		-- 		"A game for 21. century."
		if timer >45 	and stepdone==12 then stepdone=13 Talkies.onAction() 	gameshavechanged:play()			end		-- 		"GAMES HAVE CHANGED"
		if timer >48 	and stepdone==13 then stepdone=14 Talkies.onAction() 	thisiswhatwe:play()				end		-- 		"This is what we get from the computer after inserting the waterproof diskette that was found during our investigation of secret criminal organisation."
		if timer >65 	and stepdone==14 then stepdone=15 Talkies.onAction() 	weareschocked:play()			end		-- 		"We are shocked. Do not let journalists know it."
		if timer >68 	and stepdone==15 then stepdone=16 Talkies.onAction() 	youmustprevent:play()			end		-- 		"You must prevent the panic."
		if timer >75 	and stepdone==16 then stepdone=17 Talkies.onAction() 	andsubscribe:play()				end		-- 		"And subscribe two copies for us."
end
function subtitle71()

		--level 71

		if timer >0 	and stepdone==0 then stepdone=1 				 		weare:play()		fish1talk()				end		-- 		"We are locked in.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		wewillfind:play()	fish2talk()				end		-- 		"We will find a way out of here.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()		whyareyousosure:play()	fish1talk() 		end		-- 		"Why are you so sure about that?",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 		wevealways:play()	fish2talk()				end		-- 		"We’ve always found a way out.",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 		wehavecome:play()	fish2talk()				end		-- 		"We have come so far, we will not fail here.",
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()		thetopicof:play()	fish2talk()				end		-- 		"The topic of this branch is: ”UFO-Exit”, which means we will get out of here.",
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()		whatwould:play()	fish2talk()				end		-- 		"What would be the point of an unsolvable level?",
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() 		idontknow:play()	fish1talk() 			end		-- 		"I don’t know if that is a valid argument.",
		
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()			weshouldturn:play()	fish2talk()			end		-- 		"We should turn the electromagnet off.",
		if timer >35 	and stepdone==9 then stepdone=10 Talkies.onAction()  		howwould:play()	fish1talk()				end		-- 		"How would we do that. We are only able to move items."
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() 		soletsmove:play()	fish2talk()			end		-- 		"So let’s move the lever down there.",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 		youthink:play()	fish1talk() 			end		-- 		"You think the lever has any impact on the magnets?",
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() 		atleastitshould:play()	fish2talk()		end		-- 		"At least it should be connected to the magnetic field at the top right.",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() 		thatcouldbe:play()	fish1talk() 		end		-- 		"That could be.",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() 		perhapswehave:play()	fish2talk()		end		-- 		"Perhaps we have to find out what the point is of all these magnets in this level?",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() 		idont:play()	fish1talk() 			end		-- 		"I don’t think so. The brief said we should find out how the interstellar propulsion works.",
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 		whatifthemagnets:play()	fish2talk()		end		-- 		"What if the magnets are part of the interstellar propulsion?",
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 		thenweshould:play()	fish1talk() 		end		-- 		"Then we should figure that out. But in my opinion they are only here to be in our way.",	
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() 		lookalien:play()	fish2talk()			end		-- 		"Look, alien garbage.",
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() 		where:play()	fish1talk() 			end		--		"Where?",
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() 		wellthesmall:play()	fish2talk()			end		-- 		"Well, the small can down there.",
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction() 		perhaps:play()	fish1talk() 			end		-- 		"Perhaps it is still full.",
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction() 		itisan:play()	fish1talk() 			end		-- 		"It is an artefact and not garbage.",
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction() 		idontthink:play()	fish1talk() 		end		-- 		"I don’t think it is really a can. It’s probably a cloaked alien control unit.",
		if timer >110 	and stepdone==24 then stepdone=25 Talkies.onAction() 		anywayitisgarbage:play() fish2talk()	end		-- 		"Anyway, it is garbage to me and in the way.",
		if timer >115 	and stepdone==25 then stepdone=26 Talkies.onAction() 		lookatthecrystal:play()	fish1talk()		end		-- 		"Look at the crystal.",
		if timer >120 	and stepdone==26 then stepdone=27 Talkies.onAction() 		nicewhatmaterial:play()	fish2talk()		end		-- 		"Nice. What material do you think it is made from?",
		if timer >125 	and stepdone==27 then stepdone=28 Talkies.onAction() 		dilicium:play()	fish1talk()				end		-- 		"Dilicium."
		if timer >130 	and stepdone==28 then stepdone=29 Talkies.onAction() 		dihydrogenmonoxide:play() fish1talk()	end		-- 		"Dihydrogenmonoxide.",
		if timer >135 	and stepdone==29 then stepdone=30 Talkies.onAction() 		sodiumchloride:play() fish1talk()		end		-- 		"Sodium chloride.",
		if timer >140 	and stepdone==30 then stepdone=31 Talkies.onAction() 			nonferrous:play() fish1talk()		end		-- 		"Non-ferrous metal.",	
		if timer >145 	and stepdone==31 then stepdone=32 Talkies.onAction() 		glasswithcolored:play() fish1talk()		end		-- 		"Glass with colored paper glued on it.",
		if timer >150 	and stepdone==32 then stepdone=33 Talkies.onAction() 		analienalloy:play()	fish1talk()			end		-- 		"An alien alloy.",
		if timer >155 	and stepdone==33 then stepdone=34 Talkies.onAction() 		idontthinkso:play()	fish2talk()			end		-- 		"I don’t think so.",
		if timer >160 	and stepdone==34 then stepdone=35 Talkies.onAction() 		whydothemagnets:play()	fish1talk()		end		-- 		"Why do the magnets stay at fixed distances and don’t attract each other more strongly?"
		if timer >165 	and stepdone==35 then stepdone=36 Talkies.onAction() 		theyarealien:play()	fish2talk()			end		-- 		"They are alien magnets, they work differently.",
		if timer >170 	and stepdone==36 then stepdone=37 Talkies.onAction() 		isee:play()	fish1talk()					end		-- 		"I see.",
		if timer >175 	and stepdone==37 then stepdone=38 Talkies.onAction() 		anotherfuel:play()				fish1talk()	end		-- 		"Another fuel rod.",
		if timer >180 	and stepdone==38 then stepdone=39 Talkies.onAction() 		ihopeweare:play()	fish2talk()			end		-- 		"I hope we are far enough from the last level if the reactor explodes.",
		if timer >185 	and stepdone==39 then stepdone=40 Talkies.onAction() 		perhapsweshould:play()			fish1talk()	end		-- 		"Perhaps we shouldn’t have moved all fuel rods into the reactor.",
		if timer >190 	and stepdone==40 then stepdone=41 Talkies.onAction() 		thenwewouldstill:play()		fish2talk()	end		-- 		"Then we would still be stuck there."
		if timer >195 	and stepdone==41 then stepdone=42 Talkies.onAction() 		perhapsitwasnot:play()	fish2talk()		end		-- 		"Perhaps it was not a reactor.",
		if timer >200 	and stepdone==42 then stepdone=43 Talkies.onAction() 		whatisthatlonelything:play() fish2talk() end		-- 		"What is that lonely thing up there?",	
		if timer >205 	and stepdone==43 then stepdone=44 Talkies.onAction() 		anunidentified:play()			fish1talk()	end		-- 		"An unidentified alien artefact. Which one do you mean exactly?",
		if timer >210 	and stepdone==44 then stepdone=45 Talkies.onAction() 		theangularonewhich:play()	fish2talk()	end		-- 		"The angular one which is in our way, the one spanning half the level.",
		if timer >215 	and stepdone==45 then stepdone=46 Talkies.onAction() 		asisasiditis:play()			fish1talk()	end		-- 		"As I said, it is an unidentified alien artefact.",
		if timer >220 	and stepdone==46 then stepdone=47 Talkies.onAction() 		yesbutwhatis:play()		fish2talk()		end		-- 		"Yes. But what is its purpose?",
		if timer >225 	and stepdone==47 then stepdone=48 Talkies.onAction() 		ifiknewthat:play()				fish1talk()	end		-- 		"If I knew that, it wouldn’t be an unidentified alien artefact anymore but a plain alien artefact.",
		if timer >230 	and stepdone==48 then stepdone=49 Talkies.onAction() 												end		-- 		
					

				
					
end

function subtitle71standinginthealienmagnet()
					thatticles:play()
					thatprickles:play()
					thatishot:play()
end

function subtitle71shootingthelaser()
					oops:play()
					itwasntmyfault:play()
					illtrytoremember:play()
					whyarent:play()
end

function subtitle72()

		--level 72

		if timer >0 	and stepdone==0 then stepdone=1 				 		waitseethered:play()		fish1talk()	end		-- 		"Wait, see the red on the traffic light!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		thisisthebuilding:play() fish2talk() 	end		-- 		"This is the building of Fish Detective Training Organization.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction() 		ithink:play()			 fish1talk() 	end		-- 		"I think we can take it to pieces",
		if timer >15 	and stepdone==3 then stepdone=4 Talkies.onAction()		butourhomeholds:play()	fish2talk() 	end		-- 		"But our home holds together despite UFO crashed into it.",
		if timer >20	and stepdone==4 then stepdone=5 Talkies.onAction() 		sorryitisntagood:play()	fish2talk() 	end		-- 		"Sorry, it isn’t a good idea.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction() 		doyouseeit:play()		fish1talk()		end		-- 		"Do you see it?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		lookthegreen:play()		fish1talk()		end		-- 		"Look, the green light is started now.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction()								fishstop()		end		-- 		"FDTO - Nobody knows about us!",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction() 												end		-- 		"FDTO - We are the best!",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()		theseahorsewinks:play()	fish2talk() 	end		-- 		"The seahorse winks very nice.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction()  	noitdoesnt:play()		fish1talk()		end		-- 		"No it doesn’t.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	lookrightnow:play()		fish2talk() 	end		-- 		"Look... right now!",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	lookrightnow2:play()	fish2talk() 	end		-- 		"Look... right now!",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	lookrightnow3:play()	fish2talk() 	end		-- 		"Look... right now!",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	lookrightnow4:play()	fish2talk() 	end		-- 		"Look... right now!",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 	itwouldbebetter:play()	fish1talk()		end		-- 		"It would be better if it never existed.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() 	seeweareiconographic:play()	fish1talk()	end		-- 		"See, we are iconographic here and we are moving.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() 	yeswearetheir:play()	fish2talk() 	end		-- 		"Yes, we are their ablest agents...",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() 	underwater:play()		fish1talk()		end		-- 		"...underwater.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() 	youmaybeless:play()		fish2talk()		end		-- 		"You may be less able.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() 	whyareyoudrivelling:play()	fish1talk()	end		--		"Why are you drivelling?",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	youarentcomplete:play()	fish2talk() 	end		-- 		"You aren’t complete there.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() 	whatareyoudoing:play()	fish1talk()		end		-- 		"What are you doing whith these small balls when the antenna is here?",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 	whyarewe:play()			fish2talk() 	end		-- 		"Why we are destroying this building when it is clear than we have to put one ball to this horse?",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() 	whatthetraffic:play()	fish1talk()		end		-- 		"What? The traffic light is broken.",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() 							fishstop()		end		-- 		
		

end

function subtitle73()

		--level 73

		if timer >0 	and stepdone==0 then stepdone=1 				 	itisalreadythere:play() 	fish1talk()	end		-- 		"It is already there!",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	what:play()					fish2talk() end		-- 		"What?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	eitherigetout:play()		fish1talk()	end		-- 		"Either I get out or you!",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	andwhowill:play()			fish2talk() end		-- 		"And who will it be?",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	itshouldbeme:play()			fish1talk()	end		-- 		"It should be me,",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	buttheplayer:play()			fish1talk()	end		-- 		"but player decides it.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	whatareyou:play()			fish2talk() end		-- 		"What are you thinking of? You set the giant free and you leave me here. OK, he can move the steel, so what?",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	hewillbe:play()				fish2talk() end		-- 		"He will be wating for my help in the next room. Restart it, right now!",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	itisntfair:play()			fish1talk()	end		-- 		"It isn’t fair, my way was free. I only pushed to the steel the wrong way.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction() thesmallfish:play()			fish1talk()	end		-- 		"The small fish gets lost in the open ocean. Whithout me she won’t get far away. You should restart the room immediatelly.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() itisntwhat:play()			fish1talk()	end		-- 		"It isn’t what the doctor ordered.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() ishouldnot:play()			fish2talk() end		-- 		"I should not throw the smaller piece onto the bigger one.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() somebody:play()			fish1talk()	end		-- 		"Somebody could think this room is only a copy of one room from fish fillets 2.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() butinfact:play()			fish2talk() end		-- 		"But in fact the author was only inspired by the hardware problem room.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 							fish1talk()	end		-- 		"Why do I have to fold up all the pieces while a simple recursive program could do it?",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() howcantheplayer:play()		fish1talk()	end		-- 		"How can the player find a pleasure in this?",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() hemaylookforward:play()	fish2talk() end		-- 		"He may look foward for next rooms.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() hedoesntknow:play()		fish1talk()	end		-- 		"He doesn’t know, what they are like.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() possibly:play()			fish2talk() end		-- 		"Possibly somebody psyched him up.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() ohwell:play()				fish1talk()	end		--		"Oh well, player, if you don’t solve it, you are a big loser.",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() ofcourse:play()			fish2talk() end		-- 		"Of course, you are not entertained, when you almost don’t help me.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() yousaidit:play()			fish2talk() end		-- 		"You said it already three times.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() ifishouldsay:play()		fish1talk()	end		-- 		"If I should say always something different then we fill player’s disc up.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() maybenowadays:play()		fish2talk() end		-- 		"Maybe nowadays player’s disc can accommodate more data then in past.",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() ithinkwhatever:play()		fish1talk()	end		-- 		"I think what ever size the disc is, it’s always full.",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() beashamed:play()			fish2talk() end		-- 		"Be ashamed, cheater.",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() finallyipraise:play()		fish1talk()	end		-- 		"Finally I praise the player, that he solved it by the longer way.",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() 										end		-- 		
			

end

function subtitle74()

		--level 74

		if timer >0 	and stepdone==0 then stepdone=1 				 	itisahaunted:play()		fish2talk() end		-- 		"It is a haunted room.",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	ifearitwillbe:play()	fish1talk()	end		-- 		"I fear it will be dificult to move things over this hole.",
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()	hereyouare:play()		fish2talk() end		-- 		"Here you are, as you wish.",
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 							end		-- 		"Is anybody there?",
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 							end		-- 		"Who is there?",
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()							end		-- 		"I think, the receiver does not work.",
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()							end		-- 		"Put it back immediately, the indian headdress is mine.",
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() 							end		-- 		"Put it back immediately, it is mine!",
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()							end		-- 		"O O O O, I am indian. Put it back immediately!",
		if timer >35 	and stepdone==9 then stepdone=10 Talkies.onAction()  						end		-- 		"O O O O, put it back immediately!",
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() 						end		-- 		"Where are you pushing me? I am famous Vinnetou!",
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 	sorryweneed:play()	fish2talk() end		-- 		"Sorry, we need it necessarily.",
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() 	howcanitbeyours:play()	fish1talk()	end		-- 		"How can it be yours, if you are not indian?",
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() 	youcantbean:play()	fish2talk() end		-- 		"You can’t be an indian because you are not red.",
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() 	icantdemur:play()	fish1talk()		end		-- 		"I can’t demur anything.",
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() 	playerifyouare:play()	fish1talk()	end		-- 		"Player, if you are an indian, don’t perceive it as an discrimination please.",
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 	itmaybeamagic:play() fish2talk() end		-- 		"It may be a magic receiver.",
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 	tryit:play()		fish1talk()	end		-- 		"Try it.",
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() 	helloiwisht:play()	fish2talk() end		-- 		"Hello, I wish to disappear the steel near exit?",
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() 	putitback:play()	fish1talk()	end		--		"Put it back, it should be solved by player, not by ourself.",
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() 									end		-- 		
		
end

function subtitle75()

		--level 75

		if timer >0 	and stepdone==0 then stepdone=1 				 	looksomeone:play()		fish2talk()		end		-- 		"Look, someone’s built a lock in the stone.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	wheredoyouseealock:play() fish1talk()	end		-- 		"Where do you see a lock? I only see I can’t leave through the door.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	thereisalock:play()		fish2talk() 	end		-- 		"There is a lock here somewhere. The key is down there.",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 	icantleave:play()	fish1talk()		end		-- 		"I can’t leave anyway. Everything is blocked over there.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	thatswhywe:play()		fish2talk() 	end		-- 		"That’s why we need the key.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	couldyouplease:play()	fish1talk()		end		-- 		"Could you please get me the key, I would like to leave.",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	iwouldificould:play()	fish2talk() 	end		-- 		"I would if I could, but I think it’s stuck.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	lookabroom:play()		fish1talk()		end		-- 		"Look, a broom.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	idontwant:play()		fish2talk() 	end		-- 		"I don’t want to clean up here. It was bad enough in the boiler room.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()  whyareyou:play()		fish1talk()		end		-- 		"Why are you complaining? I cleared away all the heavy things there.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() withoutme:play()		fish2talk() 	end		-- 		"Without me, you would still be stuck there.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() wecouldjust:play()		fish2talk() 	end		-- 		"We could just swim out. Do you see how big the holes in the net are?",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() yestoosmall:play()		fish1talk()		end		-- 		"Yes, too small for me.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() ithinkweshould:play()	fish2talk() 	end		-- 		"I think we should really use the key to open the lock.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() thengogetit:play()		fish1talk()		end		-- 		"Then go get it, smarty-pants.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() benicetome:play()		fish2talk() 	end		-- 		"Be nice to me, or I might swim out alone.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() okiwontsay:play()		fish1talk()		end		-- 		"OK, I won’t say such things to you anymore.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() donteventhinkthem:play()	fish2talk() end		-- 		"Don’t even think them.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() hmmok:play()			fish1talk()		end		-- 		"Hmm... OK.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() doyouthink:play()		fish1talk()		end		--		"Do you think the key will get loose if we move it around a bit?",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() idontthinkso:play()	fish2talk() 	end		-- 		"I don’t think so, but we can try.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	wellneverget:play()	fish1talk()		end		-- 		"We’ll never get the key out. It’s been stuck there so long that it’s grown over with coral.",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() 	dontgiveup:play()	fish2talk() 	end		-- 		"Don’t give up.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 	couldtheicicle:play() fish1talk()	end		-- 		"Could the icicle over there help?",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() 	wecantgetitout:play()	fish2talk() end		-- 		"We can’t get it out anyway.",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() 	wejusthave:play()	fish1talk()		end		-- 		"We just have to apply enough force.",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() 	perhaps:play()		fish2talk() 	end		-- 		"Perhaps if you use the hammer?",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() 	ouch:play()			fish2talk() 	end		-- 		"Outch, that cactus has spikes.",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() 	ofcourseitsacactus:play() fish1talk()	end		--	 	"Of course, it’s a cactus.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() 	whatsitdoing:play()	fish2talk() 	end		-- 		"What’s it doing here?",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() 	outchthatcactus:play()	fish1talk()	end		-- 		"Outch, that cactus has spikes.",
		if timer >155 	and stepdone==31 then stepdone=32 Talkies.onAction() 	ofcourse:play()		fish2talk() 	end		-- 		"Of course, it’s a cactus.",
		if timer >160 	and stepdone==32 then stepdone=33 Talkies.onAction() 	whatsitdoing:play() fish1talk()	end		-- 		"What’s it doing here?",
		if timer >165 	and stepdone==33 then stepdone=34 Talkies.onAction() 	wevegotit:play()	fish1talk()		end		-- 		"We’ve got it. Now hold the key until I’ve left.",
		if timer >170 	and stepdone==34 then stepdone=35 Talkies.onAction() 	noproblem:play()	fish2talk() 	end		-- 		"No problem.",
		if timer >175 	and stepdone==35 then stepdone=36 Talkies.onAction() 	assonas:play()		fish2talk() 	end		-- 		"As soon I’ve learned to carry steel.",
		if timer >180 	and stepdone==36 then stepdone=37 Talkies.onAction() 	youdbetterlearn:play()	fish1talk()	end		-- 		"You’d better learn quickly.",
		if timer >185 	and stepdone==37 then stepdone=38 Talkies.onAction() 	pleasehold:play()	fish1talk()		end		-- 		"Please hold the key this time.",
		if timer >190 	and stepdone==38 then stepdone=39 Talkies.onAction() 	illtry:play()		fish2talk() 	end		-- 		"I’ll try.",
		if timer >195 	and stepdone==39 then stepdone=40 Talkies.onAction() 	nothedoor:play()	fish1talk()		end		-- 		"No! The door closed again.",
		if timer >200 	and stepdone==40 then stepdone=41 Talkies.onAction() 	soopenit:play()		fish2talk() 	end		-- 		"So open it again.",
		if timer >205 	and stepdone==41 then stepdone=42 Talkies.onAction() 		grmbl:play()	fish1talk()		end		-- 		"Grmbl...",
		if timer >210 	and stepdone==42 then stepdone=43 Talkies.onAction() 	staycalm:play()		fish2talk() 	end		-- 		"Stay calm. We’ll get it soon.",
		if timer >215 	and stepdone==43 then stepdone=44 Talkies.onAction() 										end		-- 		

end

function subtitle76()

		--level 76

		if timer >0 	and stepdone==0 then stepdone=1 				 		lookasisaid:play()		fish2talk() 	end		-- 		"Look, as I said.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 		whatdidyousay:play()	fish1talk()		end		-- 		"What did you say?",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()		thatsomeonehas:play()	fish2talk() 	end		-- 		"That someone has built a lock in the stone.",
		if timer >15		and stepdone==3 then stepdone=4 Talkies.onAction() 		soyouweretalking:play()	fish1talk()	end		-- 		"So you were talking nonsense. There is no lock in the stone.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 		intheprevious:play() 	fish2talk() 	end		-- 		"In the previous level there was.",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()		whatdoesthat:play()		fish1talk()		end		-- 		"What does that matter in this level?",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()		therearefourlocks:play() fish2talk() 	end		-- 		"There are four locks and four keys here.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 		aha:play()				fish1talk()		end		-- 		"Aha.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()		wearegettingcloser:play() fish2talk() 	end		-- 		"We are getting closer to the creator of the lock in the previous level.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()  	ohwhereishe:play()		fish1talk()		end		-- 		"Oh? Where is he?",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() 	idontknow:play()		fish2talk() 	end		-- 		"I don’t know.",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() 	thenhowdoyouknow:play()	fish1talk()		end		-- 		"Then how do you know that we are getting closer.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() 	lookaround:play()		fish2talk() 	end		-- 		"Look around, what do you see?",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() 	abrickwall:play()		fish1talk()		end		-- 		"A brick wall.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	steel:play()			fish1talk()		end		-- 		"Steel.",
		if timer >75 	and stepdone==15 then stepdone=16 Talkies.onAction() 	you:play()				fish1talk()		end		-- 		"You.",
		if timer >80 	and stepdone==16 then stepdone=17 Talkies.onAction() 	water:play()			fish1talk()		end		-- 		"Water.",
		if timer >85 	and stepdone==17 then stepdone=18 Talkies.onAction() 	anexitwecan:play()		fish1talk()		end		-- 		"An exit we cannot reach.",
		if timer >90 	and stepdone==18 then stepdone=19 Talkies.onAction() 	darkness:play()			fish1talk()		end		-- 		"Darkness. But only when my eyes are closed.",
		if timer >95 	and stepdone==19 then stepdone=20 Talkies.onAction() 	whatelse:play()			fish2talk() 	end		--		"What else?",
		if timer >100 	and stepdone==20 then stepdone=21 Talkies.onAction() 	igiveup:play()			fish2talk() 	end		-- 		"I give up.",
		if timer >105 	and stepdone==21 then stepdone=22 Talkies.onAction() 	whichofthekeys:play()	fish1talk()		end		-- 		"Which of the keys might fit in the lock?",
		if timer >110 	and stepdone==22 then stepdone=23 Talkies.onAction() 	allornone:play()		fish2talk() 	end		-- 		"All or none. They all look the same.",
		if timer >115 	and stepdone==23 then stepdone=24 Talkies.onAction() 	atleastthekeys:play()	fish1talk()		end		-- 		"At least the keys are not stuck in the ground.",
		if timer >120 	and stepdone==24 then stepdone=25 Talkies.onAction() 	whydontyoulook:play()	fish1talk()		end		-- 		"Why don’t you swim really hard into to the wall? It might collapse.",
		if timer >125 	and stepdone==25 then stepdone=26 Talkies.onAction() 	whydontyou:play()		fish2talk() 	end		-- 		"Why don’t you look for a hole and take it apart from there?",
		if timer >130 	and stepdone==26 then stepdone=27 Talkies.onAction() 	maybetheplayer:play() 	fish2talk() 	end		-- 		"Maybe the player could modify this program so it would be possible?",
		if timer >135 	and stepdone==27 then stepdone=28 Talkies.onAction() 	thatwouldbecheating:play()	fish1talk()	end		-- 		"That would be cheating.",
		if timer >140 	and stepdone==28 then stepdone=29 Talkies.onAction() 		thiswouldbe:play()	fish1talk()		end		-- 		"This would be boring.",
		if timer >145 	and stepdone==29 then stepdone=30 Talkies.onAction() 	thatstrue:play()		fish2talk() 	end		-- 		"That’s true.",
		if timer >150 	and stepdone==30 then stepdone=31 Talkies.onAction() 	butthelevelwould:play()	fish2talk() 	end		-- 		"But the level would be solved.",
		if timer >155 	and stepdone==31 then stepdone=32 Talkies.onAction() 	doyouthink:play()		fish1talk()		end		-- 		"Do you think these keys would have been useful in the previous level?",
		if timer >160 	and stepdone==32 then stepdone=33 Talkies.onAction()	 ofcoursenot:play()	 	fish2talk() 	end		--"Of course not.",
		if timer >165 	and stepdone==33 then stepdone=34 Talkies.onAction() 	whynot:play() 			fish1talk()		end		--	"Why not?",
		if timer >170 	and stepdone==34 then stepdone=35 Talkies.onAction() 	thinkalittlebit:play()	fish2talk() 	end		--	"Think a little bit.",
		if timer >175 	and stepdone==35 then stepdone=36 Talkies.onAction() 	ofcourse1:play()	 	fish1talk()		end		--	"Of course! We needed a key with three tines.",
		if timer >180 	and stepdone==36 then stepdone=37 Talkies.onAction() 	ofcourse2:play() 		fish1talk()		end		--	"Of course! On these keys the tines have unequal length.",
		if timer >185 	and stepdone==37 then stepdone=38 Talkies.onAction() 	ofcourse3:play() 		fish1talk()		end		--	"Of course! There are four keys here but there was only one lock there.",
		if timer >190 	and stepdone==38 then stepdone=39 Talkies.onAction()	 nothelock:play()		fish2talk() 	end		--	"No. The lock was much bigger.",
		if timer >195 	and stepdone==39 then stepdone=40 Talkies.onAction() 	isee:play() 			fish1talk()		end		--	"I see.",
		if timer >200 	and stepdone==40 then stepdone=41 Talkies.onAction() 	 ifallthelocks:play()	fish1talk()		end		--	"If all the locks would vanish, we could leave more easily.",
		if timer >210 	and stepdone==41 then stepdone=42 Talkies.onAction() 	thatisthepoint:play()	fish2talk() 	end		--	"That is the point of locks.",
		if timer >215 	and stepdone==42 then stepdone=43 Talkies.onAction()	weshouldtake:play()		fish1talk()		end		--	"We should take a key with us for a later level.",
		if timer >220 	and stepdone==43 then stepdone=44 Talkies.onAction() 	goodidea:play()	 		fish2talk() 	end		--	"Good idea.",
		if timer >225 	and stepdone==44 then stepdone=45 Talkies.onAction()  	whydontwejust:play()	fish1talk()		end		--	"Why don’t we just unlock it?",
		if timer >230 	and stepdone==45 then stepdone=46 Talkies.onAction() 	ok:play() 				fish2talk() 	end		--	"OK.",
		if timer >235 	and stepdone==46 then stepdone=47 Talkies.onAction() 	 										end		--	
		if timer >240 	and stepdone==47 then stepdone=48 Talkies.onAction() 						 					end		--	
		if timer >245 	and stepdone==48 then stepdone=49 Talkies.onAction() 	 										end		--	

end

function subtitle77()

		--level 77

		if timer >0 	and stepdone==0 then stepdone=1 				 	fatherwasright:play()	human_vel002:play()		fish2talk() end		-- 		"Father was right. The linux users must be mad.",
		if timer >5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	whoelsewould:play()				fish1talk()					end		-- 		"Who else would make so many barriers.",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	totopitalloff:play()	human_vel002:play()		fish2talk() end		-- 		"To top it all off they did it for free.",
		if timer >15	and stepdone==3 then stepdone=4 Talkies.onAction() 	itisclearnow:play()				fish2talk() 				end		-- 		"It is clear now, they were trying to protect themselves from us.",
		if timer >20 	and stepdone==4 then stepdone=5 Talkies.onAction() 	theywere:play()			human_vel002:play()		fish1talk()	end		-- 		"They were building mountains, valleys, ...",
		if timer >25 	and stepdone==5 then stepdone=6 Talkies.onAction()	thelinuxusers:play()			fish2talk() 				end		-- 		"The linux users!",
		if timer >30 	and stepdone==6 then stepdone=7 Talkies.onAction()	thereisonelast:play()	human_vel002:play()		fish1talk()	end		-- 		"There is one last task: to put them away.",
		if timer >35 	and stepdone==7 then stepdone=8 Talkies.onAction() 	wemust:play()					fish1talk()					end		-- 		"We must put them away.",
		if timer >40 	and stepdone==8 then stepdone=9 Talkies.onAction()	ithinkweshould:play()	human_vel002:play()		fish2talk() end		-- 		"I think we should be happy if we manage to get one.",
		if timer >45 	and stepdone==9 then stepdone=10 Talkies.onAction()  butifwe:play()					fish1talk()					end		-- 		"But if we manage both we would be happier.",
		if timer >50 	and stepdone==10 then stepdone=11 Talkies.onAction() doyouthink:play()		human_vel002:play()		fish2talk() end		-- 		"Do you think that gettin one linux user out is enough?",
		if timer >55 	and stepdone==11 then stepdone=12 Talkies.onAction() noidont:play()					fish1talk()					end		-- 		"No I don’t. The remaining one could fork himself.",
		if timer >60 	and stepdone==12 then stepdone=13 Talkies.onAction() ourgoalisto:play()		human_vel002:play()		fish2talk() end		-- 		"Our goal is to reconcile the linux users.",
		if timer >65 	and stepdone==13 then stepdone=14 Talkies.onAction() oratleast:play()				fish1talk()					end		-- 		"Or at least to get them out.",
		if timer >70 	and stepdone==14 then stepdone=15 Talkies.onAction() 	inmyopinion:play()	human_vel002:play()		fish2talk() end		-- 		"In my opinion it would be better to give up on the linux users. Anyway, they weren’t in the assignment.",
		if timer >80 	and stepdone==15 then stepdone=16 Talkies.onAction() buttheyhacked:play()	human_vel002:play()		fish1talk()	end		-- 		"But they hacked this game and we can’t win while they’re here.",
		if timer >85 	and stepdone==16 then stepdone=17 Talkies.onAction() 	wow:play()			human_vel002:play()		fish1talk()	end		-- 		"Wow, you had to try pretty hard to put only one linux user away. You got over the steel construction.",
		if timer >90 	and stepdone==17 then stepdone=18 Talkies.onAction() 		butitisntenough:play() 	fish2talk()					end		-- 		"But it isn’t enough to win.",
		if timer >95 	and stepdone==18 then stepdone=19 Talkies.onAction() 	iwouldlike:play()	human_vel002:play()		fish1talk()	end		-- 		"I would like to tell the player to not take this room too seriously. After all a linux user made it.",
		if timer >100 	and stepdone==19 then stepdone=20 Talkies.onAction() 	hewasprobably:play()		fish2talk() 				end		--		"He was probably mad.",
		if timer >105 	and stepdone==20 then stepdone=21 Talkies.onAction() 	playerif:play()		human_vel002:play()		fish1talk()	end		-- 		"Player, if you are a linux user, don’t take this madness personally.",
		if timer >110 	and stepdone==21 then stepdone=22 Talkies.onAction() 	wearenttalkingabout:play() 	fish2talk()					end		-- 		"We aren’t talking about you, we are talking about the others.",
		if timer >115 	and stepdone==22 then stepdone=23 Talkies.onAction() 	itsinteresting:play()		fish2talk() 				end		-- 		"It’s interesting, they are quarreling about the same problems.",
		if timer >120 	and stepdone==23 then stepdone=24 Talkies.onAction() 	andtheyareusing:play()	human_vel002:play()	fish1talk()	end		-- 		"And they are using the same arguments.",
		
		if timer >125 	and stepdone==24 then stepdone=25 Talkies.onAction() 	whathaveyougot:play()									end		-- 		"What have you got over there on the bottom?",
		if timer >130 	and stepdone==25 then stepdone=26 Talkies.onAction() 	frozenbubbles:play()	human_vel002:play()	fishstop() linuser1talk()			end		-- 		"Frozen bubbles.",
		if timer >135 	and stepdone==26 then stepdone=27 Talkies.onAction() 	ifyoudropthree:play()						linuser2talk()			end		-- 		"If you join three bubbles of the same color they will drop away.",
		if timer >140 	and stepdone==27 then stepdone=28 Talkies.onAction() 							human_vel002:play()				end		-- 		"If only the linux users would be funny.",
		if timer >145 	and stepdone==28 then stepdone=29 Talkies.onAction() 	onesuperjoke:play()							linuser2talk()			end		-- 		"I know one super joke, do you know the difference between chattr and chmod?",
		if timer >150 	and stepdone==29 then stepdone=30 Talkies.onAction() 							human_vel002:play()	linuser2talk()			end		-- 		"man chattr > 1; man chmod > 2; diff 1 2",
		if timer >165 	and stepdone==30 then stepdone=31 Talkies.onAction() 	itsmoreembarrasing:play()	human_vel002:play()	linuser1talk()		end		-- 		"It’s more embarrassing than the last comic strip on userfriendly.org.",
		if timer >175 	and stepdone==31 then stepdone=32 Talkies.onAction() 	anddoyouknow:play()								linuser2talk()		end		-- 		"And do you know what eight trilobites is? It’s one trilobyte.",
		if timer >180 	and stepdone==32 then stepdone=33 Talkies.onAction() 	 												linuser2talk()		end		--		"Ha ha ha",
		if timer >185 	and stepdone==33 then stepdone=34 Talkies.onAction() 	 lookthisroom:play()	human_vel002:play()		linuser1talk()		end		--		"Look, this room is trying not to be opinionated. There is a penguin in the background, but if there would be logo of Debian, Fedora, ...",
		if timer >195 	and stepdone==34 then stepdone=35 Talkies.onAction() 	whydidyousaid:play()							linuser2talk()		end		--		"Why did you say Debian first?",
		if timer >200 	and stepdone==35 then stepdone=36 Talkies.onAction() 	iamrecitingin:play()	human_vel002:play() 	linuser1talk()		end		--		"I am reciting in alphabetical order.",
		if timer >205 	and stepdone==36 then stepdone=37 Talkies.onAction() 	 soyoushould:play()		human_vel002:play()		linuser2talk()		end		--		"So you should start with Arch Linux.",
		if timer >210	and stepdone==37 then stepdone=38 Talkies.onAction() 	 sorryiforgot:play()							linuser1talk()		end		--		"Sorry, I forgot about it. So Arch Linux, Debian, ...",
		if timer >215 	and stepdone==38 then stepdone=39 Talkies.onAction() 	whydidyousaydebian:play() human_vel002:play()	linuser2talk()		end		--		"Why did you say Debian twice?",
		if timer >220 	and stepdone==39 then stepdone=40 Talkies.onAction() 	 thisgamewas:play()								linuser1talk()		end		--		"This game was programmed on Debian.",
		if timer >225 	and stepdone==40 then stepdone=41 Talkies.onAction() 	 whywasntitdone:play()	human_vel002:play()		linuser2talk()		end		--		"Why wasn’t it done on Ubuntu?",
		if timer >230 	and stepdone==41 then stepdone=42 Talkies.onAction() 	ubuntuisjust:play()								linuser1talk()		end		--		"Ubuntu is just a bad copy of Debian.",
		if timer >235 	and stepdone==42 then stepdone=43 Talkies.onAction()	haveyoutried:play()		human_vel002:play()		linuser2talk()		end		--		"Have you tried Ubuntu?",
		if timer >240 	and stepdone==43 then stepdone=44 Talkies.onAction() 	canyoulogin:play()		 						linuser1talk()		end		--		"No, but I’ve heard plenty. Can you log in as the root on it?",
		if timer >245 	and stepdone==44 then stepdone=45 Talkies.onAction()  	ubuntudoesntneed:play()	human_vel002:play()		linuser2talk()		end		--		"Ubuntu doesn’t need antiquities like a root user.",
		if timer >250 	and stepdone==45 then stepdone=46 Talkies.onAction() 	butitisbased:play()	 	human_vel002:play()		linuser1talk()		end		--		"But it is based on them.",
		if timer >255 	and stepdone==46 then stepdone=47 Talkies.onAction() 	 aninterface:play()								linuser2talk()		end		--		"An interface between the user and Unix exists in every distribution.",
		if timer >260 	and stepdone==47 then stepdone=48 Talkies.onAction() 	whataboutslackware:play()	human_vel002:play() linuser1talk()		end		--		"What about Slackware?",
		if timer >265 	and stepdone==48 then stepdone=49 Talkies.onAction() 	 ohyes:play()									linuser2talk()		end		--		"Oh yes, but its packaging system is lousy.",
	
		if timer >270 	and stepdone==49 then stepdone=50 Talkies.onAction()	atleastitsowned:play() 							linuser2talk()		end		-- 		"At least it’s owned by Slackware.",
		if timer >275 	and stepdone==50 then stepdone=51 Talkies.onAction()	doyouknowwhy:play()		human_vel002:play()		linuser1talk()		end		-- 		"Do you know why there are snakes?",
		if timer >280 	and stepdone==51 then stepdone=52 Talkies.onAction()	 ofcourse:play()								linuser2talk()		end		-- 		"Of course, it’s the logo of the best programming language.",
		if timer >285 	and stepdone==52 then stepdone=53 Talkies.onAction()	 whendidc:play()		human_vel002:play()		linuser1talk()		end		-- 		"When did C get a logo with snakes?",
		if timer >290 	and stepdone==53 then stepdone=54 Talkies.onAction()	 cisancient:play()								linuser1talk()		end		-- 		"C is ancient and outdated. Now there are C++ and Java.",
		
		if timer >295 	and stepdone==54 then stepdone=55 Talkies.onAction()	 java:play()			human_vel002:play()		linuser1talk()		end		-- 		"Java is an interpreted programming language so it’s very slow. Your Python has the same problem.",
		if timer >300 	and stepdone==55 then stepdone=56 Talkies.onAction()	 imnotinterested:play()							linuser2talk()		end		-- 		"I am not interested in speed but in programming comfort.",
		if timer >305 	and stepdone==56 then stepdone=57 Talkies.onAction()	 yesyouare:play()		human_vel002:play()		linuser2talk()		end		-- 		"Yes, you are.",
		if timer >310 	and stepdone==57 then stepdone=58 Talkies.onAction()	 lookwilber:play()								linuser1talk()		end		-- 		"Look, Wilber!",
		if timer >315 	and stepdone==58 then stepdone=59 Talkies.onAction()	 itisthemascot:play()	human_vel002:play()		linuser2talk()		end		-- 		"It is the mascot of a great graphics application.",
		if timer >320	and stepdone==59 then stepdone=60 Talkies.onAction()	 whichistrying:play()							linuser1talk()		end		-- 		"Which is trying to catch up to Photoshop.",
		if timer >325 	and stepdone==60 then stepdone=61 Talkies.onAction()	 thegimp:play()			human_vel002:play()		linuser2talk()		end		-- 		"The GIMP is not Photoshop. It works completely differently.",
		if timer >330 	and stepdone==61 then stepdone=62 Talkies.onAction()	 thegimpdevelopers:play()						linuser1talk()		end		-- 		"The GIMP developers say that to conceal their inability.",
		if timer >335 	and stepdone==62 then stepdone=63 Talkies.onAction()	 imbored:play()			human_vel002:play()		linuser2talk()		end		-- 		"I am bored with this music.",
		if timer >340 	and stepdone==63 then stepdone=64 Talkies.onAction()	 couldntyou:play()								linuser1talk()		end		-- 		"Couldn’t you write a script for this?",
		if timer >345 	and stepdone==64 then stepdone=65 Talkies.onAction()	 						human_vel002:play()		linuser2talk()		end		-- 		"And I am bored with the music switching.",
		if timer >350 	and stepdone==65 then stepdone=66 Talkies.onAction()	 												linusersstop()		end		-- 		
		if timer >360 	and stepdone==66 then stepdone=67 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >370	and stepdone==67 then stepdone=68 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >380 	and stepdone==68 then stepdone=69 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >390 	and stepdone==69 then stepdone=60 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >400 	and stepdone==70 then stepdone=71 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >410 	and stepdone==71 then stepdone=72 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >420 	and stepdone==72 then stepdone=73 Talkies.onAction()	 human_vel002:play()									end		-- 		
		if timer >430 	and stepdone==73 then stepdone=74 Talkies.onAction()	 human_vel002:play()									end		-- 		
		

end

function subtitle77end()
		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction() 	whenwesaw:play()			end		-- 		When we saw the linux users we didn’t know what we should do with them.
		if timer >7 	and stepdone==1 then stepdone=2 twotalkiesactions()	wedidntunderstand:play()	end		-- 		We didn’t understand why we got them and it seemed they would never agree with each other.
		if timer >15 	and stepdone==2 then stepdone=3 twotalkiesactions()	butfinally:play()			end		-- 		But finally we got them to calm down, by confronting them with a windows user.
		if timer >23	and stepdone==3 then stepdone=4 twotalkiesactions() fromthenon:play()			end		-- 		From then on they kept the motto: Gentoo or Mandriva, we’re all one family.
		if timer >35 	and stepdone==4 then stepdone=5 twotalkiesactions() althought:play()			end		-- 		Although you didn’t solve any problem which we assigned to you 
		if timer >40 	and stepdone==5 then stepdone=6 twotalkiesactions()	iwouldnthaveleft:play()		end		-- 		I wouldn’t have left them in one game together either 
		if timer >45 	and stepdone==6 then stepdone=7 Talkies.onAction()	andtheyarequite:play()		end		-- 		and they are quite useful here.
end

function subtitle79()

		--level 79

		if timer >0 	and stepdone==0 then stepdone=1 				 	theplayershould:play()		fish1talk()				end		-- 		"The player should know that the cylinders are linked with the cars of the same color",
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 	byaninvisible:play()		fish2talk()				end		-- 		"BY AN INVISIBLE POWER!!",
		if timer >10 	and stepdone==2 then stepdone=3 Talkies.onAction()	somebody:play()				fish2talk()				end		-- 		"Somebody is playing games with us -- the cars are moved. Or do you do it? Or do I? I am begining to be afraid.",
		if timer >25		and stepdone==3 then stepdone=4 Talkies.onAction() 	iwonderif:play()			fish1talk()				end		-- 		"I wonder if fish fillets is a good game to solve this room.",
		if timer >30 	and stepdone==4 then stepdone=5 Talkies.onAction() 	whatdoyoumean:play()		fish2talk()				end		-- 		"What do you mean it?",
		if timer >35 	and stepdone==5 then stepdone=6 Talkies.onAction()	theplayershouldlearn:play() fish1talk()				end		-- 		"The player should learn to solve it somewhere else and he should only repeat it here.",
		if timer >40 	and stepdone==6 then stepdone=7 Talkies.onAction()	onthecontrary:play()		fish2talk()				end		-- 		"On the contary, if the player solves it only here, I will admire him secretly.",
		if timer >45 	and stepdone==7 then stepdone=8 Talkies.onAction() 	weshouldgetout:play()		fish2talk()				end		-- 		"We should get out the red car.",
		if timer >50 	and stepdone==8 then stepdone=9 Talkies.onAction()								fish1talk()				end		-- 		"Don’t be clever when somebody else already is.",
		if timer >55 	and stepdone==9 then stepdone=10 Talkies.onAction()  whatareyoudoing:play()		fish1talk()				end		-- 		"What are you doing? You aren’t out yet.",
		if timer >60 	and stepdone==10 then stepdone=11 Talkies.onAction() 							fish1talk()				end		-- 		
		--[[if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 					end		-- 		
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() 					end		-- 		
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() 					end		-- 		
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() 					end		-- 		
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() 					end		-- 		
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 					end		-- 		
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 					end		-- 		
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() 					end		-- 		
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() 					end		--
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() 					end		-- 		
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction() 					end		-- 		
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction() 					end		-- 		
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction() 					end		-- 		
		if timer >110 	and stepdone==24 then stepdone=25 Talkies.onAction() 					end		-- 		
		if timer >115 	and stepdone==25 then stepdone=26 Talkies.onAction() 					end		-- 		
		if timer >120 	and stepdone==26 then stepdone=27 Talkies.onAction() 					end		-- 		
		if timer >125 	and stepdone==27 then stepdone=28 Talkies.onAction() 					end		-- 		
		if timer >130 	and stepdone==28 then stepdone=29 Talkies.onAction() 					end		-- 	
		if timer >135 	and stepdone==29 then stepdone=30 Talkies.onAction() 					end		-- 		
		if timer >140 	and stepdone==30 then stepdone=31 Talkies.onAction() 					end		-- 		
		if timer >145 	and stepdone==31 then stepdone=32 Talkies.onAction() 					end		-- 		
		--]]
end


function subtitle79end()

		--level 79

		if timer >0 	and stepdone==0 then stepdone=1 Talkies.onAction() 	goodmorningfish:play()		end		-- 		"Good morning, fish!",
		if timer >3 	and stepdone==1 then stepdone=2 twotalkiesactions() 	againyoudidnt:play()	end		-- 		"Again, you didn’t disappoint us.",
		if timer >6 	and stepdone==2 then stepdone=3 twotalkiesactions()	generalcomittee:play()		end		-- 		"General Committee decided to decorate you with the highest orders.",
		if timer >10	and stepdone==3 then stepdone=4 twotalkiesactions() 	 theyaremade:play()		end		-- 		"They are made of milk chocolate. Due to confidentiality, eat them immediately.",
		if timer >15 	and stepdone==4 then stepdone=5 twotalkiesactions() 	boss:play()				end		-- 		"BOSS",
		if timer >20 	and stepdone==5 then stepdone=6 twotalkiesactions()	iunderstand:play() 			end		-- 		"PS: I understand this little pld issue, but next time please tell me in advance,",
		if timer >25 	and stepdone==6 then stepdone=7 twotalkiesactions()	sothatwecanprovide:play()	end		--		"so that we can provide an adoption permission.",
		if timer >30 	and stepdone==7 then stepdone=8 twotalkiesactions() 	tellme:play()			end		-- 		"PPS: Tell me, where did you find such a good player that he managed it all?",
		if timer >35 	and stepdone==8 then stepdone=9 twotalkiesactions()	iwishhewon:play()			end		-- 		"I wish he won the computer or at least some of the other prizes.",
		if timer >40 	and stepdone==9 then stepdone=10 Talkies.onAction()  							end		-- 		

end
		
--[[
function subtitle79()

		--level 79

		if timer >0 	and stepdone==0 then stepdone=1 				 						end		-- 		
		if timer >3.5 	and stepdone==1 then stepdone=2 Talkies.onAction() 						end		-- 		
		if timer >7 	and stepdone==2 then stepdone=3 Talkies.onAction()						end		-- 		
		if timer >9		and stepdone==3 then stepdone=4 Talkies.onAction() 						end		-- 		
		if timer >11 	and stepdone==4 then stepdone=5 Talkies.onAction() 						end		-- 		
		if timer >14 	and stepdone==5 then stepdone=6 Talkies.onAction()						end		-- 		
		if timer >20 	and stepdone==6 then stepdone=7 Talkies.onAction()						end		-- 		
		if timer >25 	and stepdone==7 then stepdone=8 Talkies.onAction() 						end		-- 		
		if timer >30 	and stepdone==8 then stepdone=9 Talkies.onAction()						end		-- 		
		if timer >35 	and stepdone==9 then stepdone=10 Talkies.onAction()  					end		-- 		
		if timer >40 	and stepdone==10 then stepdone=11 Talkies.onAction() 					end		-- 		
		if timer >45 	and stepdone==11 then stepdone=12 Talkies.onAction() 					end		-- 		
		if timer >50 	and stepdone==12 then stepdone=13 Talkies.onAction() 					end		-- 		
		if timer >55 	and stepdone==13 then stepdone=14 Talkies.onAction() 					end		-- 		
		if timer >60 	and stepdone==14 then stepdone=15 Talkies.onAction() 					end		-- 		
		if timer >65 	and stepdone==15 then stepdone=16 Talkies.onAction() 					end		-- 		
		if timer >70 	and stepdone==16 then stepdone=17 Talkies.onAction() 					end		-- 		
		if timer >75 	and stepdone==17 then stepdone=18 Talkies.onAction() 					end		-- 		
		if timer >80 	and stepdone==18 then stepdone=19 Talkies.onAction() 					end		-- 		
		if timer >85 	and stepdone==19 then stepdone=20 Talkies.onAction() 					end		--
		if timer >90 	and stepdone==20 then stepdone=21 Talkies.onAction() 					end		-- 		
		if timer >95 	and stepdone==21 then stepdone=22 Talkies.onAction() 					end		-- 		
		if timer >100 	and stepdone==22 then stepdone=23 Talkies.onAction() 					end		-- 		
		if timer >105 	and stepdone==23 then stepdone=24 Talkies.onAction() 					end		-- 		
		if timer >110 	and stepdone==24 then stepdone=25 Talkies.onAction() 					end		-- 		
		if timer >115 	and stepdone==25 then stepdone=26 Talkies.onAction() 					end		-- 		
		if timer >120 	and stepdone==26 then stepdone=27 Talkies.onAction() 					end		-- 		
		if timer >125 	and stepdone==27 then stepdone=28 Talkies.onAction() 					end		-- 		
		if timer >130 	and stepdone==28 then stepdone=29 Talkies.onAction() 					end		-- 	
		if timer >135 	and stepdone==29 then stepdone=30 Talkies.onAction() 					end		-- 		
		if timer >140 	and stepdone==30 then stepdone=31 Talkies.onAction() 					end		-- 		
		if timer >145 	and stepdone==31 then stepdone=32 Talkies.onAction() 					end		-- 		

end
--]]
function sixtalkiesactions()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
end

function fivetalkiesactions()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
end
function twotalkiesactions()
			Talkies.onAction()
			Talkies.onAction()
end
function threetalkiesactions()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
end
function fourtalkiesactions()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
			Talkies.onAction()
end
