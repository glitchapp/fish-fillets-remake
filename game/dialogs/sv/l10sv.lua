l10={
"Titta. Partybåten.",
"Jag har en känsla av att det inte kommer att bli en normal picknick.",
"Du och dina känslor. Den är säkert fullt av snälla och trevliga människor.",
"Du kanske har rätt. Det här är en underlig picknick.",
"Tror du att de kan ta sig ut ur den här båten?",
"Jag hoppas att de kan ta sig ut ur båten.",
"Vad gör vi om de börjar att förfölja oss?",
"Jag tror inte de får tag i oss. Skelett simmar inte så bra.",
"Sluta tänk på det och sätt fart på fenorna så vi kommer ut.",
"Kan du se till att stödja stålröret med ett glas?",
"Jag kan inte göra något annat för dig.",
"Försiktig, så du inte spiller!",
}
