l64={
"Den där saken måste var den heliga gral!",
"Hur kan du vara säker på det?",
"Ser du inte den heliga inskriptionen?",
"Ser du inte hur den lyser upp vid de andra?",
"Ser du inte hur misstänkt den är placerad i mitten av grottan?",
"Den heliga inskriptionen betyder inget. Den heliga gral kan vara helt slät.",
"Allt som glimmar är inte guld. Ingen har sagt att den heliga gral måste lysa.",
"Hum... Att ställa en vanlig gral i ett misstänkt rum gör den inte till en helig.",
"Jag känner att det inte kommer att bli som det ser ut.",
"Du och dina känslor.",
"Kom igen, vi tar med oss den lysande gralen i mitten.",
"Jag skulle hellre ta med alla.",
"Humm... Det ser ut som om du har rätt. Vi tar med alla.",
"Vårt mål på denna nivå är att knuffa ut den heliga gralen.",
"Nej, knuffa ut alla graler.",
"Den heliga räcker.",
"Vårt mål är att knuffa ut alla gralar ur det här rummet.",
"Nästan alla är borta, nu!",
"En till gral, sen är vi klara!",
}
