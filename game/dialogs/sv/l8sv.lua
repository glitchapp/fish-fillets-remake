l8={
"Toaletten är Davids favorit ställe.",
"Va?",
"Du känner inte David? Han är en av konstnärerna som gjorde det här spelet.",
"David var ingen grafisk konstnär. Han var skulptör. Hans skulpturer är bland de mest kända från renässansen.",
"Du har missförstått det hela.",
"Sade jag inte till dig att VI inte behöver en spolande toalett? Och dekorerad med dina ekivoka bilder, också!",
"En spolande toalett är mera hygienisk och när den är under vatten är den även mera ekologisk.",
"Som tur var behövde jag inte klättra inuti.",
"Det känns inte så kul. Det känns som om jag är på en kyrkogård.",
"Vad menar du?",
"Vet du inte var många akvariefiskar slutar sina liv?",
}
