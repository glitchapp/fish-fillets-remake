l61={
"Vi hittade den till slut.",
"Vad då?",
"Kapten Silvers skatt, så kart!",
"Den heliga gral, så klart!",
"Så praktiskt: skatten och gralen på samma plats.",
"Jag tror att det är för bra för att vara sant.",
"Jag tror att spelaren skulle veta om det var vårt mål.",
"Men jag har redan sagt det. Det kan vara tillräckligt.",
"Men jag har skapat tvivel. Det kan förvirra dem.",
"Tja, OK. TROLIGEN är det här inte vad vi söker.",
"Bättre. Det finns fortfarande en viss osäkerhet.",
"Kan vi inte stänga kistan?",
"Om vi skulle försöka att stänga kistan?",
"Det skulle vara mycket enklare med kistan stängd.",
"Försök att trycka ner locket.",
"Ska vi inte ta något med oss?",
"Om vi skulle ta lite skatter med oss?",
"Är det inte synd att lämna alla skatter?",
"Vi kommer att vara glada att över huvudtaget ta oss ut själva.",
"Glöm inte vårt uppdrag.",
"Skatten skulle bara hindra oss.",
"Allt det här glittret är en fröjd för ögat.",
"Allt här glittrar så mycket. Jag blir alldeles yr.",
}
