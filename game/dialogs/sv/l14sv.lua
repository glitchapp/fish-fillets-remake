l14={
"Vad är det här för ett konstigt skepp?",
"Det här är vraket efter passagerarflygplanet LC-10 Lemura.",
"Det här är vraket efter civilflygplanet Atlantobus.",
"Det här är vraket efter civilflygplanet Poseidon 737.",
"Kan du se ögat? Ett tyst vittne till en tragedi... Någon litade på det här planet - och glasögat är det enda som finns kvar.",
"Det är inget glasöga utan ett gyroskop. Åtminstone på den här nivån",
"Säten. Varför finns det så många säten här?",
"Var tacksam. Kan du komma ut utan dem?",
}
