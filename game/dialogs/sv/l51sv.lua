l51={
"Så, kartan finns trotts allt!",
"OK. Här är kartan.",
"Så det är tydligt att vårt mål är att få ut kartan på något sätt.",
"Så klart. Det skulle ha varit för lätt om vi bara skulle simma iväg.",
"Prata inte strunt utan försök att tänka istället!",
"Vårt mål är att få ut kartan på något sätt.",
"Vad finns det på kartan?",
"Tror du verkligen att den pekar ut Silvers skatt?",
"Vi får se när vi har fått ut den där jädra kartan.",
"Vi behöver flera sniglar.",
"Du menar Escargoer, eller hur?",
"Vi behöver fler glasögon.",
"Det enklaste sättet borde vara att ta oss till den övre delen av nivån. Försök att starta om, vi kanske dyker upp där.",
"Låssas inte om att du inte förstår reglerna efter så många nivåer.",
"Så vi lyckades flytta på den!.",
"Fortsätt så där!",
"Ja, det är nästan klart.",
}

l51end={"Efter mycket hårt arbete lyckade vi hitta kapten Silver karta.",
"Den första entusiasmen efteråt förbyttes mot bitter besvikelse",
"när vi fann att tidigare nämnda kartan",
"inte pekar ut var skatten finns",
"utan platsen där de sista överlevande av Silvers papegojor som,",
"oturligt nog,",
"lider av skleros och kan inte minnas var skatten finns.",
}
