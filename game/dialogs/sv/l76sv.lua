l76={
"Titta, precis som jag sa.",
"Vad sa du?",
"Att någon har gjort ett lås i stenen.",
"Du bara snackar. Här finns det inget lås.",
"På förra nivån fanns det.",
"Vad har det för betydelse för den här nivån?",
"Här finns fyra lås och fyra nycklar.",
"Aha.",
"Vi kommer närmare skaparen av låsen i den förra nivån.",
"Nå? Var finns han?",
"Jag vet inte.",
"Hur vet du att vi närmar oss?",
"Se dig omkring, vad ser du?",
"En tegelmur.",
"Stål.",
"Dig.",
"Vatten.",
"En utgång som vi inte kan nå.",
"Mörker. Men bara när jag stänger ögonen.",
"Vad mer?",
-- get another of the rand-0-5 which was not used jet.
"Jag ger upp.",
"Vilken av nycklarna kan passa i låset?",
"Alla eller ingen. De ser alla likadana ut.",
"Nycklarna sitter åtminstone inte fast i marken.",
"Kan du inte simma in i väggen i full fart. Den rasar kanske ihop?",
"Kan du inte leta efter en spricka och bryta upp väggen därifrån?",
"Spelaren kanske vill ändra i programmet så att det blir möjligt.",
"Det skulle inte vara just.",
"Det skulle vara långtråkigt.",
"Det är sant.",
"Men nivån skulle bli löst.",
"Tror du att nyckeln skulle ha funkat i förra nivån?",
"Naturligtvis inte.",
"Varför inte?",
"Tänk efter lite.",
"Självklart! Vi behöver en nyckel med tre ax.",
"Självklart! De här nycklarna har ax av samma längd.",
"Självklart! Det är fyra nycklar men bara ett lås.",
"Nej. Låset var mycket större.",
"Jaha.",
"Om alla lås försvann skull det vara lättare att ta komma ut.",
"Det är vitsen med lås.",
"Vi borde ta med oss en nyckel till senare nivåer.",
"Bra idé.",
"Varför låser vi inte bara upp?",
"Ok.",
}
