l56={
"Tänd ljuset, NU!",
"Vad tror du att du gör?",
"Sluta upp och tänd ljuset.",
"Vänta, jag försöker hitta knappen.",
"Jag kan inte. Den jädra knappen trillade ner.",
"Tänd ljuset själv, om du är så smart.",
"Var är du?",
"Hallååå.",
"Säg nåt!",
"Här.",
"Jag är här.",
"Jag är här borta.",
"Var är du, jag är rädd.",
"Jag är mörkrädd.",
"Snälla gå inte. Det är så mörkt här.",
"Var inte rädd. Jag är här.",
"Sluta gnälla. Vi klarar det.",
"Det finns inget att vara rädd för.",
"Jag hoppas att inget kommer att sparka mig...",
"De här blinkande ljusen börjar irritera mina ögon.",
"Jag trodde aldrig att jag skulle bli så här glad att se dig igen.",
"Till slut är ljuset tänt igen...",
"Försiktig, den biter!",
"Titta! Det måste vara en... ROBOTHUND!",
"Vad då? DEN robothund?!?",
"Jaså, nu klarnar allt.",
"Även Kubakrisen och Nostradamus profetior.",
"Även mordet på John Lennon och Tunguzian meteoriten.",
"Även Challenger explosionen och slaget vid Bull Run.",
"Även Chernobyl olyckan och resterna av Noaks ark.",
"Nu kan jag se det rationella bakom det här spelet.",
"Och även meningen med livet, universum och allting.",
"KLICK",
"KLICK",
"Lyssna inte på dem, spelare. De bara skojar med dig.",
"Lyssna inte på dem, spelare. Det försöker att var roliga.",
"Lyssna inte på dem, spelare. De försöker lura dig.",
"Jag är endast en ödmjuk cybernetisk leksak.",
"Trots att jag från början skulle bli en skoter.",
"Trots att jag från början skulle bli en gräsklippare.",
"Trots att jag från början skulle bli en rymdstation.",
"Trots att jag från början skulle bli en tårtbit.",
"Och jag var bara på Kuba en enda gång.",
"Och alla vet att någon annan mördade honom, de kan inte bevisa det.",
"Och vem bryr sig om några soldater.",
"Och de sa till mig att reglerstavarna inte var viktiga.",
}
