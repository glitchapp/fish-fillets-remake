l17={
"Det här skeppet måste ha sjunkit för länge sedan.",
"Definitivt. Det är ett av de tidigaste långskeppen. Att döma av formen på huvudet, antalet spiraler och färger på vattnet skulle jag tro att det tillhörde Erik den rödes farfar.",
"Eller Olaf den tappres svärfar.",
"Eller kanske Leif den kluriges farbrors far.",
"Den kan också ha tillhört Harald den stores syster dotter.",
"Eller möjligtvis Snorre den modiges faster.",
"Det är också möjligt att den tillhörde Thorson den hårde.",
"Jag har alltid trott att jag var Erik den ofärdiges fiskebåt... Men idag har jag fått veta sanningen. Ja, det är aldrig försent att starta ett nytt liv!",
"Titta på den stackars hunden.",
"Ser han inte ut att andas lite konstigt?",
"För en hund under vattnet så tycker jag han klarar sig bra.",
"Han ser faktiskt inte så dålig ut med tanke på att han har varit här i århundraden.",
"Håll tyst!",
"Kan ni vara tysta!",
"Så ni tänker inte hålla tyst, gör ni?",
"Aaaaargh...",
"He-he-he-he-he... Eh-eh-eh-eh...",
"Är det slut?",
"Är vi i säkerhet nu?",
"Är striden slut?",
"Är fienden äntligen borta?",
"Slåss vi fortfarande?",
"Är vi i Valhall ännu, chefen?",
"När kommer vi till Valhall?",
"Jag säger till när vi kommer dit.",
"Kan jag gå och lätta på trycket, chefen?",
"Vänta tills vi är i Valhall.",
"Börjar inte resan till Valhall att ta lite för lång tid?",
"Tålamod är ett tecken på en riktig krigare.",
"Är du säker på att det är så här man kommer till Valhall?",
"En krigare måste lita på sin befälhavare!",
"Neeej, jag har befälet över ett skepp fullt av ynkryggar!",
"Varför skulle jag få ett skepp fullt av skrattande fånar?",
"Skrattande fånar och en fyllehund. Jag tror att jag dränker mig.",
"Riktiga vikingar har skägg.",
"F-f-flätor är inne nu f-f-för tiden.",
"Vilken fjolla är det där? Flätor? Men herre gud!",
"V-v-vikinga modes m-m-månadstidning r-r-rekomenderar en ljusa fläta för att komplementera en m-m-mörk hjälm och en b-blå sköld.",
"Jag håller helt enkelt inte med en sådan fjolla.",
"Vi borde hålla oss till vikingatraditionerna istället.",
"M-m-men jag är c-c-cool!",
"Till och med Erik den s-s-store hade fläta!",
"Nonsens. Han hade skägg.",
"Men h-han h-hade också fläta.",
"Hade han inte alls. Där med basta!",
"T-t-tror du att ett s-s-skägg hade passat mig b-b-bättre?",
"Definitivt.",
"Jag ska t-t-tänka på d-det.",
"Hummm... Ehmmm... Hahmmm...",
"N-nej, f-f-flätor är d-definitift b-b-bättre.",
"Dagens ungdom - helt otroligt.",
"En krigare med flätor. Jag har aldrig hör på maken.",
"Ungdomarna tror att man kan komma undan med vad som helst idag.",
"Men, jag ser fortfarande cool ut.",
}
