l73={
"Den är redan här!",
"Vad då?",
"Antingen kommer jag ut eller du.",
 
"Och vem ska det bli?",
"Det borde vara jag,",
"men spelaren bestämmer.",

"Vad tänker du på? Du befriar jätten och lämnar mig här. OK, han kan flytta på stål, och?",

"Han kommer att vänta på mig i nästa rum. Starta om, nu!",

"Det är inte rättvist, min väg var fri. Jag flyttade bara stålet åt fel håll",

"Den lilla fisken simmar vilse i den öppna oceanen. Utan mig kommer hon inte långt. Du borde starta om rummet omedelbart.",
"Det är inte doktorns order.",
"Jag borde inte slänga de större bitarna ovanpå de små.",

"Någon kan tro att det här bara en kopia av ett rum från fish fillets 2.",
"Men författaren vara bara inspirerad av rummet med hårdvaruproblemet.",

"Varför måste jag sortera alla bitar när ett enkelt rekursivt program skulle kunna göra det?",
"Hur kan spelaren finna nöje i det här?",
"Han kanske ser fram mot nästa rum.",
"Han vet inte hur de ser ut.",
"Antagligen psykade någon honom.",
"Spelare om du inte löser det här, då är du en stor förlorare.",
"Självklart så känner du dig inte road om du nästan inte hjälper mig alls.",
"Du har redan sagt det tre gånger.",
"Om jag alltid skulle säga något annat skulle vi fylla upp spelarens disk",
"Kanske dagens spelares diskar rymmer mer data än tidigare",
"Jag tror att oberoende av disken storlek så är den alltid full.",
"Skräms, fuskare.",

"Äntligen tackar jag spelaren för att han löste det den långa vägen.",
}
