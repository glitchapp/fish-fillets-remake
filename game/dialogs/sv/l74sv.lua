l74={
"Det här är ett hemsökt rum.",
"Jag är rädd... att det kommer att bli svårt att flytta saker över hålet.",
"Var så god, som du önskar.",
"Är det någon här?",
"Vem är där?",
"Jag tror att mottagaren inte fungerar.",
"Lägg tillbaks den genast, indianskruden är min!",
"Lägg tillbaks den genast, den är min!",
"O O O O, jag är indian. Lägg tillbaks den genast!",
"O O O O, lägg tillbaks den genast",
"Vart knuffar du mig? Jag är den berömde Vinnetou!",
"Förlåt, vi måste ha den.",
"Hur kan den vara din, om du inte är indian?",
"Du kan inte vara indian, för du är inte röd?",
"Jag kan inte invända något.",
"Spelare, om du är en indian, se det inte som diskriminering, snälla.",
"Det kanske är en magisk mottagare.",
"Testa den.",
"Hallå jag vill att stålet vid utgången försvinner?",
"Ställ tillbaka den, det borde lösas av spelaren, inte av oss själva.",
}
