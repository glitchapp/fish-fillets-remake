l66={"Voici un niveau pour les vrais connaisseurs.",
"Ouais, Je me sens comme si je pouvais embrasser l'histoire du huit bits.",
"Peux-tu voir ces graphiques pixélisés tout autour de nous ? Je dois avouer que ça me rend nostalgique.",
"C'est une opportunité pour jouer aux joyeux vieux jeux du ZX Spectrum.",
"À quoi penses-tu en ce moment ?",
"Et bien, je viens de réaliser que nos animations seules pourraient remplir la RAM du ZX Spectrum.",
"Est-ce que tu sais que ta question seule au format WAV ne tiendrait pas dans la RAM du ZX Spectrum ? Et imagine la quantité de sons qu'il y a dans ce jeu.",
"Ces jeux avaient néanmoins un petit quelque chose, non ?",
"Tu peux voir ce Knight Lorer ?",
"Et là ce doit être le Manic Miner.",
"Je ne pensais vraiment pas que tant de choses nous séparaient encore.",
"Cette rangée de robots m'est familière.",
"Bien sûr, ils viennent de Highway Encounter.",
"Je me demande ce qui est en train de se charger.",
"Le seul avantage substantiel de cette pièce est qu'il n'y a pas d'acier.",
}
