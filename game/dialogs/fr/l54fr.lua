l54={"Je pense que nous pouvons glaner quelques informations à propos de la propulsion interstellaire ici.",
"Ceci ressemble plus à la propulsion conventionnelle d'un véhicule d'atterrissage.",
"Enfin. Nous avons trouvé l'accès. Nous avons atteint un des objectifs de notre mission.",
"Pas de précipitation. Nous n'avons pas encore fouillé toute l'épave.",
"Je suis déçue qu'aucune de ces merveilles technologiques ne marche.",
"J'aimerais voir fonctionner quelques uns de ces gadgets extraterrestres.",
"Je me demande si ce moteur peut tourner sous l'eau.",
"Peut-être qu'il vaut mieux pour nous qu'il ne puisse pas.",
"Je suis assez rassuré de ne rien pouvoir allumer ici.",
"Nous devrions être contents d'être incapables d'allumer quoique ce soit.",
}

l54engineon={"Qu'as-tu fait ? Éteins ce tintamarre.",
"Qu'est-ce que tu as déclenché ? Où est-ce qu'on nous emmène ?",
"C'est horrible ! Éteins-le avant qu'il ne soit trop tard !",
"Je ne peux pas l'arrêter !",
"Je ne sais pas comment ! Impossible de l'arrêter !",
"Je ne peux pas ! Impossible de l'arrêter !",
"Comment puis-je l'arrêter ?!",
"SOS! SOS!",
"How can I turn it off?!",
"I can’t turn it off!"
}

l54engineonfish1={"Je ne peux pas l'arrêter !",
"Je ne sais pas comment ! Impossible de l'arrêter !",
"Je ne peux pas ! Impossible de l'arrêter !",
"Comment puis-je l'arrêter ?!",
"SOS! SOS!",
"Comment puis-je l'arrêter ?!",
}
--"I can’t turn it off!"
l54engineoff={"Enfin.",
"Quel soulagement.",
"Merci.",
"Enfin.",
"Enfin.",
"J'ai seulement peur que nous ayons à le rallumer.",
""
}

l54aproachengine={"Qu'est-ce que tu fais ? Où allons nous ?",
"Attention avec cette clé.",
"Je ne peux blesser personne ici.",
}
