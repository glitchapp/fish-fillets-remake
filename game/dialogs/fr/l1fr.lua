l1={"Qu'est ce que c'était ?",
"Aucune idée.",
"Nous devrions aller voir dehors.",
"Attends ! Je viens avec toi",
"Pourquoi rien ne se passe ?",
"Hé, joueur(se) !",
"Tu peux nous contrôler avec les flèches, et utiliser la barre espace pour changer de poisson.",
"Ou tu peux nous contrôler à la souris -- Clique quelque part et ...",
"... si nous pouvons y aller sans déplacer d'objet...",
"... nous suivrons tes clics.",
"Et si tu cliques sur le bouton droit quelque part, nous essaierons d'y parvenir en poussant tous les objets devant nous.",
"Ton but est de nous faire sortir tous les deux.",
"Attention à ne pas nous blesser. Si tu nous fais tomber quelque chose dessus ou ...",
"Oh, arrête. Nous lui dirons dans le prochain niveau.",
"D'accord.",



"Oups ! Quelle erreur. Nous devons sortir tous les deux pour pouvoir continuer.",
"Plus moyen de sortir maintenant. Tu dois refaire le niveau.",
"Tu dois appuyer sur la longue flèche du clavier.",
"Les Hommes l'appellent touche retour.",
"Oui, c'est bien plus marrant en Tchèque.",
"L'option redémarrage se trouve aussi dans le panneau de contrôle.",
"Si tu penses qu'il est préférable de refaire le niveau, pas de souci.",
"Tu n'as qu'à appuyer sur la longue flèche du clavier.",
"Je suis un lourd cylindre en acier. Il n'est pas facile de me déplacer. Un petit poisson comme l'orange n'a aucune chance. Je peux l'écraser sans peine.",
}

l1icantgetthrough={"Je ne peux pas sortir d'ici. Ce cylindre en acier bloque le chemin.",
"Je ne pas bouger ce cylindre.",
"Peux-tu m'aider ?",
"Pas de problème...",
}
l1wowyoumovedit={"Ouah, Tu le déplaces ! Je ne pourrais jamais faire ça !",
"Merci, maintenant je peux partir avec toi.",
}

l1damnit={"Malheur, je ne peux pas passer.",
"J'ai dû prendre du poids.",
"Peux-tu pousser un petit peu la chaise sur la gauche ?",
}
