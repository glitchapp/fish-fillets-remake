l31={"Ça c'est une pièce bizarre.",
"C'est vraiment une pièce inhabituelle.",
"Le poste d'assistant pour la coordination du design a été créé à cause de ce niveau. Ainsi l'auteur a son propre mérite.",
"Ca Suffit avec l'équipe du jeu et maintenant bossons un peu.",
"Quel étrange corail.",
"C'est un corail très particulier.",
"Et à quoi est-il suspendu ?",
"Je ne sais pas. Mais nous devons le faire tomber de toute façon.",
"Je ne sais pas. Est-ce que nous devons le faire tomber ?",
"Comment le corail a-t-il pu prendre une forme si bizarre.",
"On l'a soigneusement fait pousser pour les jeux de logique.",
"Pourquoi veux-tu faire tomber ce corail, après tout ?",
"Aucune idée. Demande au joueur.",
"Escargot content, escargot heureux, sors tes cornes...",
"Arrête ça ! Comme si tu ne savais pas qu'aucun objet ne bouge si nous ne l'avons pas poussé avant !",
"Mais je peux essayer, non ? Escargot content, escargot heureux...",
"Arrête ! Ça me hérisse les écailles.",
"N'écoute pas alors. Escargot content, escargot heureux, sors tes...",
"Arrête ça ! Ou je te lâche ce corail sur la tête !",
}
