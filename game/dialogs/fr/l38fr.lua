l38={"Une autre belle pagaille.",
"Qu'est-ce que c'est que cet endroit ?",
"Ouah, quel endroit!",
"Où l'Homme - le Poisson - peut-il aller en grimpant aux toilettes ?",
"Ça ressemble à un douillet petit dépotoir.",
"Regarde sur quel genre de choses les gens peuvent tirer la chasse.",
"Je m'attendais à pire en entrant.",
"Tu peux trouver beaucoup de choses bizarres dans un tel dépotoir.",
"Penses-tu que c'est l'ordinateur qui est misé dans ce jeu ?",
"Sûrement pas ! Ce n'est pas un ordinateur multimédia puissant. Ce n'est qu'un XT avec écran 12 pouces.",
"Celui qui résoudra, je veux dire a résolu, recevra, je veux dire a reçu, un ordinateur MMX avec une carte vidéo 3Dfx, beaucoup de RAM, un énorme disque dur...",
"Et pourquoi faire ? Je connais beaucoup de jeux aussi bons que celui-là qui peuvent tourner sur ce XT.",
"Chut !",
"Une idée : Se pourrait-il que ce soit l'ordinateur que nous recherchons ?",
"Ça se pourrait.",
"Qu'est que nous attendons alors ? Récupérons les données sur la disquette et partons.",
"Ce n'est pas aussi simple que ça. Les données sont sûrement cachées. Nous devons entrer à l'intérieur.",
"Que penses-tu d'utiliser cette ouverture ?",
"C'est un lecteur de disquettes.",
"Je dois arriver à l'arrière du lecteur.",
"Pose cette visse et viens m'aider.",
"Oh, quelle vieille machine. Si grande, si lourde et sûrement très lente.",
"Et de plus juste monoprocesseur.",
}
