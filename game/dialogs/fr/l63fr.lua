l63={"Cette chauve-souris doit être terriblement forte.",
"Et bien, oui.",
"Cette créature rouge est un poil étrange.",
"C'est peut-être vrai, mais je pense que nous allons en avoir besoin.",
"C'est trop étroit pour moi. Tu vas devoir te débrouiller seule.",
"Ce poisson blanc est un formidable obstacle.",
"Un poisson ? Je croyais que c'était juste une pierre.",
"Je pense que nous devrions chercher le Graal ailleurs...",
"Ne te bile pas. Nous allons trouver la solution en un rien de temps.",
"Je ne suis pas si sûr.",
"Tu vois ? Tu y est.",
"Qu'est ce que ce monstre qui change de couleur ?",
"C'est une abomination.",
"C'est une offense à la Nature.",
"C'est un poisson-marteau tape-à-l'oeil.",
"C'est un repoussant lustré.",
"C'est un poisson politicien.",
"Cette chauve-souris a quelque chose d'étrange.",
"C'est une chauve-souris très flegmatique.",
"C'est une chauve-souris empaillée.",
"C'est une sculpture de chauve-souris.",
"C'est une stalagmite en forme de chauve-souris.",
"C'est une simple stalagmite.",
"C'est seulement un morceau de pierre ordinaire.",
"Je pense que tu vas devoir utiliser ce 'monstre'.",
"Je pense que tu vas devoir surmonter ton dégoût vis-à-vis de cette 'abomination'.",
}
