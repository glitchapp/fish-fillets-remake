l30={"Je n'ai jamais vu des coraux si violets.",
"Je n'aime pas le corail violet.",
"J'aime le corail violet.",
"Ils me plaisent bien.",
"J'espère qu'il y aura du corail plus intéressant dans les prochains niveaux.",
"Qui sait.",
"Nous devons soulever cet acier...",
"Salut, le crabe!",
"N'allons-nous pas remonter le moral de ce crabe un petit peu ?",
"Ignore-le.",
"Laisse ce crabe tranquille.",
"Ne peux-tu pas voir comme il est nerveux ?",
"Essaie de te passer de ce crabe nerveux.",
"Ce crabe pourrait être utile.",
"Ce crabe peut sûrement nous aider à trouver la solution.",
}

l30crab={"Qui me réveille ?",
"Laissez moi, je dors ici !",
"Que voulez-vous de moi ?!?",
"Ne me touchez pas !",
}

l30crabups={
"Oups.",
}
