l24={"Ça serait plus facile si vous...",
"Ce serait mieux si vous...",
"Ça vous aiderait si vous...",
"J'ai une idée. Si vous...",
"Désolé d'interrompre, mais si vous...",
"Détruisez un mur.",
"Enlevez quelques pierres du mur et bouchez les trous avec.",
"Bouchez ces trous avec quelque chose, ainsi cet hameçon ne s'y coincera plus.",
"Jette ce pilier par la sortie.",
"Faites attention aux clous.",
"Réarrangez les objets ainsi vous pourrez nager hors du tableau.",
"Trouvez la solution et allez au niveau suivant.",
"Commencez à réfléchir plus sérieusement.",
"Nagez à travers ce trou, sur la gauche.",

"Tu peux te mettre tes conseils...",
"Nous le savons parfaitement.",
"Oublie moi.",
"Ma patience est à bout.",
"Arrgh... Je vais le casser en petits morceaux.",
"Ignore-le, laisse-le causer.",
"Réfléchissons.",
"Ce doit être quelque indice, à nouveau.",
"Dommage que je n'aie pas d'oreilles. Je pourrais les boucher.",
"J'y perds mes branchies avec ces absurdités.",

}
