l47={"Tu vois ? Ce navire possède plusieurs canons. Ce devait être un navire de guerre. L'odeur acre de la poudre semble flotter dans l'air...",
"Tu veux dire dans l'eau.",
"À qui était ce navire ? Peut-être à l'amiral Nelson... ou au capitaine Hornblower.",
"Non, je pense que tu te trompes de siècle.",
"Je sens que nous allons avoir besoin de cette épée.",
"Toi et tes intuitions. De plus, nous sommes pas censés donner des indices.",
"C'était un peu trop simple, non ?",
"Est-il possible que ce soit si simple ?",
"Il doit y avoir une erreur.",
}
