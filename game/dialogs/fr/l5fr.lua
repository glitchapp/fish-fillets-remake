l5={"Quelque chose est encore tombé ici.",
"C'est une sorte de coquillage. Nous devons probablement encore lui faire monter les escaliers.",
"Je comprends maintenant : 'le coquillage rencontre la plante'. Un peu comme la rencontre d'un fer à repasser et d'une machine à coudre sur une table d'opération.",
"Je suis désolée, je me suis laissée emporter.",
"Désolée, j'étais ailleurs.",
"Où vais-je avec ce coquillage ?",
}
