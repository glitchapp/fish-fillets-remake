l3={"Tu connais déjà les règles.", 
"Maintenant tu peux t'exercer. ",
"Nous te donnerons quelques astuces de temps en temps.",
"Je ne dois pas nager vers le bas.", 
"Et je ne peux pas plus monter.",
"Hé, range les objets correctement. ",
"Fais attention !",
"",
}


l3icantmove={"Je ne peux pas bouger ça. C'est en acier",}

l3comeback={"Reviens, tu ne peux pas aller plus loin !",}

l3thisistricky={"Hum... C'est un piège.,",
"Si nous poussons le miel, nous serons écrasés par le pot de confiture d'abricot.",
"Est-ce qu'on ne pourrait pas le pousser avec cette hache ?",
"C'est une bonne idée.",
"Maintenant je ne peux pas prendre la hache, et le pot nous bloque toujours.",
"Tu ne dois pas pousser la hache !",
"Si tu peux pousser cette boîte jusqu'à moi, je ferai glisser la hache dessus et ce sera bon.",}

l3nowicanottake={"Maintenant je ne peux pas prendre la hache, et le pot nous bloque toujours.",}
l3youmust={"Tu ne dois pas pousser la hache !",}
l3thislooksrealbad={"Ça se présente mal. Tu vas devoir tout recommencer.",}
l3ifyoucanpush={"Si tu peux pousser cette boîte jusqu'à moi, je ferai glisser la hache dessus et ce sera bon.",}

l3ifyousolvesomespecific={"Quand tu résous une partie tu peux sauver la position.",
"Mais seulement si tu es sûr de ta position.",
"Rappel toi ce que nous avons dit à propos des sauvegardes.",}

l3youarestandinginmyway={"Tu es sur mon passage là. Cache toi ailleurs.",
"Si tu le lâches et que je reste ici, je ne pourrai jamais sortir.",
"Ici ? J'ai un peu peur. Pourrai-je sortir ?",
"Un peu de patience, tu verras.",}

l3ishoulndtbe={"Je ne devrais pas y penser.",}

l3wewillgiveyouahint={"Nous allons te donner une astuce maintenant. Tu dois pousser ce livre sur la gauche tout au bout.",
"Ainsi il supportera le cylindre d'acier quand je le pousserai.",
"Mais nous n'allons pas te dire comment faire.",
"Et si je passais par le haut ?",
"Chut. N'en dis pas trop.",}

l3justremember={"Un petit rappel, si nous ne pouvons pas pousser des objets que nous portons, nous pouvons néanmoins les faire tomber ou les pousser sur une structure solide.",}

l3thingslikethat={"Ces situations arrivent parfois.",}

l3sometimes={"De temps à autre, tu devras prévoir de (très?, nombreux mouvements à l'avance.",
"Et souvent tu devras tout recommencer.",}

l3weadmitwedid={"Soyons honnêtes : nous l'avons fait exprès.",
"Allez, recommence le niveau - Sans aide cette fois.",}
