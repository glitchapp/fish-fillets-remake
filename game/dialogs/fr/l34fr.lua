l34={"Regarde tous ces homards...",
"Ils dorment depuis des milliers d'années... dans un calme éternel...",
"... et le gouvernement essaie de cacher ça.",
"... rien ne peut les réveiller...",
"Sauf si nous réveillons cette pieuvre.",
"Écoute, tout est si calme et si silencieux.",
"Mais les anémones bougent.",
"Peut-être qu'elles sont à l'agonie.",
"J'y trouve un certain symbolisme.",
"C'est intéressant. Ce type de corail ne pousse normalement que dans le triangles des Bermudes.",
"Est-ce que tu savais que certains types de corail sont aussi intelligents qu'un poisson ?",
"Attention, un grand nombre de coraux sont toxiques.",
"Penses-tu qu'il existe une 'âme corallienne' ?",
"As-tu la sensation que ce corail nous observe ?",
"Je me demande pourquoi le corail a une forme si bizarre.",
"C'est une histoire de nombre, c'est ça ?",
"Ouais, C'est un bon joueur.",
"C'est rigolo. Et si nous le poussons encore ?",
"As-tu vu ce que faisaient les homards ?",
"Et comment ces anémones dansaient ?",
"Et bien tu sais, on ne s'amuse pas tant que ça ici au fond de l'océan.",
"Il est bon, n'est-ce pas ?",
"Pas mal pour un poulpe...",
"Crois-tu qu'il connaisse d'autres morceaux ?",
"Ne penses-tu pas que nous en avons déjà assez ?",
"J'en suis malade...",
"J'en suis malade et fatigué...",
"Ça me tape sur les branchies.",
"Je ne peux pas bouger ça d'où je suis. Je dois essayer de le déplacer depuis le coté gauche.",
"Et bien, tu dois passer à travers les anémones.",
"Ce n'est pas comme ça que je le voyais...",
"Nous aurions dû d'abord boucher ce trou.",
}
