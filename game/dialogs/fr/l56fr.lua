l56={"Allume la lumière, MAINTENANT !",
"Qu'essaies-tu de faire ?",
"Arrête ça et allume la lumière.",
"Attends, j'essaie de trouver l'interrupteur.",
"Je ne peux pas. l'interrupteur est tombé.",
"Allume la lumière toi même, si tu es si maligne.",
"Où est-ce que tu es ?",
"Hello-o-o.",
"Dis quelque chose.",
"Ici.",
"Je suis là.",
"Je suis juste là.",
"Où es-tu, j'ai peur.",
"J'ai peur du noir.",
"S'il te plaît ne pars pas. Il fait si noir ici.",
"N'aie pas peur. Je suis là.",
"Arrête de geindre. Nous allons nous en sortir.",
"Il n'y a pas de quoi avoir peur.",
"J'espère qu'il ne va pas me donner un coup de pied.",
"Cette lumière clignotante me blesse les yeux.",
"Je n'imaginais pas être si contente de te revoir.",
"Finalement, la lumière est revenue.",
"Attention. Ça mord.",
"Regarde ! Ce doit être... ROBOCHIEN !",
"Pardon ? ÇA robochien ?!?",
"Et bien, maintenant je comprends tout.",
"Y compris la crise des missiles de Cuba et les prophéties de Nostradamus.",
"Y compris le meurtre de John Lennon et la météorite de Tunguza.",
"Y compris l'explosion de la navette Challenger et le mythe du King.",
"Y compris la catastrophe de TChernobyl et l'épave de l'arche de Noé.",
"Maintenant je peux voir le côté rationnel de ce jeu.",
"Et aussi le sens de la vie, de l'univers et du reste.",
"CLIC",
"CLIC",
"Ne les écoute pas, joueur. Ils se moquent de toi.",
"Ne les écoute pas, joueur. Ils essaient d'être drôles.",
"Ne les écoute pas, joueur. Ils essaient de t'embobiner.",
"Je ne suis qu'un simple jouet cybernétique.",
"Même si au départ je devais être un scooter.",
"Même s'il était prévu que je sois une tondeuse à gazon.",
"Même s'il était prévu que je sois une station orbitale.",
"Même si j'aurais dû être du gâteau.",
"Et je n'ai visité Cuba qu'une seule fois.",
"Et tout le monde sait que quelqu'un d'autre l'a tué, ils ne peuvent pas tout prouver.",
"Et qui se soucie de quelques soldats.",
"Et ils m'ont dit que ce levier de réglage n'était pas important.",
}
