l14={"Quel est ce drôle de navire ?",
"C'est l'épave de l'avion civil LC-10 Lemura.",
"Ca, c'est l'épave de l' avion civil Atlantobus.",
"Et ici, de l'avion civil Poséidon 737.",
"Vois-tu cet oeil ? Témoin silencieux de la tragédie... Quelqu'un avait confiance en cet avion - et cet oeil de verre est la seule chose qu'il a laissée.",
"Ce n'est pas un oeil de verre mais un gyroscope. Au moins dans ce niveau.",
"Des sièges. Pourquoi y en a-t-il autant ici ?",
"Soit reconnaissant. pourrions nous sortir sans eux ?",
}
