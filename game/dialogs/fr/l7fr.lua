l7={"Mon dieu, l'impact a causé cet affaissement.",
"Je ne peux pas passer dessous, nous devrons trouver un autre chemin pour sortir.",
"Ce n'est pas la voie à suivre.",
"Je n'y arriverai pas tout seul.",
"J'aurais sans doute dû soulever ce coquillage d'abord.",
"Peut-être devrions-nous permuter ces objets.",
"Un matelas. La meilleure chose que l'on peut trouver sous l'eau.",

}

l7part2={"Maintenant nous pouvons faire tomber l'escargot sur le matelas.",
"Ça va être difficile de récupérer cet escargot.",
"Finalement ça y est.",
"Pauvre escargot...",
"Nous devrions fouiller le récif de corail.",
"Il va y avoir beaucoup de choses intéressantes à étudier là bas.",
"N'aurons-nous pas besoin d'un microscope pour étudier le corail ?",
"Oui ils sont minuscules. Mais il peut y avoir d'autres formes de vie.",
"Les tortues coralliennes, par exemple.",
"De plus je crois que les bathyscaphes sont équipés de microscopes.",
}
