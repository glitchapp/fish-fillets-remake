l38={"Αυτή είναι μια ακόμα ωραία ζημιά.",
"Τι είδους μέρος είναι αυτό;",
"Ω έλα τι μέρος!",
"Πού μπορεί ο άνθρωπος - εννοώ ο ψάρι - να περάσει ανεβαίνοντας μέσα από ένα τουαλέτα;",
"Φαίνεται σαν ένα ζεστό μικρό χαλαρωτικό.",
"Δες, τι είδους πράγματα έχουν περάσει οι άνθρωποι από την τουαλέτα.",
"Νόμιζα ότι θα ήταν χειρότερα όταν ανέβαινα μέσα.",
"Μπορείς να βρεις πολλά παράξενα πράγματα σε ένα τέτοιο χαλαρωτικό.",
"Νομίζεις ότι αυτός ο υπολογιστής είναι σε κίνδυνο σε αυτό το παιχνίδι;",
"Σίγουρα όχι! Αυτός δεν είναι ένας ισχυρός πολυμέσος υπολογιστής. Αυτό είναι απλώς ένα μηχάνημα XT με οθόνη δώδεκα ιντσών.",
"Αυτός που λύνει, εννοώ όποιος έχει λύσει αυτό, θα λάβει, εννοώ θα έχει λάβει, έναν υπολογιστή βασισμένο στην τεχνολογία MMX με μια κάρτα 3Dfx, πολύ μνήμη RAM, έναν τεράστιο σκληρό δίσκο...",
"Και γιατί; Εγώ για ένα γνωστό παιχνίδι ξέρω πολλά που είναι σχεδόν τόσο καλά όσο αυτό και μπορούν να τρέξουν εύκολα σε αυτό το μηχάνημα XT.",
"Σσσσ!",
"Ψστ, δες, απλώς μια ιδέα: Μήπως αυτός είναι ο υπολογιστής που ψάχνουμε;",
"Αυτό μπορεί να είναι αυτό.",
"Τι περιμένουμε λοιπόν; Βάλε αυτά τα ύποπτα δεδομένα στο δισκέτο και πάμε.",
"Δεν θα είναι τόσο εύκολο. Τα δεδομένα σίγουρα θα είναι κρυμμένα κάπου. Πρέπει να μπούμε μέσα.",
"Και για να περάσουμε μέσα από αυτήν την ανοίγματα;",
"Αυτό είναι μια θήκη για δισκέτα.",
"Πρέπει να πάω στο πίσω μέρος της.",
"Βάλε κάτω αυτό το ανοιχτήρι και έλα να με βοηθήσεις.",
"Ω μπράβο, τι ακατάλληλο μηχάνημα. Τόσο μεγάλο, τόσο βαρύ και σίγουρα τόσο αργό.",
"Και επίσης μόνο μονοφωνικό.",
}
