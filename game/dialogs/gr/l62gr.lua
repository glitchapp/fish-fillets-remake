l62={"Από τον θησαυρό στην αποθήκη των κηροπήγιων. Αυτό είναι πρόοδος.",
"Πρέπει να βλέπεις τη θετική πλευρά. Δεν υπάρχει τίποτα που να μοιάζει με Άγιο Δίσκο εδώ.",
"Φαίνεται ότι θα πρέπει να περάσω από αυτό το φρικτό λαβύρινθο.",
"Το παχύ σώμα σου. Πάντα μας εμποδίζει.",
"Αφήστε με μόνο, εσύ αδύναμη βούβαλη.",
"Σταμάτα να λες μαλακίες, διαστροφέ μάζα μυών και κλιμάκων!",
"Νιώθω σαν να είμαι στην αποθήκη κεραμικής.",
"Εννοείς αποθήκη αμφορέων.",
"Όχι, πρέπει να είναι αποθήκη αμφορέων.",
"Ξέρεις τι; Νομίζω ότι αυτή η αποστολή με βοήθησε πραγματικά.",
"Και γιατί;",
"Έμαθα να καταλαβαίνω ότι τα κοσμήματα και το χρυσό είναι απλά φτηνά κομπολόια.",
"Δεν αντέχω άλλο αυτή την κατρακύλα!"}

--"Μπορείς να επανεκκινήσεις το επίπεδο, παρακαλώ; Φαίνεται ότι δεν τα πηγαίνουμε πολύ καλά τώρα."
