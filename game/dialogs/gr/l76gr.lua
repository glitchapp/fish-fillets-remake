l76={"Δες, όπως είπα.",
"Τι είπες;",
"Ότι κάποιος έχτισε ένα κλειδαριά στον λίθο.",
"Οπότε μιλούσες μπαρούφες. Δεν υπάρχει κλειδαριά στον λίθο.",
"Στο προηγούμενο επίπεδο υπήρχε.",
"Τι σημασία έχει αυτό σε αυτό το επίπεδο;",
"Εδώ υπάρχουν τέσσερις κλειδαριές και τέσσερα κλειδιά.",
"Αχα.",
"Πλησιάζουμε στον δημιουργό της κλειδαριάς στο προηγούμενο επίπεδο.",
"Ω; Πού είναι αυτός;",
"Δεν ξέρω.",
"Πώς λοιπόν ξέρεις ότι πλησιάζουμε;",
"Κοίτα γύρω, τι βλέπεις;",
"Ένα τοίχο από τούβλα.",
"Χάλυβα.",
"Εσένα.",
"Νερό.",
"Ένα έξοδο στην οποία δεν μπορούμε να φτάσουμε.",
"Σκοτάδι. Αλλά μόνο όταν κλείνω τα μάτια μου.",
"Τι άλλο;",
"Αποκατασταθήκαμε.",
"Ποιο από τα κλειδιά μπορεί να ταιριάξει στην κλειδαριά;",
"Όλα ή κανένα. Όλα μοιάζουν ίδια.",
"Τουλάχιστον τα κλειδιά δεν είναι κολλημένα στο έδαφος.",
"Γιατί δεν κολυμπάς πολύ δυνατά προς τον τοίχο; Μπορεί να καταρρεύσει.",
"Γιατί δεν ψάχνεις για μια τρύπα και το παίρνεις από εκεί;",
"Ίσως ο παίκτης μπορούσε να τροποποιήσει αυτό το πρόγραμμα για να γίνει αυτό δυνατό;",
"Αυτό θα ήταν απάτη.",
"Αυτό θα ήταν βαρετό.",
"Αυτό είναι αλήθεια.",
"Αλλά το επίπεδο θα λυνόταν.",
"Νομίζεις ότι αυτά τα κλειδιά θα ήταν χρήσιμα στο προηγούμενο επίπεδο;",
"Φυσικά όχι.",
"Γιατί όχι;",
"Σκέψου λίγο.",
"Φυσικά! Χρειαζόμασταν ένα κλειδί με τρία δόντια.",
"Φυσικά! Σε αυτά τα κλειδιά τα δόντια έχουν ανισόμηκη μήκος.",
"Φυσικά! Εδώ υπάρχουν τέσσερα κλειδιά αλλά εκεί υπήρχε μόνο μια κλειδαριά.",
"Όχι. Η κλειδαριά ήταν πολύ μεγαλύτερη.",
"Το βλέπω.",
"Αν όλες οι κλειδαριές εξαφανιζόντουσαν, θα μπορούσαμε να φύγουμε πιο εύκολα.",
"Αυτό είναι το νόημα των κλειδαριών.",
"Θα έπρεπε να πάρουμε ένα κλειδί μαζί μας για ένα μεταγενέστερο επίπεδο.",
"Καλή ιδέα.",
"Γιατί δεν απλά το ξεκλειδώνουμε;",
"Εντάξει.",
"",
"",
"",
"",
}
