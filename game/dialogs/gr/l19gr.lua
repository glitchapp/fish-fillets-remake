l19={"Έχω την υποψία ότι πρόκειται να ανακαλύψουμε κάτι φοβερό.",
"Πάντα το ήξερα: οι θεοί πρέπει να είναι τρελοί.",
"Ο Θεός είναι τρελός και ολόκληρος ο κόσμος είναι το παιχνίδι του.",
"Νομίζω ότι έχετε δίκιο.",
"Φοβάμαι ότι έχετε δίκιο.",
"Ναι, είναι σοκαριστικό.",
"Τι πρέπει να κάνουμε γι' αυτό;",
"Δεν μπορούμε να το αφήσουμε έτσι. Πρέπει να τους καταπιάσουμε.",
"Τι πρέπει να κάνουμε;",
"Βάλτε τους στην τρελή... εννοώ, στην εγκλειστική μονάδα του FDTO.",
"Έχετε δίκιο. Να πάρουμε και τους δύο;",
"Φυσικά. Αν αφήσουμε έναν από αυτούς εδώ, μπορεί να του έρθει κάποια τρελή ιδέα. Μπορεί να προσπαθήσει να παίξει γκολφ, για παράδειγμα.",
"Με αυτό το ρόπαλο δύσκολα. Νομίζω ότι θα είμαστε ευτυχείς αν καταφέρουμε να πάρουμε έναν. Ο άλλος θα είναι ανένοχος τότε.",
"Καλά. Ποιον από τους δύο;",
"Εκείνον τον κίτρινο.",
"Εκείνον τον μπλε.",
"Εκείνον τον κίτρινο.",
"Εκείνον τον μπλε.",
"Αυτό θα το αφήσουμε στον παίκτη.",
"Θα το αφήσουμε στον παίκτη.",
"Κοίτα αυτό το ρόπαλο: συνέβη κάτι φοβερό σε αυτό.",
"Ένα πλάσμα πιθανόν το μάσησε.",
"Η μητέρα είχε δίκιο. Η παράνοια είναι η ρίζα κάθε κακού.",
"Αυτό είναι αλήθεια. Θα ήταν πολύ πιο εύκολο αν αυτή η ρακέτα του τένις ήταν κάπου αλλού.",
"Επίσης, παρατηρήσατε ότι η πραγματική μπάλα γκολφ είναι στην πάνω αριστερή γωνία ενώ αυτό το πράγμα στην κάτω δεξιά γωνία είναι μια μπάλα κρίκετ;",
"Υπάρχει κάποιο κρυμμένο σύμβολο;",
"Ποιος ξέρει.",
"Νόμιζα ότι ήταν μια μπάλα τένις εκεί πάνω.",
"Είναι δύσκολο να πεις με αυτήν την ανάλυση οθόνης.",
"Αυτήν τη φορά, στόχος μας είναι να βγάλουμε έναν από αυτούς τους θεούς.",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Πλήγη!",
"Πλήγη!",
"Πλήγη!",
"Πλήγη!",
"Ναυάγησε!",
"Ναυάγησε!",
"Ναυάγησε!",
"Κέρδισα, κουνελάκι των θαλασσών!!!",
"Αρχίζω!",
"Εντάξει!",
"Να παίξουμε άλλο ένα;",
"Δεν κάνω φιγούρες.",
"Ήταν ένα λάθος, πιθανόν...",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Αστοχία!",
"Πλήγη!",
"Πλήγη!",
"Πλήγη!",
"Πλήγη!",
"Ναυάγησε!",
"Ναυάγησε!",
"Ναυάγησε!",
"Χε, χε, χε... Κέρδισα!",
"Εντάξει!",
"Ας το ξαναδοκιμάσουμε;",
"Έχεις ήδη δοκιμάσει αυτό!",
"Έχεις ήδη πει αυτό!",
"Δεν μπορεί να είναι αστοχία σε αυτό το σημείο.",
"Κάνεις ζαβολιές!!!",
"Το δοκίμασα - και είπες αστοχία!",
"",
"Α%1.",
"Β%1.",
"Γ%1.",
"Δ%1.",
"Ε%1.",
"Φ%1.",
"Γ%1.",
"Η%1.",
"Ι%1.",
"Τ%1.",
"",
"Α%1.",
"Β%1.",
"Γ%1.",
"Δ%1.",
"Ε%1.",
"Φ%1.",
"Γ%1.",
"Η%1.",
"Ι%1.",
"Τ%1.",
}

l19end={"Η κατάληψη του καταδικασμένου που σας στέλνουμε είναι ένας θεός των ωκεανών.",
"Εκτός από τις εξαφανίσεις αεροπλάνων και πλοίων (η ονομαζόμενη Υπόθεση Ναυμαχίας της Θαλάσσης)",
"είναι υπεύθυνος και για τα άλλα εγκλήματα,",
"τη μετακίνηση των ηπείρων (κωδική ονομασία Δρόμος, ηπείρωση, δρόμος)",
"και τη μετεωρίτη στην Τουγκουζκα (κωδική ονομασία Jumping Jack) είναι ανάμεσά τους.",
"Καταφέραμε να παρέμβουμε ακριβώς στη στιγμή της τελευταίας στιγμής:",
"βρήκαμε ένα ολοκαίνουργιο, ακόμα αποκομμένο κουτί ",
"με ένα επιτραπέζιο παιχνίδι που ονομάζεται STAR WARS στο σπίτι του καταδικασμένου.",
"Μπορείτε να βρείτε τα αρχεία των θαλάσσιων μαχών του στο συνημμένο.",
}
