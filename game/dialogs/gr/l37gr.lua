l37={"Το βλέπεις; Πρέπει να είναι αυτή η τηλεπαθητική χελώνα.",
"Ναι, αυτή είναι. Η τηλεπαθητική χελώνα.",
"Καλά, δεν ξέρω. Με βάση εκείνες τις φωτογραφίες, μπορεί να είναι αυτή.",
"Δεν είμαι σίγουρος... τι γίνεται με αυτές τις τηλεπαθητικές δυνάμεις;",
"Σε κάθε περίπτωση, πρέπει να τη βγάλουμε έξω από το δωμάτιο.",
"Ας βγάλουμε αυτήν τη χελώνα.",
"Τι κάνεις;",
"Τι έχεις κάνει;",
"Τι σημαίνει αυτό;",
"Πού κολυμπάς;",
"Τι πρέπει να σημαίνει;",
"Τι συμβαίνει με εσένα;",
"Δεν ξέρω...",
"Δεν μπορώ να το καταλάβω...",
"Δεν ξέρω τι μου συμβαίνει...",
"Δεν ήθελα...",
"Τι μας συμβαίνει;",
"Τι μπορεί να σημαίνει αυτό;",
"Πρέπει να είναι αυτή η χελώνα!",
"Πρέπει να είναι αυτή η χελώνα!",
"Έτσι δεν μπορεί να υπάρχουν αμφιβολίες πλέον - αυτή είναι. Αυτήν που ψάχνουμε!",
"Χωρίς αμφιβολία, αυτή είναι η χελώνα που ψάχνουμε!",
"Εκείνο το θηρίο!",
"Σταμάτα, καταραμένε!",
"Είναι αρκετό!",
"Το απολαμβάνεις;",
"Τόσες πολλές παράξενες μορφές υπάρχουν μόνο σε ένα κοραλλιογενές ύφαλο.",
"Αυτά τα κοράλλια είναι θαύματα της Μητέρας Φύσης.",
"Από πού προέρχονται τόσες παράξενες μορφές;",
"Είναι εύκολο - οι σχεδιαστές δημιουργούν προβληματικά προβλήματα και αφήνουν το σχέδιο στους γραφίστες.",
"Ο στόχος μας σε αυτό το δωμάτιο είναι να βγάλουμε αυτήν τη χελώνα έξω.",
}

l37end={"Πέτυχαν να αιχμαλωτίσουν μια επικίνδυνη κοραλλιογενή αλλά εντελώς ακίνδυνη χελώνα.",
"Συνιστούμε ακραία μέτρα ασφαλείας που δεν είναι απαραίτητα να προστατευτούν, είναι πραγματικά φιλική.",
"Μπορεί όχι μόνο να διαβάζει πραγματικά πολλές φιλικές σκέψεις, αλλά τις επηρεάζει - αυτά είναι παπαριές.",
"Και μας δάγκωσε, επίσης, και τι;",
"Με κάθε τρόπο, αφήστε την ξεμπερδεμένη, μόνο αν θέλει να κάνει χωρίς να την ξεμπερδεύετε.",
}
