function Obey.lev2()

   
loadcorrespondentfonts()
  
 Talkies.say( smallfisht,
    {
      l2[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
  
   Talkies.say( bigfisht,
    { 
       l2[2],
       l2[3],
       l2[4],
       l2[5],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2[6],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
     Talkies.say( bigfisht,
    { 
       l2[7],
       l2[8],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
end

function Obey.lev2thedisk()
 
  --avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.png")
  --avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.png")
  
  
	  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then	smallfisht="小鱼" bigfisht="大鱼" 
elseif language=="en" then smallfisht="small fish" bigfisht="Big fish"
elseif language=="es" then smallfisht="pez pequeño" bigfish="pez grande"
end
  
  
    
       Talkies.say( smallfisht,
    {

      l2thediskself[1],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[2],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {

      l2thediskself[2],
      l2thediskself[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[4],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[5],
      l2thediskself[6],
      l2thediskself[7],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[8],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[9],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[10],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[11],
      l2thediskself[12],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[13],
      l2thediskself[14],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[15],
      l2thediskself[16],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[17],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[18],
      l2thediskself[19],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[20],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[21],
      l2thediskself[22],
      l2thediskself[23],
      l2thediskself[24],
      l2thediskself[25],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[26],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( smallfisht,
    {
      "...",
      l2thediskself[27],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( bigfisht,
    { 
      l2thediskself[28],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
end

function Obey.lev2briefcase()
 
  --avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.png")
  --avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.png")
  
	  if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then	smallfisht="小鱼" bigfisht="大鱼" 
elseif language=="en" then smallfisht="small fish" bigfisht="Big fish"
elseif language=="es" then smallfisht="pez pequeño" bigfish="pez grande"
end

 Talkies.say( "FDTO",
    {
      l2briefcasetalkie[1],
       l2briefcasetalkie[2],
       l2briefcasetalkie[3],
       l2briefcasetalkie[4],
       l2briefcasetalkie[5],
       l2briefcasetalkie[6],
       l2briefcasetalkie[7],
       l2briefcasetalkie[8],
       l2briefcasetalkie[9],
       l2briefcasetalkie[10],
       l2briefcasetalkie[11],
       l2briefcasetalkie[12],
       l2briefcasetalkie[13],
       l2briefcasetalkie[14],
       l2briefcasetalkie[15],
       l2briefcasetalkie[16],
       l2briefcasetalkie[17],
       l2briefcasetalkie[18],
       l2briefcasetalkie[19],
       l2briefcasetalkie[20],
       l2briefcasetalkie[21],
       l2briefcasetalkie[22],
       l2briefcasetalkie[23],
       l2briefcasetalkie[24],
       l2briefcasetalkie[25],
       l2briefcasetalkie[26],
       l2briefcasetalkie[27],
       l2briefcasetalkie[28],
       l2briefcasetalkie[29],
       l2briefcasetalkie[30],
       
       
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
 
end
