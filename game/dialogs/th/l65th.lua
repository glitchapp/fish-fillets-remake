l65={"ดังนั้นนี่คือรูปแบบต้นแบบของเกมที่ประสบความสำเร็จมากที่สุดในทุกยุค",
"ความเคารพอันลึกซึ้งต่อความทรงจำจึงป้องกันฉันไม่ให้บอกว่ามันมีลักษณะเป็นอย่างไร...",
"แต่ทุกคนสามารถเห็นได้ด้วยตนเอง",
"คุณรู้ว่าฉันมีความคิดอะไรอยู่ในใจหรือเปล่า?",
"อย่างไร?",
"ฉันคิดว่าเราสามารถทำเกมเตรียมที่ดีกว่าห้องนี้",
"คุณหมายถึงอะไรที่ดีกว่า?",
"อย่างน้อยคุณจะไม่ต้องย้ายชิ้นส่วนทั้งหมดไปทางขวา",
"ความจริงคือเรามีวัตถุที่เหมาะสมมากมายที่นี่",
"ฉันคิดว่าหน้าที่เล็กน้อยของโค้ดอาจทำให้ผู้เล่นมีโอกาสสนุกกับเกมเทตริสต้นแบบบางส่วน",
"โอเค เริ่มทำโปรแกรมได้เลย!",
"ระวังที่จะไม่ทำร้ายเรา",
"เราจะต้องจัดเรียงชิ้นส่วนเหล่านี้ได้อย่างมีประสิทธิภาพมากขึ้นบ้าง บางที",
"ฮมม... ฉันควรจัดเรียงมันได้ดีกว่านี้",
}
