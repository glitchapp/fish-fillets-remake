l59={"คุณเห็นมั้ยว่ามีม้าน้ำตัวนั้น?",
"มันถูกกีดขวางด้วยเหยือกหินที่เต็มไปด้วย",
"มันเมากับเหยือกหินอยู่",
"ฉันสงสัยว่ามีอะไรเหลืออยู่ในนั้นหรือเปล่า",
"คุณอาจต้องไปตรวจดูด้วยตัวเองนะ",
"สุดท้ายแล้ว ฉันสามารถเห็นกะโหลกชนิดใหม่บ้างเเล้ว",
"คุณเห็นว่านกกระจอกหรือยัง? เขาคือเทพเจ้า Shelloxuatl ของเม็กซิโก",
"ดูเหมือนเป็นอยู่นะ",
"เหยือกหินเหล่านี้ตกลงมาช้ามากที่เหลือความอดทนไม่ไหว",
"อืมม อะไรเสียบ้างเนี่ย แต่ในที่สุดคุณไม่ใช่หมี แล้วก็เถอะ",
"อย่าลืมว่าเราอยู่ใต้น้ำ",
"ยักษ์! ผู้เขียนเกิดต้องทำอนิเมชั่นให้เราเห็นอะไรแบบนี้ได้",
"ทูเท็มนี้ดูดีมากในสายตาฉัน",
"กะโหลกนั้นดูเหมือนมีแสงออกมาอย่างแปลกตา",
"มันมีชีวิตอยู่หรือเป็นเวทมนตร์ของบางอย่าง?",
"",
"",
"",
}

l59skull_canyouall={"ทุกคนจะได้รับให้ใจกับเรื่องของตัวเองหน่อยได้ไหม? เรากำลังพักผ่อนอย่างสงบ",""}

l59skull_wellanother={"นอกจากนี้ก็ยังมีความไม่สบายใจอยู่ตลอดคืนเนื่องจากคนที่อยากรู้อะไร",""}

l59skull_welliwas={"ฉันกำลังหลับอยู่จนกระทั่งถูกปลุกขึ้นมาอย่างไม่เป็นมารยาท",""}

l59skull_anothernight={"อีกคืนหนึ่งที่เสียงหลับหลายของฉันถูกทำร้าย",""}
