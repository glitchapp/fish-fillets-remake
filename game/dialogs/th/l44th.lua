l44={"คนจากกรีนพีซควรมาเห็นสิ่งนี้",
"คนจาก Three Mile Island ควรมาเห็นสิ่งนี้",
"เราควรทำอะไรกับมัน?",
"เราต้องกำจัดพวกเขาทั้งหมด สิ่งที่น่าสะอิดสะเอียนเหล่านี้ไม่มีสิทธิ์ที่จะหายใจความเป็นเช่นเดียวกันกับเรา...อืม, น้ำทะเลเราคือคำตอบ",
"ฉันคิดว่าเราควรกำจัดสาเหตุไม่ใช่ผลลัพธ์",
"ดีกว่า, คุณอาจจะถูกต้อง",
"และสาเหตุนั้นน่าจะเป็นถังใหญ่ขนาดนี้แน่นอน เราต้องหาวิธีที่จะนำออก",
"ใช่ เรามาทำงานกันเถอะ เราสามารถทิ้งมันลงบนสวนหน้าของนายบี หลังจากนั้น",
"ฉันคิดว่าฉันกำลังเริ่มชอบพวกเขา",
"ถ้าพวกเขาไม่มาอยู่หน้าทางของเรามากกว่านี้ก็จะดี",
"ถ้าพวกเขาอยู่ที่ที่เราต้องการ",
"ชุดสุดท้ายของอสรพิษที่น่าสะอิดสะเอียน ฉันจะเสียใจเมื่อต้องออกไป",
"ไม่มีเวลาที่จะเกิดขึ้นในไม่ช้า",
"คุณจะมีเวลามากมายในการเพลิดเพลินกับพวกเขา",
"ถ้าเราเอาพวกเขาไปและเก็บไว้ในแอลกอฮอล์ เราสามารถสร้างงานแสดงคนประหลาดได้",
"และการเป็นสมาชิกของ FDTO นั้นไม่เพียงพอสำหรับคุณหรอก?",
"ฉันได้ยินทุกอย่าง!",
"ขอโทษนะ หัวหน้า ฉันไม่ได้ตั้งใจที่จะดูหมิ่น",
"คุณรู้มั้ยว่าผมเริ่มเข้าใจว่ามนุษย์ก็มีจิตวิญญาณ",
"อาจจะแม้ UFO ก็มีจิตวิญญาณหรือเปล่า?",
"ฉันคิดว่าระดับนี้จะเปลี่ยนเราตลอดกาล",
"เท้านั้นไม่เพียงแค่น่ารังเกียจ แต่ยังไม่รู้สิ่งอันน่าเสียดายที่มันทำ",
"มองที่สิ่งนั้นดีๆ มันยิ่งเสียหน้ายิ่งมากขึ้น",
"แต่มันก็ยังครอบครองความห่วงใยแม่มาบ้าง มองว่ามันดูแลลูกดีอยู่นี่นา",
"ใช่ มันเกือบทำให้มันตายด้วยเสาเหล็ก",
"ปลาคนนี้ดูคุ้นเคยกับฉัน",
"อาจจะอยู่ในรูปถ่ายจากโรงไฟฟ้า",
"ปูน้อยน่าสงสัย มีคีมมากมายและไม่มีหัว แม้ว่า...อาจจะดีกว่าเรา",
"ไม่มีคนคนเดียวที่จะเป็นคนเดียวกับเรานอกจากเป็ดสีเหลืองน้อยๆ",
"ครั้งนี้เป้าหมายของเราคือการดันถังออกไปพร้อมกับสิ่งเสียที่นั่น",
"แต่เราต้องทิ้งสิ่งมองไม่เห็นเหล่านี้ไว้ที่นี่ พวกเขาต้องไม่ทำให้พลังงานดีประชากรที่เป็นสุขได้รับความเสียหาย",
}

l44end={"ขอสวัสดิ์ที่มาจากเศษขยะถึงนายหัวหน้าที่เรารัก",
"ขอแสดงความนับถือ",
"เอเย่นต์",
"PS: ยินดีมากที่ได้อยู่ที่นี่ เราจัดการรังสรรค์และว่ายน้ำในทะเลมากเป็นอย่างมาก เรามีเพื่อนใหม่มากมาย",
}
