l34={"มองที่กุ้งมากมายเหล่านี้...",
"พวกเขาหลับนานหลายพันปี... ในความเงียบสงบที่ไม่มีที่สิ้นสุด...",
"...และรัฐบาลพยายามปกปิด",
"...ไม่มีอะไรสามารถปลุกพวกเขาขึ้นมาได้...",
"เว้นแต่เราเปิดใช้งานปลาหมึกนั่น",
"มองเอาทุกอย่างที่นี่อยู่เงียบสงบและเงียบสงบ",
"แต่ทางเพดานทะเลก็ยังคงเคลื่อนไหวอยู่",
"อาจเป็นเพราะพวกเขาอยู่ในช่วงสุดท้ายของชีวิต",
"ฉันพบสัญลักษณ์บางอย่างในนั้น",
"น่าสนใจนะ ประเภทของปะการังนี้มักเติบโตเฉพาะในสามเหลี่ยมเบอร์มิวด้า",
"คุณรู้หรือไม่ว่ามีปะการังบางประเภทที่มีความฉลาดเกือบเท่าปลา?",
"ระวังนะ ปะการังบางชนิดมีพิษ",
"คุณคิดว่ามีอะไรที่เรียกว่า 'วิญญาณปะการัง' หรือไม่?",
"คุณรู้สึกว่าปะการังเหล่านี้กำลังมองเราอยู่หรือเปล่า?",
"ฉันสงสัยว่าทำไมปะการังมีรูปร่างแปลกประหลาดเช่นนี้",
"มันมากเลยหน่ะ...",
"ใช่เขาเป็นนักเล่นที่ยอดเยี่ยม",
"เยี่ยมเลย อยากสัมผัสเขาอีกหรือไม่?",
"คุณเห็นว่ากุ้งมองอะไรอยู่เหรอ?",
"และปะการังเหล่านั้นเต้นรำได้เล่นด้วย?",
"ฉันรู้ว่า ลงที่ด้านล่างของมหาสมุทรเป็นสิ่งที่ไม่ค่อยมีความสนุก",
"เขาเก่งและเก่งด้วย?",
"ไม่แย่เลยสำหรับปลาหมึก...",
"คุณคิดว่าเขารู้เพลงอื่นๆอีกหรือเปล่า?",
"คุณคิดว่าเราพอได้แล้วหรือยัง?",
"ฉันเบื่อมากแล้ว...",
"ฉันเบื่อและเมื่อกว่านี้...",
"ฉันหมดความอดทนกับสิ่งนี้แล้ว",
"ฉันไม่สามารถเคลื่อนที่ได้จากที่ฉันอยู่ ฉันต้องพยายามเคลื่อนที่จากด้านซ้าย",
"แต่คุณต้องผ่านปะการังเหล่านั้น",
"ฉันไม่ได้คิดถึงเรื่องนั้น...",
"เราควรต้องปิดปากของช่องเล็กก่อน",
}
