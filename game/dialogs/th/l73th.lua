l73={"มันอยู่นั่นแล้ว!",
"อะไร?",
"ไม่ว่าจะเป็นใครก็ตาม ผมจะออกหรือคุณจะออก!",
"แล้วใครจะออก?",
"ควรจะเป็นผม",
"แต่ผู้เล่นตัดสินใจ",
"คุณคิดอะไรอยู่? คุณปล่อยยักษ์ใหญ่ออกมาแล้วทิ้งฉันไว้ที่นี่ โอเค เขาสามารถเลื่อนเหล็กได้ แต่มันจะเป็นอะไร?",
"เขาจะรอคอยความช่วยเหลือจากฉันในห้องถัดไป เริ่มใหม่เลยตอนนี้!",
"มันไม่ยุติธรรม ทางของฉันมีทางเป็น",
"ปล่อยปละปลาเล็กไปอยู่ในมหาสมุทร โดยไม่มีฉัน เธอจะไม่ไกลได้ห่าง คุณควรเริ่มใหม่ทันที",
"ไม่ใช่ตามที่หมอสั่ง",
"ผมไม่ควรจะโยงเส้นที่เล็กกว่าลงบนเส้นที่ใหญ่กว่า",
"บางคนอาจจะคิดว่าห้องนี้เป็นเพียงสำเนียงของหนึ่งในห้องใน Fish Fillets 2",
"แต่ในความเป็นจริง ผู้เขียนเพียงแรงบันดาลใจจากปัญหาฮาร์ดแวร์",
"ทำไมฉันต้องพับชิ้นงานทั้งหมด ในขณะที่โปรแกรมที่เรียกตัวเองเพียงอย่างง่ายก็สามารถทำได้?",
"ผู้เล่นจะได้รับความสุขจากสิ่งนี้ได้อย่างไร?",
"เขาอาจคาดหวังห้องถัดไป",
"เขาไม่รู้ว่ามันเป็นยังไง",
"อาจมีใครบางคนมันตั้งใจโดยเฉพาะ",
"อืมม โอเค ผู้เล่นหากคุณไม่สามารถแก้ไขมัน คุณคือคนที่แพ้ใหญ่",
"แน่นอน คุณไม่ได้สนุกเมื่อคุณเกือบไม่ช่วยฉันเลย",
"คุณพูดมันอยู่แล้วสามครั้ง",
"ถ้าผมควรพูดอะไรใหม่เสมอ แล้วเราจะเต็มดิสก์ของผู้เล่น",
"บางทีดิสก์ของผู้เล่นในวันนี้อาจจะรองรับข้อมูลได้มากกว่าในอดีต",
"ผมคิดว่าไม่ว่าดิสก์จะมีขนาดเท่าใดก็ตาม มันก็จะเต็มเสมอ",
"อายแสวงความรู้สองสมอง",
"สุดท้ายผมยกย่องผู้เล่นที่แก้ไขมันด้วยวิธีที่ยาวนานกว่า",
}
