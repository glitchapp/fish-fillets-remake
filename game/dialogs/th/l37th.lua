l37={"คุณเห็นมันได้ไหม? นี่น่าจะเป็นเต่าที่มีความสามารถในการสื่อสารแบบสมอง",
 "ใช่ นี่เอง เต่าที่มีความสามารถในการสื่อสารแบบสมอง",
 "อืม ฉันไม่แน่ใจ จากการมองภาพ มันอาจจะเป็นเค้าได้",
 "ฉันไม่แน่ใจ... เกี่ยวกับพลังในการสื่อสารแบบสมอง",
 "ในทุกกรณี เราต้องนำมันออกจากห้อง",
 "มาเอาเต่าออกกันเถอะ",
 "คุณทำอะไรอยู่?",
 "คุณทำอะไรแล้ว?",
 "มันหมายถึงอะไรน่ะ?",
 "คุณกำลังว่างอยู่ที่ไหน?",
 "มันควรหมายถึงอะไร?",
 "เกิดอะไรกับคุณแล้ว?",
 "ฉันไม่รู้...",
 "ฉันไม่สามารถเข้าใจได้...",
 "ฉันไม่รู้ว่ามันกำลังเกิดขึ้นกับฉันอย่างไร...",
 "ฉันไม่ต้องการ...",
 "มันกำลังเกิดขึ้นกับเราอะไรหลายอย่าง?",
 "มันควรหมายถึงอะไร?",
 "นี่ต้องเป็นเต่านั่นแหละ!",
 "นี่คือเต่าที่เรากำลังมองหา!",
 "ดังนั้นไม่มีความสงสัยแล้ว - นี่คือเธอ คือสิ่งที่เรากำลังมองหา!",
 "ไม่มีความสงสัย นี่คือเต่าที่เรากำลังมองหา!",
 "สัตว์เสียว!",
 "หยุดเถอะ คุณปีศาจ!",
 "พอแล้ว!",
 "คุณรู้สึกสนุกไหม?",
 "มีรูปร่างแปลกประหลาดมากในปะการัง",
 "ปะการังเหล่านี้เป็นความประหลาดของแม่ธรรมชาติ",
 "ที่มาของรูปร่างแปลกประหลาดมากนี้มาจากไหน?",
 "สบายมาก - นักออกแบบสร้างปัญหาที่ยากลำบากและให้ศิลปินดิจิทัลวาดภาพ",
 "เป้าหมายของเราในห้องนี้คือการดึงเต่านั้นออกมา",
}

l37end={"เราประสบความสำเร็จในการจับปลายอันตรายเต่าที่ไม่เป็นอันตรายเลย",
 "เราขอแนะนำมาตรการความปลอดภัยสูงสุดที่ไม่จำเป็นต้องรักษา เธอน่ารักจริงๆ",
 "เธอไม่เพียงแค่อ่านความคิดที่ดีอย่างจริงจัง แต่ยังมีอิทธิพลต่อมัน มันแปลกไปหน่อยนะ",
 "และเธอยังกัดเราด้วย แต่ไม่เป็นไร",
 "โดยทางเปิดให้เธออิสระ แต่เธอต้องต้องอยากทำและเธอจะไม่ถูกอัดปาก",
}
