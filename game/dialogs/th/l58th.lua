l58={"แล้วนี่แหละที่เป็นอย่างนั้น!",
"อ๋อไม่!",
"พลังงานชีวภาพ เราควรคิดถึงมันไว้ก่อน มีสัญญาณบ่งบอกทั้งหมด",
"แล้วพลังงานนิวเคลียร์ทำหน้าที่อะไร?",
"คุณไม่เห็นได้รึเปล่า? มันให้พลังงานให้กับตู้จำหน่ายโคโค่-โคล่าอยู่ที่นั่น",
"แล้วกล้องยนต์นั้นไว้ทำอะไร?",
"มันทำให้ระดับหน้าจอเคลื่อนไหว",
"ไม่มีใครจะเชื่อเรา",
"บางที อาจเป็นได้ FDTO กำลังทำงานในโครงการใช้พลังงานชีวภาพ",
"เราต้องออกจากอุปกรณ์นี้",
"ถ้าฉันมองถูกต้อง มันประกอบด้วยส่วนทั้งหก",
"กรงตรงกลางคือส่วนที่สำคัญที่สุด",
"โอเค เราจะเอาส่วนนี้",
"ฟังดู มีสิ่งมีชีวิตแปลกปลอมในที่นั่น คุณคิดว่าเราควรพยายามติดต่อพวกเขาไหม?",
"เป้าหมายของเราชัดเจนเลย - หลักการและสร้างระบบเครื่องยนต์สะท้อนดาวนี้ ไม่มีคำว่าติดต่ออยู่เลย",
"แต่อย่างไรก็ตามพวกเขาอาจช่วยเราได้บ้างนะ",
"สวัสดีตอนบ่ายค่ะ ขอโทษหน่อยคุณคุณสามารถถอดเครื่องไดรฟ์ของคุณได้หรือเปล่า?",
"อืม สวัสดีตอนบ่ายครับ ของช่วยถอดเครื่องยนต์ขับเคลื่อนได้ไหมครับ?",
"เป้าหมายของเราในเลเวลนี้คือการออกจากกรงพร้อมกับสัตว์นั้น",
"ฉันรู้สึกว่ามีการโต้เถียงระหว่างสองคนเหล่านี้",
"ฉันไม่เคยคาดหวังว่าการพบมนุษย์ต่างดาวจะเป็นแบบนี้",
"แล้วคุณคาดหวังอะไรกันเนี่ย? ว่าพวกเขาจะลักพี่สาวคุณ?",
}

l58end={"กวางหมาย",
"นี่เป็นคำนึงถึงที่ที่จะถูกพูดถึงอย่างแพร่หลายในไม่ช้าในระยะเวลาสองปีหน้าเกี่ยวกับการขับเคลื่อนระหว่างดาว",
"นี่เป็นเหตุผลที่เป็นไปได้สำหรับการเยือนของ UFO มายังโลก",
"ใช่ มีรายงานผู้ถูกจับกุมในช่วง 5 ปีที่ผ่านมาว่าถูกกุมโดยกวาง",
"โดยเฉพาะในภูมิภาคอิสราเอล ยูทาห์ และมราวิยา แต่ไม่มีใครให้ความสนใจกับมันจริงๆ",
"ตอนนี้เรารู้ความจริงที่น่าสะพรึงกลัวทั้งหมด คำอธิบายทางเทคนิครวมอยู่ด้วย",
}
