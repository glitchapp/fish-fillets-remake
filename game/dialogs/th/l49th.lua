l49={"โห้ยยย...น่ากลัว น่ากลัว กัปตันซิลเวอร์ที่โหดร้าย...",
"กัปตันซิลเวอร์ที่โหดร้าย...",
"อ้อน่าเศร้า!",
"ปัญหาที่ยากจะแก้ไขอ่ะเมื่อไร",
"อืมมม!",
"โพลีต้องการขนมปัง!",
"ไปไกลๆไปสิ...",
"เราเคยเห็นอันนี้ที่ไหนแล้ว?",
"ไม่, คุณผิด. ห้องนี้ใหญ่กว่าหนึ่งร้อยสี่สิบสี่พื้นที่...",
"มองดูนกแก้วนั่น!!!",
"แล้วมันจะเป็นอะไรได้บ้าง?",
"คุณเห็นได้ไหม? ห้องนี้เป็นห้องสุดท้ายในแถว จะเป็นสมบัติที่แท้จริงไหมล่ะนะ?",
"และสิ่งที่จะเกิดขึ้นในสองอันที่เหลือ?",
"พวกเขาบอกเราให้หาแผนที่ ไม่มีใครกล่าวถึงสมบัตินะ",
"อย่ารำคาญตัวเอง. ผู้เขียนเกมคงจะแจ้งให้ผู้เล่นทราบแน่นอน",
"ปลาหมึกมีก้อนปลาหมึกที่ขดเคี้ยวอย่างไม่พึงประสงค์ ฉันเล็กหน่อยเพื่อจะพอจะผ่าตัดได้",
"...แล้วเราตัดหนึ่งเส้นเข้าไปเถอะ?!",
"ขอโทษนะ มันแค่ความคิด",
"ปลาหมึกเล็กๆ คุณไม่อาจห่างไปหน่อยได้หรือครับ?",
"ฮืมมม...ขอบคุณมากนะ",
"กระโหลกดูเป็นหัวใจสำคัญของผู้เขียนเกม",
"คงอาศัยอยู่บนบกและผู้เขียนเกมก็ต้องรับรองว่ากระโหลกเหล่านี้เป็นพิศวงของพวกเขาที่พยายามเต็มที่ในการเต็มทะเลด้วยสิ่งของที่ไม่เกี่ยวข้อง",
"ลองคิดดูสิ. เราไม่สามารถนำวิธีการแก้ปัญหาในห้องก่อนหน้านี้มาปรับปรุงเล็กน้อยหรือเปล่า?",
"ฮืมม...ฉันไม่คิดว่าจะได้นะ",
"ดังนั้นเรามาให้แสงสว่างเข้าไป",
"หลังความอ้างอิงของฉัน"
}
