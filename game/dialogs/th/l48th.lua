l48={"โอ้โห้วววว, นี่คือห้องครัวที่น่าสนใจ",
"ใช่, เราค้นพบห้องครัวของเรือแล้ว",
"ครัวน่ารำคาญ",
"และตอนนี้เราอยู่ในครัวของกัปตันซิลเวอร์",
"ฉันไม่คิดว่าเขาเป็นพ่อครัวเอง",
"แต่นี่ไม่ใช่ปัญหาของเรา",
"ไม่มีปัญหามากนักที่นี่ ฉันสามารถว่ายออกได้ทันที",
"แต่ลองมองฉันสิ",
"ฉันไม่เข้าใจว่าโต๊ะเอ็นตรีบริการเหล่านี้ได้อยู่ที่นี่",
"โต๊ะเหล่านี้อยู่ในทาง",
"ห้องเล็กๆนี้ทำให้ฉันรู้สึกหวาดกลัว",
"ฉันออกไม่ได้เลย!",
"ฉันกำลังคิดวิธีช่วยเธออยู่",
"ช่วยฉันให้ได้สักอย่าง!",
"ฉันกำลังคิด...",
"ฉันกำลังมาเเล้ว",
"อุษกรรณ์, ยักษ์ยิงธนูโดนเเยกอยู่ในหม้อนี้!",
"หม้อนี้ไม่ได้ล้างเลย",
"ไม่มีใครล้างจานที่นี่เลย",
"ถ้าไม่มีเก้าอี้, มันจะช่วยประหยัดงานเยอะเลย",
"โดยเฉพาะตัวฉันเอง",
"เก้าอี้ทำไมถึงอยู่ในครัว?",
"ควรจะไม่มีเก้าอี้ในครัว",
"ทุกคนต้องการพักผ่อนบ้างเป็นครั้งคราว",
"ฉันเคยเห็นโต๊ะเหล่านี้ที่ไหนสักที",
"พวกเขาผลิตเป็นจำนวนมาก รู้มั้ย",
"แต่พวกเขาดูน่ารัก ควรจะสั่งซื้อมากับเราบ้างดี",
"ฉันกลัวว่าอาจมีแต่ฉันเท่านั้นที่สามารถออกจากพื้นที่นี้ได้",
"ดูเหมือนว่าเรือชิปนี้มีปัญหาบางอย่างที่ยากลำบาก",
"ทำไมดาบถึงอยู่ที่นี่?",
"อาจจะใช้มันตัดเนื้อสัตว์",
"แน่นอน แต่คุณไม่ตัดเนื้อสัตว์ด้วยดาบในครัวเป็นปกติ",
"เราโชคดีที่ไม่มีห้องครัวที่นี่ เขาอาจพยายามทำอาหารเรา",
"ต้องมีสูตรบางอย่างบนกระดาษที่นั่น",
"ต้องมีเคล็ดลับการทำอาหารบางอย่างบนกระดาษนั่นเอง",
"บางที, มันบอกวิธีทำปลาน่ะ",
}
