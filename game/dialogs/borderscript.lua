borderithink_en={"I think we have a job to do here"
}
borderwedidnt_en={"We didn't fulfill our mission yet"
}

bordertheagency_en={"The ageency rely upon us, we must nof fail!"
}

bordercertainly_en={"Certainly we are not going to run away with the inside "
}



function loadborderscript()
		if language=="en" or language=="chi" then
			if accent=="br" then
				ithinkwehave = love.audio.newSource( "/externalassets/dialogs/share/en/border/1ithink.ogg","stream" )
				wedidntfulfill = love.audio.newSource( "/externalassets/dialogs/share/en/border/2wedidnt.ogg","stream" )
				theagency = love.audio.newSource( "/externalassets/dialogs/share/en/border/3theagency.ogg","stream" )
				certainly = love.audio.newSource( "/externalassets/dialogs/share/en/border/4certainly.ogg","stream" )
				
			elseif accent=="us" then
				ithinkwehave = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/1ithink.ogg","stream" )
				wedidntfulfill = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/2wedidnt.ogg","stream" )
				theagency = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/3theagency.ogg","stream" )
				certainly = love.audio.newSource( "/externalassets/dialogs/share/en-us/border/4certainly.ogg","stream" )
			end
		
		
		elseif language=="es" then
				if accent=="es" then
			elseif accent=="la" then
				
			end
		
		elseif language=="fr" then
				
		end
		if language2=="en" then
				if accent2=="br" then
			elseif accent2=="us" then
				ithinkwehave2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/2Ithinkwe.ogg","stream" )
				wedidntfulfill2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/4wedidntfulfill.ogg","stream" )
				theagency2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/5theagency.ogg","stream" )
				certainly2 = love.audio.newSource( "/externalassets/dialogs/share/en-us/border2/8certainly.ogg","stream" )
			
			
			end
		elseif language2=="pl" then
				ithinkwehave2 = love.audio.newSource( "/externalassets/dialogs/share/pl/border2/2Ithinkwe.ogg","stream" )
				wedidntfulfill2 = love.audio.newSource( "/externalassets/dialogs/share/pl/border2/4wedidntfulfill.ogg","stream" )
				theagency2 = love.audio.newSource( "/externalassets/dialogs/share/pl/border2/5theagency.ogg","stream" )
				certainly2 = love.audio.newSource( "/externalassets/dialogs/share/pl/border2/8certainly.ogg","stream" )
		end
			
				
				--fish 1
				ithinkwehave:setEffect('myEffect')
				wedidntfulfill:setEffect('myEffect')
				theagency:setEffect('myEffect')
				certainly:setEffect('myEffect')
				
				--fish 2
			
				
end


function Obey.borescript()
  avatar = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("lib/talkies/example/assets/fishtalk.webp")))
  avatar2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("lib/talkies/example/assets/fishtalk2.webp")))
  
    if language=="ru" or language =="sl" then  Talkies.font = love.graphics.newFont("externalassets/fonts/alanesiana/AlanesianaRegular.ttf", 32) 
  elseif language=="chi" then Talkies.font = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 32) 
  else
  Talkies.font = love.graphics.newFont("externalassets/fonts/PoorFish/PoorFish-Regular.otf", 32)
  end

	if language=="chi" then
		smallfisht="小鱼"
		bigfisht="大鱼"
	else
		smallfisht="small fish"
		bigfisht="Big fish"
	end

Talkies.say( "Big fish",
    { 
    	borescript[1],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  

 Talkies.say( "small fish",
    {
      	borescript[2],
      	borescript[3],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })


   Talkies.say( "Big fish",
    { 
    	borescript[4],
    	borescript[5],
    	borescript[6],
    	borescript[7],
    	borescript[8],      	
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
    
	Talkies.say( "small fish",
    {
      	borescript[9],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
   Talkies.say( "Big fish",
    { 
    	borescript[10], 	
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
    
    Talkies.say( "small fish",
    {
      	borescript[11],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[12],
    	borescript[13],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[14],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[15],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[16],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[17],
    	borescript[18],
    	borescript[19],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[20],
      	borescript[21],
      	borescript[22],
      	borescript[23],
      	borescript[24],
      	borescript[25],
      	borescript[26],
      	
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[27],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[28],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[29],
    	borescript[30],
    	borescript[31],
    	borescript[32],
    	borescript[33],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[34],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "Big fish",
    { 
    	borescript[35],
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
    
    Talkies.say( "small fish",
    {
      	borescript[36],
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })
    
end

