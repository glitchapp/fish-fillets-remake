--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
The initializeffishvalues() function initializes the variables for three different images related to fish used in a game. It checks if the variables have already been initialized, and if not, it initializes them with default values. The function initializes the positions, directions, scales, and speeds of the fish images. It also sets the positions of two fish images (fishs1 and fishs2) to specific values, and sets their initial direction to the right. The function initializes the position, speed, direction, frame, scale, and speed of another fish image (ffish1). The WebP.loadImage() function is used to load the image files for the fish images.--]]
function initializeffishvalues()
		if ffish1==nil then 
						ffish1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/ffish2.webp")))
						fishs1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/fishs1.webp")))
						fishs2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level3/fishs2.webp")))
		end
					if fishs1positionx==nil then	
					--initialize foreground position
						frgx=0 frgy=0
					--initialize fishs1 position
						fishs1speed = 20 
						fishs2speed = 20 
						fishs1positionx=500
						fishs1positiony=500 
						fishs2positionx=300
						fishs2positiony=500 
						fishs1direction="right"
						fishs2direction="right"
						fishs1scalex=0.6
						fishs1scaley=0.6
						fishs2scalex=0.6
						fishs2scaley=0.6
					end
					if ffish1positionx==nil then
						--initialize ffish1 variables
						ffish1speed = 32 
						ffish1positionx=300
						ffish1positiony=100 
						ffish1frame=0 
						ffish1direction="right" 
						ffish1scalex=0.3
						ffish1scaley=0.3
					end
end

--[[
The function animatefirststart(dt) animates various images on the screen based on certain conditions. It checks if two images, snail1280 and kukajda_00_3000, are not yet loaded and if not, loads them from file. It then sets the color to white and uses the love.graphics.draw() function to draw the images on the screen. The images drawn depend on the value of the variable ffish1direction. If it is "right", two fish images, fish4x2 and fish3x1, are drawn at certain positions and scales. If it is "left", snail1280 and kukajda_00_3000 are drawn at different positions and scales. The function takes in a dt parameter, which is not used in the function.
--]]
function animatefirststart(dt)
if snail1280==nil 		then snail1280 = 		love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level7/snail1280.webp"))) end
if kukajda_00_3000==nil then kukajda_00_3000 = 	love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/objects/level44/kukajda_00_3000.webp"))) end
love.graphics.setColor(1,1,1,1)
		--love.graphics.setShader(normalsWarp)
		if ffish1direction=="right" then
			
			  
			 --[[ lightPos1 = { 0.1 ,0.0 , 0.075 }  
			normalsWarp:send("LightPos1", lightPos1)
			
			normalsWarp:send("u_normals", fish4x2Normal)
			lightColor1 = { 0.0, 0.0 , 1.0, 0.7 }
			normalsWarp:send("LightColor1", lightColor1)--]]
			love.graphics.draw (fish4x2, fishs1positionx+frgx-200,fishs1positiony+frgy,0,fishs1scalex*dividx,fishs1scaley*dividy)
			--[[lightColor2 = { 1.0, 0.0 , 0.0, 0.7 }
			lightPos2 = { 0.15 ,0.05 , 0.075 }  
			normalsWarp:send("LightPos2", lightPos2)
			normalsWarp:send("LightColor2", lightColor2)
			normalsWarp:send("u_normals", fish3x1Normal)--]]
			love.graphics.draw (fish3x1, fishs2positionx*2+frgx-500,fishs2positiony+frgy-200,0,fishs2scalex*dividx,fishs2scaley*dividy)
			--love.graphics.setShader()
	elseif ffish1direction=="left" then
			if res=="1080p" then love.graphics.draw (snail1280, fishs2positionx*2+frgx,fishs2positiony+frgy+350,0,-fishs2scalex*dividx/3,fishs2scaley*dividy/3)
		elseif res=="1440p" then love.graphics.draw (snail1280, fishs2positionx*2+frgx,fishs2positiony+frgy+670,0,-fishs2scalex*dividx/3,fishs2scaley*dividy/3)
		end
			love.graphics.draw (kukajda_00_3000, fishs1positionx+frgx+650,fishs2positiony+frgy,0,fishs1scalex*dividx/3,fishs1scaley*dividy/3)
	end

end

--[[This is a Lua function that takes one parameter, dt. The purpose of the function is to animate a whale object on the screen.

The function first checks if the whale is moving right or if the current level is not level 1. If so, it draws the whale image on the screen based on the current whaleframe value, which is a number that represents the current frame of the animation.

The function uses a series of if statements to determine which image to draw based on the current whaleframe value. The whale object is drawn using the love.graphics.draw() function, with the appropriate whale image and positioning information.

The function also uses several variables such as whaledirection, nLevel, whaleframe, whalepositionx, whalepositiony, frgx, frgy, whalescalex, and whalescaley. The purpose of these variables is to control the movement and animation of the whale object.
--]]
function animatewhale(dt)

if whaledirection=="right" or not (nLevel==1) then
		if whaleframe>0 and whaleframe<1 then love.graphics.draw (level1whale1, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>1 and whaleframe<2 then love.graphics.draw (level1whale2, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>2 and whaleframe<3 then love.graphics.draw (level1whale3, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>3 and whaleframe<4 then love.graphics.draw (level1whale4, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>4 and whaleframe<5 then love.graphics.draw (level1whale5, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>5 and whaleframe<6 then love.graphics.draw (level1whale6, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>6 and whaleframe<7 then love.graphics.draw (level1whale7, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>7 and whaleframe<8 then love.graphics.draw (level1whale8, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>8 and whaleframe<9 then love.graphics.draw (level1whale9, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>9 and whaleframe<10 then love.graphics.draw (level1whale10, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>10 and whaleframe<11 then love.graphics.draw (level1whale11, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>11 and whaleframe<12 then love.graphics.draw (level1whale12, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>12 and whaleframe<13 then love.graphics.draw (level1whale13, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>13 and whaleframe<14 then love.graphics.draw (level1whale14, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>14 and whaleframe<15 then love.graphics.draw (level1whale15, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>15 and whaleframe<16 then love.graphics.draw (level1whale16, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>16 and whaleframe<17 then love.graphics.draw (level1whale17, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>17 and whaleframe<18 then love.graphics.draw (level1whale18, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>18 and whaleframe<19 then love.graphics.draw (level1whale19, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>19 and whaleframe<20 then love.graphics.draw (level1whale20, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>20 and whaleframe<21 then love.graphics.draw (level1whale21, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>21 and whaleframe<22 then love.graphics.draw (level1whale22, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>22 and whaleframe<23 then love.graphics.draw (level1whale23, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>23 and whaleframe<24 then love.graphics.draw (level1whale24, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>24 and whaleframe<25 then love.graphics.draw (level1whale25, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>25 and whaleframe<26 then love.graphics.draw (level1whale26, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>26 and whaleframe<27 then love.graphics.draw (level1whale27, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>27 and whaleframe<28 then love.graphics.draw (level1whale28, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>28 and whaleframe<29 then love.graphics.draw (level1whale29, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>29 and whaleframe<30 then love.graphics.draw (level1whale30, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>30 and whaleframe<31 then love.graphics.draw (level1whale31, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>31 and whaleframe<32 then love.graphics.draw (level1whale32, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>32 then whaleframe=1
	end

elseif whaledirection=="left" and nLevel==1 then
	--love.graphics.draw 	(level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
		if whaleframe>1 and whaleframe<2 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>2 and whaleframe<3 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>3 and whaleframe<4 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>4 and whaleframe<5 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>5 and whaleframe<6 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>6 and whaleframe<7 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>7 and whaleframe<8 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>8 and whaleframe<9 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>9 and whaleframe<10 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>10 and whaleframe<11 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>11 and whaleframe<12 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>12 and whaleframe<13 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>13 and whaleframe<14 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>14 and whaleframe<15 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>15 and whaleframe<16 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>16 and whaleframe<17 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>17 and whaleframe<18 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>18 and whaleframe<19 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>19 and whaleframe<20 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>20 and whaleframe<21 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>21 and whaleframe<22 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>22 and whaleframe<23 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>23 and whaleframe<24 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>24 and whaleframe<25 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>25 and whaleframe<26 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>26 and whaleframe<27 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>27 and whaleframe<28 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>28 and whaleframe<29 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>29 and whaleframe<30 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>30 and whaleframe<31 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>31 and whaleframe<32 then love.graphics.draw (level1whalex, whalepositionx+frgx,whalepositiony+frgy,0,whalescalex*dividx,whalescaley*dividy)
	elseif whaleframe>32 then whaleframe=1
	end
end

	if whaledirection=="right" then
				if nLevel==1 and whalepositionx>600 then whaledirection="left" whalescalex=-0.3 whalescaley=0.3 whalepositionx=1100 whalepositiony=550
				whalecalllow:setEffect('myEffect')
				whalecalllow:play()
		elseif nLevel==34 and whalepositionx>1900 then whaledirection="left" whalescalex=-1 whalepositionx=1800 whalepositiony=300
				whalecalllow:setEffect('myEffect')
				whalecalllow:play()
		elseif nLevel==35 and whalepositionx>1900 then whaledirection="left" whalescalex=-1 whalepositionx=1800 whalepositiony=400
				whalecalllow:setEffect('myEffect')
				whalecalllow:play()
		elseif nLevel==36 and whalepositionx>1900 then whaledirection="left" whalescalex=-1 whalepositionx=1800 whalepositiony=500
				whalecalllow:setEffect('myEffect')
				whalecalllow:play()
		end
	elseif whaledirection=="left" then
		 if nLevel==1 and whalepositionx<-300 then whaledirection="right" whalescalex=0.6 whalepositiony=100
				whalecallmid:setEffect('myEffect')
				whalecallmid:play()
	 elseif nLevel==34 and whalepositionx<-300 then whaledirection="right" whalescalex=1 whalepositiony=200
				whalecallmid:setEffect('myEffect')
				whalecallmid:play()
	 elseif nLevel==35 and whalepositionx<-300 then whaledirection="right" whalescalex=1 whalepositiony=300
				whalecallmid:setEffect('myEffect')
				whalecallmid:play()
	 elseif nLevel==36 and whalepositionx<-300 then whaledirection="right" whalescalex=1 whalepositiony=400
				whalecallmid:setEffect('myEffect')
				whalecallmid:play()
	 end
	end
end

--[[
This is a Lua function named animatedolphin(dt) that draws a series of dolphin images to create an animation. It takes in a dt parameter, which is the time since the last update.

The function first checks which image to draw based on the dolphinframe variable. If dolphinframe is between 0 and 1, it draws dolphin1, if it's between 1 and 2, it draws dolphin2, and so on. The dolphin images are drawn using the love.graphics.draw() function, which takes in several parameters such as the image to draw, its position, rotation angle, and scaling factors.

The dolphin's position is based on dolphinpositionx and dolphinpositiony, which are adjusted by frgx and frgy. The dolphin is also scaled based on dolphinscalex and dolphinscaley, which are multiplied by dividx and dividy.
--]]

function animatedolphin(dt)

	if dolphinframe>0 and dolphinframe<1 then love.graphics.draw (dolphin1, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>1 and dolphinframe<2 then love.graphics.draw (dolphin2, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>2 and dolphinframe<3 then love.graphics.draw (dolphin3, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>3 and dolphinframe<4 then love.graphics.draw (dolphin4, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>4 and dolphinframe<5 then love.graphics.draw (dolphin5, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>5 and dolphinframe<6 then love.graphics.draw (dolphin6, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>6 and dolphinframe<7 then love.graphics.draw (dolphin7, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>7 and dolphinframe<8 then love.graphics.draw (dolphin8, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>8 and dolphinframe<9 then love.graphics.draw (dolphin9, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>9 and dolphinframe<10 then love.graphics.draw (dolphin10, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>10 and dolphinframe<11 then love.graphics.draw (dolphin11, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>11 and dolphinframe<12 then love.graphics.draw (dolphin12, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>12 and dolphinframe<13 then love.graphics.draw (dolphin13, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>13 and dolphinframe<14 then love.graphics.draw (dolphin14, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>14 and dolphinframe<15 then love.graphics.draw (dolphin15, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>15 and dolphinframe<16 then love.graphics.draw (dolphin16, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>16 and dolphinframe<17 then love.graphics.draw (dolphin17, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>17 and dolphinframe<18 then love.graphics.draw (dolphin18, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>18 and dolphinframe<19 then love.graphics.draw (dolphin19, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>19 and dolphinframe<20 then love.graphics.draw (dolphin20, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>20 and dolphinframe<21 then love.graphics.draw (dolphin21, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>21 and dolphinframe<22 then love.graphics.draw (dolphin22, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>22 and dolphinframe<23 then love.graphics.draw (dolphin23, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>23 and dolphinframe<24 then love.graphics.draw (dolphin24, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy)
elseif dolphinframe>24 and dolphinframe<25 then love.graphics.draw (dolphin25, dolphinpositionx+frgx,dolphinpositiony+frgy,0,dolphinscalex*dividx,dolphinscaley*dividy) dolphinframe=1

	if dolphindirection=="right" then
				if nLevel==7 and dolphinpositionx+frgx>1300 then dolphindirection="left" dolphinscalex=-0.6 
				dolphin_clicks:setEffect('myEffect')
				dolphin_clicks:setVolume(0.5)
				dolphin_clicks:play()
		end
	elseif dolphindirection=="left" then
		 if nLevel==7 and dolphinpositionx+frgx<-300 then dolphindirection="right" dolphinscalex=0.6
				dolphin_cry:setEffect('myEffect')
				dolphin_cry:setVolume(0.5)
				dolphin_cry:play()
	 end
	end
end
end

--[[
This function is used in a game to animate a shark. It takes a parameter dt which stands for delta time and is used to update the shark's frame position based on the elapsed time. The function checks which frame the shark animation is currently on, and draws the corresponding image of the shark on the screen using the love.graphics.draw() function. The function uses a series of if-elseif statements to determine which shark image to draw based on the sharkframe variable. The sharkframe variable should be updated elsewhere in the game code to control the shark's animation speed. The function also uses the sharkpositionx, sharkpositiony, sharkscalex, and sharkscaley variables to position and scale the shark image on the screen.
--]]
function animatedshark(dt)

	if sharkframe>0 and sharkframe<1 then love.graphics.draw (shark1, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>1 and sharkframe<2 then love.graphics.draw (shark2, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>2 and sharkframe<3 then love.graphics.draw (shark3, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>3 and sharkframe<4 then love.graphics.draw (shark4, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>4 and sharkframe<5 then love.graphics.draw (shark5, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>5 and sharkframe<6 then love.graphics.draw (shark6, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>6 and sharkframe<7 then love.graphics.draw (shark7, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>7 and sharkframe<8 then love.graphics.draw (shark8, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>8 and sharkframe<9 then love.graphics.draw (shark9, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>9 and sharkframe<10 then love.graphics.draw (shark10, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>10 and sharkframe<11 then love.graphics.draw (shark11, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>11 and sharkframe<12 then love.graphics.draw (shark12, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>12 and sharkframe<13 then love.graphics.draw (shark13, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>13 and sharkframe<14 then love.graphics.draw (shark14, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>14 and sharkframe<15 then love.graphics.draw (shark15, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>15 and sharkframe<16 then love.graphics.draw (shark16, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>16 and sharkframe<17 then love.graphics.draw (shark17, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>17 and sharkframe<18 then love.graphics.draw (shark18, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>18 and sharkframe<19 then love.graphics.draw (shark19, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>19 and sharkframe<20 then love.graphics.draw (shark20, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>20 and sharkframe<21 then love.graphics.draw (shark21, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>21 and sharkframe<22 then love.graphics.draw (shark22, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>22 and sharkframe<23 then love.graphics.draw (shark23, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>23 and sharkframe<24 then love.graphics.draw (shark24, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy)
elseif sharkframe>24 and sharkframe<25 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) 
elseif sharkframe>25 and sharkframe<26 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) 
elseif sharkframe>26 and sharkframe<27 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) 
elseif sharkframe>27 and sharkframe<28 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) 
elseif sharkframe>28 and sharkframe<29 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) 
elseif sharkframe>29 and sharkframe<30 then love.graphics.draw (shark25, sharkpositionx+frgx,sharkpositiony+frgy,0,sharkscalex*dividx,sharkscaley*dividy) sharkframe=1

	if sharkdirection=="right" then
				if nLevel==15 and sharkpositionx+frgx>1300 then sharkdirection="left" sharkscalex=-0.6 
				
		end
	elseif sharkdirection=="left" then
		 if nLevel==15 and sharkpositionx+frgx<-300 then sharkdirection="right" sharkscalex=0.6
				
	 end
	end
end
end

--[[
This is a Lua function that takes a parameter dt as input. It appears to be animating a mantaray by drawing different images of the mantaray depending on the value of the mantarayframe variable. The function checks which image to draw based on the value of mantarayframe, which seems to be a number that increases over time. Depending on the value of mantarayframe, a different image is drawn using the love.graphics.draw function, with different parameters for the image file, position, rotation, and scaling. The mantarayframe variable appears to be a counter that determines which image to draw based on how much time has passed since the animation started.
--]]

function animatemantaray(dt)
	if mantarayframe>0 and mantarayframe<1 then love.graphics.draw (mantaray23, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>1 and mantarayframe<2 then love.graphics.draw (mantaray24, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>2 and mantarayframe<3 then love.graphics.draw (mantaray25, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>3 and mantarayframe<4 then love.graphics.draw (mantaray26, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>4 and mantarayframe<5 then love.graphics.draw (mantaray27, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>5 and mantarayframe<6 then love.graphics.draw (mantaray28, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>6 and mantarayframe<7 then love.graphics.draw (mantaray29, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>7 and mantarayframe<8 then love.graphics.draw (mantaray30, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>8 and mantarayframe<9 then love.graphics.draw (mantaray31, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>9 and mantarayframe<10 then love.graphics.draw (mantaray32, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>10 and mantarayframe<11 then love.graphics.draw (mantaray33, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>11 and mantarayframe<12 then love.graphics.draw (mantaray34, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>12 and mantarayframe<13 then love.graphics.draw (mantaray35, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>13 and mantarayframe<14 then love.graphics.draw (mantaray36, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>14 and mantarayframe<15 then love.graphics.draw (mantaray37, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>15 and mantarayframe<16 then love.graphics.draw (mantaray38, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>16 and mantarayframe<17 then love.graphics.draw (mantaray39, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>17 and mantarayframe<18 then love.graphics.draw (mantaray40, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>18 and mantarayframe<19 then love.graphics.draw (mantaray41, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>19 and mantarayframe<20 then love.graphics.draw (mantaray42, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>20 and mantarayframe<21 then love.graphics.draw (mantaray43, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>21 and mantarayframe<22 then love.graphics.draw (mantaray44, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>22 and mantarayframe<23 then love.graphics.draw (mantaray45, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>23 and mantarayframe<24 then love.graphics.draw (mantaray46, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>24 and mantarayframe<25 then love.graphics.draw (mantaray47, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>25 and mantarayframe<26 then love.graphics.draw (mantaray48, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>26 and mantarayframe<27 then love.graphics.draw (mantaray49, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>27 and mantarayframe<28 then love.graphics.draw (mantaray50, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>28 and mantarayframe<29 then love.graphics.draw (mantaray51, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>29 and mantarayframe<30 then love.graphics.draw (mantaray52, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>30 and mantarayframe<31 then love.graphics.draw (mantaray53, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>31 and mantarayframe<32 then love.graphics.draw (mantaray54, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>32 and mantarayframe<33 then love.graphics.draw (mantaray55, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>33 and mantarayframe<34 then love.graphics.draw (mantaray56, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>34 and mantarayframe<35 then love.graphics.draw (mantaray57, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>35 and mantarayframe<36 then love.graphics.draw (mantaray58, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>36 and mantarayframe<37 then love.graphics.draw (mantaray59, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>37 and mantarayframe<38 then love.graphics.draw (mantaray60, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>38 and mantarayframe<39 then love.graphics.draw (mantaray61, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy) 
elseif mantarayframe>39 and mantarayframe<40 then love.graphics.draw (mantaray60, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>40 and mantarayframe<41 then love.graphics.draw (mantaray59, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>41 and mantarayframe<42 then love.graphics.draw (mantaray58, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>42 and mantarayframe<43 then love.graphics.draw (mantaray57, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>43 and mantarayframe<44 then love.graphics.draw (mantaray56, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>44 and mantarayframe<45 then love.graphics.draw (mantaray55, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>45 and mantarayframe<46 then love.graphics.draw (mantaray54, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>46 and mantarayframe<47 then love.graphics.draw (mantaray53, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>47 and mantarayframe<48 then love.graphics.draw (mantaray52, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>48 and mantarayframe<49 then love.graphics.draw (mantaray51, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>49 and mantarayframe<50 then love.graphics.draw (mantaray50, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>50 and mantarayframe<51 then love.graphics.draw (mantaray49, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>51 and mantarayframe<52 then love.graphics.draw (mantaray48, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>52 and mantarayframe<53 then love.graphics.draw (mantaray47, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>53 and mantarayframe<54 then love.graphics.draw (mantaray46, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>54 and mantarayframe<55 then love.graphics.draw (mantaray45, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>55 and mantarayframe<56 then love.graphics.draw (mantaray44, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>56 and mantarayframe<57 then love.graphics.draw (mantaray43, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>57 and mantarayframe<58 then love.graphics.draw (mantaray42, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>58 and mantarayframe<59 then love.graphics.draw (mantaray41, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>59 and mantarayframe<60 then love.graphics.draw (mantaray40, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>60 and mantarayframe<61 then love.graphics.draw (mantaray39, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>61 and mantarayframe<62 then love.graphics.draw (mantaray38, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>62 and mantarayframe<63 then love.graphics.draw (mantaray37, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>63 and mantarayframe<64 then love.graphics.draw (mantaray36, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>64 and mantarayframe<65 then love.graphics.draw (mantaray35, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>65 and mantarayframe<66 then love.graphics.draw (mantaray34, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>66 and mantarayframe<67 then love.graphics.draw (mantaray33, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>67 and mantarayframe<68 then love.graphics.draw (mantaray32, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>68 and mantarayframe<69 then love.graphics.draw (mantaray31, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>69 and mantarayframe<70 then love.graphics.draw (mantaray30, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>70 and mantarayframe<71 then love.graphics.draw (mantaray29, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>71 and mantarayframe<72 then love.graphics.draw (mantaray28, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>72 and mantarayframe<73 then love.graphics.draw (mantaray27, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>73 and mantarayframe<74 then love.graphics.draw (mantaray26, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>74 and mantarayframe<75 then love.graphics.draw (mantaray25, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>75 and mantarayframe<76 then love.graphics.draw (mantaray24, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)
elseif mantarayframe>76 and mantarayframe<77 then love.graphics.draw (mantaray23, mantaraypositionx+frgx,mantaraypositiony+frgy,0,mantarayscalex*dividx,mantarayscaley*dividy)


 mantarayframe=1

	if mantaraydirection=="right" then
				if nLevel==35 and mantaraypositionx>1300 then mantaraydirection="left" mantarayscalex=-0.6 
				--:setEffect('myEffect')
				--:setVolume(0.5)
				--:play()
		end
	elseif mantaraydirection=="left" then
		 if nLevel==35 and mantaraypositionx<-300 then mantaraydirection="right" mantarayscalex=0.6
				--:setEffect('myEffect')
				--:setVolume(0.5)
				--:play()
	 end
	end
end
end

--[[
This function is an animation function that animates fish graphics on the screen. It takes a parameter "dt" which represents the time elapsed since the last frame update.

The function begins by setting a canvas and a water warp shader, but the code for the shader is commented out. Then, it checks the value of the "nLevel" variable and sets the color of the graphics accordingly. The function then draws two images of fish ("fishs1" and "fishs2") on the screen with different positions, scales, and rotations.

Next, there is a conditional statement that checks if the "nLevel" variable has a specific value. If it does, it updates the position and scale of the "fishs1" image based on the direction it is moving. This code is repeated again with a different set of conditional statements if the "nLevel" variable has another set of specific values. Finally, the function sets the canvas and shader back to their original values, although the code is commented out.
--]]
function animatefishs(dt)
--[[--water warp shader
love.graphics.setCanvas(canvas)
warpshader2:send("time", timer)
love.graphics.setShader(warpshader2)
--]]

	if nLevel==30 or nLevel==31 or nLevel==32 then love.graphics.setColor(0.1,0.1,0.1,1) end
		love.graphics.draw (fishs1, fishs1positionx+frgx+200,fishs1positiony+frgy-300,0,fishs1scalex*dividx,fishs1scaley*dividy)
		love.graphics.draw (fishs2, fishs2positionx+frgx	,fishs2positiony+frgy-300,0,fishs2scalex*dividx,fishs2scaley*dividy)

			if nLevel==3 or nLevel==7 or nLevel==14 or nLevel==16 or nLevel==19 or nLevel==20 or nLevel==21 or nLevel==28 or nLevel==29 or nLevel==34 or nLevel==35 or nLevel==36 or nLevel==101 or nLevel==102 or nLevel==103 then
					if fishs1direction=="right" and fishs1positionx+frgx>1500 then fishs1direction="left"  fishs1positiony=500 fishs1scalex=-0.6 fishs2scalex=-0.6 
				elseif fishs1direction=="left"  and fishs1positionx+frgx<300  then fishs1direction="right" fishs1positiony=600 fishs1scalex= 0.6 fishs2scalex= 0.6
				end
			end
			if nLevel==30 or nLevel==31 or nLevel==32 then
					if fishs1direction=="right" and fishs1positionx+frgx>700 then fishs1direction="left"  fishs1positiony=500 fishs1scalex=-0.6 fishs2scalex=-0.6 if nLevel==31 then fishs1scalex=-1.2 fishs2scalex=-1.2 elseif nLevel==32 then fishs1scalex=-1.2 fishs2scalex=-1.2 end
				elseif fishs1direction=="left"  and fishs1positionx+frgx<100 then fishs1direction="right" fishs1positiony=600 fishs1scalex= 0.6 fishs2scalex= 0.6 if nLevel==31 then fishs1scalex= 1.2 fishs2scalex= 1.2 elseif nLevel==32 then fishs1scalex= 1.2 fishs2scalex= 1.2 end
			end
	end

--[[love.graphics.setCanvas()
love.graphics.setShader()--]]
end

--[[
This function, animateffish1(dt), is responsible for animating the image of a fish (ffish1). It first checks if the fish object has been initialized (ffish1==nil), and if not, calls the initializeffishvalues() function. It then uses the love.graphics.draw() function to draw the fish image at a position determined by the variables ffish1positionx, ffish1positiony, frgx, and frgy, with a scale determined by ffish1scalex and ffish1scaley. Finally, it updates the direction of the fish's movement based on the current game level.

--]]
function animateffish1(dt)
	if ffish1==nil then initializeffishvalues() end
	
	love.graphics.draw (ffish1, ffish1positionx+frgx,ffish1positiony+frgy,0,ffish1scalex*dividx,ffish1scaley*dividy)
	
	if ffish1direction=="right" then
				if nLevel==3   and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==7   and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==15  and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==16  and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==35  and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==101 and ffish1positionx+frgx >1500  then ffish1direction="left" ffish1positiony=150 ffish1scalex=-0.4
			elseif nLevel==32  and ffish1positionx+frgx >1200  then ffish1direction="left" ffish1positiony=200 ffish1scalex=-0.3
				--whalecalllow:setEffect('myEffect')
				--whalecalllow:play()
			end
	elseif ffish1direction=="left" then
				if nLevel==3   and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==7   and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==15  and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==16  and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==35  and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==101 and ffish1positionx<300 then ffish1direction="right" ffish1positiony=150 ffish1scalex=0.4
			elseif nLevel==32  and ffish1positionx<300 then ffish1direction="right" ffish1positiony=200 ffish1scalex=0.3
				--whalecallmid:setEffect('myEffect')
				--whalecallmid:play()
			end
	end
end

-- The second function, ffish1update(dt), updates the position of the fish image based on its direction (ffish1direction) and speed (ffish1speed). It checks if the screenshake variable is true and sets the fish's speed accordingly. It then updates the ffish1positionx variable based on the direction of the fish's movement.

function ffish1update(dt)
if screenshake==true then ffish1speed=16 else ffish1speed=32 end
		if ffish1direction=="right" then ffish1positionx = ffish1positionx + (ffish1speed * dt)  -- ffish1position will increase by 32 for every second
	elseif ffish1direction=="left"	then ffish1positionx = ffish1positionx  - (ffish1speed * dt)  -- ffish1position will increase by 32 for every second
	end
end

-- The third function, fishsupdate(dt), updates the positions of two fish (fishs1 and fishs2) based on their direction and speed. Similar to the ffish1update() function, it checks the screenshake variable and updates the fish positions accordingly. It then updates the fishs1positionx and fishs2positionx variables based on the direction of the fish's movement.

function fishsupdate(dt)
if screenshake==true then fishs1speed=10 else fishs1speed=20 end
	if fishs1direction=="right" then
	fishs1positionx = fishs1positionx + (fishs1speed * dt)  -- ffish1position will increase by 32 for every second
	fishs2positionx = fishs2positionx + (fishs2speed * dt)  -- ffish1position will increase by 32 for every second
	elseif fishs1direction=="left" then
	fishs1positionx = fishs1positionx - (fishs1speed * dt)  -- ffish1position will increase by 32 for every second
	fishs2positionx = fishs2positionx - (fishs2speed * dt)  -- ffish1position will increase by 32 for every second
	end
end

--[[: This function updates the position of the bubbles by subtracting a value proportional to the time elapsed since the last update (dt) multiplied by 30. This gives the effect of the bubbles rising to the surface. If the bubbles go beyond a certain height (-3000 in this case), their position is reset to 0.--]]
function updatebubbles(dt)
	bubblesy = bubblesy - dt*30
	if bubblesy<-3000 then bubblesy=0 end
end

--This function draws the bubbles on the screen using a water warp shader (warpshader2). The position of the bubbles (bubblesy) is determined by the updatebubbles() function. The canvas is set to the screen, and the bubbles are drawn on it using the shader.
function drawbubbles(dt)
		--water warp shader
		love.graphics.setCanvas(canvas)
		warpshader2:send("time", timer)
		love.graphics.setShader(warpshader2)
		love.graphics.setCanvas()
			love.graphics.draw (bubbles, 0,bubblesy,0,1,1)
		love.graphics.setShader()
end

-- This function updates the position of the whale based on its direction (whaledirection) and whether the screen is shaking (screenshake). If the screen is shaking, the whale moves faster (whalespeed is set to 16 instead of 32). The whale's position (whalepositionx) is updated based on the time elapsed since the last update (dt) and its speed (whalespeed). The whale's animation frame (whaleframe) is also updated by adding 0.01 to its current value.
function whaleupdate(dt)
if screenshake==true then whalespeed=16 else whalespeed=32 end
	if whaledirection=="right" then
	whalepositionx = whalepositionx + (whalespeed * dt)  -- whaleposition will increase by 32 for every second
	elseif whaledirection=="left" then
	whalepositionx = whalepositionx - (whalespeed * dt)  -- whaleposition will increase by 32 for every second
	end
	whaleframe = whaleframe + 0.01
end

--Updates the position and frame of the dolphin sprite based on the direction and speed, which are determined by the input variable dolphindirection and dolphinspeed.
function dolphinupdate(dt)
	if dolphindirection=="right" then
	dolphinpositionx = dolphinpositionx + (dolphinspeed * dt)  -- dolphinposition will increase by 32 for every second
	elseif dolphindirection=="left" then
	dolphinpositionx = dolphinpositionx - (dolphinspeed * dt)  -- dolphinposition will increase by 32 for every second
	end
	dolphinframe = dolphinframe + 0.01
end

-- Updates the position and frame of the shark sprite based on the direction and speed, which are determined by the input variable sharkdirection and sharkspeed.
function sharkupdate(dt)
	if sharkdirection=="right" then
	sharkpositionx = sharkpositionx + (sharkspeed * dt)  -- sharkposition will increase by 32 for every second
	elseif sharkdirection=="left" then
	sharkpositionx = sharkpositionx - (sharkspeed * dt)  -- sharkposition will increase by 32 for every second
	end
	sharkframe = sharkframe + 0.01
end

-- Updates the position and frame of the mantaray sprite based on the direction and speed, which are determined by the input variable mantaraydirection and mantarayspeed.
function mantarayupdate(dt)
	if mantaraydirection=="right" then
	mantaraypositionx = mantaraypositionx + (mantarayspeed * dt)  -- mantarayposition will increase by 32 for every second
	elseif mantaraydirection=="left" then
	mantaraypositionx = mantaraypositionx - (mantarayspeed * dt)  -- mantarayposition will increase by 32 for every second
	end
	mantarayframe = mantarayframe + 0.01
end

-- Updates the frame of the squirrel and washing machine drum sprites. The squirrel frame increases by 1 multiplied by dt, and when it reaches 0.49 it resets to 0. The washing machine drum frame increases by 1 multiplied by dt, and when it reaches 360 it resets to 0.
function squirrelupdate(dt)
	squirrelframe=squirrelframe +1*dt
	if squirrelframe>0.49 then 
		squirrelframe=0 
	end
	washmachinedrumframe=washmachinedrumframe +1*dt
	if washmachinedrumframe>360 then washmachinedrumframe=0 end
end

-- Updates the frame of the face sprite. The face frame increases by 1 multiplied by dt, and when it reaches 1.5 it resets to 0.
function faceupdate(dt)	-- face level 20
	faceframe=faceframe +1*dt
	if faceframe>1.5 then 
		faceframe=0 
	end

end

-- This function updates the animation frame of the cross object, which is used in level 20 and 28 of the game. The animation cycles through 1.5 seconds of frames and loops back to the beginning.
function crossupdate(dt)	-- Cross level 20 & 28
	crossframe=crossframe +1*dt
	if crossframe>1.5 then 
		crossframe=0 
	end

end

-- This function updates the animation frame of the talking sculpture object, which is used in level 21. The animation cycles through 4 frames over time and loops back to the beginning.
function statueupdate(dt)	-- talking sculpture level 21
	statueframe=statueframe +1*dt
	if statueframe>4 then 
		statueframe=0 
	end
end

-- This function updates the animation frame of the alien engine key object. The animation cycles through 2 frames over time and loops back to the beginning.
function alienenginekeyupdate(dt)	-- alien engine key
	alienenginekeyframe=alienenginekeyframe +1*dt
	if alienenginekeyframe>2 then 
		alienenginekeyframe=0 
	end
end

--  This function updates the animation frame of the radioactive pink monster object. The animation cycles through 2 frames over time and loops back to the beginning.
function pinkmonsterupdate(dt)	-- Radioactive pink monster
	pinkmonsterframe=pinkmonsterframe +1*dt
	if pinkmonsterframe>2 then 
		pinkmonsterframe=0 
	end
end

-- This function updates the animation frame of the crab object. The animation cycles through 2 frames over time and loops back to the beginning.
function crabupdate(dt)	-- Crab animation
	crabframe=crabframe +1*dt
	if crabframe>2 then 
		crabframe=0 
	end
end

-- This function updates the animation frame of the crab object, but the animation is longer than crabupdate(dt). The animation cycles through 2.5 seconds of frames and loops back to the beginning.
function crabupdatemiddle(dt)	-- Crab animation
	crabframe=crabframe +1*dt
	if crabframe>2.5 then 
		crabframe=0 
	end
end

-- This function updates the animation frame of the crab object, but the animation is longer than crabupdate(dt) and crabupdatemiddle(dt). The animation cycles through 5 seconds of frames and loops back to the beginning.
function crabupdatelong(dt)	-- Crab animation
	crabframe=crabframe +1*dt
	if crabframe>5 then 
		crabframe=0 
	end
end

-- function crab2update(dt) is a function that updates the frame of the Crab2 animation. It increments the crab2frame variable by the delta time dt and resets it to zero if it exceeds the threshold value of 2.
function crab2update(dt)	-- Crab2 animation (level 27)
	crab2frame=crab2frame +1*dt
	if crab2frame>2 then 
		crab2frame=0 
	end
end

--function snailupdate(dt) is a function that updates the frame of the Snail animation. It increments the snailframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 2.
function snailupdate(dt)	-- Snail animation
	snailframe=snailframe +1*dt
	if snailframe>2 then 
		snailframe=0 
	end
end

-- function snaillongupdate(dt) is a function that updates the frame of the long Snail animation. It increments the snekframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 4.
function snaillongupdate(dt)	-- Snail animation
	snekframe=snekframe +1*dt
	if snekframe>4 then 
		snekframe=0 
	end
end

-- function seahorseupdate(dt) is a function that updates the frame of the Seahorse animation. It increments the seahorseframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 2.
function seahorseupdate(dt)	-- Seafish
	seahorseframe=seahorseframe +1*dt
	if seahorseframe>2 then 
		seahorseframe=0 
	end
end

-- function sasankaupdate(dt) is a function that updates the frame of the Sasanka animation. It increments the sasankaframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 2.
function sasankaupdate(dt)	-- Creature
	sasankaframe=sasankaframe +1*dt
	if sasankaframe>2 then 
		sasankaframe=0 
	end
end

-- function periscopeupdate(dt) is a function that updates the frame of the Periscope animation. It increments the periframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 6.
function periscopeupdate(dt)	-- Periscpope
	periframe=periframe +1*dt
	if periframe>6 then 
		periframe=0 
	end
end

--function boatwindowupdate(dt) is a function that updates the frame of the Boat passengers animation. It increments the boatwindowframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 18.
function boatwindowupdate(dt)	-- Boat passengers
	boatwindowframe=boatwindowframe +1*dt
	if boatwindowframe>18 then 
		boatwindowframe=0 
	end
end

function boatglassupdate(dt)	-- Party boat glass
	boatglassframe=boatglassframe +1*dt
	if boatglassframe>6 then 
		boatglassframe=0 
		glassmoving=false
		glassstandmoving=false
	end
end

-- function vikingupdate(dt) is a function that updates the frame of the Viking animation. It increments the vikingframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 2.5.
function vikingupdate(dt)	-- Viking animations
	vikingframe=vikingframe +1*dt
	if vikingframe>2.5 then 
		vikingframe=0 
	end
end

-- function neptunupdate(dt) is a function that updates the frame of the Neptun animation. It increments the neptunframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 11.5.
function neptunupdate(dt)	-- Neptun animation
	neptunframe=neptunframe +1*dt
	if neptunframe>11.5 then 
		neptunframe=0 
	end
end

-- function poseidonupdate(dt) is a function that updates the frame of the Poseidon animation. It increments the poseidonframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 12.25.
function poseidonupdate(dt)	-- Poseidon animations
	poseidonframe=poseidonframe +1*dt
	if poseidonframe>12.25 then 
		poseidonframe=0 
	end
end

-- function korunaupdate(dt) is a function that updates the frame of the Crown animation. It increments the korunaframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 1.5.
function korunaupdate(dt)	-- crown animation
	korunaframe=korunaframe +1*dt
	if korunaframe>1.5 then 
		korunaframe=0 
	end
end

-- function krystalupdate(dt) is a function that updates the frame of the Diamond animation. It increments the krystalframe variable by the delta time dt and resets it to zero if it exceeds the threshold value of 1
function krystalupdate(dt)	-- Diamond animation
	krystalframe=krystalframe +1*dt
	if krystalframe>1 then 
		krystalframe=0 
		krystalMovement=math.random(-10,10)
	end
end

--krystalcupdate(dt): This function updates the frame of a diamond animation that lasts longer than the krystalupdate() animation. The krystalcframe variable is incremented by dt and reset to zero once it reaches 2.
function krystalcupdate(dt)	-- Diamond animation long
	krystalcframe=krystalcframe +1*dt
	if krystalcframe>2 then 
		krystalcframe=0 
		
	end
end

--totemupdate(dt): This function updates the frame of a totem animation that lasts 6 frames. The totemframe variable is incremented by dt and reset to zero once it reaches 6.
function totemupdate(dt)	-- totem animation level 59
	totemframe=totemframe +1*dt
	if totemframe>6 then 
		totemframe=0 
	end
end

--elkupdate(dt): This function updates the frame of an elk animation that lasts 2 frames. The elkframe variable is incremented by dt and reset to zero once it reaches 2.
function elkupdate(dt)	-- Elk
	elkframe=elkframe +1*dt
	if elkframe>2 then 
		elkframe=0 
	end
end

--deadparrotupdate(dt): This function updates the frame of a dead parrot animation that lasts 2 frames. The deadparrotframe variable is incremented by dt and reset to zero once it reaches 2.
function deadparrotupdate(dt)	-- dead parrot animation
	deadparrotframe=deadparrotframe +1*dt
	if deadparrotframe>2 then 
		deadparrotframe=0 
	end
end

--medusaupdate(dt): This function updates the frame of a medusa animation that lasts 1.5 frames. The medusaframe variable is incremented by dt and reset to zero once it reaches 1.5.
function medusaupdate(dt)	-- medusa animation
	medusaframe=medusaframe +1*dt
	if medusaframe>1.5 then 
		medusaframe=0 
	end
end

--rybickaupdate(dt): This function updates the frame of a rybicka animation that lasts 2 frames. The rybickaframe variable is incremented by dt and reset to zero once it reaches 2.
function rybickaupdate(dt)	-- rybicka animation
	rybickaframe=rybickaframe +1*dt
	if rybickaframe>2 then 
		rybickaframe=0 
	end
end

--eyeupdate(dt): This function updates the frame of an eye animation that lasts 2 frames. The eyeframe variable is incremented by dt and reset to zero once it reaches 2.
function eyeupdate(dt)	-- eye animation
	eyeframe=eyeframe +1*dt
	if eyeframe>2 then 
		eyeframe=0 
	end
end

-- strangecreatureupdate(dt): This function updates the frame of a strange creature animation that lasts 3 frames. The strangecreatureframe variable is incremented by dt and reset to zero once it reaches 3.
function strangecreatureupdate(dt)	-- creature level 4 animation
	strangecreatureframe=strangecreatureframe +1*dt
	if strangecreatureframe>3 then 
		strangecreatureframe=0 
	end
end

-- mutant level 44 animations  --
-- barelupdate(dt): This function updates the frame of a barrel animation that lasts 10 frames. The barelframe variable is incremented by dt and reset to zero once it reaches 10.
function barelupdate(dt)	-- barel animation
	barelframe=barelframe +1*dt
	if barelframe>10 then 
		barelframe=0 
	end
end

--bagetupdate(dt): This function updates the frame of the baguette animation by incrementing the bagetframe variable and resetting it to zero when it exceeds 2.
function bagetupdate(dt)	-- baget animation
	bagetframe=bagetframe +1*dt
	if bagetframe>1 then 
		bagetframe=0 
	end
end

--doubleupdate(dt): This function updates the frame of the double animation by incrementing the doubleframe variable and resetting it to zero when it exceeds 2.
function doubleupdate(dt)	-- double animation
	doubleframe=doubleframe +1*dt
	if doubleframe>2 then 
		doubleframe=0 
	end
end

--hadupdate(dt): This function updates the frame of the had animation by incrementing the hadframe variable and resetting it to zero when it exceeds 6.
function hadupdate(dt)	-- had animation
	hadframe=hadframe +1*dt
	if hadframe>6 then 
		hadframe=0 
	end
end

--hlubinnaupdate(dt): This function updates the frame of the hlubinna animation by incrementing the hlubinnaframe variable and resetting it to zero when it exceeds 4.5.
function hlubinnaupdate(dt)	-- hlubinna animation
	hlubinnaframe=hlubinnaframe +1*dt
	if hlubinnaframe>4.5 then 
		hlubinnaframe=0 
	end
end

--kachnaupdate(dt): This function updates the frame of the kachna animation by incrementing the kachnaframe variable and resetting it to zero when it exceeds 4.5.
function kachnaupdate(dt)	-- kachna animation
	kachnaframe=kachnaframe +1*dt
	if kachnaframe>4.5 then 
		kachnaframe=0 
	end
end

--killerupdate(dt): This function updates the frame of the killer animation by incrementing the killerframe variable and resetting it to zero when it exceeds 2.
function killerupdate(dt)	-- killer animation
	killerframe=killerframe +1*dt
	if killerframe>2 then 
		killerframe=0 
	end
end

--mutantkrabupdate(dt): This function updates the frame of the mutantkrab animation by incrementing the mutantkrabframe variable and resetting it to zero when it exceeds 2.
function mutantkrabupdate(dt)	-- mutantkrab animation
	mutantkrabframe=mutantkrabframe +1*dt
	if mutantkrabframe>2 then 
		mutantkrabframe=0 
	end
end

--kukajdaupdate(dt): This function updates the frame of the kukajda animation by incrementing the kukajdaframe variable and resetting it to zero when it exceeds 8.5.
function kukajdaupdate(dt)	-- kakajda animation
	kukajdaframe=kukajdaframe +1*dt
	if kukajdaframe>8.5 then 
		kukajdaframe=0 
	end
end

--mutantsharkupdate(dt): This function updates the frame of the mutantshark animation by incrementing the mutantsharkframe variable and resetting it to zero when it exceeds 2.
function mutantsharkupdate(dt)	-- shark animation
	mutantsharkframe=mutantsharkframe +1*dt
	if mutantsharkframe>2 then 
		mutantsharkframe=0 
	end
end

--nohaupdate(dt): This function updates the frame of the mutant foot animation by incrementing the nohaframe variable and resetting it to zero when it exceeds 5.5.
function nohaupdate(dt)	-- mutant foot animation
	nohaframe=nohaframe +1*dt
	if nohaframe>5.5 then 
		nohaframe=0 
	end
end


function mutanteyeupdate(dt)	-- dead parrot animation
	mutanteyeframe=mutanteyeframe +1*dt
	if mutanteyeframe>2 then 
		mutanteyeframe=0 
	end
end

function mnohonozkaupdate(dt)
	mnohonozkaframe=mnohonozkaframe +1*dt
	if mnohonozkaframe>2 then 
		mnohonozkaframe=0 
	end
end

function balonekupdate(dt)
	balonekframe=balonekframe +1*dt
	if balonekframe>2 then 
		balonekframe=0 
	end
end

function budikupdate(dt)
	budikframe=budikframe +1*dt
	if budikframe>2 then 
		budikframe=0 
	end
end


function meduzaupdate(dt)
	meduzaframe=meduzaframe +1*dt
	if meduzaframe>2 then 
		meduzaframe=0 
	end
end

function uhorupdate(dt)
	uhorframe=uhorframe +1*dt
	if uhorframe>2 then 
		uhorframe=0 
	end
end

function washmachineupdate(dt)
	washmachineframe=washmachineframe +1*dt
	if washmachineframe>5 then 
		washmachineframe=0 
	end
end
-- level 15
function dalekohledupdate(dt)
	dalekohledframe=dalekohledframe +1*dt
	if dalekohledframe>2 then 
		dalekohledframe=0 
	end
end

function mikroskopupdate(dt)
	mikroskopframe=mikroskopframe +1*dt
	if mikroskopframe>1.5 then 
		mikroskopframe=0 
	end
end

function broomupdate(dt)
	broomframe=broomframe +1*dt
	if broomframe>3 then 
		broomframe=0 broommoving=false
	end
end

function chobotniceupdate(dt)					--octopus animation
	chobotniceframe=chobotniceframe +1*dt
	if chobotniceframe>4.5 then 
		chobotniceframe=0
	end
end

function papouchaupdate(dt)					--octopus animation
	papouchaframe=papouchaframe +1*dt
	if papouchaframe>2 then 
		papouchaframe=0
	end
end

function snehulakupdate(dt)					--snowman animation
	snehulakframe=snehulakframe +1*dt
	if snehulakframe>3 then 
		snehulakframe=0
	end
end

function deloupdate(dt)					--Cannons animation
	deloframe=deloframe +1*dt
	if deloframe>1.5 then 
		deloframe=0
	end
end

function drahokamupdate(dt)					--drahoka animation
	drahokamframe=drahokamframe +1*dt
	if drahokamframe>1.5 then 
		drahokamframe=0
	end
end

function lebzaupdate(dt)					--skull animation
	lebzaframe=lebzaframe +1*dt
	if lebzaframe>2 then 
		lebzaframe=0
	end
end

function okoupdate(dt)					--eye animation
	okoframe=okoframe +1*dt
	if okoframe>2.5 then 
		okoframe=0
	end
end

function linuxakupdate(dt)					--Linux users animation
	linuxakframe=linuxakframe +1*dt
	if linuxakframe>1.4 then 
		linuxakframe=0
	end
end

function snekupdate(dt)					--Snails users animation
	snekframe=snekframe +1*dt
	if snekframe>1.4 then 
		snekframe=0
	end
end

function balalupdate(dt)					--Octopus animation
	balalframe=balalframe +1*dt
	if balalframe>11 then 
		balalframe=0
	end
end

function sepieupdate(dt)					--Sepie animation
	sepieframe=sepieframe +1*dt
	if sepieframe>1.4 then 
		sepieframe=0
	end
end

function pohonupdate(dt)					--squirrel animation
	pohonframe=pohonframe +1*dt
	if pohonframe>34 then 
		pohonframe=0
	end
end

function ufoupdate(dt)					--Alien animation
	ufoframe=ufoframe +1*dt
	if ufoframe>11 then 
		ufoframe=0
	end
end

function plutonium4update(dt)					--plutonium4 animation
	plutonium4frame=plutonium4frame +1*dt
	if plutonium4frame>3 then 
		plutonium4frame=0
	end
end

function hadiceupdate(dt)					--hadice animation
	hadiceframe=hadiceframe +1*dt
	if hadiceframe>2 then 
		hadiceframe=0
	end
end

function podstavecupdate(dt)					--alien platform animation
	podstavecframe=podstavecframe +1*dt
	if podstavecframe>4 then 
		podstavecframe=0
	end
end

function klavirupdate(dt)					--pianist octopus animation
	klavirframe=klavirframe +1*dt
	if klavirframe>9 then 
		klavirframe=0
	end
end

function rejnokupdate(dt)					--pianist octopus animation
	rejnokframe=rejnokframe +1*dt
	if rejnokframe>3 then 
		rejnokframe=0
	end
end

-- laboratory

function thinjarupdate(dt)					--Thin jar animation
	thinjarframe=thinjarframe +1*dt
	if thinjarframe>1.5 then 
		thinjarframe=0
	end
end

function porganismupdate(dt)					--Organism animation
	porganismframe=porganismframe +1*dt
	if porganismframe>6.25 then 
		porganismframe=0
	end
end

function qorganismupdate(dt)					--Organism 2 animation
	qorganismframe=qorganismframe +1*dt
	if qorganismframe>2 then 
		qorganismframe=0
	end
end

function okaupdate(dt)					--eyes animation
	okaframe=okaframe +1*dt
	if okaframe>7.5 then 
		okaframe=0
	end
end

function malaupdate(dt)					--Organism animation
	malaframe=malaframe +1*dt
	if malaframe>3.5 then 
		malaframe=0
	end
end

function mutantupdate(dt)					--Organism animation
	mutantframe=mutantframe +1*dt
	if mutantframe>5 then 
		mutantframe=0
	end
end

function sklenaupdate(dt)					--Hand animation
	sklenaframe=sklenaframe +1*dt
	if sklenaframe>6 then 
		sklenaframe=0
	end
end

function sklenaupdate(dt)					--Hand animation
	sklenaframe=sklenaframe +1*dt
	if sklenaframe>6 then 
		sklenaframe=0
	end
end

function horni_tvorupdate(dt)					--Hand animation
	horni_tvorframe=horni_tvorframe +1*dt
	if horni_tvorframe>3 then 
		horni_tvorframe=0
	end
end

function lahvacupdate(dt)					--Eyes bottle animation
	lahvacframe=lahvacframe +1*dt
	if lahvacframe>6 then 
		lahvacframe=0
	end
end

function lahvaccrashupdate(dt)					--Eyes bottle animation
	lahvaccrashframe=lahvaccrashframe +1*dt
	if lahvaccrashframe>1.5 then 
		lahvaccrashframe=0
		if lahvacstate=="crash" then lahvaccrashedframe=0 lahvacstate="crashed" end
	end
end

function lahvaccrashedupdate(dt)					--Eyes bottle animation
	lahvaccrashedframe=lahvaccrashedframe +1*dt
	if lahvaccrashedframe>1.75 then 
		lahvaccrashedframe=0
	end
end

-- level 63
function dasfishupdate(dt)					--dasfish animation
	dasfishframe=dasfishframe +1*dt
	if dasfishframe>4.5 then 
		dasfishframe=0
	end
end
-- level 72
function nahoreupdate(dt)					--Nahore animation
	if subbeat<0.01 and timer3>1 then nahoreframe=nahoreframe +1 end
	if nahoreframe>23 then 
		nahoreframe=0
	end
end

function shrimpupdate(dt)					--Velryb animation
	shrimpframe=shrimpframe +1*dt
	--if subbeat<0.01 and timer3>1 then velrybframe=velrybframe +1 konikframe=konikframe +1 end
	if shrimpframe>2.5 then 
		shrimpframe=0
	end
end

function antickaupdate(dt)					--Velryb animation
	antickaframe=antickaframe +1*dt
	--if subbeat<0.01 and timer3>1 then velrybframe=velrybframe +1 konikframe=konikframe +1 end
	if antickaframe>1.5 then 
		antickaframe=0
	end
end

function ballupdate(dt)					--Velryb animation
	ballframe=ballframe +1*dt
	--if subbeat<0.01 and timer3>1 then velrybframe=velrybframe +1 konikframe=konikframe +1 end
	if ballframe>2 then 
		ballframe=0
	end
end

function velrybupdate(dt)					--Velryb animation
	velrybframe=velrybframe +1*dt
	--if subbeat<0.01 and timer3>1 then velrybframe=velrybframe +1 konikframe=konikframe +1 end
	if velrybframe>11 then 
		velrybframe=0
	end
end

function konikupdate(dt)					--konik animation

	if konikframe>2 then 
		konikframe=0
	end
end


function semaforupdate(dt)					--Semafor bottle animation
		if subbeat<0.01 and timer3>1 then semaforframe=semaforframe+1 timer3=0
	end
	if semaforframe<0 or semaforframe>3 then 
		semaforframe=0
	end
end

function zupdate(dt)					--Turtle animation
	zframe=zframe +dt
		if turtlestatus=="idle"		 and zframe>4 then zframe=0
	elseif turtlestatus=="telepatic" and zframe>5 then zframe=0
	elseif turtlestatus=="lookaround" and zframe>11.5 then zframe=0
	elseif turtlestatus=="hide"		 and zframe>4.5 then zframe=0
	elseif turtlestatus=="lookunder" and zframe>5 then zframe=0
	end
end

--[[
The function smallfishupdate(dt) updates the animation frame of a small fish.
If the variable boresentencesinterrupted is true, the small fish animation may be synchronized to the music on level 38 by increasing the animation frames at specific times,
indicated by the variable subbeat. Otherwise, the small fish animation is animated normally.
The variable fish2status is used to indicate the current status of the small fish, which can be "switch", "moving", "talking", or "idle". If the small fish is in the "switch" status,
the animation has 4 frames and loops. Otherwise, the animation has 2 frames and loops.
Finally, after 1 second of inputtime has elapsed, the small fish switches from "moving" status to "idle" status.
--]]
function smallfishupdate(dt)
	--[[if boresentencesinterrupted==true then			-- if bored and level 38 sync them to the music
		--dance to the rythm on level 38
		if nLevel==38 then
			--if timer>39 and timer <74 then		 -- dance only between 39 and 74 seconds
				if subbeat<0.01 then  
					smallfishframe=smallfishframe+500*dt
					bigfishframe=bigfishframe+500*dt
					timer3=0 
				end
			--else smallfishframe=smallfishframe+1*dt
			--end
		end
	else											-- if not bored and on level 38 animate normally
	--]]
	smallfishframe=smallfishframe+1*dt
	
		if fish2status=="switch" then 
			if smallfishframe>4 then smallfishframe=0 end
		else
			if smallfishframe>1.45 then smallfishframe=0.001 end
		end
		
			if inputtime>1 and fish2status=="moving" then fish2status="idle" -- wait 1 second to change fish from swimming status to idle
		--elseif inputtime>10 and fish2status=="talking" then fish2status="idle" -- wait 10 seconds to change fish from talking status to idle
		end
		
	--end												-- end of the boresentencesinterrupted condition
end

--[[
This is a Lua function called "bigfishupdate" that updates the animation frames and status of a character known as the "bigfish."
The function takes a time delta as input (dt) which is used to calculate the time difference between frames.
 The code contains an if statement commented out that checks if the variable "boresentencesinterrupted" is true and if the current level is 38, however, it is currently disabled.
 The animation frame is updated by incrementing "bigfishframe" by 1 multiplied by dt. 
 There is another if statement that checks the status of the "fish1status" variable, and if it is set to "switch," the bigfish animation frame is set to 0 if it is greater than 4.
  Otherwise, if it is set to any other value, the bigfish animation frame is set to 0.001 if it is greater than 1.45.
  Finally, there is another if statement that checks the input time and sets the "fish1status" variable to "idle" if the input time is greater than 1 and the current status is "moving."
--]]
function bigfishupdate(dt)
	--[[if boresentencesinterrupted==true then			-- if bored and level 38 sync them to the music
		--dance to the rythm on level 38
		if nLevel==38 then 
			--if timer<39 or timer >74 then 		-- move normally till 39 and from 74 seconds on
				bigfishframe=bigfishframe+1*dt
			--end
		--else bigfishframe=bigfishframe+1*dt		-- if not level 38 normal animation
		end
	else
	--]]										 	-- if not bored and on level 38 animate normally
	bigfishframe=bigfishframe+1*dt
	
		if fish1status=="switch" then 
			if bigfishframe>4 then bigfishframe=0 end
		else
			if bigfishframe>1.45 then bigfishframe=0.001 end
		end
		
			if inputtime>1 and fish1status=="moving" then fish1status="idle" -- wait 1 second to change fish from swimming status to idle
		--elseif inputtime>10 and fish1status=="talking" then fish1status="idle" -- wait 10 seconds to change fish from talking status to idle
		end
	--end												-- end of the boresentencesinterrupted condition
end

--[[The first three functions (fish1talk, fish2talk, fishstop) are related to controlling the behavior of two fish characters in a game. fish1talk sets the status of the first fish character to "talking" and the status of the second fish character to "idle". fish2talk does the opposite, setting the status of the second fish character to "talking" and the status of the first fish character to "idle". fishstop sets the status of both fish characters to "idle".--]]
function fish1talk()
	fish1status="talking"
	fish2status="idle"
end

function fish2talk()
	fish1status="idle"
	fish2status="talking"
end

function fishstop()
	fish1status="idle"
	fish2status="idle"
end

-- statue
--The next two functions (statuetalk and statuestop) are related to controlling the behavior of a statue character in the same game. statuetalk sets the status of the statue character to "talking". statuestop sets the status of the statue character to "idle".
function statuetalk()
	statuestatus="talking"
end

function statuestop()
	statuestatus="idle"
end

-- linux users
--The final three functions (linuser1talk, linuser2talk, linusersstop) are related to controlling the behavior of two Linux user characters in the same game. linuser1talk sets the status of the first Linux user character to "talking" and the status of the second Linux user character to "idle". linuser2talk does the opposite, setting the status of the second Linux user character to "talking" and the status of the first Linux user character to "idle". linusersstop sets the status of both Linux user characters to "idle".
function linuser1talk()
	linuser1status="talking"
	linuser2status="idle"
end

function linuser2talk()
	linuser1status="idle"
	linuser2status="talking"
end

function linusersstop()
	linuser1status="idle"
	linuser2status="idle"
end
