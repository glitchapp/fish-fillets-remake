--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--love.filesystem.setIdentity("Fish-fillets")

	-- Get the desktop dimensions and set the game window size to 3/4 of the desktop size
		desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	
	-- load the function that prepare the maps
	require("game/levels/prepareMap")

-- Load Talkies dialog library and example functions
Talkies = require ("lib/talkies/talkies")				--Load dialogs library
Obey = require("lib/talkies/example/other")				--Functions that render the dialogs

-- Load the main states of the game
require ('game/states/mainStates')
loadStates()


-- Load the save mechanism functions
require ('game/mainfunctions/savegame')
-- Load saved game if it exists
savegameload()

--[[ Check if the language settings file exists, if not, save it
-- (This block of code is currently commented out)
info = love.filesystem.getInfo("language.txt")
if not (info) then
	print("No settings saved, settings will be saved now")
	savemygame()
end
--]]

	require ("game/mainfunctions/ErrorHandling")
	
	red,green,blue, sunIntensity = 1, 1, 1, 1
	sunAngle = 190
	require ("combinedShader")
	require ("causticsAndNormals")
	
	
	--normalsWarp:send("LightPos", {0.1, 0.1, 0.075})  -- Set light position in top-left
	lightPos1 = {0.1, 0.1, 0.075}
	lightPos2 = {0.1, 0.1, 0.075}
	normalsWarp:send("LightPos1", lightPos1)  -- Set light position in top-left
    normalsWarp:send("AmbientColor", {1.0, 1.0, 1.0, 0.6})  -- Example ambient color
    normalsWarp:send("Falloff", {1.0, 0.1, 0.01})  -- Adjust these values for attenuation
	
-- Load main game configuration
	require ("config")
	require ("game/mainfunctions/globalvariables")

-- Load achievements
	require("/game/mainfunctions/achievements")
	loadachievements()

-- Load external libraries
	require ("lib/tesound")		-- Sound library
	WebP = require("lib/love-webp/love-webp")			-- webp library for love
	
	--socket = require("socket")		
	--server = assert(socket.bind("*", 8888))
	--client = server:accept()			
    
    --require("game/mainfunctions/lazyloading")
    --vivid = require ("lib/vivid")								-- Color manipulation library

-- Load assets and animations
	require("game/mainfunctions/loadAssets")	-- Function to check if assets are loaded in memory and load them if not
	require ("lib/love-boids/boids")			-- Reynolds' boid library (https://github.com/Jehadel/love-boids)
	initDemo() 									-- init boids
	require("game/mainfunctions/loadingProgress")
    require("assets/loadassets")
    require("assets/loadexternalassets")
    loadavatars()
    require("game/animatefauna")



	require("game/interface/loading")
	require("game/interface/Transition/Transition")
	require("game/interface/Transition/TransitionUpdate")
	require("game/interface/Transition/TransitionDraw")
	
	require("game/interface/TransitionLogo/TransitionLogo")
	require("game/interface/TransitionLogo/TransitionLogoUpdate")
	require("game/interface/TransitionLogo/TransitionLogoDraw")
	

	require("game/interface/hints")
	loadHintButtons()
	require("game/interface/screenshot")
	loadscreenshotdata()
	loadingvariables()								-- load loading icon and variables

-- Load level-specific variables
	require("game/levels/loadlevelvariables")
	loadlevelvariables()				
	require("game/levels/setagentinitialdirection")

-- Load level selection menu
--levelmenu = require ('game/levelmenu')
require ('game/About')

-- Load level completed menu
require ('game/levelcompleted')
loadlevelcompleted()

-- Load options menu and sub-menus
optionsmenu = require ('game/optionsmenu')		-- Load options menu
require ('game/interface/optionslanguages')		-- Load languages options
require ('game/interface/optionsaudio')			-- Load sound options
require ('game/interface/optionscontrols')		-- Load control options
require ('game/interface/optionsgraphics')		-- Load graphic options
require ('game/interface/optionsgraphicshub')	-- Load HUB options

-- Load FPS graph library and create graphs
	fpsGraph = require "lib/fpsgraph/FPSGraph"
	testGraph = fpsGraph.createGraph()	-- fps graph
	benchmarkGraph = fpsGraph.createGraph(1600,0,250,150,0.5,true)	-- benchmark fps graph			(x, y, width, height, delay, draggable)
	memoryGraph = fpsGraph.createGraph(1600,200,250,50,0.5,true)	-- memory graph
	testGraph3 = fpsGraph.createGraph(0, 60)		-- random graph

-- Load extras menu
--extrasmenu = require ('game/extras')

-- Load credits menu
--creditsmenu = require ('game/credits')

-- Load music player
musicplayer = require ('game/musicplayer')

-- Load saved settings for various menus
creditsload()
musicload()
optionsload()
extrasload()



	--multiresactivated=true
	--local rs = require ('lib/resolution_solution')		-- multiresolution library
	--rs.init({width = 1920, height = 1080, mode = 3})
	--rs.setMode(1920, 1080, {resizable = true})
	if game==nil then	-- prevent game from crashing due to not saved resolution and game not initialized
		game = {}
		--game.screen_width = 1920
		--game.screen_height = 1080
		game.screen_width = 2560
		game.screen_height = 1440
	end

		--love.window.setMode (3/4*desktopWidth, 3/4*desktopHeight, {resizable=true, borderless=false})
--rs.setMode(3/4*desktopWidth, 3/4*desktopHeight, {resizable = false})

--[[This section of the code is responsible for loading the necessary libraries and files when the game starts.
 It first loads the GUI library (loveframes) and a library for smooth animations (tween). (Commented and not currently used)
 It also loads a text file for terminal instructions, a library for the terminal, and the main game library.
 The code then loads the LoveBPM library for music analysis and sets the default BPM.
 The game window size is set based on the resolution settings.
 The necessary game functions and libraries are then loaded, including functions for benchmarking, changing the resolution, key input, gamepad input, mouse input, and shake detection.
 The first level is loaded, along with assets drawing logic, touch interface, and help functionality. Level-specific functionality is loaded for each level,
 including loading level music, sounds, assets, subtitles, and voices.
 Cutscenes and dialogs are also loaded, including a Talkies dialog library and example functions.
 The code also disables VSync and automatically loads saved games if the autoload setting is true in the config file.
--]]

-- Load the necessary libraries and files on game start
function love.load()
 
	--loveframes = require("lib/loveframes/loveframes") -- GUI library for Love2D
	--tween = require("lib/loveframes/tween") -- Library for smooth animations
	require("assets/text/terminalinstructionstext") -- Text file for terminal instructions
	Terminal = require("lib/lv100/terminal") -- Terminal library
	require("lib/lv100/main") -- Main game library
	loadinstructions() -- Load instructions for the game
	
	require("lib/lovebpm/main") -- LoveBPM library for music analysis
	mybpm = 112 -- Default BPM

-- Load the music based on the skin and autoload settings
if autoload then
		if skin == "remake" or skin == "retro" then
			lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg")
		elseif skin == "classic" and autoload then
			lovebpmload("/externalassets/music/classic/menu.ogg")
		end
	end


-- Load the necessary game functions and libraries
	-- require ('/game/mainfunctions/createcanvas')  					-- Canvas (commented out)
	--require ('/game/mainfunctions/benchmark')				 			-- Benchmark
	require ('/game/mainfunctions/changeresolution')					-- Resolution change
	
	-- Gamepad input files
	require ('/lib/keymap/main')					  					-- Keymap input library
	loadkeymap()
	
	--gamepad
	
	require ('/game/mainfunctions/gamepadinput')  						-- gamepad input
	loadcontrollermappings()											-- load controller mappings
	
	
	require ('/game/mainfunctions/keyinput')  							-- key input
	keyinputload()														-- load all key input files from game/mainfunctions/keypad/
	
	-- Touch controls
	require ('/game/mainfunctions/touchcontrols')  						-- touch input
	
	require ('/game/mainfunctions/mouseinput')  		-- mouse input
	mouseinputload()
	
	require ('/game/mainfunctions/HandleWindowDragging')
	
	require ('/lib/ShakeDetector/myshakedetector')  	-- shake detector
	--require ('/lib/netcontrols/netcontrolserver') 		-- Remote network control server (called if enabled from input menu)
	shakeload() -- Load the shake detector library

local level = require('game/levels/level-1')	-- Load the first level
	levelload() -- Load the level
	
	-- Load 3DreamEngine graphics (if not on Android)
	if android == false then
		require("game/levels/3dlevel")
	end

-- load rewind feature
--require ('game/logic/rewind')
--loadRewindFeature()

-- Load push-blocks game logic
pb = require ('game/logic/push-blocks')

-- DFS Algorithm
require ('game/logic/DFS_Algorithm')

-- Load game events logic
require ('game/logic/events')

-- Load game assets drawing logic
require ('game/logic/drawassets')				-- draw the assets with the game logic
require ('game/logic/drawassets30')				-- draw the assets after level 30 with the game logic
require ('game/logic/drawassets60')				-- draw the assets after level 59 with the game logic
require ('game/logic/drawlips')					

-- Load touch interface and help functionality
require "game/interface/touchinterface1"
loadtouchtext()
require "game/interface/showhelp"

-- Load level-specific functionality

	require("game/levels/changelevel")
	require("game/levels/loadlevelmusic")
	require("game/levels/loadlevelmusicclassic")
	require("game/levels/loadlevelsounds")
	require("game/levels/assignfrgbck")
	require("game/levels/assignfrgbckclassic")
	
	require("game/levels/loadlevelassets")		
	require("game/levels/loadlevelassets40")
	require("game/levels/loadlevelassetsclassic")			-- load level assets
	require("game/levels/loadlevelassetsclassic40")
	require("game/levels/loadlevelassetsclassic60")
	require("game/levels/loadsubtitles")
	require("game/levels/loadvoiceExceptions")				-- assign english dubs to prevent game from crashing due to missing dubs
	require("game/levels/loadvoices")
	require("game/levels/loadvoices20")
	require("game/levels/loadvoices40")
	require("game/levels/loadvoices60")
	require("game/levels/SetUpDialogsVolume")
	require("game/levels/loadleveltitle")

-- Load cutscenes functionality
	require ('/game/states/Cutscenes/Cutscenes')	-- Load cutscenes



-- Load subtitle syncing functionality
require("game/dialogs/subtitlessync1")
-- Set initial step to 0
stepdone=0

-- Load game over functionality
--require ('game/gameover')
--loadgameover()

-- Load bore dialogs
require("game/dialogs/borescript")		--bore dialogs
loadborescript()

-- Load border dialogs
require("game/dialogs/borderscript")		--border dialogs
loadborderscript()

-- Disable VSync
love.window.setVSync(false)

-- If autoload is set to true in config.lua, load saved game automatically
if autoload==true then

	loadmygame()
	changeresolution()
	
	levelload()
	
else
	--Start Splash screen
	
	--gamestatus="splashScreen"
	-- Start intro scene if no saved data (first start)
	nLevel=101 
	gamestatus="firststart" 
	love.graphics.setFont( font3 )
	screensaver=true
	changelevel()	
	caustics=true	-- set up caustics and dialogs on on first start
	talkies=true
	mousestate = not love.mouse.isVisible()
	love.mouse.setVisible(mousestate)
	--load extra assets
	kukajda_00_3000 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level44/kukajda_00_3000.webp")))
	snail1280 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level7/snail1280.webp")))		

end

--[[This section of code includes several shader effects to enhance the graphics of the game using the Moonshine library. The first part defines the parameters for the shaders based on the screen resolution.
-- The effect is a chain of Moonshine effects that are enabled or disabled based on the game state.
--]]

-- Import Moonshine library and its various effects
moonshine = require 'assets/shaders/moonshine'
-- Define Moonshine effect chains based on screen resolution
		if res=="1080p" then effect = moonshine(1920,1080,moonshine.effects.filmgrain)
					.chain(moonshine.effects.colorgradesimple)
					.chain(moonshine.effects.vignette)
					.chain(moonshine.effects.scanlines)
					.chain(moonshine.effects.crt)
					.chain(moonshine.effects.dmg)
					.chain(moonshine.effects.godsray)
					.chain(moonshine.effects.desaturate)
					.chain(moonshine.effects.pixelate)
					.chain(moonshine.effects.posterize)
					.chain(moonshine.effects.boxblur)
					.chain(moonshine.effects.fastgaussianblur)
					.chain(moonshine.effects.chromasep)
					.chain(moonshine.effects.glow)
	elseif res=="1440p" then effect = moonshine(2560,1440,moonshine.effects.filmgrain)
					.chain(moonshine.effects.colorgradesimple)
					.chain(moonshine.effects.vignette)
					.chain(moonshine.effects.scanlines)
					.chain(moonshine.effects.crt)
					.chain(moonshine.effects.dmg)
					.chain(moonshine.effects.godsray)
					.chain(moonshine.effects.desaturate)
					.chain(moonshine.effects.pixelate)
					.chain(moonshine.effects.posterize)
					.chain(moonshine.effects.boxblur)
					.chain(moonshine.effects.fastgaussianblur)
					.chain(moonshine.effects.chromasep)
					.chain(moonshine.effects.glow)
	end
					
-- Enable or disable Moonshine effects based on game state
effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
		effect.enable("scanlines","crt","glow","filmgrain")
-- Set parameters for specific Moonshine effects
		effect.scanlines.opacity=0.1
		effect.glow.min_luma = 1
		effect.filmgrain.size=0.5
		effect.filmgrain.opacity=1

--[[-- This section defines various other shader effects, including waterwarp effects, and loads a help file.
-- It also defines the game state and level, and sets up various graphics settings.
--]]

require 'shaders/normals/NormalsAndNoiseAndShadowsandEnvmap'	-- Normals, noise, shadows and reflections

-- Import waterwarp library and its various effects
waterwarp = require 'shaders/waterwarp'	--warp effect intensity 10
waterwarp2 = require 'shaders/waterwarp2' --warp effect intensity 15
waterwarp2 = require 'shaders/waterwarp2_2' --warp effect intensity 15
waterwarp3 = require 'shaders/waterwarp3' --warp effect #vec2(2, 2) // / 12;
waterwarp4 = require 'shaders/waterwarp4' --warp effect vec2(1, 20) // / 12;

-- Import blue noise dithering shader created by chatGPT
--bluenoise = require '/shaders/zblueNoise/bluenoise'
--bluenoise = require '/shaders/zblueNoise/bluenZx'

-- Import help file
require ("assets/text/help")

-- Get dimensions of screen and window
	local ddwidth, ddheight = love.window.getDesktopDimensions( display )
	width, height = love.graphics.getDimensions( )

-- Set screen options to false
    screenoptions=false

-- Set game status based on whether autoload is true
	if autoload then
		gamestatus="levelselection"
	end

-- If autoload is true, set nLevel to 1 and load the level
	if autoload then 
		nLevel = 1 
	end
	pb:load (level)							-- load level from the game logic

-- Load wall borders and other contour highlights
    require ("game/levels/drawborders")		
	
-- Load borders for the canvas if autoload is true
	if autoload then 
		loadwallborder() 
	end	
	
	-- Make the default mouse invisible if autoload is true
	if autoload then
		love.mouse.setVisible(true)
	end

-- Ensure that the palette is assigned an integer value to prevent errors
palettemustbeinteger()
	-- init screenwidth and screenheight
	screenwidth, screenheight, flags = love.window.getMode( )
	
	-- start gamepad emulated mouse centered and above
	emulatedmousex=screenwidth/3
	emulatedmousey=screenheight/6
	
end


function love.update(dt)

updateStates(dt)

	--resolution solution update
	--love.resize = function(w, h)
--		rs.resize(w, h)
--	end

	keymapupdate(dt)
  	HideShowmouse()			-- Toggle mouse / thumbstick based pointer (second mouse button / thumbstick)

	
	if showAnalogClock==true then
		updateAnalogClock(dt)
	end

--[[The following code is a series of conditional statements that update
 different aspects of the game based on the current gamestatus.--]]



--[[
This part of the code is responsible for updating the game state when it is in "music" mode. It first calls the musicupdate function to update the music, followed by an equalizerupdate function if a CD is currently playing. It then updates the FPS metrics using the updatefpsmetrics function. Finally, it applies a framerate limit, which is based on the value of limitframerate.

If the limitframerate variable is set to 30, the code will check if dt is less than 1/15. If it is, the code will sleep for a short period of time (1/17 - dt) to limit the framerate. If limitframerate is set to 60, the code will check if dt is less than 1/60 and sleep for a short period of time (1/40 - dt) to limit the framerate.

Overall, this part of the code ensures that the game state is properly updated in "music" mode, including the music and equalizer, and that the framerate is limited to ensure smooth gameplay.
--]]

    
    
      
        
        --[[
        This part of the code handles the game status of "options" and performs the necessary updates for this mode.
        The code updates the FPS metrics and applies the framerate limit based on the limit set to either 30 or 60 frames per second. 
        Additionally, when the graph button is being hovered, a timer is started to display information after a certain amount of time.
        --]]
        


if gamestatus=="cutscenes" then cutscenesupdate(dt) -- if the game is in cutscenes mode, update the cutscenes
updategetjoystickaxis()
updateButtonFocus(dt)	-- sparkle animations


elseif gamestatus=="gameover" then         -- update game over
updategameover()
updategetjoystickaxis()

-- If the gamestatus is "levelcompleted", update BPM and FPS metrics
elseif gamestatus=="levelcompleted" then
	updategetjoystickaxis()
	lovebpmupdate(dt)
	timer = timer + dt
	updatefpsmetrics(dt)
	updateButtonFocus(dt)

-- If the gamestatus is "assets", set shader2 to false and update term
elseif gamestatus=="assets" then
	shader2=false
	term:update(dt)
	
-- If the gamestatus is "instructions", update instructions
elseif gamestatus=="instructions" then
	instructionsupdate(dt)
	
-- If the gamestatus is "intro", update intro
elseif gamestatus=="intro" then
	introupdate(dt)
	radialmenu=false



--[[
This code is part of a game loop and updates different game statuses depending on the current game status.
If the game status is "endmenu", "fishhouseend", "silversshipend", "cityinthedeepend", "ufoend", "coralreefend",
 "treasurecaveend", "barrelend", "secretcomputerend", "linuxend" or "gameend",
  the timer is updated with the delta time.
  In addition, depending on the game status, specific functions are called to update the corresponding game elements.
  If the player is playing the game and the game status is "ufoend", the BPM is also updated.
--]]
		

end



end

debugText = {}  -- Store debug text lines in a table

function printDebugText()
 local debugTextY = 10  -- Initial Y position for the debug text
    local textPadding = 10  -- Padding around the text

    -- Calculate the width and height of the rectangle based on the text content
    local maxWidth = 0
    for i, line in ipairs(debugText) do
        local lineWidth = poorfishsmall:getWidth(line)
        maxWidth = math.max(maxWidth, lineWidth)
    end
    local rectWidth = maxWidth + 40 * textPadding
    local rectHeight = #debugText * 20 + 2 * textPadding

    -- Calculate the position for the rectangle
    local rectX = 10
    local rectY = debugTextY - textPadding

    -- Draw the white rectangle
    love.graphics.setColor(1, 1, 1,0.8)  -- Set color to white
    love.graphics.rectangle("fill", rectX, rectY, rectWidth, rectHeight)

    -- Set color for the text
    love.graphics.setColor(0, 0, 0)  -- Set color to black

    -- Draw the debug text
    for i, line in ipairs(debugText) do
        love.graphics.print(line, rectX + textPadding, debugTextY)
        debugTextY = debugTextY + 20  -- Adjust the vertical spacing
    end
     love.graphics.setColor(1, 1, 1)  -- Set color to black
end

function love.draw(dt)

	love.graphics.push()
	
	-- scale horizontally
	love.graphics.scale(love.graphics.getWidth() / game.screen_width)
	
	-- or scale vertically
	-- love.graphics.scale(love.graphics.getHeight() / game.screen_height)

		drawstates()
	love.graphics.pop()
			if debugmode=="yes" then
				printDebugText()
			end

-- Draw loading icon if loading
if isloading then
    loadingdraw(dt)
end

drawfpsmetrics() -- Draw FPS metrics
drawtimeanddate()	-- Draw time and date if enabled in HUB

--[[ This function displays the battery status on the screen if the showbattery variable is set to true.
It uses the love.graphics.print() function to display the battery state, percentage, and remaining time
at specific positions on the screen using a small font. The battery state is represented by a battery emoji 🔋.--]]

-- Show battery status if enabled
if showbattery then
    love.graphics.setFont(poorfishsmall)
    love.graphics.print("🔋: " .. powerstate, 1200, 50)
	love.graphics.print (powerpercent .. "%", 1400,50)
	love.graphics.print (powerseconds .. "s", 1550,50)
end

if joystick and (gamestatus=="levelselection" 
				or gamestatus=="options" 
				or gamestatus=="gameplusoptions" 
				or gamestatus=="music"  
				or gamestatus=="extras" 
				
				or gamestatus=="creditsend" 
				or gamestatus=="cutscenes" 
				or gamestatus=="gameover"
				or gamestatus=="levelcompleted"
				
				
				or gamestatus=="endmenu" 
				or gamestatus == "fishhouseend"
    
				or gamestatus == "shipwrecksend"
				or gamestatus == "silversshipend"
				or gamestatus == "cityinthedeepend"
				or gamestatus == "ufoend"
				or gamestatus == "coralreefend"
				or gamestatus == "treasurecaveend"
				or gamestatus == "barrelend"
				or gamestatus == "secretcomputerend"
				or gamestatus == "linuxend"
				or gamestatus == "gameend")
				then 
	if axistouchedonce then drawemulatedmouse() end
end

if radialmenu and not (gamestatus=="intro") then
	drawradialmenu()
end
--rs.stop()
end

--[[Resume of metrics functions

The code defines two functions: updatefpsmetrics(dt) and drawfpsmetrics(). 

The purpose of these functions is to display FPS metrics on the screen.

updatefpsmetrics(dt) function updates the FPS metrics and takes the current delta time as input. It checks if showfps is true, and if it is, it retrieves the FPS using the love.timer.getFPS() function and updates a graph using the fpsGraph.updateFPS() function.

drawfpsmetrics() function draws the FPS metrics on the screen. It checks if showfps is true, and if it is, it calls the fpsGraph.drawGraphs() function to draw the graph.

Overall, the code provides a way to track the FPS of the game and display it on the screen.
--]]

-- This function updates the FPS metrics and graph
function updatefpsmetrics(dt)
	if showfps then
		fps = love.timer.getFPS() -- get FPS for metrics
		fpsGraph.updateFPS(testGraph, dt) -- update graphs using the update functions included in fpsGraph
	end
end

-- This function draws the FPS graph
function drawfpsmetrics()
	if showfps then
		fpsGraph.drawGraphs({testGraph}) -- draw FPS graph
	end
end

-- This function draws time and date
function drawtimeanddate()
	if showtimedate then
		love.graphics.print (os.date("%H:%M:%S"),1700,50)
		love.graphics.print (os.date("%a %d %b %y"),1700,100)
	end
	if showAnalogClock==true then
		drawAnalogClock()
	end
end


