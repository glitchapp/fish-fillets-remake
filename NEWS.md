Januar 2024

-- Added love-boids library (https://github.com/Jehadel/love-boids), a Reynolds'boids algorithm implementation.
Reynlods' boids algorithm provide realistic-looking representations of flocks of birds and other creatures, such as schools of fish or herds of animals.

November 2023

-- Added instructional guide with ilustrations and screenshots

-- Added alternative controller layout

-- Corrected game logic bug for level 19.

-- Added function to load custom levels by draging and droping them to the game's window

-- Added instructions and function to cross save and transfer game between different games instances of Fish fillets remake 2d and Fish fillets Vr.

-- Smooth transitions between the level selection menu and the levels

September 2023

-- Level editor added. Creating custom levels without coding is now possible.

August 2023

-- Game is almost fully playable without mouse /touch input (gamepad and keyboard control for the menus added)

-- You can switch tabs on the options menu with the gamepad and keyboard (left and right shoulder buttons / arrows)

-- Button focus animations for the gamepad and keyboard menu controls added.

-- Skulls dubs activated (dialogs will be triggered when you push the skulls if the main script has been played)

-- End credits scene added (it needs to be unlocked).

-- Some bugs in the music player solved


Juni 2023

New languages and translations added: Danish, norwegian, icelandic, chinese, japanese, thai.

-- Mai 2023

- New "blue noise dithering shader" added to the graphic effects.

- Options menu is now transparent when triggered while playing a level, this allows to see changes in realtime.

- Music player features animations while playing music.

- Many bugs related to old and new introduced features corrected.


-- April 2023
The following files and the functions within them have been commented and explained in an effort to improve the readability of the code:

main.lua: This is the main file which sets the environment and initialize the main functions and variables for the game.
shaders.lua: In charge of the fancy effects
game/levelmenu.lua: Level selection menu
game/options.lua: options menu
game/assignfrgbck.lua: Assign graphics and effects to each level.
game/logic.push-blocks: Game logic.

New graphic options added: 
- RGB palette can now be changed (through color presets or manually)
- New chromatic aberration effect
- vSync
- FPS hub
- Framerate can be limited
- Battery hub
- New benchmark tool has been added: it rates performance and suggest options to improve performance and achieve a stable framerate.

Fish fillets remake October 2022

* I.A. upscaled classic graphics mode added.

Fish fillets remake September 2022

* Classic graphics mode added.

Fish fillets remake July 2022

* Vibration for game controllers added

* 02.05.2022 - British English dub for the first fish completed

* 19.05.2022 - New option: Accents for the same language can be mixed

* 04.06.2022 - European French dub for the first fish completed

* 09.06.2022 - Chinese translation started. Level's names and script for the level 1 translated.
