-- License CC0 (Creative Commons license) (c) darkfrei, 2022
-- push-blocks

local path = 'assets/players/'

local pb = {}

local function setBlockOutline (block)
	local m = {} -- map of outlines
	local tiles = block.tiles -- list of tiles as {x1,y1, x2,y2, x3,y3 ...}
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if not m[y] then m[y] = {} end
		if not m[y][x] then 
			m[y][x] = {v=true, h=true} 
		else
			m[y][x].v = not m[y][x].v
			m[y][x].h = not m[y][x].h
		end
		
		if not m[y][x+1] then 
			m[y][x+1] = {v=true, h=false} 
		else
			m[y][x+1].v = not m[y][x+1].v
		end
		
		if not m[y+1] then m[y+1] = {} end
		if not m[y+1][x] then 
			m[y+1][x] = {v=false, h=true} 
		else
			m[y+1][x].h = not m[y+1][x].h
		end
	end
	local lines = {}
	for y, xs in pairs (m) do
		for x, tabl in pairs (xs) do
			if m[y][x].v then
				table.insert (lines, {x,y, x,y+1})
			end
			if m[y][x].h then
				table.insert (lines, {x,y, x+1,y})
			end
		end
	end
	block.lines = lines
end

function pb:load (level)
--	print (level.name)
	local width, height = love.graphics.getDimensions()
	self.map = level.map
	self.gridWidth = level.w
	self.gridHeight = level.h
	self.gridSize = math.min(width/(level.w), height/(level.h))
	--[[print ('gridWidth: ' .. self.gridWidth, 
		'gridHeight: ' .. self.gridHeight,
		'gridSize: ' .. self.gridSize)--]]
	
	self.blocks = level.blocks
	for i, block in ipairs (self.blocks) do
		setBlockOutline (block)
	end
	self.agents = level.agents
	for i, agent in ipairs (self.agents) do
		setBlockOutline (agent)
		local filename = path..agent.name..'.png'
		local info = love.filesystem.getInfo(filename)
		if info then
			local image = love.graphics.newImage(filename)
			agent.image = image
			local width, height = image:getDimensions( )
			local scale = self.gridSize/(width/agent.w)
			
			agent.image_scale = scale
			
			agent.image_sx = scale
			agent.image_sy = scale
			agent.image_ox = image:getWidth()/2
			agent.image_oy = image:getHeight()/2
			agent.image_dx = agent.image_ox*agent.image_scale
			agent.image_dy = agent.image_oy*agent.image_scale
		end
	end
	self.activeAgentIndex = 1
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
		
		--silhouette1= love.graphics.newImage("/assets/players/silhouette1.png")
		--silhouette2= love.graphics.newImage("/assets/players/silhouette2.png")
		
end

function pb:switchAgent ()
	self.agent.active = false
	local index = self.activeAgentIndex + 1
	if index > #self.agents then
		index = 1
	end
	self.activeAgentIndex = index
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
end


local function isValueInList (value, list)
	for i, element in ipairs (list) do
		if element == value then return true end
	end
	return false
end

function pb:isBlockToMapCollision (block, dx, dy)
	local x, y = block.x, block.y
	local map = self.map
	for i = 1, #block.tiles-1, 2 do
		local mapX = x + block.tiles[i]   + dx
		local mapY = y + block.tiles[i+1] + dy
		if map[mapY][mapX] then return true end
	end
end

function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end


--testing
function pb:getCollisionExit (Agent)
for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isCollisionAgentToAnyExitArea (agent) then
		 return true -- collision with exit
		end
	end
end
	
function pb:getCollisionBlocks (blockA, blocks, dx, dy)
	-- agent to map or block to map collision
	if self:isBlockToMapCollision (blockA, dx, dy) then
		return false
	end
	
	for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return false -- cannot move any agent

		end
	end
	
	for i, block in ipairs (self.blocks) do
		if block == blockA then
			-- self collision: do nothing
		elseif isValueInList (block, blocks) then
			-- block is already in list: do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			-- checks if the agent is strong
			if block.heavy and not self.agent.heavy then
			--script that trigger dialogs when small fish try to push the pipe in the first level
				if nLevel==1 and block.name=="steel-pipe-1x81" and pipetouched==false then
							
							
						whatwasthat:stop() 
						ihavenoidea:stop()
						weshouldgoandhave:stop()
						waitimgoingwithyou:stop()
						
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.Icantgetthrough() end
				
					if pipepushed==true then wowyoumovedit:stop() end
				
									pipetouched=true
							
								
				end
				--script that trigger dialogs when small fish touch short steal pipe on level 3
				if nLevel==3 and block.name=="shortpipe2" and shortpipetouched==false then
					if language=="en" then
						if accent2=="br" then
							youknowtherulesf:stop() youknowtherulesm:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/en/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						elseif accent2=="us" then
							youknowtherulesf:stop() youknowtherulesm:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/en-us/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						end
						elseif language=="es" then
						if accent2=="es" then
							youknowtherulesf:stop() youknowtherulesm:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/es/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						elseif accent2=="la" then
							youknowtherulesf:stop() youknowtherulesm:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/es/es-la/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
						end
						
					end
					if language2=="pl" then
							youknowtherulesf:stop() youknowtherulesf:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/pl/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
					elseif language2=="nl" then
							youknowtherulesf:stop() youknowtherulesf:stop()
							shortpipetoucheddialog = love.audio.newSource( "/externalassets/dialogs/level3/nl/icantmovesteel.ogg","stream" )
							shortpipetoucheddialog:setEffect('myEffect')
							shortpipetoucheddialog:play()
							shortpipetouched=true
					end
				end
				return false
			end
			table.insert (blocks, block)
			
			-- make it deeper!
			if not self:getCollisionBlocks (block, blocks, dx, dy) then
				return false
			end
		end
	end
	return true
end



function pb:getBlocksToMove (agent, dx, dy)
	local blocks = {}
	local canMove = self:getCollisionBlocks (agent, blocks, dx, dy)
	return blocks, canMove
end


function pb:moveAgent (agent, dx, dy)
	self.agent.x = self.agent.x + dx
	self.agent.y = self.agent.y + dy
	
				-- triggers audio by agent position
				--level 4
	if nLevel==4 and self.agent.y<10 and aproachcreature==false then
		if shipwrecksarrogantm:isPlaying() then shipwrecksarrogantm:stop() end
			if language=="en" then
				if lookitbrookourbookcase:isPlaying() then lookitbrookourbookcase:stop() end
			end
		--print (self.agent.y)
				if language=="en" then
					if accent=="br" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
					elseif accent=="us" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/en-us/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
					end
				elseif language=="fr" then
								youareanobstacle = love.audio.newSource( "/externalassets/dialogs/level4/fr/youareanobstacle.ogg","stream" )
								youareanobstacle:setEffect('myEffect')
								youareanobstacle:play()
			aproachcreature=true
			end
			
		elseif nLevel==54 and enginepushed==false and aproachengineonce==false and self.agent.x>25 and self.agent.x<28 and self.agent.y<14 and self.agent.y>10 then
		
			
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54aproach() end
							
							aproachengineonce=true
		--elseif nLevel==54 and aproachengineonce==true and self.agent.x<25 and self.agent.x>28 and self.agent.y>14 and self.agent.y<10 then
							
			
		elseif  nLevel==56 and self.agent.y>20 and aproachrobodog==false then --level 56
			
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev56robodog() end
			
			aproachrobodog=true
	end
end

function pb:moveBlocks (blocks, dx, dy)
	for i, block in ipairs (blocks) do
		block.x = block.x + dx
		block.y = block.y + dy
		if soundon==true and block.heavy==true then
		    scrape6:play()		--friction sound
		elseif soundon==true and block.heavy==false then
		    underwatermovement2:play()		--friction sound low
		end
			if soundon==true and nLevel==60 then
			gemsound:play()		--gem sound
			elseif soundon==true and nLevel==61 then
				if block.name=="1" or block.name=="2" or block.name=="3" or block.name=="4" or block.name=="5" or block.name=="6" or block.name=="7" or block.name=="8" or block.name=="9" then
					gemsound:play()		--gem sound
				end
		end

		-- level 1

		--script that trigger dialogs when big fish push the pipe in the first level
		if nLevel==1 and block.name=="steel-pipe-1x81" and pipepushed==false then
		
		
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.l1wowyoumovedit() end
		
						if whatwasthat:isPlaying() then whatwasthat:stop() end
						if icantgetsaidonce==true then icantgetthrough:stop() end
						if language=="en" then
							if ihavenoidea:isPlaying() then ihavenoidea:stop() end 
						end
						--if not (language=="chi") then	-- only silence when not chinese to avoid bug
							
						--end

		pipepushed=true
		
		end
		--script that trigger dialogs when big fish touch the chair in the first level
		if nLevel==1 and block.name=="chair2" and chairtouched==false and self.agent.heavy then
		
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.l1damnit() end
		
		if language=="en" and accent=="br" then
						whatwasthat:stop() 
						ihavenoidea:stop()
						weshouldgoandhave:stop()
						waitimgoingwithyou:stop()
					
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/en/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="en" and accent=="us" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/en-us/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="fr" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/fr/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
		elseif language=="es" then
						damnit = love.audio.newSource( "/externalassets/dialogs/level1/es-la/damnit.ogg","stream" )
						damnit:setEffect('myEffect')
						chairtouched=true
					end
		end
		
		-- level 3
		
		--script when shortpipe is pushed by big fish on level 3
		if nLevel==3 and block.name=="shortpipe2" and shortpipepushed==false then
			if language=="en" then
		
						shortpipepushed=true
			elseif language=="fr" then
			
						shortpipepushed=true
			end
		end
		--script when big can is pushed by any fish on level 3
		if nLevel==3 and block.name=="strawberrymarmelade" and strawberrymarmeladepushed==false then
			if language=="en" then
		
						shortpipepushed=true
						strawberrymarmeladepushed=true
			
			elseif language=="fr" then
	
						thisistrickym= love.audio.newSource( "/externalassets/dialogs/level3/fr/thisistrickym.ogg","stream" )
						thisistrickym:setEffect('myEffect')
						thisistrickym:play()
				
						shortpipepushed=true
						strawberrymarmeladepushed=true
			end			
			if language2=="nl" then
						
						thisistrickym= love.audio.newSource( "/externalassets/dialogs/level3/nl/thisistrickym.ogg","stream" )
						thisistrickym:setEffect('myEffect')
						thisistrickym:play()
				
						shortpipepushed=true
						strawberrymarmeladepushed=true
			end
		end
		--script when big fish pushed long horizontal steal pipe on level 3
		if nLevel==3 and block.name=="pipelonghorizontal" and longpipehorizontalpushed==false then
			if language=="en" then
				if accent=="br" then
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/en/youarestandingm.ogg","stream" )
						youarestandingm:setEffect('myEffect')
						youarestandingm:play()
				elseif accent=="us" then
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/en-us/youarestandingm.ogg","stream" )
						youarestandingm:setEffect('myEffect')
						youarestandingm:play()
				end
				if accent2=="br" then
						ifyoudrop= love.audio.newSource( "/externalassets/dialogs/level3/en/ifyoudrop.ogg","stream" )
						ifyoudrop:setEffect('myEffect')
						ifyoudrop:play()
				elseif accent2=="us" then
						ifyoudrop= love.audio.newSource( "/externalassets/dialogs/level3/en-us/ifyoudrop.ogg","stream" )
						ifyoudrop:setEffect('myEffect')
						ifyoudrop:play()
				end
			elseif language=="es" then
				if accent=="es" then
					
						
				end
				if accent2=="es" then
					
				elseif accent2=="la" then
						ifyoudrop= love.audio.newSource( "/externalassets/dialogs/level3/es/es-la/ifyoudrop.ogg","stream" )
						ifyoudrop:setEffect('myEffect')
						ifyoudrop:play()
				end
			elseif language=="fr" then
			
						youarestandingm= love.audio.newSource( "/externalassets/dialogs/level3/fr/youarestandingm.ogg","stream" )
						youarestandingm:setEffect('myEffect')
						youarestandingm:play()

			end
			if language2=="pl" then
								
						ifyoudrop= love.audio.newSource( "/externalassets/dialogs/level3/pl/ifyoudrop.ogg","stream" )
						ifyoudrop:setEffect('myEffect')
						ifyoudrop:play()
						
			elseif language2=="nl" then
						ifyoudrop= love.audio.newSource( "/externalassets/dialogs/level3/nl/ifyoudrop.ogg","stream" )
						ifyoudrop:setEffect('myEffect')
						ifyoudrop:play()
			end
			longpipehorizontalpushed=true
		end
		--script when any fish pushed the book on level 3
		if nLevel==3 and block.name=="booklvl3" and booklvl3pushed==false then
						
			if language=="en" then
				if accent=="br" then
						wewillgiveyouahintdialogm= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahintm.ogg","stream" )
						wewillgiveyouahintdialogm:setEffect('myEffect')
						wewillgiveyouahintdialogm:play()
				elseif accent=="us" then
						wewillgiveyouahintdialogm= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahintm.ogg","stream" )
						wewillgiveyouahintdialogm:setEffect('myEffect')
						wewillgiveyouahintdialogm:play()
				end
				if accent2=="br" then
						wewillgiveyouahintdialogf= love.audio.newSource( "/externalassets/dialogs/level3/en/wewillgiveyouahintf.ogg","stream" )
						wewillgiveyouahintdialogf:setEffect('myEffect')
						wewillgiveyouahintdialogf:play()
						booklvl3pushed=true
				elseif accent2=="us" then
						wewillgiveyouahintdialogf= love.audio.newSource( "/externalassets/dialogs/level3/en-us/wewillgiveyouahintf.ogg","stream" )
						wewillgiveyouahintdialogf:setEffect('myEffect')
						wewillgiveyouahintdialogf:play()
				end	
			elseif language=="es" then
				if accent=="es" then
				
				elseif accent=="la" then
			
				end
				if accent2=="es" then

				elseif accent2=="la" then
						wewillgiveyouahintdialogf= love.audio.newSource( "/externalassets/dialogs/level3/es/es-la/wewillgiveyouahintf.ogg","stream" )
						wewillgiveyouahintdialogf:setEffect('myEffect')
						wewillgiveyouahintdialogf:play()
				end
				
			elseif language=="fr" then
										
						wewillgiveyouahintm= love.audio.newSource( "/externalassets/dialogs/level3/fr/wewillgiveyouahintm.ogg","stream" )
						wewillgiveyouahintm:setEffect('myEffect')
						wewillgiveyouahintm:play()
			end

			if language2=="pl" then
					
						wewillgiveyouahintdialogf= love.audio.newSource( "/externalassets/dialogs/level3/pl/wewillgiveyouahintf.ogg","stream" )
						wewillgiveyouahintdialogf:setEffect('myEffect')
						wewillgiveyouahintdialogf:play()
			elseif language2=="nl" then
					
						wewillgiveyouahintdialogf= love.audio.newSource( "/externalassets/dialogs/level3/nl/wewillgiveyouahintf.ogg","stream" )
						wewillgiveyouahintdialogf:setEffect('myEffect')
						wewillgiveyouahintdialogf:play()
			end
			booklvl3pushed=true
		end
		
		-- level 7
		
			--script when any fish pushed matress on level 7
		if nLevel==7 and block.name=="matress" and matresspushed==false then
							
			matresspushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev7part2() end
		end
		
		if nLevel==27 and block.name=="door" and dooropened==false then
					
							dooropened=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev27door() end
		end
				
		
		-- level 13
		--[[
		if nLevel==13 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev13skull() end
		end
		
		-- level 20
		
		if nLevel==20 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							
							canyouallmind = love.audio.newSource( "/externalassets/dialogs/level20/en/canyouallmind.ogg","stream" )
							canyouallmind:setEffect('myEffect')
							canyouallmind:play()
							
							if talkies==true then Talkies.clearMessages() Obey.lev20skull() end
		end
		
		-- level 28
		
				if nLevel==28 and block.name=="skull" and skullpushed==false then
		
		
							curiouspeople = love.audio.newSource( "/externalassets/dialogs/level28/en/curiouspeople.ogg","stream" )
							curiouspeople:setEffect('myEffect')
							curiouspeople:play()
		
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev28skull() end
		end
		--]]
		if nLevel==30 and block.name=="crab" and crabtouched==false then
					
						-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev30crab() end
	
						crabtouched=true
									
		end
		--[[
		-- level 45
		
			if nLevel==45 and block.name=="skull" and skullpushed==false then
		
							skullpushed=true
							
							welliwasasleep = love.audio.newSource( "/externalassets/dialogs/level45/en/welliwasasleep.ogg","stream" )
							welliwasasleep:play()
							
							
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev45skull() end
		end
		--]]
		-- sound for the magnet on level 53
			if nLevel==53 and block.name=="magnet" and magnetpushed==false then
					electromagnetfield = love.audio.newSource( "/externalassets/sounds/level53/electromagnetfield.ogg","stream" )
					electromagnetfield:play()
					magnetpushed=true
		 elseif nLevel==53 and block.name=="radio" and radiopushed==false then
					alienradio = love.audio.newSource( "/externalassets/sounds/level53/alienradio.ogg","stream" )
					alienradio:play()
					radiopushed=true
		end
		
		if nLevel==54 then
				if enginepushed==false then			-- if the engine is off
					if block.name=="enginekey" and positionalienenginey==positionenginekeyy-2 then	-- if the key is pushed while the engine is off	and they are in the same y coordinate
					
						if thislooks:isPlaying() then thislooks:stop() end 
			
						engineon= love.audio.newSource( "/externalassets/dialogs/level54/engineon.ogg","stream" )
						engineon:setEffect('myEffect')
						engineon:play()
						
						engineon2= love.audio.newSource( "/externalassets/dialogs/level54/engineon2.ogg","stream" )
						engineon2:setEffect('myEffect')
						engineon2:play()
						
							
							UnderwaterSynthesisedLowClicky = love.audio.newSource("externalassets/sounds/atmosphere/AtmosDeepRumblyUnderwaterSynthesisedLowClicky.ogg","stream" )
							UnderwaterSynthesisedLowClicky:play()
				
						enginepushed=true				-- then engine have just been turn on
						engineononce=true
						
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54engineon() end
						
					end
				elseif enginepushed==true then			-- if the engine is on
					if block.name=="enginekey" or block.name=="alienengine" then	-- if the key or engine is moved
		
						if (positionalienenginex-positionenginekeyx)>2 then		-- if the key of the engine is separated by 1 blocks, the engine will turn off
							engineon:stop()
							engineon2:stop()
							
							whatareyoudoing:stop()
							whathaveyouactivated:stop()
							whathaveyoudone:stop()
							thisisterrible:stop()
							mayday:stop()
							howcani:stop()
							
							enginepushed=false
							
							-- trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
						
							engineoff= love.audio.newSource( "/externalassets/dialogs/level54/engineoff.ogg","stream" )
							engineoff:setEffect('myEffect')
							engineoff:play()
						
							if language=="en" then
									-- trigger subtitles
									timer=0
									stepdone=0
									if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
							elseif language=="fr" then
										-- trigger subtitles
									timer=0
									stepdone=0
									if talkies==true then Talkies.clearMessages() Obey.lev54engineoff() end
							end
						end
				end
			end
				
		elseif nLevel==54 and block.name=="enginekey" and enginepushed==true then	-- if the engine is on
		
						if (positionalienenginex-positionenginekeyx)>2 then		-- if the key of the engine is separated by 3 blocks, the engine will turn off
							engineon:stop()
							engineon2:stop()
							
							if icantakeitout:isPlaying() then icantakeitout:stop() end
							--if :isPlaying() then :stop() end
							enginepushed=false
							
							engineoff= love.audio.newSource( "/externalassets/dialogs/level54/engineoff.ogg","stream" )
							engineoff:setEffect('myEffect')
							engineoff:play()
						
							
							if language=="en" then
								finally= love.audio.newSource( "/externalassets/dialogs/level54/finally.ogg","stream" )
								finally:setEffect('myEffect')
								finally:play()
							elseif language=="fr" then
								finally= love.audio.newSource( "/externalassets/dialogs/level54/fr/finally.ogg","stream" )
								finally:setEffect('myEffect')
								finally:play()
							end

						end
			
			
			-- level 55 Nothing but steel
			elseif nLevel==55 and block.name=="pipe3triggeralarm" and pipepushed==false then
					pipepushed=true
					 --= love.audio.newSource("externalassets/.ogg","stream" )
					--love.audio.play( Hypnotic_Puzzle )
					--Hypnotic_Puzzle :setVolume(0.2)
		
					lovebpmload("externalassets/dialogs/level55/alarmloop.ogg")
					--music:setBPM(127)
					music:play()
					music:setVolume(0.4)
		
			
			-- level 56
		
			elseif nLevel==56 and lightswitchon==false and lightswitchpushedafterfalling==false then
					if block.name=="lightswitch" then
					
							lightswitchon=true
							lightswitchpushedafterfalling=true
							
							switchonsound:setEffect('myEffect')
							switchonsound:play()
							
							 --trigger subtitles
							timer=0
							stepdone=0
							if talkies==true then Talkies.clearMessages() 
							Obey.lev56switchon()
							end
							
								if wait:isPlaying() then wait:stop() end
					
					end
		
		-- level 58 The real propulsion
		
			elseif nLevel==58 and block.name=="alien1" or block.name=="alien2"  then
									
								alienlanguage= love.audio.newSource( "/externalassets/sounds/level58/Alien_Language_00.ogg","stream" )
								alienlanguage:setEffect('myEffect')
								alienlanguage:play()
	
		-- level 59
		
			elseif nLevel==59 and skullpushed==false then
					if block.name=="skullbottom" then
					--if block.name=="skullupleft" or block.name=="skullupleft+" or block.name=="skullupright" or block.name=="skullmidright" or block.name=="skullbottom" then
							skullpushed=true
							-- trigger subtitles
							timer=0
							stepdone=0
							anothernight = love.audio.newSource( "/externalassets/dialogs/level59/en/anothernight.ogg","stream" )
							anothernight:setEffect('myEffect')
							anothernight:play()
							if talkies==true then Talkies.clearMessages() end
					end
					--if block.name=="skullupleft" then if talkies==true then Obey.lev59skullupleft() end end
					--if block.name=="skullupleft+" then if talkies==true then Obey.lev59skullupleftplus() end end
					if block.name=="skullupright" then if talkies==true then Obey.lev59skullupright() end end
					--if block.name=="skullmidright" then if talkies==true then Obey.lev59skullmidright() end end
					if block.name=="skullbottom" then if talkies==true then Obey.lev59skullbottom() end end
			
	
			end
	end
end

function pb:isCollisionBlockToAllBlocks (blockA, dx, dy)
	for i, block in ipairs (self.blocks) do
		if not (block == blockA) 
		and self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

function pb:isCollisionBlockToAllAgents (blockA, dx, dy)
	for i, agent in ipairs (self.agents) do
		if self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return agent -- dead agent :(
		end
	end
	return false
end

function pb:areAllAgentsInExitAreas ()
	for i, agent in ipairs (self.agents) do
		if not self:isCollisionAgentToAnyExitArea (agent) then
			return false -- at least one of agents is not on exit, the game is not over
		end
	return true -- all agents are in exit areas, level is done
	end
end

function pb:isCollisionAgentToAnyExitArea (agent)
	for i, area in ipairs (self.exitAreas) do
		if self:isBlockToBlockCollision (area, agent, 0, 0) then -- dx and dy are 0
			return true -- collision one of areas to agent in /this/ position
		end
	end
	return false -- this agent has no collision with any of exit areas
end

function pb:fallBlocks (blocks)
	local dx, dy = 0, 1 -- no horizontal speed, but positive (down) vertical
	for i = 1, self.gridWidth do
		for i, block in ipairs (blocks) do
			if self:isBlockToMapCollision (block, dx, dy) then
				-- not falling
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllBlocks (block, dx, dy) then
				block.deadly = false
				-- collision to block: probably not falling
			elseif block.deadly and self:isCollisionBlockToAllAgents (block, dx, dy) then
				local deadAgent = self:isCollisionBlockToAllAgents (block, dx, dy)
				deadAgent.dead = true
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllAgents (block, dx, dy) then
				-- the block is on fish
			else
				-- sure falling
				if soundon==true and block.heavy==true then
				    impactmetal:play()		--impact sound
				elseif soundon==true and block.heavy==false then
					impactgolf:play()		--impact sound low
				end
		
				-- Pipe falling level 1
				if nLevel==1 and block.name=="steel-pipe-1x81" and pipefalled==false then
							pipefalled=true
								-- trigger subtitles
							timer=0
							stepdone=0
							loadsubtitles()
							loadtouchtext()		-- reload the touch interface
							if talkies==true then Talkies.clearMessages() Obey.lev1() end
				end
		
				-- Sound briefcase level 2
				if nLevel==2 and block.name=="trunk" and briefcaseclosed==true then
					require("game/levels/briefcasemessage")
					gamestatus="briefcasemessage"
					briefcaseload()
								-- trigger subtitles
							timer=0
							stepdone=0
							loadsubtitles()
							optionsload()		-- reload the values for the languages on the menus
							loadtouchtext()		-- reload the touch interface
							if talkies==true then Talkies.clearMessages() Obey.lev2briefcase() end
				end

				-- zeus statue falling level 23
				
					if nLevel==23 and block.name=="zeus" and zeusfalled==false then
					zeusfalled=true
								-- trigger subtitles
							timer=0
							stepdone=0
							--loadsubtitles()
							--optionsload()		-- reload the values for the languages on the menus
							--loadtouchtext()		-- reload the touch interface
							if talkies==true then Talkies.clearMessages() Obey.lev23zeus() end
				end
				
				-- level 56 guarded corridor
				if nLevel==56 and block.name=="lightswitch" and lightswitchon==true and lightonvoiceplayed==false then
					
						
						switchonsound:setEffect('myEffect')
						switchonsound:play()
						
								-- trigger subtitles
									if talkies==true then 
									Talkies.clearMessages()
									timer=0
									stepdone=0
									Obey.lev56switchoff()
							end
							
							lightswitchon=false
							lightonvoiceplayed=true
				end
				
				-- Sound crystals level 61
					if soundon==true and nLevel==60 then
						gemsound2:play()		--gem sound
				elseif soundon==true and nLevel==61 then
					if block.name=="1" or block.name=="2" or block.name=="3" or block.name=="4" or block.name=="5" or block.name=="6" or block.name=="7" or block.name=="8" or block.name=="9" then
						gemsound2:play()		--gem sound
					end
				end

				if nLevel==30 and block.name=="crab" and crabtouched==true and crabfalled==false then
					whowokemeup:stop()
					leavemealone:stop()
					whatdoyouwant:stop()
					donttouchme:stop()
						
						-- trigger subtitles
									if talkies==true then 
									Talkies.clearMessages()
									timer=0
									stepdone=0
									Obey.lev30crabups()
							end
					
					
					if language=="en" and accent=="br" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/en-us/crab/5ups.ogg","stream" )
						ups:setEffect('myEffect')
						crabfalled=true
					elseif language=="en" and accent=="us" then
						ups = love.audio.newSource( "/externalassets/dialogs/level30/en-us/crab/5ups.ogg","stream" )
						ups:setEffect('myEffect')
						crabfalled=true
					elseif language=="de" then
					talkingcrabde:stop()
						talkingcrabde = love.audio.newSource( "/externalassets/dialogs/level30/de/TalkingCraboopsde.ogg","stream" )
						talkingcrabde:setPosition( -1,0,0 )
						talkingcrabde:setEffect('myEffect')
						talkingcrabde:play()
						crabfalled=true
					elseif language=="fr" then
					talkingcrabfr:stop()
						talkingcrabfr = love.audio.newSource( "/externalassets/dialogs/level30/fr/level30craboopsfr.ogg","stream" )
						talkingcrabfr:setPosition( -1,0,0 )
						talkingcrabfr:setEffect('myEffect')
						talkingcrabfr:play()
						crabfalled=true
					elseif language=="ru" then
					talkingcrabru:stop()
						talkingcrabru = love.audio.newSource( "/externalassets/dialogs/level30/ru/TalkingCraboopsru.ogg","stream" )
						--talkingcrabru:setPosition( -1,0,0 )
						talkingcrabru:setEffect('myEffect')
						talkingcrabru:play()
						crabfalled=true
					end
				end

				block.x = block.x + dx -- never changes
				block.y = block.y + dy
				block.deadly = true
			end
		end
	end
end

function pb:mainMoving (dx, dy,dt)
	local agent = self.agent -- active agent
	if dx > 0 then 
		agent.direction = "right"
	elseif dx < 0 then
		agent.direction = "left"
	end
	local blocks, canMove = self:getBlocksToMove (agent, dx, dy)
	if canMove then
	
		self:moveAgent (agent, dx, dy)
		self:moveBlocks (blocks, dx, dy)
		self:fallBlocks (self.blocks, dx, dy)
	end
end


function pb:keypressedMoving (scancode)
	if scancode == 'w' or scancode == 'a' or scancode == 's' or scancode == 'd' then
		-- d means 1; a means -1; otherwise 0
		local dx = scancode == 'd' and 1 or scancode == 'a' and -1 or 0
		-- s means 1; w means -1; otherwise 0
		local dy = scancode == 's' and 1 or scancode == 'w' and -1 or 0
		pb:mainMoving (dx, dy)
	end
	if scancode == 'right' or scancode == 'left' or scancode == 'up' or scancode == 'down' then
		local dx = scancode == 'right' and 1 or scancode == 'left' and -1 or 0
		local dy = scancode == 'down' and 1 or scancode == 'up' and -1 or 0
		pb:mainMoving (dx, dy)
	end
end

function love.gamepadpressed(joystick,button)

		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		
			if joystick:isGamepadDown("dpleft") then
				dx = -1
				dy = 0
			elseif joystick:isGamepadDown("dpright") then
				dx = 1
				dy = 0
			elseif  joystick:isGamepadDown("dpup") then
				dx = 0
				dy = -1
			elseif joystick:isGamepadDown("dpdown") then
				dx = 0
				dy = 1
			elseif joystick:isGamepadDown("a")  then
			pb:switchAgent ()
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("b")  then
			Talkies.clearMessages() talkies=false
			if language=="en" then 
						if nLevel==1 then dialogsen1m:stop() dialogsen1f:stop() TylerSong3_Normal:setVolume(0.9)
					elseif nLevel==2 then dialogsen2m:stop() dialogsen2f:stop() UnderwaterAmbientPadisaiah658:setVolume(0.9)
					elseif nLevel==3 then dialogsen3m:stop() dialogsen3f:stop() pixelspheresong21:setVolume(0.9)
					end
			elseif language=="es" then dialogses1m:stop() dialogses1f:stop() TylerSong3_Normal:setVolume(0.9)
			elseif language=="de" then dialogsde1m:stop() dialogsde1f:stop() TylerSong3_Normal:setVolume(0.9)
			elseif language=="it" then dialogsit1m:stop() dialogsit1f:stop() TylerSong3_Normal:setVolume(0.9)
			elseif language=="fr" then dialogsfr1m:stop() dialogsfr1f:stop() TylerSong3_Normal:setVolume(0.9)
			elseif language=="ru" then dialogsru1m:stop() dialogsru1f:stop() TylerSong3_Normal:setVolume(0.9)
			elseif language=="nl" then dialogsnl1m:stop() dialogsnl1f:stop() TylerSong3_Normal:setVolume(0.9)
			end
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("y")  then
					if helpison=="yes" then helpison="no"
				elseif helpison=="no" then helpison="yes"
				end
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("start")  then
			dx = 0
			dy = 0
			gamestatus="levelselection"
			elseif joystick:isGamepadDown("back")  then
			dx = 0
			dy = 0
			shader2=false
			gamestatus="options"
			end
		    pb:mainMoving (dx, dy)
		end
		
		 if joystick:isGamepad() then
			isgamepad=true
		end
		
end

---------------------------------------------------------------------------------------------------
-- draw
---------------------------------------------------------------------------------------------------

function pb:drawBackgroundGrid ()
	local gridSize = self.gridSize
	local gridWidth = self.gridWidth
	local gridHeight = self.gridHeight
	love.graphics.setLineWidth(1)
	love.graphics.setColor(0.3,0.4,0.4)
	for i = 0, gridWidth do
		love.graphics.line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
	end
	for i = 0, gridHeight do
		love.graphics.line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
	end

end


function pb:drawMap ()
	local map = self.map
	local tileSize = self.gridSize
	love.graphics.setLineWidth(2)
if background=="yes" then
	for y, xs in ipairs (map) do
		for x, value in ipairs (xs) do
			-- value is boolean: true or false
			if value==true then -- map tile
				-- beware of -1
				--love.graphics.setColor(0.5,0.5,0.5)
					if palette==1 then love.graphics.setColor(cs1tile1) 
				elseif palette==2 then love.graphics.setColor(cs2tile1)
				elseif palette==3 then love.graphics.setColor(cs3tile1)
				elseif palette==4 then love.graphics.setColor(cs4tile1)
				elseif palette==5 then love.graphics.setColor(cs5tile1)
				elseif palette==6 then love.graphics.setColor(cs6tile1)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
				else
					if not(palette==1) then
						love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
						--love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
					end
				end
				
				--love.graphics.setColor(0.6, 0.58, 0.2)
					if palette==1 then love.graphics.setColor(cs1tile2) 
				elseif palette==2 then love.graphics.setColor(cs2tile2)
				elseif palette==3 then love.graphics.setColor(cs3tile2)
				elseif palette==4 then love.graphics.setColor(cs4tile2)
				elseif palette==5 then love.graphics.setColor(cs5tile2)
				elseif palette==6 then love.graphics.setColor(cs6tile2)
				end
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				else
					--if assets==1 or nLevel>1 then
					if not(palette==1) then
						love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
						--love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					end
				end
				--love.graphics.setColor(0,0,0)
				
					if debugmode=="yes" and palette==1 then 
            love.graphics.setColor(0,0,0)
					--love.graphics.print("#",(x-1)*tileSize, (y-1)*tileSize) end
					love.graphics.print ((x)..' '..(y), (x-1)*tileSize, (y-1)*tileSize) end
			elseif value=="exit" then -- map tile
			if not(palette==1) then
				love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
			end


			--borders
			elseif value=="border" then -- map tile
					if palette==1 then love.graphics.setColor(cs1border1)
                elseif palette==2 then love.graphics.setColor(cs2border1)
                elseif palette==3 then love.graphics.setColor(cs3border1)
                elseif palette==4 then love.graphics.setColor(cs4border1)
                elseif palette==5 then love.graphics.setColor(cs5border1)
                elseif palette==6 then love.graphics.setColor(cs6border1)
                end
                if palette==7 then love.graphics.setColor(cs6border1)
                
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)	
                else
					if not(palette==1) then
						love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
						--love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
					end
								--dream:draw(cube1,  (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
              end
              if threeD==true then
                						--dream:draw(cube1,  1, 1,-60, 10,10,10)
              end
				
					if palette==1 then love.graphics.setColor(cs1border2)
                elseif palette==2 then love.graphics.setColor(cs2border2)
                elseif palette==3 then love.graphics.setColor(cs3border2)
                elseif palette==4 then love.graphics.setColor(cs4border2)
                elseif palette==5 then love.graphics.setColor(cs5border2)
                elseif palette==6 then love.graphics.setColor(cs6border2)
                end
                if palette==7 then love.graphics.setColor(cs6border1)
					love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
				else
					if not(palette==1) then
						love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
						--love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					--elseif assets==2 and nLevel==1 then
						--love.graphics.draw (level1style1, (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					end
				end
			end
		end
	end
	end
		
				--[[print("Tilesize :" .. tileSize)
				print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight)
				print("=========================================================")--]]

				if extradebug==true and debugmode=="yes" then
				love.graphics.setFont(love.graphics.newFont(20))
				love.graphics.setColor(1,1,1)
								
				love.graphics.print("Tilesize :" .. tileSize ,50,100)
				love.graphics.print("Gridsize :" .. self.gridWidth .. "x " .. self.gridHeight,50,150)
				end

end

function pb:drawOutline  (block)

	local lines = block.lines
	local tileSize = self.gridSize
	local x, y = block.x-1, block.y-1
	local exceptions = {1, 2, 3, 4, 6, 20, 21, 26, 28, 29, 30, 31, 48, 50, 52, 53, 54, 55, 56, 59, 60, 61}
	local exception = isValueInList (nLevel, exceptions) -- bool
	--do not draw Outlines if levels are:
	if objectscells=="yes" and not exception then
		for i, line in ipairs (lines) do
			love.graphics.line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
		end
	end
	
	
end

function pb:drawBlock (block)

	local x, y = block.x, block.y
	local tileSize = self.gridSize
	for i = 1, #block.tiles-1, 2 do
		local dx, dy = block.tiles[i], block.tiles[i+1]
				if objectscells=="yes" then
		-- beware of -1
		
					if palette==1 then love.graphics.setColor(cs1block)
				elseif palette==2 then love.graphics.setColor(cs2block)
				elseif palette==3 then love.graphics.setColor(cs3block)
				elseif palette==4 then love.graphics.setColor(cs4block)
				elseif palette==2 then love.graphics.setColor(1,1,1)
				elseif palette==3 then love.graphics.setColor(1,1,1)
				elseif palette==4 then love.graphics.setColor(1,1,1)
				elseif palette==5 then love.graphics.setColor(cs5block)
				elseif palette==6 then love.graphics.setColor(cs6block)
				end
				
				if palette==7 then love.graphics.setColor(cs7tile1)
					love.graphics.rectangle ('line', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
				else
					
						if not (palette==1) then love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize) 
					--elseif not (palette==1) and not (nLevel==60) then love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize) 
					end
							--if nLevel>1 then
							--love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
					
						if nLevel==1 then
							--[[if block.name=="chair1" then
								love.graphics.rectangle ('fill', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
							end--]]
							if i==1 then
								if block.name=="chair1" then
										if palette==1 then 	love.graphics.draw (chair1, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
									--[[elseif palette==2 then love.graphics.draw (c64chair1, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
									elseif palette==3 then love.graphics.draw (neschair1, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
									elseif palette==4 then love.graphics.draw (z80chair1, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
										--if threeD==true then dream:draw(woodenchair3d, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5) end
									--]]
									end
								elseif block.name=="chair2" then
										if palette==1 then love.graphics.draw (chair2, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
									--[[elseif palette==2 then love.graphics.draw (c64chair2, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
									elseif palette==3 then love.graphics.draw (neschair2, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
									elseif palette==4 then love.graphics.draw (z80chair2, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 6/dividx,5.4/dividy)
									--]]
									end
								elseif block.name=="steel-pipe-1x81" then
									love.graphics.setColor(cs1block)
										if palette==1 then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
									--[[elseif palette==2 then love.graphics.draw (c64pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0, 6/dividx,6/dividy)
									elseif palette==3 then love.graphics.draw (nespipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0, 6/dividx,6/dividy)
									elseif palette==4 then love.graphics.draw (z80pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0, 6/dividx,6/dividy)
									--]]
									end
								elseif block.name=="cushion1" then
										if palette==1 then love.graphics.draw (cushion1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
									--[[elseif palette==2 then love.graphics.draw (c64cushion1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									elseif palette==3 then love.graphics.draw (nescushion1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									elseif palette==4 then love.graphics.draw (z80cushion1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									--]]
									end
								elseif block.name=="table1" then
										if palette==1 then love.graphics.draw (table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
									--[[elseif palette==2 then love.graphics.draw (c64table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									elseif palette==3 then love.graphics.draw (nestable1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									elseif palette==4 then love.graphics.draw (z80table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,6/dividx,5.4/dividy)
									--]]
									end
								end
							end
						--level 2
						elseif nLevel==2 then
							if i==1 then
							if block.name=="trunk" and palette==1 then   
									if briefcaseclosed==true and block.name=="trunk" then
										love.graphics.draw (briefcase, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) 
								elseif briefcaseclosed==false and block.name=="trunk" then
										love.graphics.draw (briefcaseopen, ((x+dx-1))*tileSize, (y-4.2+dy-1)*tileSize,0,0.5,0.5)
									end
								end
							
								if block.name=="warning" then if palette==1 then love.graphics.draw (warning, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy) end
								elseif block.name=="bottle" then  	  if palette==1 then love.graphics.draw (bottle, ((x+dx-2))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="L" then 	  if palette==1 then love.graphics.draw (L, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="nut" then	  if palette==1 then love.graphics.draw (hexagonalnut, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="hammer" then  if palette==1 then love.graphics.draw (hammer, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="pliers" then  if palette==1 then love.graphics.draw (pliers, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="pipehorizontal" then if palette==1 then love.graphics.draw (pipehorizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								end
							end
							--level 3
						elseif nLevel==3 then
							if i==1 then
									if block.name=="shortpipe1" then if palette==1 then love.graphics.draw (shortpipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="shortpipe2" then if palette==1 then love.graphics.draw (shortpipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="minipipe" then if palette==1 then love.graphics.draw (minipipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="pipelonghorizontal" then if palette==1 then love.graphics.draw (pipelonghorizontal, ((x-3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="booklvl3" then   if palette==1 then love.graphics.draw (booklv3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="longpipehorizontal" then  	  if palette==1 then love.graphics.draw (longpipehorizontal, ((x+dx-4))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="apricosemarmelade" then 	  if palette==1 then love.graphics.draw (apricosemarmelade, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="honey" then	  if palette==1 then love.graphics.draw (honey, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="strawberrymarmelade" then  if palette==1 then love.graphics.draw (strawberrymarmelade, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="oillamp" then  if palette==1 then love.graphics.draw (oillamp, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="snail" then  if palette==1 then love.graphics.draw (snail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="shell" then  if palette==1 then love.graphics.draw (shell, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="coffee" then  if palette==1 then love.graphics.draw (coffee, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="plate" then  if palette==1 then love.graphics.draw (plate, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								elseif block.name=="axe" then  if palette==1 then love.graphics.draw (axe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								--elseif block.name=="" then  if palette==1 then love.graphics.draw (, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								--elseif block.name=="" then if palette==1 then love.graphics.draw (, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) end
								end
							end
							-- level 4
						elseif nLevel==4 then
							if i==1 then
									if block.name=="sPipe" then love.graphics.draw (spipe, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="cpipe" then love.graphics.draw (cpipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book1" then love.graphics.draw (book1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book2" then love.graphics.draw (book2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book3" then love.graphics.draw (book3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book4" then love.graphics.draw (book4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book5" then	love.graphics.draw (book5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book6" then	love.graphics.draw (book6, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book7" then	love.graphics.draw (book7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book8" then	love.graphics.draw (book8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="book9" then	love.graphics.draw (book9, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="map1" then	love.graphics.draw (book9, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="bookbig1" then love.graphics.draw (bookbig1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="booksmall1" then love.graphics.draw (booksmall1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="booksmall2" then love.graphics.draw (booksmall2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="booksmall3" then love.graphics.draw (booksmall3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="bookhoriz1" then love.graphics.draw (bookhoriz1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="bookhoriz2" then love.graphics.draw (bookhoriz2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							--level 5
						elseif nLevel==5 then
							if i==1 then
									if block.name=="chair1upside" then love.graphics.draw (chair1upside, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
								elseif block.name=="chair2upside" then love.graphics.draw (chair2upside, ((x+dx-1)-3)*tileSize, (y+dy-1)*tileSize,0, 1/dividx,1/dividy)
								elseif block.name=="table1" then love.graphics.draw (table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							
						--level 6
						elseif nLevel==6 then
							if i==1 then
									if block.name=="broom" then love.graphics.draw (broom, (x-1+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									
									elseif block.name=="rock" then love.graphics.draw (rock, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="rock2" then love.graphics.draw (rock2, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="rock3" then love.graphics.draw (rock3, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="rock4" then love.graphics.draw (rock4, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="coal" then love.graphics.draw (coal, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="longpipe" then love.graphics.draw (longpipe, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="wood" then love.graphics.draw (wood, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
									elseif block.name=="wood2" then love.graphics.draw (wood2, (x+dx-1)*tileSize, (y+dy-1)*tileSize,0, 0.5,0.5)
								end
							end
							
							--level 7
						elseif nLevel==7 then
							if i==1 then
									if block.name=="snail7" then love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="snail72" then love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							
							--level 9
						elseif nLevel==9 then
							if i==1 then
									if block.name=="mirror" then
										love.graphics.draw (mirror, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										mirrorpositionx=x+dx-1
									end
							end
							--level 11
						elseif nLevel==11 then
							if i==1 then
									if block.name=="pipe11" then love.graphics.draw (pipe11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="pipe4_11" then love.graphics.draw (pipe4_11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="snail11" then love.graphics.draw (snail1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="bullet11" then love.graphics.draw (bullet11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								elseif block.name=="bullet112" then love.graphics.draw (bullet11, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
							end
							
							--level 13
								elseif nLevel==13 then
							if i==1 then
										if block.name=="vik1" then love.graphics.draw (viking1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="vik2" then love.graphics.draw (viking2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="vik3" then love.graphics.draw (viking3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="vik4" then love.graphics.draw (viking4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="vik5" then love.graphics.draw (viking5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="vik6" then love.graphics.draw (viking6, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="axe1" then love.graphics.draw (axe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="axe2" then love.graphics.draw (axe2, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="shield1" then love.graphics.draw (shield1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="shield2" then love.graphics.draw (shield2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 14
								elseif nLevel==14 then
							if i==1 then
									if block.name=="eye" then love.graphics.draw (eye, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 15
								elseif nLevel==15 then
							if i==1 then
										if block.name=="microscope" then love.graphics.draw (microscope, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="phone" then love.graphics.draw (phone, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="phone2" then love.graphics.draw (phone, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="phone3" then love.graphics.draw (phone, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="basephone" then love.graphics.draw (basephone, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="alarmclock" then love.graphics.draw (alarmclock, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
							end
							
							--level 17
								elseif nLevel==17 then
							if i==1 then
										if block.name=="viking1" then love.graphics.draw (viking1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking2" then love.graphics.draw (viking2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking3" then love.graphics.draw (viking3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking4" then love.graphics.draw (viking4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking5" then love.graphics.draw (viking5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking6" then love.graphics.draw (viking6, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="viking7" then love.graphics.draw (viking7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe" or block.name=="pipe2" or block.name=="pipe3" then
										love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
							end
							--level 20
								elseif nLevel==20 then
							if i==1 then
								if block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="amphora" then love.graphics.setColor(0,1,1)
										love.graphics.draw (amphora, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										love.graphics.setColor(cs1block)
								elseif block.name=="shell" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
										love.graphics.draw (shell, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="sculpture" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
										love.graphics.draw (sculpture, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="elevator1" then love.graphics.draw (elevator1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="elevator2" then love.graphics.draw (elevator2, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="cross" then love.graphics.draw (cross, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 21
								elseif nLevel==21 then
							if i==1 then
									if block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="crab" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
										love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pedestal" then love.graphics.draw (pedestal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipelong" then love.graphics.draw (pipelong, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipeshort" then love.graphics.draw (pipeshort, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="arm" then love.graphics.draw (arm, ((x+dx-1))*tileSize, (y-0.1+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="platform" then love.graphics.draw (platform, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize, (y-0.1+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 22
								elseif nLevel==22 then
							if i==1 then
									if block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 23
							
							elseif nLevel==23 then
							if i==1 then
									if block.name=="column1" then love.graphics.draw (column1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="column2" then love.graphics.draw (column2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="zeus" then love.graphics.draw (zeus, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="temple1" then love.graphics.draw (temple1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="temple2" then love.graphics.draw (temple2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 24
							
							elseif nLevel==24 then
							if i==1 then
								if block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize, (y-0.1+dy-1)*tileSize,0,0.5,0.5)
								end
							end
								
								--level 25
								elseif nLevel==25 then
								if i==1 then
									if block.name=="egyptianwoman" then
											love.graphics.draw (egyptianwoman, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
								end
							
							--level 26
								elseif nLevel==26 then
								if i==1 then
									if block.name=="crab" then
									if (math.fmod(beat,2) ==0) then love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)	
										elseif (math.fmod(beat,3) ==0) then love.graphics.draw (craba2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										else love.graphics.draw (craba3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										end
										
									elseif block.name=="speaker" then
										if (math.fmod(beat,2) ==0) then love.graphics.draw (speaker, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.4,0.4)
										else love.graphics.draw (speaker, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										end
										
									elseif block.name=="bigpipe" then love.graphics.draw (bigpipe, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="skull" then love.graphics.draw (skull, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="face" then love.graphics.draw (face, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column1" then love.graphics.draw (column1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column2" then love.graphics.draw (column2, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column3" then love.graphics.draw (column3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column4" then love.graphics.draw (column4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="statue" then love.graphics.draw (statue, ((x+dx-1))*tileSize, (y-0.1+dy-1)*tileSize,0,0.5,0.5)
									end
								end
							
							--level 27
								elseif nLevel==27 then
							if i==1 then
									if block.name=="ball1" then love.graphics.draw (ball1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
							end
							--level 28
								elseif nLevel==28 then
							if i==1 then
										if block.name=="skull" then love.graphics.draw (skull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="amphora" then
										love.graphics.setColor(0.5,1,1)
											love.graphics.draw (amphora, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
											love.graphics.setColor(cs1block)
										elseif block.name=="shell" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
											love.graphics.draw (shell, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="sculpture" or block.name=="crab2" or block.name=="crab3" or block.name=="crab4" or block.name=="crab5" or block.name=="crab6" or block.name=="crab7" then
											love.graphics.draw (sculpture, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="elevator1" then love.graphics.draw (elevator1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="elevator2" then love.graphics.draw (elevator2, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cross" then love.graphics.draw (cross, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 29
								elseif nLevel==29 then
							if i==1 then
										if block.name=="plug" then love.graphics.draw (plug, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe1block" then love.graphics.draw (pipe1block, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2blocks" then love.graphics.draw (pipe2blocks, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column" then love.graphics.draw (column, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="column2" then love.graphics.draw (column2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="crab" then love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="snail" then love.graphics.draw (snail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
						
							--level 30
								elseif nLevel==30 then
							if i==1 then
								if block.name=="crab" then love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="piece1" then love.graphics.draw (piece1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="piece2" then love.graphics.draw (piece2, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="piece3" then love.graphics.draw (piece3, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end

							--level 31
								elseif nLevel==31 then
							if i==1 then
									if block.name=="labyrinth" then love.graphics.draw (labyrinth, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 34
								elseif nLevel==34 then
							if i==1 then
									if block.name=="crab" then love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 35
								elseif nLevel==35 then
							if i==1 then
									if block.name=="crab" then love.graphics.draw (crab, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 37
								elseif nLevel==37 then
							if i==1 then
									if block.name=="turtle" then
										--love.graphics.draw (turtle, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 45
								elseif nLevel==45 then
							if i==1 then
									if block.name=="skull" then	love.graphics.draw (skull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipehorizontal" then love.graphics.draw (pipehorizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 46
								elseif nLevel==46 then
							if i==1 then
										if block.name=="snowman" then love.graphics.draw (snowman, ((x-1.65+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="table" then love.graphics.draw (table1, ((x-0.5+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="table2" then love.graphics.draw (table2, ((x-0.3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 47
							
							elseif nLevel==47 then
								if i==1 then
											if block.name=="barrel" then love.graphics.draw (barrel, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cannon1l" then love.graphics.draw (cannon1, ((x+6+dx-1))*tileSize, (y+dy-1)*tileSize,0,-0.5,0.5)
										elseif block.name=="cannon1r" then love.graphics.draw (cannon1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cannon2l" then love.graphics.draw (cannon2, ((x+6+dx-1))*tileSize, (y+dy-1)*tileSize,0,-0.5,0.5)
										elseif block.name=="cannon2r" then love.graphics.draw (cannon1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cube" then love.graphics.draw (cube, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										end
								end
							
							--level 48
								elseif nLevel==48 then
								if i==1 then
											if block.name=="armchair" then love.graphics.draw (armchair, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="table" then love.graphics.draw (table1, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="barrel" then love.graphics.draw (barrel, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="furniture" then love.graphics.draw (furniture, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="furniture2" then love.graphics.draw (furniture2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="sword" then love.graphics.draw (sword, ((x-5+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cup" then love.graphics.draw (cup, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="map" then love.graphics.draw (map, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="table3" then love.graphics.draw (table3, ((x-0.3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="table5" then love.graphics.draw (table5, ((x-0.5+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood4" then love.graphics.draw (wood4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood5ver" then love.graphics.draw (wood5ver, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood8" then love.graphics.draw (wood8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pot" then love.graphics.draw (pot, ((x-0.3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pot2" then love.graphics.draw (pot2, ((x-0.3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="egg" then love.graphics.draw (egg, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										
										end
								end
							
							
							--level 50
								elseif nLevel==50 then
									if i==1 then
											if block.name=="skelblock" then love.graphics.draw (skelblock, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="metaldoghnut" then love.graphics.draw (metaldoghnut, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="armchair" then love.graphics.draw (armchair, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2hor" then love.graphics.draw (pipe2hor, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="ost" then love.graphics.draw (ost, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="map2" then love.graphics.draw (map2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="map3" then love.graphics.draw (map3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood2" then love.graphics.draw (wood2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="woodhor4" then love.graphics.draw (woodhor4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="woodhor4_2" then love.graphics.draw (woodhor4_2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="woodver4" then love.graphics.draw (woodver4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="woodver4_2" then love.graphics.draw (woodver4_2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood5" then love.graphics.draw (wood5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood52" then love.graphics.draw (wood52, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="wood8" then love.graphics.draw (wood8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="diamond" then love.graphics.draw (diamond, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="diamond2" then love.graphics.draw (diamond2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="diamond3" then love.graphics.draw (diamond3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="eye" then love.graphics.draw (eye, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="cup" then love.graphics.draw (cup, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="table" then love.graphics.draw (table1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="hook" then love.graphics.draw (hook, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="sword" then love.graphics.draw (sword, ((x-5+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										end
									end
									
								--level 51
								elseif nLevel==51 then
							if i==1 then
									if block.name=="flint" then love.graphics.draw (flint, ((x-0.1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										
									end
							end
								
							--level 52
								elseif nLevel==52 then
							if i==1 then
									if block.name=="pinkthing" then love.graphics.draw (pinkthing, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="p1" then love.graphics.draw (p1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe7" then love.graphics.draw (pipe7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)	
									elseif block.name=="pipe8" then love.graphics.draw (pipe8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="cube" then love.graphics.draw (cube, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										
									end
							end
							
							--level 53
								elseif nLevel==53 then
							if i==1 then
										if block.name=="magnet" then love.graphics.draw (magnet, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="scifigun" then love.graphics.draw (scifigun, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="lpipe" then love.graphics.draw (lpipe, ((x-5+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="tpipe" then love.graphics.draw (tpipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="radio" then love.graphics.draw (radio, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
						--level 54
								elseif nLevel==54 then
							if i==1 then
							
							
								if enginepushed then
										love.graphics.setColor(1/subbeat/2,0.5,0.5,1)
										if block.name=="alienengine" then
												love.graphics.setColor(1/subbeat/2,0,0,1)
												love.graphics.draw (alienengine, math.random(0,2)+((x-3+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
												positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginey= block.y		-- y position of the alien engine
										
										elseif block.name=="enginekey" then	
												positionenginekeyx= block.x			-- x position of the alien engine key
												positionenginekeyy= block.y			-- y position of the alien engine key
												love.graphics.draw (key, math.random(0,2)+((x+dx-1))*tileSize,math.random(0,2)+ (y+dy-1)*tileSize,0,0.5,0.5)
												
										elseif block.name=="pipe1" 	   then	love.graphics.draw (pipe1, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="nut" 	   then love.graphics.draw (nut, ((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="screw" 	   then love.graphics.draw (screw, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2horizontal" then love.graphics.draw (pipe2horizontal, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2" 	   then love.graphics.draw (pipe2, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe3" 	   then love.graphics.draw (pipe3, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="xalien"    then love.graphics.draw (xalien, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
										end
								elseif not (enginepushed) then

											if block.name=="alienengine" then
												love.graphics.draw (alienengine, ((x-3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginex= block.x		-- x position of the alien engine
												positionalienenginey= block.y		-- y position of the alien engine
										elseif block.name=="enginekey" then											
												positionenginekeyx= block.x			-- x position of the alien engine key
												positionenginekeyy= block.y			-- y position of the alien engine key
												love.graphics.draw (key, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe1" then	love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="nut" then love.graphics.draw (nut, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="screw" then love.graphics.draw (screw, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2horizontal" then love.graphics.draw (pipe2horizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										elseif block.name=="xalien" then love.graphics.draw (xalien, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										end
								end
							end
							

							--level 55
								elseif nLevel==55 then
							if i==1 then
								
							if pipepushed==true then love.graphics.setColor(1/subbeat/3+0.35,0.5,0.5,1) end
							
										if block.name=="pipe1" then	love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe1horizontal" then love.graphics.draw (pipe1horizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" or block.name=="pipe3triggeralarm" then
										love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3horizontal" then love.graphics.draw (pipe3horizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipesnail" then love.graphics.draw (pipesnail, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipebig" then love.graphics.draw (pipebig, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										
									end
							end
							
							--level 56
								elseif nLevel==56 then
							if i==1 then
										if lightswitchon==true and lightswitchpushedafterfalling==true then love.graphics.setColor(1/subbeat/3.2,0.8,0.8,1) end
										
										if lightswitchon==true then
										
											if aproachrobodog==false then
														if block.name=="dogleft" then love.graphics.draw (robodog, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize,0,-0.5,0.5)
													elseif block.name=="dogright" then love.graphics.draw (robodog, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
													end
											elseif aproachrobodog==true then
														if block.name=="dogleft" then love.graphics.draw (robodogeyeon, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize,0,-0.5,0.5)
													elseif block.name=="dogright" then love.graphics.draw (robodogeyeon, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
													end
											end
												if block.name=="lightswitch" then love.graphics.draw (lightswitch, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="nut" then love.graphics.draw (nut, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="pipehorizontal" then love.graphics.draw (pipehorizontal, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="door" then love.graphics.draw (door, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif block.name=="sciplatform" then love.graphics.draw (sciplatform, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												end
										elseif lightswitchon==false then
												love.graphics.setColor(1/subbeat/3+0.35,0,0,1)
														if block.name=="dogleft" then love.graphics.draw (robodoglightoff, ((x+9+dx-1))*tileSize, (y+dy-1)*tileSize,0,-0.5,0.5)
													elseif block.name=="dogright" then love.graphics.draw (robodoglightoff, ((x-7+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
													elseif block.name=="lightswitch" then love.graphics.draw (lightswitchoff, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
													end
										end
										love.graphics.setColor(0,0,0,1)
										
								end
							
						
							--level 57
								elseif nLevel==57 then
							if i==1 then
									if block.name=="incubator" then love.graphics.draw (incubator, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="incubator2" then love.graphics.draw (incubator2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="incubator3" then love.graphics.draw (incubator3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="incubator4" then love.graphics.draw (incubator4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe4" then love.graphics.draw (pipe4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe5" then love.graphics.draw (pipe5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe6" then love.graphics.draw (pipe6, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="s1" then love.graphics.draw (spaceworm, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
							--level 58
							elseif nLevel==58 then
								if i==1 then
										if block.name=="vendingmachine" then love.graphics.draw (vendingmachine, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe" then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe3" then love.graphics.draw (pipe3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										--[[	if timer<10 then love.graphics.draw (vendingmachine, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
											elseif timer>8 and timer<14 then
											
													if transitionmixdone==false then love.audio.play( transitionmix ) transitionmixdone=true end	--sound of transition
											
															love.graphics.setColor(math.random(0.5,1),math.random(0.5,1),math.random(0.5,1))
															love.graphics.draw (vendingmachine, math.random(0,2)+((x+dx-1))*tileSize, math.random(0,2)+(y+dy-1)*tileSize,0,0.5,0.5)
												
											elseif timer>14 and timer<20 then
											
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
											
													if laundrystarted==false then love.audio.play( washingmachinesound ) laundrystarted=true end		--sound of washingmachine
													if transitionmixdone2==false then love.graphics.draw (laundry, ((x+0.5+1.2+dx-1.1))*tileSize, (y+0.5+1.8+dy-1)*tileSize,washmachinedrumframe*8,0.5,0.5,25,25)
													end
													
											elseif timer>20 and timer<22 then 
												if transitionmixdone2==false then love.audio.play( transitionmix ) transitionmixdone2=true end	--sound of transition
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)			
													
													love.graphics.setColor(math.random(0.5,1),math.random(0.5,1),math.random(0.5,1))
													love.graphics.draw (laundry, (math.random(0,0.1)+(x+0.5+1.2+dx-1.1))*tileSize,math.random(0,0.5)+ (y+0.5+1.8+dy-1)*tileSize,washmachinedrumframe*8,0.5,0.5,25,25)
												
											elseif timer>22 then
													
													love.graphics.draw (washingmachine, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)			
													
													if squirrelframe>0   and squirrelframe<0.1 then love.graphics.draw (wormhole1, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.1 and squirrelframe<0.2 then love.graphics.draw (wormhole2, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.2 and squirrelframe<0.3 then love.graphics.draw (wormhole3, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.3 and squirrelframe<0.4 then love.graphics.draw (wormhole4, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.4 and squirrelframe<0.5 then love.graphics.draw (wormhole5, ((x+1.2+dx-1.1))*tileSize, (y+1.8+dy-1)*tileSize,0,0.5,0.5)
												end
											elseif timer>22 then
											
											end--]]
									
										elseif block.name=="capsule" then
													if squirrelframe>0   and squirrelframe<0.1 then love.graphics.draw (capsule1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) 
												elseif squirrelframe>0.1 and squirrelframe<0.2 then love.graphics.draw (capsule2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.2 and squirrelframe<0.3 then love.graphics.draw (capsule3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
												elseif squirrelframe>0.3 and squirrelframe<0.4 then love.graphics.draw (capsule4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) 
												elseif squirrelframe>0.4 and squirrelframe<0.5 then love.graphics.draw (capsule5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5) 
												end
										end
									end

							
							--level 59
						elseif nLevel==59 then
							if i==1 then
								if block.name=="amphora" then
								love.graphics.setColor(1,1,0.5)
										love.graphics.draw (amphora1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										love.graphics.setColor(cs1block)
								elseif block.name=="amphora2" then
										love.graphics.setColor(1,0.5,0.5)
										love.graphics.draw (amphora2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
										love.graphics.setColor(cs1block)
								elseif block.name=="amphora3" then love.graphics.draw (amphora3, ((x-0.15+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="amphora4" then love.graphics.draw (amphora4, ((x-0.15+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="mobject" then love.graphics.draw (mobject, ((x-2.3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="skullupleft" or block.name=="skullupleft+" or block.name=="skullupright" or block.name=="skullmidright" or block.name=="skullbottom" then
										love.graphics.draw (skull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="cross"  then love.graphics.draw (cross, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe"  then love.graphics.draw (pipe, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="pipe2hor"  then love.graphics.draw (pipe2hor, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="bigskull"  then love.graphics.draw (bigskull, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="totem2"  then love.graphics.draw (totem2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								elseif block.name=="d1"  then love.graphics.draw (d1, ((x-3+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
								end
							end
							
						--level 60
						elseif nLevel==60 then
								if i==1 then
									love.graphics.draw (gem1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1/dividx,1/dividy)
								end
						--level 61
						elseif nLevel==61 then
								if i==1 then
										if block.name=="1" or block.name=="2" then love.graphics.draw (gem1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1,1)
									elseif block.name=="3" or block.name=="4" then love.graphics.draw (gem2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1,1)
									elseif block.name=="5" or block.name=="6" then love.graphics.draw (gem3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1,1)
									elseif block.name=="7" or block.name=="8" then love.graphics.draw (gem4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1,1)
									elseif block.name=="9"					  then love.graphics.draw (gem5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,1,1)
									elseif block.name=="bigcup"				  then love.graphics.draw (bigcup, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="cupsmall"			  then love.graphics.draw (cupsmall, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="bigobject"			  then love.graphics.draw (bigobject, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="crown"				  then love.graphics.draw (crown, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="crown2"				  then love.graphics.draw (crown, ((x+dx-1))*tileSize, (y+2+dy-1)*tileSize,0,0.5,-0.5)
									elseif block.name=="crownsmall"			  then love.graphics.draw (crownsmall, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe"			  	  then love.graphics.draw (pipe, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipehor"			  then love.graphics.draw (pipehor, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="coins"			 	  then love.graphics.draw (coins, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="ring"			 	  then love.graphics.draw (ring, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="goldpiece"			  then love.graphics.draw (goldpiece, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
								end
							
						--level 65
						elseif nLevel==65 then
								if i==1 then
										if block.name=="pipe1" then love.graphics.draw (pipe1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="pipe2" then love.graphics.draw (pipe2, ((x-9+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece1" then love.graphics.draw (piece1, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece12" then love.graphics.draw (piece12, ((x-2+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece2" then love.graphics.draw (piece2, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece3" then love.graphics.draw (piece3, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece31" then love.graphics.draw (piece31, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece32" then love.graphics.draw (piece32, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece33" then love.graphics.draw (piece33, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece4" then love.graphics.draw (piece4, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece5" then love.graphics.draw (piece5, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece6" then love.graphics.draw (piece6, ((x-1+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece7" then love.graphics.draw (piece7, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									elseif block.name=="piece8" then love.graphics.draw (piece8, ((x+dx-1))*tileSize, (y+dy-1)*tileSize,0,0.5,0.5)
									end
								end
						end
				end
			
			    end

			
			if threeD==true then
			--if threeD==true and not (block.name=="chair1") then
		dream:draw(cube1, x+dx- threeDcellx+1, -((y+dy)- threeDcelly), threeDcellz,threeDcellwidth-0.5,threeDcellheight-0.5,threeDcelldepth-0.5)
		
    end
--dream:draw(cube1,  1, 1,-60, 10,10,10)
		
		--dream:draw(cube1, ((x+dx-1)*threeDcellwidth+1) , (y+dy-1)*threeDcellheight, threeDcellz,threeDcellwidth-0.5,threeDcellwidth-0.5,threeDcellwidth-0.5)
	end
end

function pb:drawBlocks ()
	love.graphics.setLineWidth(2)
	love.graphics.setColor(1,1,0.5)
	for i, block in ipairs (self.blocks) do
		-- draw filled block
		love.graphics.setLineWidth(1)
		--love.graphics.setColor(1,1,0.5)
		self:drawBlock (block)
		
		-- outline
		love.graphics.setLineWidth(3)
		if block.heavy then
			love.graphics.setColor(0,1,1)
		else
			love.graphics.setColor(0,1,0)
		end
		
		self:drawOutline  (block)
	end
	
	-- different colors for different status (screensaver, normal game)
	 
	

		if threeD==true then
			if screensaver==true then 
				love.graphics.setColor(0,1,0.5)
				diorama:rotateY(0.0001)
				dream:draw(diorama, 0, 0, -100,15,15,15)
				
			elseif screensaver==false then
		love.graphics.setColor(blockscolor)
			
    end
    dream:present()
		end
		
		love.graphics.setColor(1,1,1)		
end

function pb:drawDeadAgent (agent)
	local tileSize = self.gridSize 
	local x = (agent.x-1)*tileSize
	local y = (agent.y-1)*tileSize
	local w = agent.w*tileSize
	local h = agent.h*tileSize
	
	love.graphics.line (x, y, x+w, y+h)
	love.graphics.line (x, y+h, x+w, y)
end

local function drawTexture (agent, tileSize)
	
	local sx = agent.image_sx
	if agent.direction and agent.direction == "left" then
		sx = -agent.image_sx
	end

	--if shader2==true then 	--love.graphics.setColor(0.09,0.27,0.41)
	--end
	if palette==1 then
		
			-- default player assets for palette 1
		if not (nLevel==54 or nLevel==55 or nLevel==56 or nLevel==110) then			-- draw default assets on all levels excepting the special ones
		
			love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		end
		-- special colors and effects of the player's assets for different levels
		
		if nLevel==9 then 										--mirror level 9		  	
			--mirror
			love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
				
																-- mirror right
				if agent.x==7  and not (agent.y==2) then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
							(agent.y-1)*tileSize+agent.image_dy,
							0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif agent.direction=="left" then											-- mirror left
							love.graphics.draw (player1mirrorleft, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
							(agent.y-1)*tileSize+agent.image_dy,
							0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
						end
				elseif agent.x==7 and agent.y==2 then
							if agent.direction=="left" then	
								love.graphics.draw (player1mirror3left, 
								(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
								(agent.y-1)*tileSize+agent.image_dy,
								0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
							end
				
				elseif agent.x==8 and not (agent.y==2) then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror2, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
							(agent.y-1)*tileSize+agent.image_dy,
							0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
						elseif agent.direction=="left" then	
							love.graphics.draw (player1mirror2left, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
							(agent.y-1)*tileSize+agent.image_dy,
							0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
						end
				elseif agent.x==8 and agent.y==2 then
						if agent.direction=="right" then	
							love.graphics.draw (player1mirror3, 
							(agent.x)+(19+1*-agent.x)*tileSize+agent.image_dx, 
							(agent.y-1)*tileSize+agent.image_dy,
							0, sx*-1, agent.image_sy, agent.image_ox,  agent.image_oy)
						
						end
				end
					
		
		
			
		
		elseif nLevel==54 then
					if enginepushed==false then		love.graphics.setColor(1,1,1,1)   						--engine level 54
				elseif enginepushed==true then 		love.graphics.setColor(subbeat/3+0.35,0,0,1)
				end
			love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
			
		elseif nLevel==55 then
					if pipepushed==false then		love.graphics.setColor(1,1,1,1)   						--steel level 55
				elseif pipepushed==true then 		love.graphics.setColor(1/subbeat/3+0.35,0,0,1)
				end
					love.graphics.draw (agent.image, 
					(agent.x-1)*tileSize+agent.image_dx, 
					(agent.y-1)*tileSize+agent.image_dy,
					0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
			
			--if nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==false then		 --lights level 56
			--love.graphics.setColor(1/subbeat/3.2,1,1,1)
		elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==false then			-- lights are on
			love.graphics.setColor(0.3,0.8,0.8,1)
		
			love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		elseif nLevel==56 and lightswitchon==true and lightswitchpushedafterfalling==true then			-- lights are on
			love.graphics.setColor(1/subbeat/3.2-0.5,0.8,0.8,1)
		
			love.graphics.draw (agent.image, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif nLevel==56 and lightswitchon==false and agent.name=='fish-3x1'  then						-- lights are off, show only eyes
			love.graphics.draw (fish3x1_nolight, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif nLevel==56 and lightswitchon==false and agent.name=='fish-4x2'  then						-- lights are off, show only eyes
			love.graphics.draw (fish4x2_nolight, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif nLevel==65 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif nLevel==65 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		elseif nLevel==110 and agent.name=='fish-3x1' then 										--???
			--mirror
			love.graphics.draw (fish3x1space, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif nLevel==110 and agent.name=='fish-4x2' then 										--???
			--mirror
			love.graphics.draw (fish4x2space, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		end
		
		--c64
		elseif palette==2 and agent.name=='fish-3x1' then love.graphics.draw (c64fish3x1, 
	
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==2 and agent.name=='fish-4x2' then love.graphics.draw (c64fish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		--nes
		elseif palette==3 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==3 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		--sinclair
		elseif palette==4 and agent.name=='fish-3x1' then love.graphics.draw (z80fish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==4 and agent.name=='fish-4x2' then love.graphics.draw (z80fish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
	
		--megadrive
		elseif palette==5 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==5 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		
		--high contrast
		elseif palette==6 and agent.name=='fish-3x1' then love.graphics.draw (nesfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==6 and agent.name=='fish-4x2' then love.graphics.draw (nesfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
		
		--grid
		elseif palette==7 and agent.name=='fish-3x1' then love.graphics.draw (gridfish3x1, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		elseif palette==7 and agent.name=='fish-4x2' then love.graphics.draw (gridfish4x2, 
			(agent.x-1)*tileSize+agent.image_dx, 
			(agent.y-1)*tileSize+agent.image_dy,
			0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
		
	
		end
end

local function drawagentrectangle(agent, tileSize)
	local sx = agent.image_sx
	love.graphics.rectangle ('fill', (agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)
end

function pb:drawAgents ()

	local activeAgent = self.agent
	local tileSize = self.gridSize
	gridSize=self.gridSize
	for i, agent in ipairs (self.agents) do

		if agent.image then
			if agent == activeAgent then
				--love.graphics.setColor(1,1,1)
					if palette==1 then love.graphics.setColor(cs1agent1) 
				elseif palette==2 then love.graphics.setColor(cs2agent1)
				elseif palette==3 then love.graphics.setColor(cs3agent1)
				elseif palette==4 then love.graphics.setColor(cs4agent1)
				elseif palette==5 then love.graphics.setColor(cs5agent1)
				elseif palette==7 then 
				end
				drawTexture (agent, tileSize)
			else
				love.graphics.setColor(0.7,0.7,0.7)
					if palette==1 then love.graphics.setColor(cs1agent2) 
				elseif palette==2 then love.graphics.setColor(cs2agent2)
				elseif palette==3 then love.graphics.setColor(cs3agent2)
				elseif palette==4 then love.graphics.setColor(cs4agent2)
				elseif palette==5 then love.graphics.setColor(cs5agent2)
				elseif palette==6 then love.graphics.setColor(cs6agent2)
				elseif palette==7 then 
				end
				drawTexture (agent, tileSize)
			end
		else
			if agent == activeAgent then
				love.graphics.setColor(1,1,1)
				self:drawBlock (agent)
				
				local x, y = agent.x, agent.y
				love.graphics.setColor (0, 0, 0)
				love.graphics.print (agent.x..' '..agent.y, (agent.x-1)*tileSize, (agent.y-1)*tileSize)
			else
				love.graphics.setColor(0.75,0.75,0.5)
				self:drawBlock (agent)
			end
			
			-- outline
			love.graphics.setLineWidth(3)
			if agent.heavy then
				love.graphics.setColor(0,1,1)
			else
				love.graphics.setColor(0,1,0)
			end
			self:drawOutline  (agent)
		end

		if agent.dead then
			love.graphics.setColor(0,0,0)
			self:drawDeadAgent (agent)
		end
		

	end
	love.graphics.setColor(1,1,1)
end



function pb:drawMouse ()
	--love.graphics.setColor (0, 1, 0)
	--love.graphics.print (x..' '..y, (x-1)*tileSize, (y-1)*tileSize) -- beware of -1
	--love.graphics.print (mx..' '..my, (x-1)*tileSize, (y-1)*tileSize)
	
		-- control coordinates:	Left: 	X:	1045 - 1145		right 	1175 - 1270	
										--Y: 810 - 905
								--		X_	1113 - 1200		
								--Up	Y:	750 - 840		down	875 - 965
		
		--Player selection coor: Big fish:		X:50 - 242
										--		Y:900 - 995
								--Small fish	X:250 - 440
										
		
		-- Configuration button:X:	1150 - 1226
								--Y:48 - 122
       if love.mouse.isDown(1) then
		if mx>1045 and mx<1145 and my>810 and my<905 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --left 
            dx = -1
            dy = 0
            			pb:mainMoving (dx, dy)
        
        elseif mx>1175 and mx<1270 and my>810 and my<905 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --right
            dx = 1
            dy = 0
            			pb:mainMoving (dx, dy)
        
        elseif mx>631 and mx<1200  and my>875 and my<965 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --down
            dx=0
            dy = 1
            			pb:mainMoving (dx, dy)
        
        elseif mx>1113 and mx<1200 and my>750 and my<840 and touchinterfaceison==true then love.timer.sleep( 0.1 ) --up
            dx=0
            dy = -1
            			pb:mainMoving (dx, dy)
      
      elseif mx>250 and mx<448 and my>900 and my<995 and touchinterfaceison==true and ispaneldown==false then love.timer.sleep( 0.2 ) --Select small fish
      self.agent = self.agents[1]
      elseif mx>50 and mx<242 and my>900 and my<995 and touchinterfaceison==true and ispaneldown==false then love.timer.sleep( 0.2 ) --Select big fish
      self.agent = self.agents[2]
      elseif mx>1150 and mx<1226 and my>48 and my<122 and touchinterfaceison==true and panelright==true then --Options menu
      shader2=false
      gamestatus="options"
      if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
      elseif mx>1150 and mx<1226 and my>149 and my<224 and touchinterfaceison==true and panelright==true then --levelselection menu
      shader2=false
      gamestatus="levelselection"
      music:stop()
		:stop()
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static") end
      love.timer.sleep( 0.3 )
      --lovebpmload("/externalassets/music/CleytonRX-Underwater.ogg")
    
		
  elseif mx>1170 and mx<1225 and my>315 and my<395 and touchinterfaceison==true then if  panelright==true then  touchinterfaceison=false gui21:play() love.timer.sleep( 0.3 ) elseif  touchinterfaceison==false and panelright==true then  touchinterfaceison=true love.timer.sleep( 0.3 ) end --Touch controls on / off
  
  --panels
  
  elseif mx>1170 and mx<1225 and my>400 and my<470 and touchinterfaceison==true then if panelright==true then gui21:play() love.timer.sleep( 0.3 )  panelright=false elseif  panelright==false and touchinterfaceison==true then gui24:play() love.timer.sleep( 0.3 ) panelright=true end --panel right show / hide
  --[[elseif mx>1140 and mx<1240 and my>490 and my<570 and touchinterfaceison==true then 
if panelright==true then
	gui24:play() 
	love.timer.sleep( 0.3 ) 
	--require ("lib/netcontrols/netserver")
	      love.graphics.setColor(1,1,1)
	  love.graphics.rectangle('fill',600,600,400,150)
	  love.graphics.setColor(0,0,0)
      love.graphics.print("This option is still under development",600,620) 
	--print("netcontrol")
end -- net controls server
--]]		
elseif mx>500 and mx<600 and my>900 and my<1000 and touchinterfaceison==true then if ispaneldown==true then gui21:play() love.timer.sleep( 0.3 )  ispaneldown=false elseif  ispaneldown==false and touchinterfaceison==true then gui24:play() love.timer.sleep( 0.3 ) ispaneldown=true end --panel down show / hide

		--dialogs on
		elseif  mx>200 and mx<288 and my>830 and my<985 and touchinterfaceison==true and ispaneldown==true and talkies==false then
		talkies=true createcanvas() 
        if nLevel==1 then  Obey.lev1()
		elseif nLevel==2 then  Obey.lev2()
    elseif nLevel==3 then  Obey.lev3()
    elseif nLevel==4 then  Obey.lev4()
    elseif nLevel==5 then  Obey.lev5()
    elseif nLevel==6 then  Obey.lev6()
    elseif nLevel==7 then  Obey.lev7()
		end
      
     
     --sound
    elseif mx>300 and mx<375 and my>900 and my<980 and touchinterfaceison==true and soundon==true and ispaneldown==true and talkies==false then soundon=false   gui21:play() love.timer.sleep( 0.2 ) nLevel=nLevel-1 changelevel() --turn sounndon on
  elseif mx>300 and mx<375 and my>900 and my<980 and touchinterfaceison==true and soundon==false and ispaneldown==true and talkies==false then soundon=true   gui21:play() love.timer.sleep( 0.2 ) nLevel=nLevel-1 changelevel()--turn sounndon on
     
     --music
      elseif mx>400 and mx<473 and my>900 and my<970 and touchinterfaceison==true and ispaneldown==true and talkies==false and musicison==true then  gui21:play() musicison=false --turn music off
          --if player:hasEnded() then player:play(true) else player:stop() end
          love.timer.sleep( 0.2 )
          nLevel=nLevel-1 changelevel()
          if assets==2 then
          love.audio.stop()
          end
      elseif mx>400 and mx<473 and my>900 and my<970 and touchinterfaceison==true and ispaneldown==true and talkies==false and musicison==false then  gui21:play() musicison=true --turn music on
          --if player:hasEnded() then player:play(true) else player:stop()  end
          love.timer.sleep( 0.2 )
          nLevel=nLevel-1 changelevel()
          if assets==2 then
          nLevel=nLevel-1
          changelevel()
          end
			

  
  elseif mx>1150 and mx<1226 and my>249 and my<306 and touchinterfaceison==true then if screenoptions==false then  gui21:play() screenoptions=true	end --screen options menu
		
      --crteffect
      elseif mx>200 and mx<290 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true then  gui21:play() love.timer.sleep( 0.3 ) if shader1==true then shader1=false elseif shader1==false then shader1=true end
      --shadertoy
      elseif mx>400 and mx<490 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true then  gui21:play() love.timer.sleep( 0.3 ) if shader2==true then shader2=false elseif shader2==false then shader2=true end
      --change color
      elseif mx>600 and mx<690 and my>300 and my<360 and touchinterfaceison==true and screenoptions==true  then  shader2=false gui21:play() love.timer.sleep( 0.3 ) shader2=false palette=palette+1 createcanvas() if palette>7 then palette=0 end if palette==1 then shader2=true end
      --3d on off
      elseif mx>200 and mx<290 and my>500 and my<560 and touchinterfaceison==true and screenoptions==true then  gui21:play() love.timer.sleep( 0.3 ) if threeD==true then threeD=false elseif threeD==false then threeD=true end
      --diorama
      elseif mx>400 and mx<490 and my>500 and my<560 and touchinterfaceison==true and screenoptions==true then  gui21:play() love.timer.sleep( 0.3 ) if screensaver==false then screensaver=true shader2=true threeD=true elseif screensaver==true then screensaver=false end
      -- screen resolution
      elseif mx>600 and mx<690 and my>500 and my<560 and touchinterfaceison==true then --res=res+1 love.timer.sleep( 0.3 ) changeresolution()
      love.graphics.setColor(1,1,1)
	  love.graphics.rectangle('fill',600,600,300,150)
	  love.graphics.setColor(0,0,0)
      love.graphics.print("This option is still under development",600,620) 
      love.graphics.print("Use the key -R- to change resolution",600,650) 

      -- level editor
      --[[elseif mx>200 and mx<290 and my>700 and my<760 and touchinterfaceison==true then 
      love.timer.sleep( 0.5 )
       TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_030.ogg","static")
       essenceeditor = require ('leveleditor')
      
      
      --touchinterfaceison=false
      --leveleditor=true
      --gamestatus="game"
      --]]
      
      -- Close screen options
      elseif mx>400 and mx<470 and my>700 and my<760 and touchinterfaceison==true then  gui24:play() screenoptions=false
      end
end
end

return pb

