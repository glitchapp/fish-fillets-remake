function draw_gamegui()
--rectangle right
			love.graphics.setColor(0.5,0.5,0.5,0.8)
			love.graphics.rectangle('fill',800,50,350,100)
	--text right
			love.graphics.setColor(1,1,1)
			love.graphics.setFont(love.graphics.newFont(20))
			love.graphics.print("Press space to switch characters:",800,70)
			love.graphics.print("Press h for help / 1 for debug",800,90)
			if talkies==true then
			love.graphics.print("Press c to close the tutorial",800,120) end

end


   function draw_levelnumber()
			
			--rectangle left
			love.graphics.setColor(0.5,0.5,0.5,0.8)
			love.graphics.rectangle('fill',250,50,200,150)
			
			love.graphics.setColor(1,1,1)
            --love.graphics.setFont(love.graphics.newFont(12))
			
			love.graphics.setFont(love.graphics.newFont(20))
			--text left
            love.graphics.print("Level :" .. nLevel,250,50)	--print currentlevel
				--print("=========================================================")
				--print("Level :" .. nLevel)
			
			--love.graphics.print("player1 position: x= " ..f1x .. "y=:" .. f1y,50,50)		
			--love.graphics.print("player2 position: x= " ..f2x .. "y=:" .. f2y,50,70)

end

function draw_resolution()
				love.graphics.setFont(love.graphics.newFont(20))
				if res==1 then  love.graphics.print("Res :" .. res .. ": 1920x1080" ,50,50)	--print currentlevel
					--print("Res :" .. res .. ": 1920x1080") 
				elseif res==2 then 	love.graphics.print("Res :" .. res .. ": 256x192" ,50,50)	--print currentlevel
				--print("Res :" .. res .. ": 256x192")
				elseif res==3 then	love.graphics.print("Res :" .. res .. ": 256x240" ,50,50)	--print currentlevel
				--print("Res :" .. res .. ": 256x240")
				elseif res==4 then	love.graphics.print("Res :" .. res .. ": 320x240" ,50,10)	--print currentlevel
				--print("Res :" .. res .. ": 320x240")
				elseif res==5 then	love.graphics.print("Res :" .. res .. ": 640x480" ,50,50)	--print currentlevel
				--print("Res :" .. res .. ": 640x480")
				elseif res==6 then love.graphics.print("Res :" .. res .. ": 1024x768" ,50,50)	--print currentlevel
				--print("Res :" .. res .. ": 1024x768") 
				end
end

function draw_extradebug()
			love.graphics.setColor(0.5,0.5,0.5,0.8)
			love.graphics.rectangle('fill',410,950,450,55)
			love.graphics.setFont(love.graphics.newFont(20))
			love.graphics.setColor(1,1,1)
			 love.graphics.print("Press 1 to switch between: old code / new code",410,950)
			 draw_debugmodeselection()

end

	

function draw_debugmodeselection()
love.graphics.setColor(1,1,1)
			if problemdebug=="1" then love.graphics.rectangle('line',680,950,100,20)
			elseif problemdebug=="2" then love.graphics.rectangle('line',780,950,100,20)
			end
	
end



function update_slabdebugmenu(dt)
	
Slab.BeginWindow('debugmenu', {Title = "Debug menu", AutoSizeWindow = false,X=10,Y=160,W=35,H=300,BgColor={0.5,0.5,0.5,0.5}})

--local PowerColor = PowerButton and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
--Slab.Image('gridicon', {Path = "assets/interface/debug/gridicon.png",Image = Power, Color = PowerColor,Scale=1})
--PowerButtonx = Slab.IsControlHovered()
	--if Slab.IsControlClicked() and debugmode=="yes" then love.timer.sleep(0.1) debugmode="no" 
--elseif Slab.IsControlClicked() and debugmode=="no "then love.timer.sleep(0.1) debugmode="no" end
--if debugmode=="no" then PowerButton=true end
--if debugmode=="yes" then PowerButton=false end

local PowerColor1 = PowerButton1 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('bugicon', {Path = "assets/interface/debug/bugicon.png",Tooltip="Switch code",Image = Power, Color = PowerColor1,Scale=1})
--PowerButton1 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and problemdebug=="1" then love.timer.sleep(0.1) problemdebug="2"
elseif Slab.IsControlClicked() and problemdebug=="2" then love.timer.sleep(0.1) problemdebug="1" end
--PowerButton2 = not PowerButton2 end
if problemdebug=="1" then PowerButton1=true end
if problemdebug=="2" then PowerButton1=false end

local PowerColor2 = PowerButton2 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('backgroundicon', {Path = "assets/interface/debug/backgroundicon.png",Tooltip="Background switch",Image = Power, Color = PowerColor2,Scale=1})
--PowerButton2 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and background=="yes" then love.timer.sleep(0.1) background="no" coloredcells="no" charactercells="no" 
elseif Slab.IsControlClicked() and background=="no" then  love.timer.sleep(0.1) background="yes" coloredcells="yes" charactercells="yes" end
--PowerButton2 = not PowerButton2 end
if background=="no" then PowerButton2=true end
if background=="yes" then PowerButton2=false end

local PowerColor3 = PowerButton3 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('objectsicon', {Path = "assets/interface/debug/objectsicon.png",Tooltip="Object switch",Image = Power, Color = PowerColor3,Scale=1})
--PowerButton3 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and objectscells=="yes" then love.timer.sleep(0.1) objectscells="no" 
elseif Slab.IsControlClicked() and objectscells=="no" then love.timer.sleep(0.1) objectscells="yes" end
--PowerButton3 = not PowerButton3 end
if objectscells=="no" then PowerButton3=true end
if objectscells=="yes" then PowerButton3=false end

local PowerColor4 = PowerButton4 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('fishicon', {Path = "assets/interface/debug/fishicon.png",Tooltip="Agent switch",Image = Power, Color = PowerColor4,Scale=1})
--PowerButton4 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and playerscells=="yes" then love.timer.sleep(0.1)  playerscells="no"
elseif Slab.IsControlClicked() and playerscells=="no" then love.timer.sleep(0.1)  playerscells="yes" end

--PowerButton4 = not PowerButton4 end
if playerscells=="no" then PowerButton4=true end
if playerscells=="yes" then PowerButton4=false end

local PowerColor5 = PowerButton5 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('fishicon2', {Path = "assets/interface/debug/fishicon2.png",Tooltip="Adjacent cells switch",Image = Power, Color = PowerColor5,Scale=1})
--PowerButton5 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and adjacentcells=="yes" then adjacentcells="no"
elseif Slab.IsControlClicked() and adjacentcells=="no" then adjacentcells="yes" end

--PowerButton5 = not PowerButton5 end
if adjacentcells=="yes" then PowerButton5=true end
if adjacentcells=="no" then PowerButton5=false end

local PowerColor6 = PowerButton6 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('milestonesicon', {Path = "assets/interface/debug/milestonesicon.png",Tooltip="Doc tool",Image = Power, Color = PowerColor6,Scale=1})
--PowerButton6 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and milestones=="yes" then milestones="no" love.window.setMode(668, 600, {resizable=true, borderless=false}) createcanvas() love.mouse.setVisible(true)
elseif Slab.IsControlClicked() and milestones=="no" then milestones="yes" love.window.setMode(1400, 800, {resizable=true, borderless=false}) createcanvas() love.mouse.setVisible(false) end
-- PowerButton6 = not PowerButton6 end
if milestones=="yes" then PowerButton6=true end
if milestones=="no" then PowerButton6=false end

local PowerColor7 = PowerButton7 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('helpicon', {Path = "assets/interface/debug/helpicon.png",Tooltip="Help switch",Image = Power, Color = PowerColor7,Scale=1})
--PowerButton7 = Slab.IsControlHovered()
	if Slab.IsControlClicked() and helpison=="yes" then helpison="no"
elseif Slab.IsControlClicked() and helpison=="no" then helpison="yes" end
--PowerButton7 = not PowerButton7 end
if helpison=="yes" then PowerButton7=true end
if helpison=="no" then PowerButton7=false end

local PowerColor8 = PowerButton8 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('updateicon', {Path = "assets/interface/debug/updateicon.png",Tooltip="Check for game updates",Image = Power, Color = PowerColor8,Scale=1})
--PowerButton8 = Slab.IsControlHovered()
			if Slab.IsControlClicked() and msgbox==true then msgbox=false
		elseif Slab.IsControlClicked() and msgbox==false then msgbox=true
		--local Result = Slab.MessageBox("Message Box", "This is a test message box.")
				--if Result ~= "" then
				--msgbox = false
				--end
		end				
if msgbox==true then PowerButton8=true end
if msgbox==false then PowerButton8=false end				


local PowerColor9 = PowerButton9 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('shadericon', {Path = "assets/interface/debug/shader.png",Tooltip="Shader",Image = Power, Color = PowerColor9,Scale=1})
--PowerButton9 = Slab.IsControlHovered()

			if Slab.IsControlClicked() and shader1==true then love.timer.sleep(0.1) shader1=false
		elseif Slab.IsControlClicked() and shader1==false then love.timer.sleep(0.1) shader1=true
		end
if shader1==true then PowerButton9=true end
if shader1==false then PowerButton9=false end	

local PowerColor10 = PowerButton10 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('shadericon2', {Path = "assets/interface/debug/shader2.png",Tooltip="Shader2",Image = Power, Color = PowerColor10,Scale=1})
--PowerButton9 = Slab.IsControlHovered()

			if Slab.IsControlClicked() and shader2==true then love.timer.sleep(0.1) shader2=false
		elseif Slab.IsControlClicked() and shader2==false then love.timer.sleep(0.1) shader2=true
		end
if shader2==true then PowerButton10=true end
if shader2==false then PowerButton10=false end	

local PowerColor11 = PowerButton11 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('vricon', {Path = "assets/interface/debug/vr.png",Tooltip="VrMode",Image = Power, Color = PowerColor11,Scale=1})
--PowerButton9 = Slab.IsControlHovered()

			if Slab.IsControlClicked() and vrmode==true then love.timer.sleep(0.1) vrmode=false
		elseif Slab.IsControlClicked() and vrmode==false then love.timer.sleep(0.1) 
		vrmode=true --objectscells="no" playerscells="no"	debugmode="no" shader1=false milestones="no" checkvr()
		checkvr()
		
		end
if vrmode==true then PowerButton11=true end
if vrmode==false then PowerButton11=false end				
		
			

local PowerColor12 = PowerButton12 and {1.0, 0.0, 0.0, 1.0} or {1.0, 1.0, 1.0, 1.0}
Slab.Image('powericon', {Path = "assets/interface/debug/powericon.png",Tooltip="Quit",Image = Power, Color = PowerColor12,Scale=1})
--PowerButton9 = Slab.IsControlHovered()
if Slab.IsControlClicked() then PowerButton12=true love.event.quit() end
--PowerButton9 = not PowerButton8 end
Slab.EndWindow()

--	if msgbox==true then
	--Slab.MessageBox("Message Box", "This is a test message box.")
	--love.graphics.print("This feature is still under development",50,270)	
	--end
end
