
--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This code defines a function called levelload() that performs several operations.

First, it calls another function reloadadjustedcolors() to be found in game/interface/optionsgraphics.lua

Next, it checks the value of a variable res and sets the value of a variable scalefactor based on the result.

Then, it creates two fonts, font1 and font3, using the Love2D game engine's love.graphics.newFont() function.

It loads two images, sphere and thumb, using the Love2D game engine's love.graphics.newImage() function. The sphere image is loaded from a WebP file, and the thumb image is set to thumb1.

The function then sets the values of various text strings based on the value of the language variable.

Finally, it sets the values of three additional text strings: confirmsavetext, rejectsavetext, and cutscenest, and resetsavegamet.
--]]

function levelload()
		reloadadjustedcolors()
		if res=="1080p" then scalefactor=1
	elseif res=="1440p" then scalefactor=1.34
	elseif res=="4k" then scalefactor=2
	else scalefactor=1
	end
	
	-- initialize bfocus only if a gamepad is detected
	if joystick then bfocus="exit" else bfocus="none" end

	thumb=thumb1

	font1 = love.graphics.newFont(12) -- standard font
	font3 = love.graphics.newFont(12*3) -- three times bigger

	--levelmap= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/levelselection.webp")))
	sphere= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/sphere.webp")))

		if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
	elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
	elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends" loadtext="Load" savetext="Save"
	elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" poorfishmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" poorfishmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" poorfishmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
	end

confirmsavetext="yes"
confirmexittext="yes"
rejectsavetext="no"
rejectexittext="no"
cutscenest="cutscenes"
resetsavegamet="Reset"
exitbuttonpressed=false
exitconfirmpressed=false
gamepadselectanimation=false
screensaver=false
--emulatedmousex=0
--emulatedmousey=0
emulatedmousexscale=0
emulatedmouseyscale=0
PlayingFromLevelEditor=false
	fwidth=150
	fheight=50
	
	-- Define global variables for the pulsating effect and particle sparkles
	pulsateTimer = 0
	sparkleParticles = {}
	
	button = {}
	
	
	if bfocustab=="languages" then drawOptionsFocusLanguages() end
	if language=="jp" or language=="chi" or language=="thai" or language=="tamil" or language=="hindi" then love.graphics.setFont( poorfish )
	--else love.graphics.setFont( button.font )
	end

--[[This code defines variables for various buttons that will be used in a game or application interface.
 Each button has its own properties such as text, position, rotation, scaling, color, and font.
 The buttons include options for cutscenes, music, extras, credits, loading, saving, resetting save games,
 confirming saves, rejecting saves, exiting, and using a level editor.
 The code also requires a text file for the English version of a level menu.
--]]

--buttons


	optionsButton = {
		text = optionstext,
		--x = 1500, y = 10, 
		x = 1100*scalefactor, y = 800*scalefactor, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	musicButton = {
		text = musictext,
		--x = 1500, y = 100, 
		x = 100, y = 100, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	extrasButton = {
	text = extrastext,
	--x = 1500, y = 200, 
	x = 1600, y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1}, alpha=1,
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	endButton = {
	text = endtext,
	--x = 1500, y = 300,
	x = 1600, y = 300,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1}, alpha=1,
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}
	
	
	creditsButton = {
		text = creditstext,
		--x = 1500, y = 400, 
		x = 100, y = 800*scalefactor, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

confirmexitButton = {
		text = "yes",
		x = 500,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
rejectexitButton = {
		text = "no",
		x = 800,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	loadButton = {
		text = loadtext,
		x = 1600,
		y = 200, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	saveButton = {
		text = savetext,
		x = 1600,
		y = 400, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	resetsavegameButton = {
		text = resetsavegamet,
		x = 1600,
		y = 500, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	confirmsaveButton = {
		text = "yes",
		x = 500,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	rejectsaveButton = {
		text = "no",
		x = 800,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	exitButton = {
		text = exittext,
		--x = 1500, y = 700,
		x = 1600, y = 100,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1}, alpha=1,
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}




	leditorButton = {
	text = leditortext,
	x = 1500,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1}, alpha=1,
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}


	require("assets/text/levelmenu/level-en")
	--loadlevelpreviews()
	
		AboutFFilletsButton = {
	text = "",
	textPosition ="top",
	x = 1200*scalefactor, y = 900*scalefactor,
    sx = 1,
    image = AboutIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	

	
	HowToPlayButton = {
	text = "",
	textPosition ="top",
	x = 1250*scalefactor, y = 900*scalefactor,
    sx = 1,
    image = InfoIcon,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = poorfishsmall,
    unlocked = true,
	}
	
end

--[[function loadlevelpreviews()
	lprenil = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/previews/l1.webp")))
	lpre1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/previews/l1.webp")))
	lpre2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/previews/l2.webp")))
end
--]]


--buttons

--[[This code defines a local function named isButtonHovered that takes a table named button as an argument.
 The button table has properties such as text, font, sx, sy, x, and y which are used to calculate the position and size of the button.
 The function checks whether the mouse pointer is over the button by comparing the mouse's position to the position and size of the button.
 If the mouse is over the button, the function sets the button's width and height properties to the calculated values and returns true. Otherwise, it returns false.
--]]

-- Function to check if a button (regular or image) is hovered
function isButtonHovered (button)
	  local x, y, width, height
  -- Check if the button is an image button
    if button.image then
        x = button.x
        y = button.y
        width = button.image:getWidth()
        height = button.image:getHeight()
        
    else
       -- Calculate dimensions for a regular button with text
        local font = button.font or love.graphics.getFont()
        local textWidth = font:getWidth(button.text)
        local textHeight = font:getHeight()
        local sx, sy = button.sx or 1, button.sy or button.sx or 1
        x = button.x
        y = button.y
        width = textWidth * sx
        height = textHeight * sy
    end

-- Get the current mouse position
    local mouseX, mouseY = love.mouse.getPosition()
     -- Calculate emulated mouse position (if needed)
    local emulatedmousexscale = (emulatedmousex / 64) * 1.34
    local emulatedmouseyscale = (emulatedmousey / 45)
    local gx = emulatedmousexscale * 48
    local gy = emulatedmouseyscale * 46

  -- Check if the mouse cursor is within the button's bounds
    if (mouseX >= x and mouseX <= x + width and mouseY >= y and mouseY <= y + height) or
       ((gx + 1) >= x and (gx - 1) <= x + width and (gy + 1) >= y and (gy - 1) <= y + height) then
          -- Set the button's dimensions to the calculated width and height
        button.w, button.h = width, height
        return true	-- Return true to indicate that the button is being hovered over
    end

       return false  -- Return false to indicate that the button is not being hovered over
end


--[[This code defines a function drawButton that takes three parameters: button, hovered, and text.
This function is used to draw a button on the screen with the specified properties.--]]

function drawButton (button, hovered,text)	--Defines a function drawButton with three parameters: button, hovered, and text.
	
	drawButtonFocus()
	love.graphics.setFont( button.font )	--Sets the font of the button to the font specified in button.font.
	
	-- regular buttons (without image)
	if gamestatus=="levelEditor" then
		hideLevelEditorButtons(button, hovered,text)	-- function called from game/status/leveleditorButtons.lua
	 else
    if hovered then									--If the button is being hovered over, 
		love.graphics.setColor(button.hoveredColor)	--the button color is set to button.hoveredColor, 
		
		 -- Draw a rectangle around the button when hovered
        love.graphics.rectangle('line', button.x, button.y, button.w, button.h)
    else
        love.graphics.setColor(button.color)
    end    
	
	end
	    
    -- If the button is an image
     if button.image then
        local x, y, width, height = button.x, button.y, button.image:getWidth(), button.image:getHeight()
		if hovered then									--If the button is being hovered over, 
			love.graphics.setColor(1,0.5,0.5,1)
		end
        -- Calculate the position for the text based on textPosition
        if textPosition == "top" then
            y = y - height / 2  -- Adjust the y-coordinate to be at the top of the image button
        elseif textPosition == "bottom" then
            y = y + height / 2  -- Adjust the y-coordinate to be at the bottom of the image button
        end

        -- Draw the image button
        love.graphics.draw(button.image, x, y, button.r, button.sx)
    else
        -- Handle regular buttons without images here
        love.graphics.print(button.text, button.x, button.y, button.r, button.sx)
    end

	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)	--Prints the specified text on the button at the position specified by button.x and button.y with the specified rotation button.r and scale button.sx.
	
	--updateButtonAlpha(button)
	updateButtonZoom(button)
	
end

--[[
function isImageButtonHovered(button)
    local x, y = button.x, button.y  -- Gets the position of the image button.
    local width = button.image:getWidth()  -- Gets the width of the image button.
    local height = button.image:getHeight()  -- Gets the height of the image button.
    local mouseX, mouseY = love.mouse.getPosition()  -- Gets the position of the mouse.

    if mouseX >= x and mouseX <= x + width and mouseY >= y and mouseY <= y + height then
        return true  -- Returns true to indicate that the image button is being hovered over.
    end

    return false  -- Returns false to indicate that the image button is not being hovered over.
end


-- Draw the background buttons using the drawImageButton function
function drawBackgroundsLevelEditor()
    local hovered = isButtonHovered(background1Button)
    drawImageButton(background1Button, hovered)
    if hovered and love.mouse.isDown(1) then
        drawingMode = "wall"
        love.timer.sleep(0.3)
    end

    hovered = isButtonHovered(background2Button)
    drawImageButton(background2Button, hovered)
    if hovered and love.mouse.isDown(1) then
        -- Handle click for background2Button
    end
    
end

--]]
 

-- draw the particle sparkles
function drawSparkleParticles()
    love.graphics.setColor(1, 1, 1, 0.8)
    for _, sparkle in ipairs(sparkleParticles) do
        love.graphics.circle('fill', sparkle.x, sparkle.y, sparkle.size)
    end
   
end

function updateButtonFocus(dt)
    -- Update pulsating effect
    pulsateTimer = pulsateTimer + dt
    if pulsateTimer >= 1 then
        pulsateTimer = 0
    end

    -- Update particle sparkles
    for i, sparkle in ipairs(sparkleParticles) do
        sparkle.x = sparkle.x + sparkle.speed * dt
        sparkle.y = sparkle.y + sparkle.speed * dt

        -- Remove sparkles that go off-screen
        if sparkle.x > love.graphics.getWidth() or sparkle.y > love.graphics.getHeight() then
            table.remove(sparkleParticles, i)
        end
    end
end

-- In your game update function (e.g., love.update), update the alpha when hovered for any button
function updateButtonAlpha(button)
    if isButtonHovered(button) then
        button.alpha = math.min(1, button.alpha + 0.05 ) -- Increase alpha gradually
    else
        button.alpha = math.max(0, button.alpha - 0.05 ) -- Decrease alpha gradually
    end
    
end

function updateButtonZoom(button)
local hoverScale = 1.2 -- Scale factor when hovered

		if isButtonHovered(button) and not (button.image) then
			button.sx = math.min(1.5, button.sx + 0.05) -- Increase button gradually
	elseif isButtonHovered(button) and button.image then
			--button.sx = math.min(1.5, button.sx + 0.05) -- Increase image gradually		
    elseif not (isButtonHovered(button)) and button.image then
			--button.sx = math.max(0.5, button.sx - 0.05) -- Decrease image gradually
    elseif not (isButtonHovered(button)) and not (button.image) then
			button.sx = math.max(1, button.sx - 0.05) -- Decrease button gradually
		
    end
end


function buttonfocusanimations()

-- Implement the pulsating effect on the buttons
    local focusColorShift = math.abs(math.sin(pulsateTimer * math.pi))
    
			if timer2>1 then --love.graphics.setColor(0, 1 - focusColorShift, 0, 1)
							love.graphics.setColor(0,1,0,1) 
								gamepadselectanimation=false
			elseif timer2<1 then love.graphics.setColor(0,timer2,timer2-1,1)
			end	
			
					if gamepadselectanimation==false then
				elseif gamepadselectanimation==true then
						if timer2<3 then fwidth=100+timer2*40 fheight=20+timer2*40
						end
						
				end
end



function drawButtonFocus()

		
	buttonfocusanimations()		-- focus color and size animations
		
	--if bfocus=="exit" or bfocus=="extras" or bfocus=="ends" or bfocus=="save" or bfocus=="reset" or bfocus=="music" or bfocus=="credits" or bfocus=="options" then	
	--end
	
	
	--level menu
		if bfocus=="exit" 	then love.graphics.rectangle ('line', exitButton.x, exitButton.y, fwidth, fheight)		--fwidth and fheight are the size variables used for the button focus animations
	elseif bfocus=="extras" then love.graphics.rectangle ('line', extrasButton.x, extrasButton.y, fwidth, fheight)
	elseif bfocus=="ends" 	then love.graphics.rectangle ('line', endButton.x, endButton.y, fwidth, fheight)
	elseif bfocus=="save" 	then love.graphics.rectangle ('line', saveButton.x, saveButton.y, fwidth, fheight)
	elseif bfocus=="reset" 	then love.graphics.rectangle ('line', resetsavegameButton.x, resetsavegameButton.y, fwidth, fheight)
	elseif bfocus=="music" 	then love.graphics.rectangle ('line', musicButton.x, musicButton.y, fwidth, fheight)
	elseif bfocus=="options" then love.graphics.rectangle ('line', optionsButton.x, optionsButton.y, fwidth, fheight)
	elseif bfocus=="credits" then love.graphics.rectangle ('line', creditsButton.x, creditsButton.y, fwidth, fheight)
	elseif bfocus=="rejectexit" then love.graphics.rectangle ('line', rejectexitButton.x, rejectexitButton.y, fwidth, fheight)
	elseif bfocus=="confirmexit" then love.graphics.rectangle ('line', confirmexitButton.x, confirmexitButton.y, fwidth, fheight)
	elseif bfocus=="confirmsave" then love.graphics.rectangle ('line', confirmsaveButton.x, confirmsaveButton.y, fwidth, fheight)
	elseif bfocus=="rejectsave" then love.graphics.rectangle ('line', rejectsaveButton.x, rejectsaveButton.y, fwidth, fheight)
	end
	
	-- After drawing all the button outlines, draw the particle sparkles
    --drawSparkleParticles()
	
end


--[[The first function drawTile draws a single tile on the screen. It takes in x and y coordinates and a tileSize value as parameters. It checks if the sum of x and y is even, and sets the tile color accordingly. Then, it sets the color to the chosen value, and draws a rectangle at the appropriate coordinates with the specified tile size.--]]
local function drawTile (x, y, tileSize)
	local color = {0.2, 0.2, 0.2}
	if (x+y)%2 == 0 then
		color = {0.3, 0.3, 0.3}
	end
	love.graphics.setColor (color)
	love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
end

--[[The second function drawCenteredText draws a given text string centered in a specified rectangle. It takes in rectX and rectY as the top-left corner of the rectangle, rectWidth and rectHeight as the dimensions of the rectangle, and the text string to be drawn. It then calculates the width and height of the text using the current font, and prints the text at the center of the rectangle.--]]
local function drawCenteredText (rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end

--[[The third function drawCenteredSprite draws a given sprite centered on a specified tile. It takes in x and y as the tile coordinates, tileSize as the size of the tile, and sprite as the image to be drawn. It first calculates the dimensions of the sprite, and then scales it to fit within the tile size. Finally, it draws the sprite at the center of the tile coordinates.--]]
local function drawCenteredSprite (x, y, tileSize, sprite)
--	x and y it tiles, [1,1] is top left
	local w, h = sprite:getDimensions()
	local scale = tileSize/math.max (w, h)
	love.graphics.draw (sprite, (x-0.5)*tileSize, (y-0.5)*tileSize, 0, scale, scale, w/2, h/2)
end

--[[This code defines a Lua table called "spheres" which holds the positions and levels of different levels in a game. The levels are organized into several groups, with the first group consisting of levels 1-8, and the second group consisting of levels 9-19. For each level, the table includes a "level" property that specifies the level number and an "x" and "y" property that specify the position of the level on a map.--]]
local spheres = {
-- 0. Fish house (8 levels)
	{level= 1, x=13.9, y=5.6},
	{level= 2, x=14.2, y=6.9},
	{level= 3, x=13.8, y=8.4},
	{level= 4, x=13, y=9.9},
	{level= 5, x=12.4, y=11},
	{level= 6, x=12.2, y=12.4},
	{level= 7, x=12.7, y=13.6},
	{level= 8, x=13.6, y=14.6},

-- 1. Ship Wrecks (9 - 19) (11 levels)
	{level= 9, x=14.7, y=10},
	{level=10, x=16.4, y=9.8},
	{level=11, x=18.1, y=9.3},
	{level=12, x=19.5, y=8.4},
	{level=13, x=20.6, y=7.1},
	{level=14, x=21,   y=5.5},
	{level=15, x=20.4, y=4},
	{level=16, x=19.1, y=3},
	{level=17, x=17.3, y=3.1},
	{level=18, x=16,  y=4.2},
	{level=19, x=16.8,  y=5.7},

-- 3. City in the deep (20-29)	 10 levels
	{level=20, x=10.8, y=10.3},
	{level=21, x=9.6, y=9.6},
	{level=22, x=8.5, y=8.5},
	{level=23, x=7.6, y=7.2},
	{level=24, x=7.3, y=5.7},
	{level=25, x= 7.8, y=3.9},
	{level=26, x= 9.2, y=2.7},
	{level=27, x= 10.7, y=2.4},
	{level=28, x= 11.6, y=3.7},
	{level=29, x= 11, y=5.3},
	
--5 Coral Reef	(30 - 37)		7 levels

	{level=30, x=14.2, y=12.8},
	{level=31, x=15.8, y=12.8},
	{level=32, x=17.2, y=13.6},
	{level=33, x=18, y=15},
	{level=34, x=17.7, y=16.5},
	{level=35, x=16.5, y=17.5},
	{level=36, x=15.1, y=17.5},
	{level=37, x=14.5, y=16.1},

-- 7. Dump (38-44) 				(7 levels)
		
	{level=38, x=12.6, y=15.9},
	{level=39, x=11.2, y= 17.1},
	{level=40, x=9.6, y= 17.8},
	{level=41, x=7.6, y= 17.8},
	{level=42, x=6.2, y= 16.5},
	{level=43, x=6.9, y= 14.7},
	{level=44, x=8.5, y= 15.2},

-- 2. Silver's ship (45-51) (7 levels)
	
	{level=45, x=19.9, y=10},
	{level=46, x=20.8, y=11.3},
	{level=47, x=22.2, y=11.9},
	{level=48, x=23.8, y=11.1},
	{level=49, x=24.3, y=9.3},
	{level=50, x=23.4, y=7.8},
	{level=51, x=21.7, y=8.9},

-- 4. UFO (52-58)	 (7 levels) (not 6)
	
	{level=52, x=7.4, y= 9.8},
	{level=53, x=6, y= 10.4},
	{level=54, x=4.4, y= 10.4},
	{level=55, x= 3.4, y= 9.4},
	{level=56, x= 3.7, y= 7.5},
	{level=57, x= 5.2, y= 7.2},
	{level=58, x= 5.5, y= 8.5},
-- 6. Treasure cave	 (59-64) (6 levels)

	
	{level=59, x=11.1, y=15.1},
	{level=60, x=10, y=13.7},
	{level=61, x=8.8, y=12.6},
	{level=62, x=7.3, y=12.1},
	{level=63, x=5.9, y=12.5},
	{level=64, x=4.6, y=13.6},


-- 8. Secret computer (65-70) (6 levels)

	{level=65, x=17.5, y=12.2},
	{level=66, x=18.9, y=12.7},
	{level=67, x=20, y=13.8},
	{level=68, x=21.1, y=14.7},
	{level=69, x=22.6, y=14.4},
	{level=70, x=23.7, y=13.7},

-- 9 Secret.			 (71-79) (9 levels)
	{level=71, x= 9, y=20},
	{level=72, x= 8, y=20},
	{level=73, x= 7, y=20},
	{level=74, x= 6, y=20},
	{level=75, x= 5, y=20},
	{level=76, x= 4, y=20},
	{level=77, x= 3, y=20},
	{level=78, x= 2, y=20},
	{level=79, x= 1, y=20},

}


--[[This code defines a function called drawfishhousesphere which draws a selection circle around fish houses based on the music beat.--]]
	function drawfishhousesphere(x,y)	--Defines the function with two parameters x and y,     The function name implies it will draw a circle around the levels buttons.
				if y>1 and y<10 and x==13 then	--Checks if y is greater than 1 and less than 10 and if x is equal to 13.
												--This condition ensures that only the fish house area is targeted.
					love.graphics.setColor(0.5,0.5,0)	--Sets the color for the circle to dark gray.
					love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)	-- Draws a circle with a radius of 20 + subbeat * 20 at position (-25+x50, -25+y50). The subbeat variable is used to sync the circle to the music.
					love.graphics.setColor(1,1,1)										-- Sets the color back to white.
					love.graphics.print (zonetext[1],500, 850,0,1) 						-- Prints the text stored in zonetext[1] at position (500, 850). This line seems to display the name of the zone.
					love.graphics.print ((y-1) .. '.' ..fishhouse0[y-1],500, 900,0,1) 	-- Prints the fish house number and level at position (500, 900).
					--love.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
				end
	end


function drawlevelmenubck()	-- Function to draw the background for the level menu
	local tileWidth = 24 -- Define the amount of tiles in the x and y direction
	local tileHeight = 16
	
	--[[love.graphics.setCanvas(canvas)
	warpshader2_2:send("time", timer)
	love.graphics.setShader(warpshader2_2)
	--]]
	
	-- If skin is "remake" then draw the background based on the screen resolution
	
	if skin=="remake" then
			if res=="1080p" then love.graphics.draw(menumapremake,0,0,0,0.5)
		elseif res=="1440p" then love.graphics.draw(menumapremake,0,0,0,0.7)
		elseif res=="4k" then love.graphics.draw(menumapremake,0,0,0,1)
		else love.graphics.draw(menumapremake,0,0,0,0.5)
		end
	
	
	--[[love.graphics.setCanvas()
	love.graphics.setShader()
	--]]
		-- Draw the branches on top of the background
		love.graphics.draw(allbranch,0,0,0,0.52*scalefactor)
	end
	-- If skin is "classic"
	if skin=="classic" then
	-- If the skin is not upscaled, then draw the background
		if skinupscaled==false then
			love.graphics.draw(menumap,0,0,0,2.1)
			-- If skin is upscaled and the level 68 is not unlocked
		elseif skinupscaled==true then
		end
		-- Draw the upscaled background and branches of each unlocked zone
			if not (unlockedlevels[68]) then
				love.graphics.draw(menumapupscaled,0,0,0,0.52*scalefactor)
				love.graphics.draw(branch1house,0,0,0,0.52*scalefactor)
				
				if unlockedlevels[4] then love.graphics.draw(branch2ship,0,0,0,0.52*scalefactor) end
				if unlockedlevels[5] then love.graphics.draw(branch3city,0,0,0.52*scalefactor) end
				if unlockedlevels[12] then love.graphics.draw(branch5,0,0,0,0.52*scalefactor) end
				if unlockedlevels[22] then love.graphics.draw(branch4,0,0,0,0.52*scalefactor) end
				if unlockedlevels[7] then love.graphics.draw(branch67,0,0,0,0.52*scalefactor) end
				if unlockedlevels[58] then love.graphics.draw(branch67,0,0,0,0.52*scalefactor) end
			-- If all levels are unlocked, draw the complete background
			else love.graphics.draw(menumapupscaled_complete,0,0,0,0.52*scalefactor)
			end
		
	end

end



--[[This function draws the levels as spheres on the screen, with different sizes depending on the current screen resolution, and displays the level number on top of each sphere. It also checks if the level is unlocked, and if so, it will be displayed with a green color.--]]

function drawlevelspheres(x,y)
	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	local width, height = love.graphics.getDimensions()

	-- to make the spheres adaptive to window size:
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)

	--This section defines the width and height of the screen, and sets the tileSize to the smaller of either the width divided by the amount of tiles, or the height divided by the amount of tiles. It also sets a maximum tileSize of 50.
	local mx, my = love.mouse.getPosition()
	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1

	--This section gets the position of the mouse, and calculates which tile the mouse is currently hovering over.
	for i, spherelevel in ipairs (spheres) do
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y

			love.graphics.setColor(0,1,0) 
				if res=="720p"  then drawCenteredSprite ((x*1.04), (y*1.03), tileSize, sphere)
			elseif res=="1080p" then drawCenteredSprite (x, y, tileSize, sphere)
			elseif res=="1440p" then drawCenteredSprite ((x*1.34)-0.1, (y*1.34)-0.1, tileSize, sphere)
			elseif res=="4k" 	then drawCenteredSprite ((x*scalefactor/1.5)-0.2, (y*scalefactor/1.5)-0.2, tileSize*1.5, sphere)
			else drawCenteredSprite (x, y, tileSize, sphere)
			end
			love.graphics.setFont(poorfishsmall)
			love.graphics.setColor(0,0,0)
			drawCenteredText ((x-1)*tileSize*scalefactor, (y-1)*tileSize*scalefactor, tileSize, tileSize, level)
		end
	end
	--[[The section above loops through each sphere, checking if the corresponding level is unlocked. If it is, it sets the color to green, and draws the sphere centered on the screen with the size adjusted depending on the screen resolution. It also displays the level number in the center of the sphere.--]]
	


--[[This code appears to be checking whether the mouse is hovering over a particular sphere in a list of spheres. Here is a summary of the code with comments for each line:--]]

-- Declare a variable to hold the sphere that is currently being hovered over
	local sphereHovered

-- Loop through the list of spheres
	for i, spherelevel in ipairs (spheres) do
	-- Check if the level is unlocked
		if unlockedlevels[i]==true then
		-- Get the level and position of the current sphere
			local level = spherelevel.level
			local x1, y1 = spherelevel.x, spherelevel.y
			
			-- Scale the position of the sphere based on screen resolution
			if res=="1080p" then
				scalefactor=1
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
		elseif res=="1440p" then
				x1=x1*1.34
				y1=y1*1.34
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
				scalefactor=1.34
		elseif res=="4k"	then
				x1=x1*scalefactor-0.2
				y1=y1*scalefactor-0.2
				emulatedmousexscale=(emulatedmousex/64)*1.34
				emulatedmouseyscale=(emulatedmousey/45)
		else
			scalefactor=1
		end
			--print(emulatedmousexscale)
			--print(emulatedmouseyscale)
			
			-- Check if the mouse is hovering over the current sphere
			if x1 > x-1 and x1< x+1 and y1 > y-1 and y1< y+1 and love.mouse.isVisible( ) then
			-- If the mouse is hovering over the sphere, set the sphereHovered variable and break out of the loop
				sphereHovered = spherelevel
				break
			end
				if x1 > emulatedmousexscale-1 and x1< emulatedmousexscale+1 and y1 > emulatedmouseyscale-1 and y1< emulatedmouseyscale+1 and axistouchedonce==true then
					
						-- If the gamepad emulated mouse is hovering over the sphere, set the sphereHovered variable and break out of the loop
							sphereHovered = spherelevel
					
				break
			end
		end
	end

-- If a sphere is currently being hovered over and the save button has not been pressed, draw the sphere with a yellow border
	if sphereHovered and savebuttonpressed==false and exitbuttonpressed==false and not (aboutInfo==true) then
		-- Set the color to yellow
		love.graphics.setColor(1,1,0)
		
		-- Get the level and position of the hovered sphere
		local level = sphereHovered.level
		local x1, y1 = sphereHovered.x, sphereHovered.y
		
		-- Scale the position and font based on screen resolution
		if res=="1440p" then
			x1=x1*1.34-0.1
			y1=y1*1.34-0.1
			--love.graphics.setFont(poorfishmiddle)
			drawCenteredSprite (x1, y1, tileSize, sphere)
	elseif res=="4k" then
			x1=x1*scalefactor-0.5
			y1=y1*scalefactor-0.5
			love.graphics.setFont(poorfish)
			--drawCenteredSprite (x1, y1, tileSize, sphere)
	end
		if vibration==true and axistouchedonce and level~=lastgamepadtouchedlevel then
			  if leftxaxis>0.2 or leftxaxis<-0.2 or  leftyaxis>0.2 or leftyaxis<-0.2 then
					joystickvibration = joystick:setVibration(0.05, 0.05,0.05)
					lastgamepadtouchedlevel=level
			end
		
		end
		
			--[[ This code draws an animated selection circle around a sphere level and displays information about the level.--]]
		
				love.graphics.setColor(0.5,0.5,0)	-- Set the color of the animated selection circle to grey.
				love.graphics.circle("line",(x1-0.5)*tileSize, (y1-0.5)*tileSize,20+subbeat*20)	-- Draw a circle with line style using the specified x, y, and radius.
		
		love.graphics.setColor(1,1,1)	-- Set the color of the following graphics to white.
		drawCenteredText ((x1-1)*tileSize, (y1-1)*tileSize, tileSize, tileSize, level)	-- Draw centered text at the specified x, y position with the specified width and height.
				
displaylevelinformation(level)

	
				
		--[[This code checks if the left mouse button is pressed and if the current level is unlocked. If so, it sets several variables to specific values, indicating that the loading process is starting. It then sets the current level to a variable nLevel. Depending on the value of shader1type, it enables and disables different shader effects using the Love2D graphics library.--]]
		if (love.mouse.isVisible( ) and love.mouse.isDown(1)) or joystick and joystick:isGamepadDown("a") and unlockedlevels[level]==true then	-- This line checks if the left mouse button is currently being pressed (love.mouse.isDown(1)) and if the current game level is unlocked (unlockedlevels[level]==true).
			--These lines reset various variables to their default values, preparing them for the loading process.
			
			timer2=0
			timer3=0
			isloading=true
			loadingprocess=true
			loadbuttonpressed=false
			savebuttonpressed=false
			levelcompletedsentencesaid=false
			bfocus=nil
				--This line sets the value of the nLevel variable to be equal to the current level.
				nLevel=level
				loadcurrenttrackname()	-- Load the current music track name (function on loading.lua)
				
				if selectedTile==0 then selectedTile=nil end
					--This line checks if the shader1type variable is set to "crt".
					if shader1type=="crt" then
						--standard crt
						--These lines enable and disable various post-processing effects provided by the effect library, depending on which shader is selected. In this case, it is disabling several effects and enabling scanlines, crt, glow, and filmgrain.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
						effect.enable("scanlines","crt","glow","filmgrain")
				elseif shader1type=="godsgrid" then	--This line checks if the shader1type variable is set to "godsgrid".
						--These lines disable all post-processing effects except for godsray, which is enabled.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("godsray")
				elseif shader1type=="gameboy" then	--This line checks if the shader1type variable is set to "gameboy".
						--gameboy
						--These lines disable all post-processing effects except for scanlines and dmg, which are enabled. It then sets various parameters for those effects to create a Game Boy-like look.
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("scanlines","dmg")
						palette=3
						 effect.vignette.radius=1.2
						effect.vignette.opacity=3
						effect.dmg.palette=4
						effect.filmgrain.size=2.5
						effect.filmgrain.opacity=2
						effect.scanlines.width=1
						--effect.scanlines.thickness=0.5
				end			
		end
	end




------------------------------------------------
-- buttons
------------------------------------------------


	-- quit
	--This code checks if the exit button is being hovered over and then draws the button with the corresponding text. If the button is being hovered over and the left mouse button is pressed, the program waits for 0.5 seconds and then quits the game.
	local hovered = isButtonHovered (exitButton)
		 drawButton (exitButton, hovered,exittext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		exitbuttonpressed=true exitconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
			
			love.timer.sleep( 0.5 )	
		
	end

--[[This code displays two buttons: one to save progress and settings, and one to reset the saved game. If the mouse is hovering over a button and left-clicked, the corresponding action is triggered. If the save button is clicked, progress and settings are saved, and a video effect is enabled for half a second. If the reset button is clicked, all progress, achievements, and settings are deleted, and the video effect is enabled. Then, the achievements are loaded, and the save button is pressed.--]]
	-- save
	local hovered = isButtonHovered (saveButton)
	drawButton (saveButton, hovered,savetext)
	if hovered then love.graphics.print ("This option saves current progress, achievements and settings", 200,900) end
	
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		
		love.timer.sleep( 0.5 )	
	end
		
		-- reset saved game
	local hovered = isButtonHovered (resetsavegameButton)
	drawButton (resetsavegameButton, hovered,resetsavegamet)
	if hovered then love.graphics.print ("This option will delete all progress, achievements and settings", 200,900) end

	if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
	resetcolors()
		savebuttonpressed=true saveconfirmpressed=false timer2=0 inputtime=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		loadachievements()
		savebuttonpressed=true saveconfirmpressed=false timer2=0
		love.timer.sleep( 0.5 )	
	end
	

	-- options
	--This code is related to the "options" button in a game. It checks if the mouse is hovering over the button and if the left mouse button is clicked. If the button is clicked, it sets the palette to 1 if it is not already set to a valid value, loads the appropriate assets based on the selected skin, and sets the game status to "options".
	hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			
	if level1bck==nil then
		level1frg = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("externalassets/levels/level1/level1frg.webp")))
		level1bck = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck.webp")))
		level1bck2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/levels/level1/level1bck2.webp")))
	end

		  --prevent palette not to be assigned
		    if palette==1 or palette==2 or palette==3 or palette==4 or palette==5 or palette==6 or palette==7 then
		    else palette=1
		    end
		    if currentfrg==false then 
					if skin=="remake" then
						loadlevelassets()			-- load assets
						--loadlevelmusic()			-- load music
						assignfrgbck()				-- Assign layers
				elseif skin=="classic" then	
						loadlevelassetsclassic()	-- load classic assets
						--loadlevelmusicclassic()		-- load music
						assignfrgbckclassic()		-- Assign layers
				end
			end
		--loadlevelassets()
		gui076:play()
		optionsloadlanguages()
			bfocus="langarrowup"
			bfocustab="languages"
			gamestatus="options" 
		end
	end


	--credits
	--This code handles the "Credits" button. It checks if the button is being hovered over and if the left mouse button is pressed. If so, it stops any currently playing music, loads a new audio source for the credits music, sets its volume, and changes the game status to "credits" to switch to the credits screen.
	hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			--gui073:play()
			inputtime=0
			love.timer.sleep( 0.2 )
			music:stop()
				--alternative end credits version
				--loadassetscscene()
				--loadcutscenes()
				--gamestatus="creditsend" 
				--music = love.audio.newSource( "/externalassets/music/mixes/TylerMix.ogg","static" )
				
				--Standard Credits version
			music = love.audio.newSource( "/externalassets/music/pixelsphere/003_Vaporwareloop.ogg","static" )
				love.audio.play( music )
				music:setVolume(0.4)
				gamestatus="credits" 
		end
	end
	
	--Music player
	--This code creates a music player button that is only visible if the musicplayerunlocked variable is true. When the button is hovered and clicked, it stops the current music and sets the gamestatus to "music", allowing the player to access the music player menu.
if musicplayerunlocked==true then
	hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			inputtime=0
			cdplaying = false
			music:stop()
			currenttrackname="none"
			--loadassetscscene()
			--loadcutscenes()
			gui078:play()
			love.timer.sleep( 0.5 )
			gamestatus="music"
		end
	end
end

--[[This is a block of code that displays an "Extras" button if a certain condition is met (i.e., the "extrasunlocked" variable is true). When the button is hovered over and clicked with the left mouse button, it stops the current music and sets the game status to "extras", presumably to access some sort of extra content in the game.--]]
	-- extras
if extrasunlocked==true then
	hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			 music:stop()
			 gui078:play()
			gamestatus="extras" 
			love.timer.sleep( 0.3 )
		end
	end
end	

--This code checks if the endmenuunlocked flag is set to true. If it is true, it checks if the user is hovering over the endButton and if the left mouse button is clicked. If both conditions are true, it stops the current music, loads the "end menu" module and sets the game status to "endmenu".
if endmenuunlocked==true then	
	-- Ends

	hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  music:stop()
			   gui079:play()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
		end
	end
end


local hovered = isButtonHovered (AboutFFilletsButton)
			drawButton (AboutFFilletsButton, hovered, AboutFFilletsButton.text)
				if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
					
					windowX, windowY = 800, 360  -- Initial window position
					AboutFFilletsCloseButton.x = windowX+600
					AboutFFilletsCloseButton.y = windowY

					FFilletsHyperlinkButton.x = windowX + 200
					FFilletsHyperlinkButton.y = windowY + 540

					FFilletsAuthorsButton.x = windowX + 400
					FFilletsAuthorsButton.y = windowY + 540
		
					DonateButton.x =  windowX + 600
					DonateButton.y = windowY + 540
				
					CloseDonateButton.x = windowX + 600
					CloseDonateButton.y = windowY + 540
					aboutInfo=true
					donateInfo=false
					--video effect
					effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
					effect.enable("boxblur")
				elseif hovered then
					love.graphics.print("About Fish fillets remake", AboutFFilletsButton.x,AboutFFilletsButton.y+100,0,1)
				end
				
				    local hovered = isButtonHovered (HowToPlayButton)
				drawButton (HowToPlayButton, hovered)
					if hovered and (love.mouse.isDown(1)  or isjoystickbeingpressed(joystick,button)) and not (aboutInfo==true) then 
							
		
						shader2=false gamestatus="instructions"
						in1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/1.webp")))
						in2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/2.webp")))
						in3  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/3.webp")))
						in4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/4.webp")))
						in5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/5.webp")))
						in6 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/6.webp")))
						in7 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/7.webp")))
						prepareInstructions()
						love.mouse.setCursor(cursorRetro)
						
					elseif hovered then
					love.graphics.print("How to play", AboutFFilletsButton.x,AboutFFilletsButton.y+100,0,1)
				end
				
			

end

function prepareInstructions()

					effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.crt).chain(moonshine.effects.glow).chain(moonshine.effects.filmgrain)
					effect.scanlines.opacity=0.6
					effect.glow.min_luma = 0.2
					effect.filmgrain.size=1.5
					effect.filmgrain.opacity=2
					shader2=false gamestatus="instructions"
					love.graphics.setColor(1,1,1)
					love.timer.sleep( 0.3 )	

end


function displaylevelinformation(level)
				-- Display level information based on the level number.
if level>1 and level <9 then
-- Display the name of the zone and the name of the fishhouse for the current level.
love.graphics.print (zonetext[1],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..fishhouse0[level],500*scalefactor, 900*scalefactor,0)
elseif level>8 and level <20 then
-- Display the name of the zone and the name of the shipwreck for the current level.
love.graphics.print (zonetext[2],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..shipwrecks1[level-8], 500*scalefactor, 900*scalefactor,0,1)
elseif level>44 and level <52 then
-- Display the name of the zone and the name of the silver ship for the current level.
love.graphics.print (zonetext[3],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..silversship2[level-44], 500*scalefactor, 900*scalefactor,0,1)
elseif level>19 and level <30 then
-- Display the name of the zone and the name of the city in the deep for the current level.
love.graphics.print (zonetext[4],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..cityinthedeep3[level-19], 500*scalefactor, 900*scalefactor,0,1)
elseif level>29 and level <38 then
-- Display the name of the zone and the name of the coral reef for the current level.
love.graphics.print (zonetext[6],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..coralreef5[level-29], 500*scalefactor, 900*scalefactor,0,1)
elseif level>37 and level <45 then
-- Display the name of the zone and the name of the dump for the current level.
love.graphics.print (zonetext[8],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..dump7[level-37], 500*scalefactor, 900*scalefactor,0,1)
elseif level>51 and level <59 	then
-- Display the name of the zone and the name of the ufo for the current level.
love.graphics.print (zonetext[5],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..ufo4[(level-51)], 500*scalefactor, 900*scalefactor,0,1)
elseif level>58 and level <66 	then
-- Display the name of the zone and the name of the treasurecave for the current level.
love.graphics.print (zonetext[7],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..treasurecave6[(level-58)], 500*scalefactor, 900*scalefactor,0,1) 
elseif level>64 and level <71 	then
-- Display the name of the zone and the name of the secretcomputer for the current level.
love.graphics.print (zonetext[9],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..secretcomputer8[level-64], 500*scalefactor, 900*scalefactor,0,1)
elseif level>70 and level <79 	then
-- Display the name of the zone and the name of the nextgeneration levels(secret) for the current level.
love.graphics.print (zonetext[10],500*scalefactor, 850*scalefactor,0,1)
love.graphics.print ((level) .. '.' ..secret9[level-70], 500*scalefactor, 900*scalefactor,0,1)
end
end		

--[[The code defines two functions: drawloadmessage() and drawsavemessage().

The drawloadmessage() function is used to draw a message on the screen if the loadbuttonpressed variable is true. The message will display "Settings loaded" at position (500,500) on the screen with a font size of 1. The timer2 variable is used to reset loadbuttonpressed after 4 seconds.
--]]

function drawloadmessage()
	if loadbuttonpressed==true then
	love.graphics.print ("Settings loaded ", 500,500,0,1)
		if timer2>4 then 
			loadbuttonpressed=false 
		end
	end
end
--The drawsavemessage() function is used to draw a message on the screen if both savebuttonpressed and saveconfirmpressed variables are true. The message will display "Settings saved" at position (500,500) on the screen with a font size of 1. The timer2 variable is used to reset both savebuttonpressed and saveconfirmpressed after 4 seconds.
function drawsavemessage()
	if savebuttonpressed==true and saveconfirmpressed==true then
	love.graphics.print ("Settings saved ", 500,500,0,1)
		if timer2>4 then 
			savebuttonpressed=false 
			saveconfirmpressed=false
		end
	end
end
 
function exitmessage()
local playsoundonce=false
	if exitbuttonpressed==true and exitconfirmpressed==true then
	love.graphics.print ("shutting down in ".. (math.floor((timer2/2)-1)), 500,500,0,1)
	
		if timer2>1 and playsoundonce==false then underwatermovement2:play() playsoundonce=true end
		
		if timer2>2 then love.event.quit() end
	end
end

--[[The confirmsavebuttons() function handles the confirmation process for saving a game.
If the save button has been pressed and the save confirmation button has not been pressed, it will display a message asking the user if they are sure they want to proceed with the save.
It will also display two buttons, one to confirm the save and another to reject it.
If the confirm button is hovered over and clicked, it will set the saveconfirmpressed variable to true and call the savemygame() function to save the game.
If the reject button is clicked, it will reset the savebuttonpressed and saveconfirmpressed variables.
The drawloadmessage() and drawsavemessage() functions are used to display messages to the user when loading or saving the game settings.
--]]
function confirmsavebuttons()
		-- confirm save
if savebuttonpressed==true and saveconfirmpressed==false then
	local hovered = isButtonHovered (confirmsaveButton)
	drawButton (confirmsaveButton, hovered,confirmsavetext)

love.graphics.print ("Saving will replace any previous saved data, are you sure to proceed?", 200,400)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		saveconfirmpressed=true timer2=0 inputtime=0
		if joystick then bfocus="exit" end
		
		savemygame()	-- on game/mainfunctions/savegame.lua
		love.timer.sleep( 0.5 )	
	end
	
	-- reject save
	local hovered = isButtonHovered (rejectsaveButton)
	drawButton (rejectsaveButton, hovered,rejectsavetext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		negativegui:play()
		savebuttonpressed=false timer2=0 inputtime=0
		saveconfirmpressed=false
		if joystick then bfocus="exit" end
		love.timer.sleep( 0.5 )	
	end
end	
end

function confirmexitbutton()
-- confirm save
	if exitbuttonpressed==true and exitconfirmpressed==false then
		local hovered = isButtonHovered (confirmexitButton)
		drawButton (confirmexitButton, hovered,confirmexittext)
		love.graphics.print ("Do you really want to close the game?", 200,400)
			if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
				exitconfirmpressed=true timer2=0 inputtime=0
				love.timer.sleep( 0.1 )	
			end
	end
	-- reject exit
	local hovered = isButtonHovered (rejectexitButton)
	drawButton (rejectexitButton, hovered,rejectexittext)

	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		exitbuttonpressed=false timer2=0 inputtime=0
		exitconfirmpressed=false
		if joystick then bfocus="exit" end
		love.timer.sleep( 0.5 )	
	end

end



--This code is responsible for drawing the level selection menu of the game. It first checks if either the load button or the save button have been pressed, and if so, applies a shader effect to the background before drawing the level spheres and displaying any relevant messages. If neither button has been pressed, it simply draws the level spheres and the background without the shader effect.

function leveldraw()
	if loadbuttonpressed or savebuttonpressed or exitbuttonpressed and not (aboutInfo==true) then
			love.graphics.setShader()
			effect(function()
				drawlevelmenubck(x,y)
				drawlevelspheres(x,y)
			end)
			drawloadmessage()
			drawsavemessage()
			confirmsavebuttons()
			
	elseif aboutInfo==true then
		love.graphics.setShader()
			effect(function()
				drawlevelmenubck(x,y)
				drawlevelspheres(x,y)
			end)
				drawAboutFFillets()
				if donateInfo==true then
					drawDonateInfo()
				end
	else
		drawlevelmenubck(x,y)
		drawlevelspheres(x,y)
	end
	if exitbuttonpressed then
		confirmexitbutton()
	end
	if exitconfirmpressed then
		exitmessage()
	end
end

function drawonlytitles(x,y)
-- change shadow light from green to yellow when mouse hover spheres
	--[[if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end
	--]]
		--love.graphics.setFont(poorfishmiddle)
	  --Fish House levels 1-8 
		--if x>10 and x<16 and y>0 and y<12 and unlockedlevels[y-1]==true then drawfishhousesphere(x,y) end
	
	--[[
		--1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 and unlockedlevels[x]==true then drawshipwreckssphere(x,y) end
	    --2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 and unlockedlevels[x]==true then drawsilvershipsphere(x,y) end
       --3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 and unlockedlevels[x]==true then drawcitydeepsphere(x,y) end
		--4. UFO	--level 52-58
		if y>8 and y<10 and x>4 and x<14 and unlockedlevels[x]==true then drawufosphere(x,y) end
	   --5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 and unlockedlevels[x]==true then drawcoralreefsphere(x,y) end
	    --6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 and unlockedlevels[x]==true then drawtreasurecavesphere(x,y) end
		--7. Dump--level 38-44
		if x>12 and x<14 and y>8 and y<18 and unlockedlevels[y]==true then drawdumpsphere(x,y) end
		--8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 and unlockedlevels[x]==true then drawsecretcomputersphere(x,y) end
		-- 9. NG
		if y>11 and y<13 and x>3 and x<13 and unlockedlevels[x]==true then drawsecretsphere(x,y) end
		--]]
end

-- this function is not being used due to performance issues (it heavily impact performance)
function drawshadows(x,y,newBody)
	-- change shadow light from green to yellow when mouse hover spheres
	if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end

  --shadows --0. Fish House levels 1-8 
    if x>10 and x<16 and y>0 and y<12 then
        for i=1, 8, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, i*2*1)
			CircleShadow:new(newBody, 0, 20+i*50*1, 16)
		end
			drawfishhousesphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
				--shadows 1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then
			for i=1, 11, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 110)
				CircleShadow:new(newBody, 300+i*50*1, 110, 16)
			end
			drawshipwreckssphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
			
		end
	
			     --shadows 2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then 
			for i=1, 7, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 260)
				CircleShadow:new(newBody, 300+i*50*1, 260, 16)
			end            
			drawsilvershipsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
	       --shadows 3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then 
			for i=1, 10, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 140)
				CircleShadow:new(newBody, 315-i*50*1, 140, 16)
			end
			drawcitydeepsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
		--shadows 	4. UFO	--level 52-58
	if y>8 and y<10 and x>4 and x<14 then
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(315-i*0.5, 210)
			CircleShadow:new(newBody, 315-i*50*1, 210, 16)
		end 
		drawufosphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
	    --shadows 5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 then 
			for i=1, 8, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawcoralreefsphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 then
			for i=1, 4, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawtreasurecavesphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 7. Dump--level 38-44
      if x>12 and x<14 and y>8 and y<18 then 
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, 200+i*2*1)
			CircleShadow:new(newBody, 0, 200+i*50*1, 16)
		end
		drawdumpsphere(x,y)

       else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
       end
	
		 --shadows 8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then
			for i=1, 6, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 160)
				CircleShadow:new(newBody, 300+i*50*1, 160, 16)
			end
			drawsecretcomputersphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
                
        end

end

