local o_ten_one = require "lib/splashes/o-ten-one"
timer=0

function loadSplashScreen()
 splash = o_ten_one()
  splash.onDone = function() print "DONE" end
end

function updateSplashScreen(dt)
	splash:update(dt)
	timer=timer+dt
	
	if timer>4 then 
		splash:skip()
		 
		-- Start intro scene if no saved data (first start)
		nLevel=101 
		gamestatus="firststart" 
		love.graphics.setFont( font3 )
		screensaver=true
		changelevel()	
		caustics=true	-- set up caustics and dialogs on on first start
		talkies=true
		mousestate = not love.mouse.isVisible()
		love.mouse.setVisible(mousestate)
		--load extra assets
		kukajda_00_3000 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level44/kukajda_00_3000.webp")))
		snail1280 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/externalassets/objects/level7/snail1280.webp")))		
	end
end

function drawSplashScreen()
  splash:draw()
end
