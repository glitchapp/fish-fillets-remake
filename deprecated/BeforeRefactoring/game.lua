-- Update logic for "game" mode

--[[
This section of the code is responsible for updating the game during the "game" state.
It performs various tasks, such as updating LoveBPM and FPS metrics, limiting the frame rate if necessary,
and getting power information if battery status is enabled.

Additionally, if loading is in progress, it updates the loading icon and fade, and if loading process is triggered, 
it runs the process. If Talkies is enabled, it updates dialogs. 
The code also sets a maximum delta time of 0.05 seconds and updates graphics from 3DreamEngine if 3D graphics is enabled
and not running on Android. Finally, if Tetris is enabled, it updates blocks.
--]]

function updateGameState(dt)
	
lovebpmupdate(dt)                     -- update LoveBPM library
updatefpsmetrics(dt)                  -- update FPS metrics
updategetjoystickaxis()
updateRadialMenu()						--update the radial menu
if networkcontrol then					-- update routines if network controls enabled
	update_controller_state(data)			-- update controls from network control standalone app (remote control)
	receiveincomingmessages()				-- receive network control messages
end

if limitframerate==30 then            -- if frame rate limit is 30 fps, sleep if necessary
    if dt < 1/15 then love.timer.sleep(1/17 - dt) end
elseif limitframerate==60 then        -- if frame rate limit is 60 fps, sleep if necessary
    if dt < 1/60 then love.timer.sleep(1/40 - dt) end
end

if showbattery then                   -- if battery status is enabled, get power info
    powerstate, powerpercent, powerseconds = love.system.getPowerInfo( )
    if powerpercent==nil then         -- if power percentage is nil, set it to infinity
        powerpercent="∞" 
        powerseconds="∞" 
    end
end

if isloading==true then               -- if loading is in progress, update loading icon and fade
    loadingupdate(dt) 
    fadeloadingprocess(dt) 
end
if loadingprocess==true then          -- if loading process is triggered, run the process
    triggerloadingprocess(dt) 
end
if screenshottaken==true then			--if a screenshot has been taken
	updatetakescreenshot()
end

if talkies==true then                 -- if Talkies is enabled, update dialogs
    Talkies.update(dt)
end
if dt > 0.05 then 						-- set a maximum delta time of 0.05 seconds
	dt = 0.05 
end
if threeD==true and android==false then -- if 3D graphics is enabled and not running on Android, update graphics from 3DreamEngine
    dream:update()
end

	if not (aleatoryBordersentence==nil) and time3r>4 then	--restore dialogs volume after border dialogs triggered and enought time passed
		SetUpDialogsVolume(1)
	end

end

	
--[[
This part of the code is responsible for drawing the game screen when the game is in progress (gamestatus=="game"). It starts by drawing the background with a music visualizer using the "lovebpmdraw()" function.

If the game is set to 3D mode and not running on Android, it will draw 3D graphics using the "threeDdraw()" function.

The code then checks if the first shader is enabled (Moonshine) and if the second shader is disabled. If so, it will set the shader, draw different content depending on the selected skin (remake, retro, or classic), and draw dialogs using the "Talkies.draw()" function.

If both shaders are enabled, it will set the shader, draw different content depending on the selected skin, and draw dialogs using the "Talkies.draw()" function.

If the first shader is disabled and the second shader is enabled, it will draw different content depending on the selected skin and set the shader.

If both shaders are disabled, it will draw different content depending on the selected skin and set the color if the palette is 7.

If talkies are enabled and the first shader is disabled, it will draw dialogs using the "Talkies.draw()" function.

Finally, it will draw the touch interface using the "drawtouchinterface()" function.
	--]]
function drawGamestate()

	-- Draw background with music visualizer
	lovebpmdraw()

	-- Draw 3D graphics if enabled
	if threeD==true and android==false then
		threeDdraw()
	end

-- draw the blue noise shader if enabled
if shader1type=="bluenoise" then
 -- Set the dithering shader
    love.graphics.setShader(blueNoiseDitherShader)

    -- Pass the intensity value to the shader
    blueNoiseDitherShader:send("intensity", ditherIntensity)
				if skin=="remake" then
					drawallcontent()
					drawforshader2()
			elseif skin=="retro" then
					love.graphics.setShader()
					drawforretro()
			elseif skin=="classic" then
					drawallcontentclassic()
					drawforshader2classic()
			end
	  -- Reset the shader
    love.graphics.setShader()
else

-- Draw first shader if enabled (Moonshine)
	if shader1==true and shader2==false then
		love.graphics.setShader()
		effect(function()
			-- Draw different content depending on the selected skin
				if skin=="remake" then
					drawallcontent()
					drawforshader2()
			elseif skin=="retro" then
					love.graphics.setShader()
					drawforretro()
			elseif skin=="classic" then
					drawallcontentclassic()
					drawforshader2classic()
			end
		end)
			
			
		-- Draw dialogs if enabled
		if talkies==true then
			Talkies.draw()
		end
	end
	
-- Draw two shaders if enabled
	if shader1==true and shader2==true then
		effect(function()
			-- Draw different content depending on the selected skin
			if skin=="remake" then
				drawallcontent()
				love.graphics.setShader(shader)
				drawforshader2()
			elseif skin=="retro" then
				drawforretro()
			elseif skin=="classic" then
				drawallcontentclassic()
				love.graphics.setShader(shader)
				drawforshader2classic()
			end

			love.graphics.setShader()
						
			
		-- Draw dialogs if enabled
			if talkies==true then
				Talkies.draw()
			end
		end)
		
	end

-- Draw second shader if enabled (shadertoy)
	if shader1==false and shader2==true then
		-- Draw different content depending on the selected skin
		if skin=="remake" then
			drawallcontent()
			love.graphics.setShader(shader)
			drawforshader2()
		elseif skin=="retro" then
			drawforretro()
		elseif skin=="classic" then
			drawallcontentclassic()
			love.graphics.setShader(shader)
			drawforshader2classic()
		end
		love.graphics.setShader()
		-- If talkies are enabled and shader1 is disabled, draw dialogs
	if talkies==true and shader1==false then
		Talkies.draw()
	end
	end
	
	-- If both shaders are disabled
	if shader1==false and shader2==false then
		-- Draw different content depending on the selected skin
		if skin=="remake" then
			drawforshader2()
		elseif skin=="retro" then
			drawforretro()
		elseif skin=="classic" then
			drawforshader2classic()
			drawallcontentclassic()
		end
		-- Set color if palette is 7
		if diorama==false then
			if palette==7 then
				love.graphics.setColor(0,0,0)
			end
		end
		
		love.graphics.setShader()
		-- If talkies are enabled and shader1 and shader2 is disabled, draw dialogs
		if talkies==true and shader1==false then
			Talkies.draw()
		end

	end
end



	-- Draw touch interface
if touchinterfaceison==true then drawtouchinterface() end
--if touchinterfaceison==true then touchcontrolsdraw() end	-- render touch controls

-- If the game is loading, draw the loading icon
if isloading==true then loadingdraw(dt) end

fadehint()	-- Draw hints on specific levels (check game/interface/loading.lua & hint.lua)
fadescreenshotinformation()	-- information when screenshot is taken

if pauseisactive then 
	love.graphics.setFont(poorfish)
	love.graphics.print("Pause", 800, 900, 0, 0.8, 1)
end

if gamestatus == "gameplusoptions" then		-- Render the options menu over the game screen
		love.graphics.setShader()
		optionsdraw(dt)
	end
end




