function benchmarkload()
	nLevel=3
	loadultraassets()
	loadassetscutscenes()
	
	
	love.timer.sleep( 0.3 )
	fpsGraph.createGraph(x, y, 400, 300, delay, draggable)
	if isvsyncworking==love.window.getVSync( ) then isvsyncworking="Yes" else isvsyncworking="No" end	-- check vsync status
	processorCount = love.system.getProcessorCount( )								-- get processor count
	gpuname, gpuversion, gpuvendor, gpudevice = love.graphics.getRendererInfo( )	-- Get GPU info
	
		applybenchmarksettingsButton = {
		text = "yes",
		x = 1200,
		y = 450,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

end

function benchmarkupdate(dt)
	timer=timer +dt
	timer2=timer2 +dt
	if timer2>1 then timer2=0 end
	 fps = love.timer.getFPS( ) 					-- get FPS for metrics
		-- update graphs using the update functions included in fpsGraph
		fpsGraph.updateFPS(benchmarkGraph, dt)
		fpsGraph.updateMem(memoryGraph, dt)
		graphstats = love.graphics.getStats()
		sgraphstats = string.format("Texture mem used: %.2f MB", graphstats.texturememory / 1024 / 1024)
		
end

function benchmarkdraw(dt)
	
		playcutscene(dt)
	
		--water warp shader 4
				love.graphics.setCanvas(canvas)
				warpshader4:send("time", timer)
				love.graphics.setShader(warpshader4)
					drawbenchmarkscenelayer2(dt)
				love.graphics.setCanvas()
								
		--water warp shader 3
				love.graphics.setCanvas(canvas)
				warpshader3:send("time", timer)
				love.graphics.setShader(warpshader3)
			effect(function()	
			
			--drawcscene(dt)
			drawbenchmarkscene(dt)
	
		love.graphics.setCanvas()
		love.graphics.setShader()
	
		--cutscenetalkingfish()

			love.graphics.setColor(0,0,0.5,0.5)					-- draw rectangle
			--love.graphics.rectangle("fill",50,0,750,150)
			love.graphics.rectangle("fill",1550,20,350,550)
			love.graphics.setColor(1,1,1,1)
	-- draw FPS metrics
		fpsGraph.drawGraphs({benchmarkGraph})
	
		love.graphics.setColor(0,0,0,1)
		love.graphics.print("CPU threads: " .. processorCount,1572,252,0,1)
		love.graphics.print("Renderer: " .. gpuname,1575,305,0,1)
		love.graphics.print("V." .. gpuversion,1572,327,0,0.8)
		love.graphics.print("GPU: " .. gpuvendor,1575,355,0,1)
		love.graphics.print("" .. gpudevice,1572,377,0,0.8)
		love.graphics.print("Vsync: " .. isvsyncworking,1575,405,0,1)
		love.graphics.print(sgraphstats,1575,455,0,1)
		if fpsrate==nil then
			love.graphics.print("Results in " .. (math.floor(20-timer)) .. " s.",1575,505,0,1)	-- countdown for results
		end
					ifmousepressedreturn()	-- return to menu
		-- draw shadows
		love.graphics.setColor(1,1,1,1)
		love.graphics.print("CPU threads: " .. processorCount,1570,250,0,1)
		love.graphics.print("Renderer: " .. gpuname,1570,300,0,1)
		love.graphics.print("V." .. gpuversion,1570,325,0,0.8)
		love.graphics.print("GPU: " .. gpuvendor,1570,350,0,1)
		love.graphics.print("" .. gpudevice,1570,375,0,0.8)
		love.graphics.print("Vsync: " .. isvsyncworking,1570,400,0,1)
		love.graphics.print(sgraphstats,1570,450,0,1)
		if fpsrate==nil then
			love.graphics.print("Results in " .. (math.floor(20-timer)) .. " s.",1570,500,0,1)	-- countdown for results
			ifmousepressedreturn()	-- return to menu
		end
		
		if timer2>0 and timer2<0.5 then love.graphics.print("Press any mouse key to start",600,800,0, 0.8, 1) end
		--print(timer2)
	
			if timer>20 then
				if fpsrate==nil then fpsrate=fps/120 end	-- if rating not assigned create one by dividing the current fps to 120
				drawbenchmarkresults()	 -- print results
			end
		end)
end

function drawbenchmarkresults()
			love.graphics.setColor(0,0,0.5,0.5)					-- draw rectangle
			love.graphics.rectangle("fill",550,200,750,350)
			love.graphics.setColor(0,0,0,1)
	
		love.graphics.setFont( font3 )
		if fps<30 then					love.graphics.print("Your frame rate is below 30 FPS",600,300,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),600,350,0, 0.8, 1)
										love.graphics.print("Recommended Settings: Disable shaders",600,400,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",600,450,0, 0.8, 1)
										--draw shadows
										love.graphics.setColor(1,1,1,1)
										love.graphics.print("Your frame rate is below 30 FPS",595,295,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),595,345,0, 0.8, 1)
										love.graphics.print("Recommended Settings: Disable shaders",595,395,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",595,445,0, 0.8, 1)
										
										-- Apply recommended settings button
										
										local hovered = isButtonHovered (applybenchmarksettingsButton)
										drawButton (applybenchmarksettingsButton, hovered,savetext)
										if hovered then love.graphics.print ("Press yes to apply settings", 200,900) 
										else 		ifmousepressedreturn()	-- return to menu
										end
	
										if hovered and love.mouse.isDown(1) then 
												love.window.setVSync(false)	vsyncstatus=false			-- set vsync off
												shader1=false caustics=false shader2=false chromaab=false	-- set all effects off
												 skinhaschanged=true				-- set skin changed to ask to save changes
												gamestatus="options"
												currenttab="graphics"
												skinhaschanged=true
												mus:stop()
												love.timer.sleep( 0.5 )	
										end
										
	elseif fps>30 and fps <60 then		love.graphics.print("Your frame rate is below 60 FPS",600,300,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),600,350,0, 0.8, 1)
										love.graphics.print("Recommended: Standard profile + fps limit:30",600,400,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",600,450,0, 0.8, 1)
										--draw shadows
										love.graphics.setColor(1,1,1,1)
										love.graphics.print("Your frame rate is below 60 FPS",595,295,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),595,345,0, 0.8, 1)
										love.graphics.print("Recommended: Standard profile + fps limit:30",595,395,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",595,445,0, 0.8, 1)
										
										-- Apply recommended settings button
										
										local hovered = isButtonHovered (applybenchmarksettingsButton)
										drawButton (applybenchmarksettingsButton, hovered,savetext)
										if hovered then love.graphics.print ("Press yes to apply settings", 200,900) 
										else 		ifmousepressedreturn()	-- return to menu
										end
	
										if hovered and love.mouse.isDown(1) then 
												love.window.setVSync(false)	vsyncstatus=false			-- set vsync off
												shader1=true shader2=false chromaab=false				-- set caustics on
												palette=1 skin="remake" skinhaschanged=true				-- set skin to "retro"
												limitframerate=30										-- limit framerate to 30
												gamestatus="options"
												currenttab="graphics"
												skinhaschanged=true
												mus:stop()
												love.timer.sleep( 1 )	
										end
	
	elseif fps>60 then					love.graphics.print("Your frame rate is above 60 FPS",600,300,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),600,350,0, 0.8, 1)
										love.graphics.print("Recommended: High profile + fps unlimited",600,400,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",600,450,0, 0.8, 1)
										--draw shadows
										love.graphics.setColor(1,1,1,1)
										love.graphics.print("Your frame rate is above 60 FPS",595,295,0, 0.8, 1)
										love.graphics.print("Rate: " .. (fpsrate),595,345,0, 0.8, 1)
										love.graphics.print("Recommended: High profile + fps unlimited",595,395,0, 0.8, 1)
										love.graphics.print("Apply recommended settings now?",595,445,0, 0.8, 1)
										
										-- Apply recommended settings button
										
										local hovered = isButtonHovered (applybenchmarksettingsButton)
										drawButton (applybenchmarksettingsButton, hovered,savetext)
										if hovered then love.graphics.print ("Press yes to apply settings", 200,900) 
										else 		ifmousepressedreturn()	-- return to menu
										end
	
										if hovered and love.mouse.isDown(1) then 
												love.window.setVSync(true)	vsyncstatus=true		-- set vsync on
												shader1=true shader2=true chromaab=false			-- set all effects on
												palette=1 skin="remake" skinhaschanged=true			-- set skin to "remake"
												limitframerate=nil									-- Frame rate unlimited
												gamestatus="options"
												currenttab="graphics"
												skinhaschanged=true
												mus:stop()
												love.timer.sleep( 0.5 )	
										end
	end
	
		
end

function ifmousepressedreturn()
	if not (isButtonHovered (applybenchmarksettingsButton)) then
			if love.mouse.isDown(0) or love.mouse.isDown(1) or love.mouse.isDown(2) and not (isButtonHovered (applybenchmarksettingsButton)) then 
				love.graphics.setCanvas()
				love.graphics.setShader()
					mus:stop()
					shader1=false shader2=false autoload=true inputtime=0
		
					nLevel=1
					isloading=false
					loadingprocess=false
					gamestatus="levelselection"
					love.timer.sleep( 0.5 )
					music:play()
			
			end
	
	end
end

function drawbenchmarkscene(dt)
	if timer >0 and timer <23 then
					love.graphics.setColor(1,1,1,timer/10-0.4)
					
					love.graphics.draw (cutbck, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180)
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180) end
					--print(timer)
			elseif timer >23 and timer <33 then
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					love.graphics.draw (cutbck, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180)
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-2.3)
					love.graphics.draw (cutbck, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					if nLevel==1 then love.graphics.draw (cutfrg, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					--print(timer)
					--print(timer/10-2.3)
					
			elseif timer >33 and timer <50 then
					love.graphics.setColor(1,1,1,timer/-10+4.3)
					love.graphics.draw (cutbck, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					if nLevel==1 then love.graphics.draw (cutfrg, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1,1,timer/10-3.3)
			
					love.graphics.draw (cutbck, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					--print(timer)
					--print(timer/-10+4.3)
	
			elseif timer >50 then
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+6)
					love.graphics.draw (cutbck, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-5)
					love.graphics.draw (cutbck, -0+timer*2,-400,0,1.05+(timer/240),1.05+(timer/240))
					if nLevel==1 then love.graphics.draw (cutfrg, -0+timer*2,-400,0,1.05+(timer/240),1.05+(timer/240)) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-5)
					--print(timer)
					--print(timer/-10-6)
			end
end

function drawbenchmarkscenelayer2(dt)
	if timer >0 and timer <23 then
					love.graphics.setColor(1,1,1,timer/10-0.4)
					love.graphics.draw (cutbck3, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/240)					
					love.graphics.draw (cutbck2, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/240)
					cutscenetalkingfish()
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180) end
					--print(timer)
			elseif timer >23 and timer <33 then
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					love.graphics.draw (cutbck3, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/240)					
					love.graphics.draw (cutbck2, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/240)
				
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-250-timer*6,0,1.05+timer/240,1.05+timer/180) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-2.3)
				
					love.graphics.draw (cutbck3, 0-timer*2,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					love.graphics.draw (cutbck2, 0-timer*2,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					
					if nLevel==1 then love.graphics.draw (cutfrg, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					--print(timer)
					--print(timer/10-2.3)
					
			elseif timer >33 and timer <50 then
					love.graphics.setColor(1,1,1,timer/-10+4.3)
					love.graphics.draw (cutbck3, 0-timer*2,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					love.graphics.draw (cutbck2, 0-timer*2,0-timer*2,0,1.05+timer/240,1.05+timer/240)
					
					if nLevel==1 then love.graphics.draw (cutfrg, -timer*3,0-timer*2,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1,1,timer/10-3.3)
			
					love.graphics.draw (cutbck3, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					love.graphics.draw (cutbck2, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+3.3)
					--print(timer)
					--print(timer/-10+4.3)
	
			elseif timer >50 then
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/-10+6)
					love.graphics.draw (cutbck3, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					love.graphics.draw (cutbck2, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240)
					
					if nLevel==1 then love.graphics.draw (cutfrg, 0-timer*2,-500-timer*6,0,1.05+timer/240,1.05+timer/240) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-5)
					love.graphics.draw (cutbck3, 0-timer*2,-400-timer*6,0,1.05+timer/240,1.05+timer/240)
					love.graphics.draw (cutbck2, 0-timer*2,-400-timer*6,0,1.05+timer/240,1.05+timer/240)
				
					if nLevel==1 then love.graphics.draw (cutfrg, -0+timer*2,-400,0,1.05+(timer/240),1.05+(timer/240)) end
					
					love.graphics.setColor(1,1.1-subbeat/2,1.1-subbeat/2,timer/10-5)
					--print(timer)
					--print(timer/-10-6)
			end
end
