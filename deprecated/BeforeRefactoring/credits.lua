--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
This function defines the creditsload() function which is used to load the credits screen of a game. It sets the font for the credits text and initializes the position and speed of the credits scrolling animation. It also defines the text for the credits, with the names of the main developer and the game logic author.

The function creates two font objects, font1 and font3, with sizes of 12 and 36 respectively.

It also sets the credposition variable to 900 and credspeed variable to 32, which are used to define the starting position and scrolling speed of the credits.

Finally, it defines the text to be displayed in the credits, which includes the names of the main developer and game logic author. However, the text is incomplete and is missing the actual names.
--]]
function creditsload()
	
font1 = love.graphics.newFont(12) -- standard font
font3 = love.graphics.newFont(12*3) -- three times bigger
font6 = love.graphics.newFont(12*6) -- six times bigger
	
--lv1sketch= love.graphics.newImage("/assets/levels/level1/lv1sketch.webp")

credposition=900
credspeed = 32
credit1=[[main developer:














game logic:

















Digital art and background design:














Animation:


























Voice actors (Main characters):































































































Voice actors (English):






























































Voice actors (German):









































Voice actors (French):
















































































Voice actors (Spanish):





















































Voice actors (Dutch):
































































Voice actors (Swedish):


















































Voice actors (Polish):































Voice actors (Czech):


















Voice actors (Russian):
































Translations (Chinese)























2d Assets:




















































































































































































































































Fonts:













































Backgrounds:




































Textures








































































































shaders:























Sound effects:

















sound atmospheres










Music:
]]
voiceactors=[[

























blue fish (English)
red fish (English)

blue fish (English)
red fish (English)

blue fish (German)


blue fish (French)
red fish (French)

blue fish (Spanish)
red fish (Spanish)


red fish (Polish)


red fish (Dutch)


red fish (Czech)









Family Roundfish
Skulls
Captive
snails
chief inspector Clouseau
self destructive message
Ocean gods
Crab
Skeletor
Parrot
Talking sculptures
Talking sculptures
Vikings on a viking ship
Robodog
Letters to FDTO
Congratulations from FDTO
The linux users
Medusa



Snails
self destructive message
chief inspector Clouseau
Talking sculptures
Captive
Crab
Letters to FDTO
Congratulations from FDTO
Ocean gods





self destructive message
Talking sculptures
Vikings on a viking ship
Ocean gods
Captive
Skeletor
Crab
Snails
Letters to FDTO
Congratulations from FDTO
Medusa
Chief Inspector Clouseau















self destructive message
Talking sculptures
Letters to FDTO
Skeletor
Ocean gods













Snails
Crab
Family Roundfish
Congratulations from FDTO
self destructive message
chief inspector Clouseau 
Talking sculptures
















Vikings on a viking ship
Captive
Gods
Letters to FDTO
Talking sculptures
Snails
Crab
self destructive message









self destructive message
Snails
Captive
Gods
Skeletor
Crab
Talking sculptures




Crab
Medusa




chief inspector Clouseau
Crab
Skeletor
Captive
]]
credit0=[[
glitchapp




darkfrei





Rosie Krijgsman
Max Mint



Max Mint








Aaron Relf
ishVA

John Combs
Snoozy

lunarwingz


Labyrinthus
Vn_aiis

Bastián Villaroel
zerosky


Nelakori


Rosie Krijgsman


cat-leroy









Ross K. Foad.
Ross K. Foad.
Reyrey98
Logan MacDonald
Rhys Normington
Baerutt
Huscarl
John Combs	/ shattersoflight
Katie Otten
Katie Otten
Aaron Relf	/ Diego Reyes
SirLordSpam
Bernardo Linhares
T1M0Rtal
Pronob
AliasEnigma
dextermanning
Sirlordspam



Lyrokun
Lyrokun
Stelcher
Sengi
Marvin "Merpix" Kreckwitz
Yuuto
Zeusbeard
lixid
lunarwingz





Juju_draw
lordtyler / Avos / SecheLinge
Maxwell_Gant-de-Lin
Avos / sechelinge2000
Le-Tropeur
Le-Tropeur
Labyrinthus
hubeaugoss / Labyrinthus
ika-ori
ika-ori
Kuroinight
Labyrinthus















Camila
Diego Estrada
MrDaniRamo
MrDaniRamo
Catter













Philip Kraaijenhof
Philip Kraaijenhof
Lobna_AL
Lobna_AL
TheSparkledash
TimdeVriesVA
equi_nox
















rainbohdah
gegoxaren
gegoxaren
Bagingi
Bagingi
Bagingi
Bagingi
Bagingi









Nelakori
Maciej_A
Maciej_A
Justlumpek
Justlumpek
Sadina
Sadina




Kat-Leroy
Kat-Leroy




Dmitri Zaitsev
Dmitri Zaitsev
Dmitri Zaitsev
Dmitri Zaitsev







Jiuze “George” Liu







CigSmoker311
ctdabomb
bluecarrot
Tuomo Untinen
phaelax
glitchapp
darkfrei
TwistedDonkey
kotnaszynce
BrighamKeys
ansimuz
Rosie Krijgsman
sunburn
qubodup
tzunghaor
EdSquare
loafbrr_1
magicink
durmieu
richtaur
Atmostatic
Jana Ochse (2D!PIXX)
ubunho
asalga
STKRudy85
pistachio
staticvoidlol
Arthur
serenejeanne
MikeUSA
mold
Heathal
Cadenas
tiko479
Scaydi
alizard
zomBCool
bleutailfly
fupi
Savino
musdasch
Pyromantic
CityBuildingKit
Teh_Bucket
Crimzan
MillionthVector
doudoulolita
Tech Knight
Justin Nichol
para
cemkalyoncu
bobjh
Unseeliefae
yughues
Clint Bellanger
yd
Enetheru
benjinsmith
zonked
Jay Mayu
sandsound
devurandom
zisongbr
djonvincent
hwoarangmy
Kutejnikov
little Killy
killy one
quaternius
TyberiusGames
jazztickets
drummyfish
TiZiana
weirdybeardyman
Chasersgaming
ulf
rasteron
Frode 'Modanung' Lindeijer




Stewart C. Russell
Kris Alans
BabelStone
JREF
Tharique Azeez
Mr.Suppakit CHALERMLARP
DXKorea








donte
Scribe
lzubiaur
PumpkinGlitters
Robert Brooks
ansimuz
Ciera Elizabeth Hoover
Ironthunder
APoeticHeart
Beast
Rosie Krijgsman
Commander
Aswin909
Sindwiller
aqaraza
Keith333
qubodup
JCW
William.Thompson
Georges "TRaK" Grondin
Eric Matyas
TinyWorlds 
p0ss
rubberduck
Rahix
txturs
Wolfgang
Biohazard19842
knittel
Deco
Varkalandar
qubodup
vrav
zelta
AwesomePenguin
CleytonKauffman
zisongbr










greenbird10
frankbatista
WuTangTan
Yetman 
wazoowazoo
Lokif


jalastram
AntumDeluge
rubberduck
dklon
Little Robot Sound Factory

SONNISS GAMEAUDIOGDC
]]

credit2=[[
Thanks to all the artists who created free assets used in this game.

Thanks to all the graphic artists who created original assets for this game.

Thanks to the love2d community for all the help with development.

Thanks to all the actors involved in this game.












based on the wonderful puzzle game Fish Fillets from ALTAR interactive (www.altarinteractive.com)
and the port released on march 2004: http://fillets.sourceforge.net/

All current levels and dialogs are directly ported from Fisfh Fillets - Next Generation
Original (classic) assets by altarinteractive.com
]]
creditmusic1=[[
Ambient Loop
Underwater Ambient Pad		

Deep sea
Sinking feeling

Hypnotic Puzzle
Dreaming of Reefs
Island of mysteries
Monkey island band
Puzzle game 2
They are here

enchanted_tiki_86.mp3
Calm Ambient 1 (Synthwave 4k)
Calm Ambient 2 (Synthwave 15k)
Calm Ambient 3 (Lifewave 2k)
Sirens in the darkness
crystal cave
Mysterious ambience
Calm piano 1 (Vaporware)

Underwater_Theme_I
Underwater_Theme_II

Airy Ambience

Water Ambience

Dream Ambience
Dream Ambience II
Creepy

ambient 2 (Nautilus)


rupture
ova
vanish

Sci-Fi Ambient - Crashed Ship
Space / Scifi Ambient

upbeat refreshing commercial
epic melodic electronic dance music

A Robot's way to heaven
]]

creditmusic2=[[
isaiah658
isaiah658 

umplix


Eric Matyas






pixelsphere.org / https://cynicmusic.com/








Cleyton Kauffman


marcelofg55

SpringySpringo

TokyoGeisha



poinl


hectavex



Ted Kerr
Ted Kerr

ISAo


Viktor Kraus
]]


--[[
The code defines two button objects called "creditsReturnButton" and "creditsForwardButton". These buttons have several properties, including their text, position, rotation, scale, and font. The buttons are not initially hovered, and have a white color when not hovered and a yellow color when hovered.
--]]
creditsReturnButton = {
	text = "Return",
	x = 1000,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

creditsForwardButton = {
	text = "Forward",
	x = 1000,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end


--The "creditsupdate" function updates the position of the credits text by decreasing the "credposition" variable by a factor of "credspeed" multiplied by "dt". This function is likely called within the "love.update" function to update the position of the credits text every frame.
function creditsupdate(dt)
	credposition = credposition - (credspeed * dt)  -- x will increase by 32 for every second right is held down
end




--[[The creditsdraw(dt) function is responsible for rendering the credits screen.

It checks whether the "Return" or "Forward" button is being hovered over by the mouse, and changes the button's appearance accordingly. If the "Return" button is clicked, it stops the current music and sets the game status to "levelselection". If the "Forward" button is clicked, it increases the speed at which the credits scroll.

The function also prints out the different sections of the credits, including the main developer, game logic, voice actors, music credits, and special thanks. These sections are printed in different fonts and at different positions on the screen.
--]]
function creditsdraw(dt)
	
	if not (gamestatus=="creditsend") then 
	local hovered = isButtonHovered (creditsReturnButton)
	drawButton (creditsReturnButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  music:stop()
		  if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
		if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
		gamestatus="levelselection"
	end
	
	local hovered = isButtonHovered (creditsForwardButton)
	drawButton (creditsForwardButton, hovered)
	if hovered and love.mouse.isDown(1) then 
		  credspeed=1532
	else
	credspeed=32
	end
	end
	--credits
	love.graphics.setColor(1,1,1)
	
	love.graphics.setFont(font3) --if gamestatus=="creditsend" then love.graphics.setFont(font6) end
	love.graphics.print(credit0,300,credposition,0,1)
	love.graphics.print(voiceactors,900,credposition,0,1)

	love.graphics.print(creditmusic1,300,credposition+15700,0,1)
	love.graphics.print(creditmusic2,1000,credposition+15700,0,1)

	love.graphics.setFont(font1)	--if gamestatus=="creditsend" then love.graphics.setFont(font3) end
	love.graphics.print(credit1,300,credposition-15,0,1)
	love.graphics.print(credit2,300,credposition+18400,0,1)

end
