function DragCustomLevel()
	require ('game/states/levelEditor/leveleditor')
	gridWidth = 50
	gridHeight = 30
	require ('game/states/levelEditor/levelEditor_Load_levels') 	-- this file load custom le	vels
	require ('game/states/levelEditor/levelEditor_Play_level') 	-- this file load custom levels
	
	CustomLevelIcon =  love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/customlevel.webp")))
end

function drawDragCustomLevel()

	--if not (filename) then
		local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
		--print(desktopWidth)
		love.graphics.print("Drag and drop your custom level here: " ,(desktopWidth/3)-100,(desktopHeight/4)-100,0, 0.8, 1) 
		love.graphics.draw(CustomLevelIcon,(desktopWidth/3)-100,(desktopHeight/4),0, 0.3, 0.3) 
	--end
	
	local hovered = isButtonHovered (extrasReturnButton)
		if hideplayercontrols==false then
			drawButton (extrasReturnButton, hovered)
		end
		if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then
			love.audio.stop()
			advertising1playing=false
			advertising2playing=false
			introvideoplaying=false
			PlayingFromLevelEditor=false
		cdplaying=false
		if soundon==true then TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static") end
			if musicison==true then lovebpmload("/externalassets/music/cleytonKauffman/CleytonRX-Underwater.ogg") end
			love.timer.sleep( 0.3 )
			gamestatus="levelselection"
		end
end

function love.filedropped(file)
    local filename = file:getFilename()
    local ext = filename:match("%.%w+$")

    if not (filename == nil) then
        if ext == ".lua" then
            print("File detected")
            file:open("r")
            local fileData = file:read(file:getSize()) -- Read the entire file

            -- Load and execute the Lua code to get the level data
            local chunk, error_message = load(fileData) -- Use 'load' here
            if chunk then
                local level = chunk()
                if type(level) == "table" then
                    PlayingFromLevelEditor = true
                    music:stop()
                    lovebpmload("externalassets/music/poinl/ambient2_Nautilus.ogg")
                    music:play()
                    assignfrgbck()
                    love.graphics.clear()
                    dx, dy = 0, 0
                    steps = 0
                    pb:load(level)
                    palette = 2
                    shader1 = false
                    shader2 = false
                    nLevel = 120
                    gamestatus = "game"
                else
                    print("Error: Lua code did not return a table")
                end
            else
                print("Error loading Lua code: " .. error_message)
            end
        end
    end
end


