-- If gamestatus is "levelselection2", draw the level selection menu
function drawLevelselection2()
	shader2=false
	-- Draw 3d background with lovebpm
	lovebpmdraw()
	-- Draw level selection 2
	leveldraw2()
end
