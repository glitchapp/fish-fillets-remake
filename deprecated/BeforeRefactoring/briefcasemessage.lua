--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]


--This code defines a function briefcaseload which loads a briefcase cutscene for the game.
function briefcaseload()
		mousestate = not love.mouse.isVisible()	-- show mouse pointer
		love.mouse.setVisible(mousestate)
briefcasetext=[[Good morning, fish.




This is an affair of the gravest importance and so we have chosen you,
our ablest underwater agents.




Agents of FDTO - Fish Detective Training Organization - managed to get
hold of several amateur snapshots of an extraterrestrial object 
which has crashed somewhere in your vicinity. Your mission,
if you choose to accept it, will be to recover the UFO and acquire 
all the information you can about the principles
and nature of interstellar propulsion.




You should also try to close some of our as yet unsolved cases.
We are mostly interested in the circumstances
surrounding the sinking of the mythical city, by chance in the same area.




This case may be connected to the mystery of the Bermuda Triangle.




Find out why so many ships and planes have disappeared in this area
over the past several decades.




One of the lost ships is rumored to have belonged to
the legendary Captain Silver. There are still many uncertainties surrounding
this famous pirate. Most of all we are interested in the map which shows
the location of his buried treasure.




One of your most important tasks is to find the computer hidden 
n the deep by a certain criminal organization. It contains data about
a project which could change the entire world.




You should also find and detain the mysterious coral turtle.
It escaped from the FDTO interrogation facility.
It is not armed, but it supposedly has telepathic abilities.




We have been notified that a certain nuclear power plant
has been dumping its radioactive waste illegally. Check it out.




And don’t forget to find the holy grail.

And as usual: If anyone from your team is injured or killed,
Altar will deny any knowledge of your existence and the level
will be restarted.

This disk will self-destruct in five seconds.
]]

briefcasetextspanish=[[
Buenos días, peces. Este es un asunto de grave importancia
así que los hemos escogido a ustedes, 
nuestros agentes submarinos más hábiles.




Agentes de la FDTO - Federación de Detectives peces Tomando Observaciones
pudieron conseguir varias fotos aficionadas de un objeto extraterrestre
que cayó en algún lugar en su vecindad. Su misión, si decide aceptarla,
será recuperar el OVNI y adquirir toda la información que puedan acerca
de los principios y naturaleza de la propulsión interestelar.





También deberían de tratar de solucionar los casos aún no resueltos.
Estamos muy interesados  en las circunstancias que rodean
el hundimiento de una ciudad mítica, de casualidad en la misma área.





Este caso podría estar conectado al misterio del Triángulo de las Bermudas.
Encuentren la razón de porqué tantos barcos y aviones han desaparecido 
en esta área en varias décadas pasadas.





Se rumorea que una de las naves perdidas perteneció al legendario Capitán Silver.
Hay todavía muchas dudas que rodean a este famoso pirata.
La mayoría de nosotros estamos interesados en el mapa que muestra
la ubicación de su tesoro enterrado.





Una de sus tareas más importantes es encontrar el computador escondido
en las profundidades por cierta organización criminal.
Este contiene datos acerca de un proyecto que podría cambiar el mundo entero.





Deberías encontrar y detener también la misteriosa tortuga de coral.
Escapó desde el lugar de interrogación de la FTDO. No está armada,
pero supuestamente tiene habilidades telepáticas.





Hemos sido notificados que una cierta planta de energía nuclear
ha estado arrojando sus desechos radioactivos en forma ilegal. Revísenlo.





Y no olviden encontrar el Santo Grial.





Y lo usual: Si cualquiera de su equipo es herido o muerto, Altar negará cualquier conocimiento de su existencia y el nivel será reiniciado.
Este disco se autodestruirá en cinco segundos.
]]

briefcasetextdeutsch=[[Guten Morgen Fische. 

Dies ist eine Angelegenheit von größter Wichtigkeit.



Deshalb haben haben wir euch ausgewählt, unsrere fähigsten Unterwasser-Agenten.



Agenten der FDTO (Fisch Detektiv Training Organisation) - haben es geschafft,
einige Amateurfotos eines außerirdischen Objektes in die Hände zu bekommen,
dass irgendwo in unserer Nähe abgestürzt ist.



Eure Aufgabe wird sein, wenn ihr sie annehmt,
das UFO ausfindig zu machen und alle Informationen über die Prinzipien 
und die Natur des interstellaren Antriebs zu sammeln.



Ihr solltet auch versuchen, einige unserer ungelösten Fälle abzuschließen.



Wir sind sehr an den Umständen des Untergangs der sagenumwobenen Stadt interessiert,
die sich zufälligerweise in der gleichen Gegend befindet.



Dieser Fall könnte mit dem Geheimnis des Bermuda-Dreieckes zu tun haben.



Findet heraus, warum so viele Schiffe und Flugzeuge 
in den letzten Jahrzehnten dort verschwunden sind.



Eines der verlorenen Schiffe soll angeblich 
dem sagenhaften Kapitän Silver gehört haben.



Es gibt immer noch viele Ungewissheiten um diesen berühmten Piraten.



Am meisten sind wir an der Karte interessiert,
die den Ort seines vergrabenen Schatzes zeigt.



Eine eurer wichtigsten Aufgaben ist es, den Rechner zu finden,
den eine bestimmte kriminelle Organisation in der Tiefe versteckt hat.
Er enthält Daten über ein Projekt, das die ganze Welt verändern könnte.






Ihr solltet auch die geheimnisvolle Korallenschildkröte finden und verhaften.
Sie ist aus der Verhöhrstation  der FDTO entwischt. Sie ist nicht bewaffnet,
hat aber angeblich telepatische Fähigkeiten.



Wir wurden informiert, dass ein bestimmtes Atomkraftwerk 
seinen radioaktiven Abfall illegal entsorgt hat. Überprüft das.



Und vergesst nicht, den heiligen Gral zu finden.

Und wie üblich: Wenn einer aus eurer Mannschaft verletzt wird oder stirbt,
wird Altar jegliches Wissen über euch abstreiten und die Ebene wird neugestartet.

Diese Nachricht wird sich in 5 Sekunden selbst zerstören.
]]

briefcasetextfrench=[[Bonjour, Poisson. 

C'est une affaire de la plus haute importance et nous vous avons choisis,
vous, nos meilleurs agents sous-marins.




Des agents de la FDTO - Formation Détective Tout pOisson -
ont récupéré plusieurs photos amateurs d'un objet extraterrestre
qui s'est abîmé en mer quelque part non loin de vous.




Votre mission, si vous l'acceptez, est de retrouver l'ovni et de récupérer
toutes les informations que vous trouverez sur les principes et
la nature de la propulsion interstellaire.





Vous devrez aussi résoudre quelques uns de nos dossiers non-classés.
Nous sommes vraiment curieux de connaître dans
quelles circonstances étonnantes la ville mythique a été engloutie,
par chance au même endroit.




Ce dossier est peut être lié au mystère du triangle des Bermudes.



Découvrez pourquoi tant de bateaux et d'avions ont disparu là, depuis des décennies.



La rumeur affirme qu'un des navires disparus a appartenu au
légendaire capitaine Silver. Il reste beaucoup d'incertitudes à propos de
ce célèbre pirate. Nous sommes particulièrement intéressés par la carte qui indique
l'emplacement de son trésor.



Une de vos principales missions est de trouver l'ordinateur caché dans 
les profondeurs par une certaine organisation criminelle. 
Il contient des données qui peuvent changer le monde entier.



Vous devrez de même dénicher et arrêter la mystérieuse tortue corallienne.
Elle s'est échappée de la chambre d'interrogation de la FDTO. 
Elle n'est pas armée, mais il est possible qu'elle possède des pouvoirs télépathiques.



Nous avons été informés qu'une centrale nucléaire rejette illégalement
ses déchets radioactifs. Enquêtez.



Et n'oubliez pas de ramener LE Graal.

Et comme d'habitude : si l'un d'entre vous est blessé ou tuez,

Altar niera toute connaissance de votre existence et le niveau devra être rejoué.

Ce message s'autodétruira dans 5 secondes.
]]

briefcasetextpolish=[[
Dzień dobry, ryby. To jest sprawa wielkiej wagi, więc wybraliśmy was,
naszych najzdolniejszych podwodnych agentów.






Informatorom FDTO - Faktycznie Działającej Tajnej Organizacji - 
udało się przechwycić amatorskie zdjęcia obiektu pozaziemskiego,
który rozbił się w waszej okolicy. Waszym zadaniem, jeśli się go podejmiecie,
będzie odnalezienie UFO i pozyskanie wszelkich możliwych informacji
o zasadach funkcjonowania i konstrukcji napędu międzygwiezdnego.






Przy okazji możecie spróbować rozwiązać kilka z naszych starych spraw.
Jesteśmy żywo zainteresowani okolicznościami zatonięcia mitycznego miasta,
zbiegiem okoliczności w tej samej okolicy."






Sprawa ta może być związana z tajemnica Trójkąta Bermudzkiego.
Dowiedzcie się, dlaczego tak wiele statków
i samolotów przez wieki zniknęło na tym obszarze.






Wieść gminna niesie, że jeden z zaginionych
statków należał do legendarnego kapitana Silvera.
Postać tego pirata wciąż owiana jest mgiełką tajemnicy.
Najbardziej interesuje nas mapa miejsca zakopania jego skarbu."






Jednym z najważniejszych zadań stojących przed wami jest odnalezienie komputera,
ukrytego w głębinach przez pewną organizację terrorystyczną.
Zawiera on dane o projekcie, który może zmienić oblicze Ziemi.






Powinniście również odnaleźć i pojmać tjemniczego żółwia koralowego,
który zbiegł z pokoju przesłuchań FDTO.
Nie jest uzbrojony, ale przypuszczalnie ma zdolności telepatyczne.






Doniesiono nam, że pewna elektrownia atomowa składuje odpady radioaktywne na dnie oceanu.
Sprawdźcie to.






I nie zapomnijcie odszukać Świętego Graala.

I jak zawsze: jeśli ktoś z waszej drużyny zginie lub zostanie ranny,
Altar wyprze się jakichkolwiek z wami powiązań i poziom zostanie zrestartowany."

Ten dysk ulegnie samozniszczeniu w ciągu pięciu sekund.
]]
briefcasetextdutch=[[
Goede morgen, vissen.
Dit is een zeer belangrijke zaak en daarom benaderen we jullie,
onze beste onderwateragenten.

Agenten van de VDTO - Vissen Detective Training Organisatie
- zijn erin geslaagd om een aantal amateuropnamen
te bemachtigen van een buitenaards object
dat ergens bij jullie in de buurt is neergestort.

Jullie missie, als jullie die accepteren,
is om de vliegende schotel te bemachtigen en zoveel mogelijk informatie
te vinden over de interstellaire aandrijving.

Het zou fijn zijn als jullie ook even kunnen kijken naar wat andere onopgeloste zaken.

We zijn vooral geïnteresseerd in de omstandigheden
omtrend het zinken van de legendarische stad, toevallig ook daar in de buurt.
Deze zaak houdt wellicht verband met het mysterie van de Bermuda Driehoek.
Achterhaal waarom er de afgelopen tientallen jaren zo veel schepen
en vliegtuigen zijn verdwenen in deze omgeving.

Een van de verloren schepen zou het schip van de legendarische kapitein Silver zijn geweest.
Er is nog steeds een hoop onduidelijkheid omtrend deze beruchte piraat.
We zijn vooral geïnteresseerd in de kaart waar de verstopplaats van zijn schat op is aangegeven.

Een van jullie belangrijkste opdrachten is het vinden van de computer
die door een bepaalde criminele organisatie in de diepte is verstopt.
Er staat informatie op over een project dat de hele wereld zou kunnen veranderen.

Jullie moeten ook de mysterieuze koraalschildpad zien te vinden en arresteren.
Die is aan de VDTO ondervragingsafdeling ontsnapt.
Hij is niet bewapend, maar is waarschijnlijk wel telepatisch.

We hebben bericht gekregen dat een bepaalde kernreactor
illegaal radioactief afval heeft gedumpt. Zoek het uit.
En vergeet niet de Heilige Graal te vinden, als je toch bezig bent.
En, zoals altijd, als een van jullie team gewond raakt of omkomt,

dan zal Altar ontkennen iets met jullie te maken te hebben en het veld zal worden herstart.

Dit bericht zal zichzelf over vijf seconden vernietigen.
]]

briefcasetextswedish=[[
God morgon, fiskar. Det här är en sak av största vikt och vi har valt er,
våra bästa undervattensagenter.,






Agenter hos FDTO - FiskDetektivTräningsOrganisationen - 
har fått tag på flera amatörfoton av oidentifierade utomjordiska objekt
som har kraschat i närheten av er. Ert uppdrag, om ni väljer att acceptera det,
är att hitta UFOt och få tag i all information
ni kan om principen och ursprunget till intergalaktisk drivkraft.,






Ni borde också försöka att lösa några av våra olösta fall.
Vi är i första hand intresserade av händelseförloppet när den mystiska staden sjönk,
av en händelse också i samma område.,
Det kan hänga ihop med mysterierna i Bermudatriangeln.






Ta reda på varför så många skepp och flygplan har försvunnit
där de senaste decennierna.,
Enligt rykten så ska ett av de försvunna skeppen ha tillhört kapten Silver.
Det finns fortfarande många mysterier kring den ökände piraten.






Framför allt är vi intresserade av kartan som visar var hans skatter är nergrävda.,
En av våra viktigaste uppgifter är att hitta datorn som är gömd i djupet av
en kriminell organisation. 






Den innehåller data om projekt som skulle kunna förändra hela världen.,
Ni borde också hitta och ta fast den mystiska korallsköldpaddan.






Den rymde från FDTOs förhörskomplex. 
Den är inte beväpnad men den tros ha telepatiska förmågor.,
Vi har blivit underrättade om ett visst kärnkraftverk
som har dumpat sitt radioaktiva avfall illegalt. Kontrollera det.,
Och glöm inte att leta efter den heliga gralen.,






Och som vanligt: Om någon i ert team skulle bli skadade eller dödade,
kommer Altar att förneka all kännedom
om er existens och nivån kommer att startas om.,
Den här disken kommer att utplåna sig själv om fem sekunder.,
]]

-- Initially, shader2 is set to false, briefcaseclosed is set to false and briefcasevideoplaying is set to false.
	shader2=false
	briefcaseclosed=false
	briefcasevideoplaying=false
	
	--The function loads a music file (briefcasemusic.ogg) to be played during the cutscene using love.audio.newSource and stops any currently playing music if musicison is true.
	   briefcasemusic = love.audio.newSource( "externalassets/dialogs/level2/briefcasemusic.ogg","stream" )
		if musicison==true then love.audio.stop() music:stop() love.audio.play( briefcasemusic )
		end
		
		--The function also sets the dialogue text to be displayed during the cutscene based on the chosen language (language) using if-else statements. The text is stored in variables like briefcasetext, briefcasetextspanish, briefcasetextdeutsch, etc., depending on the language.
			if language=="en" and not (language2=="pl") and not (language2=="nl") then
					briefcasetext=briefcasetext
					
		elseif language=="es" then 	briefcasetext=briefcasetextspanish
		elseif language=="de" then  briefcasetext=briefcasetextdeutsch
		elseif language=="fr" then 	briefcasetext=briefcasetextfrench
		elseif language2=="pl" then 	briefcasetext=briefcasetextpolish
		elseif language2=="nl" then 	briefcasetext=briefcasetextdutch
		elseif language=="sv" then 	briefcasetext=briefcasetextswedish
		end
		
			--The function then creates a new video using love.graphics.newVideo with the file path externalassets/video/briefcase.ogg and plays it. It sets briefcasevideoplaying to true and calls love.timer.sleep(0.1) to pause the cutscene briefly.
				briefcasevideo = love.graphics.newVideo( "externalassets/video/briefcase.ogg" )
				briefcasevideo:play()							
				briefcasevideoplaying=true
				love.timer.sleep( 0.1 )
		
				--Finally, the function defines two new fonts, font1 and font3, and sets briefcaseposition and briefcasespeed to 900 and 32 respectively.		
				--font1 = love.graphics.newFont(12) -- standard font
				--font3 = love.graphics.newFont(12*3) -- three times bigger
	
				briefcaseposition=900
				briefcasespeed = 32


briefcaseReturnButton = {
	text = "Return",
	x = 1500,
	y = 200, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}

briefcaseForwardButton = {
	text = "Forward",
	x = 1500,
	y = 300, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = font3,
}


end


