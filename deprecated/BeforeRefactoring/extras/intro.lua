-- If gamestatus is "intro", draw the intro scene
function drawIntro()
-- If the intro video is playing, draw it with the correct aspect ratio
		if introvideoplaying==true then
		if res=="1080p" then love.graphics.draw(introvideo, 0, 0,0,1.33,1)
	elseif res=="1440p" then love.graphics.draw(introvideo, 0, 0,0,1.78,1.33)
	end
	-- If the timer is between 0 and 0.5 seconds and no mouse key has been pressed, prompt to start with a message
	if timer2>0 and timer2<0.5 and inputtime==0 then love.graphics.print("Press any key to start",600,800,0, 0.8, 1) end
	-- Draw FPS metrics
	drawfpsmetrics()
	end
	-- Draw the intro scene player interface
	drawintrosceneplayer()

	
	-- Check if CD is playing and draw equalizer
	if cdplaying then
		equalizerdraw(dt)
	end
end

