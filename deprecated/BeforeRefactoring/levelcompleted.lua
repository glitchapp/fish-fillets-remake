


function updateLevelCompleted(dt)

	if  PlayingFromLevelEditor==true then
	--confetti effect update
     for _, confetto in ipairs(confettiParticles) do
        -- Simulate confetti movement (falling)
        confetto.y = confetto.y + 50 * dt

        -- Reset confetti when it goes off-screen
        if confetto.y > love.graphics.getHeight() then
            confetto.y = -confetto.size
            confetto.x = love.math.random(0, love.graphics.getWidth())
        end

        -- Update confetti color based on hue
        confetto.hue = confetto.hue + 0.001 * dt  -- Adjust the rate of color change
        if confetto.hue > 1 then
            confetto.hue = 0
        end

        -- Convert hue to RGB color
        local r, g, b = hsvToRgb(confetto.hue, 1, 1)
        confetto.color = {r, g, b}
    end
    timer=timer+dt
    end
end

function drawLevelcompleted()
	drawlevelcompleted()
	-- Draw FPS metrics
	drawfpsmetrics()
	
	if  PlayingFromLevelEditor==true then
		--draw confetti
		for _, confetto in ipairs(confettiParticles) do
			-- Draw confetti particles as polygons
			love.graphics.setColor(confetto.color)
			love.graphics.rectangle("fill", confetto.x, confetto.y, confetto.size, confetto.size)
		end
   end
end

-- Function to convert HSV color to RGB
function hsvToRgb(h, s, v)
    local r, g, b

    local i = math.floor(h * 6)
    local f = h * 6 - i
    local p = v * (1 - s)
    local q = v * (1 - f * s)
    local t = v * (1 - (1 - f) * s)

    if i % 6 == 0 then
        r, g, b = v, t, p
    elseif i % 6 == 1 then
        r, g, b = q, v, p
    elseif i % 6 == 2 then
        r, g, b = p, v, t
    elseif i % 6 == 3 then
        r, g, b = p, q, v
    elseif i % 6 == 4 then
        r, g, b = t, p, v
    else
        r, g, b = v, p, q
    end

    return r, g, b
end
