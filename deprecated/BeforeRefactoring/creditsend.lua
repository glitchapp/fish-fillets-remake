-- This is a copy the credits update code above mixed with benchmark / cutscene animations that could be used if the player beats the game.
function updateCreditsEnd(dt)
        
   creditsupdate(dt)
        creditsendcutsceneupdate(dt)			-- This is new in this version of the credits and will update animations behind the credits text
        updatefpsmetrics(dt)			
	updateButtonFocus(dt)	--sparkles animations
	
        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end
end
