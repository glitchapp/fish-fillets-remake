-- If gamestatus is "gameover", draw the game over screen
function drawGameover()
	drawgameover()
	-- Draw FPS metrics
	drawfpsmetrics()
end
