  -- If the game status is "music", update music, equalizer and FPS metrics
  --If the game is in the benchmark screen or the music screen, the benchmark or music is updated and the FPS is limited.
  function updateMusic(dt)
    updategetjoystickaxis()
        updateRadialMenu()
        musicupdate(dt)		
        --updateButtonFocus(dt)	--sparkles animations
        if cdplaying == true and gamestatus=="music" then 
            equalizerupdate(dt) 
        end
        updatefpsmetrics(dt)			

        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end
  end
  
  
  
  -- If gamestatus is "music", draw the music menu
function drawMusic()
	-- Draw the music menu with delta time
	--if timer>20 then drawcscene()
	--else drawrotatingcd(dt)
	--end
	musicdrawcd(dt)
	musicdraw(dt)
	musiccreditsdraw(dt)
	-- Draw FPS metrics
	drawfpsmetrics()
	-- If the CD is playing, draw the equalizer
	if cdplaying==true then
		equalizerdraw(dt)
	end
end
