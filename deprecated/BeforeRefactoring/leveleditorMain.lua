function loveLoadLevelEditor()
	require ('game/states/levelEditor/leveleditor')
	loadLevelEditor()
end

function loveUpdateLevelEditor(dt)
	updateLevelEditor(dt)
end

function loveDrawLevelEditor(dt)
	drawLevelEditor(dt)
end
