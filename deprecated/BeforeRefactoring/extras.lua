 --[[
This part of the code is responsible for handling the "extras" game status, which involves updating various components. The code first calls the extrasupdate() function and the updatefpsmetrics() function to update any extra elements in the game and keep track of the game's FPS, respectively.

The code then applies a framerate limit depending on the value of the limitframerate variable. If the value is 30, the code checks if the time since the last frame (dt) is less than 1/15 (i.e., the target frame rate is 30 FPS), and if so, it sleeps for the difference between 1/17 and dt to ensure that the frame rate is not exceeded. If the value is 60, the code checks if dt is less than 1/60 (i.e., the target frame rate is 60 FPS), and if so, it sleeps for the difference between 1/40 and dt.

Finally, if either advertising1playing or advertising2playing is true, the code updates the Talkies component and displays subtitles for the corresponding advertising video.

--]]
	
function updateExtras(dt)

  updategetjoystickaxis()
        extrasupdate(dt)		
        updatefpsmetrics(dt)			
		updateButtonFocus(dt)	--sparkles animations

        -- Apply framerate limit
        if limitframerate == 30 then 
            if dt < 1/15 then 
                love.timer.sleep(1/17 - dt) 
            end
        elseif limitframerate == 60 then 
            if dt < 1/60 then 
                love.timer.sleep(1/40 - dt) 
            end
        end

        -- If advertising1 is playing, update Talkies and display subtitles
        if advertising1playing == true then
            Talkies.update(dt)
            subtitle21advertising1()
        -- If advertising2 is playing, update Talkies and display subtitles
        elseif advertising2playing == true then
            Talkies.update(dt)
            subtitle21advertising2()
        end


end


--[[This section of code checks the current game status and calls the corresponding function to draw the game interface.
 If the game status is "extras", the extras menu is drawn and Talkies dialog for advertising 1 or 2, if they are playing.
  Then, FPS metrics are drawn. If the game status is "intro", the intro scene is drawn,
   the prompt message to start the game is displayed if the timer is between 0 and 0.5 seconds and no mouse key has been pressed.
   The intro scene player interface is also drawn, and FPS metrics are displayed. 
   If the CD is playing, the equalizer is drawn.
   
   --]]
function drawExtras()

	-- Draw the extras menu with delta time
	extrasdraw(dt)
	-- If advertising 1 is playing, draw its Talkies dialog
		if advertising1playing==true then Talkies.draw(dt)
	-- If advertising 2 is playing, draw its Talkies dialog
	elseif advertising2playing==true then Talkies.draw(dt)
	end
	-- Draw FPS metrics
	drawfpsmetrics()


end
