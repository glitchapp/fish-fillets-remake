--[[
This code represents the logic for the level selection status in a game. The code starts by updating the FPS metrics and checking the frame rate limit. Then, if enabled, it checks the battery status of the device.

Next, it updates the game elements, including the BPM and input time, which tracks how long it has been since the user selected a level. If the game is still loading, it updates the loading process and fades the screen. Once the loading is completed, the code loads the game, changes the game status to "game", and resets some variables.

Lastly, the code checks if the user has not selected a level for a long time, in which case it initializes some values, loads the level assets, assigns the foreground and background, and sets the game status to "firststart" to bring the user back to the start screen. Overall, this code helps to manage the game flow and ensure that the game is loaded properly.
--]]

function updateLevelSelection(dt)

inputtime = inputtime + dt
	timer2 = timer2 + dt
	timer3 = timer3 + dt
	
	updateButtonFocus(dt)
	updategetjoystickaxis()	--get gamepad axis positions
	updateRadialMenu()
	updatefpsmetrics(dt)			-- update FPS metrics
	
	-- Framerate limit
	if limitframerate == 30 then 
		if dt < 1/15 then 
			love.timer.sleep(1/17 - dt) 
		end
	elseif limitframerate == 60 then 
		if dt < 1/30 then 
			love.timer.sleep(1/32 - dt) 
		end
	end
	
	-- Check battery status if enabled
	if showbattery then 
		powerstate, powerpercent, powerseconds = love.system.getPowerInfo( )	
		if powerpercent == nil then 
			powerpercent = "∞" 
			powerseconds = "∞" 
		end
	end
	

	
	--[[
	if showtimedate then	-- if time and date is enabled from hub menu get the needed data
		systemdate = os.date("*t")
		systemsec = getTime.sec
		systemmin = getTime.min
		systemhours = getTime.hour
	end--]]
	
	
	-- Update game elements
	lovebpmupdate(dt)
	inputtime = inputtime + dt
	timer = timer + dt
	timer2 = timer2 + dt
	timer3 = timer3 + dt
	
				-- Check if game is still loading
	if isloading == true then 
		loadingupdate(dt) 
		fadeloadingprocess(dt) 
	end	
	
		-- Load the game once the loading is completed
	if loadingprocess == true then
		changelevel()
		gamestatus="game"
		timer3=0
		stepdone=0
		loadingprocess=false
		--love.audio.play( sharpechosound )
	end
		
-- Check if user has not selected a level for a long time
	if inputtime > 60 then
		initializeffishvalues()
		autoload=false 
		nLevel=101
		loadlevelassets()
		assignfrgbck()
		timer=0 
		timer2=0
		stepdone=0
		inputtime=0
		gamestatus="firststart"
		--music:stop()
	end
end



-- If gamestatus is "levelselection", draw the level selection menu
function drawLevelselection()
	shader2=false
	-- Draw 3d background with lovebpm
	lovebpmdraw()
	-- Draw level selection
	leveldraw()
	-- If the game is loading, draw the loading icon
	if isloading==true then loadingdraw(dt) end
	-- Draw FPS metrics
	drawfpsmetrics()
	-- Draw battery info if enabled
end
