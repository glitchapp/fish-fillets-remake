
function updateEnds(dt)

-- If the gamestatus is "endmenu", update timer and endmenu
if gamestatus=="endmenu" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	endmenuupdate(dt)

-- If the gamestatus is "fishhouseend", update timer and fishhouseend
elseif gamestatus=="fishhouseend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	fishhouseendupdate(dt)

elseif gamestatus=="shipwrecksend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	shipwrecksendupdate(dt)
	subtitle19end()
	
	
-- If the gamestatus is "silversshipend", update timer and silversshipend
elseif gamestatus=="silversshipend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	silversshipendupdate(dt)
	subtitle51end()

-- If the gamestatus is "cityinthedeepend", update timer and cityinthedeepend
elseif gamestatus=="cityinthedeepend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	cityinthedeependupdate(dt)
	subtitle29end()

-- If the gamestatus is "ufoend", update timer, ufoend, subtitle58end, and BPM (if player is playing)
elseif gamestatus=="ufoend" then
	if playerplaying==true then
		timer = timer + dt
		updategetjoystickaxis()	--get gamepad axis positions
		ufoendupdate(dt)
		subtitle58end()
		lovebpmupdate(dt)
	end

-- If the gamestatus is "coralreefend", update timer and coralreefend
elseif gamestatus=="coralreefend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	coralreefendupdate(dt)
	subtitle37end()

-- If the gamestatus is "treasurecaveend", update timer and subtitle64end
elseif gamestatus=="treasurecaveend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	subtitle64end()

-- If the gamestatus is "barrelend", update timer and barrelend
elseif gamestatus=="barrelend" then
	timer = timer + dt
	updategetjoystickaxis()	--get gamepad axis positions
	barrelendupdate(dt)
  	subtitle44end()

-- Level completion status
elseif gamestatus=="secretcomputerend" then
		timer = timer + dt	-- timer update
		updategetjoystickaxis()	--get gamepad axis positions
  		subtitle70end()  		

-- Linux end status
	elseif gamestatus=="linuxend" then
		timer = timer + dt	-- timer update
		updategetjoystickaxis()	--get gamepad axis positions
  		subtitle77end()
  		
-- Game end status
  		elseif gamestatus=="gameend" then
		timer = timer + dt	-- timer update
		updategetjoystickaxis()	--get gamepad axis positions
  		subtitle79end()
  	end
end
