chair1obj={
{
{'a',' ',' ',' '},
{'a',' ',' ',' '},
{'a','a','a','a'},
{'a',' ',' ','a'},
{'a',' ',' ','a'},
},
}

tableobj={
{
{'d','d','d','d','d','d','d',},
{' ','d',' ',' ',' ','d',' ',},
{' ','d',' ',' ',' ','d',' ',},
{' ','d',' ',' ',' ','d',' ',},
},
}

chair2obj={
{
{' ',' ',' ','b'},
{' ',' ',' ','b'},
{'b','b','b','b'},
{'b',' ',' ','b'},
{'b',' ',' ','b'},
},
}

cushionobj={
{
{'c','c','c'},
},
}

steelbarobj={
{
{'A'},
{'A'},
{'A'},
{'A'},
{'A'},
{'A'},
{'A'},
{'A'},
},
}

  function loadobjects(obj1,obj2,obj3,obj4,obj5)
        o1 = {}
        for y, row in ipairs(obj1[1]) do
            o1[y] = {}
            for x, cell in ipairs(row) do
                o1[y][x] = cell
            end
        end
        
        o2 = {}
        for y, row in ipairs(obj2[1]) do
            o2[y] = {}
            for x, cell in ipairs(row) do
                o2[y][x] = cell
            end
        end
        
        o3 = {}
        for y, row in ipairs(obj3[1]) do
            o3[y] = {}
            for x, cell in ipairs(row) do
                o3[y][x] = cell
            end
        end
             
        o4 = {}
        for y, row in ipairs(obj4[1]) do
            o4[y] = {}
            for x, cell in ipairs(row) do
                o4[y][x] = cell
            end
        end
        
        o5 = {}
        for y, row in ipairs(obj5[1]) do
            o5[y] = {}
            for x, cell in ipairs(row) do
                o5[y][x] = cell
            end
        end
        
        
    end

function draw_chair1()
    for y, row in ipairs(o1) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                    [chair1] = {0.5, 0.5, 0},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + 6) * cellSize,
                    (y + 16) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    o1[y][x],
                    (x + 6) * cellSize,
                    (y + 16) * cellSize
                )
            end
        end
    end
end
function draw_table()
    for y, row in ipairs(o2) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
					[table] = {0.62, 0.47, 1},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + 11) * cellSize,
                    (y + 17) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    o2[y][x],
                    (x + 11) * cellSize,
                    (y + 17) * cellSize
                )
            end
        end
    end
end

function draw_chair2()
    for y, row in ipairs(o3) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                     [chair2] = {1, 0.79, 0.49},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + 19) * cellSize,
                    (y + 16) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    o3[y][x],
                    (x + 19) * cellSize,
                    (y + 16) * cellSize
                )
            end
        end
    end
end

function draw_cushion()
    for y, row in ipairs(o4) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                      [cushion] = {0.61, 0.9, 1},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + 7) * cellSize,
                    (y + 17) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    o4[y][x],
                    (x + 7) * cellSize,
                    (y + 17) * cellSize
                )
            end
        end
    end
end

function draw_steelbar()
    for y, row in ipairs(o5) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                      [steelbar] = {0.59, 1, 0.5},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + 14) * cellSize,
                    (y + 9) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    o5[y][x],
                    (x + 14) * cellSize,
                    (y + 9) * cellSize
                )
            end
        end
    end
end
