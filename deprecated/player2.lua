
fish2obj={
{
{'2','2','0'},
},
}

fish2objright={
{
{'0','2','2'},
},
}




--function loadplayers(pl1,pl2,pl1right,pl2right)
function loadplayer2(pl2,pl2right)

        p2 = {}
        for y, row in ipairs(pl2[1]) do
            p2[y] = {}
            for x, cell in ipairs(row) do
                p2[y][x] = cell
            end
        end
        p2r = {}
        for y, row in ipairs(pl2right[1]) do
            p2r[y] = {}
            for x, cell in ipairs(row) do
                p2r[y][x] = cell
            end
        end
        
    end

function draw_fish2()
    for y, row in ipairs(p2) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
					
                    [fish2] = {0, 0, 1},
                    [fish2body] = {0, 0, 1},
                }
                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + f2x) * cellSize,
                    (y + f2y) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    p2[y][x],
                    (x + f2x) * cellSize,
                    (y + f2y) * cellSize
                )
            end
        end
    end
end

function draw_fish2right()
    for y, row in ipairs(p2r) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
					
                    [fish2] = {0, 0, 1},
                    [fish2body] = {0, 0, 1},
                }
                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x + f2x) * cellSize,
                    (y + f2y) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    p2r[y][x],
                    (x + f2x) * cellSize,
                    (y + f2y) * cellSize
                )
            end
        end
    end
end
