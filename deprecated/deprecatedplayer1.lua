

fish1obj={
{
{'@','1','1'},
{'1','1','1'},
},
}

fish1objright={
{
{'1','1','@'},
{'1','1','1'},
},
}

function loadplayer1(pl1,pl1right)
p1 = {}
        for y, row in ipairs(pl1[1]) do
            p1[y] = {}
            for x, cell in ipairs(row) do
                p1[y][x] = cell
            end
        end
        
p1r = {}
        for y, row in ipairs(pl1right[1]) do
            p1r[y] = {}
            for x, cell in ipairs(row) do
                p1r[y][x] = cell
            end
        end
end

function draw_fish1()
    for y, row in ipairs(p1) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                    [fish1] = {0.64, 0.53, 1},
                    [fish1body] = {0.64, 0.53, 1},
                }

                love.graphics.setColor(colors[cell])
                love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle('fill',(x +f1x) * cellSize,(y + f1y) * cellSize, cellSize,cellSize)
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(p1[y][x],(x + f1x) * cellSize,(y + f1y) * cellSize)
            end
        end
    end
end

function draw_fish1right()
    for y, row in ipairs(p1r) do
        for x, cell in ipairs(row) do
            if cell ~= empty then
                local cellSize = 23
                
                 local colors = {
                    [fish1] = {0.64, 0.53, 1},
                    [fish1body] = {0.64, 0.53, 1},
                }

                love.graphics.setColor(colors[cell])
                --love.graphics.setColor(0.5,0.5,0)
                love.graphics.rectangle(
                    'fill',
                    (x +f1x) * cellSize,
                    (y + f1y) * cellSize,
                    cellSize,
                    cellSize
                )
                love.graphics.setColor(1, 1, 1)
                love.graphics.print(
                    p1r[y][x],
                    (x + f1x) * cellSize,
                    (y + f1y) * cellSize
                )
            end
        end
    end
end
