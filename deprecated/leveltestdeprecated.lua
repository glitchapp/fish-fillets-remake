
function levelload()

	thumb=thumb1

	font1 = love.graphics.newFont(12) -- standard font
	font3 = love.graphics.newFont(12*3) -- three times bigger

	--levelmap= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/levelselection.webp")))
	sphere= love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/levels/sphere.webp")))

		if language=="es" then exittext="Finalizar" musictext="Musica" optionstext="Opciones" creditstext="Creditos" leditortext="Level editor" extrastext="Extras"
	elseif language=="de" then exittext="Beenden" musictext="Musik" optionstext="Einstellungen" creditstext="Credits" leditortext="Level editor" extrastext="Extras"
	elseif language=="en" then exittext="Exit"  musictext="Music" optionstext="Options" creditstext="Credits" leditortext="Level editor" extrastext="Extras" endtext="Ends" loadtext="Load" savetext="Save"
	elseif language=="jp" then exittext="出口" musictext="音楽プレーヤー" optionstext="オプション" creditstext="修得" leditortext="レベルエディタ" extrastext="エキストラ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/MT_Tare/MT_TARE.ttf", 64)
	elseif language=="chi" then exittext="退出" musictext="音乐播放器" optionstext="选项" creditstext="信用额度" leditortext="关卡编辑器" extrastext="额外" poorfishmiddle = love.graphics.newFont("externalassets/fonts/BabelStoneHan/BabelStoneHan.ttf", 64)
	elseif language=="thai" then exittext="ทางออก" musictext="เครื่องเล่นเพลง" optionstext="ตัวเลือก" creditstext="เครดิต" leditortext="แก้ไขระดับ" extrastext="ความพิเศษ" poorfishmiddle = love.graphics.newFont("externalassets/fonts/thsarabun-new/THSarabunNew001.ttf", 64)
	elseif language=="tamil" then exittext="வெளியேற" musictext="இசைப்பான்" optionstext="ตัวเลือก" creditstext="வரவுகள்" leditortext="நிலை ஆசிரியர்" extrastext="கூடுதல்" poorfishmiddle = love.graphics.newFont("externalassets/fonts/neythal/neythal-regular.ttf", 64)
	elseif language=="hindi" then exittext="बाहर निकलन" musictext="संगीत बजाने वाला" optionstext="विकल्प" creditstext="क्रेडिट" leditortext="स्तर संपादक" extrastext="एक्स्ट्रा कलाकार" poorfishmiddle = love.graphics.newFont("externalassets/fonts/marathi/Marathi_Tirkas.ttf", 34)
	end

confirmsavetext="yes"
rejectsavetext="no"
cutscenest="cutscenes"
resetsavegamet="Reset"
--buttons

	optionsButton = {
		text = optionstext,
		--x = 1500, y = 10, 
		x = 1100, y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	musicButton = {
		text = musictext,
		--x = 1500, y = 100, 
		x = 100, y = 10, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	extrasButton = {
	text = extrastext,
	--x = 1500, y = 200, 
	x = 1500, y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}

	endButton = {
	text = endtext,
	--x = 1500, y = 300,
	x = 1500, y = 100,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}
	
	
	creditsButton = {
		text = creditstext,
		--x = 1500, y = 400, 
		x = 100, y = 700, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	loadButton = {
		text = loadtext,
		x = 1500,
		y = 200, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	saveButton = {
		text = savetext,
		x = 1500,
		y = 300, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	resetsavegameButton = {
		text = resetsavegamet,
		x = 1500,
		y = 400, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	confirmsaveButton = {
		text = "yes",
		x = 500,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}
	
	rejectsaveButton = {
		text = "no",
		x = 800,
		y = 800, 
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	exitButton = {
		text = exittext,
		--x = 1500, y = 700,
		x = 1200, y = 10,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}

	cutscenesButton = {
		text = cutscenest,
		x = 1500,
		y = 500,
		r = 0,
		sx = 1,
		hovered = false,
		color = {1,1,1},
		hoveredColor = {1,1,0},
		font = poorfishmiddle,
	}





	leditorButton = {
	text = leditortext,
	x = 1500,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
	}


	require("assets/text/levelmenu/level-en")
end



--buttons

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
	and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end

local function drawButton (button, hovered,text)

	love.graphics.setFont( button.font )

	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(text,button.x,button.y,button.r,button.sx)
end


local function drawTile (x, y, tileSize)
	local color = {0.2, 0.2, 0.2}
	if (x+y)%2 == 0 then
		color = {0.3, 0.3, 0.3}
	end
	love.graphics.setColor (color)
	love.graphics.rectangle ('fill', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
end

local function drawCenteredText (rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, 
		rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, 
		math.floor(textWidth/2)+0.5, 
		math.floor(textHeight/2)+0.5)
end


local function drawCenteredSprite (x, y, tileSize, sprite)
--	x and y it tiles, [1,1] is top left
	local w, h = sprite:getDimensions()
	local scale = tileSize/math.max (w, h)
	love.graphics.draw (sprite, (x-0.5)*tileSize, (y-0.5)*tileSize, 0, scale, scale, w/2, h/2)
end


spheres = {
-- 0. Fish house (8 levels)
	{level= 1, x=13.9, y=5.6},
	{level= 2, x=14.2, y=6.9},
	{level= 3, x=13.8, y=8.4},
	{level= 4, x=13, y=9.9},
	{level= 5, x=12.4, y=11},
	{level= 6, x=12.2, y=12.4},
	{level= 7, x=12.7, y=13.6},
	{level= 8, x=13.6, y=14.6},
--[[
-- 1. Ship Wrecks (9 - 19) (11 levels)
	{level= 9, x=14, y=5},
	{level=10, x=15, y=5},
	{level=11, x=16, y=5},
	{level=12, x=17, y=5},
	{level=13, x=18, y=5},
	{level=14, x=19, y=5},
	{level=15, x=20, y=5},
	{level=16, x=21, y=5},
	{level=17, x=22, y=5},
	{level=18, x=23, y=5},
	{level=19, x=24, y=5},

-- 3. City in the deep (20-29)	 10 levels
	{level=20, x=12, y=6},
	{level=21, x=11, y=6},
	{level=22, x=10, y=6},
	{level=23, x= 9, y=6},
	{level=24, x= 8, y=6},
	{level=25, x= 7, y=6},
	{level=26, x= 6, y=6},
	{level=27, x= 5, y=6},
	{level=28, x= 4, y=6},
	{level=29, x= 3, y=6},

--5 Coral Reef	(30 - 37)		7 levels
	{level=30, x=14, y=8},
	{level=31, x=15, y=8},
	{level=32, x=16, y=8},
	{level=33, x=17, y=8},
	{level=34, x=18, y=8},
	{level=35, x=19, y=8},
	{level=36, x=20, y=8},
	{level=37, x=21, y=8},

-- 7. Dump (38-44) 				(7 levels)
	{level=38, x=13, y=10},
	{level=39, x=13, y=11},
	{level=40, x=13, y=12},
	{level=41, x=13, y=13},
	{level=42, x=13, y=14},
	{level=43, x=13, y=15},
	{level=44, x=13, y=16},

-- 2. Silver's ship (45-51) (7 levels)
	{level=45, x=14, y=11},
	{level=46, x=15, y=11},
	{level=47, x=16, y=11},
	{level=48, x=17, y=11},
	{level=49, x=18, y=11},
	{level=50, x=19, y=11},
	{level=51, x=20, y=11},

-- 4. UFO (52-58)	 (7 levels) (not 6)
	{level=52, x=12, y= 9},
	{level=53, x=11, y= 9},
	{level=54, x=10, y= 9},
	{level=55, x= 9, y= 9},
	{level=56, x= 8, y= 9},
	{level=57, x= 7, y= 9},
	{level=58, x= 6, y= 9},

-- 6. Treasure cave	 (59-64) (6 levels)
	{level=59, x=14, y=13},
	{level=60, x=15, y=13},
	{level=61, x=16, y=13},
	{level=62, x=17, y=13},
	{level=63, x=18, y=13},
	{level=64, x=19, y=13},

-- 8. Secret computer (65-70) (6 levels)
	{level=65, x=14, y= 7},
	{level=66, x=15, y= 7},
	{level=67, x=16, y= 7},
	{level=68, x=17, y= 7},
	{level=69, x=18, y= 7},
	{level=70, x=19, y= 7},

-- 9 Secret.			 (71-79) (9 levels)
	{level=71, x=12, y=12},
	{level=72, x=11, y=12},
	{level=73, x=10, y=12},
	{level=74, x= 9, y=12},
	{level=75, x= 8, y=12},
	{level=76, x= 7, y=12},
	{level=77, x= 6, y=12},
	{level=78, x= 5, y=12},
	{level=79, x= 4, y=12},
	--]]
}


-- function draw selection circle synced to the music
	
	function drawfishhousesphere(x,y)
			--if y>1 and y<10 and x==13 then
		for i, spherelevel in ipairs (spheres) do
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y
				if unlockedlevels[i]==true then
					love.graphics.setColor(0.5,0.5,0)
					love.graphics.circle("line",x,y,20+subbeat*20)
					--love.graphics.setColor(1,1,0)			-- highlight level under cursor
					--love.graphics.draw(sphere,(x*50)-50,(y*50)-50,0,0.5)

					love.graphics.setColor(1,1,1)
					love.graphics.print (zonetext[1],500, 850,0,1) 
					love.graphics.print ((y-1) .. '.' ..fishhouse0[i],500, 900,0,1) 
					--love.graphics.print (' '..y-1, (x-1)*tileSize, (y-1)*tileSize,0,1)
				end
			end
	end
	--[[
	function drawshipwreckssphere(x,y)
		if x>13 and x<25 and y==5 then 
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				--love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
        		love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[2],500, 850,0,1) 
				love.graphics.print ((x-5) .. '.' ..shipwrecks1[x-13], 500, 900,0,1)
                --love.graphics.print (' '..x-5, (x-1)*tileSize, (y-1)*tileSize,0,1)
			 end
	end
	
	function drawsilvershipsphere(x,y)
		if x>13 and x<21 and y==11 then 
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				--love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[3],500, 850,0,1) 
				love.graphics.print ((x+31) .. '.' ..silversship2[x-13], 500, 900,0,1) 
				--love.graphics.print (' '..x+31, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
            end
	 end
	

	
	function drawcitydeepsphere(x,y)
		if x>2 and x<13 and y==6 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				--love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
               	love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[4],500, 850,0,1) 
				love.graphics.print ((-x+32) .. '.' ..cityinthedeep3[(-x+13)], 500, 900,0,1)
				--love.graphics.print (' '..-x+32, (x-1)*tileSize, (y-1)*tileSize,0,1.1)
             end
	 end

	 function drawufosphere(x,y)
		if x>5 and x<13 and y==9 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				--love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
				love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[5],500, 850,0,1) 
				love.graphics.print ((-x+64) .. '.' ..ufo4[(-x+13)], 500, 900,0,1) 
				--love.graphics.print (' '..-x+64, (x-1)*tileSize, (y-1)*tileSize,0,1)
        end
	 end
	 
	 function drawcoralreefsphere(x,y)
		if x>13 and x<22 and y==8 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
			-- love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[6],500, 850,0,1) 
				love.graphics.print ((x+16) .. '.' ..coralreef5[x-13], 500, 900,0,1) 
				--love.graphics.print (' '..x+16, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
	 end

	 
	 	function drawtreasurecavesphere(x,y)
			if x>13 and x<20 and y==13 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				-- love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
            	love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[7],500, 850,0,1) 
				love.graphics.print ((x+45) .. '.' ..treasurecave6[x-13], 500, 900,0,1) 
				--love.graphics.print (' '..x+45, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
	 end
	
	 function drawdumpsphere(x,y)
		if y>9 and y<17 and x==13 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
			-- love.graphics.draw(sphere,600,-50+y*50,0,0.1)
            	love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[8],500, 850,0,1) 
				love.graphics.print ((y+28) .. ' ' ..dump7[y-9],500,900,0,1) 
				--love.graphics.print (' '..y+28, (x-1)*tileSize, (y-1)*tileSize,0,1)
            end
		 end
	
		 function drawsecretcomputersphere(x,y)
			if x>13 and x<20 and y==7 then
				love.graphics.setColor(0.5,0.5,0)
				love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
				love.graphics.setColor(1,1,0)			-- highlight level under cursor
				--love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
				love.graphics.setColor(1,1,1)
				love.graphics.print (zonetext[9],500, 850,0,1) 
				love.graphics.print ((x+51) .. '.' ..secretcomputer8[x-13], 500, 900,0,1)
				--love.graphics.print (' '..x+51, (x-1)*tileSize, (y-1)*tileSize,0,1)
			end
	end

	function drawsecretsphere(x,y)
		if x>3 and x<13 and y==12 then
			love.graphics.setColor(0.5,0.5,0)
			love.graphics.circle("line",(-25+x*50),(-25+y*50),20+subbeat*20)
			love.graphics.setColor(1,1,0)			-- highlight level under cursor
            --love.graphics.draw(sphere,(-50+x*50),(-50+y*50),0,0.1)
        	love.graphics.setColor(1,1,1)
			love.graphics.print (zonetext[10],500, 850,0,1) 
			love.graphics.print ((-x+83) .. '.' ..secret9[(-x+13)], 500, 900,0,1) 
            --love.graphics.print (' '..-x+83, (x-1)*tileSize, (y-1)*tileSize,0,1.2)
         end
	end
--]]
function drawlevelmenubck()
local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16

	if skin=="classic" then
		if skinupscaled==false then
			love.graphics.draw(menumap,0,0,0,2.1)
		elseif skinupscaled==true then
			--love.graphics.draw(menumapupscaled,0,0,0,0.52)
			love.graphics.draw(menumapupscaled_lower,0,0,0,0.52)
		end
	end
end

function drawlevelspheres(x,y)
	local tileWidth = 24 -- amount of tiles, one tile is one sphere
	local tileHeight = 16
	
	

	local width, height = love.graphics.getDimensions()
	
	-- to make constant tilesize:
	--tileSize = 50

	-- to make the spheres adaptive to window size:
	tileSize = math.min (width/tileWidth, height/tileHeight)
	tileSize = math.min (50, tileSize)



	local mx, my = love.mouse.getPosition()

	local x = math.floor(mx/tileSize)+1
	local y = math.floor(my/tileSize)+1



	for i, spherelevel in ipairs (spheres) do
	
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y

			love.graphics.setColor(0,1,0)
			drawCenteredSprite (x, y, tileSize, sphere)
			love.graphics.setFont(poorfishsmall)
			love.graphics.setColor(0,0,0)
			drawCenteredText ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize, level)
		end
	end


	--shadows
	local newBody = Body:new(newLightWorld)
	newBody:SetPosition(x*tileSize, y*tileSize)

	for i, spherelevel in ipairs (spheres) do
		local level = spherelevel.level
		local x1, y1 = spherelevel.x*tileSize, spherelevel.y*tileSize
		CircleShadow:new(newBody, x1*tileSize, y1*tileSize, 16) 
	end
	


	local sphereHovered
		
	for i, spherelevel in ipairs (spheres) do
		if unlockedlevels[i]==true then
			local level = spherelevel.level
			local x1, y1 = spherelevel.x, spherelevel.y
			if x1 == x and y1 == y then
				sphereHovered = spherelevel
				break
			end
			print(x)
			print(x1)
		end
	end
print(sphereHovered)
	if sphereHovered then
		local level = sphereHovered.level
		local x1, y1 = sphereHovered.x, sphereHovered.y
		
		love.graphics.setColor(1,1,0)
		drawCenteredSprite (x1, y1, tileSize, sphere)

		love.graphics.setColor(1,1,1)
		drawCenteredText ((x1-1)*tileSize, (y1-1)*tileSize, tileSize, tileSize, level)
			
		if love.mouse.isDown(1) and unlockedlevels[level]==true then
			--restore crt effect
			--effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
			--effect.enable("scanlines","crt","glow","filmgrain")
			
			timer2=0
			isloading=true
			loadingprocess=true
			loadbuttonpressed=false
			savebuttonpressed=false
				nLevel=level
					if shader1type=="crt" then
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep")
						effect.enable("scanlines","crt","glow","filmgrain")
				elseif shader1type=="godsgrid" then
						effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
						effect.enable("godsray")
				end
				--nLevel=level
				--changelevel() 
				--gamestatus="game"
				--timer=0
				--stepdone=0
			
		end
	end

		--drawonlytitles(x,y)
		--drawshadows(x,y,newBody)


------------------------------------------------
-- buttons
------------------------------------------------


	-- quit

	local hovered = isButtonHovered (exitButton)
	drawButton (exitButton, hovered,exittext)

	if hovered and love.mouse.isDown(1) then 
		love.timer.sleep( 0.5 )
		love.event.quit()
	end
	--[[
	-- load
	local hovered = isButtonHovered (loadButton)
	drawButton (loadButton, hovered,loadtext)

	if hovered and love.mouse.isDown(1) then 
		loadbuttonpressed=true timer2=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		loadmygame()
		love.timer.sleep( 0.5 )
	end
	--]]
	-- save
	local hovered = isButtonHovered (saveButton)
	drawButton (saveButton, hovered,savetext)
	if hovered then love.graphics.print ("This option saves current progress, achievements and settings", 200,900) end
	
	if hovered and love.mouse.isDown(1) then 
		savebuttonpressed=true saveconfirmpressed=false timer2=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		
		love.timer.sleep( 0.5 )	
	end
	
	
		-- reset saved game
	local hovered = isButtonHovered (resetsavegameButton)
	drawButton (resetsavegameButton, hovered,resetsavegamet)
	if hovered then love.graphics.print ("This option will delete all progress, achievements and settings", 200,900) end

	if hovered and love.mouse.isDown(1) then 
		savebuttonpressed=true saveconfirmpressed=false timer2=0
		--video effect
			effect.disable("colorgradespimple","vignette","scanlines","crt","dmg","godsray","desaturate","pixelate","posterize","boxblur","fastgaussianblur","chromasep","filmgrain","glow")
			effect.enable("boxblur")
		loadachievements()
		savebuttonpressed=true saveconfirmpressed=false timer2=0
		love.timer.sleep( 0.5 )	
	end
	


	
	-- options

	hovered = isButtonHovered (optionsButton)
	drawButton (optionsButton, hovered,optionstext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
		  --prevent palette not to be assigned
		    if palette==1 or palette==2 or palette==3 or palette==4 or palette==5 or palette==6 or palette==7 then
		    else palette=1
		    end
		    if currentfrg==false then 
					if skin=="remake" then
						loadlevelassets()			-- load assets
						--loadlevelmusic()			-- load music
						assignfrgbck()				-- Assign layers
				elseif skin=="classic" then	
						loadlevelassetsclassic()	-- load classic assets
						--loadlevelmusicclassic()		-- load music
						assignfrgbckclassic()		-- Assign layers
				end
			end
		--loadlevelassets()
		
			gamestatus="options" 
		end
	end


	--credits

	hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditstext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			love.timer.sleep( 0.2 )
			music:stop()
			music = love.audio.newSource( "/externalassets/music/pixelsphere/003_Vaporwareloop.ogg","static" )
				love.audio.play( music )
				music:setVolume(0.4)
			gamestatus="credits" 
		end
	end
	
	--Music player
if musicplayerunlocked==true then
	hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			music:stop()
			love.timer.sleep( 0.5 )
			gamestatus="music"
		end
	end
end

--[[
	--Level editor
if leveleditorunlocked==true then
	hovered = isButtonHovered (leditorButton)
	drawButton (leditorButton, hovered,leditortext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			music:stop()
			gamestatus="leditor"
		end
	end
end	
--]]
	-- extras
if extrasunlocked==true then
	hovered = isButtonHovered (extrasButton)
	drawButton (extrasButton, hovered,extrastext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			 music:stop()
			gamestatus="extras" 
		end
	end
end	

if endmenuunlocked==true then	
	-- Ends

	hovered = isButtonHovered (endButton)
	drawButton (endButton, hovered,endtext)

	if hovered and love.mouse.isDown(1) then 
		if love.mouse.isDown(1) then
			  music:stop()
			  require ('game/ends/endmenu')		-- Load ends menu
			  endmenuload()
				gamestatus="endmenu"
		end
	end
end
	if cutscenesunlocked==true then
		-- cutscenes
		local hovered = isButtonHovered (cutscenesButton)
		drawButton (cutscenesButton, hovered,cutscenest)
	
		if hovered and love.mouse.isDown(1) then 
			gamestatus="cutscenes"
			loadcutscenes()
			love.timer.sleep( 0.5 )	
		end
	end

end



function drawloadmessage()
	if loadbuttonpressed==true then
	love.graphics.print ("Settings loaded ", 500,500,0,1)
		if timer2>4 then 
			loadbuttonpressed=false 
		end
	end
end

function drawsavemessage()
	if savebuttonpressed==true and saveconfirmpressed==true then
	love.graphics.print ("Settings saved ", 500,500,0,1)
		if timer2>4 then 
			savebuttonpressed=false 
			saveconfirmpressed=false
		end
	end
end

function confirmsavebuttons()
		-- confirm save
if savebuttonpressed==true and saveconfirmpressed==false then
	local hovered = isButtonHovered (confirmsaveButton)
	drawButton (confirmsaveButton, hovered,confirmsavetext)

love.graphics.print ("Saving will replace any previous saved data, are you sure to proceed?", 200,400)
	if hovered and love.mouse.isDown(1) then 
		saveconfirmpressed=true timer2=0
		
		savemygame()
		love.timer.sleep( 0.5 )	
	end
	
	-- reject save
	local hovered = isButtonHovered (rejectsaveButton)
	drawButton (rejectsaveButton, hovered,rejectsavetext)

	if hovered and love.mouse.isDown(1) then 
		savebuttonpressed=false timer2=0
		saveconfirmpressed=false
		
		love.timer.sleep( 0.5 )	
	end
end	

end


function leveldraw()
	if loadbuttonpressed or savebuttonpressed then
			love.graphics.setShader()
			effect(function()
				drawlevelmenubck(x,y)
				drawlevelspheres(x,y)
			end)
			drawloadmessage()
			drawsavemessage()
			confirmsavebuttons()
	else
		drawlevelmenubck(x,y)
		drawlevelspheres(x,y)
	end

end

function drawonlytitles(x,y)
-- change shadow light from green to yellow when mouse hover spheres
	--[[if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end
	--]]
		love.graphics.setFont(poorfishmiddle)
	  --Fish House levels 1-8 
		--if x>10 and x<16 and y>0 and y<12 and unlockedlevels[y-1]==true then drawfishhousesphere(x,y) end
		for i, spherelevel in ipairs (spheres) do
			local level = spherelevel.level
			local x, y = spherelevel.x, spherelevel.y
				if unlockedlevels[i]==true then
					print (spherelevel.x)
					print (spherelevel.x)
					love.graphics.setColor(1,1,0)			-- highlight level under cursor
					--love.graphics.draw(sphere,(x*50)-50,(y*50)-50,0,0.1)
					--drawfishhousesphere((x*50)-50,(y*50)-50) 
				end
			end
	--[[
		--1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 and unlockedlevels[x]==true then drawshipwreckssphere(x,y) end
	    --2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 and unlockedlevels[x]==true then drawsilvershipsphere(x,y) end
       --3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 and unlockedlevels[x]==true then drawcitydeepsphere(x,y) end
		--4. UFO	--level 52-58
		if y>8 and y<10 and x>4 and x<14 and unlockedlevels[x]==true then drawufosphere(x,y) end
	   --5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 and unlockedlevels[x]==true then drawcoralreefsphere(x,y) end
	    --6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 and unlockedlevels[x]==true then drawtreasurecavesphere(x,y) end
		--7. Dump--level 38-44
		if x>12 and x<14 and y>8 and y<18 and unlockedlevels[y]==true then drawdumpsphere(x,y) end
		--8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 and unlockedlevels[x]==true then drawsecretcomputersphere(x,y) end
		-- 9. NG
		if y>11 and y<13 and x>3 and x<13 and unlockedlevels[x]==true then drawsecretsphere(x,y) end
		--]]
end

function drawshadows(x,y,newBody)


	-- change shadow light from green to yellow when mouse hover spheres
	if (x>12 and x<14 and y>1 and y<10) or (y==5 and x>13 and x<25) or y==11 and x>13 and x<21 or (y==6 and x>2 and x<13) or (y==9 and x>5 and x<13) or ( y==8 and x>13 and x<22) or (y==13 and x>13 and x<21) or (x==13 and y>9 and y<17) or y==7 and x>13 and x<20 or(y==12 and x>3 and x<13) then 
		newLight:SetColor(255, 255, 0, 100)
	else newLight:SetColor(0, 255, 0, 100)
	end

  --shadows --0. Fish House levels 1-8 
    if x>10 and x<16 and y>0 and y<12 then
        for i=1, 8, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(spherelevel.x, spherelevel.y)
			CircleShadow:new(newBody, 0, 20+i*50*1, 16)
		end
			drawfishhousesphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
				--shadows 1. Ship Wrecks	--level 9-19	
		if y>4 and y<6 and x>12 and x<26 then
			for i=1, 11, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 110)
				CircleShadow:new(newBody, 300+i*50*1, 110, 16)
			end
			drawshipwreckssphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
			
		end
	
			     --shadows 2. Silver's ship --level 45-51	 	
		if y>10 and y<12 and x>12 and x<22 then 
			for i=1, 7, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 260)
				CircleShadow:new(newBody, 300+i*50*1, 260, 16)
			end            
			drawsilvershipsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
	       --shadows 3. City in the deep	--level 20-29
       	if y>5 and y<7 and x>1 and x<14 then 
			for i=1, 10, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(315-i*0.5, 140)
				CircleShadow:new(newBody, 315-i*50*1, 140, 16)
			end
			drawcitydeepsphere(x,y)

		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
		end
	
		--shadows 	4. UFO	--level 52-58
	if y>8 and y<10 and x>4 and x<14 then
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(315-i*0.5, 210)
			CircleShadow:new(newBody, 315-i*50*1, 210, 16)
		end 
		drawufosphere(x,y)
		else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
	end
	
	    --shadows 5. Coral reef--level 30-37
	    if y>7 and y<9 and x>12 and x<23 then 
			for i=1, 8, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawcoralreefsphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 6.Treasure cave--level 59 64
	    if y==13 and x>13 and x<21 then
			for i=1, 4, 1 do	
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 190)
				CircleShadow:new(newBody, 300+i*50*1, 190, 16)
			end
			drawtreasurecavesphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
        end
	
	    --shadows 7. Dump--level 38-44
      if x>12 and x<14 and y>8 and y<18 then 
        for i=1, 7, 1 do
			newBody = Body:new(newLightWorld)
			newBody:SetPosition(620, 200+i*2*1)
			CircleShadow:new(newBody, 0, 200+i*50*1, 16)
		end
		drawdumpsphere(x,y)

       else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
       end
	
		 --shadows 8. Secret computer   --level 65-	70
		if y>6 and y<8 and x>12 and x<21 then
			for i=1, 6, 1 do
				newBody = Body:new(newLightWorld)
				newBody:SetPosition(320+i*2*1, 160)
				CircleShadow:new(newBody, 300+i*50*1, 160, 16)
			end
			drawsecretcomputersphere(x,y)

        else
		-- Clean up
			newBody:Remove()
			CircleShadow:Remove()
                
        end

end
