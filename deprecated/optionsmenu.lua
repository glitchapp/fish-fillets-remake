urutora = require('/lib/urutora/urutora')



local style = { 
	fgColor = urutora.utils.toRGB('#788089'),
	bgColor = urutora.utils.toRGB('#efefef'),
	hoverbgColor = urutora.utils.toRGB('#e3e3ef'),
	hoverfgColor = urutora.utils.toRGB('#148ee3'),
	disablefgColor = urutora.utils.toRGB('#ffffff'),
	disablebgColor = urutora.utils.toRGB('#cccccc'),
	outlineColor = urutora.utils.toRGB('#aaaaaa'),
}

local bgColor = {0.98,0.98,0.98}
local canvas
local panelA, panelB, panelC, panelD

function urutora.load()
	--local w, h = love.window.getMode()
	w=1080
	h=640
	--w = w / 2
	--h = h / 2

	u = urutora:new()

	function love.mousepressed(x, y, button) u:pressed(x, y) end
	function love.mousemoved(x, y, dx, dy) u:moved(x, y, dx, dy) end
	function love.mousereleased(x, y, button) u:released(x, y) end
	function love.textinput(text) u:textinput(text) end
	function love.wheelmoved(x, y) u:wheelmoved(x, y) end

	function love.keypressed(k, scancode, isrepeat)
		u:keypressed(k, scancode, isrepeat)

		if k == 'escape' then
			love.event.quit()
		end
	end

	canvas = love.graphics.newCanvas(w, h)
	canvas:setFilter('nearest', 'nearest')
	local font1 = love.graphics.newFont('lib/urutora/fonts/proggy/proggy-tiny.ttf', 16)
	local font2 = love.graphics.newFont('lib/urutora/fonts/proggy/proggy-square-rr.ttf', 16)

	u.setDefaultFont(font1)
	u.setResolution(canvas:getWidth(), canvas:getHeight())

	panelC = u.panel({ rows = 20, cols = 6, csy = 18, tag = 'PanelC'})
	panelC.outline = true
	panelC
		--:colspanAt(1, 1, 4)
	--[[for i = 1, 20 do
		panelC
			:colspanAt(i, 1, 3)
			:colspanAt(i, 4, 3)
			:addAt(i, 1, u.label({ text = tostring(i) }))
			:addAt(i, 4, u.button({ text = 'Button' }):action(function (e)
				e.target.text = tostring(i) .. ' clicked!'
			end))
	end--]]
	
			:colspanAt(1, 2, 3)
			:addAt(1, 2, u.button({ text = 'English' }):action(function (e)
				e.target.text = ' English selected'
				language="en"
		end))

	
			:colspanAt(2, 2, 3)
			:addAt(2, 2, u.button({ text = 'Espanol' }):action(function (e)
				e.target.text = ' Espanol seleccionado'
				language="es"
		end))
		
			:colspanAt(3, 2, 3)
			:addAt(3, 2, u.button({ text = 'Deutsch' }):action(function (e)
				e.target.text = ' Deutsch gewählt'
				language="de"
		end))
		
		:colspanAt(4, 2, 3)
			:addAt(4, 2, u.button({ text = 'French' }):action(function (e)
				e.target.text = ' French selected'
				language="fr"
		end))
		
		:colspanAt(5, 2, 3)
			:addAt(5, 2, u.button({ text = 'Brazilian' }):action(function (e)
				e.target.text = ' Brazilian selected'
				language="br"
		end))
		
		:colspanAt(6, 2, 3)
			:addAt(6, 2, u.button({ text = 'German Switzerland' }):action(function (e)
				e.target.text = ' German Switzerland selected'
				language="ch"
		end))
		
		:colspanAt(7, 2, 3)
			:addAt(7, 2, u.button({ text = 'Czech' }):action(function (e)
				e.target.text = ' Czech selected'
				language="cs"
		end))
		
		:colspanAt(8, 2, 3)
			:addAt(8, 2, u.button({ text = 'Esperanto' }):action(function (e)
				e.target.text = ' Esperanto selected'
				language="eo"
		end))
		
		:colspanAt(9, 2, 3)
			:addAt(9, 2, u.button({ text = 'Italian' }):action(function (e)
				e.target.text = ' Italian selected'
				language="it"
		end))
		
		:colspanAt(10, 2, 3)
			:addAt(10, 2, u.button({ text = 'Dutch' }):action(function (e)
				e.target.text = ' Dutch selected'
				language="nl"
		end))
		
		:colspanAt(11, 2, 3)
			:addAt(11, 2, u.button({ text = 'Polish' }):action(function (e)
				e.target.text = ' Polish selected'
				language="pl"
		end))
		
		:colspanAt(12, 2, 3)
			:addAt(12, 2, u.button({ text = 'Portuguese' }):action(function (e)
				e.target.text = ' Portuguese selected'
				language="pr"
		end))
		
		:colspanAt(13, 2, 3)
			:addAt(13, 2, u.button({ text = 'Russian' }):action(function (e)
				e.target.text = ' Russian selected'
				language="ru"
		end))
		
		:colspanAt(14, 2, 3)
			:addAt(14, 2, u.button({ text = 'Swedish' }):action(function (e)
				e.target.text = ' Swedish selected'
				language="se"
		end))
		
		:colspanAt(15, 2, 3)
			:addAt(15, 2, u.button({ text = 'Slovenian' }):action(function (e)
				e.target.text = ' Slovenian selected'
				language="sk"
		end))
		
	
	panelE = u.panel({ rows = 20, cols = 6, csy = 18, tag = 'PanelE'})
	panelE.outline = true
	panelE
		
		--:colspanAt(1, 1, 4)
	--[[for i = 1, 20 do
		panelC
			:colspanAt(i, 1, 3)
			:colspanAt(i, 4, 3)
			:addAt(i, 1, u.label({ text = tostring(i) }))
			:addAt(i, 4, u.button({ text = 'Button' }):action(function (e)
				e.target.text = tostring(i) .. ' clicked!'
			end))
	end--]]
	
			:colspanAt(1, 2, 4)
			:addAt(1, 2, u.button({ text = 'Dialogs' }):action(function (e)
				if talkies==true then 
					e.target.text = ' Dialogs disabled'
					talkies=false
				elseif talkies==false then
					e.target.text = ' Dialogs enabled'
					talkies=true
				end
		end))
		

	panelF = u.panel({ rows = 20, cols = 6, csy = 18, tag = 'PanelF'})
	panelF.outline = true
	panelF
		
			:colspanAt(1, 2, 4)
			:addAt(1, 2, u.button({ text = 'CRT Effect' }):action(function (e)
				if shader1==true then 
					e.target.text = ' CRT Effect disabled'
					shader1=false
				elseif shader1==false then
					e.target.text = ' CRT Effect enabled'
					shader1=true
				end
		end))
		

			:colspanAt(2, 2, 4)
			--:addAt(2, 1, u.label({ text = "Underwater scene" }))
			:addAt(2, 2, u.button({ text = 'Underwater scene' }):action(function (e)
				e.target.text = ' Underwater scene enabled'
				shader2=true
		end))


	panelG = u.panel({ rows = 20, cols = 6, csy = 18, tag = 'PanelG'})
	panelG.outline = true
	panelG
		
			:colspanAt(1, 2, 4)
			:addAt(1, 2, u.button({ text = 'Touch controls' }):action(function (e)
				if touchinterfaceison==true then 
					e.target.text = ' Touch disabled'
					touchinterfaceison=false
				elseif touchinterfaceison==false then
					e.target.text = ' Touch enabled'
					touchinterfaceison=true
				end
		end))
		

	panelD = u.panel({ rows = 4, cols = 2, tag = 'PanelD' })
	panelD.outline = true
	panelD:setStyle({outlineColor = {1, 1, 1}})
	panelD
		:colspanAt(2, 1, 2)
		:colspanAt(3, 1, 2)
		:rowspanAt(3, 1, 2)
		:addAt(1, 1, u.button({ text = '1' }))
		:addAt(1, 2, u.button({ text = '2' }))
		:addAt(2, 1, u.slider())
		:addAt(3, 1, u.image({ image = love.graphics.newImage('lib/urutora/img/unnamed.png'), keep_aspect_ratio = true }))

	panelB = u.panel({ rows = 10, cols = 4, tag = 'PanelB', csy = 32, spacing = 5 })
	panelB.outline = true
	panelB
		:colspanAt(1, 1, 4)
		:colspanAt(3, 2, 2)
		:rowspanAt(3, 2, 2)
		:rowspanAt(3, 4, 2)

		:colspanAt(5, 1, 4)		-- Dialogs
		:colspanAt(6, 2, 2)
		
		:colspanAt(7, 1, 4)		-- Graphics
		:colspanAt(8, 2, 2)
		
		:colspanAt(9, 1, 4)		-- Controls
		:colspanAt(10, 2, 2)

		:addAt(1, 1, u.label({ text = 'Use mouse wheel to scroll' }))
		:addAt(3, 1, u.label({ text = 'Language' }))

		:addAt(4, 1, u.toggle({ text = 'D enabled', value = true }):action(function (e)
			if e.target.value then
				e.target.text = 'D enabled'
				panelD:enable()
			else
				e.target.text = 'D disabled'
				panelD:disable()
			end
		end))
		:addAt(2, 3, u.toggle({ text = 'D visible', value = true }):action(function (e)
			if e.target.value then
				e.target.text = 'D visible'
				panelD:show()
			else
				e.target.text = 'D unvisible'
				panelD:hide()
			end
		end))
		
		:addAt(2, 4, u.label({ text = 'D panel'}))
		:addAt(3, 2, panelC)
		:addAt(3, 4, panelD)
		
		:addAt(6, 1, u.label({ text = 'Dialogs' }))
		:addAt(6, 2, panelE)
		
		:addAt(8, 1, u.label({ text = 'Graphics' }))
		:addAt(8, 2, panelF)
		
		:addAt(10, 1, u.label({ text = 'Controls' }))
		:addAt(10, 2, panelG)

	panelA = u.panel({ rows = 6, cols = 4, x = 10, y = 20, w = w - 20, h = h - 20, tag = 'PanelA' })
	--panelA.outline = true
	panelA
		:rowspanAt(1, 4, 2)
		:rowspanAt(5, 1, 2)
		:colspanAt(5, 1, 3)
		:rowspanAt(3, 2, 3)
		:colspanAt(3, 2, 3)
		:rowspanAt(3, 1, 3)
		:addAt(1, 1, u.label({ text = 'A panel' }))
		:addAt(1, 2, u.toggle({ text = 'Slider Toggle' }):action(function (e)
			local slider = panelA:getChildren(1, 3)
			if e.target.value then
				slider:enable()
			else
				slider:disable()
			end
		end))
		:addAt(1, 3, u.slider({ value = 0.3, tag = 'slider', axis = 'x'  }):disable():action(function(e)
			panelC:setScrollY(e.target.value)
		end))
		:addAt(3, 1, u.slider({ value = 0.3, tag = 'slider', axis = 'y' }):action(function(e)
			panelC:setScrollY(e.target.value)
		end))
		:addAt(2, 3, u.toggle({ value = false, text = 'Boolean' }):right())
		:addAt(2, 2, u.text({ text = 'привет мир!' }):setStyle({ font = font2 }))
		:addAt(3, 2, panelB)
		:addAt(1, 4, u.joy())

		:addAt(2, 1, u.multi({ items = { 'One', 'Two', 'Three' } }):left() 
			:setStyle({ bgColor = { 0.6, 0.7, 0.8 } }, true))

	u:add(panelA)
	panelA:setStyle(style)

	local clickMe = urutora.button({
		text = 'Return to game',
		x = 2, y = 2,
		w = 200,
	})
	
	--local clickimage = urutora.image({ image='img/unnamed.png', x=100, y=100, w=100, h=100, keep_aspect_ratio=true})
	--local clickmenow = urutora.button({ text="clickmenow", x=0, y=50, w=50, h=50 })

	local num = 0
	clickMe:action(function(e)
		--num = num + 1
		--e.target.text = 'You clicked me ' .. num .. ' times!'
		e.target.text = 'Return to game'
				gamestatus="levelselection"
				--if talkies==true then Obey.lev1() end
	end)

	u:add(clickMe)
	--u:add(clickmenow)

	--activation and deactivation elements by tag
	--u:deactivateByTag('PanelD')
end

function optionsbutton()
	local w, h = love.window.getMode()
	--w=1120
	--h=800
	--w = w / 1
	--h = h / 1

	u = urutora:new()

	function love.mousepressed(x, y, button) u:pressed(x, y) end
	function love.mousemoved(x, y, dx, dy) u:moved(x, y, dx, dy) end
	function love.mousereleased(x, y, button) u:released(x, y) end
	function love.textinput(text) u:textinput(text) end
	function love.wheelmoved(x, y) u:wheelmoved(x, y) end

	function love.keypressed(k, scancode, isrepeat)
		u:keypressed(k, scancode, isrepeat)

		if k == 'escape' then
			love.event.quit()
		end
	end

	canvas = love.graphics.newCanvas(w, h)
	canvas:setFilter('nearest', 'nearest')
	local font1 = love.graphics.newFont('lib/urutora/fonts/proggy/proggy-tiny.ttf', 16)
	local font2 = love.graphics.newFont('lib/urutora/fonts/proggy/proggy-square-rr.ttf', 16)

	u.setDefaultFont(font1)
	u.setResolution(canvas:getWidth(), canvas:getHeight())

	panelC = u.panel({ rows = 20, cols = 6, csy = 18, tag = 'PanelC'})
	panelC.outline = true
	panelC
		:colspanAt(1, 1, 6)
		:addAt(1, 1, u.label({ text = 'C in B' }))
	for i = 1, 20 do
		panelC
			:colspanAt(i, 1, 3)
			:colspanAt(i, 4, 3)
			:addAt(i, 1, u.label({ text = tostring(i) }))
			:addAt(i, 4, u.button({ text = 'Button' }):action(function (e)
				e.target.text = tostring(i) .. ' clicked!'
				gamestatus="options"
				optionsmenu()
			end))
	end


	local clickMe = urutora.button({
		text = 'Click me!',
		x = 2, y = 2,
		w = 200,
	})
	
	--local clickimage = urutora.image({ image='img/unnamed.png', x=100, y=100, w=100, h=100, keep_aspect_ratio=true})
	--local clickmenow = urutora.button({ text="clickmenow", x=0, y=50, w=50, h=50 })

	local num = 0
	clickMe:action(function(e)
		num = num + 1
		e.target.text = 'You clicked me ' .. num .. ' times!'

	end)

	u:add(clickMe)
	--u:add(clickmenow)

	--activation and deactivation elements by tag
	--u:deactivateByTag('PanelD')
	
	
end



function love.update(dt)
	u:update(dt)

	--panelA:moveTo(x, y)
	--x = x + 0.1
	--y = y + 0.1
end

function love.draw()
print("x: " .. x .. "y: " .. y)
	
	love.graphics.setCanvas(canvas)
	love.graphics.clear(bgColor)
	u:draw()
	love.graphics.setCanvas()

	love.graphics.draw(canvas, 0, 0, 0,
		love.graphics.getWidth() / canvas:getWidth(),
		love.graphics.getHeight() / canvas:getHeight()
	)
end
