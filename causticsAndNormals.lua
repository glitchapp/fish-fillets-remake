causticsAndNormals = love.graphics.newShader([[
/* Shader constants */
#define P 0.051 // Precision step for derivatives
#define WATER vec4(0.9, 0.9, 1.0, 1.0) // Water color overlay
#define TAU 6.28318530718 // Tau (2 * Pi)
#define MAX_ITER 7 // Maximum iterations for caustics computation

/* Uniform variables */
uniform float time; // Current time for animation
uniform sampler2D u_heightmap; // Heightmap texture
uniform sampler2D u_normals; // Normals texture
uniform bool hasNormals; // Flag to indicate normal mapping
uniform bool hasHeightmap; // Flag to indicate heightmap usage

uniform vec2 Resolution; // Screen resolution
uniform vec3 LightPos1; // Position of the first light source
uniform vec4 LightColor1; // Color of the first light source
uniform vec3 LightPos2; // Position of the second light source
uniform vec4 LightColor2; // Color of the second light source
uniform vec4 AmbientColor; // Ambient lighting color
uniform vec3 Falloff; // Light falloff parameters
uniform float GrainIntensity; // Intensity of grain effect
uniform float CausticIntensity; // Intensity of caustics effect
uniform vec2 WaveSize; // Wave size scaling

/* Helper function to sample height from the heightmap */
float height(vec2 coords) {
    return Texel(u_heightmap, coords).r;
}

/* Computes a wave pattern for distortion effects */
float wave(vec2 coords, float offset) {
    return sin(coords.x * WaveSize.x + offset) * sin(coords.y * WaveSize.y + offset) * 0.02;
}

/* Calculates shadow effects using light direction and occlusion */
float calculateShadow(vec2 coords, vec2 lightDir) {
    float shadowFactor = 1.0;
    float shadowTime = time * 0.1; // Separate time scaling for shadows

    for (float i = 1.0; i <= 5.0; i += 1.0) {
        vec2 offset = lightDir * i * P;
        offset += vec2(wave(coords, shadowTime), wave(coords, shadowTime + 1.57)) * i * 0.03;

        float sampleHeight = height(coords + offset);
        if (sampleHeight > height(coords) + 0.03 * i) {
            shadowFactor -= 0.15;
            break;
        }
    }
    return shadowFactor;
}

/* Sine-based function for generating wave-like height patterns */
float func(vec2 coords, float offset) {
    return (sin(coords.x - offset) * sin(coords.y + offset) + 1.0) / 8.0;
}

/* Computes x-derivative for wave distortion */
float xDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(P, 0.0), offset) - func(coords - vec2(P, 0.0), offset)) / (2.0 * P);
}

/* Computes y-derivative for wave distortion */
float yDerivative(vec2 coords, float offset) {
    return (func(coords + vec2(0.0, P), offset) - func(coords - vec2(0.0, P), offset)) / (2.0 * P);
}

/* Generates pseudo-random noise */
float noise(vec2 uv) {
    return fract(sin(dot(uv, vec2(12.9898, 78.233)) + time * 0.1) * 43758.5453);
}

/* Computes caustic lighting effect */
vec3 computeCaustics(vec2 uv) {
    float causticTime = time * 0.2; // Time scaling for caustics
    vec2 p = mod(uv * TAU, TAU) - 250.0;
    vec2 i = vec2(p);
    float c = 1.0;
    float inten = 0.005;

    for (int n = 0; n < MAX_ITER; n++) {
        float t = causticTime * (1.0 - (3.5 / float(n + 1)));
        i = p + vec2(cos(t - i.x) + sin(t + i.y), sin(t - i.y) + cos(t + i.x));
        c += 1.0 / length(vec2(p.x / (sin(i.x + t) / inten), p.y / (cos(i.y + t) / inten)));
    }
    c /= float(MAX_ITER);
    c = 1.17 - pow(c, 1.4);
    return clamp(vec3(pow(abs(c), 8.0)) + vec3(0.0, 0.35, 0.5), 0.0, 1.0);
}

/* Main fragment shader function */
vec4 effect(vec4 vColor, Image texture, vec2 vTexCoord, vec2 pixcoord) {
    float heightMapValue = height(vTexCoord * WaveSize);
    float waveHeight = func(vTexCoord * WaveSize, time);
    float adjustedWaveHeight = waveHeight + heightMapValue;

    float displacementFactor = 0.15;
    vec2 warpOffset = vec2(xDerivative(vTexCoord * WaveSize, time), yDerivative(vTexCoord * WaveSize, time)) * adjustedWaveHeight * displacementFactor;
    vec2 warpedTexCoord = vTexCoord + warpOffset;

    vec4 DiffuseColor = Texel(texture, warpedTexCoord);
    vec3 NormalMap = hasNormals ? Texel(u_normals, vTexCoord).rgb : vec3(0.5, 0.5, 1.0);

    vec3 CausticEffect = computeCaustics(warpedTexCoord);

    vec3 LightDir1 = vec3(LightPos1.xy - (pixcoord.xy / Resolution.xy), LightPos1.z);
    LightDir1.x *= Resolution.x / Resolution.y;
    float D1 = length(LightDir1);
    vec3 N = normalize(NormalMap * 2.0 - 1.0);
    vec3 L1 = normalize(LightDir1);
    float shadowFactor1 = calculateShadow(vTexCoord, LightDir1.xy);
    vec3 Diffuse1 = (LightColor1.rgb * LightColor1.a) * max(dot(N, L1), 0.0) * shadowFactor1;

    vec3 LightDir2 = vec3(LightPos2.xy - (pixcoord.xy / Resolution.xy), LightPos2.z);
    LightDir2.x *= Resolution.x / Resolution.y;
    float D2 = length(LightDir2);
    vec3 L2 = normalize(LightDir2);
    float shadowFactor2 = calculateShadow(vTexCoord, LightDir2.xy);
    vec3 Diffuse2 = (LightColor2.rgb * LightColor2.a) * max(dot(N, L2), 0.0) * shadowFactor2;

    vec3 Ambient = AmbientColor.rgb * AmbientColor.a;
    float Attenuation1 = 1.0 / (Falloff.x + (Falloff.y * D1) + (Falloff.z * D1 * D1));
    float Attenuation2 = 1.0 / (Falloff.x + (Falloff.y * D2) + (Falloff.z * D2 * D2));

    vec3 Intensity = Ambient + Diffuse1 * Attenuation1 + Diffuse2 * Attenuation2;
    vec3 FinalColor = DiffuseColor.rgb * Intensity;
    FinalColor = mix(FinalColor, FinalColor + CausticEffect * CausticIntensity, CausticIntensity);

    float grain = noise(warpedTexCoord * 15.0) * GrainIntensity;
    FinalColor += grain;

    vec4 finalOutput = vColor * vec4(clamp(FinalColor, 0.0, 1.0), DiffuseColor.a);
    return mix(finalOutput, finalOutput * WATER, waveHeight * 0.8);
}

]])
