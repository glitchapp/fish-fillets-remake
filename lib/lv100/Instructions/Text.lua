function LoadLV100Text()




if language=="en" then
	require ('lib/lv100/Instructions/en/Controller')
	require ('lib/lv100/Instructions/en/crossSave')
	require ('lib/lv100/Instructions/en/HowToSave')
	require ('lib/lv100/Instructions/en/Hud')
	require ('lib/lv100/Instructions/en/LevelEditor')
	require ('lib/lv100/Instructions/en/MusicPlayer')
	require ('lib/lv100/Instructions/en/Performance')
	require ('lib/lv100/Instructions/en/Rules')
	require ('lib/lv100/Instructions/en/Skins')
	
	
	if InstructionsPage=="rules" then LoadLV100Rules()
elseif InstructionsPage=="howtosave" then LoadLV100HowToSave()
elseif InstructionsPage=="controllerLayout" then LoadLV100Controller()
elseif InstructionsPage=="keyboardSchema" then
elseif InstructionsPage=="mouseSchema" then 
elseif InstructionsPage=="Skins" then LoadLV100Skins()
elseif InstructionsPage=="Hud" then LoadLV100Hud()
elseif InstructionsPage=="Performance" then LoadLV100Performance()
elseif InstructionsPage=="CrossSave" then LoadLV100CrossSave()
elseif InstructionsPage=="LevelEditor" then LoadLV100LevelEditor()
elseif InstructionsPage=="MusicPlayer" then LoadLV100MusicPlayer()
end
	
elseif language=="es" then
	require ('lib/lv100/Instructions/es/Controller')
	require ('lib/lv100/Instructions/es/crossSave')
	require ('lib/lv100/Instructions/es/HowToSave')
	require ('lib/lv100/Instructions/es/Hud')
	require ('lib/lv100/Instructions/es/LevelEditor')
	require ('lib/lv100/Instructions/es/MusicPlayer')
	require ('lib/lv100/Instructions/es/Performance')
	require ('lib/lv100/Instructions/es/Rules')
	require ('lib/lv100/Instructions/es/Skins')
			
	if InstructionsPage=="rules" then LoadLV100Rules()
elseif InstructionsPage=="howtosave" then LoadLV100HowToSavees()
elseif InstructionsPage=="controllerLayout" then LoadLV100Controlleres()
elseif InstructionsPage=="keyboardSchema" then
elseif InstructionsPage=="mouseSchema" then 
elseif InstructionsPage=="Skins" then LoadLV100Skinses()
elseif InstructionsPage=="Hud" then LoadLV100Hudes()
elseif InstructionsPage=="Performance" then LoadLV100Performancees()
elseif InstructionsPage=="CrossSave" then LoadLV100CrossSavees()
elseif InstructionsPage=="LevelEditor" then LoadLV100LevelEditores()
elseif InstructionsPage=="MusicPlayer" then LoadLV100MusicPlayeres()
end

elseif language=="de" then
	require ('lib/lv100/Instructions/de/Controller')
	require ('lib/lv100/Instructions/de/crossSave')
	require ('lib/lv100/Instructions/de/HowToSave')
	require ('lib/lv100/Instructions/de/Hud')
	require ('lib/lv100/Instructions/de/LevelEditor')
	require ('lib/lv100/Instructions/de/MusicPlayer')
	require ('lib/lv100/Instructions/de/Performance')
	require ('lib/lv100/Instructions/de/Rules')
	require ('lib/lv100/Instructions/de/Skins')
	
		if InstructionsPage=="rules" then LoadLV100Rulesde()
elseif InstructionsPage=="howtosave" then LoadLV100HowToSavede()
elseif InstructionsPage=="controllerLayout" then LoadLV100Controllerde()
elseif InstructionsPage=="keyboardSchema" then
elseif InstructionsPage=="mouseSchema" then 
elseif InstructionsPage=="Skins" then LoadLV100Skinsde()
elseif InstructionsPage=="Hud" then LoadLV100Hudde()
elseif InstructionsPage=="Performance" then LoadLV100Performancede()
elseif InstructionsPage=="CrossSave" then LoadLV100CrossSavede()
elseif InstructionsPage=="LevelEditor" then LoadLV100LevelEditorde()
elseif InstructionsPage=="MusicPlayer" then LoadLV100MusicPlayerde()
end


	elseif language=="fr" then
	require ('lib/lv100/Instructions/fr/Controller')
	require ('lib/lv100/Instructions/fr/crossSave')
	require ('lib/lv100/Instructions/fr/HowToSave')
	require ('lib/lv100/Instructions/fr/Hud')
	require ('lib/lv100/Instructions/fr/LevelEditor')
	require ('lib/lv100/Instructions/fr/MusicPlayer')
	require ('lib/lv100/Instructions/fr/Performance')
	require ('lib/lv100/Instructions/fr/Rules')
	require ('lib/lv100/Instructions/fr/Skins')
		
			if InstructionsPage=="rules" then LoadLV100Rulesfr()
elseif InstructionsPage=="howtosave" then LoadLV100HowToSavefr()
elseif InstructionsPage=="controllerLayout" then LoadLV100Controllerfr()
elseif InstructionsPage=="keyboardSchema" then
elseif InstructionsPage=="mouseSchema" then 
elseif InstructionsPage=="Skins" then LoadLV100Skinsfr()
elseif InstructionsPage=="Hud" then LoadLV100Hudfr()
elseif InstructionsPage=="Performance" then LoadLV100Performancefr()
elseif InstructionsPage=="CrossSave" then LoadLV100CrossSavefr()
elseif InstructionsPage=="LevelEditor" then LoadLV100LevelEditorfr()
elseif InstructionsPage=="MusicPlayer" then LoadLV100MusicPlayerfr()
end


	elseif language=="nl" then
	require ('lib/lv100/Instructions/nl/Controller')
	require ('lib/lv100/Instructions/nl/crossSave')
	require ('lib/lv100/Instructions/nl/HowToSave')
	require ('lib/lv100/Instructions/nl/Hud')
	require ('lib/lv100/Instructions/nl/LevelEditor')
	require ('lib/lv100/Instructions/nl/MusicPlayer')
	require ('lib/lv100/Instructions/nl/Performance')
	require ('lib/lv100/Instructions/nl/Rules')
	require ('lib/lv100/Instructions/nl/Skins')
		
			if InstructionsPage=="rules" then LoadLV100Rulesnl()
elseif InstructionsPage=="howtosave" then LoadLV100HowToSavenl()
elseif InstructionsPage=="controllerLayout" then LoadLV100Controllernl()
elseif InstructionsPage=="keyboardSchema" then
elseif InstructionsPage=="mouseSchema" then 
elseif InstructionsPage=="Skins" then LoadLV100Skinsnl()
elseif InstructionsPage=="Hud" then LoadLV100Hudnl()
elseif InstructionsPage=="Performance" then LoadLV100Performancenl()
elseif InstructionsPage=="CrossSave" then LoadLV100CrossSavenl()
elseif InstructionsPage=="LevelEditor" then LoadLV100LevelEditornl()
elseif InstructionsPage=="MusicPlayer" then LoadLV100MusicPlayernl()
end

end

end












