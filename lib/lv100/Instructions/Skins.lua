

function LoadSkins()

	Skin1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/skins/1.webp")))
	Skin2 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/skins/2.webp")))
	Skin3 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/skins/3.webp")))
	Skin4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/skins/4.webp")))
	Skin5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/skins/5.webp")))
						
CleanLV100Screen()



term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,2,lvr1)
term:set_cursor_color(Terminal.schemes.basic[2])
--term:blit(12,20,lvr2)
term:set_cursor_color(Terminal.schemes.basic[3])
term:blit(12,20,lvr3)
--term:set_cursor_color(Terminal.schemes.basic[5])
--term:blit(12,22,lvr4)

term:set_cursor_color(Terminal.schemes.basic[6])
term:set_cursor_backcolor(Terminal.schemes.basic[0])
term:reverse_cursor(true)
term:blit(12,24,"Press a mouse key to return")

CrossSaveScreen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/crossPlay.webp")))

end

function updateSkins(dt)
	timer=timer+dt
end

function DrawSkinsScreenshots()
		
			if timer<15 			  then love.graphics.draw(Skin1,50,150,0,0.25,0.25) 
		elseif timer>15 and timer <30 then love.graphics.draw(Skin2,50,150,0,0.25,0.25)
		elseif timer>30 and timer <45 then love.graphics.draw(Skin3,50,150,0,0.25,0.25)
		elseif timer>45 and timer <60 then love.graphics.draw(Skin4,50,150,0,0.25,0.25)
		elseif timer>60 and timer <75 then love.graphics.draw(Skin5,50,150,0,0.25,0.25)
		elseif timer>75 then timer=0
		end
		
end
