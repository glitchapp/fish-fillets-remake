function LoadHow2Save()
CleanLV100Screen()

term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,2,lvr1)
term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,15,lvr2)
term:set_cursor_color(Terminal.schemes.basic[3])
term:blit(12,20,lvr3)
term:set_cursor_color(Terminal.schemes.basic[5])
term:blit(12,22,lvr4)

term:set_cursor_color(Terminal.schemes.basic[6])
term:set_cursor_backcolor(Terminal.schemes.basic[0])
term:reverse_cursor(true)
term:blit(12,24,"Press a mouse key to return")

how2SaveScreen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/how2Save.webp")))

end

function DrawHowToSaveScreenshots()
		
      	love.graphics.draw(how2SaveScreen,450,150,0,1,1)
      	
end
