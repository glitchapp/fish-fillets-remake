function LoadInstructionsIndex()

require 'lib/lv100/Instructions/GameRules'

require 'lib/lv100/Instructions/How2Save'
require 'lib/lv100/Instructions/ControllerLayout'
require 'lib/lv100/Instructions/KeyboardInstructions'
require 'lib/lv100/Instructions/MouseInstructions'
require 'lib/lv100/Instructions/CrossSave'
require 'lib/lv100/Instructions/Skins'
require 'lib/lv100/Instructions/Hud'
require 'lib/lv100/Instructions/Performance'
require 'lib/lv100/Instructions/LevelEditor'
require 'lib/lv100/Instructions/MusicPlayer'

require 'lib/lv100/Instructions/Text'
LoadLV100Text()


		disk3 = love.audio.newSource( "externalassets/sounds/level38/diskdrive/disk3.ogg","static" )
		disk5 = love.audio.newSource( "externalassets/sounds/level38/diskdrive/disk5.ogg","static" )
		disk6 = love.audio.newSource( "externalassets/sounds/level38/diskdrive/disk6.ogg","static" )
		
		disk3:setVolume(0.3)
		disk5:setVolume(0.3)
		disk6:setVolume(0.3)

GameRulesButton = {
	text = "game rules",
	x = 25, 
	y = 200,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

--controls
How2SaveButton = {
	text = "How to save",
	x = 25, 
	y = 300,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

ControllerLayoutButton = {
	text = "Controller layout",
	x = 25, 
	y = 350,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

KeyboardSchemaButton = {
	text = "Keyboard Schema",
	x = 25, 
	y = 400,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

MouseSchemaButton = {
	text = "Mouse Schema",
	x = 25, 
	y = 450,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

--graphics

SkinButton = {
	text = "Skins,effects & colors",
	x = 25, 
	y = 600,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

HudButton = {
	text = "Hud",
	x = 25, 
	y = 650,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

PerformanceButton = {
	text = "Performance",
	x = 25, 
	y = 700,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

--other

CrossSaveButton = {
	text = "Cross save",
	x = 25, 
	y = 850,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

LevelEditorButton = {
	text = "Level editor",
	x = 25, 
	y = 900,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

MusicPlayedrButton = {
	text = "Music Player",
	x = 25, 
	y = 950,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}



InstructionsPage="rules"



end

function updateInstructionsIndex()

end

function DrawCurrentInstructionsPage()
		
		love.graphics.setColor(1,0,0,1)
		
		if InstructionsPage=="rules" 				then
			love.graphics.rectangle("line", GameRulesButton.x-10,GameRulesButton.y-10,GameRulesButton.x+200,GameRulesButton.x+20)
	elseif InstructionsPage=="howtosave" 			then
			love.graphics.rectangle("line", How2SaveButton.x-10,How2SaveButton.y-10,How2SaveButton.x+200,How2SaveButton.x+20)
	elseif InstructionsPage=="controllerLayout" 	then
			love.graphics.rectangle("line", ControllerLayoutButton.x-10,ControllerLayoutButton.y-10,ControllerLayoutButton.x+200,ControllerLayoutButton.x+20)
	elseif InstructionsPage=="keyboardSchema" 		then
			love.graphics.rectangle("line", KeyboardSchemaButton.x-10,KeyboardSchemaButton.y-10,KeyboardSchemaButton.x+200,KeyboardSchemaButton.x+20)
	elseif InstructionsPage=="mouseSchema"	 		then
			love.graphics.rectangle("line", MouseSchemaButton.x-10,MouseSchemaButton.y-10,MouseSchemaButton.x+200,MouseSchemaButton.x+20)
	elseif InstructionsPage=="CrossSave"	 		then
			love.graphics.rectangle("line", CrossSaveButton.x-10,CrossSaveButton.y-10,CrossSaveButton.x+200,CrossSaveButton.x+20)
	elseif InstructionsPage=="Skins"	 			then
			love.graphics.rectangle("line", SkinButton.x-10,SkinButton.y-10,SkinButton.x+200,SkinButton.x+20)
	elseif InstructionsPage=="Hud"	 				then
			love.graphics.rectangle("line", HudButton.x-10,HudButton.y-10,HudButton.x+200,HudButton.x+20)
	elseif InstructionsPage=="Performance"	 		then
			love.graphics.rectangle("line", PerformanceButton.x-10,PerformanceButton.y-10,PerformanceButton.x+200,PerformanceButton.x+20)
	elseif InstructionsPage=="LevelEditor"	 		then
			love.graphics.rectangle("line", LevelEditorButton.x-10,LevelEditorButton.y-10,LevelEditorButton.x+200,LevelEditorButton.x+20)
	elseif InstructionsPage=="MusicPlayer"	 		then
			love.graphics.rectangle("line", MusicPlayedrButton.x-10,MusicPlayedrButton.y-10,MusicPlayedrButton.x+200,MusicPlayedrButton.x+20)
	end
	
	
	
	love.graphics.setColor(1,1,1,1)
	
end

function DrawInstructionsIndex()

	love.graphics.print("Move mouse here",10,125,0,0.7)
	love.graphics.print("Index:",10,150,0,1)

	if IndexLeftMenuTriggered==true then
	
	love.graphics.setColor(0.5,0.1,0.3,0.5)
	love.graphics.rectangle("fill", GameRulesButton.x-10,GameRulesButton.y-10,GameRulesButton.x+250,(GameRulesButton.x+20)*50)
	love.graphics.setColor(1,1,1,1)
	
	hovered = isButtonHovered (GameRulesButton)
	drawButton (GameRulesButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			CleanLV100Screen()
			disk6:play()
			while disk6:isPlaying() do end
			disk6:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			love.mouse.setCursor(cursorRetro)
			InstructionsPage="rules"
			LoadLV100Text()
			LoadGameRules()
			love.timer.sleep( 0.3 )
		end
	end
	
	
	--controls--
	love.graphics.setColor(0,1,0,1)
	love.graphics.print("Controls:",How2SaveButton.x,How2SaveButton.y-50,0,1)
	love.graphics.setColor(1,1,1,1)
		hovered = isButtonHovered (How2SaveButton)
	drawButton (How2SaveButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="howtosave"
			disk6:play()
			while disk6:isPlaying() do end
			disk6:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			love.mouse.setCursor(cursorRetro)
			LoadLV100Text()
			LoadHow2Save()
			love.timer.sleep( 0.3 )
		end
	end
	
	
		hovered = isButtonHovered (ControllerLayoutButton)
	drawButton (ControllerLayoutButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="controllerLayout"
			disk6:play()
			while disk6:isPlaying() do end
			disk6:play()
			while disk6:isPlaying() do end
			disk6:play()
			love.mouse.setCursor(cursorRetro)
			LoadLV100Text()
			LoadControllerLayout()
			love.timer.sleep( 0.3 )
		end
	end
	
		hovered = isButtonHovered (KeyboardSchemaButton)
	drawButton (KeyboardSchemaButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="keyboardSchema"
			disk6:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadKeyboardSchema()
			love.timer.sleep( 0.3 )
		end
	end
	
	hovered = isButtonHovered (MouseSchemaButton)
	drawButton (MouseSchemaButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="mouseSchema"
			disk6:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadMouseSchema()
			love.timer.sleep( 0.3 )
		end
	end
	
	--graphics--
	love.graphics.setColor(0,1,0,1)
	love.graphics.print("Graphics:",SkinButton.x,SkinButton.y-50,0,1)
	love.graphics.setColor(1,1,1,1)
		hovered = isButtonHovered (SkinButton)
	drawButton (SkinButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="Skins"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadSkins()
			love.timer.sleep( 0.3 )
		end
	end
	
	
	
	hovered = isButtonHovered (HudButton)
	drawButton (HudButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="Hud"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadHud()
			love.timer.sleep( 0.3 )
		end
	end
	
	hovered = isButtonHovered (PerformanceButton)
	drawButton (PerformanceButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="Performance"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadPerformance()
			love.timer.sleep( 0.3 )
		end
	end
	
	
	--other--
	love.graphics.setColor(0,1,0,1)
	love.graphics.print("Advanced:",CrossSaveButton.x,CrossSaveButton.y-50,0,1)
	love.graphics.setColor(1,1,1,1)
	hovered = isButtonHovered (CrossSaveButton)
	drawButton (CrossSaveButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="CrossSave"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadCrossSave()
			love.timer.sleep( 0.3 )
		end
	end
	
	
	hovered = isButtonHovered (LevelEditorButton)
	drawButton (LevelEditorButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="LevelEditor"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadLevelEditorInstructions()
			love.timer.sleep( 0.3 )
		end
	end
	
		hovered = isButtonHovered (MusicPlayedrButton)
	drawButton (MusicPlayedrButton, hovered,extrastext)
	if hovered and (love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button)) then 
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			InstructionsPage="MusicPlayer"
			disk5:play()
			while disk6:isPlaying() do end
			disk5:play()
			while disk5:isPlaying() do end
			disk3:play()
			LoadLV100Text()
			LoadMusicPlayerInstructions()
			love.timer.sleep( 0.3 )
		end
	end
	
	DrawCurrentInstructionsPage()
	end
	
end
