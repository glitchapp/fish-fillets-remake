function LoadLV100Performancenl()

lvr1=[[------------------------------------------------------------------
				|		 Performance tools, profile presets, benchmark & fps lock		 |
------------------------------------------------------------------


The different graphic options have an impact on performance and battery consumption.
]]
lvr2=[[
If you experience performance issues you can change several options to improve performance.
]]
lvr3=[[You can lock the FPS or disable several advanced shaders such as chromatic aberration to improve performance.
]]
lvr4=[[Hint: There is a benchmark tool which provides you with the average framerate and suggest a preset if it detects framerates lower than 60FPS.
--]]
end
