function LoadLV100HowToSavenl()
lvr1=[[--------------------------------------------------------
				|		 How to save		 |
--------------------------------------------------------



You can save your progress at any time with the following procedures / shortcuts:]]
lvr2=[[
- By pressing the key F3 (load the game with key F2)
- By pressing the button save on the level selection menu
- Through the radial menu triggered with a game controller (left trigger)
Once there is a saved game the game will be automatically loaded every time you start the game.
]]
lvr3=[[
By saving the game not only the progress will be saved but also the game's options.
]]
lvr4=[[Hint: You can transfer your game progress to other game by copying the save folder of your game to another system.
]]
end
