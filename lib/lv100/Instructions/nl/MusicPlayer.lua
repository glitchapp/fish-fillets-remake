function LoadLV100MusicPlayernl()

lvr1=[[--------------------------------------------------------
					|		 Music player		 |
--------------------------------------------------------


The music player alows you to play the game's soundtrack with ease while showing info about the authors and beautiful effects and a spectrum bar.
]]
lvr2=[[
You can control playback and volume with the player's control.
]]
lvr4=[[
Hint: Some content needs to be unlocked by progressing through the game, if a track is not available you need to complete the level first.
]]
end
