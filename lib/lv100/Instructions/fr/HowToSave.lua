function LoadLV100HowToSavefr()
lvr1=[[--------------------------------------------------------
				|         Comment sauvegarder          |
--------------------------------------------------------



Vous pouvez sauvegarder votre progression à tout moment en suivant les procédures / raccourcis suivants :]]
lvr2=[[
- En appuyant sur la touche F3 (chargez le jeu avec la touche F2).
- En appuyant sur le bouton de sauvegarde dans le menu de sélection des niveaux.
- À travers le menu radial déclenché avec une manette de jeu (gâchette gauche).
Une fois qu'une partie est sauvegardée, le jeu se chargera automatiquement à chaque démarrage.
]]
lvr3=[[
En sauvegardant la partie, non seulement la progression sera enregistrée, mais aussi les options du jeu.
]]
lvr4=[[Astuce : Vous pouvez transférer votre progression de jeu vers un autre jeu en copiant le dossier de sauvegarde de votre jeu sur un autre système.
]]
end
