function LoadLV100Performancefr()

lvr1=[[------------------------------------------------------------------
				|  Outils de performance, préréglages de profil, benchmark et verrouillage des FPS  |
------------------------------------------------------------------


Les différentes options graphiques ont un impact sur les performances et la consommation de la batterie.
]]
lvr2=[[
Si vous rencontrez des problèmes de performances, vous pouvez changer plusieurs options pour améliorer les performances.
]]
lvr3=[[Vous pouvez verrouiller les FPS ou désactiver plusieurs shaders avancés tels que l'aberration chromatique pour améliorer les performances.
]]
lvr4=[[Astuce : Il existe un outil de benchmark qui vous donne le framerate moyen et suggère un préréglage s'il détecte des framerates inférieurs à 60 FPS.
--]]
end
