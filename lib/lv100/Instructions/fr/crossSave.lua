
function LoadLV100CrossSavefr()
lvr1=[[--------------------------------------------------------
				|             Transfert de sauvegarde            |
--------------------------------------------------------



Saviez-vous que vous pouvez transférer et continuer votre partie sauvegardée entre le jeu en 2D et le jeu en VR ?
]]

lvr2=[[
Utilisez la fonction d'exportation dans "Extras / Exporter la partie".
]]
lvr4=[[
Cela créera un fichier zip dans le dossier de sauvegarde de Love. Copiez, transférez et décompressez-le dans votre jeu cible.
]]

end
