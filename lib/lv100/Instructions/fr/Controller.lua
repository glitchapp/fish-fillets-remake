
function LoadLV100Controllerfr()
lvr1=[[--------------------------------------------------------
				|   Configuration de la manette    |
--------------------------------------------------------

Il existe deux configurations de manette de jeu disponibles :]]
lvr2=[[
- Par défaut : Vous contrôlez les deux poissons avec le joystick analogique gauche et la caméra avec le joystick analogique droit.
- Alternative : Le joystick analogique gauche contrôle le poisson bleu et le joystick analogique droit contrôle le poisson orange.
]]
lvr3=[[Il n'y a pas de contrôle de la caméra avec la configuration alternative.]]

lvr4=[[Pour contrôler le pointeur dans les menus, il vous suffit de déplacer le joystick analogique gauche.]]

lvr5=[[Astuce : Vous pouvez...]]
end
