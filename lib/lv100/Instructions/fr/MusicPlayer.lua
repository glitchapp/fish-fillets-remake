function LoadLV100MusicPlayerfr()

lvr1=[[--------------------------------------------------------
					|          Lecteur de musique          |
--------------------------------------------------------


Le lecteur de musique vous permet de jouer la bande sonore du jeu tout en affichant des informations sur les auteurs, des effets visuels et une barre de spectre.
]]
lvr2=[[
Vous pouvez contrôler la lecture et le volume avec les commandes du lecteur.
]]
lvr4=[[
Astuce : Certains contenus doivent être débloqués en progressant dans le jeu. Si une piste n'est pas disponible, vous devez d'abord terminer le niveau.
]]

end
