function LoadLV100Rulesfr()

lvr1=[[--------------------------------------------------------
				|                 Règles                 |
--------------------------------------------------------



Libérez le chemin vers la sortie en déplaçant les objets,
mais faites attention :]]
lvr2=[[- Si un objet tombe sur un poisson, la partie est terminée.
- Vous pouvez transférer des objets d'un poisson à un autre.
- Il existe des objets en acier (lourds) que seul le grand poisson peut déplacer.
- Vous pouvez pousser des objets tant qu'ils sont soutenus
  par un autre objet ou la carte dans la position suivante.]]
end
