
function LoadLV100LevelEditorfr()

lvr1=[[--------------------------------------------------------
					|         Éditeur de niveaux         |
--------------------------------------------------------


Fish Fillets Remake propose un éditeur de niveaux avec lequel vous pouvez facilement créer et partager de nouveaux niveaux.
]]
lvr2=[[
Les outils du côté droit de l'éditeur vous permettent d'exporter, de jouer et d'ouvrir le dossier de votre niveau personnalisé.
]]
lvr4=[[
Astuce : dans "Extras / Jouer un niveau personnalisé", vous pouvez charger un niveau personnalisé en le faisant simplement glisser dans la fenêtre du jeu.
]]

end
