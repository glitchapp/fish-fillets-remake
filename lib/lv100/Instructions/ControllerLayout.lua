function LoadControllerLayout()
CleanLV100Screen()


 
term:set_cursor_color(Terminal.schemes.basic[4])
term:frame("line", 1,1,80,25)
term:set_cursor_color(Terminal.schemes.basic[6])
term:print(30, 1, "Fish Fillets Remake")
--term:set_cursor_color(Terminal.schemes.basic[2])
--term:blit(12,2,lvt0)
term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,2,lvr1)
term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,15,lvr2)
term:set_cursor_color(Terminal.schemes.basic[3])
term:blit(12,20,lvr3)
term:set_cursor_color(Terminal.schemes.basic[6])
term:blit(12,21,lvr4)
term:set_cursor_color(Terminal.schemes.basic[5])
term:blit(12,22,lvr5)

term:set_cursor_color(Terminal.schemes.basic[6])
term:set_cursor_backcolor(Terminal.schemes.basic[0])
term:reverse_cursor(true)
term:blit(12,24,"Press a mouse key to return")

--ControllerLayoutScreen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/ControllerLayout.webp")))
ControllerLayoutScreen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/xboxgamepadDefault.webp")))
ControllerLayoutAlternativeScreen = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/xboxgamepadAlternative.webp")))

end

function DrawControllerLayoutScreenshots()
		love.graphics.setColor(0.5,0.5,0.5,1)
      	love.graphics.rectangle("fill",175,120,350,200)
      	love.graphics.rectangle("fill",575,120,350,200)
      	love.graphics.setColor(1,1,1,1)
      	love.graphics.draw(ControllerLayoutScreen,200,120,0,0.15,0.15)
      	love.graphics.draw(ControllerLayoutAlternativeScreen,600,120,0,0.15,0.15)
      	
      	
end
