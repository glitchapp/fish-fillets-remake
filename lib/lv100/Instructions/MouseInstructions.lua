function LoadMouseSchema()
CleanLV100Screen()
lvr1=[[--------------------------------------------------------
				|		 Mouse schema		 |
--------------------------------------------------------



]]
lvr2=[[

]]
lvr3=[[

]]
lvr4=[[
]]

term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,2,lvr1)
term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,15,lvr2)
term:set_cursor_color(Terminal.schemes.basic[3])
term:blit(12,20,lvr3)
term:set_cursor_color(Terminal.schemes.basic[5])
term:blit(12,22,lvr4)

term:set_cursor_color(Terminal.schemes.basic[6])
term:set_cursor_backcolor(Terminal.schemes.basic[0])
term:reverse_cursor(true)
term:blit(12,24,"Press a mouse key to return")

MouseSchemaWEBP = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/MouseSchema.webp")))

end

function DrawMouseSchema()
		
      	love.graphics.draw(MouseSchemaWEBP,400,150,0,0.1,0.1)
      	
end
