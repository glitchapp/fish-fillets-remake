function LoadLV100Performancede()

lvr1=[[------------------------------------------------------------------
				|    Leistungstools, Profilvoreinstellungen, Benchmark & FPS-Sperre    |
------------------------------------------------------------------


Die verschiedenen Grafikoptionen beeinflussen die Leistung und den Batterieverbrauch.
]]
lvr2=[[
Wenn du Leistungsprobleme hast, kannst du verschiedene Optionen ändern, um die Leistung zu verbessern.
]]
lvr3=[[Du kannst die FPS sperren oder mehrere fortgeschrittene Shader wie chromatische Aberration deaktivieren, um die Leistung zu verbessern.
]]
lvr4=[[Tipp: Es gibt ein Benchmark-Tool, das dir den Durchschnitts-FPS liefert und eine Voreinstellung vorschlägt, wenn Frameraten unter 60FPS erkannt werden.
--]]

end
