function LoadLV100HowToSavede()
lvr1=[[--------------------------------------------------------
				|         Wie man speichert         |
--------------------------------------------------------



Du kannst deinen Fortschritt jederzeit mit den folgenden Verfahren / Tastenkombinationen speichern:]]
lvr2=[[
- Durch Drücken der Taste F3 (Laden des Spiels mit der Taste F2)
- Durch Drücken der Taste "Speichern" im Levelauswahlmenü
- Über das radiale Menü, das mit einem Gamecontroller (linker Trigger) ausgelöst wird
Sobald ein Spiel gespeichert ist, wird es jedes Mal automatisch geladen, wenn du das Spiel startest.
]]
lvr3=[[
Durch das Speichern des Spiels werden nicht nur der Fortschritt, sondern auch die Spieleinstellungen gespeichert.
]]
lvr4=[[Tipp: Du kannst deinen Spielstand auf ein anderes System übertragen, indem du den Save-Ordner deines Spiels auf ein anderes System kopierst.
]]
end
