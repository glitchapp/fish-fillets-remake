
function LoadLV100LevelEditorde()

lvr1=[[--------------------------------------------------------
					|        Level-Editor        |
--------------------------------------------------------


Fish Fillets Remake enthält einen Level-Editor, mit dem du einfach neue Level erstellen und teilen kannst.
]]
lvr2=[[
Die Werkzeuge auf der rechten Seite des Editors ermöglichen es dir, deinen benutzerdefinierten Level zu exportieren, abzuspielen und den Ordner zu öffnen.
]]
lvr4=[[
Tipp: In "Extras / Benutzerdefiniertes Level spielen" kannst du ein benutzerdefiniertes Level laden, indem du es einfach in das Fenster des Spiels ziehst.
]]
end
