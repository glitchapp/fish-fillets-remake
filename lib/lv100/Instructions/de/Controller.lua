
function LoadLV100Controllerde()
lvr1=[[--------------------------------------------------------
				|    Controller-Layout    |
--------------------------------------------------------

Es gibt zwei verfügbare Controller-Layouts:]]
lvr2=[[
- Standard: Du steuerst beide Fische mit dem linken Analogstick und die Kamera mit dem rechten.
- Alternativ: Der linke Analogstick steuert den blauen Fisch und der rechte Analogstick steuert den orangefarbenen Fisch.
]]
lvr3=[[Mit dem alternativen Layout gibt es keine Kamerasteuerung.]]

lvr4=[[Um den Zeiger in den Menüs zu steuern, bewege einfach den linken Analogstick.]]

lvr5=[[Tipp: Du kannst...
]]
end
