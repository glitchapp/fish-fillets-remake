function LoadLV100MusicPlayerde()


lvr1=[[--------------------------------------------------------
					|         Musikspieler         |
--------------------------------------------------------


Der Musikspieler ermöglicht es dir, die Spielmusik einfach abzuspielen, während Informationen über die Autoren und schöne Effekte sowie eine Spektrum-Leiste angezeigt werden.
]]
lvr2=[[
Du kannst die Wiedergabe und die Lautstärke mit den Steuerelementen des Players steuern.
]]
lvr4=[[
Tipp: Einige Inhalte müssen durch Fortschreiten im Spiel freigeschaltet werden. Wenn ein Track nicht verfügbar ist, musst du das Level zuerst abschließen.
]]
end
