function LoadLV100Rulesde()


lvr1=[[--------------------------------------------------------
				|              Regeln               |
--------------------------------------------------------



Befreie den Weg zum Ausgang, indem du die Objekte bewegst,
aber sei vorsichtig:]]
lvr2=[[- Wenn ein Objekt auf einen Fisch fällt, ist das Spiel vorbei.
- Du kannst Objekte von einem Fisch auf einen anderen übertragen.
- Es gibt schwere (stählerne) Objekte, die nur der große Fisch bewegen kann.
- Du kannst Objekte schieben, solange sie von einem anderen Objekt oder der Karte in der nächsten Position gestützt werden.]]


end
