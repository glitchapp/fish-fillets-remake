
function LoadLV100LevelEditores()

lvr1=[[--------------------------------------------------------
					|        Editor de Niveles        |
--------------------------------------------------------


Fish fillets remake cuenta con un editor de niveles con el que puedes crear y compartir fácilmente nuevos niveles.
]]
lvr2=[[
Las herramientas en el lado derecho del editor te permiten exportar, jugar y abrir la carpeta de niveles personalizados.
]]
lvr4=[[
Consejo: en "extras / jugar nivel personalizado" puedes cargar un nivel personalizado simplemente arrastrándolo a la ventana del juego.
]]

end
