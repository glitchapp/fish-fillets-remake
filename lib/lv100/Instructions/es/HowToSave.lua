function LoadLV100HowToSavees()
lvr1=[[--------------------------------------------------------
				|         Cómo Guardar         |
--------------------------------------------------------



Puedes guardar tu progreso en cualquier momento con los siguientes procedimientos / atajos:]]
lvr2=[[
- Presionando la tecla F3 (cargar el juego con la tecla F2)
- Presionando el botón de guardar en el menú de selección de nivel
- A través del menú radial activado con un controlador de juego (gatillo izquierdo)
Una vez que haya un juego guardado, se cargará automáticamente cada vez que inicies el juego.
]]
lvr3=[[
Al guardar el juego, no solo se guardará el progreso sino también las opciones del juego.
]]
lvr4=[[Consejo: Puedes transferir tu progreso en el juego a otro juego copiando la carpeta de guardado de tu juego a otro sistema.
]]

end
