
function LoadLV100CrossSavees()
lvr1=[[--------------------------------------------------------
				|         Transferencia Cruzada        |
--------------------------------------------------------



¿Sabías que puedes transferir y continuar tu juego guardado entre el juego 2D y el juego VR?
]]

lvr2=[[
Utiliza la función de exportación en "extras / exportar juego".
]]
lvr4=[[
Creará un archivo zip en la carpeta de guardado de Love. Cópialo, transfierelo y descomprímelo en tu juego de destino.
]]

end
