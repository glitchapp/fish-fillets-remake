


function LoadLV100Ruleses()

lvr1=[[--------------------------------------------------------
				|              Reglas               |
--------------------------------------------------------



Libera el camino hacia la salida moviendo los objetos,
pero ten cuidado:]]
lvr2=[[- Si un objeto cae sobre un pez, el juego se acaba.
- Puedes transferir objetos de un pez a otro
- Hay objetos de acero (pesados) que solo el pez grande puede mover.
- Puedes empujar objetos siempre y cuando estén soportados
  por otro objeto o el mapa en la siguiente posición.]]

end
