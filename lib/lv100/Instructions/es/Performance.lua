function LoadLV100Performancees()

lvr1=[[------------------------------------------------------------------
				|    Herramientas de Rendimiento, Perfiles, Benchmark y Bloqueo de FPS    |
------------------------------------------------------------------


Las diferentes opciones gráficas tienen un impacto en el rendimiento y el consumo de batería.
]]
lvr2=[[
Si experimentas problemas de rendimiento, puedes cambiar varias opciones para mejorar el rendimiento.
]]
lvr3=[[Puedes bloquear los FPS o desactivar varios shaders avanzados como la aberración cromática para mejorar el rendimiento.
]]
lvr4=[[Consejo: Hay una herramienta de benchmark que te proporciona el promedio de FPS y sugiere un perfil si detecta FPS inferiores a 60.
--]]
end
