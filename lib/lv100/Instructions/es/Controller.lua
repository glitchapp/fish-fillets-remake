
function LoadLV100Controlleres()
lvr1=[[--------------------------------------------------------
				|    Distribución del Controlador    |
--------------------------------------------------------

Hay dos distribuciones de controlador disponibles:]]
lvr2=[[
- Predeterminada: Controlas ambos peces con el stick analógico izquierdo, y la cámara con el derecho.
- Alternativa: El stick analógico izquierdo controla el pez azul y el derecho controla el pez naranja.
]]
lvr3=[[No hay control de cámara con la distribución alternativa.]]

lvr4=[[Para controlar el puntero en los menús, simplemente mueve el stick analógico izquierdo.]]

lvr5=[[Consejo: Puedes...]]
end
