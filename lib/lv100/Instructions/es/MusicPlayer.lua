function LoadLV100MusicPlayeres()

lvr1=[[--------------------------------------------------------
					|         Reproductor de Música         |
--------------------------------------------------------


El reproductor de música te permite reproducir la banda sonora del juego fácilmente mientras muestra información sobre los autores y efectos visuales y una barra de espectro.
]]
lvr2=[[
Puedes controlar la reproducción y el volumen con los controles del reproductor.
]]
lvr4=[[
Consejo: Algunos contenidos necesitan ser desbloqueados al progresar en el juego, si una pista no está disponible, primero debes completar el nivel.
]]
end
