
function LoadLV100Controller()
lvr1=[[--------------------------------------------------------
				|		 Controller Layout		 |
--------------------------------------------------------

There are two available game controller layouts:]]
lvr2=[[
- Default: You control both fish with the left analog stick,camera with the right one.
- Alternative: The left analog stick controls the blue fish and the right analog stick controls the orange fish.
]]
lvr3=[[There's no camera control with the alternative layout.]]

lvr4=[[To control the pointer on menus simply move the left analog stick.]]

lvr5=[[Hint: You can save after selecting your prefered layout to keep changes.]]
end
