function CleanLV100Screen()

	term:set_cursor_color(Terminal.schemes.basic[0])
	
	for i = 1,25,1 do
		for z=1,80,1 do
			term:print(z, i, " ")
		end
	end
	term:set_cursor_backcolor(Terminal.schemes.basic[0])
end

function LoadGameRules()

						in1 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/1.webp")))
						in2  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/2.webp")))
						in3  = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/3.webp")))
						in4 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/4.webp")))
						in5 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/5.webp")))
						in6 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/6.webp")))
						in7 = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/instructions/7.webp")))


term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,2,lvr1)
term:set_cursor_color(Terminal.schemes.basic[2])
term:blit(12,15,lvr2)

term:set_cursor_color(Terminal.schemes.basic[6])
term:set_cursor_backcolor(Terminal.schemes.basic[0])
term:reverse_cursor(true)
term:blit(12,22,"Press a mouse key to return")

term:reverse_cursor(false)
term:set_cursor_color(Terminal.schemes.basic[7])
term:print(55, 24, "By Eiyeron")

term:set_cursor_color(Terminal.schemes.basic[6])
term:print(4, 24, "-A terminal-ish library for Love2D--------------")

end

function DrawGameRulesScreenshots()
		love.graphics.draw(in3,0,150,0,1,1)
      	love.graphics.draw(in1,250,150,0,1,1)
      	love.graphics.draw(in2,500,150,0,1,1)
      	love.graphics.draw(in4,750,150,0,1,1)
      	love.graphics.draw(in5,1000,150,0,1,1)
      	love.graphics.draw(in6,1250,150,0,1,1)
      	love.graphics.draw(in7,1500,150,0,1,1)
end
