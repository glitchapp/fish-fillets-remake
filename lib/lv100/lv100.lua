
lv100cursorx=41
asciiart1=
[[ ___      __   __         __
 .0kxxxxxxxxxxxxxxxxxxxOk.          
 ;M.                   xN0O'        
 ;M.                   xK ,O0;      
 ;M.                   xK   .kK:    
 ;M.                   xX.....:KXc  
 ;M.        .          ;olllllxllXk 
 ;M.                             OO 
 ;M..KNl. .OMo        .NX:  :KW, OO 		--------------------------------
 ;M.  cXWKMO'           oNNNWx.  OO 		Lovr is not found!  
 ;M.  ;0MXMk.           cXNNNo.  OO 		--------------------------------
 ;M..KWd. ,0Md        .NNl  :XWc OO 
 ;M.  .     .          .      .  OO 		you need it to play lovr games
 ;M.                             OO 
 ;M. ..........................  OO 
 ;M..OOOKMX0MNOWMOOOOOOOOOOOOOOd OO 
 ;M.     XK.MO'Mx                OO 
 ;M.     .KNMWWx                 OO 
 ;M.       .,'                   OO 
 ;M.                             OO 
 'Kkxxxxxxxxxxxxxxxxxxxxxxxxxxxxx0o 
]]

function lv100load()
effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.crt).chain(moonshine.effects.glow).chain(moonshine.effects.filmgrain)
	effect.enable("scanlines","crt","glow","filmgrain")
	effect.disable("godsray")

		effect.scanlines.opacity=0.6
		effect.glow.min_luma = 0.2
		effect.filmgrain.size=1.5
		effect.filmgrain.opacity=2
--effect.godsray.samples=3
font = love.graphics.newFont("../assets/fonts/x14y24pxHeadUpDaisy.ttf", 24) -- Thanks @hicchicc for the font
term = Terminal(14*80, (font:getHeight()-4)*25, font, nil, font:getHeight()-4)
term:hide_cursor()



term:set_cursor_color(Terminal.schemes.basic[4])
term:frame("line", 1,1,80,25)

term:set_cursor_color(Terminal.schemes.basic[3])
term:blit(3,2,asciiart1)

term:set_cursor_color(Terminal.schemes.basic[7])
term:print(41, 16, "do you wish to download lovr now?")

term:set_cursor_color(Terminal.schemes.basic[6])
--term:reverse_cursor(45, 16,"fff")
term:print(lv100cursorx, 18, "☛ ")
term:print(43, 18, "Yes /  No")
term:print(41, 24, "Confirm selection with -c-")
end
function love.keypressed(key, scancode, isrepeat)
 --terminal selection
    	if key == 'left' then term:print(48, 18, " ")
			lv100cursorx=41 term:print(lv100cursorx, 18, "☛ ")
			lv100selection="yes"
		elseif key == 'right' then term:print(41, 18, " ")
			lv100cursorx=48 term:print(lv100cursorx, 18, "☛ ")
			lv100selection="no"
		elseif key=="y" then downloadlovr() lovrfound="yes"
		elseif key=="c" and lv100selection=="yes" then sh.command('qterminal -e ./runvr2')
		elseif key=="c" and lv100selection=="no" then 
		end
	end
function lv100draw()
    love.graphics.clear()
    effect(function()
        love.graphics.push()
        love.graphics.scale(love.graphics.getWidth()/term.canvas:getWidth(), love.graphics.getHeight()/term.canvas:getHeight())
        love.graphics.rectangle("fill", 0,0,term.canvas:getWidth(), term.canvas:getHeight())
        term:draw()
        love.graphics.pop()
    end)
end

