

local moonshine = require 'assets/shaders/moonshine'

function loadinstructions()

IndexLeftMenuTriggered=false

Terminal = require "lib/lv100/terminal"
require 'lib/lv100/Instructions/Index'
LoadInstructionsIndex()


MonitorFrame = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/assets/interface/MonitorFrame_4096.webp")))
 vgmenuhighlight = love.audio.newSource( "/externalassets/sounds/vgmenuhighlight.ogg","static" )

effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.crt).chain(moonshine.effects.glow).chain(moonshine.effects.filmgrain)
effect.scanlines.opacity=0.6
effect.glow.min_luma = 0.2

effect.filmgrain.size=1.5
effect.filmgrain.opacity=2
--effect.godsray.samples=3
local font = love.graphics.newFont("assets/fonts/x14y24pxHeadUpDaisy.ttf", 24) -- Thanks @hicchicc for the font
term = Terminal(14*80, (font:getHeight()-4)*25, font, nil, font:getHeight()-4)
term:hide_cursor()
local cursorx=41
local selection="yes"

LoadGameRules()
--LoadHow2Save()


end

function instructionsupdate(dt)
    term:update(dt)
    love.graphics.setColor(1,1,1,1)
    if love.mouse.isDown(1) and not (IndexLeftMenuTriggered==true) then
		if nLevel==1 and (level==nil) then
			gamestatus="levelselection"
		else gamestatus="game" shader2=true
		end
		love.mouse.setCursor(cursor)
    end
    keyinputTernminalGuide(key, scancode, isrepeat)
    HideIndexMenu()
    if InstructionsPage=="Skins" then updateSkins(dt) end
end

function HideIndexMenu()
	
	local mx, my = love.mouse.getPosition()
		
		if res=="1440p" or res=="4k" then
			
			-- if mouse hover the left side of the screen
			if mx <300 or (joystick and emulatedmousexscale>200) then
				IndexLeftMenuTriggered=true
			else IndexLeftMenuTriggered=false
			end
						
		elseif res=="1080p" then
		
		-- if mouse hover the left side of the screen
			if mx <200 or (joystick and emulatedmousexscale>2000) then
				IndexLeftMenuTriggered=true
			else IndexLeftMenuTriggered=false
			end
	
		elseif res=="720p" then
		
		-- if mouse hover the left side of the screen
			if mx <150 or (joystick and emulatedmousexscale>2000) then
				IndexLeftMenuTriggered=true
			else IndexLeftMenuTriggered=false
			end
		end
	
end


returnButton = {
	text = "Return",
	x = 69,
	y = 6,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishsmall,
}

--[[
local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x*23, -40+button.y*40
	local w, h = 120+width*sx, height*sy+40
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end
--]]

local function drawButton (button, hovered)
	
	
	if hovered then
		term:set_cursor_color(Terminal.schemes.basic[3])
		love.graphics.rectangle ('line', button.x*23, -40+button.y*40, button.w+80, button.h-30)
	else
		term:set_cursor_color(Terminal.schemes.basic[1])
	end
	term:set_cursor_color(Terminal.schemes.basic[2])
	term:print(button.x,button.y,button.text)
end

function instructionsdraw(dt)
		if res=="4k" then love.graphics.draw(MonitorFrame,0,0,0,1,1)
    elseif res=="1440p" then love.graphics.draw(MonitorFrame,0,-35,0,0.67,0.65)
    elseif res=="1080p" then love.graphics.draw(MonitorFrame,0,0,0,0.5,0.5)
    end
    
    effect(function()
    
        love.graphics.push()
        
        love.graphics.scale(love.graphics.getWidth()/term.canvas:getWidth(), love.graphics.getHeight()/term.canvas:getHeight())
        love.graphics.setColor(0,0,0,1)
        love.graphics.rectangle("fill", 0,0,term.canvas:getWidth(), term.canvas:getHeight())
        love.graphics.setColor(1,1,1,1)
        term:draw()
        	if InstructionsPage=="rules" 			then DrawGameRulesScreenshots()
	elseif InstructionsPage=="howtosave" 			then DrawHowToSaveScreenshots()
	elseif InstructionsPage=="controllerLayout" 	then DrawControllerLayoutScreenshots()
	elseif InstructionsPage=="keyboardSchema" 		then DrawKeyboardSchema()
	elseif InstructionsPage=="mouseSchema"	 		then DrawMouseSchema()
	elseif InstructionsPage=="CrossSave"	 		then DrawCrossSaveScreenshots()
	elseif InstructionsPage=="Skins"	 			then DrawSkinsScreenshots()
	elseif InstructionsPage=="Hud"	 				then DrawHudScreenshots()
	elseif InstructionsPage=="Performance"	 		then DrawPerformanceScreenshots()
	elseif InstructionsPage=="LevelEditor"	 		then DrawLevelEditorScreenshots()
	elseif InstructionsPage=="MusicPlayer"	 		then DrawMusicPlayerScreenshots()
	end
        love.graphics.pop()
        --term:print(5, 22, "Page:" .. inspage)
                
	
 
 DrawInstructionsIndex()
 
    end)
end

