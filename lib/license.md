
Copyright (C) 2022 Glitchapp

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

The license text can be found in GPL-2.0.txt.

---

This directory contains most of the external libraries used in the game. Some of them have been only tested and not fully implemented or discarded.

Below are libraries organized by license type:

GNU General Public License (GPL) 2.0:
1. blocks: Tetris logic for the mini tetris game

GPL-2.0 or later:
1. netcontrols: A Lua library for creating networked multiplayer games.
2. record: A Lua library for recording and playing back player input in Love2D games.

MIT License:
1. bitser.lua: Another serialization library for Lua that supports both binary and JSON formats.
2. fpsgraph: A debugging tool that displays a graph of the game's frame rate over time.
3. loveframes: A GUI library for creating user interfaces in Lua games.
4. love-loader: A Lua library for managing the loading of assets in Love2D games.
5. moonvox: LuaJIT bindings for SunVox.
6. ShakeDetector: A Lua module for detecting device shake events in mobile games.
7. slab: A Lua library for creating user interfaces in Love2D games using a simple layout system.
8. talkies: A Lua library for creating in-game dialogue boxes and text-to-speech functionality.
9. tesound.lua: A Lua library for playing and manipulating audio in Love2D games.
10. zip: ZIP Library for lövelove-boids
11. splahes: A collection of splash screens for LÖVE.
12. love-boids:  a Reynolds'boids algorithm implementation on LÖVE provide realistic-looking representations of schools of fish .

ISC License:
1. binser.lua: A Lua serialization library that can convert Lua values into a binary format for more efficient data storage or network transmission.

Zlib License:
1. loveframes: A GUI library for creating user interfaces in Lua games.
2. splahes: A collection of splash screens for LÖVE.

The Unlicense:
1. resolution_solution.lua: A Lua module for managing the game's resolution and aspect ratio.
2. save-tsv-01: A Lua module for saving game data in a tab-separated values format.

CC0 "No Rights Reserved":
1. multiresolution: A Love2D module for automatically scaling the game's graphics to fit different screen resolutions.
2. save-tsv-01: A Lua module for saving game data in a tab-separated values format.



Repositories and url to licenses: 

1. binser.lua: A Lua serialization library that can convert Lua values into a binary format for more efficient data storage or network transmission.
   - Repository: https://github.com/bakpakin/binser
   - MIT License: https://github.com/bakpakin/binser/blob/master/LICENSE.md

2. bitser.lua: Another serialization library for Lua that supports both binary and JSON formats.
   - Repository: https://github.com/gvx/bitser
   - ISC License: https://github.com/gvx/bitser/blob/master/LICENSE.txt

3. blocks: Tetris logic for the mini tetris game
	- repository: https://simplegametutorials.github.io/love/blocks/
   - License: GPL 2.0 or later

4. equalizer: A Lua library for applying audio equalization filters to audio streams.
   - Licensed under the same terms as Lua itself.
   
5. fpsgraph: A debugging tool that displays a graph of the game's frame rate over time.
   - License: MIT License

6. hump: A collection of common utility functions for game development in Lua.
   - Repository: https://github.com/vrld/hump
   - MIT License

7. lady: A Lua library for handling MIDI input and output.
   - MIT License

8. love-bpm: A Lua library for handling beat and tempo detection in music.
   - Repository: https://github.com/rxi/lovebpm
   - MIT License: https://github.com/rxi/lovebpm/blob/master/LICENSE

9. loveframes: A GUI library for creating user interfaces in Lua games.
   - Repository: https://github.com/cyborgize/LoveFrames
   - Zlib License: https://github.com/cyborgize/LoveFrames/blob/master/license.txt
   - MIT License: https://github.com/kikito/tween.lua

10. love-loader: A Lua library for managing the loading of assets in Love2D games.
    - Repository: https://github.com/kikito/love-loader
    - MIT License: https://github.com/kikito/love-loader/blob/master/MIT-LICENSE.txt

11. love-webp: A Love2D module for loading and displaying images in the WebP format.
    - Repository: https://github.com/ImagicTheCat/love-webp
    - MIT License: https://github.com/ImagicTheCat/love-webp/blob/master/LICENSE

12. lv100: A Love2D library to make terminal-like stuff.
    - Repository: https://github.com/Eiyeron/LV-100

13. moonvox: LuaJIT bindings for SunVox.
    - Repository: https://github.com/9912/moonvox
    - MIT License: https://github.com/9912/moonvox/blob/master/LICENSE

14. multiresolution: A Love2D module for automatically scaling the game's graphics to fit different screen resolutions.
    - License: CC0 (Creative Commons license) (c) darkfrei, 2021

15. netcontrols: A Lua library for creating networked multiplayer games.
    - License: GPL 2.0 or later

16. record: A Lua library for recording and playing back player input in Love2D games.
    - License: GPL-2.0 or later

17. resolution_solution.lua: A Lua module for managing the game's resolution and aspect ratio.
    - Repository: https://github.com/Vovkiv/resolution_solution
    - The Unlicense: https://github.com/Vovkiv/resolution_solution/blob/main/LICENSE

18. save-tsv-01: A Lua module for saving game data in a tab-separated values format.
    - License: CC0 "No Rights Reserved" / darkfrei 2022

19. ShakeDetector: A Lua module for detecting device shake events in mobile games.
    - MIT License: https://github.com/alexgibson/shake.js/blob/master/shake.js

20. sick.lua: A Lua module for testing Lua code using a simple BDD-style syntax.

21. slab: A Lua library for creating user interfaces in Love2D games using a simple layout system.
    - Repository: https://github.com/flamendless/Slab/blob/master/LICENSE
    - MIT License: https://github.com/flamendless/Slab/blob/master/LICENSE

22. tablesave10.lua: A Lua module for saving and loading Lua tables to and from disk.

23. talkies: A Lua library for creating in-game dialogue boxes and text-to-speech functionality.
    - Repository: https://github.com/tanema/talkies
    - MIT License: https://github.com/tanema/talkies/blob/master/LICENSE

24. tesound.lua and tesoundLICENSE.md: A Lua library for playing and manipulating audio in Love2D games.
    - https://love2d.org/wiki/TEsound
    - ZLIB License
25 zip: ZIP Library for löve
	- https://github.com/matiasah/zip
	- MIT license

26 Splahes:  A collection of splash screens for LÖVE.
	- https://github.com/love2d-community/splashes
	- ZLIB License

27 Love-Boids: 
    - https://github.com/Jehadel/love-boids
    -GPL 3.0 License (https://github.com/Jehadel/love-boids/blob/main/LICENSE)