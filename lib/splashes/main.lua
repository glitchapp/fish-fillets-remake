--[[
Copyright (c) 2016 love-community members (as per git commits in repository above)

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

The official LÖVE assets are licensed under the zlib license aswell:
>Copyright (c) 2006-2016 LOVE Development Team
>
>This software is provided 'as-is', without any express or implied
>warranty. In no event will the authors be held liable for any damages
>arising from the use of this software.
>
>Permission is granted to anyone to use this software for any purpose,
>including commercial applications, and to alter it and redistribute it
>freely, subject to the following restrictions:
>
>1. The origin of this software must not be misrepresented; you must not
>   claim that you wrote the original software. If you use this software
>   in a product, an acknowledgement in the product documentation would be
>   appreciated but is not required.
>2. Altered source versions must be plainly marked as such, and must not be
>   misrepresented as being the original software.
>3. This notice may not be removed or altered from any source distribution.
--]]

local splashes = {
  ["o-ten-one"]           = {module="o-ten-one"},
  ["o-ten-one: lighten"]  = {module="o-ten-one", {fill="lighten"}},
  ["o-ten-one: rain"]     = {module="o-ten-one", {fill="rain"}},
  ["o-ten-one: black"]    = {module="o-ten-one", {background={0, 0, 0}}},
}

local current, splash

local function next_splash()
  current = next(splashes, current) or next(splashes)
  splash = splashes[current]()
  splash.onDone = next_splash
end

function love.load()
  for name, entry in pairs(splashes) do
    entry.module = require(entry.module)
    splashes[name] = function ()
      return entry.module(unpack(entry))
    end
  end

  next_splash()
end

function love.update(dt)
  splash:update(dt)
end

function love.draw()
  splash:draw()

  -- draw with both colors so its definetely visible
  love.graphics.setColor(1, 1, 1)
  love.graphics.print(current, 10, 10)

  love.graphics.setColor(0, 0, 0)
  love.graphics.print(current, 10, love.graphics.getHeight() - 20)
end

function love.keypressed(key)
  if key == "escape" then
    love.event.push("quit")
  end

  splash:skip()
end
