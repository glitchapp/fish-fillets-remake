function shakeload()
    --shakeDetector = require "shakeDetector"
    shakeDetector = require ('/lib/ShakeDetector/shakeDetector')  -- shake detector
    joysticks = love.joystick.getJoysticks()
    joystick = joysticks[#joysticks]    
end

function shakeupdate(dt)    
    xshake, yshake, zshake = joystick:getAxes()
    shakeDetector:update(dt, xshake, yshake, zshake)
    --shakesCount = shakeDetector.count
end

function shakedraw()
    love.graphics.print(shakeDetector.count)
end
