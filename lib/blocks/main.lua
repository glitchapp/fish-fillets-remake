
--[[
 Original code: https://simplegametutorials.github.io/love/blocks/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

--]]


function blocksload()
    --love.graphics.setBackgroundColor(255, 255, 255)
	
	block1=love.graphics.newImage("lib/blocks/assets/box.png")
	--[[block2=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
	block3=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
	block4=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
	block5=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
	block6=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
	block7=love.graphics.newImage("externalassets/objects/level1/cushion1.png")
    --]]
    pieceStructures = {
        {
            {
                {' ', ' ', ' ', ' '},
                {'i', 'i', 'i', 'i'},
                {' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 'i', ' ', ' '},
                {' ', 'i', ' ', ' '},
                {' ', 'i', ' ', ' '},
                {' ', 'i', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {' ', 'o', 'o', ' '},
                {' ', 'o', 'o', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {'j', 'j', 'j', ' '},
                {' ', ' ', 'j', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 'j', ' ', ' '},
                {' ', 'j', ' ', ' '},
                {'j', 'j', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {'j', ' ', ' ', ' '},
                {'j', 'j', 'j', ' '},
                {' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 'j', 'j', ' '},
                {' ', 'j', ' ', ' '},
                {' ', 'j', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {'l', 'l', 'l', ' '},
                {'l', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 'l', ' ', ' '},
                {' ', 'l', ' ', ' '},
                {' ', 'l', 'l', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', ' ', 'l', ' '},
                {'l', 'l', 'l', ' '},
                {' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {'l', 'l', ' ', ' '},
                {' ', 'l', ' ', ' '},
                {' ', 'l', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {'t', 't', 't', ' '},
                {' ', 't', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 't', ' ', ' '},
                {' ', 't', 't', ' '},
                {' ', 't', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 't', ' ', ' '},
                {'t', 't', 't', ' '},
                {' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 't', ' ', ' '},
                {'t', 't', ' ', ' '},
                {' ', 't', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {' ', 's', 's', ' '},
                {'s', 's', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {'s', ' ', ' ', ' '},
                {'s', 's', ' ', ' '},
                {' ', 's', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
        {
            {
                {' ', ' ', ' ', ' '},
                {'z', 'z', ' ', ' '},
                {' ', 'z', 'z', ' '},
                {' ', ' ', ' ', ' '},
            },
            {
                {' ', 'z', ' ', ' '},
                {'z', 'z', ' ', ' '},
                {'z', ' ', ' ', ' '},
                {' ', ' ', ' ', ' '},
            },
        },
    }

    gridXCount = 10
    gridYCount = 18

    pieceXCount = 4
    pieceYCount = 4

    tetristimerLimit = 0.5

    function canPieceMove(testX, testY, testRotation)
        for y = 1, pieceYCount do
            for x = 1, pieceXCount do
                local testBlockX = testX + x
                local testBlockY = testY + y

                if pieceStructures[pieceType][testRotation][y][x] ~= ' ' and (
                    testBlockX < 1
                    or testBlockX > gridXCount
                    or testBlockY > gridYCount
                    or inert[testBlockY][testBlockX] ~= ' '
                ) then
                    return false
                end
            end
        end

        return true
    end

    function newSequence()
        sequence = {}
        for pieceTypeIndex = 1, #pieceStructures do
            local position = love.math.random(#sequence + 1)
            table.insert(
                sequence,
                position,
                pieceTypeIndex
            )
        end
    end

    function newPiece()
        pieceX = 3
        pieceY = 0
        pieceRotation = 1
        pieceType = table.remove(sequence)

        if #sequence == 0 then
            newSequence()
        end
    end

    function reset()
        inert = {}
        for y = 1, gridYCount do
            inert[y] = {}
            for x = 1, gridXCount do
                inert[y][x] = ' '
            end
        end

        newSequence()
        newPiece()

        tetristimer = 0
    end

    reset()
end

function blocksupdate(dt)
	timer=timer + dt
    tetristimer = tetristimer + dt
    
    timer2=timer2 +1*dt
	if timer2>10 then love.audio.play( undermotion )
		timer2=0
	end
    
    if tetristimer >= tetristimerLimit then
        tetristimer = 0

        local testY = pieceY + 1
        if canPieceMove(pieceX, testY, pieceRotation) then
            pieceY = testY
        else
            -- Add piece to inert
            for y = 1, pieceYCount do
                for x = 1, pieceXCount do
                    local block =
                        pieceStructures[pieceType][pieceRotation][y][x]
                    if block ~= ' ' then
                        inert[pieceY + y][pieceX + x] = block
                    end
                end
            end

            -- Find complete rows
            for y = 1, gridYCount do
                local complete = true
                for x = 1, gridXCount do
                    if inert[y][x] == ' ' then
                        complete = false
                        break
                    end
                end

                if complete then
                    for removeY = y, 2, -1 do
                        for removeX = 1, gridXCount do
                            inert[removeY][removeX] = inert[removeY - 1][removeX]
                        end
                    end
				screenshake=true
				bubbles:play()
                    for removeX = 1, gridXCount do
                        inert[1][removeX] = ' '
                    end
                end
            end
			
			impactgolf:play()
            newPiece()

            if not canPieceMove(pieceX, pieceY, pieceRotation) then
                reset()
            end
        end
    end
end



function blocksdraw()
--print (timerLimit)
--print (timer)
--print(pieceType)
love.graphics.setShader(shader)
    local function drawBlock(block, x, y)
        local colors = {
            --[' '] = {.87, .87, .87},
            [' '] = {.7, .7, .7,0.3},
            i = {.47, .76, .94},
            j = {.93, .91, .42},
            l = {.49, .85, .76},
            o = {.92, .69, .47},
            s = {.83, .54, .93},
            t = {.97, .58, .77},
            z = {.66, .83, .46},
            preview = {.75, .75, .75},
        }
        local color = colors[block]
        love.graphics.setColor(color)

        local blockSize = 40
        local blockDrawSize = blockSize - 1
        --love.graphics.rectangle('fill',(x - 1) * blockSize,(y - 1) * blockSize,blockDrawSize,blockDrawSize)
        
			--[[if pieceType==1 then love.graphics.draw (block1, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==2 then love.graphics.draw (block2, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==3 then love.graphics.draw (block3, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==4 then love.graphics.draw (block4, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==5 then love.graphics.draw (block5, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==6 then love.graphics.draw (block6, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        elseif pieceType==7 then love.graphics.draw (block7, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
        end--]]
        love.graphics.draw (block1, (x - 1) * blockSize,(y - 1) * blockSize,0,blockDrawSize/30)
    end

    local offsetX = 18
    local offsetY = 5

    for y = 1, gridYCount do
        for x = 1, gridXCount do
            drawBlock(inert[y][x], x + offsetX, y + offsetY)
        end
    end

    for y = 1, pieceYCount do
        for x = 1, pieceXCount do
            local block = pieceStructures[pieceType][pieceRotation][y][x]
            if block ~= ' ' then
                drawBlock(block, x + pieceX + offsetX, y + pieceY + offsetY)
            end
        end
    end

    for y = 1, pieceYCount do
        for x = 1, pieceXCount do
            local block = pieceStructures[sequence[#sequence]][1][y][x]
            if block ~= ' ' then
                drawBlock('preview', x + 5, y + 1)
            end
        end
    end
    love.graphics.setShader()
end
