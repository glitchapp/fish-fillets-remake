local Talkies = require("lib/talkies/talkies")

local Obey = {}
local avatar;
local blop
--require ("game/dialogs/l1en")

function Obey.sayHello()
  blop = love.audio.newSource("lib/talkies/example/assets/sfx/talk.wav", "static")
  avatar = love.graphics.newImage("lib/talkies/example/assets/fishtalk.png")
  avatar2 = love.graphics.newImage("lib/talkies/example/assets/fishtalk2.png")

  Talkies.say( "Big fish",
    { "Hello and welcome to this tutorial, press any key to keep reading",
      
      "You can always stop this tutorial pressing the key -c-",
      "You can always start the tutorial by pressing the key -t-",
      
      "There are 3 of the original levels already implemented!",
      "You can skip levels by pressing the key enter",
      "but not in the final game! this is just for testing!",
      
      "The goal of the game is to free the path to the exit",
      "To do so, you can push the objects and rearrange them",
	  "you can push and rise objects as long as their path is free",
      "but be careful! the fish are very fragile, don't let objects fall over them!",
      "if you get stuck you can always restart the level",
      "I can push steel and heavy objects, they are outlined with blue color",
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })
  Talkies.say( "Small fish",
    {
      "I can not push heavy objects, only the lightweight outlined with green color",
      "but I can fit in smaller places",
      "You have to combine our abilities to solve the level!"
    },
    {
      image=avatar2,
      talkSound=blop,
      typedNotTalked=true,
    })  
      Talkies.say("Big fish",
    {
     "you can test the debug mode by pressing the key -d-",
      "You can access more options in debug mode, check the keys by pressing -h- to show help",
      "If you don't want to see this tutorial again please set talkies to false in main.lua, you can also set the variable debug to true to always start in debug mode",
      "That's all by now, have fun! please press the key -c- now to close this tutorial",
      "You need to press the key -c- now",
      "You need to press the key -c- now",
      "You need to press the key -c- now",
    },
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=true,
    })  
   
end




function Obey.sayGoodbye()
  Talkies.say(
    "Goodbye",
    "See ya around!",
    {
      image=avatar,
      talkSound=blop,
      typedNotTalked=false,
      oncomplete=function() rand() end,
      titleColor = {1, 0, 0}
    }
  )
end

return Obey
