-- Author: richardperkins 
-- Source: https://love2d.org/forums/viewtopic.php?p=160350&hilit=setKeyRepeat+gamepad#p160350


KeyMap = require("lib/keymap/KeyMap")

buttons = {}

function loadkeymap()

	KeyMap:addMapping({"up","down","left","right","fire"})
	
	KeyMap:bind( "up" , {"joy" , "dpup" , "up" } )
	
	KeyMap:bind( "down" , {"joy" , "dpdown" , "down" } )
	
	KeyMap:bind( "left" , {"joy" , "dpleft" , "left" } )
	
	KeyMap:bind( "right" , {"joy" , "dpright" , "right" } )
	
	KeyMap:bind( "up" , {"key", "w", "up"} )
	KeyMap:bind( "down" , {"key", "s", "down"} )
	KeyMap:bind( "left" , {"key", "a", "left"} )
	KeyMap:bind( "right" , {"key", "d", "right"} )
	KeyMap:bind( "fire" , {"key", "space"} )
	--KeyMap:bind( "fire" , {"joy" , "a" } )
	screenWidth = love.graphics.getWidth()
	screenHeight = love.graphics.getHeight()
	
	local num = 1
	for i , k in pairs( KeyMap:getMapping() ) do
	
		table.insert( buttons , { x = (screenWidth - 100)-500, y = (num * 20)+400 , w = 90 , h = 15 , id = i })
		
		num = num + 1
	
	end
	
end

function keymapupdate(dt)

	KeyMap:update(dt)
	joystickkeymap(dt)	
	
end


function joystickkeymap(dt)
	 
	-- Thumbstick support
	if gamestatus=="game" and not (pauseisactive==true) then
		if KeyMap:isHeld("left") or KeyMap:isHeld("right") or KeyMap:isHeld("up") or KeyMap:isHeld("down")
		and not (fish1status=="dead") and not (fish2status=="dead")
		then
			if nLevel==37 and turtlestatus=="telepatic" and joystick:isGamepad() and movementTimer >= movementDelay then
					dx=math.random(-1,1) dy=math.random(-1,1)	pb:mainMoving (dx, dy, dt)
			end 
		end
		
		if KeyMap:isDown("fire") then
			love.timer.sleep(0.3)
			pb:switchAgent ()
		end
		if not (radialmenu) and not (fish1status=="dead") and not (fish2status=="dead") then
			
			if KeyMap:isHeld("left") then
				dx = -1
				dy = 0
			elseif KeyMap:isHeld("right") then
				dx = 1
				dy = 0
			elseif  KeyMap:isHeld("up") then
				dx = 0
				dy = -1
			elseif KeyMap:isHeld("down") then
				dx = 0
				dy = 1
			else
				dx=0 dy=0
			end
			if movementTimer >= movementDelay then
						steps=steps+1 --add steps to the movement
						movementTimer = 0
						if not (fish1status=="dead") and not (fish2status=="dead") then
							pb:mainMoving (dx, dy, dt)
						end
			end
		end
				
	elseif gamestatus=="tetris" then
		if KeyMap:isHeld("left") 
		or KeyMap:isHeld("right")
		or KeyMap:isHeld("up")
		or KeyMap:isHeld("down")
		then
			love.timer.sleep(1/5 - dt)
		end
				if KeyMap:isHeld("left") then
					local testX = pieceX - 1
					if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end
			elseif KeyMap:isHeld("right") then
					local testX = pieceX + 1
					if canPieceMove(testX, pieceY, pieceRotation) then pieceX = testX end
			elseif  KeyMap:isHeld("up") then
			
			elseif KeyMap:isHeld("down") then
					while canPieceMove(pieceX, pieceY + 1, pieceRotation) do
						pieceY = pieceY + 1
						tetristimer = tetristimerLimit
						love.timer.sleep(1/24 - dt)
					end
			end
		
	end
end

function love.mousereleased( button )

	mx = love.mouse.getX()
	my = love.mouse.getY()
	
	for i , b in pairs( buttons ) do
		if CheckCollision( mx , my , b ) then
			
			if not KeyMap:isBinding() then
				KeyMap:addBind(b.id)
			else
				KeyMap:unbind(b.id)
			end
			
		end
	end

end

function keymapdraw()
	love.graphics.setColor( 255 , 255 , 255 , 255 )
	
	love.graphics.print("Click \"BIND\" and press a button to bind." , 500 , 800 )
	love.graphics.print("Click \"BIND\" and click again to unbind all." , 500 , 820 )
	love.graphics.print("Click \"BIND\" and press escape to cancel binding." , 500 , 840 )
	
	
	local num = 1
	for i , k in pairs( KeyMap:getMapping() ) do
	
		if KeyMap:isDown( i ) then
			love.graphics.setColor( 255 , 255 , 0 , 255 )
			love.graphics.rectangle( "fill" , 5+500 , (2 + num * 20)+400 , 10 , 10)
			love.graphics.setColor( 255 , 255 , 255 , 255)
		end
		
		if KeyMap:isPushed( i ) then
			love.graphics.setColor( 255 , 255 , 0 , 255 )
			love.graphics.rectangle( "fill" , 15+500 , (2 + num * 20 )+400, 10 , 10)
			love.graphics.setColor( 255 , 255 , 255 , 255)
		end
		
		if KeyMap:isHeld( i ) then
			love.graphics.setColor( 255 , 255 , 0 , 255 )
			love.graphics.rectangle( "fill" , 25+500 , (2 + num * 20)+400 , 10 , 10)
			love.graphics.setColor( 255 , 255 , 255 , 255)
		end
	
		love.graphics.print( tostring(i) ,50 +500 , (num * 20)+400 )
		
		for j , m in pairs( k.keys ) do
			
			if m == " " then
				love.graphics.print( "space" , (100 + 50 * j) +500 , (num * 20) +400 )
			else
				love.graphics.print( m , (100 + 50 * j) +500 , (num * 20) +400 )
			end
			
		end
		
		for j , m in pairs( k.joy ) do
			
			love.graphics.print( m , (300 + 55 * j)+500 , (num * 20)+ 400 )
			
		end
		
		love.graphics.rectangle( "fill" , buttons[num].x , buttons[num].y , buttons[num].w , buttons[num].h)
		
		if not KeyMap:isBinding() then
			love.graphics.setColor( 0 , 0 , 0 , 255 )
			love.graphics.print("BIND" , buttons[num].x + 30 , buttons[num].y)
			love.graphics.setColor( 255 , 255 , 255 , 255 )
		end
		
		
		
		num = num + 1
		
	end

end

function CheckCollision(x,y,box)
  return x < box.x+box.w and x > box.x and y < box.y + box.h and  y > box.y
end
