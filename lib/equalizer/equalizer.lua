-- This function has been modified assisted by chatGPT. Concretely the particle effect and color fading effect were written by it.

function equalizerload(song)
	
	--love.graphics.setCaption('loveEQ')
	sound = love.sound.newSoundData(song)
	rate = sound:getSampleRate()
    channels = sound:getChannels()
	--print(rate)
    --print(channels)
	
	music = love.audio.newSource(sound)
	music:play()
	
	spectrum = {}
	gTime = 0
	
	-- used for geometry
	volumeThreshold = 0.5  -- Adjust the volume threshold for triggering the effect
	triggerDuration = 0.2  -- Adjust the duration of the triggered effect
	rotationSpeed = math.pi  -- Adjust the rotation speed
	centerX, centerY = love.graphics.getWidth() / 2, love.graphics.getHeight() / 2
	angle = 0
	bassIntensity = 0

	triggerTime = 0
	
	particles = {}  -- Initialize the particles table
	particleImage = love.graphics.newImage("assets/interface/particle.png")
end

function equalizerupdate(dt)
	local curSample = music:tell('samples')
	local wave = {}
	local size = next_possible_size(2048)
	--This condition ensures that curSample + size - 1 does not exceed the total number of samples in the audio. If it does, the code inside the if block won't be executed, preventing the "Attempt to get out-of-range sample!" error.
    if curSample + size - 1 <= sound:getSampleCount() then	
		if channels == 2 then
			for i=curSample, (curSample+(size-1) / 2) do
				sample = (sound:getSample(i * 2) + sound:getSample(i * 2 + 1)) * 0.5
				table.insert(wave,complex.new(sample,0))
			end
		else
			for i=curSample, curSample+(size-1) do
				table.insert(wave,complex.new(sound:getSample(i),0))
			end
		end
    
	local spec = fft(wave,false)
	--local reconstructed = fft(spec,true)
	
	
	function divide(list, factor)
		for i,v in ipairs(list) do
			list[i] = list[i] / factor
		end
	end
	
	divide(spec,size/2)
	
	spectrum = spec
	
	updateparticles(dt)
	else
	end
end

function updateRealTimeGeometry(dt)

  -- Update the audio input (calculate spectrum, volume, etc.)
    local spectrum = {}  -- Replace this with your actual spectrum data
    
    -- Calculate the intensity of a specific frequency band (e.g., bass)
    
    local startFrequencyIndex = 1  -- Adjust this based on the frequency range you want to analyze
    --local endFrequencyIndex = #spectrum  -- Adjust this based on the frequency range you want to analyze
    local endFrequencyIndex = math.min(#spectrum, 20)

if startFrequencyIndex <= endFrequencyIndex then    
    for i = startFrequencyIndex, endFrequencyIndex do
        -- Accumulate intensity values in the specified frequency range
        bassIntensity = bassIntensity + spectrum[i]
    end
    
    -- Normalize the intensity based on the frequency range
    local maxIntensity = (endFrequencyIndex - startFrequencyIndex + 1) * 1.0  -- Adjust as needed
    bassIntensity = bassIntensity / maxIntensity
    
    -- Map the bass intensity to a rotation speed
    local minSpeed = 0.1
    local maxSpeed = 3.0
    rotationSpeed = minSpeed + (maxSpeed - minSpeed) * bassIntensity
    
    -- Cap the rotation speed within a reasonable range
    rotationSpeed = math.min(math.max(rotationSpeed, minSpeed), maxSpeed)
    
    -- Update the angle based on the rotation speed
    angle = (angle + rotationSpeed * dt) % (2 * math.pi)
end

end

function updateparticles(dt)

  -- Emit new particles
    local particle = {
        x = love.math.random(0, love.graphics.getWidth()),
        y = love.graphics.getHeight(),
        speedY = -love.math.random(50, 150),  -- Negative Y direction to move upwards
        alpha = 1.0  -- Initial alpha value
    }
    table.insert(particles, particle)
    
    -- Update particles
    for i = #particles, 1, -1 do
        local particle = particles[i]
        particle.y = particle.y + particle.speedY * dt
        particle.alpha = particle.alpha - 0.5 * dt  -- Fading out over time
        
        if particle.alpha <= 0 then
            table.remove(particles, i)  -- Remove faded-out particles
        end
    end

end

function equalizerdraw()
	local division = 10
	  local colors = {
        {1, 0.5, 0.5, 0.6},
        {0.5, 0.8, 1, 0.6},
        {0.5, 1, 0.5, 0.6}
    }
	
	local trailLength = 5  -- Adjust the length of the motion trails
	local trailOpacityDecay = 0.2  -- Adjust the opacity decay rate of the motion trails
	
    for i = 1, #spectrum / division do
        local height = love.graphics.getHeight()
        local width = love.graphics.getWidth()
        local dist = width / (#spectrum / division)
        
        local v = spectrum[i]
        local n = height * 2 * v:abs()
        
        for t = 0, trailLength do
            local trailOpacity = 1 - t * trailOpacityDecay
            
            local colorIndex = ((i - 1) / (#spectrum / division)) * (#colors - 1) + 1
            local lowerColorIndex = math.floor(colorIndex)
            local upperColorIndex = math.min(math.ceil(colorIndex), #colors)
            local colorFactor = colorIndex - lowerColorIndex

            local color = {}
            for c = 1, 4 do
                color[c] = (1 - colorFactor) * colors[lowerColorIndex][c] + colorFactor * colors[upperColorIndex][c]
            end
            
            -- Apply trail transparency to the color
            color[4] = color[4] * trailOpacity
            
            love.graphics.setColor(unpack(color))
		
		love.graphics.rectangle(
			'line',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)

			
		--love.graphics.setColor(1,0.5,0.5,0.3)
		love.graphics.rectangle(
			'fill',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)

	end	
	
	-- add a beautiful particle effect
	 for _, particle in ipairs(particles) do
        love.graphics.setColor(1, 1, 1, particle.alpha)
        love.graphics.draw(particleImage, particle.x, particle.y)
    end
    

		--drawGeometry(dt, dist, division)
		love.graphics.setColor(1,1,1,1)
	end
end

function drawGeometry(dt, dist, division)

   -- Calculate the line's length based on the bass intensity
    local lineLength = 100 + bassIntensity * 100
    
    -- Calculate the end point of the line
    local lineEndX = centerX + math.cos(angle) * lineLength
    local lineEndY = centerY + math.sin(angle) * lineLength
    
    -- Calculate the division index that the line should be associated with
    local divisionIndex = math.floor((angle / (2 * math.pi)) * (#spectrum / division)) + 1
    
    -- Calculate the position of the line based on the division
    local lineStartX = (divisionIndex - 1) * dist * 1.5
    local lineStartY = height / 1 - lineLength
    
    -- Draw the line
    love.graphics.setColor(1, 1, 1)
    love.graphics.line(lineStartX, lineStartY, lineEndX, lineEndY)

end

--[[
equalizerdrawOLD

	local division = 10
	for i=1, #spectrum/division do
		local height = love.graphics.getHeight()
		local width = love.graphics.getWidth()
		local dist = width / (#spectrum/division)
		
		local v = spectrum[i]
		local n = height * 2 * v:abs(),0
		--if n>50 then
        love.graphics.setColor(1,0.5,0.5,0.6)
		love.graphics.rectangle(
			'line',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)
		--end
	
		
		love.graphics.setColor(1,0.5,0.5,0.3)
		love.graphics.rectangle(
			'fill',
			(i-1)*dist*1.5,height/1 - n,
			20,n
		)
		
		
		love.graphics.setColor(1,1,1,1)
--]]

function limit_max(v,limit)
	if v > limit then
		return limit
	end
	return v
end

function limit_min(v,limit)
	if v < limit then
		return limit
	end
	return v
end
