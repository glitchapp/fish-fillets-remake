--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

function recordload()

ReturnButton = {
	text = "Return",
	x = 1000,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	font = poorfishmiddle,
}

	local major, minor, revision, codename = love.getVersion()
	ver = string.format("Version %d.%d.%d - %s", major, minor, revision, codename)

	devices = love.audio.getRecordingDevices()
	num = #devices
	d = devices[1]
		name = d:getName()
		channels = d:getChannelCount()
		bits = d:getBitDepth()		
		rate = d:getSampleRate()
    
end


local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end


local function drawButton (button, hovered)
	
	love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)
end

function recordupdate(dt)
	samples = d:getSampleCount()
	recording = d:isRecording()
end

function love.keypressed(key)
	if key=="escape" then
		love.event.quit()
	end

end

function love.mousepressed(x, y)
	num = num + 1 -- to check mouse input has been detected.
	if recording then
		data = d:getData()
		d:stop()
		local source = love.audio.newSource(data)
		love.audio.play(source)
	elseif data == nil then
		success = d:start(100000, 44100, 16, 1)
		rate = d:getSampleRate()
	end

end

function recorddraw()
	love.graphics.print("num : "..num, 40, 40)
	love.graphics.print("bits : "..bits, 40, 80)
	love.graphics.print("channels : "..channels, 40, 120)
	love.graphics.print("name : "..name, 40, 160)
	love.graphics.print("samples : "..samples, 40, 200)
	love.graphics.print("rate : "..rate, 40, 240)
	love.graphics.print("recording : "..tostring(recording), 300, 40)
	love.graphics.print("ver : "..ver, 300, 100)
	if success then love.graphics.print("Success!", 300, 200) end
	
	local hovered = isButtonHovered (ReturnButton)
	drawButton (ReturnButton, hovered)
	
	if hovered and love.mouse.isDown(1) then
	TEsound.play("externalassets/sounds/GUI_Sound_Effects/GUI_Sound_Effects_021.ogg","static")
		love.timer.sleep( 0.3 )
		gamestatus="levelselection"
	end

end
