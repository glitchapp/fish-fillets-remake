# -----------------------------------------------------------------------------
# WebP to PNG Conversion and Text Replacement Script
# -----------------------------------------------------------------------------
# Written by: ChatGPT
#
# This Python script attempts to convert all files inside the game from WebP to PNG format
# and substitutes every call to the WebP library with the standard function to load PNGs.
# The goal of this script is to make the game playable on Android devices since the libwebp
# library is not compiled for ARM.
#
# Please note that this script has not been thoroughly tested and may require
# customization to suit your specific use case. Users are encouraged to test
# and report any issues or provide feedback for improvements.
#
# Required Libraries:
# - Pillow (PIL): Used for image processing.
#
# To install the required libraries, run the following command:
# pip install pillow
#
# Usage:
# 1. Set the 'base_directory' variable to the root directory of your game files.
# 2. Run the script.
#
# -----------------------------------------------------------------------------


import os
from PIL import Image
import fileinput
import re

# Function to convert WebP files to PNG
def convert_webp_to_png(file_path):
    try:
        img = Image.open(file_path)
        if img.format == "WEBP":
            img.save(file_path.replace(".webp", ".png"), "PNG")
            os.remove(file_path)  # Optional: Remove the original WebP file
    except Exception as e:
        print(f"Error processing {file_path}: {e}")

# Function to replace text in text files
def replace_text_in_files(files, old_text_pattern):
    for file_path in files:
        try:
            with open(file_path, 'r') as file:
                file_text = file.read()

            # Replace the old text pattern with the new text
            new_text = re.sub(old_text_pattern, r'love.graphics.newImage("\1.png")', file_text)

            # Write the updated text back to the file
            with open(file_path, 'w') as file:
                file.write(new_text)
        except Exception as e:
            print(f"Error processing {file_path}: {e}")

# Directory where your files are located
base_directory = '/path/to/your/directory'

# Task 1: Walk through all subdirectories and convert WebP to PNG
for root, _, files in os.walk(base_directory):
    for file in files:
        file_path = os.path.join(root, file)
        if file.lower().endswith('.webp'):
            convert_webp_to_png(file_path)

# Task 2: Replace text in text files
# Define the old_text pattern with a wildcard for the filename
old_text_pattern = r'love.graphics.newImage\(WebP.loadImage\(love.filesystem.newFileData\("(.*\.webp)"\)\)\)'
for root, _, files in os.walk(base_directory):
    text_files = [os.path.join(root, file) for file in files if file.lower().endswith('.txt')]
    replace_text_in_files(text_files, old_text_pattern)

