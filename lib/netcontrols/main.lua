--[[
 Copyright (C) 2022  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

--]]

--[[ This code is a Lua script that was fully written by me, ChatGPT, an AI language model.

This is a Lua code that listens to incoming messages on a UDP socket, updates a controller state based on those messages, and sends the updated controller state back over the network.
The code uses the Love2D game framework's keypressed and keyreleased functions to handle user input events and update the controller state accordingly.
The code defines three functions, update_controller_state, send_controller_state, and love.draw, to update the controller state, send the controller state over the network, and draw the game screen, respectively. The code also defines the love.update function to update the game state and check for incoming messages.
--]]

-- Import the socket library
local socket = require("socket")

-- Define the IP address and port number to listen on
local ip_address = "192.168.178.36"
local port = 12345

-- Create a UDP socket to listen for incoming messages
local udp_socket = socket.udp()
udp_socket:setsockname(ip_address, port)
udp_socket:settimeout(0)

-- Initialize the controller state
local controller_state = {
    up = false,
    down = false,
    left = false,
    right = false,
    fire = false
}

-- Initialize variables
message= 0
action= 0
state= 0
data= "0"

-- Define the function to update the controller state based on incoming messages
function update_controller_state(data)
    -- Parse the incoming message
    --message = data:split(",")
    message = data
    action = message[1]
    state = message[2]

    -- Update the controller state based on the incoming message
    if action == "up" then 
        controller_state.up = (state == "true")
    elseif action == "down" then
        controller_state.down = (state == "true")
    elseif action == "left" then
        controller_state.left = (state == "true")
    elseif action == "right" then
        controller_state.right = (state == "true")
    elseif action == "fire" then
        controller_state.fire = (state == "true")
    end
end

-- Define the function to send the controller state over the network
function send_controller_state()
    -- Encode the controller state as a message
    local message = ""
    message = message .. (controller_state.up and "1" or "0")
    message = message .. (controller_state.down and "1" or "0")
    message = message .. (controller_state.left and "1" or "0")
    message = message .. (controller_state.right and "1" or "0")
    message = message .. (controller_state.fire and "1" or "0")

    -- Send the message over the network
    udp_socket:sendto(message, ip_address, 54321)
end

-- Define the function to handle user input events
function love.keypressed(key)
    -- Update the controller state based on the user input
    if key == "up" then
        controller_state.up = true
    elseif key == "down" then
        controller_state.down = true
    elseif key == "left" then
        controller_state.left = true
    elseif key == "right" then
        controller_state.right = true
    elseif key == "space" then
        controller_state.fire = true
    end

    -- Send the updated controller state over the network
    send_controller_state()
end

function love.keyreleased(key)
    -- Update the controller state based on the user input
    if key == "up" then
        controller_state.up = false
    elseif key == "down" then
        controller_state.down = false
    elseif key == "left" then
        controller_state.left = false
    elseif key == "right" then
        controller_state.right = false
    elseif key == "space" then
        controller_state.fire = false
    end

    -- Send the updated controller state over the network
    send_controller_state()
end

-- Define the function to draw the game screen
function love.draw()
    -- Draw the game screen here
    love.graphics.print ("message: " .. message,100,100)
    --love.graphics.print ("action: " .. action,100,200)
    --love.graphics.print ("state: " .. state,100,300)
end

-- Define the function to update the game state
function love.update(dt)
    -- Check for incoming messages
    update_controller_state(data)
    send_controller_state()
end


