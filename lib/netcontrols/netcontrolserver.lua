-- Define the function to update the controller state based on incoming messages
function update_controller_state(data)
    -- Parse the incoming message
    local message = data:split(",")
    local action = message[1]
    local state = message[2]

    -- Update the controller state based on the incoming message
    if action == "up" then
        controller_state.up = (state == "true")
    elseif action == "down" then
        controller_state.down = (state == "true")
    elseif action == "left" then
        controller_state.left = (state == "true")
    elseif action == "right" then
        controller_state.right = (state == "true")
    elseif action == "fire" then
        controller_state.fire = (state == "true")
    end
end

function receiveincomingmessages()

	-- Receive incoming messages
	while true do
		data, ip, port = udp_socket:receivefrom()
	if not data then
		break
	end
		update_controller_state(data)
	end
-- End of the Lua script.
end
