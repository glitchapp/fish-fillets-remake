function myshadowsload()
Shadows = require("shadows")
LightWorld = require("shadows.LightWorld")
Light = require("shadows.Light")
Body = require("shadows.Body")
PolygonShadow = require("shadows.ShadowShapes.PolygonShadow")
CircleShadow = require("shadows.ShadowShapes.CircleShadow")
-- Create a light world
newLightWorld = LightWorld:new()

-- Create a light on the light world, with radius 300
newLight = Light:new(newLightWorld, 200)

-- Set the light's color to white
newLight:SetColor(0, 255, 0, 55)

-- Set the light's position
newLight:SetPosition(400, 400)

-- Create a body
--newBody = Body:new(newLightWorld)

-- Set the body's position and rotation
--newBody:SetPosition(300, 300)
--newBody:SetAngle(-15)

-- Create a polygon shape on the body with the given points
--PolygonShadow:new(newBody, -10, -10, 10, -10, 10, 10, -10, 10)

-- Create a circle shape on the body at (-30, -30) with radius 16
--CircleShadow:new(newBody, -30, -30, 16)

-- Create a second body
--newBody2 = Body:new(newLightWorld)

-- Set the second body's position
--newBody2:SetPosition(350, 350)

-- Add a polygon shape to the second body
--PolygonShadow:new(newBody2, -20, -20, 20, -20, 20, 20, -20, 20)
end

function myshadowsupdate()
if android==false then
newLight:SetPosition(love.mouse.getX(), love.mouse.getY(), 1.1)
	newLightWorld:Update()
	end
end
function myshadowsdraw(dt)
	-- Clear the screen
	love.graphics.setColor(1,1,1)
	love.graphics.rectangle("fill", 0, 0, 1000,800)
	
	-- Draw the light world with white color
	newLightWorld:Draw()
end


