--# Include
dream = require("3DreamEngine")
collision = require("3DreamEngine/collision")


--# Directories
local modelsDir = "objects/models"

--# Point
local world = {}

--# Model Data
world.modelData = {}
world.scene = {}

--cube
--world.modelData.cube = {}
--world.modelData.cube.objectDir = modelsDir.."/cube/cube"
--world.modelData.cube.size = vec3(1, 1, 1)
--[[
--ground
world.modelData.ground = {}
world.modelData.ground.objectDir = modelsDir.."/ground/ground"
world.modelData.ground.size = vec3(300, 1, 300)
--simplewall
world.modelData.simplewall = {}
world.modelData.simplewall.objectDir = modelsDir.."/simplewall/simplewall"
world.modelData.simplewall.size = vec3(10, 10, 2)
--wallwindow
world.modelData.wallwindow = {}
world.modelData.wallwindow.objectDir = modelsDir.."/wallwindow/wallwindow"
world.modelData.wallwindow.size = vec3(5, 5, 1)
--]]

--# Functions
function world.newModel(name, position, rotation)
	rotation = rotation or 0

	local model = {}
	model.ref = world.modelData[name]
	model.name = name
	model.object = dream:loadObject(model.ref.objectDir.."_rot"..tostring(rotation))
	model.position = position or vec3(0, 0, 0)
	model.rotation = rotation or 0
	model.size = model.ref.size
	model.origSize = model.ref.size
	model.collider = collision:newMesh(model.object, position)
	table.insert(world.scene, model)

	return model
end

function world.updateModelPos(model, position)
	model.collider:moveTo(position)
	model.position = position

	return model
end

function world.updateModelSize(model)
	local rot = model.rotation
	if rot < 0 then rot = rot * -1 end

	if rot == 90 or rot == 270 then
		model.size = vec3(model.origSize.z, model.origSize.y, model.origSize.x)
	elseif rot == 0 or rot == 180 or rot == 360 then
		model.size = vec3(model.origSize.x, model.origSize.y, model.origSize.z) 
	end

	return model
end

function world.rotateModel(model, rotation, update_size)
	--model.object:rotateY(math.rad(rotation))
	if rotation < 0 then rotation = rotation * -1 end

	rotation = model.rotation + rotation
	if rotation == 90 or rotation == 270 then
		rotation = 90
		print("is 90")
	elseif rotation == 0 or rotation == 180 or rotation == 360 then
		rotation = 0
		print("is 0")
	end
	model.rotation = rotation
	model.object = nil
	model.collider = nil
	model.object = dream:loadObject(model.ref.objectDir.."_rot"..tostring(model.rotation))
	model.collider = collision:newMesh(model.object, model.position)

	if update_size == nil then update_size = true end
	if update_size == true then
		world.updateModelSize(model)
	end

	return model
end


--# Finalize
return world
