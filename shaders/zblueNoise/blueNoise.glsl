extern number intensity;
    extern Image blueNoiseTexture;

    vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
        // Get the grayscale color value
        vec4 texColor = Texel(texture, texture_coords);
        number gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));

        // Calculate the dither threshold using the blue noise texture
        vec2 noiseCoords = screen_coords / love_ScreenSize.xy;
        number noiseValue = Texel(blueNoiseTexture, noiseCoords).r;
        number threshold = noiseValue * intensity;

        // Apply dithering
        number dithered = (gray + threshold) / (1.0 + intensity);

        // Output the final color with alpha from the original texture
        return vec4(vec3(dithered), texColor.a);
    }
