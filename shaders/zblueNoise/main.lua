-- This software has been created by chatGPT, it may be tweaked and modified in the future.

-- Create a blue noise dithering shader
local blueNoiseDitherShader = love.graphics.newShader([[
    extern number intensity;
    extern Image blueNoiseTexture;

    vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
        // Get the grayscale color value
        vec4 texColor = Texel(texture, texture_coords);
        number gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));

        // Calculate the dither threshold using the blue noise texture
        vec2 noiseCoords = screen_coords / love_ScreenSize.xy;
        number noiseValue = Texel(blueNoiseTexture, noiseCoords).r;
        number threshold = noiseValue * intensity;

        // Apply dithering
        number dithered = (gray + threshold) / (1.0 + intensity);

        // Output the final color with alpha from the original texture
        return vec4(vec3(dithered), texColor.a);
    }
]])


-- Load the blue noise texture
local blueNoiseTexture = love.graphics.newImage("shaders/blueNoise/BlueNoise470.png")

-- Set the intensity of the dithering effect
local ditherIntensity = 0.5

function love.load()

	-- load an image
	image = love.graphics.newImage("capturelevel45.png")
    
    -- Set the blue noise dithering shader
    love.graphics.setShader(blueNoiseDitherShader)

    -- Pass the intensity value and blue noise texture to the shader
    blueNoiseDitherShader:send("intensity", ditherIntensity)
    blueNoiseDitherShader:send("blueNoiseTexture", blueNoiseTexture)

    -- ...
end



function love.draw()
    -- Set the dithering shader
    love.graphics.setShader(blueNoiseDitherShader)

    -- Pass the intensity value to the shader
    blueNoiseDitherShader:send("intensity", ditherIntensity)

    -- Draw your scene here
    love.graphics.draw(image)

    -- Reset the shader
    love.graphics.setShader()
end

