-- This software has been created by chatGPT, it may be tweaked and modified in the future.

-- Create a blue noise dithering shader
blueNoiseDitherShader = love.graphics.newShader([[
extern number paletteWidth = 16;
extern Image paletteTexture;
extern number intensity;
extern Image blueNoiseTexture;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    // Get the grayscale color value
    vec4 texColor = Texel(texture, texture_coords);
    number gray = dot(texColor.rgb, vec3(0.299, 0.587, 0.114));

    // Calculate the dither threshold using the blue noise texture
    vec2 noiseCoords = screen_coords / love_ScreenSize.xy;
    number noiseValue = Texel(blueNoiseTexture, noiseCoords).r;
    number threshold = noiseValue * intensity;

    // Apply dithering
    number dithered = (gray + threshold) / (1.0 + intensity);

    // Define the ZX Spectrum color palette
    vec3 palette[16];
	palette[0] = vec3(0.0, 0.0, 0.0);        // Black
	palette[1] = vec3(0.0, 0.0, 1.0);        // Blue
	palette[2] = vec3(1.0, 0.0, 0.0);        // Red
	palette[3] = vec3(1.0, 0.0, 1.0);        // Magenta
	// Define the remaining color values for each index in the palette

    // Map the dithered grayscale value to the corresponding palette index
    int index = int(dithered * 15.0);
    vec3 finalColor = palette[index];

    // Output the final color with alpha from the original texture
    return vec4(finalColor, texColor.a);
}



]])


-- Load the blue noise texture
local blueNoiseTexture = love.graphics.newImage("shaders/zblueNoise/BlueNoise470.png")

-- Load zx palette
local zxpaletteTexture = love.graphics.newImage("shaders/zblueNoise/zxpaletteTexture.png")

-- Set the intensity of the dithering effect
local ditherIntensity = 0.5
