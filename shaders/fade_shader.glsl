extern float time;
extern Image noiseTexture;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    // Sample the blue noise texture
    vec4 noise = Texel(noiseTexture, screen_coords / love_ScreenSize.xy);

    // Calculate the alpha value based on time, blue noise, and Y-coordinate
    float alpha = smoothstep(1.0, 0.0, time) * noise.r;

    // Introduce the curtain effect based on the Y-coordinate
    float curtainEffect = smoothstep(0.0, 0.1, abs(screen_coords.y - love_ScreenSize.y / 2.0));
    alpha *= curtainEffect;

    // Add some variations to the curtain effect
    curtainEffect += noise.g * 0.2; // Adjust the multiplier as needed

    vec4 pixel = Texel(texture, texture_coords);

    // Apply the alpha to the pixel color
    pixel.a *= alpha;

    return pixel * color;
}

